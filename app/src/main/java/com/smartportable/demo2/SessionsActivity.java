package com.smartportable.demo2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.smartportable.demo2.adapters.SessionsAdapter;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.PreferencesManager;
import com.smartportable.demo2.helper.PunchLogs;
import com.smartportable.demo2.helper.RecyclerListSelectorModel;
import com.smartportable.demo2.interfaces.SelectedListListener;
import com.smartportable.demo2.interfaces.SessionSelected;
import com.smartportable.demo2.models.SessionsInfoModel;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import xyz.danoz.recyclerviewfastscroller.vertical.VerticalRecyclerViewFastScroller;

/**
 * Created by Asfak on 3/27/2017.
 */

public class SessionsActivity extends AppCompatActivity implements View.OnLongClickListener, SessionSelected, SelectedListListener {
    RecyclerView rvSessionsList;
    TextView tvSessionWarningMsg;
    VerticalRecyclerViewFastScroller fastScrollerSessionsList;
    DatabaseHandler db;
    SessionsAdapter sessionListAdapter;
    ArrayList<SessionsInfoModel.DataBean> sessionList = new ArrayList<SessionsInfoModel.DataBean>();
    ArrayList<RecyclerListSelectorModel> list_boolean = new ArrayList<RecyclerListSelectorModel>();
    boolean deleteSession = false, displayDeleteOption = false;

    public static final int ADD_SESSION = 1;
    public static final String SESSION_POSITION = "edit_session_position";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sessions);

        rvSessionsList = (RecyclerView) findViewById(R.id.rvSessionsList);
        tvSessionWarningMsg = (TextView) findViewById(R.id.tvSessionWarningMsg);
        //fastScrollerSessionsList = (VerticalRecyclerViewFastScroller) findViewById(R.id.fastScrollerSessionsList);

        rvSessionsList.setHasFixedSize(true);
        rvSessionsList.setLayoutManager(new LinearLayoutManager(this));
        // Connect the recycler to the scroller (to let the scroller scroll the list)
        //fastScrollerSessionsList.setRecyclerView(rvSessionsList);

        //fastScrollerSessionsList.setVisibility(View.GONE);
        tvSessionWarningMsg.setVisibility(View.VISIBLE);

        db = new DatabaseHandler(this);
        new GetSessionsList().execute();

        rvSessionsList.setOnLongClickListener(this);
    }

    @Override
    public boolean onLongClick(View v) {
        sessionListAdapter.enableCheckBox(true);
        sessionListAdapter.notifyDataSetChanged();
        return false;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_session, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (deleteSession) {
            menu.findItem(R.id.action_add_session).setVisible(false);
            if (displayDeleteOption) {
                menu.findItem(R.id.action_delete_session).setVisible(true);
                displayDeleteOption = false;
            } else {
                menu.findItem(R.id.action_delete_session).setVisible(false);
            }
        } else {
            menu.findItem(R.id.action_add_session).setVisible(true);
            menu.findItem(R.id.action_delete_session).setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add_session) {
            Log.d("SessionActivity", "Add Session");
            startActivityForResult(new Intent(this, AddSessionActivity.class), ADD_SESSION);

            if (URLCollections.whichDirection(SessionsActivity.this) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
            } else {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            }
            return true;
        }

        if (item.getItemId() == R.id.action_delete_session) {
            Log.d("SessionActivity", "Delete Sessions");
            ArrayList<Integer> selectedSessionId = new ArrayList<Integer>();
            for (int i=0; i<list_boolean.size();i++) {
                if (list_boolean.get(i).isSelected()) {
                    selectedSessionId.add(""+sessionList.get(i).getSessionId());
                } else {
                }
            }
            if (selectedSessionId.size() == 0) {
                Boast.makeText(SessionsActivity.this, getString(R.string.sel_one_session_min), Toast.LENGTH_SHORT).show();
            } else {
                new DeleteSessions(selectedSessionId).execute();
            }
            return true;
        }

        if (item.getItemId() == android.R.id.home) {
            Log.d("SessionActivity", "Home Button Click");
            ActionBar ab = getSupportActionBar();
            CharSequence title_str = ab.getTitle();
            deleteSession = false;
            if (title_str.equals(getString(R.string.delete_session))) {
                sessionListAdapter.enableCheckBox(false);
                sessionListAdapter.updateSessionList(sessionList);

                if (sessionList.size() > 0) {
                    list_boolean.clear();
                    for (int i = 0; i < sessionList.size(); i++) {
                        RecyclerListSelectorModel model = new RecyclerListSelectorModel();
                        model.setSelected(false);
                        list_boolean.add(model);
                    }
                }
                sessionListAdapter.updateListBoolean(list_boolean);
                sessionListAdapter.notifyDataSetChanged();
                ab.setTitle(R.string.sessions_title);
                ab.setDisplayHomeAsUpEnabled(true);
                invalidateOptionsMenu();
            }else {
                invalidateOptionsMenu();
                SessionsActivity.this.finish();
                if (URLCollections.whichDirection(SessionsActivity.this) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                }
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    private class GetSessionsList extends AsyncTask<Void, Void, Void> {
        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SessionsActivity.this, R.style.MyTheme);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            sessionList = db.getAllSessions();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (sessionList.size() > 0) {
                rvSessionsList.setVisibility(View.VISIBLE);
                tvSessionWarningMsg.setVisibility(View.GONE);
                if (sessionList.size() > 15)
                //fastScrollerSessionsList.setVisibility(View.VISIBLE);

                for (int i=0; i<sessionList.size(); i++) {
                    //Sessions session = sessionList.get(i);
                    //Log.d("Session name", session.getSessionName());
                    RecyclerListSelectorModel model = new RecyclerListSelectorModel();
                    model.setSelected(false);
                    list_boolean.add(model);
                }

                //sessionListAdapter = new SessionsAdapter(SessionsActivity.this, sessionList, list_boolean);
                sessionListAdapter.enableCheckBox(false);
                rvSessionsList.setAdapter(sessionListAdapter);

            } else {
                rvSessionsList.setVisibility(View.GONE);
                //fastScrollerSessionsList.setVisibility(View.GONE);
                tvSessionWarningMsg.setVisibility(View.VISIBLE);
            }

            // if delete opration perform then set to normal state
            ActionBar ab = getSupportActionBar();
            CharSequence title_str = ab.getTitle();
            deleteSession = false;
            if (title_str.equals(getString(R.string.delete_session))) {
                sessionListAdapter.enableCheckBox(false);
                //sessionListAdapter.updateSessionList(sessionList);

                if (sessionList.size() > 0) {
                    list_boolean.clear();
                    for (int i = 0; i < sessionList.size(); i++) {
                        RecyclerListSelectorModel model = new RecyclerListSelectorModel();
                        model.setSelected(false);
                        list_boolean.add(model);
                    }
                }
                sessionListAdapter.updateListBoolean(list_boolean);
                sessionListAdapter.notifyDataSetChanged();
                ab.setTitle(getString(R.string.sessions_title));
                ab.setDisplayHomeAsUpEnabled(true);
                invalidateOptionsMenu();
            }
        }
    }

    public class DeleteSessions extends AsyncTask<Void, Void, Void> {
        ProgressDialog pDialogDeleteSession;
        ArrayList<Integer> selectedSessionList = new ArrayList<Integer>();

        public DeleteSessions (ArrayList<Integer> selectedSessionList) {
            this.selectedSessionList = selectedSessionList;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialogDeleteSession = new ProgressDialog(SessionsActivity.this, R.style.MyTheme);
            pDialogDeleteSession.setCancelable(false);
            pDialogDeleteSession.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (selectedSessionList.size() > 0) {
                for (int j=0; j<selectedSessionList.size();j++) {
                    db.deleteSessionFromSessionId(selectedSessionList.get(j));

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SessionsActivity.this);
                    int sessionId = prefs.getInt(PreferencesManager.KEY_SET_SESSION_ID, 0);
                    if (sessionId != 0) {
                        if (sessionId == selectedSessionList.get(j)) {
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putInt(PreferencesManager.KEY_SET_SESSION_ID, 0);
                            editor.putString(PreferencesManager.KEY_SET_SESSION_NAME, "");
                            editor.putLong(PreferencesManager.KEY_SET_SESSION_DATE, 0);
                            editor.putLong(PreferencesManager.KEY_SET_SESSION_END_TIME, 0);
                            editor.putLong(PreferencesManager.KEY_SET_SESSION_START_TIME, 0);
                            editor.commit();
                        }
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pDialogDeleteSession.isShowing()) {
                pDialogDeleteSession.dismiss();
            }
            if (selectedSessionList.size() > 0) {
                new GetSessionsList().execute();
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_SESSION && resultCode == ADD_SESSION) {
            new GetSessionsList().execute();
        }
    }

    @Override
    public void onSessionSelected() {
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle(getString(R.string.delete_session));
        deleteSession = true;
        invalidateOptionsMenu();
    }

    @Override
    public void onPunchListChanged(ArrayList<PunchLogs> list) {}
    @Override
    public void onItemRemoved(String card_no) {}

    @Override
    public void onListChanged(ArrayList<RecyclerListSelectorModel> list) {
        list_boolean = list;
        for (int i=0; i<list.size();i++) {
            if (list.get(i).isSelected()) {
                displayDeleteOption = true;
                break;
            }
        }
        invalidateOptionsMenu();
    }

    @Override
    public void onBackPressed() {
        ActionBar ab = getSupportActionBar();
        CharSequence title_str = ab.getTitle();
        deleteSession = false;
        if (title_str.equals(getString(R.string.delete_session))) {
            sessionListAdapter.enableCheckBox(false);
            //sessionListAdapter.updateSessionList(sessionList);

            if (sessionList.size() > 0) {
                list_boolean.clear();
                for (int i = 0; i < sessionList.size(); i++) {
                    RecyclerListSelectorModel model = new RecyclerListSelectorModel();
                    model.setSelected(false);
                    list_boolean.add(model);
                }
            }
            sessionListAdapter.updateListBoolean(list_boolean);
            sessionListAdapter.notifyDataSetChanged();
            ab.setTitle("Sessions");
            ab.setDisplayHomeAsUpEnabled(true);
            invalidateOptionsMenu();
        }else {
            invalidateOptionsMenu();
            super.onBackPressed();
            if(URLCollections.whichDirection(SessionsActivity.this) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
            }
        }

    }
}
