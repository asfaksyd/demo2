package com.smartportable.demo2.prefs;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.smartportable.demo2.R;
import com.smartportable.demo2.HomeScreen;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.helper.Boast;
import com.smartportable.demo2.helper.PreferencesManager;
import com.smartportable.demo2.helper.ServiceHelper;
import com.smartportable.demo2.receiver.SyncDataServiceBroadcastReceiver;
import com.smartportable.demo2.services.SyncDataService;
import com.smartportable.demo2.utils.KeysKt;
import com.smartportable.demo2.utils.SharedPref;

import fr.coppernic.sdk.utils.debug.Log;
import fr.coppernic.sdk.utils.helpers.CpcOs;

/**
 * Created by Ananth on 12/27/2016.
 */

public class MyPreferenceFragment extends PreferenceFragment {
    //PowerMgmt mPowerMgmt;
    CheckBoxPreference key_card_service;
    ListPreference /*change_lang,*/ /*set_device,*/ mifare_reader_type;
    /*SwitchPreference led_switch;
    PreferenceCategory pref_cat_led_on_off;*/
    EditTextPreference pref_key_sync_interval, set_conn_url;

    String actionStr = "";

    @Override
    public void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs_category);
        set_conn_url = (EditTextPreference) findPreference(KeysKt.keyBaseUrl);
        set_conn_url.setDefaultValue(SharedPref.INSTANCE.getStringValue(getActivity(), KeysKt.keyBaseUrl,""));
        set_conn_url.setText(SharedPref.INSTANCE.getStringValue(getActivity(), KeysKt.keyBaseUrl,""));
        //set_conn_url.setOnPreferenceClickListener(mConnUrlPrefClickListener);
        set_conn_url.setOnPreferenceChangeListener(mConnUrlPrefChangeListener);

        try {
            Bundle bundle = getArguments();
            if (bundle != null) {
                String actionStr = bundle.getString(HomeScreen.ACTION_STR);
                if (actionStr.equals(HomeScreen.SET_URL)) {

                    String url = set_conn_url.getEditText().getText().toString();

                    //mConnUrlPrefClickListener.onPreferenceClick(set_conn_url);
                    mConnUrlPrefChangeListener.onPreferenceChange(set_conn_url, url);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //change_lang = (ListPreference) findPreference(PreferencesManager.KEY_CHANGE_LANG);
        /*key_card_service = (CheckBoxPreference) findPreference(PreferencesManager.KEY_CARD_SERVICE);
        set_device = (ListPreference) findPreference(PreferencesManager.KEY_SET_DEVICE);
        led_switch = (SwitchPreference) findPreference(PreferencesManager.KEY_LED_SWITCH);
        pref_cat_led_on_off = (PreferenceCategory) findPreference(PreferencesManager.KEY_CAT_LED_SWITCH);*/
        mifare_reader_type = (ListPreference) findPreference(PreferencesManager.KEY_SET_MIFARE_READING_TYPE);
        pref_key_sync_interval = (EditTextPreference) findPreference(PreferencesManager.KEY_SYNC_INTERVAL);

        // check current set value of device and based on that enable/disabled led options
        /*SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(getActivity());
        String value = prefs.getString(PreferencesManager.KEY_SET_DEVICE, "");
        if (value == null && value.length() == 0) {
            value = URLCollections.DEVICE_CONE;
        }

        if (value.equals(URLCollections.DEVICE_CONE)) {
            led_switch.setEnabled(false);
            pref_cat_led_on_off.setEnabled(false);
            led_switch.setChecked(false);
        } else {
            led_switch.setEnabled(true);
            pref_cat_led_on_off.setEnabled(true);
        }

        if (CpcOs.isC5()) {
            mifare_reader_type.setEnabled(true);
            key_card_service.setEnabled(false);
            set_device.setEnabled(false);
            led_switch.setEnabled(false);
        }
        mPowerMgmt = new PowerMgmt(getActivity());*/

        // on preference change for language
        /*change_lang.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String value = (String) newValue;//((ListPreference) preference).getValue();
                String language_code = URLCollections.LANGUAGE_CODE_ENGLISH;
                if (value.equals(getActivity().getResources().getString(R.string.english_lang_label))) {
                    language_code = URLCollections.LANGUAGE_CODE_ENGLISH;
                } else {
                    language_code = URLCollections.LANGUAGE_CODE_ARABIC;
                }

                Resources res = getActivity().getResources();
                // Change locale settings in the app.
                DisplayMetrics dm = res.getDisplayMetrics();
                android.content.res.Configuration conf = res.getConfiguration();
                conf.setLocale(new Locale(language_code)); // API 17+ only.
                // Use conf.locale = new Locale(...) if targeting lower versions
                res.updateConfiguration(conf, dm);

                // change the status like app language has change by the user
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor edit = prefs.edit();
                edit.putBoolean(PreferencesManager.KEY_LANGUAGE_HAS_CHANGE, true);
                edit.commit();

                getActivity().finish();
                Intent intent = getActivity().getIntent();
                startActivity(intent);

                return true;
            }
        });*/

        /*set_device.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String value = (String) newValue;//((ListPreference) preference).getValue();
                if (value.equals(URLCollections.DEVICE_CONE)) {
                    led_switch.setEnabled(false);
                    pref_cat_led_on_off.setEnabled(false);
                    led_switch.setChecked(false);
                } else {
                    led_switch.setEnabled(true);
                    pref_cat_led_on_off.setEnabled(true);
                }
                return true;
            }
        });

        led_switch.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean switched = ((SwitchPreference) preference).isChecked();
                if (!switched) {
                    // to ON LED1 blue light on
                    mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.Led, PowerMgmt.ManufacturersCone.BuiltIn, PowerMgmt.ModelsCone.Led1, PowerMgmt.InterfacesCone.ExpansionPort, true);
                } else {
                    // to ON LED1 blue light off
                    mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.Led, PowerMgmt.ManufacturersCone.BuiltIn, PowerMgmt.ModelsCone.Led1, PowerMgmt.InterfacesCone.ExpansionPort, false);
                }
                return true;
            }
        });*/

        // Sync interval sharepreference value change event
        pref_key_sync_interval.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                int old_val = Integer.parseInt(prefs.getString(PreferencesManager.KEY_SYNC_INTERVAL, ""+300));
                int interval_sec = Integer.parseInt(""+newValue);
                if (newValue.toString().length() > 0) {
                    if (interval_sec >= 60) {
                        if (old_val != interval_sec) {
                            Log.d("Sync Interval", "Changed" + interval_sec);

                            AlarmManager alarmMgr = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);

                            if (CpcOs.isCone() || CpcOs.isC5()) {

                                boolean isServiceRunning = ServiceHelper.getInstance().isMyServiceRunning(getActivity(), SyncDataService.class);

                                Intent intent = new Intent(getActivity(), SyncDataServiceBroadcastReceiver.class);
                                PendingIntent alarmIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                                //alarmMgr.cancel(alarmIntent);
                                //alarmMgr.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), interval_sec * 1000, alarmIntent);*/

                                if (isServiceRunning) {
                                    //alarmIntent.putExtra(ServiceHelper.KEY_SERVICE_ACTION, ServiceHelper.STOP_SERVICE);
                                    //alarmIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                                    alarmMgr.cancel(alarmIntent);
                                    alarmIntent.cancel();
                                    alarmIntent.cancel();
                                    SyncDataServiceBroadcastReceiver.stopService(getActivity());

                                    // again start service
                                    intent = new Intent(getActivity(), SyncDataServiceBroadcastReceiver.class);
                                    //intent_restart.putExtra(ServiceHelper.KEY_SERVICE_ACTION, ServiceHelper.START_SERVICE);
                                    alarmIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
                                    alarmMgr = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                                }
                                alarmMgr.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), interval_sec * 1000, alarmIntent);
                            }
                            return true;
                        } else {
                            Log.d("Sync Interval", "Not Changed");
                            return false;
                        }
                    } else { // if less thenn 60 seconds
                        Boast.makeText(getActivity(), getString(R.string.min_60_secs), Toast.LENGTH_SHORT).show();
                        return false;
                    }
                } else {
                    Boast.makeText(getActivity(), getString(R.string.value_not_blank), Toast.LENGTH_SHORT).show();
                    return false;
                }

                //return true;
            }
        });
    }

    Preference.OnPreferenceChangeListener mConnUrlPrefChangeListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if (URLCollections.isValidUrl(String.valueOf(newValue))) {
                /*String host = URLCollections.getHostAddressFromURL(String.valueOf(newValue));
                Log.d("MyPreferebceFragment", "HostAddress "+ host);*/
                SharedPref.INSTANCE.setStringValue(getActivity(), KeysKt.keyAPIBaseUrl, String.valueOf(newValue)+"/API/api/");
                return true;
            }
            Boast.makeText(getActivity(), R.string.conn_string_not_define, Toast.LENGTH_SHORT).show();
            return false;
        }
    };

    Preference.OnPreferenceClickListener mConnUrlPrefClickListener = new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {

            return true;
        }
    };
}
