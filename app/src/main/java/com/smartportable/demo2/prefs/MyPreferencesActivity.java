package com.smartportable.demo2.prefs;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.smartportable.demo2.R;
import com.smartportable.demo2.HomeScreen;


/**
 * Created by Ananth on 12/27/2016.
 */

public class MyPreferencesActivity extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_non_cone_device);

        TextView tvDeviceWarning = (TextView) findViewById(R.id.tvDeviceWarning);
        ListView lvSharePrefs = (ListView) findViewById(android.R.id.list);
        LinearLayout llNonConeDevice = (LinearLayout) findViewById(R.id.llNonConeDevice);

        //if (CpcOs.isCone() || CpcOs.isC5()) {

            llNonConeDevice.setBackgroundColor(this.getResources().getColor(android.R.color.transparent));
            tvDeviceWarning.setVisibility(View.GONE);
            lvSharePrefs.setVisibility(View.VISIBLE);

            MyPreferenceFragment myPreferenceFragment = new MyPreferenceFragment();

            try {
                if (getIntent().getExtras() != null) {
                    String set_url_action = getIntent().getExtras().getString(HomeScreen.ACTION_STR);
                    if (set_url_action != null) {
                        Bundle bundle = new Bundle();
                        bundle.putString(HomeScreen.ACTION_STR, set_url_action);
                        myPreferenceFragment.setArguments(bundle);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            getFragmentManager().beginTransaction().replace(android.R.id.content, myPreferenceFragment).commit();

        /*} else {
            llNonConeDevice.setBackgroundColor(this.getResources().getColor(R.color.color_app_bg));
            tvDeviceWarning.setVisibility(View.VISIBLE);
            lvSharePrefs.setVisibility(View.GONE);
        }*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            MyPreferencesActivity.this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
