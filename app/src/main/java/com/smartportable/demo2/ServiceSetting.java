package com.smartportable.demo2;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.helper.Config;
import com.smartportable.demo2.helper.PreferencesManager;
import com.smartportable.demo2.helper.ServiceHelper;
import com.smartportable.demo2.receiver.ScanCardServiceBroadcastReceiver;
import com.smartportable.demo2.services.ScanCardService;

import java.util.Timer;
import java.util.TimerTask;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import fr.coppernic.cpcframework.cpcpowermgmt.cone.PowerMgmt;
import fr.coppernic.sdk.hid.iclassProx.BaudRate;
import fr.coppernic.sdk.hid.iclassProx.Card;
import fr.coppernic.sdk.hid.iclassProx.CascadeLevel;
import fr.coppernic.sdk.hid.iclassProx.ErrorCodes;
import fr.coppernic.sdk.hid.iclassProx.FrameProtocol;
import fr.coppernic.sdk.hid.iclassProx.OnGetReaderInstanceListener;
import fr.coppernic.sdk.hid.iclassProx.Reader;
import fr.coppernic.sdk.utils.core.CpcDefinitions;

/**
 * Created by Ananth on 12/28/2016.
 */

public class ServiceSetting extends AppCompatActivity implements View.OnClickListener {
    Button btnStartService, btnStopService, btnRestartService, btnTestCard;
    TextView tvDeviceWarning;
    TextView tvTestCardTitle, tvTestCardResult;
    private static String TAG = "ServiceSetting";
    Handler handler = new Handler();
    Timer timer = new Timer();
    Reader sReader;
    PowerMgmt mPowerMgmt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_setting);

        ActionBar ab = getSupportActionBar();
        ab.setHomeButtonEnabled(true);

        btnStartService = (Button) findViewById(R.id.btnStartService);
        btnStopService = (Button) findViewById(R.id.btnStopService);
        btnRestartService = (Button) findViewById(R.id.btnRestartService);
        btnTestCard = (Button) findViewById(R.id.btnTestCard);
        tvDeviceWarning = (TextView) findViewById(R.id.tvDeviceWarning);

        btnStartService.setOnClickListener(this);
        btnStopService.setOnClickListener(this);
        btnRestartService.setOnClickListener(this);
        btnTestCard.setOnClickListener(this);

        tvTestCardTitle = (TextView) findViewById(R.id.tvTestCardTitle);
        tvTestCardResult = (TextView) findViewById(R.id.tvTestCardResult);
        tvTestCardTitle.setVisibility(View.GONE);
        tvTestCardResult.setVisibility(View.GONE);

        String device_model = Build.MODEL;
        if (device_model.equals(URLCollections.READER_ENABLED_DEVICE_MODEL)) {
            btnRestartService.setVisibility(View.VISIBLE);
            btnStartService.setVisibility(View.VISIBLE);
            btnStartService.setVisibility(View.VISIBLE);
            btnTestCard.setVisibility(View.VISIBLE);
            tvDeviceWarning.setVisibility(View.GONE);

            mPowerMgmt = new PowerMgmt(this);/*, new PowerUtils.PowerUtilsNotifier() {
                @Override
                public void onPowerUp(CpcResult.RESULT result, int vid, int pid) {
                    Log.d(TAG, "VID : "+vid);
                    if (vid == CpcDefinitions.VID_FTDI_EXPANSION_PORT) {
                        System.out.println("Expansion port is power on");
                    } else {
                        System.out.println("Expansion port is power off/failed!");
                    }
                }

                @Override
                public void onPowerDown(CpcResult.RESULT result, int vid, int pid) {
                    System.out.println("Power is going down");
                }
            });*/

        } else {
            btnRestartService.setVisibility(View.GONE);
            btnStartService.setVisibility(View.GONE);
            btnStopService.setVisibility(View.GONE);
            btnTestCard.setVisibility(View.GONE);
            tvDeviceWarning.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
        handler.removeCallbacks(null);
    }

    @Override
    public void onClick(View v) {
        boolean isServiceRunning = ServiceHelper.getInstance().isMyServiceRunning(this, ScanCardService.class);
        Log.d(TAG, "Service Running: "+isServiceRunning);
        switch (v.getId()) {
            case R.id.btnStartService:

                Log.d(TAG, "btnStartService");
                if (!isServiceRunning) {
                    AlarmManager am_start = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
                    Intent intent_start = new Intent(this, ScanCardServiceBroadcastReceiver.class);
                    //intent_start.putExtra(ServiceHelper.KEY_SERVICE_ACTION, ServiceHelper.START_SERVICE);
                    PendingIntent pi_start = PendingIntent.getBroadcast(this, 0, intent_start, 0);
                    am_start.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), Config.SCAN_CARD_SERVICE_INTERVAL, pi_start);
                    // change status of start service button
                    btnStartService.setEnabled(false);
                    btnStartService.setClickable(false);
                    btnStartService.setText(getResources().getString(R.string.btn_starting_service));
                }

                break;
            case R.id.btnStopService:

                Log.d(TAG, "btnStopService");
                if (isServiceRunning) {
                    Intent intent_stop = new Intent(this, ScanCardServiceBroadcastReceiver.class);
                    //intent_stop.putExtra(ServiceHelper.KEY_SERVICE_ACTION, ServiceHelper.STOP_SERVICE);
                    PendingIntent pi_stop = PendingIntent.getBroadcast(this, 0, intent_stop, 0);
                    AlarmManager am_stop = (AlarmManager) this.getSystemService(ALARM_SERVICE);
                    //am_stop.set(AlarmManager.RTC, System.currentTimeMillis(), pi_stop);
                    am_stop.cancel(pi_stop);
                    pi_stop.cancel();
                    ScanCardServiceBroadcastReceiver.stopService(this);
                    // change status of stop service button
                    btnStopService.setEnabled(false);
                    btnStopService.setClickable(false);
                    btnStopService.setText(getResources().getString(R.string.btn_stopping_service));
                }

                break;
            case R.id.btnRestartService:
                Log.d(TAG, "btnRestartService");

                AlarmManager am_restart = (AlarmManager) this.getSystemService(ALARM_SERVICE);
                Intent intent_restart = new Intent(this, ScanCardServiceBroadcastReceiver.class);
                //intent_restart.putExtra(ServiceHelper.KEY_SERVICE_ACTION, ServiceHelper.START_SERVICE);
                PendingIntent pi_restart = PendingIntent.getBroadcast(this, 0, intent_restart, 0);
                if (isServiceRunning) {
                    intent_restart.putExtra(ServiceHelper.KEY_SERVICE_ACTION, ServiceHelper.STOP_SERVICE);
                    pi_restart = PendingIntent.getBroadcast(this, 0, intent_restart, 0);
                    am_restart.cancel(pi_restart);
                    pi_restart.cancel();
                    ScanCardServiceBroadcastReceiver.stopService(this);

                    // again start service
                    intent_restart = new Intent(this, ScanCardServiceBroadcastReceiver.class);
                    //intent_restart.putExtra(ServiceHelper.KEY_SERVICE_ACTION, ServiceHelper.START_SERVICE);
                    pi_restart = PendingIntent.getBroadcast(this, 0, intent_restart, 0);
                    am_restart = (AlarmManager) this.getSystemService(ALARM_SERVICE);
                }
                am_restart.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), Config.SCAN_CARD_SERVICE_INTERVAL, pi_restart);
                // control the restart and other button for service
                // for start button
                btnStartService.setEnabled(false);
                btnStartService.setClickable(false);
                // for stop button
                btnStopService.setEnabled(false);
                btnStopService.setClickable(false);
                // for restart button
                btnRestartService.setEnabled(false);
                btnRestartService.setClickable(false);
                btnRestartService.setText(getResources().getString(R.string.btn_restarting_service));
                break;

            case R.id.btnTestCard:
                Log.d(TAG, "btnTestCard");
                tvTestCardTitle.setVisibility(View.GONE);
                tvTestCardResult.setVisibility(View.GONE);

                Thread testScanThread = new Thread(testScanRunnable);
                testScanThread.start();

                break;

            default:
                break;
        }
    }

    private Runnable testScanRunnable = new Runnable() {
        @Override
        public void run() {

            Card card = new Card();
            Reader.getInstance(getApplicationContext(), new OnGetReaderInstanceListener() {
                @Override
                public void OnGetReaderInstance(Reader instance) {
                    sReader = instance;
                }
            });

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String device_value = prefs.getString(PreferencesManager.KEY_SET_DEVICE, "");
            PowerMgmt.InterfacesCone interfaceCone = PowerMgmt.InterfacesCone.ExpansionPort;
            if (device_value != null && device_value.length() != 0) {
                if (device_value.equals(URLCollections.DEVICE_CONE)) {
                    interfaceCone = PowerMgmt.InterfacesCone.ExpansionPort;
                } else {
                    interfaceCone = PowerMgmt.InterfacesCone.UsbGpioPort;
                }
            }

            mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.RfidSc, PowerMgmt.ManufacturersCone.Hid, PowerMgmt.ModelsCone.IClassProx, interfaceCone, true);

            try {
                Thread.sleep(700);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            final ErrorCodes er = sReader.open(CpcDefinitions.HID_ICLASS_PROX_READER_PORT, BaudRate.B9600);//(BaudRate) mSpBaudRate.getSelectedItem());

            // this code will be executed after 0.7 seconds
            ErrorCodes er_sus = sReader.samCommandSuspendAutonomousMode();
            if (er_sus == ErrorCodes.ER_OK) {
                System.out.println("myapplication"+"Init OK");
            }

            // actual process of scanning card
            final FrameProtocol[] fp = new FrameProtocol[2];
            fp[0] = FrameProtocol.PICO15693;
            fp[1] = FrameProtocol.ISO14443A;

            // To automatically scan the card all the time
            ErrorCodes er_scan = sReader.samCommandScanFieldForCard(fp, CascadeLevel.CASCADE_LEVEL_DISABLED, card);

            if (er == ErrorCodes.ER_OK) {
                final Card card1 = new Card();
                ErrorCodes erc = sReader.getCardNumber(card1);//sReader.samCommandScanAndProcessMedia(card1);//sReader.getCard_Number(card1);
                System.out.println("Card error: " + erc);

                runOnUiThread(new Thread(new Runnable() {
                    public void run() {
                        tvTestCardTitle.setVisibility(View.VISIBLE);
                        tvTestCardResult.setVisibility(View.VISIBLE);
                    }
                }));

                if (erc == ErrorCodes.ER_OK) {
                    if (card1.getCardNumber() != -1) {
                           /* if (mediaPlayer != null) {
                                System.out.println("Card mediaPlayer not null");
                                try {
                                    mediaPlayer = MediaPlayer.create(ScanCardService.this, R.raw.censored_beep_mastercard);//R.raw.a_tone_his_self);
                                    mediaPlayer.start();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }*/
                    }
                    System.out.println("Card Number: " + card1.getCardNumber());
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            tvTestCardResult.setText(getResources().getString(R.string.tv_card_result, ""+card1.getCardNumber()));
                        }
                    }));
                } else {
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            tvTestCardResult.setText(getResources().getString(R.string.tv_card_result, getResources().getString(R.string.tv_card_result_unknown)));
                        }
                    }));
                }
            }
            mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.RfidSc, PowerMgmt.ManufacturersCone.Hid, PowerMgmt.ModelsCone.IClassProx, URLCollections.getInterfaceConeType(), false);
            /*runOnUiThread(new Thread(new Runnable() {
                public void run() {
                    onClick(btnStartService);
                }
            }));*/
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            ServiceSetting.this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void callAsynchronousTask() {
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            Log.d(TAG, "Thread Running");
                            updateUI();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        if (timer != null)
            timer = new Timer();

        if (handler != null)
            handler = new Handler();

        timer.schedule(doAsynchronousTask, 0, 5000); //execute in every 50000 ms
    }

    public void updateUI() {
        Log.d(TAG, "Update UI");
        if (ServiceHelper.getInstance().isMyServiceRunning(this, ScanCardService.class)) {
            Log.d(TAG, "Service is running");
            // control start serivce button
            btnStartService.setEnabled(false);
            btnStartService.setClickable(false);
            btnStartService.setText(getResources().getString(R.string.btn_running_service));
            // control stop service button
            btnStopService.setEnabled(true);
            btnStopService.setClickable(true);
            btnStopService.setText(getResources().getString(R.string.btn_stop_service));
        }else {
            Log.d(TAG, "Service is not running");
            // control start serivce button
            btnStartService.setEnabled(true);
            btnStartService.setClickable(true);
            btnStartService.setText(getResources().getString(R.string.btn_start_service));
            // control stop service button
            btnStopService.setEnabled(false);
            btnStopService.setClickable(false);
            btnStopService.setText(getResources().getString(R.string.btn_stopped_service));
        }
        // control button restart service
        btnRestartService.setEnabled(true);
        btnRestartService.setClickable(true);
        btnRestartService.setText(getResources().getString(R.string.btn_restart_service));
        /*if (intent!=null) {
            String time = intent.getStringExtra("time");
            String serviceStatus = intent.getStringExtra("serviceStatus");
            Log.d(TAG, "serviceStatus: "+serviceStatus+" time "+time);
        }*/
    }
    @Override
    public void onResume() {
        super.onResume();
        String device_model = Build.MODEL;
        if (device_model.equals(URLCollections.READER_ENABLED_DEVICE_MODEL)) {
            callAsynchronousTask();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        String device_model = Build.MODEL;
        if (device_model.equals(URLCollections.READER_ENABLED_DEVICE_MODEL)) {
            timer.cancel();
            handler.removeCallbacks(null);
        }
    }
}
