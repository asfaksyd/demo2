package com.smartportable.demo2.authentication;

import android.os.Bundle;
import android.view.MenuItem;

import com.smartportable.demo2.R;
import com.smartportable.demo2.api.URLCollections;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class BarcodeAuth extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_auth);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            //setResult(SyncActivity.REQUEST_SYNC_EMPLOYEES);
            BarcodeAuth.this.finish();
            if(URLCollections.whichDirection(BarcodeAuth.this) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(URLCollections.whichDirection(BarcodeAuth.this) == URLCollections.DEV_DIR_RTL) {
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
        } else {
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }
}
