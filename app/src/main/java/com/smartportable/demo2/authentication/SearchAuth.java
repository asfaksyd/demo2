package com.smartportable.demo2.authentication;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.smartportable.demo2.R;
import com.smartportable.demo2.adapters.SearchStudentAdapter;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.helper.GlobalClass;
import com.smartportable.demo2.models.Student;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SearchAuth extends AppCompatActivity implements View.OnClickListener {
    EditText etSearchStudInfo;
    Button btnSearchRecord;
    RecyclerView rvSearchResultList;
    TextView tvStNoRecords;
    SearchStudentAdapter adapter;
    ArrayList<Student> stList = new ArrayList<Student>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_auth);

        etSearchStudInfo = (EditText) findViewById(R.id.etSearchStudInfo);
        btnSearchRecord = (Button) findViewById(R.id.btnSearchRecord);
        rvSearchResultList = (RecyclerView) findViewById(R.id.rvSearchResultList);
        tvStNoRecords = (TextView) findViewById(R.id.tvStNoRecords);

        btnSearchRecord.setOnClickListener(this);

        stList = GlobalClass.getInstance().populateStudentData();
        FragmentManager fm = getSupportFragmentManager();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvSearchResultList.setLayoutManager(mLayoutManager);
        rvSearchResultList.setItemAnimator(new DefaultItemAnimator());

        adapter = new SearchStudentAdapter(this, stList, fm);
        if (stList.size() > 0) {
            tvStNoRecords.setVisibility(View.GONE);
            rvSearchResultList.setVisibility(View.VISIBLE);
            rvSearchResultList.setAdapter(adapter);
        } else {
            tvStNoRecords.setVisibility(View.VISIBLE);
            rvSearchResultList.setVisibility(View.GONE);
        }

        // keypad search option
        etSearchStudInfo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (isValidated()) {
                        searchStudentRecord(etSearchStudInfo.getText().toString());
                    }
                    return true;
                }
                return false;
            }
        });
    }

    /*private void populateStudentData() {
        if (stList.size() != 0) {
            stList.clear();
        }
        Student st1 = new Student();
        st1.setStId(1001);
        st1.setStName("George Mevrik");
        st1.setStCardno("6A6B6C6D");
        st1.setStDeptName("Dept. Of IT");
        st1.setStPhoto(stArray[0]);

        Student st2 = new Student();
        st2.setStId(1002);
        st2.setStName("Peter Tondon");
        st2.setStCardno("5A5B5C5D");
        st2.setStDeptName("Dept. Of IT");
        st2.setStPhoto(stArray[1]);

        Student st3 = new Student();
        st3.setStId(1003);
        st3.setStName("Abdul Raheman");
        st3.setStCardno("4A4B4C4D");
        st3.setStDeptName("Dept. Of IT");
        st3.setStPhoto(stArray[2]);

        Student st4 = new Student();
        st4.setStId(1004);
        st4.setStName("Randa Restnick");
        st4.setStCardno("3A3B3C3D");
        st4.setStDeptName("Dept. Of IT");
        st4.setStPhoto(stArray[3]);

        Student st5 = new Student();
        st5.setStId(1005);
        st5.setStName("Julia Peter");
        st5.setStCardno("2A2B2C2D");
        st5.setStDeptName("Dept. Of IT");
        st5.setStPhoto(stArray[4]);

        stList.add(st1);
        stList.add(st2);
        stList.add(st3);
        stList.add(st4);
        stList.add(st5);
    }*/

    private void searchStudentRecord (String queryStr) {

        /*populateStudentData();

        ArrayList<Student> searchList = new ArrayList<Student>();
        *//*if (searchList.size() > 0) {
            searchList.clear();
        }*//*

        for (int i=0;i<stList.size();i++) {
            Student student = stList.get(i);
            if ((""+student.getStId()).toLowerCase().contains(queryStr.toLowerCase())) {
                searchList.add(student);
            } else if (student.getStName().toLowerCase().contains(queryStr.toLowerCase())) {
                searchList.add(student);
            } else if (student.getStCardno().toLowerCase().contains(queryStr.toLowerCase())) {
                searchList.add(student);
            } else if (student.getStDeptName().toLowerCase().contains(queryStr.toLowerCase())) {
                searchList.add(student);
            } else {
                //stList.add(student);
            }
        }*/

        ArrayList<Student> searchList = new ArrayList<Student>();
        searchList = GlobalClass.getInstance().searchStudentRecord(queryStr);
        adapter.updateList(searchList);
        if (searchList.size() > 0) {
            tvStNoRecords.setVisibility(View.GONE);
            rvSearchResultList.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();
        } else {
            tvStNoRecords.setVisibility(View.VISIBLE);
            rvSearchResultList.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnSearchRecord) {
            //Boast.makeText(this, "Search Click" + etSearchStudInfo.getText().toString(), Toast.LENGTH_SHORT).show();
            if (isValidated()) {
                searchStudentRecord(etSearchStudInfo.getText().toString());
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            //setResult(SyncActivity.REQUEST_SYNC_EMPLOYEES);
            SearchAuth.this.finish();
            if(URLCollections.whichDirection(SearchAuth.this) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(URLCollections.whichDirection(SearchAuth.this) == URLCollections.DEV_DIR_RTL) {
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
        } else {
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }

    private boolean isValidated () {
        if (etSearchStudInfo.getText().length() == 0) {
            etSearchStudInfo.setError(getResources().getString(R.string.st_search_field_not_emplty));
            return false;
        }
        return true;
    }
}
