package com.smartportable.demo2;

import android.app.Application;

import net.danlew.android.joda.JodaTimeAndroid;

import java.util.ArrayList;

import io.realm.Realm;

/**
 * Created by Ananth on 1/17/2017.
 */

public class MyApplication extends Application {
    public static MyApplication instance;
    public ArrayList<Integer> cetegoriesIdList = new ArrayList<Integer>();
    public static ArrayList<Integer> areaIdList = new ArrayList<Integer>();

    public static MyApplication getInstance() {
        if (instance == null) {
            instance = new MyApplication();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //JodaTimeAndroid.init(this);
        instance = this;
        // Initialize Realm (just once per application)
        Realm.init(this);
        JodaTimeAndroid.init(this);

    }

    public ArrayList<Integer> getCetegoriesIdList() {
        return cetegoriesIdList;
    }

    public void setCetegoriesIdList(ArrayList<Integer> cetegoriesIdList) {
        this.cetegoriesIdList = cetegoriesIdList;
    }

    public static ArrayList<Integer> getAreaIdList() {
        return areaIdList;
    }

    public static void setAreaIdList(ArrayList<Integer> areaIdList) {
        MyApplication.areaIdList = areaIdList;
    }

}
