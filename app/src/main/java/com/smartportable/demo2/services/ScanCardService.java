package com.smartportable.demo2.services;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import com.smartportable.demo2.R;
import com.smartportable.demo2.MemberAccessMainActivity;
import com.smartportable.demo2.NFCMemberAccessMainActivity;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.Config;
import com.smartportable.demo2.helper.HandlerHelper;
import com.smartportable.demo2.helper.MemberAccessLogs;
import com.smartportable.demo2.helper.PreferencesManager;
import com.smartportable.demo2.helper.PunchLogs;
import com.smartportable.demo2.helper.ServiceHelper;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import androidx.annotation.Nullable;
import fr.coppernic.cpcframework.cpcpowermgmt.cone.PowerMgmt;
import fr.coppernic.sdk.hid.iclassProx.BaudRate;
import fr.coppernic.sdk.hid.iclassProx.Card;
import fr.coppernic.sdk.hid.iclassProx.CascadeLevel;
import fr.coppernic.sdk.hid.iclassProx.ErrorCodes;
import fr.coppernic.sdk.hid.iclassProx.FrameProtocol;
import fr.coppernic.sdk.hid.iclassProx.OnGetReaderInstanceListener;
import fr.coppernic.sdk.hid.iclassProx.Reader;
import fr.coppernic.sdk.utils.core.CpcDefinitions;
import fr.coppernic.sdk.utils.helpers.CpcOs;

/**
 * Created by Ananth on 12/22/2016.
 */

public class ScanCardService extends Service {

    private PowerMgmt mPowerMgmt;
    Reader sReader;
    Card card = new Card();
    MediaPlayer mediaPlayer;

    public static final String TAG = "ScanCardService";
    //Handler handler;

    @Override
    public void onCreate() {
        super.onCreate();
        //System.out.println("Service got created");

        mPowerMgmt = new PowerMgmt(this);
        // to handle the ui udpate
        //handler = new Handler(Looper.getMainLooper(),new LoadEmployeesLocation(this));

        // Media player pobject
        mediaPlayer = MediaPlayer.create(this, R.raw.censored_beep_mastercard);//R.raw.a_tone_his_self);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Log.d(TAG, "on media player completion");
                // to start LED1 blue fir cone eid
                mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.Led, PowerMgmt.ManufacturersCone.BuiltIn, PowerMgmt.ModelsCone.Led1, PowerMgmt.InterfacesCone.ExpansionPort, false);
            }
        });

            Reader.getInstance(this, new OnGetReaderInstanceListener() {
            @Override
            public void OnGetReaderInstance(Reader instance) {
                sReader = instance;
            }
        });

        SharedPreferences mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String device_value = mSharedPrefs.getString(PreferencesManager.KEY_SET_DEVICE, "");
        PowerMgmt.InterfacesCone interfaceCone = PowerMgmt.InterfacesCone.ExpansionPort;;
        if (device_value != null && device_value.length() != 0) {
            if (device_value.equals(URLCollections.DEVICE_CONE)) {
                interfaceCone = PowerMgmt.InterfacesCone.ExpansionPort;
            } else {
                interfaceCone = PowerMgmt.InterfacesCone.UsbGpioPort;
            }
        }

        mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.RfidSc, PowerMgmt.ManufacturersCone.Hid, PowerMgmt.ModelsCone.IClassProx, interfaceCone, true);

        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        final ErrorCodes er = sReader.open(CpcDefinitions.HID_ICLASS_PROX_READER_PORT, BaudRate.B9600);//(BaudRate) mSpBaudRate.getSelectedItem());

        // this code will be executed after 0.7 seconds
        ErrorCodes er_sus = sReader.samCommandSuspendAutonomousMode();
        if (er_sus == ErrorCodes.ER_OK) {
            System.out.println("myapplication"+"Init OK");
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //System.out.println("onDestory: called");
        sReader.close();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String device_value = prefs.getString(PreferencesManager.KEY_SET_DEVICE, "");
        PowerMgmt.InterfacesCone interfaceCone = PowerMgmt.InterfacesCone.ExpansionPort;
        if (device_value != null && device_value.length() != 0) {
            if (device_value.equals(URLCollections.DEVICE_CONE)) {
                interfaceCone = PowerMgmt.InterfacesCone.ExpansionPort;
            } else {
                interfaceCone = PowerMgmt.InterfacesCone.UsbGpioPort;
            }
        }
        mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.RfidSc, PowerMgmt.ManufacturersCone.Hid, PowerMgmt.ModelsCone.IClassProx, interfaceCone, false);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Boast.makeText(this, "ServiceClass.onStart()", Toast.LENGTH_SHORT).show();
        //System.out.println("Service got started");

        boolean app_in_bg = false;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        try {
            app_in_bg = prefs.getBoolean(PreferencesManager.KEY_CARD_SERVICE, false);
            //System.out.println("Service app_in_bg: "+app_in_bg);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mPowerMgmt==null) {
            mPowerMgmt = new PowerMgmt(this);
        }

        //SharedPreferences mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String device_value = prefs.getString(PreferencesManager.KEY_SET_DEVICE, "");
        // get details of from where this service got started
        String service_started_from = prefs.getString(PreferencesManager.KEY_SERVICE_STARTED_FROM, "");

        PowerMgmt.InterfacesCone interfaceCone = PowerMgmt.InterfacesCone.ExpansionPort;
        if (device_value != null && device_value.length() != 0) {
            if (device_value.equals(URLCollections.DEVICE_CONE)) {
                interfaceCone = PowerMgmt.InterfacesCone.ExpansionPort;
            } else {
                interfaceCone = PowerMgmt.InterfacesCone.UsbGpioPort;
            }
        }

        mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.RfidSc, PowerMgmt.ManufacturersCone.Hid, PowerMgmt.ModelsCone.IClassProx, interfaceCone, true);
        //if (!mPowerMgmt.getPower(PowerMgmt.PeripheralTypesCone.RfidSc, PowerMgmt.ManufacturersCone.Hid, PowerMgmt.ModelsCone.IClassProx, PowerMgmt.InterfacesCone.ExpansionPort)) {
            //mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.RfidSc, PowerMgmt.ManufacturersCone.Hid, PowerMgmt.ModelsCone.IClassProx, URLCollections.getInterfaceConeType(), true);

            try {
                Thread.sleep(700);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            final ErrorCodes er = sReader.open(CpcDefinitions.HID_ICLASS_PROX_READER_PORT, BaudRate.B9600);//(BaudRate) mSpBaudRate.getSelectedItem());

            // this code will be executed after 0.7 seconds
            ErrorCodes er_sus = sReader.samCommandSuspendAutonomousMode();
            if (er_sus == ErrorCodes.ER_OK) {
                System.out.println("myapplication"+"Init OK");
            }

        //}

        if (app_in_bg) {

            final FrameProtocol[] fp = new FrameProtocol[2];
            fp[0] = FrameProtocol.PICO15693;
            fp[1] = FrameProtocol.ISO14443A;

            //do {
                // To automatically scan the card all the time
                ErrorCodes er_scan = sReader.samCommandScanFieldForCard(fp, CascadeLevel.CASCADE_LEVEL_DISABLED, card);

                if (er_scan == ErrorCodes.ER_OK) {
                    Card card1 = new Card();
                    ErrorCodes erc = sReader.getCardNumber(card1);//sReader.samCommandScanAndProcessMedia(card1);//sReader.getCard_Number(card1);
                    System.out.println("Card error: " + erc);
                    if (erc == ErrorCodes.ER_OK) {
                        if (card1.getCardNumber() != -1) {
                            if (mediaPlayer != null) {
                                System.out.println("Card mediaPlayer not null");
                                try {
                                    mediaPlayer = MediaPlayer.create(ScanCardService.this, R.raw.censored_beep_mastercard);//R.raw.a_tone_his_self);
                                    mediaPlayer.start();

                                    // to ON LED1 blue fir cone eid
                                    mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.Led, PowerMgmt.ManufacturersCone.BuiltIn, PowerMgmt.ModelsCone.Led1, PowerMgmt.InterfacesCone.ExpansionPort, true);

                                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                        @Override
                                        public void onCompletion(MediaPlayer mediaPlayer) {
                                            Log.d(TAG, "on media player completion");
                                            // to start LED1 blue fir cone eid
                                            mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.Led, PowerMgmt.ManufacturersCone.BuiltIn, PowerMgmt.ModelsCone.Led1, PowerMgmt.InterfacesCone.ExpansionPort, false);
                                        }
                                    });

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            // to store punch entry in database for Mustering
                            String card_number = ""+card1.getCardNumber();
                            /*if (card_number.equals("2096"))
                                card_number = "41054";*/

                            if (service_started_from != null) {
                                if (service_started_from.length() > 0) {
                                    Log.d("service_started_from: ", service_started_from);
                                    if (service_started_from.equals(ServiceHelper.FROM_MUSTERING)) {
                                        doPuncEntriesMustering(this, "" + card_number);//card1.getCardNumber());
                                    } else if (service_started_from.equals(ServiceHelper.FROM_MEM_ACC)) {
                                        if (isForeground(this.getPackageName(), "activity_mem_acc")) {
                                            doPunchEntriesMemAcc(this, "" + card_number);//card1.getCardNumber());
                                        } else {
                                            doPuncEntriesMustering(this, "" + card_number);//card1.getCardNumber());
                                        }
                                    }
                                }
                            }
                        }
                        System.out.println("Card Number: " + card1.getCardNumber());
                    }
                }
            //} while (card == null);
        } else {
            boolean isAppInForeGround = isForeground(this.getPackageName(), "application");
            Log.d(TAG, "App is in Forground: "+isAppInForeGround);
            if (isAppInForeGround) {
                final FrameProtocol[] fp = new FrameProtocol[2];
                fp[0] = FrameProtocol.PICO15693;
                fp[1] = FrameProtocol.ISO14443A;

                //do {
                    // To automatically scan the card all the time
                    ErrorCodes er_scan = sReader.samCommandScanFieldForCard(fp, CascadeLevel.CASCADE_LEVEL_DISABLED, card);
                    System.out.println("App is in Foreground scan: "+er);
                    if (er_scan == ErrorCodes.ER_OK) {
                        Card card1 = new Card();
                        ErrorCodes erc = sReader.getCardNumber(card1);//sReader.samCommandScanAndProcessMedia(card1);//sReader.getCard_Number(card1);
                        System.out.println("Card error: " + erc);
                        if (erc == ErrorCodes.ER_OK) {
                            if (card1.getCardNumber() != -1) {
                                if (mediaPlayer != null) {
                                    System.out.println("Card mediaPlayer not null");
                                    try {
                                        mediaPlayer = MediaPlayer.create(ScanCardService.this, R.raw.censored_beep_mastercard);//R.raw.a_tone_his_self);
                                        mediaPlayer.start();

                                        // to ON LED1 blue fir cone eid
                                        mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.Led, PowerMgmt.ManufacturersCone.BuiltIn, PowerMgmt.ModelsCone.Led1, PowerMgmt.InterfacesCone.ExpansionPort, true);

                                        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                            @Override
                                            public void onCompletion(MediaPlayer mediaPlayer) {
                                                Log.d(TAG, "on media player completion");
                                                // to start LED1 blue fir cone eid
                                                mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.Led, PowerMgmt.ManufacturersCone.BuiltIn, PowerMgmt.ModelsCone.Led1, PowerMgmt.InterfacesCone.ExpansionPort, false);
                                            }
                                        });

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                // tp store pucnhes entry in database
                                //doPuncEntries(this, ""+card1.getCardNumber());

                                String card_number = ""+card1.getCardNumber();
                                /*if (card_number.equals("2096"))
                                    card_number = "41054";*/

                                // to store punch entry in database for Mustering
                                if (service_started_from != null) {
                                    if (service_started_from.length() > 0) {
                                        Log.d("service_started_from: ", service_started_from);
                                        if (service_started_from.equals(ServiceHelper.FROM_MUSTERING)) {
                                            doPuncEntriesMustering(this, "" + card_number);//card1.getCardNumber());
                                        } else if (service_started_from.equals(ServiceHelper.FROM_MEM_ACC)) {
                                            if (isForeground(this.getPackageName(), "activity_mem_acc")) {
                                                doPunchEntriesMemAcc(this, "" + card_number);//card1.get
                                                // CardNumber());
                                            } else {
                                                doPuncEntriesMustering(this, "" + card_number);//card1.getCardNumber());
                                            }
                                        }
                                    }
                                }
                            }
                            System.out.println("Card Number: " + card1.getCardNumber());
                        }
                    }
                //} while (card == null);
            }
        }

        return super.onStartCommand(intent, flags, startId);
    }
    // to check whether app is running in for-gound or not using "application"
    // And to check whether specific Activity is visible or not "activity'

    public boolean isForeground(String myPackage, String check_type) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;

        // to check user want to check application only in forground
        if (check_type.equals("application")) {
            return componentInfo.getPackageName().equals(myPackage);
        } else if (check_type.equals("activity")) {
            return false;//componentInfo.getPackageName().getClass().getCanonicalName().equals(NFCMainActivity.class.getCanonicalName());
        } else if (check_type.equals("activity_mem_acc")) {
            if (CpcOs.isCone()) {
                boolean st = componentInfo.getClassName().equals(MemberAccessMainActivity.class.getCanonicalName());
                //Log.d("service", " Class Name: "+componentInfo.getClassName()+ " Status "+st);
                return st;
            } else if (CpcOs.isC5()) {
                return componentInfo.getClassName().equals(NFCMemberAccessMainActivity.class.getCanonicalName());
            } else {
                return componentInfo.getClassName().equals(MemberAccessMainActivity.class.getCanonicalName());
            }

        } else {
            return false;
        }

    }
    // to store and update punch logs
    public void doPuncEntriesMustering (Context mContext, String cardNumber) {
        DatabaseHandler db = new DatabaseHandler(mContext);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        int sln_punch = 0;
        // content values for PunchLogs entries
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => "+c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Config.DATETIME_FORMAT_LOCAL);
        String punchDateTime = df.format(c.getTime());
        Log.d(TAG, ""+punchDateTime);
        int readerId = prefs.getInt(PreferencesManager.KEY_SET_READER_ID, -1);
        String readerName = prefs.getString(PreferencesManager.KEY_SET_READER_NAME, "");
        int musterId = prefs.getInt(PreferencesManager.KEY_SET_MUSTER_ID, -1);
        int empId = db.getEmployeeIdFromCardNumber(cardNumber);
        int sln_reader = 0;
        PunchLogs pl = new PunchLogs();
        pl.setSLN_Punch(sln_punch);
        /*if (empId != -1 && empId > -1) {
            pl.setSLN_Employee(empId);
        } else {
            pl.setSLN_Employee(empId);
        }*/

        pl.setCard_Number(cardNumber);
        pl.setSLN_Reader(sln_reader);
        pl.setReader_Name(readerName);
        pl.setPunch_Date(punchDateTime);
        //pl.setGrant_Deny(false);
        pl.setIsPunchSynced(0);
        pl.setSLN_Location(-1);
        pl.setSLN_Area_ID(-1);
        if (readerId != -1 && readerId >= 0) {
            pl.setSLN_Reader(readerId);
        } else {
            pl.setSLN_Reader(-1);
        }

        if (readerId >= 0) {
            pl.setMusteringId(musterId);
        }else {
            pl.setMusteringId(-1);
        }

        db.updatePunchLogsData(pl);

        //EventBus.getDefault().post(new LoadEmployeesLocation.MessageEvent(HandlerHelper.UPDATE_EMPLOYEE_LOCATION));
        //EventBus.getDefault().post(new MainActivity.MessageEvent(HandlerHelper.UPDATE_EMPLOYEE_LOCATION));
        //EventBus.getDefault().post(new NFCMainActivity.MessageEvent(HandlerHelper.UPDATE_EMPLOYEE_LOCATION));
    }

    public void doPunchEntriesMemAcc(Context mContext, String cardNumber) {
        DatabaseHandler db = new DatabaseHandler(mContext);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        // content values for PunchLogs entries
        TimeZone.setDefault(null);
        System.setProperty("user.timezone", "");
        TimeZone.setDefault(null);
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        //System.out.println("Current time => "+c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Config.DATETIME_FORMAT_LOCAL);
        String punchDateTime = df.format(c.getTime());
        Log.d(TAG, "MemAcc "+punchDateTime + " Timezone : "+c.getTimeZone());
        int readerId = prefs.getInt(PreferencesManager.KEY_SET_READER_ID, -1);
        int sessionId = prefs.getInt(PreferencesManager.KEY_SET_SESSION_ID, -1);
        long sessionDate = prefs.getLong(PreferencesManager.KEY_SET_SESSION_DATE, -1);
        long sessionStartTime = prefs.getLong(PreferencesManager.KEY_SET_SESSION_START_TIME, -1);
        long sessionEndTime = prefs.getLong(PreferencesManager.KEY_SET_SESSION_END_TIME, -1);
        String readerName = prefs.getString(PreferencesManager.KEY_SET_READER_NAME, "");
        int musterId = prefs.getInt(PreferencesManager.KEY_SET_MUSTER_ID, -1);

        int empId = db.getEmployeeIdFromCardNumber(cardNumber);

        // to get sesstion start //yyyy-MM-dd HH:mm:ss
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd", Config.DATETIME_FORMAT_LOCAL);
        String session_date = sdfDate.format(sessionDate);

        SimpleDateFormat sdfStartTime = new SimpleDateFormat("HH:mm:ss", Config.DATETIME_FORMAT_LOCAL);
        sdfStartTime.setTimeZone(TimeZone.getTimeZone("UTC"));
        String session_start_time = sdfStartTime.format(sessionStartTime);
        String session_end_time = sdfStartTime.format(sessionEndTime);

        int fromDateDiff = punchDateTime.compareTo(session_date+" "+session_start_time);
        int toDateDiff = punchDateTime.compareTo(session_date+" "+session_end_time);
        Log.d("Date Diff", "startDate "+session_date+" "+session_start_time+" DIFF "+fromDateDiff+" EndDate "+session_date+" "+session_end_time+" DIFF " + toDateDiff);
        // check whether the flash card is allow for these session time interval or not
        if (fromDateDiff >= 0 && toDateDiff <= 0) {
            fr.coppernic.sdk.utils.debug.Log.d(TAG, "Card punch time is correct");

            int access_status = db.checkCardHavingDoorAccess(readerId, sessionId, cardNumber, session_date+" "+session_start_time, session_date+" "+session_end_time);
            // 0 - Can access / Allowed
            // 1 - Already accessed / Not allowed
            // 2 -
            if (access_status > -1) {
                //Boast.makeText(this, "Having door access", Toast.LENGTH_SHORT).show();

                MemberAccessLogs memacc = new MemberAccessLogs();

                if (empId != -1 && empId > -1) {
                    memacc.setMemAccLogsEmpId(empId);
                } else {
                    memacc.setMemAccLogsEmpId(empId);
                }

                if (cardNumber != null) {
                    if (cardNumber.length() != 0)
                        memacc.setMemAccLogsCardNo(cardNumber);
                }
                if (readerId != -1)
                    memacc.setMemAccLogsReaderId(readerId);
                if (sessionId != -1)
                    memacc.setMemAccLogsSessionId(sessionId);

                    memacc.setMemAccLogsPunchDate(punchDateTime);
                    memacc.setMemAccLogsIsSync(0);


                memacc.setMemAccLogsAccessGrant(access_status);
                if (musterId >= 0) {
                    //memacc.setMemAccLogsMusteringId(musterId);
                } else {
                    //memacc.setMemAccLogsMusteringId(-1);
                }

                if (access_status == 0) {
                    Log.d(TAG, "First entry");
                    //showAccessGrantedDialog(memacc);
                    EventBus.getDefault().post(new MemberAccessMainActivity.MessageEvent(memacc));
                } else if (access_status == 2) {
                    Log.d(TAG, "Duplicate Entries");
                    //showAccessGrantedDialog(memacc);
                    EventBus.getDefault().post(new MemberAccessMainActivity.MessageEvent(memacc));
                }

                //db.saveMemAccPunchLogsData(memacc);

            } else {
                if (access_status == -1) {
                    access_status = 1;
                }
                //Boast.makeText(this, "Not having door access!!!", Toast.LENGTH_SHORT).show();
                MemberAccessLogs memacc = new MemberAccessLogs();

                if (empId != -1 && empId > -1) {
                    memacc.setMemAccLogsEmpId(empId);
                } else {
                    memacc.setMemAccLogsEmpId(empId);
                }

                if (cardNumber != null) {
                    if (cardNumber.length() != 0)
                        memacc.setMemAccLogsCardNo(cardNumber);
                }
                if (readerId != -1)
                    memacc.setMemAccLogsReaderId(readerId);
                if (sessionId != -1)
                    memacc.setMemAccLogsSessionId(sessionId);
                memacc.setMemAccLogsPunchDate(punchDateTime);
                memacc.setMemAccLogsIsSync(0);

                /*if (access_status == 0) {
                    memacc.setMemAccLogsAccessGrant(0);
                } else {
                    memacc.setMemAccLogsAccessGrant(1);
                }*/
                memacc.setMemAccLogsAccessGrant(access_status);
                //memacc.setMemAccLogsAreaId(-1);
                if (musterId >= 0) {
                    //memacc.setMemAccLogsMusteringId(musterId);
                } else {
                    //memacc.setMemAccLogsMusteringId(-1);
                }

                Log.d(TAG, "No Door Access Entries");
                //showAccessGrantedDialog(memacc);
                //EventBus.getDefault().post(new MemberAccessMainActivity.MessageEvent(this, HandlerHelper.MEM_ACC_NO_DOOR_ACCESS));
                EventBus.getDefault().post(new MemberAccessMainActivity.MessageEvent(memacc));
                //db.saveMemAccPunchLogsData(memacc);
                //refreshFragment();
            }
        } else {
            //Boast.makeText(this, "Session is not valid to flash card", Toast.LENGTH_SHORT).show();
            EventBus.getDefault().post(new MemberAccessMainActivity.MessageEvent(this, HandlerHelper.MEM_ACC_INVALID_SESSION));
            /*MemberAccessLogs memacc = new MemberAccessLogs();

            if (empId != -1 && empId > -1) {
                memacc.setMemAccLogsEmpId(empId);
            }
            if (cardNumber != null) {
                if (cardNumber.length() != 0)
                    memacc.setMemAccLogsCardNo(cardNumber);
            }
            if (readerId != -1)
                memacc.setMemAccLogsReaderId(readerId);
            if (sessionId != -1)
                memacc.setMemAccLogsSessionId(sessionId);
            memacc.setMemAccLogsPunchDate(punchDateTime);
            memacc.setMemAccLogsIsSync(0);

            memacc.setMemAccLogsAccessGrant(1); // not having door accessed for this time interval
            //memacc.setMemAccLogsAreaId(-1);
            if (musterId >= 0) {
                //memacc.setMemAccLogsMusteringId(musterId);
            } else {
                //memacc.setMemAccLogsMusteringId(-1);
            }

            db.saveMemAccPunchLogsData(memacc);

            refreshFragment();*/
        }
    }

}
