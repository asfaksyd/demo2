package com.smartportable.demo2.services

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.fineapp.android.retrofitClient.RetrofitClientSingleton
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection
import com.smartportable.demo2.db.DatabaseHandler
import com.smartportable.demo2.models.MainGateAccLogs
import com.smartportable.demo2.models.MainGateAccLogsModel
import com.smartportable.demo2.models.VisitorInfoModel
import com.smartportable.demo2.utils.*
import retrofit2.Call
import retrofit2.Response


public class SendMainGateWorker(mContext: Context, params: WorkerParameters) : Worker(mContext, params) {
    lateinit var mCtx : Context
    lateinit var db: DatabaseHandler

    init {
        mCtx = mContext
    }

    override fun doWork(): Result {
        Log.d(TAG, "SendPunchingLogs")
        db = DatabaseHandler(mCtx)
        var list : ArrayList<MainGateAccLogs.Data> = db.getAllMainGatePunchLogsData(0)
        sendPendingLogs(list)
        return Result.success()
    }

    fun sendPendingLogs(list : ArrayList<MainGateAccLogs.Data>) {
        val jObj : JsonObject = JsonObject()

        jObj.addProperty("Token", SharedPref.getAuthToken(mCtx))
        var jData : JsonObject = JsonObject()
        var jArray : JsonArray = JsonArray()
        for (i: Int in 0 until list.size) {
            val accLogs = list.get(i)
            val jObjAccLogs: JsonObject = JsonObject()

            jObjAccLogs.addProperty("Punch_Datetime", accLogs.punchDateTime)
            jObjAccLogs.addProperty("Access_Type", accLogs.punchType)
            jObjAccLogs.addProperty("AG_Id", accLogs.gateId)
            jObjAccLogs.addProperty("AG_Name", accLogs.gateName)
            jObjAccLogs.addProperty("Card_Number", accLogs.cardNumber)
            jObjAccLogs.addProperty("Student_Id", accLogs.studentId)
            jObjAccLogs.addProperty("MealNumber", accLogs.studentMealNumber)
            jObjAccLogs.addProperty("Punch_Note", accLogs.punchNote)
            jObjAccLogs.addProperty("Is_Sync", accLogs.isSync)
            jArray.add(jObjAccLogs)
        }
        jData.add("AccLog", jArray)
        jObj.add("data", jData)
        System.out.println("JObj: "+jObj.toString())

        var baseUrl = SharedPref.getStringValue(mCtx, keyAPIBaseUrl, "")


        var abc = RetrofitClientSingleton.getInstance(baseUrl)
        var apiRetrofit : Call<MainGateAccLogsModel> = abc.setMainGateAccLogs(jObj)


        apiRetrofit!!.enqueue(object : retrofit2.Callback<MainGateAccLogsModel>
        {

            override fun onFailure(call: Call<MainGateAccLogsModel>?, t: Throwable?)
            {

            }

            override fun onResponse(call: Call<MainGateAccLogsModel>?, response: Response<MainGateAccLogsModel>?) {
                //stopProgress()
                Log.e("SendMainGateAccLogs", "SendMainGateLogs>>response>>>> "+response)
                if (response!!.isSuccessful) {
                    val mainGateLogs: MainGateAccLogsModel? = response!!.body()

                    if (mainGateLogs!!.status == 1) {
                        //System.out.println("Sync Reader : "+readerInfo.status.toString())
                        //System.out.println("Sync Reader : "+readerInfo!!.data?.get(0)?.readerName)
                        /*if (visitorInfo!!.pageCount!!.contains("/")) {
                            var pageCountArr  = visitorInfo!!.pageCount!!.split("/")
                            totalPageCount = Integer.parseInt(pageCountArr[1])
                        }*/
                        val data: ArrayList<MainGateAccLogsModel.Data>? = mainGateLogs!!.data
                        if (data != null) {

                            db.updateMainGateAccLogs(data)

                        }
                    } else if (mainGateLogs.status == 2) {

                    } else if (mainGateLogs.status == 3) {
                        forceLogout(mCtx)

                    } else {
                        Log.d(TAG, mainGateLogs!!.message)
                    }
                }
            }

        })
    }
}