package com.smartportable.demo2.services;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.fineapp.android.retrofitClient.RetrofitClientSingleton;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.smartportable.demo2.api.ApiClient;
import com.smartportable.demo2.api.ApiInterface;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.GlobalClass;
import com.smartportable.demo2.helper.MemberAccessLogs;
import com.smartportable.demo2.helper.PunchLogs;
import com.smartportable.demo2.models.MemAccLogsInfoModel;
import com.smartportable.demo2.utils.KeysKt;
import com.smartportable.demo2.utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.Nullable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ananth on 1/19/2017.
 */

public class SendPunchDataService extends Service {
    public static final String TAG = "SendPunchDataService";
    ApiInterface apiSendPunchService;

    @Override
    public void onCreate() {
        super.onCreate();


    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Punch Send Service");
        final DatabaseHandler db = new DatabaseHandler(this);
        boolean haveMusteringEntries = db.checkNotSyncedPunchEntries();
        boolean haveMemAccEntries = db.checkMemAccLogs();
        JsonObject jMainObj = new JsonObject();
        /*if (haveMusteringEntries && GlobalClass.getInstance().isNetworkAvailable(this)) {
            ArrayList<PunchLogs> list = db.getNotSyncPunchEntries();
            if (list.size() > 0) {
                try {

                    JSONObject jsonObject = new JSONObject();
                    JSONArray jArray = new JSONArray();
                    jMainObj.put("Token", SharedPref.INSTANCE.getAuthToken(this)));

                    for (int i = 0; i < list.size(); i++) {
                        *//*Calendar c = Calendar.getInstance();
                        //System.out.println("Current time => "+c.getTime());
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String punchDateTime = df.format(c.getTime());
                        Log.d(TAG, "Service Punch Date"+punchDateTime);*//*
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                        //int readerId = prefs.getInt(PreferencesManager.KEY_SET_READER_ID, -1);
                        //String readerName = prefs.getString(PreferencesManager.KEY_SET_READER_NAME, "");
                        PunchLogs pl = list.get(i);
                        Log.d(TAG, "Card Number: " + list.get(i).getCard_Number());
                        JSONObject punchObj = new JSONObject();
                        punchObj.put("SLN_Punch", "" + pl.getSLN_Punch());
                        punchObj.put("Student_Id", "" + pl.getSLN_Employee());
                        punchObj.put("Card_Number", pl.getCard_Number());
                        punchObj.put("Reader_Id", "" + pl.getSLN_Reader());
                        punchObj.put("Reader_Name", pl.getReader_Name());
                        punchObj.put("Session_ID", pl.getSessionId());
                        punchObj.put("Session_Name", pl.getSessionName());
                        punchObj.put("Punch_Datetime", pl.getPunch_Date());
                        punchObj.put("Grant_Access", pl.getIsPunchSynced() == 0 ? false : true);
                        punchObj.put("MealNumber", "");
                        punchObj.put("Unique_No", "");
                        punchObj.put("Sync", 0);
                        punchObj.put("Access_Code", pl.getAccessCode());
                        *//* punchObj.put("SLN_Location", "" + pl.getSLN_Location());
                        punchObj.put("SLN_Area_ID", "" + pl.getSLN_Area_ID());
                        punchObj.put("MusteringId", "" + pl.getMusteringId());
                        punchObj.put("Session_Code", 0);*//*

                        jArray.put(punchObj);
                    }
                        jsonObject.put("MemAccLog", jArray);
                        jMainObj.put("data", jsonObject);
                        Log.d(TAG, "PunchArray: Mustering: "+jMainObj.toString());
                        *//*sendPunchDataAPiCall(jArray.toString());*//*
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            Log.d(TAG, "No Mustering Entries Found");
        }*/
            // to check for pending entries into the member access logs table and get the list
        if (haveMemAccEntries && GlobalClass.getInstance().isNetworkAvailable(this)) {
            ArrayList<PunchLogs> list = db.getNotSyncPunchEntries();
            ArrayList<MemberAccessLogs> memacc_list = db.isOldNotSyncedMemAccLogsExist();
            if (memacc_list.size() > 0) {

                JsonObject jsonObject = new JsonObject();
                JsonArray jArray = new JsonArray();
                jMainObj.addProperty("Token", SharedPref.INSTANCE.getAuthToken(this));
                for (int i = 0; i < memacc_list.size(); i++) {
                    /*Calendar c = Calendar.getInstance();
                    //System.out.println("Current time => "+c.getTime());
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String punchDateTime = df.format(c.getTime());
                    Log.d(TAG, "Service Punch Date"+punchDateTime);*/
                    //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                    //int readerId = prefs.getInt(PreferencesManager.KEY_SET_READER_ID, -1);
                    //String readerName = prefs.getString(PreferencesManager.KEY_SET_READER_NAME, "");
                    PunchLogs pl = list.get(i);
                    Log.d(TAG, "Card Number: " + list.get(i).getCard_Number());
                    JsonObject punchObj = new JsonObject();
                    //punchObj.addProperty("SLN_Punch", "" + pl.getSLN_Punch());
                    punchObj.addProperty("Punch_Datetime", pl.getPunch_Date());
                    punchObj.addProperty("Access_Code", pl.getAccessCode());
                    punchObj.addProperty("Grant_Access", pl.getIsPunchSynced() == 0 ? true : false);
                    punchObj.addProperty("Reader_Id", "" + pl.getSLN_Reader());
                    punchObj.addProperty("Reader_Name", pl.getReader_Name());
                    punchObj.addProperty("Card_Number", pl.getCard_Number());
                    punchObj.addProperty("Session_ID", pl.getSessionId());
                    punchObj.addProperty("Session_Name", pl.getSessionName());
                    punchObj.addProperty("Student_Id", "" + pl.getSLN_Employee());
                    punchObj.addProperty("MealNumber", "");
                    punchObj.addProperty("Unique_No", "");
                    punchObj.addProperty("Is_Sync", 0);

                    /* punchObj.put("SLN_Location", "" + pl.getSLN_Location());
                    punchObj.put("SLN_Area_ID", "" + pl.getSLN_Area_ID());
                    punchObj.put("MusteringId", "" + pl.getMusteringId());
                    punchObj.put("Session_Code", 0);*/

                    jArray.add(punchObj);
                }
                jsonObject.add("MemAccLog", jArray);
                jMainObj.add("data", jsonObject);
                Log.d(TAG, "PunchArray: Mustering: "+jMainObj.toString());
                sendPunchDataAPiCall(jMainObj);
            }
        } else {
            Log.d(TAG, "No Member Access Entries Found");
        }

        if (jMainObj.has("MemAccLog")) {
            sendPunchDataAPiCall(jMainObj);
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    // to check whether app is running in foregound or not
    public boolean isForeground(String myPackage) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        return componentInfo.getPackageName().equals(myPackage);
    }

    public void sendPunchDataAPiCall (final JsonObject jsonPunchData) {

        String baseUrl = SharedPref.INSTANCE.getStringValue(this, KeysKt.keyAPIBaseUrl, "");

        Call<MemAccLogsInfoModel> call = RetrofitClientSingleton.Companion.getInstance(baseUrl).setMemAccLog(jsonPunchData);

            call.enqueue(new Callback<MemAccLogsInfoModel>() {
                @Override
                public void onResponse(Call<MemAccLogsInfoModel> call, Response<MemAccLogsInfoModel> response) {
                    if (response.isSuccessful()) {
                        MemAccLogsInfoModel memacclogInfo = response.body();

                        if (memacclogInfo.getStatus() == 1) {
                            System.out.println("Message"+memacclogInfo.getMessage());
                            ArrayList<MemAccLogsInfoModel.DataBean> memacclogInfoData = memacclogInfo.getData();
                            if (memacclogInfoData != null) {
                                if (memacclogInfo.getData().size() > 0) {
                                    updateMemAccLogsEntries(memacclogInfo.getData());
                                }
                            }


                        }
                    }
                }

                @Override
                public void onFailure(Call<MemAccLogsInfoModel> call, Throwable t) {

                }
            });



        /*String url = URLCollections.getBaseURLFromPreference(this);
        if (url == null || url.equals("") || !URLCollections.isValidUrl(url)) {
            Log.d(TAG, "Connection Url is not set");
        } else if (GlobalClass.getInstance().isNetworkAvailable(this)) {
            apiSendPunchService = new ApiClient(this).getClient().create(ApiInterface.class);
            Call<ResponseBody> call_reader = apiSendPunchService.sendPunchData(jsonPunchData);
            ; // (0, 0);
            *//*if (punch_status) {
                Log.d(TAG, "SendPunchData True: "+punch_status);
            } else {
                Log.d(TAG, "SendPunchData False: "+punch_status);
            }*//*

            //onEmployeesLocationInDB(jsonPunchData);

            call_reader.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    int statusCode = response.code();
                    Log.d(TAG, "response code" + statusCode);
                    if (response.isSuccessful()) {
                        Log.d(TAG, "Card Number: " + "Response" +response + "Response Body : " + response.body() + "Body String: "+
                                response.body().toString() + " Header: " + response.headers() + " raw: " + response.raw());
                            //onEmployeesLocationAPICall();
                        onEmployeesLocationInDB(jsonPunchData);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    // Log error here since request failed
                    Log.d(TAG, "Error " + t);
                }
            });
        } else {
            Log.d(TAG, "No internet");
        }*/
    }

    // update mem acc logs table
    public void updateMemAccLogsEntries (ArrayList<MemAccLogsInfoModel.DataBean> list) {
        DatabaseHandler db = new DatabaseHandler(this);
        db.updateMemAccLogs(list);

    }

    // To update punches data
    public void onEmployeesLocationInDB(String jsonArray) {
        final DatabaseHandler db = new DatabaseHandler(this);
        try {
            JSONArray jArray = new JSONArray(jsonArray);
            for (int i=0; i<jArray.length(); i++) {
                JSONObject jObj = jArray.getJSONObject(i);
                int sessionId = jObj.getInt("Session_Code");
                if (sessionId == 0) {
                    PunchLogs pl = new PunchLogs();
                    pl.setSLN_Punch(Integer.parseInt(jObj.getString("SLN_Punch")));
                    //pl.setSLN_Employee(Integer.parseInt(jObj.getString("SLN_Employee")));
                    pl.setCard_Number(jObj.getString("Card_Number"));
                    pl.setSLN_Reader(Integer.parseInt(jObj.getString("SLN_Reader")));
                    pl.setReader_Name(jObj.getString("Reader_Name"));
                    pl.setPunch_Date(jObj.getString("Punch_Date"));
                    //boolean st = jObj.getBoolean("Grant_Deny");
                    pl.setGrant_Deny(true);
                    pl.setIsPunchSynced(1);
                    pl.setSLN_Location(Integer.parseInt(jObj.getString("SLN_Location")));
                    pl.setSLN_Area_ID(Integer.parseInt(jObj.getString("SLN_Area_ID")));
                    pl.setMusteringId(Integer.parseInt(jObj.getString("MusteringId")));

                    db.updateEmployeesLocations(pl);
                } else {
                    MemberAccessLogs memacc_logs = new MemberAccessLogs();
                    memacc_logs.setMemAccLogsEmpId(Integer.parseInt(jObj.getString("SLN_Employee")));
                    memacc_logs.setMemAccLogsCardNo(jObj.getString("Card_Number"));
                    memacc_logs.setMemAccLogsReaderId(Integer.parseInt(jObj.getString("SLN_Reader")));
                    memacc_logs.setMemAccLogsPunchDate(jObj.getString("Punch_Date"));
                    //boolean st = jObj.getBoolean("Grant_Deny");
                    memacc_logs.setMemAccLogsAccessGrant(Integer.parseInt(jObj.getString("Access_Code")));
                    memacc_logs.setMemAccLogsSessionId(Integer.parseInt(jObj.getString("Session_Code")));
                    memacc_logs.setMemAccLogsIsSync(1);

                    db.updateMemAccPunchLogsData(memacc_logs);
                }
            }

            // to broadcast the event so we can refresh the list
            //EventBus.getDefault().post(new MainActivity.MessageEvent(HandlerHelper.UPDATE_EMPLOYEE_LOCATION));
            //EventBus.getDefault().post(new NFCMainActivity.MessageEvent(HandlerHelper.UPDATE_EMPLOYEE_LOCATION));
            //EventBus.getDefault().post(new LoadEmployeesLocation.MessageEvent(HandlerHelper.UPDATE_EMPLOYEE_LOCATION));

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
    // To Update PunchList
    public void onEmployeesLocationAPICall () {
        final DatabaseHandler db = new DatabaseHandler(this);
        if (GlobalClass.getInstance().isNetworkAvailable(this)) {
            // to fetch the data from the API for categories
            ApiInterface apiEmployeesLocationService = new
                    ApiClient(this).getClient().create(ApiInterface.class);

            // to get current date
            Calendar c = Calendar.getInstance();
            /*int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH) + 1;
            int mDate = c.get(Calendar.DATE);
            final String curr_date = mYear+"-"+mMonth+"-"+mDate;*/
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            final String curr_date = df.format(c.getTime());
            Log.d(TAG, "Current date: "+curr_date);

            Call<PunchLogs> call_categories = apiEmployeesLocationService.getEmployeesLocation(); // (0, 0);
            call_categories.enqueue(new Callback<PunchLogs>() {
                @Override
                public void onResponse(Call<PunchLogs> call, Response<PunchLogs> response) {
                    int statusCode = response.code();
                    Log.d(TAG, "response code" + statusCode);
                    if (response.isSuccessful()) {
                        int status = 0;
                        String message = "";
                        ArrayList<PunchLogs> data;
                        PunchLogs responseObj = response.body();
                        status = responseObj.getStatus();
                        message = responseObj.getMessage();
                        data = responseObj.getData();
                        if (status == 1 && data != null && data.size() > 0) {
                            Log.d(TAG, "All Employee Message: " + message);
                            Log.d(TAG, "All Emlpoyee Size: " + data.size());
                            // to delete the table data before insert new entries
                            db.deleteEmployeesLocation();
                            // to clear employee data every time
                            for (int i = 0; i < data.size(); i++) {
                                PunchLogs pl = data.get(i);//new Categories();
                                int sln_punch = pl.getSLN_Punch();
                                Log.d(TAG, "EmpPunchId: " + sln_punch);
                                //int emp_id = pl.getSLN_Employee();//dataObj.getInt(APIParam.RES_CAT_ID);
                                //Log.d(TAG, "EmpId: " + emp_id);
                                String card_number = pl.getCard_Number();//dataObj.getString(APIParam.RES_CAT_NAME);
                                Log.d(TAG, "EmpCardNo: " + card_number);
                                int sln_reader = pl.getSLN_Reader();
                                Log.d(TAG, "EmpSLNReader: " + sln_reader);
                                String reader_name = pl.getReader_Name();
                                Log.d(TAG, "Emp Reader Name: " + reader_name);
                                String punch_date = pl.getPunch_Date();
                                Log.d(TAG, "EmpPunchDate: " + punch_date);
                                boolean grant_deny = pl.isGrant_Deny(); // set 0 or 1 while inserting
                                Log.d(TAG, "EmpGeantDeny: " + grant_deny);
                                int isSynched = pl.isGrant_Deny() ? 1 : 0 ;
                                pl.setIsPunchSynced(isSynched);
                                int sln_loc = pl.getSLN_Location();
                                Log.d(TAG, "EmpLocation: " + sln_loc);
                                int SLN_Area_ID = pl.getSLN_Area_ID();
                                Log.d(TAG, "EmpArewaId: " + SLN_Area_ID);
                                int cat_id = pl.getMusteringId();
                                Log.d(TAG, "EmpCatId: "+cat_id);

                                db.addEmployeesLocation(pl);
                            }
                            // to broadcast the event so we can refresh the list
                            //EventBus.getDefault().post(new MainActivity.MessageEvent(HandlerHelper.UPDATE_EMPLOYEE_LOCATION));
                            //EventBus.getDefault().post(new NFCMainActivity.MessageEvent(HandlerHelper.UPDATE_EMPLOYEE_LOCATION));
                            //EventBus.getDefault().post(new LoadEmployeesLocation.MessageEvent(HandlerHelper.UPDATE_EMPLOYEE_LOCATION));

                        } else if (status == -1) {

                        }
                    }
                }

                @Override
                public void onFailure(Call<PunchLogs> call, Throwable t) {
                    // Log error here since request failed
                    Log.d(TAG, "onFailure: ");
                }
            });
        } else {
            Log.d(TAG, "No internet");
        }
    }
}
