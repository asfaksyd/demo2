package com.smartportable.demo2.services;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import com.smartportable.demo2.api.ApiClient;
import com.smartportable.demo2.api.ApiInterface;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.GlobalClass;
import com.smartportable.demo2.helper.PreferencesManager;
import com.smartportable.demo2.helper.PunchLogs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.Nullable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ananth on 3/7/2017.
 */

public class SyncDataService extends Service {
    public static final String TAG = "SyncDataService";
    Context mContext;
    ApiInterface apiEmployeesLocationService;
    DatabaseHandler db;
    int count = 1;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate()");
        mContext = this;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.d(TAG, "onStartService()"+ count++);
        mContext = this;
        if (!isForeground(this.getPackageName(), "application")) { // app is in background
            Log.d(TAG, "App In Background");
            if (GlobalClass.getInstance().isNetworkAvailable(this)) {
                db = new DatabaseHandler(this);

                // to make false sync status if it is true because it is going to resend data
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                boolean loc_st = prefs.getBoolean(PreferencesManager.KEY_LOCATION_SYNCED, false);
                if (loc_st) {
                    // update the location sync flag as page refreshed
                    SharedPreferences.Editor edit = prefs.edit();
                    edit.putBoolean(PreferencesManager.KEY_LOCATION_SYNCED, false);
                    edit.commit();
                }

                if (!db.checkNotSyncedPunchEntries()) {
                    onEmployeesLocationAPICall();
                } else {
                    if (GlobalClass.getInstance().isNetworkAvailable(this)) {
                    /*AlertDialog.Builder mDialog = new AlertDialog.Builder(this);
                    LayoutInflater adbInflater = LayoutInflater.from(this);
                    View eulaLayout = adbInflater.inflate(R.layout.dialog_checkbox, null);

                    final CheckBox sync_card_first = (CheckBox) eulaLayout.findViewById(R.id.sync_with_image);
                    sync_card_first.setText("Yes, I want to sync flash card first");
                    mDialog.setView(eulaLayout);
                    mDialog.setTitle("Sync warning!");
                    mDialog.setMessage(Html.fromHtml("<h5>Some card yet not synced with server, so must have to sync that first before fetch latest location of employees."));
                    mDialog.setCancelable(false);

                    mDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {*/

                        //if (sync_card_first.isChecked()) {
                        new GetSyncCardsFirst(this).execute();
                            /*} else {
                                onEmployeesLocationAPICall();
                            }
                        }
                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.d(TAG, "Negative button click");
                        }
                    });
                    mDialog.show();*/
                    } else {
                        Log.d(TAG, "No internet connection!");
                    }
                }
            }

        } // In foreground check condition end here
        return super.onStartCommand(intent, flags, startId);
    }

    public class GetSyncCardsFirst extends AsyncTask<Void, Void, Void> {
        ProgressDialog pDialog;
        Context mContext;

        GetSyncCardsFirst(Context mContext) {
            this.mContext = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            ArrayList<PunchLogs> list = db.getNotSyncPunchEntries();
            if (list.size() > 0) {
                try {
                    //JSONObject jsonObject = new JSONObject();
                    JSONArray jArray = new JSONArray();
                    for (int i=0; i<list.size(); i++) {

                        PunchLogs pl = list.get(i);
                        Log.d(TAG, "Card Number: "+list.get(i).getCard_Number());
                        JSONObject punchObj = new JSONObject();
                        punchObj.put("SLN_Punch", "" + pl.getSLN_Punch());
                        punchObj.put("SLN_Employee", "" + pl.getSLN_Employee());
                        punchObj.put("Card_Number", pl.getCard_Number());
                        punchObj.put("SLN_Reader", "" + pl.getSLN_Reader());
                        punchObj.put("Reader_Name", pl.getReader_Name());
                        punchObj.put("Punch_Date", pl.getPunch_Date());
                        punchObj.put("Grant_Deny", pl.getIsPunchSynced()==0 ? false : true);
                        punchObj.put("SLN_Location", "" + pl.getSLN_Location());
                        punchObj.put("SLN_Area_ID", "" + pl.getSLN_Area_ID());
                        punchObj.put("MusteringId", "" + pl.getMusteringId());

                        jArray.put(punchObj);
                    }
                    //jsonObject.put("Data", jArray);
                    Log.d(TAG, "PunchArray: "+jArray.toString());
                    sendPunchDataAPiCall(jArray.toString());
                } catch(JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // Whether the pending records or not, or it has synced ot not update employee location must call
            onEmployeesLocationAPICall();
        }
    }

    public void sendPunchDataAPiCall (final String jsonPunchData) {
        String url = URLCollections.getBaseURLFromPreference(this);
        if (url == null || url.equals("") || !URLCollections.isValidUrl(url)) {
            Log.d(TAG, "Connection Url is not set");
        } else if (GlobalClass.getInstance().isNetworkAvailable(this)) {
            ApiInterface apiSendPunches = new ApiClient(this).getClient().create(ApiInterface.class);
            Call<ResponseBody> call_reader = apiSendPunches.sendPunchData(jsonPunchData);
            call_reader.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    int statusCode = response.code();
                    Log.d(TAG, "response code " + statusCode);
                    if (statusCode != 200) {
                        Log.d(TAG, "Problem with fetching data");
                    }

                    if (response.isSuccessful()) {
                        //Log.d(TAG, "Card Number: " + "Response" +response + "Response Body : " + response.body() + "Body String: "+response.body().toString() + " Header: " + response.headers() + " raw: " + response.raw());
                        //onEmployeesLocationAPICall();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    // Log error here since request failed
                }
            });
        } else {
            Log.d(TAG, "No internet");
        }
    }

    public void onEmployeesLocationAPICall () {
        String url = URLCollections.getBaseURLFromPreference(this);
        if (url == null || url.equals("") || !URLCollections.isValidUrl(url)) {
            Log.d(TAG, "Connection Url is not set");
        } else if (GlobalClass.getInstance().isNetworkAvailable(this)) {
            apiEmployeesLocationService = new
                    ApiClient(this).getClient().create(ApiInterface.class);

            // to get current date
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String curr_date = df.format(c.getTime());
            Log.d(TAG, "Current date: "+curr_date);

            Call<PunchLogs> call_categories = apiEmployeesLocationService.getEmployeesLocation(); // (0, 0);
            call_categories.enqueue(new Callback<PunchLogs>() {
                @Override
                public void onResponse(Call<PunchLogs> call, Response<PunchLogs> response) {
                    int statusCode = response.code();
                    Log.d(TAG, "response code" + statusCode);
                    if (statusCode != 200) {
                        Log.d(TAG, "Problem with fetching data");
                    }

                    if (response.isSuccessful()) {
                        int status = 0;
                        String message = "";
                        ArrayList<PunchLogs> data;
                        PunchLogs responseObj = response.body();
                        status = responseObj.getStatus();
                        message = responseObj.getMessage();
                        data = responseObj.getData();
                        if (status == 1 && data != null && data.size() > 0) {
                            //Log.d(TAG, "All Employee Message: " + message);
                            //Log.d(TAG, "All Emlpoyee Size: " + data.size());
                            // to delete the table data before insert new entries
                            db.deleteEmployeesLocation();
                            // to clear employee data every time
                            for (int i = 0; i < data.size(); i++) {
                                PunchLogs pl = data.get(i);//new Categories();
                                int sln_punch = pl.getSLN_Punch();
                                //Log.d(TAG, "EmpPunchId: " + sln_punch);
                                int emp_id ;//= pl.getSLN_Employee();//dataObj.getInt(APIParam.RES_CAT_ID);
                                //Log.d(TAG, "EmpId: " + emp_id);
                                String card_number = pl.getCard_Number();//dataObj.getString(APIParam.RES_CAT_NAME);
                                //Log.d(TAG, "EmpCardNo: " + card_number);
                                int sln_reader = pl.getSLN_Reader();
                                //Log.d(TAG, "EmpSLNReader: " + sln_reader);
                                String punch_date = pl.getPunch_Date();
                                //Log.d(TAG, "EmpPunchDate: " + punch_date);
                                boolean grant_deny = pl.isGrant_Deny(); // set 0 or 1 while inserting
                                //Log.d(TAG, "EmpGeantDeny: " + grant_deny);
                                int isSynched = grant_deny ? 1 : 0;
                                pl.setIsPunchSynced(isSynched);
                                //Log.d(TAG, "setIsPunchSynced"+isSynched);
                                int sln_loc = pl.getSLN_Location();
                                //Log.d(TAG, "EmpLocation: " + sln_loc);
                                int SLN_Area_ID = pl.getSLN_Area_ID();
                                //Log.d(TAG, "EmpAreaId: " + SLN_Area_ID);
                                int cat_id = pl.getMusteringId();
                                //Log.d(TAG, "EmpCatId: "+cat_id);

                                db.addEmployeesLocation(pl);
                            }

                            // Store the sync statuc into Shared Preference to refresh the list of the muster page
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
                            SharedPreferences.Editor editor = prefs.edit();

                            // considering that old records deleted and new records are added
                            if (isForeground(getPackageName(), "activity")) {
                                Log.d(TAG, "NFC Activity is in Foreground");
                                editor.putBoolean(PreferencesManager.KEY_LOCATION_SYNCED, false);
                            } else {
                                Log.d(TAG, "NFC Activity is in Background");
                                editor.putBoolean(PreferencesManager.KEY_LOCATION_SYNCED, true);
                            }
                            editor.commit();

                        } else if (status == -1) {
                            Log.d(TAG, "All location exception Message: " + message);
                        } else {
                            Log.d(TAG, "<h4> No records found</h4> </br>");
                        }
                    }
                }

                @Override
                public void onFailure(Call<PunchLogs> call, Throwable t) {
                    // Log error here since request failed

                    Log.e(TAG, "Location Response error : " + t.toString());
                }
            });
        } else {
            Log.d(TAG, "No Connection");
        }
    }

    // to check whether app is running in foregound or not
    public boolean isForeground(String myPackage, String check_type) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;

        // to check user want to check application only in forground
        if (check_type.equals("application")) {
            return componentInfo.getPackageName().equals(myPackage);
        } else if (check_type.equals("activity")) {
            /*if (CpcOs.isC5()) {
                return componentInfo.getClassName().equals(NFCMainActivity.class.getCanonicalName());
            }else {
                return componentInfo.getClassName().equals(MainActivity.class.getCanonicalName());
            }*/
            return false;

        } else {
            return false;
        }

    }
}
