package com.smartportable.demo2;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.Boast;
import com.smartportable.demo2.helper.Config;
import com.smartportable.demo2.listeners.DialogListener;
import com.smartportable.demo2.utils.AwesomeProgressDialog;
import com.smartportable.demo2.utils.KeysKt;
import com.smartportable.demo2.utils.SharedPref;

import java.util.HashMap;
import java.util.Map;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


/**
 * Created by Ananth on 12/25/2016.
 */

public class SplashScreen extends Activity implements DialogListener {
    Handler handler;
    Runnable runnable;
    DatabaseHandler db;
    RelativeLayout rlSplashParent;
    TextView tvCopyRight;

    AwesomeProgressDialog mDialog;
    private boolean sentToSettings = false;
    SharedPreferences permissionStatus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        rlSplashParent = (RelativeLayout) findViewById(R.id.rlSplashParent);
        tvCopyRight = (TextView) findViewById(R.id.tvCopyRight);

        permissionStatus = PreferenceManager.getDefaultSharedPreferences(this);

        SharedPref.INSTANCE.setStringValue(this, KeysKt.keyBaseUrl, URLCollections.BASE_URL);
        SharedPref.INSTANCE.setStringValue(this, KeysKt.keyAPIBaseUrl,URLCollections.BASE_URL+"/API/api/");

        if (SharedPref.INSTANCE.getStringValue(this, KeysKt.keyBaseUrl, "").equals("")) {
            displayAlert();
        }
        else {
            initialize();
        }
    }

    private void initialize() {
        final Animation in = new AlphaAnimation(0.0f, 1.0f);
        in.setDuration(1500);

        AnimationSet as = new AnimationSet(true);
        as.addAnimation(in);

        tvCopyRight.clearAnimation();
        tvCopyRight.startAnimation(in);

        db = new DatabaseHandler(this);
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //start your activity here
                Intent i = null;
                if (!SharedPref.INSTANCE.getAuthToken(getApplicationContext()).isEmpty()) {
                    i = new Intent(SplashScreen.this, HomeScreen.class);
                } else {
                    i = new Intent(SplashScreen.this, LoginActivity.class);
                }
                if (Build.VERSION.SDK_INT >= 23) {
                    int permissionCheck = ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    int permissionCameraCheck = ContextCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.CAMERA);

                    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                        if (!ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            ActivityCompat.requestPermissions(SplashScreen.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                    Config.REQUEST_CODE_ASK_PERMISSIONS);
                        } else if (permissionStatus.getBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE, false)) {
                            showMessageOKCancel();
                        } else {
                            //just request the permission
                            ActivityCompat.requestPermissions(SplashScreen.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, Config.REQUEST_CODE_ASK_PERMISSIONS);
                        }

                        SharedPreferences.Editor editor = permissionStatus.edit();
                        editor.putBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE, true);
                        editor.apply();

                        return;

                    } else if (permissionCameraCheck != PackageManager.PERMISSION_GRANTED) {
                        if (!ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, Manifest.permission.CAMERA)) {
                            ActivityCompat.requestPermissions(SplashScreen.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                    Config.REQUEST_CODE_ASK_PERMISSIONS);
                        } else if (permissionStatus.getBoolean(Manifest.permission.CAMERA, false)) {
                            showCameraMessageOKCancel();
                        } else {
                            //just request the permission
                            ActivityCompat.requestPermissions(SplashScreen.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, Config.REQUEST_CODE_ASK_PERMISSIONS);
                        }

                        SharedPreferences.Editor editor = permissionStatus.edit();
                        editor.putBoolean(Manifest.permission.CAMERA, true);
                        editor.apply();

                        return;
                    } else {
                        startActivity(i);
                        finish();
                    }
                } else {
                    startActivity(i);
                    finish();
                }

            }
        }, 2000);
    }

    public void showMessageOKCancel() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreen.this);
        builder.setTitle(R.string.alert_title_storage_permission);
        builder.setMessage(R.string.alert_desc_storage_permission);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.ok_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                //ActivityCompat.requestPermissions(SplashScreen.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Config.REQUEST_CODE_ASK_PERMISSIONS);
                sentToSettings = true;
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, Config.REQUEST_PERMISSION_SETTING);
                Boast.makeText(getBaseContext(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton(getString(R.string.cancel_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        builder.show();
    }

    public void showCameraMessageOKCancel() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreen.this);
        builder.setTitle(R.string.alert_title_camera_permission);
        builder.setMessage(R.string.alert_desc_camera_permission);
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.ok_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                //ActivityCompat.requestPermissions(SplashScreen.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Config.REQUEST_CODE_ASK_PERMISSIONS);
                sentToSettings = true;
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, Config.REQUEST_PERMISSION_SETTING);
                Boast.makeText(getBaseContext(), "Go to Permissions to Grant Camera", Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton(getString(R.string.cancel_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Config.REQUEST_CODE_ASK_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for STORAGE
                if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                    // Permission Denied
                    if (ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        ActivityCompat.requestPermissions(SplashScreen.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, Config.REQUEST_CODE_ASK_PERMISSIONS);
                    } else {
                        showMessageOKCancel();
                    }
                } else if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
                    // Permission Denied
                    if (ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, Manifest.permission.CAMERA)) {
                        ActivityCompat.requestPermissions(SplashScreen.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, Config.REQUEST_CODE_ASK_PERMISSIONS);
                    } else {
                        showCameraMessageOKCancel();
                    }
                } else {
                    // All Permissions Granted
                    Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
                /*if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    // Permission Denied
                    if (ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        ActivityCompat.requestPermissions(SplashScreen.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Config.REQUEST_CODE_ASK_PERMISSIONS);
                    } else {
                        showMessageOKCancel();
                    }
                }

                if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    // Permission Denied
                    if (ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, Manifest.permission.CAMERA)) {
                        ActivityCompat.requestPermissions(SplashScreen.this, new String[]{Manifest.permission.CAMERA}, Config.REQUEST_CODE_ASK_PERMISSIONS);
                    } else {
                        showMessageOKCancel();
                    }
                }*/
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (runnable != null) {
            handler.removeCallbacks(runnable);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Config.REQUEST_PERMISSION_SETTING) {// && resultCode == Activity.RESULT_OK) {
            int permissionStorageCheck = ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int permissionCameraCheck = ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.CAMERA);

            if (permissionStorageCheck == PackageManager.PERMISSION_GRANTED && permissionCameraCheck == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                finish();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(SplashScreen.this, Manifest.permission.CAMERA)) {
                    ActivityCompat.requestPermissions(SplashScreen.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, Config.REQUEST_CODE_ASK_PERMISSIONS);
                } else {
                    if (permissionStorageCheck == PackageManager.PERMISSION_DENIED) {
                        showMessageOKCancel();
                    } else if (permissionCameraCheck == PackageManager.PERMISSION_DENIED) {
                        showCameraMessageOKCancel();
                    }
                }
            }
        }
    }

    private void displayAlert() {
        //context ,listner
        mDialog = new AwesomeProgressDialog(this, this);
        mDialog.show();
    }

    @Override
    public void dialogClick(String base_url) {
        SharedPref.INSTANCE.setStringValue(this, KeysKt.keyBaseUrl,base_url);
        SharedPref.INSTANCE.setStringValue(this, KeysKt.keyAPIBaseUrl,base_url+"/API/api/");
        Boast.makeText(this,"base_url saved  >> " +base_url).show();
        mDialog.hide();
        initialize();
    }
}