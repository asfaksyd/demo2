package com.fineapp.android.retrofitClient



const val baseUrl = "http://smartcapus.wiseportable.com/API/api/"//"http://api.mmdesigner.in/"
const val imageBaseUrl = "http://213.55.95.36:8080"
const val getUserLogin = "StudentApp/Login"
const val getUserLogout = "StudentApp/Logout"
const val getStudentInfo = "StudentApp/GetStudentInformation"
const val getReaderInfo = "StudentApp/GetReader"
const val getStudentList = "StudentApp/GetStudentInformation"
const val getAccessList = "StudentApp/GetAccessList"
const val getSessions = "StudentApp/GetSessionList"
const val getStaffInfo = "StudentApp/GetStaffInformation"
const val getStaffAccessInfo = "StudentApp/GetStaffAccessList"
const val setMemAccLog = "StudentApp/SetMemAccLog"
const val getVisitorInfo = "StudentApp/GetVisitorList"
const val getAGInfo = "StudentApp/GetAGInformation"
const val setMainGateAccLogs = "StudentApp/GetAccessLogs"
const val getUserProfile = "fineapp/GetOfficer"
const val getLicenseCategoryEndpoint = "fineapp/GetLicenseCategory"


const val getDashbordDEtailsEndpoint = "fineapp/GetNotificationVariant"


const val createNotification = "fineapp/CreateNotification"
const val getNotification = "fineapp/GetNotification"

const val sendNotificationImages = "FineApp/NotificationImages"



const val getLicenseSubCategoryEndpoint = "fineapp/GetLicenseSubCategory"

const val getNationality = "fineapp/GetNationality"

const val getEduLevel = "fineapp/GetEducationLevel"

const val getTrainingLanguage = "fineapp/GetTrainingLanguage"

const val getTrainingCenter = "fineapp/GetTrainingCenter"

const val getRegion = "fineapp/GetRegionList"

const val getZone = "fineapp/GetZoneList"

const val getCity = "fineapp/GetCityList"

const val getSubCity = "fineapp/GetSubCity"


