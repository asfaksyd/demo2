package com.fineapp.android.retrofitClient

import com.google.gson.JsonObject
import com.smartportable.demo2.models.*
import retrofit2.Call
import retrofit2.http.*

interface ServiceGenerator {


    @Headers("Content-Type: application/json")
    @POST(getUserLogin)
    fun getUserLogin(@Body data: JsonObject): Call<LoginModel>

    @Headers("Content-Type: application/json")
    @POST(getUserLogout)
    fun getUserLogout(@Body data: JsonObject): Call<LoginModel>

    @Headers("Content-Type: application/json")
    @POST(getStudentInfo)
    fun getStudentInfo(@Body data: JsonObject): Call<StudentInfoModel>

    @Headers("Content-Type: application/json")
    @POST(getReaderInfo)
    fun getReaderInfo(@Body data: JsonObject): Call<ReaderInfoModel>

    @Headers("Content-Type: application/json")
    @POST(getStudentList)
    fun getStudentList(@Body data: JsonObject): Call<ReaderInfoModel>

    @Headers("Content-Type: application/json")
    @POST(getAccessList)
    fun getAccessList(@Body data: JsonObject): Call<AccessListModel>

    @Headers("Content-Type: application/json")
    @POST(getSessions)
    fun getSessions(@Body data: JsonObject): Call<SessionsInfoModel>

    @Headers("Content-Type: application/json")
    @POST(getStaffInfo)
    fun getStaffInfo(@Body data: JsonObject): Call<StaffInfoModel>

    @Headers("Content-Type: application/json")
    @POST(getStaffAccessInfo)
    fun getStaffAccessInfo(@Body data: JsonObject): Call<StaffAccessListModel>

    @Headers("Content-Type: application/json")
    @POST(setMemAccLog)
    fun setMemAccLog(@Body data: JsonObject): Call<MemAccLogsInfoModel>

    @Headers("Content-Type: application/json")
    @POST(getVisitorInfo)
    fun getVisitorInfo(@Body data: JsonObject): Call<VisitorInfoModel>

    @Headers("Content-Type: application/json")
    @POST(getAGInfo)
    fun getAGInfo(@Body data: JsonObject): Call<AGInfoModel>

    @Headers("Content-Type: application/json")
    @POST(setMainGateAccLogs)
    fun setMainGateAccLogs(@Body data: JsonObject): Call<MainGateAccLogsModel>

}