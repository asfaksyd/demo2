package com.smartportable.demo2.realm

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class ReadersModel : RealmObject(){
    @PrimaryKey
    @SerializedName("id")
    var id: Long ?= null

    @SerializedName("ReaderId")
    var readerId: Int? = null

    @SerializedName("ReaderName")
    var readerName: String? = null

    @SerializedName("ReaderType")
    var readerType: String? = null

}