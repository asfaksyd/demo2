package com.smartportable.demo2.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.smartportable.demo2.services.ScanCardService;

/**
 * Created by Ananth on 12/28/2016.
 */

public class ScanCardServiceBroadcastReceiver extends BroadcastReceiver {

    public static String TAG = "ScanCardBroadcast";
    @Override
    public void onReceive(Context mContext, Intent intent) {
        /*if (intent != null) {
            int service_action = intent.getExtras().getInt(ServiceHelper.KEY_SERVICE_ACTION);
            Log.d(TAG, "Service action "+service_action);
            Intent intent_broad_ser = new Intent(mContext, ScanCardService.class);
            if (service_action == ServiceHelper.START_SERVICE) {
                mContext.startService(intent_broad_ser);
            }else if (service_action == ServiceHelper.STOP_SERVICE) {
                mContext.stopService(intent_broad_ser);
            } else {
                mContext.startService(intent_broad_ser);
            }
        }*/
        //if (!ServiceHelper.getInstance().isMyServiceRunning(mContext, ScanCardService.class)) {
            Intent intent_broad_ser = new Intent(mContext, ScanCardService.class);
            mContext.startService(intent_broad_ser);
        //}
    }
    public static void stopService(Context context) {
        Intent stopServiceIntent = new Intent(context, ScanCardService.class);
        context.stopService(stopServiceIntent);
    }
}
