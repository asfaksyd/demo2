package com.smartportable.demo2.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.smartportable.demo2.services.SyncDataService;

/**
 * Created by Ananth on 3/7/2017.
 */

public class SyncDataServiceBroadcastReceiver extends BroadcastReceiver {
    public static String TAG = "SyncDataBroadcast";
    @Override
    public void onReceive(Context mContext, Intent intent) {
        //Log.d(TAG, "onReceive");
        Intent intent_broad_ser = new Intent(mContext, SyncDataService.class);
        mContext.startService(intent_broad_ser);
    }

    public static void stopService(Context context) {
        Intent stopServiceIntent = new Intent(context, SyncDataService.class);
        context.stopService(stopServiceIntent);
    }
}
