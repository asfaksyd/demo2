package com.smartportable.demo2.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.Config;
import com.smartportable.demo2.helper.PreferencesManager;

import fr.coppernic.sdk.utils.helpers.CpcOs;

/**
 * Created by Ananth on 12/25/2016.
 */

public class BootBroadcastReceiver extends BroadcastReceiver {
    public static String TAG = "BootBroadCastReceiver";

    @Override
    public void onReceive(Context mContext, Intent intent) {
        // Do your work related to alarm manager
        System.out.println("Device Reboot: ");
        // to start service
        DatabaseHandler db = new DatabaseHandler(mContext);
        int readerId = -1;
        try {
            final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
            readerId = prefs.getInt(PreferencesManager.KEY_SET_READER_ID, -1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (readerId != -1) {
            if (db.checkReaders() && db.checkCategories() && db.checkAreas() && db.checkEmployees()) {
                AlarmManager alarmMgr = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);

                //String model = android.os.Build.MODEL;
                //Log.d(TAG, "Device Model: "+model);
                Log.d(TAG, "Is C-One " + CpcOs.isCone());
                /*if (CpcOs.isCone()) {
                    //if (model.equals(URLCollections.READER_ENABLED_DEVICE_MODEL)) {
                    Intent intent_receiver = new Intent(mContext, ScanCardServiceBroadcastReceiver.class);
                    intent_receiver.putExtra(ServiceHelper.KEY_SERVICE_ACTION, ServiceHelper.START_SERVICE);
                    PendingIntent alarmIntent = PendingIntent.getBroadcast(mContext, 0, intent_receiver, 0);
                    //alarmMgr.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), Config.SCAN_CARD_SERVICE_INTERVAL, alarmIntent);
                } else {
                    Log.d("BootBroadCastReceiver", "Other device");
                }*/

                Intent intent_send_punch = new Intent(mContext, SendPunchDataServiceBroadcastReceiver.class);
                PendingIntent piSendPunchData = PendingIntent.getBroadcast(mContext, 0, intent_send_punch, 0);
                alarmMgr.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), Config.SEND_PUNCH_DATA_SERVICE_INTERVAL, piSendPunchData);

                // to check into backend in share preference and start the service for interval define into sharepreference
                // if it is not here it will take default value as 5 mins / 300 seconds
                int sync_interval = 0;
                try {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
                    sync_interval = Integer.parseInt(prefs.getString(PreferencesManager.KEY_SYNC_INTERVAL, "" + 300));

                    sync_interval = sync_interval * 1000;

                    if (sync_interval <= 0) {
                        sync_interval = Config.SYNC_DATA_SERVICE_INTERVAL;
                    }
                    System.out.println("Sync Interval"+ sync_interval);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent_sync_data = new Intent(mContext, SyncDataServiceBroadcastReceiver.class);
                PendingIntent piSyncData = PendingIntent.getBroadcast(mContext, 0, intent_sync_data, 0);
                alarmMgr.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), sync_interval, piSyncData);
            }
        }
    }
}
