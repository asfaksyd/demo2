package com.smartportable.demo2.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.smartportable.demo2.services.SendPunchDataService;

/**
 * Created by Ananth on 1/19/2017.
 */

public class SendPunchDataServiceBroadcastReceiver extends BroadcastReceiver {
    public static String TAG = "SendPunchDataBroadcast";
    @Override
    public void onReceive(Context mContext, Intent intent) {
        Log.d(TAG, "onReceive");
        Intent intent_broad_ser = new Intent(mContext, SendPunchDataService.class);
        mContext.startService(intent_broad_ser);
    }

    public static void stopService(Context context) {
        Intent stopServiceIntent = new Intent(context, SendPunchDataService.class);
        context.stopService(stopServiceIntent);
    }
}

