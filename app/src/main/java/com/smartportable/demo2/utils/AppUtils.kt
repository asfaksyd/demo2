package com.smartportable.demo2.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.*
import android.provider.Settings
import android.text.TextUtils
import android.util.Base64.encodeToString
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.smartportable.demo2.LoginActivity
import com.smartportable.demo2.R
import kotlinx.android.synthetic.main.dialog_force_logout.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.Inet4Address
import java.net.NetworkInterface
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import com.bumptech.glide.request.transition.Transition

const val TAG = "AppUtils"

var progressDialog: ProgressDialog? = null

fun showProgressDialog(context: Context) {
    try {
//        val layout = LayoutInflater.from(context).inflate(R.layout.popup_loading, null)
//        popupWindow = PopupWindow(layout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, false)
//        popupWindow!!.showAtLocation(view, Gravity.CENTER, 0, getStatusBarHeight(context))
        progressDialog = ProgressDialog(context)
        if (!progressDialog!!.isShowing) {
            progressDialog?.setMessage("Please wait...")
            progressDialog?.setCancelable(false)
            progressDialog?.show()
        }
    } catch (e: Throwable) {
        Log.e(TAG, e.message)
        e.printStackTrace()
    }

}

fun stopProgress() {
    try {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog?.dismiss()
        }
    } catch (e: Throwable) {
        e.printStackTrace()
    }
}


fun toast(context: Context?, text: String?) {
    if (context != null && text != null) {
        val toast = android.widget.Toast.makeText(context, text, android.widget.Toast.LENGTH_SHORT)
//        toast.setGravity(Gravity.TOP or Gravity.CENTER_HORIZONTAL, 0, 200)
        toast.show()
    }
}

fun toast(context: Context?, text: Int?) {
    if (context != null && text != null) {
        val toast = android.widget.Toast.makeText(context, text, android.widget.Toast.LENGTH_SHORT)
//        toast.setGravity(Gravity.TOP or Gravity.CENTER_HORIZONTAL, 0, 200)
        toast.show()
    }


}


fun isValidText(str: String): Boolean {
    val expression = "^[a-zA-Z\\s]+"
    return str.matches(expression.toRegex())
}

fun isValidUserName(str: String): Boolean {
    val expression = "^[a-zA-Z0-9]*$"
    return str.matches(expression.toRegex())
}

fun isValidMail(mailString: String): Boolean {
    return !TextUtils.isEmpty(mailString) && android.util.Patterns.EMAIL_ADDRESS.matcher(mailString).matches()
}


fun isNetworkAvailable(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetworkInfo = connectivityManager.activeNetworkInfo
    return activeNetworkInfo != null && activeNetworkInfo.isConnected
}

@SuppressLint("MissingPermission", "HardwareIds")
fun getAndroidId(context: Context): String {
    val deviceId = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    println("$TAG, getAndroidId : deviceId >> $deviceId")
    return deviceId
}

//fun getImageRequestBody(file: File): RequestBody {
  //  return file.asRequestBody("image/jpeg".toMediaTypeOrNull())//RequestBody.create(MediaType("image/*"), file)
//}

//fun getStringRequestBody(strParam: String): RequestBody {
  //  return RequestBody.create(MediaType.parse("text/plain"), strParam)
//}

//fun getIntRequestBody(intParam: Int): RequestBody {
    //return RequestBody.create(MediaType.parse("text/plain"), intParam.toString())
//}

fun getLocalIpAddress(): String {
    try {
        val en = NetworkInterface.getNetworkInterfaces()
        while (en.hasMoreElements()) {
            val intf = en.nextElement()
            val enumIpAddr = intf.inetAddresses
            while (enumIpAddr.hasMoreElements()) {
                val inetAddress = enumIpAddr.nextElement()
                if (!inetAddress.isLoopbackAddress && inetAddress is Inet4Address) {
                    return inetAddress.getHostAddress()
                }
            }
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
    }

    return ""
}

fun validatePassword(password: String): Boolean {
    val strPattern = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
    var pattern: Pattern = Pattern.compile(strPattern)
    var matcher: Matcher = pattern.matcher(password)
    return matcher.matches()
}

fun convertDateTime(fromFormat: String, toFormat: String, dateOriginalGot: String): String {

    try {
        //SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //// Getting Source format here
        val fmt = SimpleDateFormat(fromFormat)

        fmt.timeZone = TimeZone.getDefault()

        val date = fmt.parse(dateOriginalGot)

        //SimpleDateFormat fmtOut = new SimpleDateFormat("EEE, MMM d, ''yyyy");

        //// Setting Destination format here
        val fmtOut = SimpleDateFormat(toFormat)

        return fmtOut.format(date)

    } catch (e: Exception) {

        e.printStackTrace()

        e.message

    }

    return ""

}


fun formatString(d: Double): String {
    return if (d == d.toLong().toDouble())
        String.format("%d", d.toLong())
    else
        String.format("%s", d)
}


fun hasPermissions(context: Context?, permissions: Array<String>): Boolean {
    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null) {
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
    }
    return true
}

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}



fun submitSearchQuery(context: Context, query: String) {
//    toast(context, query)
//    val intent = Intent(context, ProductListActivity::class.java)
//    intent.putExtra("search_query", query)
//    context.startActivity(intent)
}


fun encodeStringToBase64(str: String): String {
    val data = str.toByteArray(charset("UTF-8"))
    return encodeToString(data, android.util.Base64.DEFAULT)
}

fun decodeBase64(base64: String): String {
    return try {
        val data = android.util.Base64.decode(base64, android.util.Base64.DEFAULT)
        String(data, charset("UTF-8"))
    } catch (e: Exception) {
        Log.e(TAG, "decodeBase64 >> $e")
        base64
    }
}

fun isColorValid(colorString: String): Boolean {
    val colorPattern = Pattern.compile("#([0-9a-f]{3}|[0-9a-f]{6}|[0-9a-f]{8})")
    val m = colorPattern.matcher(colorString)
    return m.matches()
}


fun loadJSONFromAsset(context: Context, fileName: String): String? {
    val json: String?
    try {
        val inputStream = context.assets.open(fileName)
        val size = inputStream.available()
        val buffer = ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()
        json = String(buffer)
    } catch (ex: IOException) {
        ex.printStackTrace()
        return null
    }

    return json
}


fun decodeUnicode(theString: String): String {
    try {
        var aChar: Char
        val len = theString.length
        val outBuffer = StringBuffer(len)
        var x = 0
        while (x < len) {
            aChar = theString[x++]
            if (aChar == '\\') {
                aChar = theString[x++]
                if (aChar == 'u') {
                    // Read the xxxx
                    var value = 0
                    for (i in 0..3) {
                        aChar = theString[x++]
                        when (aChar) {
                            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' -> value = (value shl 4) + aChar.toInt() - '0'.toInt()
                            'a', 'b', 'c', 'd', 'e', 'f' -> value = (value shl 4) + 10 + aChar.toInt() - 'a'.toInt()
                            'A', 'B', 'C', 'D', 'E', 'F' -> value = (value shl 4) + 10 + aChar.toInt() - 'A'.toInt()
                            else -> throw IllegalArgumentException(
                                    "Malformed   \\uxxxx   encoding.")
                        }

                    }
                    outBuffer.append(value.toChar())
                } else {
                    if (aChar == 't')
                        aChar = '\t'
                    else if (aChar == 'r')
                        aChar = '\r'
                    else if (aChar == 'n')
                        aChar = '\n'
//                    else if (aChar == 'f')
//                        aChar = '\f'
                    outBuffer.append(aChar)
                }
            } else
                outBuffer.append(aChar)
        }
        return outBuffer.toString()
    } catch (e: Exception) {
        return theString
    }
}

fun showDialogPermissionRational(context: Context, strMessage: String) {
    val builder = AlertDialog.Builder(context)

    // Set the alert dialog title
    builder.setTitle("Permission Required")

    // Display a message on alert dialog
    builder.setMessage(strMessage)

    // Set a positive button and its click listener on alert dialog
    builder.setPositiveButton("YES") { _, _ ->
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val uri = Uri.fromParts("package", context.packageName, null)
        intent.data = uri
        context.startActivity(intent)
    }

//    builder.setNegativeButton("No") { dialog, which ->
//    }
//
//    builder.setNeutralButton("Cancel") { _, _ ->
//    }

    val dialog: AlertDialog = builder.create()
//    dialog.setCancelable(false)
    dialog.show()
}

fun isMyServiceRunning(context: Context, serviceClass: Class<*>): Boolean {
    val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as (ActivityManager)
    for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
        if (serviceClass.name == service.service.className) {
            return true
        }
    }
    return false
}

fun customToastOnTop(mContext : Context, msg : String) {
    val toast = Toast.makeText(mContext, msg, Toast.LENGTH_SHORT)
    toast.setGravity(Gravity.TOP, 0, 200)
    val toastContentView = toast.getView() as LinearLayout
    toastContentView.orientation = LinearLayout.HORIZONTAL
    var params : LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
    params.setMargins(0,500,0,0)
    toastContentView.layoutParams = params
    val imageView = ImageView(mContext)
    imageView.setImageResource(R.drawable.ic_down_arrow)
    toastContentView.addView(imageView, 0)
    toast.show()
}

fun forceLogout(context: Context) {
    val dialog = Dialog(context)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    dialog.setContentView(R.layout.dialog_force_logout)
    dialog.setCancelable(false)
    dialog.show()

    dialog.linear.setOnClickListener {
        dialog.dismiss()
        ActivityCompat.finishAffinity(context as Activity)
        context.startActivity(Intent(context, LoginActivity::class.java))

        //SharedPref.clear(context)
        SharedPref.removeLogin(context)
    }
}

fun warningMessage(context: Context, msg : String) {
    val dialog = Dialog(context)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    dialog.setContentView(R.layout.dialog_force_logout)
    dialog.setCancelable(false)
    dialog.show()

    dialog.tvMessage.text = msg
    dialog.linear.setOnClickListener {
        dialog.dismiss()
        //ActivityCompat.finishAffinity(context as Activity)
    }
}

fun stopRequest(activity: Activity, hStopRequest: Handler)
{

    val dialog = Dialog(activity)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    dialog.setContentView(R.layout.dialog_force_logout)

    val txtOk = dialog.findViewById<View>(R.id.txtOk) as TextView

    val txtCancel = dialog.findViewById<View>(R.id.txtCancel) as TextView

    txtCancel.visibility = View.VISIBLE

    dialog.tvMessage.text = "Sync is in process, Do you want to stop that?"

    txtOk.setOnClickListener {

        val message = Message()
        val b = Bundle()
        b.putBoolean("isOkClicked", true)
        message.setData(b)
        hStopRequest.sendMessage(message)

        dialog.dismiss()

      /*  dialog.dismiss()
        apiRetrofit.cancel()
        context.finish()*/

    }

    txtCancel.setOnClickListener {

        val message = Message()
        val b = Bundle()
        b.putBoolean("isOkClicked", false)
        message.setData(b)
        hStopRequest.sendMessage(message)
        dialog.dismiss()
    }

    dialog.linear.setOnClickListener {

        val message = Message()
        val b = Bundle()
        b.putBoolean("isOkClicked", false)
        message.setData(b)
        hStopRequest.sendMessage(message)
        dialog.dismiss()

    }

    //dialog.setCancelable(false)

    if(activity!=null && !activity.isFinishing)
    {
        dialog.show()
    }



}


fun ImageToLocal(mContext: Context, imageUrl: String?, imageView: ImageView?,imageFolder: String?)
{
    Glide.with(mContext)
            .asBitmap()
            .load(imageUrl)
            .into(object : CustomTarget<Bitmap>(200, 200) {
                override fun onResourceReady(bMap: Bitmap, transition: Transition<in Bitmap>?) {

                    if(imageView!=null)
                        imageView.setImageBitmap(bMap)

                    @SuppressLint("DefaultLocale") val fName_ = imageUrl?.substring(imageUrl.lastIndexOf('/') + 1)

                    val sdCard = Environment.getExternalStorageDirectory()
                    //@SuppressLint("DefaultLocale") String fileName = String.format("%d.jpg", System.currentTimeMillis());
                    val dir = File(sdCard.absolutePath + "/ImageSave_play/"+imageFolder)

                    if (!dir.exists())
                    {
                        dir.mkdirs()
                    }

                    val ImageFile = File(dir, fName_) // Create image file

                    var fileOutputStream: FileOutputStream? = null
                    try {


                        if (!ImageFile.exists()) {
                            Log.e("Transform", "Transform----!exists---adapter--$fName_")

                            fileOutputStream = FileOutputStream(ImageFile)

                            bMap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream)

                        } else if (ImageFile.exists() && ImageFile.length() == 0L) {
                            fileOutputStream = FileOutputStream(ImageFile)

                            bMap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream)

                            //Log.e("Transform", "Transform----exists--corrupt----adapter---$fName_")
                        } else
                            Log.e("Transform", "Transform----exists------adapter--$fName_")


                    } catch (e: IOException) {
                        e.printStackTrace()

                       // Log.e("Transform", "Transform----IOException-ex--insert----adapter---$e")
                    } finally {
                        try {
                            fileOutputStream?.close()
                        } catch (e: IOException) {
                            e.printStackTrace()

                            //Log.e("Transform", "Transform----IOException-ex--close----adapter---$e")
                        }

                    }

                }

                override fun onLoadCleared(placeholder: Drawable?) {}
            })
}

// get Current TimeZone
fun getCurrentTimezoneOffset(): String {

    val tz = TimeZone.getDefault()
    val cal = GregorianCalendar.getInstance(tz)
    val offsetInMillis = tz.getOffset(cal.timeInMillis)

    var offset =
            String.format("%02d:%02d", Math.abs(offsetInMillis / 3600000), Math.abs(offsetInMillis / 60000 % 60))
    offset = "GMT" + (if (offsetInMillis >= 0) "+" else "-") + offset

    return offset
}

fun convert12To24FormatTime(inputTime: String) : String? {
    val df = SimpleDateFormat("hh:mm aa")
    //Desired format: 24 hour format: Change the pattern as per the need
    val outputformat = SimpleDateFormat("HH:mm")
    var date: Date? = null
    var output: String? = null
    try {
        //Converting the input String to Date
        date = df.parse(inputTime)
        //Changing the format of date and storing it in String
        output = outputformat.format(date)
        //Displaying the date
        println("24 : "+output)
    } catch (pe: ParseException) {
        pe.printStackTrace()
    }
    return output

}
/*
 * This method is used to read data from file for deSerialization.
 *
 * @param file
 * @return
 * @throws IOException
 * @throws ClassNotFoundException
@Throws(IOException::class, ClassNotFoundException::class)
fun deSerialization(file: String): Any {
    /*val fileInputStream = FileInputStream(file)
    val bufferedInputStream = BufferedInputStream(fileInputStream)
    val objectInputStream = ObjectInputStream(bufferedInputStream)
    val `object` = objectInputStream.readObject()
    objectInputStream.close()
    return `object`*/

    var bais = ByteArrayInputStream(file.toByteArray(charset("ISO-8859-1")))
    var ois = ObjectInputStream(bais)

    return ois.readObject() as Any

}*/