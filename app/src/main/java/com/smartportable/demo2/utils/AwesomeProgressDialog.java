package com.smartportable.demo2.utils;

import android.content.Context;
import android.graphics.PorterDuff;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.smartportable.demo2.R;
import com.smartportable.demo2.listeners.DialogListener;

import androidx.core.content.ContextCompat;


/**
 * Created by blennersilva on 23/08/17.
 */

public class AwesomeProgressDialog extends AwesomeDialogBuilder<AwesomeProgressDialog>   {

    private ProgressBar progressBar;
    private RelativeLayout dialogBody;
    Button btn_ok;
    EditText edt_url;
    Context ctx;
    DialogListener listner ;
    ImageView dialog_icon;

    public AwesomeProgressDialog(Context context , DialogListener listner) {
        super(context);

        ctx =context;
        setColoredCircle(R.color.colorPrimary);
        setProgressBarColor(R.color.colorWhite);
        this.listner = listner;
    }

    {
        progressBar = findView(R.id.dialog_progress_bar);
        dialogBody = findView(R.id.dialog_body);
        btn_ok = findView(R.id.btn_ok);
        edt_url = findView(R.id.edt_url);
        dialog_icon = findView(R.id.dialog_icon);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //hide key board ..

                String url = edt_url.getEditableText().toString().trim();

                if(url.length()>0){
                    //listener
                    dialog_icon.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    listner.dialogClick(url);
                }
                else{
                    Toast.makeText(ctx, "Please Insert Url!",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public AwesomeProgressDialog setDialogBodyBackgroundColor(int color){
        if (dialogBody != null) {
            dialogBody.getBackground().setColorFilter(ContextCompat.getColor(getContext(), color), PorterDuff.Mode.SRC_IN);
        }

        return this;
    }

    public AwesomeProgressDialog setProgressBarColor(int color) {
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorWhite), PorterDuff.Mode.SRC_IN);
        return this;
    }




    @Override
    protected int getLayout() {
        return R.layout.dialog_progress;
    }


}
