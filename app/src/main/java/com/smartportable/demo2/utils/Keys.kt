package com.smartportable.demo2.utils

// Login Keys
const val keyId = "id"
const val keyUserId = "user_id"
const val keyStaffId = "staffid"
const val phone = "phone"
const val keyEmail = "email"
const val keyPassword = "password"
const val keyAuthToken = "Authorization"
const val keyExpirationTime = "token_expire"
const val keyCurrentTime = "current_time"
const val keyPhoto = "photo"
const val keyCanteenId = "CanteenId"
const val keyCanteenName = "CanteenName"
const val keyUserTypeId = "UserTypeId"
const val keyUserTypeName = "UserTypeName"
const val keyDeviceId = "device_id"
const val keyDeviceType = "device_type"
const val keyTotalRecord = "total_record"
const val keyRowIndex = "row_index"


// Base URLs
const val keyBaseUrl = "base_url"
const val keyAPIBaseUrl = "base_url_api"

// Student Info Param
const val keyStudentId = "StudentID"
const val keyFullName = "FullName"
const val keyFirstName = "FirstName"
const val keyFatherName = "FatherName"
const val keyGrandFatherName = "GrandFatherName"
const val keyGender = "Gender"
const val keyDateOfBirth = "DateOfBirth"
const val keySignature = "Signature"
const val keyCollege = "College"
const val keyDepartment = "Department"
const val keyCampus = "Campus"
const val keyProgram = "Program"
const val keyDegreeType = "DegreeType"
const val keyAdmissionType = "AdmissionType"
const val keyAdmissionTypeShort = "AdmissionTypeShort"
const val keyValidDateUntil = "ValidDateUntil"
const val keyIssueDate = "IssueDate"
const val keyMealNumber = "MealNumber"
const val keyUniqueNo = "Uniqueno"
const val keyStatus = "Status"
const val keyIsactive = "Isactive"

const val keyActive = "Active"
const val keyCardNo = "CardNo"

// Staff Info Param

const val keyStaffCode = "stacode"
const val keyStaffFullName = "FullNeng"
const val keyStaffSex = "sex"
const val keyStaffJobTitle = "jobtitle"
const val keyStaffDept = "deptname"
const val keyStaffCollege = "college"

// Door Info param
const val keyDoorId = "DoorId"
const val keyDoorName = "DoorName"

const val AUTH_TYPE = "auth_type"
const val READ_CARD_OFFLINE = 1
const val READ_CARD_NUMBER = 2
const val READ_BARCODE = 3
const val READ_CARD_BY_SEARCH = 4
const val READ_CARD_NUMBER_OFF = 5

// card status code
const val CARD_ST_ACTIVE = 1
const val CARD_ST_REVOKED = 2
const val CARD_ST_LOST = 3
const val CARD_ST_SUSPENDED = 4
const val CARD_ST_EXPIRED = 5


// visitor card status code
const val VIS_CARD_ST_ACTIVE = 1
const val VIS_CARD_ST_IN_USE = 2
const val VIS_CARD_ST_EXPIRED = 3
const val VIS_CARD_ST_LOST = 4


// API params
// Common param
const val API_STATUS = "Status"
const val API_MESSAGE = "Message"
const val API_PAGE_COUNT = "PageCount"
const val API_DATA = "Data"
const val API_TOKEN = "Token"
const val API_TOKEN_EXPIRE = "TokenExpiredTime"

// login FCM params
const val DEVICEID = "DeviceId"
const val DEVICETYPE = "DeviceType"
const val DEVICETYPE_VAL = "Android"

//Login param
const val API_LOGIN_ID = "Id"
const val API_LOGIN_USER_ID = "UserId"
const val API_LOGIN_EMAIL = "Email"
const val API_LOGIN_PASSWORD = "Password"
const val API_LOGIN_STAFFID = "StaffId"
const val API_LOGIN_FULLNAME = "FullName"
const val API_LOGIN_PHOTO = "StaffImage"
const val API_LOGIN_CANTEEN_ID = "CanteenId"
const val API_LOGIN_CANTEEN_NAME = "CanteenName"
const val API_LOGIN_USER_TYPE_ID = "UserTypeId"
const val API_LOGIN_USER_TYPE_NAME = "UserTypeName"