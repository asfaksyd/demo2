package com.smartportable.demo2.utils

import android.content.Context
import android.content.SharedPreferences
import com.smartportable.demo2.models.LoginModel

object SharedPref {

    private fun getSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
    }

    fun getStringValue(context: Context, key: String, default: String = ""): String {
        return getSharedPreferences(context).getString(key, default)
    }


    fun setStringValue(context: Context, key: String, newValue: String?) {
        val editor = getSharedPreferences(context).edit()
        editor.putString(key, newValue)
        editor.apply()
    }


    fun getIntValue(context: Context, key: String, default: Int = 0): Int {
        return getSharedPreferences(context).getInt(key, default)
    }

    fun setIntValue(context: Context, key: String, newValue: Int) {
        val editor = getSharedPreferences(context).edit()
        editor.putInt(key, newValue)
        editor.apply()
    }

    fun getBooleanValue(context: Context, key: String, default: Boolean = false): Boolean {
        return getSharedPreferences(context).getBoolean(key, default)
    }

    fun setBooleanValue(context: Context, key: String, newValue: Boolean) {
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean(key, newValue)
        editor.apply()
    }

    fun clear(context: Context) {
        val editor = getSharedPreferences(context).edit()
        editor.clear()
        editor.apply()
    }

    fun getAuthToken(context: Context): String = getSharedPreferences(context).getString(keyAuthToken, "")
    fun getUserId(context: Context): String = getSharedPreferences(context).getString(keyUserId, "")
    fun getCanteenId(context: Context): String = getSharedPreferences(context).getString(keyCanteenId, "")
    fun getCanteenName(context: Context): String = getSharedPreferences(context).getString(keyCanteenName, "")
    fun getUserTypeId(context: Context): String = getSharedPreferences(context).getString(keyUserTypeId, "")
    fun getUserTypeName(context: Context): String = getSharedPreferences(context).getString(keyUserTypeName, "")

    /*fun getStaffId(context: Context): String = getSharedPreferences(context).getString(keyStaffId, "")
    fun getFullName(context: Context): String = getSharedPreferences(context).getString(keyFullName, "")
    fun getPhoto(context: Context): String = getSharedPreferences(context).getString(keyPhoto, "")

    fun saveSyncCategoryStatus(context: Context, _boolean : Boolean) {
        //store all data in to string
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean(keySyncCategory, _boolean)
        editor.apply()
    }

    fun saveSyncNationalityStatus (context: Context, _boolean: Boolean) {
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean(keySyncNationility, _boolean)
        editor.apply()
    }

    fun saveSyncEducationLevelStatus(context: Context, _boolean : Boolean) {
        //store all data in to string
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean(keySyncEducationLevel, _boolean)
        editor.apply()
    }

    fun saveSyncTrainaingLangStatus(context: Context, _boolean : Boolean) {
        //store all data in to string
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean(keySyncTrainingLanguage, _boolean)
        editor.apply()
    }

    fun saveSyncTrainaingCenterStatus(context: Context, _boolean : Boolean) {
        //store all data in to string
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean(keySyncTraingingCenterCode, _boolean)
        editor.apply()
    }

    fun saveSyncRegianStatus(context: Context, _boolean : Boolean) {
        //store all data in to string
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean(keySyncRegion, _boolean)
        editor.apply()
    }

    fun saveSyncZoneStatus(context: Context, _boolean : Boolean) {
        //store all data in to string
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean(keySyncZone, _boolean)
        editor.apply()
    }



    fun saveSyncCityStatus(context: Context, _boolean : Boolean) {
        //store all data in to string
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean(keySyncCity, _boolean)
        editor.apply()
    }

    fun saveSyncSubCityStatus(context: Context, _boolean : Boolean) {
        //store all data in to string
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean(keySyncSubCity, _boolean)
        editor.apply()
    }




    fun saveSyncSubCategoryStatus(context: Context, _boolean : Boolean) {
        //store all data in to string
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean(keySyncSubCategory, _boolean)
        editor.apply()
    }*/



    fun saveLogin(context: Context, userData: LoginModel) {
        //store all data in to string
        val editor = getSharedPreferences(context).edit()

        editor.putString(keyId, userData.data!!.id.toString())
        editor.putString(keyUserId, userData.data!!.userId.toString())
        editor.putString(keyAuthToken, userData.data!!.token)
        editor.putString(keyExpirationTime, userData.data!!.tokenExpireTime)
        //editor.putString(keyCurrentTime, userData.data!!.currentDateTime)
        editor.putString(keyStaffId, userData.data!!.staffid)
        editor.putString(keyEmail, userData.data!!.email)
        editor.putString(keyPassword, userData.data!!.password)
        editor.putString(keyFullName, userData.data!!.fullName)
        editor.putString(keyPhoto, userData.data!!.photo)
        editor.putString(keyCanteenId, userData.data!!.canteenId)
        editor.putString(keyCanteenName, userData.data!!.canteenName)
        editor.putString(keyUserTypeId, userData.data!!.userTypeId)
        editor.putString(keyUserTypeName, userData.data!!.userTypeName)
        //editor.putString(keyDeviceId, userData.data!!.deviceId)
        //editor.putString(keyDeviceType, userData.data.deviceType)
        //editor.putString(keyTotalRecord, userData.data!!.totalRecords.toString())
        //editor.putString(keyRowIndex, userData.data.rowIndex.toString())


        editor.apply()
    }

    fun removeLogin(context: Context) {
        val editor = getSharedPreferences(context).edit()

        editor.remove(keyId)
        editor.remove(keyUserId)
        editor.remove(keyAuthToken)
        editor.remove(keyExpirationTime)
        /*editor.remove(keyCurrentTime)
        editor.remove(keyTotalRecord)
        editor.remove(keyRowIndex)*/
        editor.remove(keyStaffId)
        editor.remove(keyEmail)
        editor.remove(keyPassword)
        editor.remove(keyFullName)
        editor.remove(keyPhoto)
        editor.remove(keyCanteenId)
        editor.remove(keyCanteenName)
        editor.remove(keyUserTypeId)
        editor.remove(keyUserTypeName)

        editor.apply()
    }




}
