package com.androidscade.nfckotlindemo

import android.content.Context
import android.nfc.NfcAdapter
import android.nfc.NfcManager
import android.nfc.tech.MifareClassic
import android.preference.PreferenceManager
import com.smartportable.demo2.helper.PreferencesManager
import java.util.*


object NFCUtil {

    var READER_FLAGS = NfcAdapter.FLAG_READER_NFC_A or NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK
    var C5_MODEL_NAME = "C-five(WOS)"
    var MIFARE_AUTH_KEY = MifareClassic.KEY_DEFAULT
    var MIFARE_NDEF_KEY = MifareClassic.KEY_NFC_FORUM
    var MIFARE_MAD_KEY = MifareClassic.KEY_MIFARE_APPLICATION_DIRECTORY
    var SECTOR_COUNT_FOR_MIFARE = 1
    var BYTE_COUNT_FOR_MIFARE = 1
    var BYTE_LENGTH_FOR_MIFARE = 4
    var BYTE_LENGTH_FOR_MIFARE_TO_REVERSE = 3
    var BYTE_LENGTH_FOR_DESFIRE_TO_REVRSE = 6
    var CARD_TYPE_MIFARE = "Mifare"
    var CARD_TYPE_DESFIRE = "DESFire"

    var MSB = "MSB-First"
    var LSB = "LSB-First"

    fun isDeviceHaveNFC (context: Context) : Int {
        var nfcSt = 0 // NFC is not available
        var manager : NfcManager = context.getSystemService(Context.NFC_SERVICE) as NfcManager
        var adapter : NfcAdapter = manager.defaultAdapter
        if (adapter != null) {
            if(adapter.isEnabled()) {
                nfcSt = 1 // NFC is avaialble and ON
            } else {
                nfcSt = 2 // NFC is avaialble and OFF
            }
            //Yes NFC available
        }else{
            nfcSt = 0 // NFC is not avaialbel
            //Your device doesn't support NFC
        }
        return nfcSt
    }

    // function to verify the card number is alphanumeric
    fun checkAlphNumericString(str: String): Boolean {
        return str.matches("[A-Za-z0-9]+".toRegex())
    }

    /**
     * Utility class to convert a byte array to a hexadecimal string.
     *
     * @param bytes Bytes to convert
     * @return String, containing hexadecimal representation.
     */
    fun ByteArrayToHexString(bytes: ByteArray, typeStr: String, context: Context): String {
        val hexArray = charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')
        val hexChars = CharArray(BYTE_LENGTH_FOR_MIFARE * 2)
        var v: Int

        /*for (int j = 0; j < URLCollections.BYTE_LENGTH_FOR_MIFARE; j++) {
            //for ( int j = 3; j ==0 ; j-- ) {
            v = bytes[j] & 0xFF;
            hexChars[(URLCollections.BYTE_LENGTH_FOR_MIFARE_TO_REVERSE - j) * 2] = hexArray[v >>> 4];
            hexChars[(URLCollections.BYTE_LENGTH_FOR_MIFARE_TO_REVERSE - j) * 2 + 1] = hexArray[v & 0x0F];
        }*/

        var prefsMifare = PreferenceManager.getDefaultSharedPreferences(context);

        // do not forgot to pass default values as second arguments other wise it will return empty string and condition will not be satisfied
        val mifare_reading_type = prefsMifare.getString(PreferencesManager.KEY_SET_MIFARE_READING_TYPE, PreferencesManager.DEFAULT_MIFARE_READING_TYPE_LSB)
        if (mifare_reading_type != null) {
            if (mifare_reading_type!!.length != 0) {
                if (mifare_reading_type == LSB) {
                    for (j in 0 until BYTE_LENGTH_FOR_MIFARE) {
                        //for ( int j = 3; j ==0 ; j-- ) {
                        v = bytes[j].toInt() and 0xFF
                        hexChars[(BYTE_LENGTH_FOR_MIFARE_TO_REVERSE - j) * 2] = hexArray[v.ushr(4)]
                        hexChars[(BYTE_LENGTH_FOR_MIFARE_TO_REVERSE - j) * 2 + 1] = hexArray[v and 0x0F]
                    }
                } else {
                    for (j in 0 until BYTE_LENGTH_FOR_MIFARE) {
                        v = bytes[j].toInt() and 0xFF
                        hexChars[j * 2] = hexArray[v.ushr(4)]
                        hexChars[j * 2 + 1] = hexArray[v and 0x0F]
                    }
                }
            }
        }


        var str = String(hexChars)
        if (typeStr == CARD_TYPE_DESFIRE && str.length > 8) {
            str = str.substring(0, 7)
            return str
        } else {
            return str
        }
    }

    /**
     * Utility class to convert a byte array to a hexadecimal string.
     *
     * @param bytes Bytes to convert
     * @return String, containing hexadecimal representation.
     */
    fun ByteArrayToHexString(bytes: ByteArray): String {
        val hexArray = charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')
        val hexChars = CharArray(bytes.size * 2)
        var v: Int

        for (j in bytes.indices) {
            //for ( int j = 3; j ==0 ; j-- ) {
            v = bytes[j].toInt() and 0xFF

            hexChars[j * 2] = hexArray[v.ushr(4)]
            hexChars[j * 2 + 1] = hexArray[v and 0x0F]
        }
        return hexToString(String(hexChars))
    }

    /**
     * Utility class to convert a byte array to a hexadecimal string.
     *
     * @param bytes Bytes to convert
     * @return String, containing hexadecimal representation.
     */
    fun ByteArrayToHexStringDesfire(bytes: ByteArray, context: Context): String {
        System.out.println("Tag Id "+bytes.size)
        val hexArray = charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')
        val hexChars = CharArray(bytes.size * 2)
        var v: Int

        var prefsMifare = PreferenceManager.getDefaultSharedPreferences(context);

        // do not forgot to pass default values as second arguments other wise it will return empty string and condition will not be satisfied
        val mifare_reading_type = prefsMifare.getString(PreferencesManager.KEY_SET_MIFARE_READING_TYPE, PreferencesManager.DEFAULT_MIFARE_READING_TYPE_LSB)
        if (mifare_reading_type != null) {
            if (mifare_reading_type!!.length != 0) {
                if (mifare_reading_type == LSB) {
                    for (j in 0 until bytes.size) {
                        //for ( int j = 3; j ==0 ; j-- ) {
                        v = bytes[j].toInt() and 0xFF
                        hexChars[(BYTE_LENGTH_FOR_DESFIRE_TO_REVRSE - j) * 2] = hexArray[v.ushr(4)]
                        hexChars[(BYTE_LENGTH_FOR_DESFIRE_TO_REVRSE - j) * 2 + 1] = hexArray[v and 0x0F]
                    }
                } else {
                    for (j in bytes.indices) {
                        //for ( int j = 3; j ==0 ; j-- ) {
                        v = bytes[j].toInt() and 0xFF

                        hexChars[j * 2] = hexArray[v.ushr(4)]
                        hexChars[j * 2 + 1] = hexArray[v and 0x0F]
                    }
                }
            }
        }
        return String(hexChars)//hexToString(String(hexChars))
    }

    fun hexToString(hex: String): String {
        //hex.replace((char)0xFF, );
        val sb = StringBuilder()
        val hexData = hex.toCharArray()
        var count = 0
        while (count < hexData.size - 1) {
            val firstChar = hexData[count]
            val secondChar = hexData[count + 1]
            if (firstChar == 'F' && secondChar == 'F') {

            } else {
                val firstDigit = Character.digit(hexData[count], 16)
                val lastDigit = Character.digit(hexData[count + 1], 16)
                val decimal = firstDigit * 16 + lastDigit
                sb.append(decimal.toChar())
            }
            count += 2
        }
        return sb.toString()
    }

    fun trim(bytes: ByteArray): ByteArray {
        var i = bytes.size - 1
        while (i >= 0 && bytes[i].toInt() == 0) {
            --i
        }

        return Arrays.copyOf(bytes, i + 1)
    }

    fun mifareKeyAuthentication (mifareClassic: MifareClassic, sectorIndex: Int) :Boolean {
        if (!mifareClassic.authenticateSectorWithKeyA(sectorIndex, NFCUtil.MIFARE_AUTH_KEY)) {
            if (!mifareClassic.authenticateSectorWithKeyA(sectorIndex, NFCUtil.MIFARE_AUTH_KEY)) {
                return mifareClassic.authenticateSectorWithKeyA(sectorIndex, NFCUtil.MIFARE_NDEF_KEY)
            } else {
                return true
            }
        } else {
            return true
        }
    }

    fun hexToDec(hex: String) :Int {
        return Integer.parseInt(hex, 16)
    }

}