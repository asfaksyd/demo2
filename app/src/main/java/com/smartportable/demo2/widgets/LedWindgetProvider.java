package com.smartportable.demo2.widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.smartportable.demo2.R;

import fr.coppernic.cpcframework.cpcpowermgmt.cone.PowerMgmt;
import fr.coppernic.sdk.utils.helpers.CpcOs;

/**
 * Created by Ananth on 2/8/2017.
 */

public class LedWindgetProvider extends AppWidgetProvider {

    boolean led_st = false;
    int count = 1;
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        final int count = appWidgetIds.length;
        if (count > 0 && CpcOs.isCone()) {
            for (int i = 0; i < count; i++) {
                int widgetId = appWidgetIds[i];

                RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                        R.layout.widget_led);
                remoteViews.setTextViewText(R.id.tvTBTitle, "LED");

                if (led_st) {
                    //remoteViews.setInt(R.id.llView, "setBackgroundColor", android.R.color.holo_blue_dark);
                    //remoteViews.setImageViewResource(R.id.llView, android.R.color.holo_blue_dark);
                    remoteViews.setTextViewText(R.id.tbLed, "OFF");
                    remoteViews.setInt(R.id.llView, "setBackgroundResource", R.drawable.round_corner_led_bg);
                    //views.setImageViewResource(buttonId, getButtonImageId(true));
                    //remoteViews.setString(R.id.tbLed, "setText", "OFF");
                } else {
                    //remoteViews.setInt(R.id.llView, "setBackgroundColor", android.R.color.darker_gray);
                    //remoteViews.setImageViewResource(R.id.llView, android.R.color.darker_gray);

                    remoteViews.setTextViewText(R.id.tbLed, "ON");
                    remoteViews.setInt(R.id.llView, "setBackgroundResource", android.R.color.transparent);
                    //remoteViews.setString(R.id.tbLed, "setText", "ON");
                }

                Intent intent = new Intent(context, LedWindgetProvider.class);
                intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                        0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                remoteViews.setOnClickPendingIntent(R.id.tbLed, pendingIntent);
                appWidgetManager.updateAppWidget(widgetId, remoteViews);
            }
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        if (count == 1) {
            if (intent.getAction() == AppWidgetManager.ACTION_APPWIDGET_UPDATE) {
                count++;

                PowerMgmt mPowerMgmt = new PowerMgmt(context);
                // to ON LED1 blue fir cone eid
                led_st = mPowerMgmt.getPower(PowerMgmt.PeripheralTypesCone.Led, PowerMgmt.ManufacturersCone.BuiltIn, PowerMgmt.ModelsCone.Led1, PowerMgmt.InterfacesCone.ExpansionPort);
                if (led_st) {
                    led_st = false;
                    mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.Led, PowerMgmt.ManufacturersCone.BuiltIn, PowerMgmt.ModelsCone.Led1, PowerMgmt.InterfacesCone.ExpansionPort, false);
                } else {
                    led_st = true;
                    mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.Led, PowerMgmt.ManufacturersCone.BuiltIn, PowerMgmt.ModelsCone.Led1, PowerMgmt.InterfacesCone.ExpansionPort, true);
                }
                onUpdate(context);
            }
        }
    }

    private void onUpdate(Context context) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance
                (context);

        // Uses getClass().getName() rather than MyWidget.class.getName() for
        // portability into any App Widget Provider Class
        ComponentName thisAppWidgetComponentName =
                new ComponentName(context.getPackageName(),getClass().getName()
                );
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(
                thisAppWidgetComponentName);
        onUpdate(context, appWidgetManager, appWidgetIds);
    }
}
