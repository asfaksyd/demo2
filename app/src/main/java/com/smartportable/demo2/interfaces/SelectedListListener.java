package com.smartportable.demo2.interfaces;

import com.smartportable.demo2.helper.PunchLogs;
import com.smartportable.demo2.helper.RecyclerListSelectorModel;

import java.util.ArrayList;

/**
 * Created by Ananth on 1/29/2017.
 */

public interface SelectedListListener {
    void onListChanged(ArrayList<RecyclerListSelectorModel> list);
    void onPunchListChanged(ArrayList<PunchLogs> list);
    void onItemRemoved(String card_no);
}
