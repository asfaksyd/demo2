package com.smartportable.demo2.interfaces;

/**
 * Created by Ananth on 1/29/2017.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
