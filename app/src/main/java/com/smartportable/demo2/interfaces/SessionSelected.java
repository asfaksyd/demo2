package com.smartportable.demo2.interfaces;

/**
 * Created by Ananth on 3/29/2017.
 */

public interface SessionSelected {
    public void onSessionSelected();
}
