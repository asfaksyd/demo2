package com.smartportable.demo2.interfaces;

import android.app.Activity;

/**
 * Created by asfak on 8/21/2016.
 */
public interface FragmentChangeInterface {
    public void fragmentBecameVisible(Activity act);
}
