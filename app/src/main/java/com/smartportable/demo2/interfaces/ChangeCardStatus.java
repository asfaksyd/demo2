package com.smartportable.demo2.interfaces;

import android.content.Context;

public interface ChangeCardStatus {
    public void onChangeCardStatus(Context mContext, Integer cardSt);
    public void onCardHolderImageIntoPellete(String url);
}
