package com.smartportable.demo2.interfaces;

import java.util.ArrayList;

/**
 * Created by Ananth on 12/26/2016.
 */

public interface MultiChoiceListDialogInterface {
    public void onOkay(ArrayList<Integer> arrayList);
    public void onCancel();
}
