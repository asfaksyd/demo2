package com.smartportable.demo2;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.Areas;
import com.smartportable.demo2.helper.Categories;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by Ananth on 1/16/2017.
 */

public class FilterActivity extends AppCompatActivity {
    List<Categories> categoriesList = new ArrayList<Categories>();
    List<Areas> areasList = new ArrayList<Areas>();
    ArrayList<Integer> categoriesId = new ArrayList<Integer>();
    ArrayList<Integer> areasId = new ArrayList<Integer>();
    String[] categoriesListArray = new String[0];
    String[] areasListArray = new String[0];
    DatabaseHandler db;
    public static final String TAG = "FilterActivity";
    ListView lvCategories, lvAreas;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.smartportable.demo2.R.layout.activity_filter);
        ActionBar ab = getSupportActionBar();
        ab.setHomeButtonEnabled(true);

        lvCategories = (ListView) findViewById(com.smartportable.demo2.R.id.lvCategories);
        lvAreas = (ListView) findViewById(com.smartportable.demo2.R.id.lvAreas);

        lvCategories.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lvAreas.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        lvCategories.setBackgroundColor(getResources().getColor(android.R.color.white));
        lvAreas.setBackgroundColor(getResources().getColor(android.R.color.white));

        db = new DatabaseHandler(this);

        new GetCategories().execute();
        new GetAreas().execute();
        lvCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (categoriesList.get(position).getMusteringId() == -1) {
                    for (int i=1; i<categoriesList.size();i++) {
                        if (lvCategories.isItemChecked(position)) {
                            lvCategories.setItemChecked(i, true);
                        } else {
                            lvCategories.setItemChecked(i, false);
                        }
                    }
                } else {
                    if (lvCategories.isItemChecked(0))
                        lvCategories.setItemChecked(0, false);
                }
                invalidateOptionsMenu();
            }
        });
        lvAreas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (areasList.get(position).getSLN_Area_ID() == -1) {
                    for (int i=1; i<areasList.size();i++) {
                        if (lvAreas.isItemChecked(position)) {
                            lvAreas.setItemChecked(i, true);
                        } else {
                            lvAreas.setItemChecked(i, false);
                        }
                    }
                } else {
                    if (lvAreas.isItemChecked(0))
                        lvAreas.setItemChecked(0, false);
                }
                invalidateOptionsMenu();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.smartportable.demo2.R.menu.menu_filter, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == com.smartportable.demo2.R.id.action_filter_done) {
            int categories_total_count = lvCategories.getCheckedItemCount();
            Log.d(TAG, "Categories Total Count"+categories_total_count);

            int areas_total_count = lvAreas.getCheckedItemCount();
            Log.d(TAG, "Areas Total Count"+areas_total_count);
            if (categories_total_count > 0 && areas_total_count > 0) {
                // for categories
                if (categoriesId.size() > 0) {
                    categoriesId.clear();
                }
                // for areas
                if (areasId.size() > 0) {
                    areasId.clear();
                }
                // for the categories
                SparseBooleanArray itemCategoriesArray = lvCategories.getCheckedItemPositions();
                for (int k=0; k<categoriesList.size();k++) {
                    if (itemCategoriesArray.get(k)) {
                        int musteringID = categoriesList.get(k).getMusteringId();
                        Log.d(TAG, "MusteringID: " + musteringID);
                        categoriesId.add(musteringID);
                    }
                }
                // for the areas
                SparseBooleanArray itemAreasArray = lvAreas.getCheckedItemPositions();
                for (int k=0; k<areasList.size();k++) {
                    if (itemAreasArray.get(k)) {
                        int areaID = areasList.get(k).getSLN_Area_ID();
                        Log.d(TAG, "AreaID: " + areaID);
                        areasId.add(areaID);
                    }
                }
                // to clear the selection
                lvCategories.clearChoices();
                lvAreas.clearChoices();
                // to store values in application scope
                MyApplication.getInstance().setCetegoriesIdList(categoriesId);
                MyApplication.setAreaIdList(areasId);

                Intent i = new Intent();
                i.putIntegerArrayListExtra("CategoriesIdList", categoriesId);
                i.putIntegerArrayListExtra("AreasIdList", areasId);
                i.putExtra("FilterApplied", true);
                setResult(1, i);
                FilterActivity.this.finish();
                overridePendingTransition(com.smartportable.demo2.R.anim.stay, com.smartportable.demo2.R.anim.slide_out_up);
            }
            return true;
        }
        else if (item.getItemId() == android.R.id.home) {
            Intent i = new Intent();
            i.putExtra("FilterApplied", false);
            setResult(1, i);
            FilterActivity.this.finish();
            finish();
            overridePendingTransition(com.smartportable.demo2.R.anim.stay, com.smartportable.demo2.R.anim.slide_out_up);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        int categories_total_count = lvCategories.getCheckedItemCount();
        Log.d(TAG, "Categories Total Count"+categories_total_count);

        int areas_total_count = lvAreas.getCheckedItemCount();
        Log.d(TAG, "Areas Total Count"+areas_total_count);

        if (categories_total_count > 0 && areas_total_count > 0) {
            menu.findItem(com.smartportable.demo2.R.id.action_filter_done).setVisible(true);
        } else {
            menu.findItem(com.smartportable.demo2.R.id.action_filter_done).setVisible(false);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(com.smartportable.demo2.R.anim.stay, com.smartportable.demo2.R.anim.slide_out_up);
    }

    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when sessionListAdapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }

    public class GetCategories extends AsyncTask<Void, Void, Void> {
        boolean st = false;
        @Override
        protected Void doInBackground(Void... params) {
            categoriesList = db.getAllCategories();
            if (categoriesList.size() > 0) {
                categoriesListArray = new String[categoriesList.size()];
                for (int i=0; i<categoriesList.size();i++) {
                    Categories category = categoriesList.get(i);
                    int cat_id = categoriesList.get(i).getMusteringId();
                    Log.d(TAG, "CatId: "+cat_id);
                    String cat_name = categoriesList.get(i).getMusteringName();
                    Log.d(TAG, "CatName: "+cat_name);
                    categoriesListArray[i] = cat_name;
                    //j++;
                }
                st = true;
            } else {
                st = false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (st) {
                ArrayAdapter<String> adapter_categories = new ArrayAdapter<String>(FilterActivity.this, com.smartportable.demo2.R.layout.activity_filter_row, com.smartportable.demo2.R.id.tvFilterName, categoriesListArray);
                lvCategories.setEnabled(true);
                lvCategories.setAdapter(adapter_categories);

                ArrayList<Integer> catIdList = MyApplication.getInstance().getCetegoriesIdList();
                if (catIdList.size() > 0) {
                    for (int i = 0; i < catIdList.size(); i++) {
                        System.out.println("MyApp: " + catIdList.get(i));
                        for (int j = 0; j < categoriesList.size(); j++) {
                            System.out.println("MyApp MusteringId: " + categoriesList.get(j).getMusteringId());
                            if (categoriesList.get(j).getMusteringId() == catIdList.get(i)) {
                                lvCategories.setItemChecked(j, true);
                            } else {
                                //lvCategories.setItemChecked(i, false);
                            }
                        }
                    }
                } else { // that means there there are not any other value and defaul it will load for All categories and areas\
                    for (int j = 0; j < categoriesList.size(); j++) {
                        lvCategories.setItemChecked(j, true);
                    }
                }
                adapter_categories.notifyDataSetChanged();
            } else {
                Log.d(TAG, "Something went wrong!");
            }
        }
    }

    public class GetAreas extends AsyncTask<Void, Void, Void> {
        boolean st = false;
        @Override
        protected Void doInBackground(Void... params) {
            areasList = db.getAllAreas();
            if (areasList.size() > 0) {
                areasListArray = new String[areasList.size()];
                //areasListArray[0] = "All";
                for (int i=0; i<areasList.size();i++) {
                    Areas area = areasList.get(i);
                    int area_id = areasList.get(i).getSLN_Area_ID();
                    Log.d(TAG, "AreaId: "+area_id);
                    String area_name = areasList.get(i).getArea_Name();
                    Log.d(TAG, "AreaName: "+area_name);
                    areasListArray[i] = area_name;
                    //j++;
                }
                st = true;
            } else {
                st = false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (st) {
                ArrayAdapter<String> adapter_areas = new ArrayAdapter<String>(FilterActivity.this, com.smartportable.demo2.R.layout.activity_filter_row, com.smartportable.demo2.R.id.tvFilterName, areasListArray);
                lvAreas.setEnabled(true);
                lvAreas.setAdapter(adapter_areas);

                ArrayList<Integer> areaIdList = MyApplication.getAreaIdList();
                if (areaIdList.size() > 0) {
                    for (int i=0; i<areaIdList.size(); i++) {
                        for (int j=0; j< areasList.size();j++) {
                            if (areasList.get(j).getSLN_Area_ID() == areaIdList.get(i)) {
                                lvAreas.setItemChecked(j, true);
                            } else {
                                //lvAreas.setItemChecked(i, false);
                            }
                        }
                    }
                } else { // that means there there are not any other value and defaul it will load for All categories and areas\
                    for (int j=0; j< areasList.size();j++) {
                        lvAreas.setItemChecked(j, true);
                    }
                }
                adapter_areas.notifyDataSetChanged();
                ListUtils.setDynamicHeight(lvCategories);
                ListUtils.setDynamicHeight(lvAreas);

                invalidateOptionsMenu();
            } else {
                Log.d(TAG, "Something went wrong!");
            }
        }
    }
}
