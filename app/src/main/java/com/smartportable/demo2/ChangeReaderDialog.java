package com.smartportable.demo2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.smartportable.demo2.adapters.ReaderItemAdapter;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.PreferencesManager;
import com.smartportable.demo2.helper.PunchLogs;

import java.util.ArrayList;

/**
 * Created by Ananth on 2/1/2017.
 */

public class ChangeReaderDialog extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_set_reader_list);
        DatabaseHandler db = new DatabaseHandler(this);

        final LinearLayout llSetReaderList = (LinearLayout) findViewById(R.id.llSetReaderList);
        ListView lv = (ListView) findViewById(R.id.lvSetReader);
        //lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final ArrayList<PunchLogs> readerList = new ArrayList<PunchLogs>();// = db.getAllReaders();
        if (readerList.size() != 0) {
            int readerId = prefs.getInt(PreferencesManager.KEY_SET_READER_ID, -1);
            int indexOfStoreReader = -1;
            for (int i = 0; i < readerList.size(); i++) {
                //readerArray[i] = readerList.get(i).getReader_Name();
                if (readerId != -1 && readerId >= 0 && readerId == readerList.get(i).getSLN_Reader()) {
                    indexOfStoreReader = i;
                }
            }

            final String[] readerArray = new String[readerList.size()];
            for (int i = 0; i < readerList.size(); i++) {
                readerArray[i] = readerList.get(i).getReader_Name();
            }
            final ReaderItemAdapter adapter = null; //=new ReaderItemAdapter(ChangeReaderDialog.this, readerList, indexOfStoreReader);
            adapter.enableCheckBox(true);
            lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            lv.setAdapter(adapter);

            lv.setItemChecked(indexOfStoreReader, true);

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Log.d("Position", "item position: " + position);
                    PunchLogs pl = (PunchLogs) adapter.getItem(position);

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ChangeReaderDialog.this);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putInt(PreferencesManager.KEY_SET_READER_ID, pl.getSLN_Reader());
                    editor.putString(PreferencesManager.KEY_SET_READER_NAME, pl.getReader_Name());
                    editor.putInt(PreferencesManager.KEY_SET_MUSTER_ID, pl.getMusteringId());
                    editor.commit();

                    Intent i = new Intent();
                    i.putExtra("Selected_Reader", pl.getReader_Name());
                    setResult(2, i);
                    finish();
                }
            });
        } else {
            // after set area change the activity
            //startActivity(new Intent(ChangeReaderDialog.this, MainActivity.class));
            //overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
        }
    }
}
