package com.smartportable.demo2;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

/*import com.crashlytics.android.Crashlytics;*/
import com.fineapp.android.retrofitClient.RetrofitClientSingleton;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;
import com.smartportable.demo2.activities.CardAuthActivity;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.Boast;
import com.smartportable.demo2.helper.Config;
import com.smartportable.demo2.helper.PreferencesManager;
import com.smartportable.demo2.maingateaccess.MainGateAccessActivity;
import com.smartportable.demo2.maingateaccess.MainGateAccessDialog;
import com.smartportable.demo2.models.LoginModel;
import com.smartportable.demo2.models.ReaderInfoModel;
import com.smartportable.demo2.models.SessionsInfoModel;
import com.smartportable.demo2.prefs.MyPreferencesActivity;
import com.smartportable.demo2.utils.AppUtilsKt;
import com.smartportable.demo2.utils.KeysKt;
import com.smartportable.demo2.utils.SharedPref;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import fr.coppernic.sdk.utils.helpers.CpcOs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ananth on 3/14/2017.
 */

public class HomeScreen extends AppCompatActivity implements View.OnClickListener {
    LinearLayout llSync, llCardAuth, llMainGateAccess, llMemberAccess, llAboutApp;
    DatabaseHandler db;
    LinearLayout llHome;
    public static final int SET_GATE = 2;
    public static final int SET_READER = 3;
    public static String ACTION_STR = "action_str";
    public static String SET_URL = "set_url";
    BottomSheetBehavior behavior;
    TextView tvSetReaderName;
    Button btnClickMe,btn_start;
    ArrayList<ReaderInfoModel.DataBean> mListReader;
    ArrayList<SessionsInfoModel.DataBean> mListSession ;
    private static String TAG = HomeScreen.class.getCanonicalName();
    Spinner sp_reader, sp_session;
    EditText mEtStartTime ,mEtEndTime,etDueDate;
    private Calendar myCalendar;
    private DatePickerDialog.OnDateSetListener date;
    private TimePickerDialog.OnTimeSetListener start_time ,end_time;
    private String getDate = "";

    public void updateLocaleLanguage () {
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this);
            String lang = prefs.getString(PreferencesManager.KEY_CHANGE_LANG, getResources().getString(R.string.english_lang_label));

            String language_code = URLCollections.LANGUAGE_CODE_ENGLISH;
            if (lang.equals(getResources().getString(R.string.english_lang_label))) {
                language_code = URLCollections.LANGUAGE_CODE_ENGLISH;
            } else {
                language_code = URLCollections.LANGUAGE_CODE_ARABIC;
            }

            Resources res = getResources();
            // Change locale settings in the app.
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.setLocale(new Locale(language_code)); // API 17+ only.
            // Use conf.locale = new Locale(...) if targeting lower versions
            res.updateConfiguration(conf, dm);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //updateLocaleLanguage();

        setContentView(R.layout.activity_home_screen);

        initDateAndTime();
        //Crashlytics.getInstance().crash();

        // Bottom sheet dialog
        LinearLayout bottomSheetLayout = (LinearLayout) findViewById(R.id.linear_layout_bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheetLayout);
        tvSetReaderName = (TextView) bottomSheetLayout.findViewById(R.id.tvSetReaderName);
        //btnClickMe = (Button) bottomSheetLayout.findViewById(R.id.btnClickMe);

        behavior.setPeekHeight(0);
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    int from = -1; // 1 - NFC  && 2 - Non NFC
                    // after set area change the activity
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        int nfcStatus = URLCollections.isDeviceHaveNFC(HomeScreen.this);
                        if (CpcOs.isC5()) {
                            if (nfcStatus == 1 || nfcStatus == 2) {
                                startActivityForResult(new Intent(HomeScreen.this, NFCMemberAccessMainActivity.class), 1);
                            } else {
                                startActivityForResult(new Intent(HomeScreen.this, NFCMemberAccessMainActivity.class), 1);
                            }
                        } else {
                            if (nfcStatus == 1 || nfcStatus == 2) {
                                startActivityForResult(new Intent(HomeScreen.this, NFCMemberAccessMainActivity.class), 1);
                            } else {
                                startActivityForResult(new Intent(HomeScreen.this, NFCMemberAccessMainActivity.class), 1);
                            }
                        }
                    } else {
                        startActivityForResult(new Intent(HomeScreen.this, NFCMemberAccessMainActivity.class), 1);
                    }

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this);
                    SharedPreferences.Editor edit = prefs.edit();
                    if (from > 0) {
                        edit.putInt(PreferencesManager.KEY_WHICH_ACTIVITY, from);
                    }
                    edit.commit();

                    /*Intent intent_member_access = new Intent(HomeScreen.this, MemberAccessMainActivity.class);
                    startActivity(intent_member_access);*/
                    if (URLCollections.whichDirection(HomeScreen.this) == URLCollections.DEV_DIR_RTL) {
                        overridePendingTransition(R.anim.anim_slide_in_right,
                                R.anim.anim_slide_out_right);
                    } else {
                        overridePendingTransition(R.anim.anim_slide_in_left,
                                R.anim.anim_slide_out_left);
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        /*btnClickMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });*/

        db = new DatabaseHandler(this);

        llHome = (LinearLayout) findViewById(R.id.llHome);
        llSync = (LinearLayout) findViewById(R.id.llSync);
        llCardAuth = (LinearLayout) findViewById(R.id.llCardAuth);
        llMainGateAccess = (LinearLayout) findViewById(R.id.llMainGateAccess);
        llMemberAccess = (LinearLayout) findViewById(R.id.llMemberAccess);
        //llAboutApp = (LinearLayout) findViewById(R.id.llAboutApp);

        llSync.setOnClickListener(this);
        llCardAuth.setOnClickListener(this);
        llMainGateAccess.setOnClickListener(this);
        llMemberAccess.setOnClickListener(this);
        //llAboutApp.setOnClickListener(this);

        /*llSync.setOnTouchListener(mOnAlphaChangeLisener);
        llMustering.setOnTouchListener(mOnAlphaChangeLisener);
        llMemberAccess.setOnTouchListener(mOnAlphaChangeLisener);
        llAboutApp.setOnTouchListener(mOnAlphaChangeLisener);*/

    }

    /*public View.OnTouchListener mOnAlphaChangeLisener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Log.d("HomeScreen", "Touch Event Is Called "+event.getAction());
            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    v.setAlpha(0.2f);
                    break;
                case MotionEvent.ACTION_UP:
                    v.setAlpha(1f);
                    break;
                default :
                    v.setAlpha(1f);
                    break;
            }
            return false;
        }
    };*/

    /*public void obtainMotionEventObject (View view) {
        long downTime = System.currentTimeMillis();
        long eventTime = System.currentTimeMillis() + 100;
        float x = 0.0f;
        float y = 0.0f;
        // List of meta states found here:     developer.android.com/reference/android/view/KeyEvent.html#getMetaState()
        int metaState = 0;
        MotionEvent motionEvent = MotionEvent.obtain(
                downTime,
                eventTime,
                MotionEvent.ACTION_UP,
                x,
                y,
                metaState
        );

        // Dispatch touch event to view
        mOnAlphaChangeLisener.onTouch(view, motionEvent);
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.llSync:
                //obtainMotionEventObject(llSync);
                Intent intent_sync = new Intent(this, SyncActivity.class);
                startActivity(intent_sync);
                if (URLCollections.whichDirection(HomeScreen.this) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                }
                break;

            case R.id.llCardAuth:

                Intent intent_student_auth = new Intent(this, CardAuthActivity.class);
                startActivity(intent_student_auth);
                if (URLCollections.whichDirection(HomeScreen.this) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                }
                break;

            case R.id.llMainGateAccess:
                if (db.checkSession() && db.checkAGInfo() && db.checkStudentInfo() && db.checkAccessList()) {
                    Intent i = new Intent(HomeScreen.this, MainGateAccessDialog.class);
                    startActivityForResult(i, SET_GATE);
                    /*if (URLCollections.whichDirection(HomeScreen.this) == URLCollections.DEV_DIR_RTL) {
                        overridePendingTransition(R.anim.anim_slide_in_right,
                                R.anim.anim_slide_out_right);
                    } else {
                        overridePendingTransition(R.anim.anim_slide_in_left,
                                R.anim.anim_slide_out_left);
                    }*/
                } else {
                    String msg = getResources().getString(R.string.require_sync);

                    Snackbar snackbar = Snackbar
                            .make(llHome, msg, Snackbar.LENGTH_LONG);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();
                }

                break;

            case R.id.llMemberAccess:
                //displaySessionDialog();
                //obtainMotionEventObject(llMemberAccess);
                //if (db.checkReaders() && db.checkCategories() && db.checkAreas() && db.checkEmployees() && db.checkMemberData()) {
                Log.d(TAG, "AG ST : "+db.checkAGInfo() +" Session ST: "+ db.checkSession() +" Studen St: "+ db.checkStudentInfo() +" Acc List : "+ db.checkAccessList());
                if (db.checkAGInfo() && db.checkSession() && db.checkStudentInfo() && db.checkAccessList()) {
                    //displaySessionDialog();
                    startActivityForResult(new Intent(HomeScreen.this, SetReader.class), SET_READER);
                } else {
                    String msg = getResources().getString(R.string.require_sync);
                    Snackbar snackbar = Snackbar.make(llHome, msg, Snackbar.LENGTH_LONG);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();
                }

                break;

            /*case R.id.llAboutApp:
                //obtainMotionEventObject(llAboutApp);
                Intent intent_about_app = new Intent(this, AboutApp.class);
                startActivity(intent_about_app);
                if (URLCollections.whichDirection(HomeScreen.this) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                }
                break;*/

            default:
                break;
        }
    }

    public View.OnClickListener mOnSBAddSessionClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(HomeScreen.this, SessionsActivity.class));
        }
    };
    // double time backpressed
    boolean doubleBackToExitPressedOnce = false;
    private Handler mHandler = new Handler();

    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (mHandler != null) { mHandler.removeCallbacks(mRunnable); }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Boast.makeText(this, getResources().getString(R.string.back_again), Toast.LENGTH_SHORT).show();

        mHandler.postDelayed(mRunnable, 2000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "Request Code:"+requestCode + "Result Code "+resultCode);
        if (requestCode == SET_READER && resultCode == SET_READER) {
            String readerName = data.getExtras().getString("Selected_Reader");
            String sessionName = data.getExtras().getString("Selected_Session");
            Snackbar snackbar = Snackbar.make(llHome, getApplicationContext().getResources().getString(R.string.set_reader, readerName), Snackbar.LENGTH_LONG);
            //.setAction(getResources().getString(R.string.snack_action_refresh), mOnSBAddSessionClick);
            //snackbar.setActionTextColor(Color.WHITE);


            // set BSD and set its height
            tvSetReaderName.setText(getApplicationContext().getResources().getString(R.string.set_reader, readerName));
            behavior.setPeekHeight(150);
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        behavior.setPeekHeight(0);
                                        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                                    }
                                }, 1500);

            View snackbarView = snackbar.getView();
            snackbarView.setBackgroundColor(Color.DKGRAY);
            TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            //snackbar.show();

            snackbar.addCallback(new Snackbar.Callback() {
                @Override
                public void onDismissed(Snackbar snackbar, int event) {
                    super.onDismissed(snackbar, event);

                    int from = -1; // 1 - NFC  && 2 - Non NFC
                    // after set area change the activity
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        int nfcStatus = URLCollections.isDeviceHaveNFC(HomeScreen.this);
                        if (CpcOs.isC5()) {
                            if (nfcStatus == 1 || nfcStatus == 2) {
                                startActivityForResult(new Intent(HomeScreen.this, NFCMemberAccessMainActivity.class), 1);
                            } else {
                                startActivityForResult(new Intent(HomeScreen.this, NFCMemberAccessMainActivity.class), 1);
                            }
                        } else {
                            if (nfcStatus == 1 || nfcStatus == 2) {
                                startActivityForResult(new Intent(HomeScreen.this, NFCMemberAccessMainActivity.class), 1);
                            } else {
                                startActivityForResult(new Intent(HomeScreen.this, NFCMemberAccessMainActivity.class), 1);
                            }
                        }
                    } else {
                        startActivityForResult(new Intent(HomeScreen.this, NFCMemberAccessMainActivity.class), 1);
                    }

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this);
                    SharedPreferences.Editor edit = prefs.edit();
                    if (from > 0) {
                        edit.putInt(PreferencesManager.KEY_WHICH_ACTIVITY, from);
                    }
                    edit.commit();

                    /*Intent intent_member_access = new Intent(HomeScreen.this, MemberAccessMainActivity.class);
                    startActivity(intent_member_access);*/
                    if (URLCollections.whichDirection(HomeScreen.this) == URLCollections.DEV_DIR_RTL) {
                        overridePendingTransition(R.anim.anim_slide_in_right,
                                R.anim.anim_slide_out_right);
                    } else {
                        overridePendingTransition(R.anim.anim_slide_in_left,
                                R.anim.anim_slide_out_left);
                    }
                }

                @Override
                public void onShown(Snackbar sb) {
                    super.onShown(sb);
                }
            });
        } else if (requestCode == SET_GATE && resultCode == SET_GATE) {
            String readerName = data.getExtras().getString("Selected_Gate");
            int punchType = data.getExtras().getInt("Selected_Punch_Type");
            String punch_type = "";
            if (punchType == 1) {
                punch_type = getResources().getString(R.string.punch_in_label);
            } else {
                punch_type = getResources().getString(R.string.punch_out_label);
            }
            Snackbar snackbar = Snackbar
                    .make(llHome, getApplicationContext().getResources().getString(R.string.set_gate, readerName, punch_type), Snackbar.LENGTH_LONG);
            //.setAction(getResources().getString(R.string.snack_action_refresh), mOnSBAddSessionClick);
            //snackbar.setActionTextColor(Color.WHITE);
            View snackbarView = snackbar.getView();
            snackbarView.setBackgroundColor(Color.DKGRAY);
            TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();

            snackbar.addCallback(new Snackbar.Callback() {
                @Override
                public void onDismissed(Snackbar snackbar, int event) {
                    super.onDismissed(snackbar, event);

                    // after set area change the activity
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        startActivity(new Intent(HomeScreen.this, MainGateAccessActivity.class));
                        overridePendingTransition(R.anim.anim_slide_in_left,
                                R.anim.anim_slide_out_left);
                    }
                }

                @Override
                public void onShown(Snackbar sb) {
                    super.onShown(sb);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_home, menu);
        //MenuItem mitem = menu.findItem(R.id.action_category);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        // for setting screen
        if (id == R.id.action_settings) {
            startActivity(new Intent(HomeScreen.this, MyPreferencesActivity.class));
        }

        // for service setting
        if (id == R.id.action_service) {
            startActivity(new Intent(HomeScreen.this, ServiceSetting.class));
        }

        // for debug database
        if (id == R.id.action_debug_db) {
            if (BuildConfig.DEBUG) {
                try {
                    Class<?> debugDB = Class.forName(Config.DEBUG_DB_PACKAGE);
                    Method getAddressLog = debugDB.getMethod("getAddressLog");
                    Object value = getAddressLog.invoke(null);
                    Boast.makeText(getApplicationContext(), (String) value, Toast.LENGTH_LONG).show();
                } catch (Exception ignore) {
                    ignore.printStackTrace();
                }
            }

        }

        if (id == R.id.action_logout) {
            if(AppUtilsKt.isNetworkAvailable(this)) {
                getUserLogout(SharedPref.INSTANCE.getUserId(HomeScreen.this));
            } else {
                AppUtilsKt.toast(this, R.string.no_conn);
            }
        }

        return super.onOptionsItemSelected(item);
    }
    AlertDialog access_alert;
    AlertDialog.Builder access_dialog;

    // Display the dialog for the UnSync Dialog
    public void DisplayWarningForUnSyncedDialog () {
        if (access_dialog == null) {
            access_dialog = new AlertDialog.Builder(HomeScreen.this);
        }else {
            access_alert.dismiss();
            access_dialog = new AlertDialog.Builder(HomeScreen.this);
        }

        LayoutInflater inflater = LayoutInflater.from(HomeScreen.this);
        View dialogLayout = inflater.inflate(R.layout.dialog_old_mem_acc_logs_exists, null);
        access_dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        TextView tvWarningMessage = (TextView) dialogLayout.findViewById(R.id.warningMsg);
        tvWarningMessage.setText(getApplicationContext().getResources().getString(R.string.mem_acc_not_sync_logs_warning));

        access_dialog.setPositiveButton(getString(R.string.okay_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });


        access_alert = access_dialog.create();
        access_alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                DatabaseHandler dbh = new DatabaseHandler(HomeScreen.this);
                int oldEntriesCount = dbh.isOldSyncedMemAccLogsExist();
                if (oldEntriesCount > 0) {
                    Log.d("HomeScreen", "Old Entries Exists");
                    DisplayOldEntriesExistWarningDialog(oldEntriesCount);
                } else {
                    startActivityForResult(new Intent(HomeScreen.this, SetReader.class), HomeScreen.SET_READER);
                }
            }
        });
        access_alert.setCancelable(false);
        access_alert.show();

    }

    public void DisplayOldEntriesExistWarningDialog (final int totalRecords) {
        if (access_dialog == null) {
            access_dialog = new AlertDialog.Builder(HomeScreen.this);
        }else {
            access_alert.dismiss();
            access_dialog = new AlertDialog.Builder(HomeScreen.this);
        }

        LayoutInflater inflater = LayoutInflater.from(HomeScreen.this);
        View dialogLayout = inflater.inflate(R.layout.dialog_old_mem_acc_logs_exists, null);
        access_dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        TextView tvAGLabel = (TextView) dialogLayout.findViewById(R.id.tvAGLabel);

        access_dialog.setPositiveButton(getString(R.string.yes_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d("HomeScreen", "Proceed for deleting");
                new DeleteOldmemAccLogs(totalRecords).execute();
            }
        });
        access_dialog.setNegativeButton(getString(R.string.no_label), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                startActivityForResult(new Intent(HomeScreen.this, SetReader.class), HomeScreen.SET_READER);
            }
        });

        //access_dialog.setOnDismissListener(onOldLogsDialogDismiss);

        access_alert = access_dialog.create();
        access_alert.show();
    }

    public DialogInterface.OnDismissListener onOldLogsDialogDismiss = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
            //refreshFragment();

        }
    };

    public class DeleteOldmemAccLogs extends AsyncTask<Void, Void, Void> {
        ProgressDialog pDialogDelete;
        DatabaseHandler dbh;
        int totalRecords;

        public DeleteOldmemAccLogs(int totalRecords) {
            this.totalRecords = totalRecords;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialogDelete = new ProgressDialog(HomeScreen.this, R.style.MyTheme);
            pDialogDelete.setCancelable(false);
            pDialogDelete.show();

            dbh = new DatabaseHandler(HomeScreen.this);

        }

        @Override
        protected Void doInBackground(Void... params) {

            dbh.deleteOldMemAccLogs();
            // to save the logs of deleted entries
            String str = getString(R.string.total_deleted_records_label, ""+totalRecords) + ", ";
            dbh.saveLogsForDeletedMemAccLogs(HomeScreen.this, str);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pDialogDelete.isShowing())
                pDialogDelete.dismiss();
            startActivityForResult(new Intent(HomeScreen.this, SetReader.class), HomeScreen.SET_READER);
        }
    }

    public void getUserLogout(String userId) {
        AppUtilsKt.showProgressDialog(this);

        JsonObject jObj = new JsonObject();
        jObj.addProperty(KeysKt.API_TOKEN, SharedPref.INSTANCE.getAuthToken(getApplicationContext()));
        JsonObject jData = new JsonObject();
        /*if (TextUtils.isDigitsOnly(strEmailId)) {
            jData.addProperty("Staffid", strEmailId); //email
        } else {                                            // OR
            jData.addProperty("Email", strEmailId); // staff ID
        }*/
        jData.addProperty("UserId", userId.toString()); // staff ID

        //jData.addProperty("Password", strPassword);

        jObj.add("data", jData);
        System.out.println("Data"+ jObj.toString());

        String baseUrl = SharedPref.INSTANCE.getStringValue(HomeScreen.this , KeysKt.keyAPIBaseUrl,  "");

        RetrofitClientSingleton.Companion.getInstance(baseUrl).getUserLogout(jObj)
            .enqueue(new Callback<LoginModel>() {
                @Override
                public void onFailure(Call<LoginModel> call, Throwable t) {
                    AppUtilsKt.stopProgress();
                    AppUtilsKt.toast(HomeScreen.this, getString(R.string.something_went_wrong));
                }

                @Override
                public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                    AppUtilsKt.stopProgress();
                    if (response.isSuccessful()) {
                        LoginModel userDataResponse =  response.body();
                        //System.out.println(TAG+ "Login Response: Id "+userDataResponse.getData().getId());
                        if (userDataResponse.getStatus() == 1) {
                            //SharedPref.saveLogin(this@AuthActivity, userDataResponse!!)
                            AppUtilsKt.toast(HomeScreen.this, userDataResponse.getMessage());

                            SharedPref.INSTANCE.removeLogin(HomeScreen.this);

                            HomeScreen.this.deleteDatabase(db.DATABASE_NAME);
                            Intent intent = new Intent(HomeScreen.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        } else if (userDataResponse.getStatus() == 2 || userDataResponse.getStatus() == 3) {
                            AppUtilsKt.toast(HomeScreen.this, userDataResponse.getMessage());
                        }


                    } else {
                        AppUtilsKt.toast(HomeScreen.this, R.string.something_went_wrong);
                    }
                }
            });

    }

    @Override
    protected void onResume() {
        super.onResume();
        /*try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this);
            boolean lang_has_change = prefs.getBoolean(PreferencesManager.KEY_LANGUAGE_HAS_CHANGE, false);
            if (lang_has_change) {
                // change the status like app language has change by the user
                SharedPreferences.Editor edit = prefs.edit();
                edit.putBoolean(PreferencesManager.KEY_LANGUAGE_HAS_CHANGE, false);
                edit.commit();

                Intent intent = getIntent();
                finish();
                startActivity(intent);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void displaySessionDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_session_start);
        sp_reader = (Spinner) dialog.findViewById(R.id.sp_reader);
        sp_session = (Spinner) dialog.findViewById(R.id.sp_session);
        mEtStartTime = (EditText)dialog.findViewById(R.id.mEtStartTime);
        etDueDate = (EditText)dialog.findViewById(R.id.etDueDate);
        mEtEndTime = (EditText)dialog.findViewById(R.id.mEtEndTime);
        btn_start = (Button)dialog.findViewById(R.id.btn_start);

        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(HomeScreen.this);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putString(PreferencesManager.KEY_SESSION_START_TIME,mEtStartTime.getText().toString() );
                edit.putString(PreferencesManager.KEY_SESSION_END_TIME, mEtEndTime.getText().toString());
                edit.putString(PreferencesManager.KEY_SESSION_DUE_DATE, etDueDate.getText().toString());
                edit.putString(PreferencesManager.KEY_SESSION_READER_ID, sp_reader.getSelectedItem().toString());
                edit.putString(PreferencesManager.KEY_SESSION_ID, sp_session.getSelectedItem().toString());

                edit.commit();

                Boast.makeText(HomeScreen.this, "Session started!", Toast.LENGTH_LONG).show();
                dialog.dismiss();

                startActivity(new Intent(HomeScreen.this, NFCMemberAccessMainActivity.class));
                if (URLCollections.whichDirection(HomeScreen.this) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                }

            }
        });

        etDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(HomeScreen.this, date, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });

        mEtStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(HomeScreen.this, start_time,
                        myCalendar.get(Calendar.HOUR_OF_DAY),
                        myCalendar.get(Calendar.MINUTE), false);
                timePickerDialog.show();
            }
        });

        mEtEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(HomeScreen.this, end_time,
                        myCalendar.get(Calendar.HOUR_OF_DAY),
                        myCalendar.get(Calendar.MINUTE), false);
                timePickerDialog.show();

            }
        });
        dialog.setCancelable(true);

        mListReader = db.getAllReaders();
        mListSession = db.getAllSessions();
        ArrayList<String> mName = new ArrayList<>();
        ArrayList<String> mSession = new ArrayList<>();
        Log.e(TAG, "displaySessionDialog >> "+mName.size());

        for (int i = 0; i < mListReader.size(); i++) {
            mName.add(mListReader.get(i).getReaderName());
        }

        for (int i = 0; i < mListSession.size(); i++) {
            mSession.add(mListSession.get(i).getSessionName());
        }

        ArrayAdapter<String> mAdapterReader =
                new ArrayAdapter<String>(this, R.layout.selected_item, mName);
        sp_reader.setAdapter(mAdapterReader);

        ArrayAdapter<String> mAdapterSession =
                new ArrayAdapter<String>(this, R.layout.selected_item, mSession);
        sp_session.setAdapter(mAdapterSession);
        dialog.show();


    }

    private void initDateAndTime() {

        myCalendar = Calendar.getInstance();

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabelDate();
            }

        };

        start_time = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                myCalendar.set(Calendar.MINUTE, minute);
                updateLabelTime();
            }
        };
        end_time = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                myCalendar.set(Calendar.MINUTE, minute);
                updateLabelTime2();
            }
        };

    }

    private void updateLabelTime() {
        String myFormat2 = "HH:mm";
        SimpleDateFormat sdf2 = new SimpleDateFormat(myFormat2, Locale.US);
        mEtStartTime.setText(sdf2.format(myCalendar.getTime()));
    }
    private void updateLabelTime2() {
        String myFormat2 = "HH:mm";
        SimpleDateFormat sdf2 = new SimpleDateFormat(myFormat2, Locale.US);
        mEtEndTime.setText(sdf2.format(myCalendar.getTime()));
    }
    private void updateLabelDate() {

        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        getDate = sdf.format(myCalendar.getTime());

//        String myFormat2 = "EEE, d MMM yyyy";
//        SimpleDateFormat sdf2 = new SimpleDateFormat(myFormat2, Locale.US);
        etDueDate.setText(getDate);
    }


}
