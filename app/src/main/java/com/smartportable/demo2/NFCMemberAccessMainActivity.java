package com.smartportable.demo2;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.fragments.AccessedMembersList;
import com.smartportable.demo2.fragments.AllowedMembersList;
import com.smartportable.demo2.fragments.DeniedMembersList;
import com.smartportable.demo2.fragments.PendingMembersList;
import com.smartportable.demo2.helper.Boast;
import com.smartportable.demo2.helper.Config;
import com.smartportable.demo2.helper.CustomViewPager;
import com.smartportable.demo2.fragments.MemAccLogDetailFragment;
import com.smartportable.demo2.helper.MemberAccessLogs;
import com.smartportable.demo2.helper.PreferencesManager;
import com.smartportable.demo2.interfaces.FragmentChangeInterface;
import com.smartportable.demo2.receiver.SendPunchDataServiceBroadcastReceiver;
import com.smartportable.demo2.utils.AppUtilsKt;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import fr.coppernic.sdk.utils.debug.Log;
import fr.coppernic.sdk.utils.helpers.CpcApp;
import fr.coppernic.sdk.utils.helpers.CpcDate;
import fr.coppernic.sdk.utils.helpers.CpcOs;
import fr.coppernic.sdk.utils.helpers.CpcUsb;

/**
 * Created by Ananth on 3/19/2017.
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
public class NFCMemberAccessMainActivity extends AppCompatActivity implements NfcAdapter.ReaderCallback {

    FragmentManager fm;
    FragmentTransaction ft;
    FrameLayout frameContainerMemAcc;
    Fragment currentFragment;

    public String TAG = this.getClass().getCanonicalName();
    RelativeLayout rlAllMemberAccess, rlAllowedMemberAccess;
    RelativeLayout viewAllMember, viewAllowMember;

    Toolbar toolbar;
    CustomViewPager viewPager;
    ViewPagerAdapter adapter;
    TabLayout tabLayout;
    SharedPreferences prefsMifare;
    String cardData = null; // for the Mifare card from NFC reader
    Snackbar snackbarNFC;
    CoordinatorLayout clMainFragmentActivity;
    TextView tvMemAccReaderName, tvMemAccSessionName;
    AlertDialog.Builder access_dialog;
    AlertDialog access_alert;
    private AlarmManager alarmMgr;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_member_access_main);

        clMainFragmentActivity = (CoordinatorLayout) findViewById(R.id.clMainFragmentActivity);
        tvMemAccReaderName = (TextView) findViewById(R.id.tvMemAccReaderName);
        tvMemAccSessionName = (TextView) findViewById(R.id.tvMemAccSessionName);

        Log.d(TAG, "onCreate");
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            try {
                ViewConfiguration config = ViewConfiguration.get(this);
                Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
                if(menuKeyField != null) {
                    menuKeyField.setAccessible(true);
                    menuKeyField.setBoolean(config, false);
                }
            } catch (Exception ex) {
                // Ignore
            }
        }
        //toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        prefsMifare = PreferenceManager.getDefaultSharedPreferences(this);

        viewPager = (CustomViewPager) findViewById(com.smartportable.demo2.R.id.viewpager);
        setupViewPager(viewPager);
        viewPager.beginFakeDrag();

        tabLayout = (TabLayout) findViewById(com.smartportable.demo2.R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorHeight((int) (4 * getResources().getDisplayMetrics().density));

        viewPager.setOffscreenPageLimit(1);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                FragmentChangeInterface inteface = (FragmentChangeInterface) adapter.instantiateItem(viewPager, position);
                if (inteface != null) {
                    inteface.fragmentBecameVisible(NFCMemberAccessMainActivity.this);
                }

                // to change the tab indicator color
                if (position == 0) {
                    tabLayout.setSelectedTabIndicatorColor(getApplicationContext().getResources().getColor(R.color.color_cyan));
                } else if (position == 1) {
                    tabLayout.setSelectedTabIndicatorColor(getApplicationContext().getResources().getColor(R.color.color_green));
                } else if (position == 2) {
                    tabLayout.setSelectedTabIndicatorColor(getApplicationContext().getResources().getColor(android.R.color.holo_red_dark));
                } else if (position == 3) {
                    tabLayout.setSelectedTabIndicatorColor(getApplicationContext().getResources().getColor(android.R.color.darker_gray));
                }


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // set values from share preferences
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NFCMemberAccessMainActivity.this);
        String readerName = prefsMifare.getString(PreferencesManager.KEY_SET_READER_NAME, "");
        String sessionName = prefsMifare.getString(PreferencesManager.KEY_SET_SESSION_NAME, "");
        Log.d(TAG, " name: "+readerName);
        tvMemAccReaderName.setText(getResources().getString(R.string.access_point_label, readerName));
        tvMemAccSessionName.setText(getResources().getString(R.string.session_label, sessionName));
        // to start service
        alarmMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Log.d(TAG, "Is C-Five "+CpcOs.isC5());
        Log.d(TAG, "Is C-Five Model "+CpcOs.MODEL_CONE_OLD1);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int nfcStatus = URLCollections.isDeviceHaveNFC(this);
            if (CpcOs.isC5() && !CpcOs.isCone()) {
                //configureLedWidget();
                if (nfcStatus == 1) {
                    enableReaderMode();
                } else if (nfcStatus == 2) {
                    //Boast.makeText(this, "Please ON your NFC from settings", Toast.LENGTH_LONG).show();
                    snackbarNFC = Snackbar
                            .make(clMainFragmentActivity, getResources().getString(com.smartportable.demo2.R.string.nfc_on_msg), Snackbar.LENGTH_INDEFINITE);
                    snackbarNFC.setAction(getResources().getString(com.smartportable.demo2.R.string.nfc_on_action), mOnSnackBarONClickListener);
                    snackbarNFC.setActionTextColor(Color.GREEN);
                    View snackbarView = snackbarNFC.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbarNFC.show();
                } else {
                    //Boast.makeText(this, "Your device doesn't have NFC", Toast.LENGTH_LONG).show();
                    snackbarNFC = Snackbar
                            .make(clMainFragmentActivity, getResources().getString(com.smartportable.demo2.R.string.nfc_not_available), Snackbar.LENGTH_LONG);
                    //snackbarNFC.setAction(getResources().getString(R.string.nfc_on_action), mOnSnackBarONClickListener);
                    //snackbarNFC.setActionTextColor(Color.GREEN);
                    View snackbarView = snackbarNFC.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbarNFC.show();
                }
                /*Intent intent = new Intent(this, ScanCardServiceBroadcastReceiver.class);
                alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
                alarmMgr.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), Config.SCAN_CARD_SERVICE_INTERVAL, alarmIntent);*/
            } else {
                Log.d(TAG, "Other device");
                if (nfcStatus == 1) {
                    enableReaderMode();
                } else if (nfcStatus == 2) {
                    //Boast.makeText(this, "Please ON your NFC from settings", Toast.LENGTH_LONG).show();
                    snackbarNFC = Snackbar
                            .make(clMainFragmentActivity, getResources().getString(com.smartportable.demo2.R.string.nfc_on_msg), Snackbar.LENGTH_INDEFINITE);
                    snackbarNFC.setAction(getResources().getString(com.smartportable.demo2.R.string.nfc_on_action), mOnSnackBarONClickListener);
                    snackbarNFC.setActionTextColor(Color.GREEN);
                    View snackbarView = snackbarNFC.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbarNFC.show();
                } else {
                    //Boast.makeText(this, "Your device doesn't have NFC", Toast.LENGTH_LONG).show();
                    snackbarNFC = Snackbar
                            .make(clMainFragmentActivity, getResources().getString(com.smartportable.demo2.R.string.nfc_not_available), Snackbar.LENGTH_LONG);
                    //snackbarNFC.setAction(getResources().getString(R.string.nfc_on_action), mOnSnackBarONClickListener);
                    //snackbarNFC.setActionTextColor(Color.GREEN);
                    View snackbarView = snackbarNFC.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbarNFC.show();
                }
            }
        }

        Intent intent_send_punch = new Intent(this, SendPunchDataServiceBroadcastReceiver.class);
        PendingIntent piSendPunchData = PendingIntent.getBroadcast(this, 0, intent_send_punch, 0);
        alarmMgr.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), Config.SEND_PUNCH_DATA_SERVICE_INTERVAL, piSendPunchData);

        /*frameContainerMemAcc = (FrameLayout) findViewById(R.id.frameContainerMemAcc);

        rlAllMemberAccess = (RelativeLayout) findViewById(R.id.rlAllMembersAccess);
        rlAllowedMemberAccess = (RelativeLayout) findViewById(R.id.rlAllowedMemberAccess);
        viewAllMember = (RelativeLayout) findViewById(R.id.viewAllMember);
        viewAllowMember = (RelativeLayout) findViewById(R.id.viewAllowMember);

        rlAllMemberAccess.setOnClickListener(this);
        rlAllowedMemberAccess.setOnClickListener(this);

        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();

        //rlAllMemberAccess.setBackgroundColor(getResources().getColor(R.color.color_button_press_bg));
        //rlAllowedMemberAccess.setBackgroundColor(getResources().getColor(R.color.color_button_bg));
        viewAllMember.setBackgroundColor(getResources().getColor(android.R.color.white));
        viewAllowMember.setBackgroundColor(getResources().getColor(R.color.color_button_bg));
        removeAllFragments();
        currentFragment = new AllowedMembersList();
        ft.replace(R.id.frameContainerMemAcc, currentFragment);
        ft.addToBackStack(null);
        ft.commit();*/
        //TimeZone.setDefault(TimeZone.getTimeZone(AppUtilsKt.getCurrentTimezoneOffset()));

        doPunchEntries(NFCMemberAccessMainActivity.this, "66119DD3");

    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new AllowedMembersList(), getResources().getString(com.smartportable.demo2.R.string.label_allowed));
        adapter.addFrag(new AccessedMembersList(), getResources().getString(com.smartportable.demo2.R.string.label_accessed));
        adapter.addFrag(new DeniedMembersList(), getResources().getString(com.smartportable.demo2.R.string.label_denied));
        adapter.addFrag(new PendingMembersList(), getResources().getString(com.smartportable.demo2.R.string.label_pending));

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private Fragment mCurrentFragment;

        public Fragment getCurrentFragment() {
            return mCurrentFragment;
        }

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            if (getCurrentFragment() != object) {
                mCurrentFragment = ((Fragment) object);
            }
            super.setPrimaryItem(container, position, object);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("onResume isC5: "+ CpcOs.isC5());
        if (CpcOs.isC5()) {
            enableReaderMode();

            int nfcst = URLCollections.isDeviceHaveNFC(NFCMemberAccessMainActivity.this);
            if (nfcst == 1) {

            } else if (nfcst == 2) {
                snackbarNFC = Snackbar
                        .make(clMainFragmentActivity, getResources().getString(com.smartportable.demo2.R.string.nfc_on_msg), Snackbar.LENGTH_INDEFINITE);
                snackbarNFC.setAction(getResources().getString(com.smartportable.demo2.R.string.nfc_on_action), mOnSnackBarONClickListener);
                snackbarNFC.setActionTextColor(Color.GREEN);
                View snackbarView = snackbarNFC.getView();
                snackbarView.setBackgroundColor(Color.DKGRAY);
                TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbarNFC.show();
            } else {
                snackbarNFC = Snackbar
                        .make(clMainFragmentActivity, getResources().getString(com.smartportable.demo2.R.string.nfc_not_available), Snackbar.LENGTH_INDEFINITE);
                //snackbarNFC.setAction(getResources().getString(R.string.nfc_on_action), mOnSnackBarONClickListener);
                //snackbarNFC.setActionTextColor(Color.GREEN);
                View snackbarView = snackbarNFC.getView();
                snackbarView.setBackgroundColor(Color.DKGRAY);
                TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbarNFC.show();
            }
        }

        // To refresh the data if it has synced with background service Name: SyncDataService
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean loc_st = prefs.getBoolean(PreferencesManager.KEY_LOCATION_SYNCED, false);
        if (loc_st) {
            // update the location sync flag as page refreshed
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean(PreferencesManager.KEY_LOCATION_SYNCED, false);
            edit.commit();

            android.util.Log.d(TAG, "Synced");
            ArrayList<Integer> catIdList = MyApplication.getInstance().cetegoriesIdList;
            ArrayList<Integer> areaIdList = MyApplication.getInstance().getAreaIdList();
            //new NFCMemberAccessMainActivity.GetEmployeeLocationOnFilter(getApplicationContext()).execute(catIdList, areaIdList);
        } else {
            Log.d(TAG, "Not synced");
        }
    }

    // Enable NFC reader mode
    private void enableReaderMode() {
        android.util.Log.i(TAG, "Enabling reader mode");
        NfcAdapter nfc = NfcAdapter.getDefaultAdapter(this);
        if (nfc != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                nfc.enableReaderMode(this, this, URLCollections.READER_FLAGS, null);
            }
        }
    }

    public View.OnClickListener mOnSnackBarONClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            android.util.Log.d(TAG, "SnackBar On Click");
            Intent intent = new Intent(Settings.ACTION_NFC_SETTINGS);
            startActivityForResult(intent, 11);
        }
    };

    @Override
    public void onBackPressed() {

        /*int backCount = fm.getBackStackEntryCount();
        Log.d(TAG, "BackStack Count"+backCount);
        if (backCount > 1) {
            fm.popBackStack();
        } else {
            if (backCount == 1)
                fm.popBackStack();

            super.onBackPressed();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }*/
        NFCMemberAccessMainActivity.this.finish();
        if(URLCollections.whichDirection(NFCMemberAccessMainActivity.this) == URLCollections.DEV_DIR_RTL) {
            overridePendingTransition(com.smartportable.demo2.R.anim.anim_slide_in_left,
                    com.smartportable.demo2.R.anim.anim_slide_out_left);
        } else {
            overridePendingTransition(com.smartportable.demo2.R.anim.anim_slide_in_right,
                    com.smartportable.demo2.R.anim.anim_slide_out_right);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            //setResult(SyncActivity.REQUEST_SYNC_EMPLOYEES);
            NFCMemberAccessMainActivity.this.finish();
            if(URLCollections.whichDirection(NFCMemberAccessMainActivity.this) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(com.smartportable.demo2.R.anim.anim_slide_in_left,
                        com.smartportable.demo2.R.anim.anim_slide_out_left);
            } else {
                overridePendingTransition(com.smartportable.demo2.R.anim.anim_slide_in_right,
                        com.smartportable.demo2.R.anim.anim_slide_out_right);
            }
            return true;
        }
        if (item.getItemId() == com.smartportable.demo2.R.id.action_session) {
            startActivity(new Intent(this, SessionsActivity.class));
            if (URLCollections.whichDirection(NFCMemberAccessMainActivity.this) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(com.smartportable.demo2.R.anim.anim_slide_in_right, com.smartportable.demo2.R.anim.anim_slide_out_right);
            } else {
                overridePendingTransition(com.smartportable.demo2.R.anim.anim_slide_in_left,
                        com.smartportable.demo2.R.anim.anim_slide_out_left);
            }
            return true;
        }
        if (item.getItemId() == com.smartportable.demo2.R.id.action_set_reader) {
            DatabaseHandler dbh = new DatabaseHandler(this);
            if (dbh.checkMemAccLogs()) {
                DisplayWarningForUnSyncedDialog();
            } else {
                int oldEntriesCount = dbh.isOldSyncedMemAccLogsExist();
                if (oldEntriesCount > 0) {
                    Log.d(TAG, "Old Entries Exists");
                    DisplayOldEntriesExistWarningDialog(oldEntriesCount);
                } else {
                    startActivityForResult(new Intent(NFCMemberAccessMainActivity.this, SetReader.class), HomeScreen.SET_READER);
                }
            }

            //overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
            return true;
        }

        if (item.getItemId() == com.smartportable.demo2.R.id.action_read_logs) {
            DatabaseHandler dbh = new DatabaseHandler(NFCMemberAccessMainActivity.this);
            String logs = dbh.readLogsFile(NFCMemberAccessMainActivity.this);
            Log.d(TAG, "Read File Data: "+logs);
            if (logs.length() > 0) {
                DisplayDeleteLogsFileInDialog(logs);
            } else {
                Boast.makeText(NFCMemberAccessMainActivity.this, getString(com.smartportable.demo2.R.string.no_logs_available), Toast.LENGTH_SHORT).show();
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.smartportable.demo2.R.menu.menu_member_access, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /*@Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.rlAllMembersAccess:
                if ((currentFragment instanceof AllowedMembersList)) {
                    //rlAllMemberAccess.setBackgroundColor(getResources().getColor(R.color.color_button_press_bg));
                    //rlAllowedMemberAccess.setBackgroundColor(getResources().getColor(R.color.color_button_bg));

                    viewAllMember.setBackgroundColor(getResources().getColor(android.R.color.white));
                    viewAllowMember.setBackgroundColor(getResources().getColor(R.color.color_button_bg));
                    removeAllFragments();
                    currentFragment = new AllowedMembersList();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.frameContainerMemAcc, currentFragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
                break;

            case R.id.rlAllowedMemberAccess:
                if ((currentFragment instanceof AllowedMembersList)) {
                    //rlAllMemberAccess.setBackgroundColor(getResources().getColor(R.color.color_button_bg));
                    //rlAllowedMemberAccess.setBackgroundColor(getResources().getColor(R.color.color_button_press_bg));

                    viewAllMember.setBackgroundColor(getResources().getColor(R.color.color_button_bg));
                    viewAllowMember.setBackgroundColor(getResources().getColor(android.R.color.white));
                    removeAllFragments();
                    currentFragment = new AllowedMembersList();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.frameContainerMemAcc, currentFragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
                break;

            default :

                break;
        }
    }*/


    public void removeAllFragments () {
        int count = fm.getBackStackEntryCount();
        if (count > 0) {
            for (int i = 0; i < count; i++) {
                fm.popBackStack();
            }
        }
    }

    @Override
    public void onTagDiscovered(Tag tag) {
        Log.d(TAG, "New tag discovered"+tag);
        byte[] id = tag.getId();
        android.util.Log.d(TAG, "TAG ID:" +id);
        int total_length = id.length-1;
        Log.d("TAG", "tag "+id+"tag length"+total_length);

        // Read card first with tag based on authentication key
        final MifareClassic mifareClassic = MifareClassic.get(tag);
        final IsoDep isoDep = IsoDep.get(tag);
        android.util.Log.d(TAG, "Mifare CLassic : " + mifareClassic);
        if (mifareClassic != null) {
            try {
                mifareClassic.connect();
                Log.d(TAG, "Transceive Length: " + mifareClassic.getMaxTransceiveLength());
                Log.d(TAG, "Selector count: " + mifareClassic.getSectorCount());
                int blockCount = mifareClassic.getBlockCount();
                Log.d(TAG, "SectorToBlock: " + mifareClassic.sectorToBlock(blockCount));

                boolean auth = false;
                // 5.2) and get the number of sectors this card has..and loop thru these sectors
                int secCount = mifareClassic.getSectorCount();
                int bCount = 0;
                int bIndex = 0;
                for (int j = 0; j < URLCollections.SECTOR_COUNT_FOR_MIFARE; j++) {
                    // 6.1) authenticate the sector
                    auth = mifareClassic.authenticateSectorWithKeyA(j, URLCollections.MIFARE_AUTH_KEY);
                    if (auth) {
                        // 6.2) In each sector - get the block count
                        bCount = mifareClassic.getBlockCountInSector(j);
                        bIndex = 0;
                        for (int i = 0; i < URLCollections.BYTE_COUNT_FOR_MIFARE; i++) {
                            bIndex = mifareClassic.sectorToBlock(j);
                            // 6.3) Read the block
                            byte[] data = mifareClassic.readBlock(bIndex);
                            // 7) Convert the data into a string from Hex format.
                            cardData = ByteArrayToHexString(data, Config.MIFARE);
                            android.util.Log.d(TAG, cardData);//, data.length));
                            bIndex++;
                        }
                    } else { // Authentication failed - Handle it using directly reading TAG
                        android.util.Log.d(TAG, "Authentication Failed!");
                        cardData = ByteArrayToHexString(id, Config.MIFARE);

                    }
                }
                if (cardData != null && cardData.length() > 0 && checkAlphNumericString(cardData)) {
                    android.util.Log.d(TAG, "Card Number: "+cardData);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            /*if (cardData.equals("A53809BD"))
                                cardData = "41054";*/

                            doPunchEntries(NFCMemberAccessMainActivity.this, cardData);
                        }
                    });

                }else {
                    android.util.Log.d(TAG, "CardNumber null");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if (isoDep != null) {
            android.util.Log.d(TAG, "Mifare DESFire Card");
            cardData = ByteArrayToHexString(id, Config.DESFIRE);
            android.util.Log.d(TAG, "Card Number: "+cardData);

            if (cardData != null && cardData.length() > 0 && checkAlphNumericString(cardData)) {
                android.util.Log.d(TAG, "Card Number: "+cardData);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                            /*if (cardData.equals("A53809BD"))
                                cardData = "41054";*/

                        doPunchEntries(NFCMemberAccessMainActivity.this, cardData);
                    }
                });

            }else {
                android.util.Log.d(TAG, "CardNumber null");
            }
        }
    }

    /**
     * Utility class to convert a byte array to a hexadecimal string.
     *
     * @param bytes Bytes to convert
     * @return String, containing hexadecimal representation.
     */
    public String ByteArrayToHexString(byte[] bytes, String typeStr) {
        final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[URLCollections.BYTE_LENGTH_FOR_MIFARE * 2];
        int v;

        // do not forgot to pass default values as second arguments other wise it will return empty string and condition will not be satisfied
        String mifare_reading_type = prefsMifare.getString(PreferencesManager.KEY_SET_MIFARE_READING_TYPE, PreferencesManager.DEFAULT_MIFARE_READING_TYPE_LSB);
        if (mifare_reading_type != null) {
            if (mifare_reading_type.length() != 0 ) {
                if (mifare_reading_type.equals(Config.LSB)) {
                    for (int j = 0; j < URLCollections.BYTE_LENGTH_FOR_MIFARE; j++) {
                        //for ( int j = 3; j ==0 ; j-- ) {
                        v = bytes[j] & 0xFF;
                        hexChars[(URLCollections.BYTE_LENGTH_FOR_MIFARE_TO_REVERSE - j) * 2] = hexArray[v >>> 4];
                        hexChars[(URLCollections.BYTE_LENGTH_FOR_MIFARE_TO_REVERSE - j) * 2 + 1] = hexArray[v & 0x0F];
                    }
                } else {
                    for (int j = 0; j < URLCollections.BYTE_LENGTH_FOR_MIFARE; j++) {
                        v = bytes[j] & 0xFF;
                        hexChars[j * 2] = hexArray[v >>> 4];
                        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
                    }
                }
            }
        }

        String str = new String(hexChars);
        if (typeStr.equals(Config.DESFIRE) && str.length() > 8) {
            str = str.substring(0, 7);
            return str;
        } else {
            return str;
        }
    }

    private boolean checkAlphNumericString(String str) {
        return str.matches("[A-Za-z0-9]+");
    }

    public void doPunchEntries(Context mContext, String cardNumber) {
        DatabaseHandler db = new DatabaseHandler(mContext);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        // content values for PunchLogs entries
        /*TimeZone.setDefault(null);
        System.setProperty("user.timezone", "");
        TimeZone.setDefault(null);*/
        //TimeZone.setDefault(TimeZone.getTimeZone(AppUtilsKt.getCurrentTimezoneOffset()));
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => "+c.getTime());
        LocalDateTime today = LocalDateTime.now();
        TimeZone tz = TimeZone.getDefault();
        Log.d("TImeZone", tz.getDisplayName());
        String today1 = CpcDate.getCurrentDateTime("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        //Log.d("Datetime Zone", ""+today.toString("yyyy-MM-dd HH:mm:ss"));
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        df.setTimeZone(TimeZone.getTimeZone(TimeZone.getDefault().getID()));
        String punchDateTime = df.format(new Date());//c.getTime());
        //LocalDateTime today2 = LocalDateTime.now(DateTimeZone.forTimeZone(TimeZone.getTimeZone("GMT+5.30")));
        System.out.println(TAG+ "Date Diff Punch Date "+ TimeZone.getDefault().getID() +" "+punchDateTime);
        int readerId = prefs.getInt(PreferencesManager.KEY_SET_READER_ID, -1);
        int sessionId = prefs.getInt(PreferencesManager.KEY_SET_SESSION_ID, -1);
        long sessionDate = prefs.getLong(PreferencesManager.KEY_SET_SESSION_DATE, -1);
        long sessionStartTime = prefs.getLong(PreferencesManager.KEY_SET_SESSION_START_TIME, -1);
        long sessionEndTime = prefs.getLong(PreferencesManager.KEY_SET_SESSION_END_TIME, -1);
        String readerName = prefs.getString(PreferencesManager.KEY_SET_READER_NAME, "");
        String sessionName = prefs.getString(PreferencesManager.KEY_SET_SESSION_NAME, "");
        int musterId = prefs.getInt(PreferencesManager.KEY_SET_MUSTER_ID, -1);

        int empId = db.getEmployeeIdFromCardNumber(cardNumber);
        String stuId = db.getStudentIdFromCardNumber(cardNumber);
        String stuName = db.getStudentNameFromCardNumber(cardNumber);

        // to get sesstion start //yyyy-MM-dd HH:mm:ss
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd", Config.DATETIME_FORMAT_LOCAL);
        //sdfDate.setTimeZone(TimeZone.getTimeZone("UTC"));
        String session_date = sdfDate.format(sessionDate);

        /*TimeZone.setDefault(null);
        System.setProperty("user.timezone", "");
        TimeZone.setDefault(null);*/

        SimpleDateFormat sdfStartTime = new SimpleDateFormat("HH:mm:ss");//, Locale.US);
        //sdfStartTime.setTimeZone(TimeZone.getTimeZone("UTC"));
        String session_start_time = sdfStartTime.format(sessionStartTime);
        String session_end_time = sdfStartTime.format(sessionEndTime);

        int fromDateDiff = punchDateTime.compareTo(session_date+" "+session_start_time);
        int toDateDiff = punchDateTime.compareTo(session_date+" "+session_end_time);
        System.out.println("Date Diff"+ "startDate "+session_date+" "+session_start_time+" DIFF "+fromDateDiff+" EndDate "+session_date+" "+session_end_time+" DIFF " + toDateDiff);
        // check whether the flash card is allow for these session time interval or not
        if (fromDateDiff >= 0 && toDateDiff <= 0) {
            Log.d(TAG, "Card punch time is correct");

            int access_status = db.checkCardHavingDoorAccess(readerId, sessionId, cardNumber, session_date+" "+session_start_time, session_date+" "+session_end_time);
            // 0 - Can access / Allowed
            // 1 - Already accessed / Not allowed
            // 2 -
            if (access_status > -1) {
                //Boast.makeText(this, "Having door access", Toast.LENGTH_SHORT).show();

                MemberAccessLogs memacc = new MemberAccessLogs();

                if (empId != -1 && empId > -1) {
                    memacc.setMemAccLogsEmpId(empId);
                } else {
                    memacc.setMemAccLogsEmpId(empId);
                }

                if (stuId != null) {
                    if (stuId.length() != 0) {
                        memacc.setMemAccLogsStuId(stuId);
                    } else {
                        memacc.setMemAccLogsStuId("");
                    }
                }

                if (stuName != null) {
                    if (stuName.length() != 0) {
                        memacc.setMemAccLogsEmpName(stuName);
                    } else {
                        memacc.setMemAccLogsEmpName("Unknown");
                    }
                }



                if (cardNumber != null) {
                    if (cardNumber.length() != 0)
                        memacc.setMemAccLogsCardNo(cardNumber);
                }
                if (readerId != -1) {
                    memacc.setMemAccLogsReaderId(readerId);
                    memacc.setMemAccLogsReaderName(readerName);
                }
                if (sessionId != -1) {
                    memacc.setMemAccLogsSessionId(sessionId);
                    memacc.setMemAccLogsSessionName(sessionName);
                }
                memacc.setMemAccLogsPunchDate(punchDateTime);
                memacc.setMemAccLogsIsSync(0);

                /*if (access_status == 0) {
                    memacc.setMemAccLogsAccessGrant(0);
                } else {
                    memacc.setMemAccLogsAccessGrant(1);
                }*/
                memacc.setMemAccLogsAccessGrant(access_status);
                //memacc.setMemAccLogsAreaId(-1);
                if (musterId >= 0) {
                    //memacc.setMemAccLogsMusteringId(musterId);
                } else {
                    //memacc.setMemAccLogsMusteringId(-1);
                }

                if (access_status == 0) {
                    Log.d(TAG, "First entry");
                    showAccessGrantedDialog(memacc);
                } else if (access_status == 2) {
                    Log.d(TAG, "Duplicate Entries");
                    showAccessGrantedDialog(memacc);
                }

                db.saveMemAccPunchLogsData(memacc);

            } else {
                if (access_status == -1) {
                    access_status = 1;
                }
                //Boast.makeText(this, "Not having door access!!!", Toast.LENGTH_SHORT).show();
                MemberAccessLogs memacc = new MemberAccessLogs();

                if (empId != -1 && empId > -1) {
                    memacc.setMemAccLogsEmpId(empId);
                } else {
                    memacc.setMemAccLogsEmpId(empId);
                }

                if (stuId != null) {
                    if (stuId.length() != 0) {
                        memacc.setMemAccLogsStuId(stuId);
                    }
                }

                if (stuName != null) {
                    if (stuName.length() != 0) {
                        memacc.setMemAccLogsEmpName(stuName);
                    }
                }

                if (cardNumber != null) {
                    if (cardNumber.length() != 0)
                        memacc.setMemAccLogsCardNo(cardNumber);
                }
                if (readerId != -1) {
                    memacc.setMemAccLogsReaderId(readerId);
                    memacc.setMemAccLogsReaderName(readerName);
                }
                if (sessionId != -1) {
                    memacc.setMemAccLogsSessionId(sessionId);
                    memacc.setMemAccLogsSessionName(sessionName);
                }
                memacc.setMemAccLogsPunchDate(punchDateTime);
                memacc.setMemAccLogsIsSync(0);

                /*if (access_status == 0) {
                    memacc.setMemAccLogsAccessGrant(0);
                } else {
                    memacc.setMemAccLogsAccessGrant(1);
                }*/
                memacc.setMemAccLogsAccessGrant(access_status);
                //memacc.setMemAccLogsAreaId(-1);
                if (musterId >= 0) {
                    //memacc.setMemAccLogsMusteringId(musterId);
                } else {
                    //memacc.setMemAccLogsMusteringId(-1);
                }

                Log.d(TAG, "No Door Access Entries");
                showAccessGrantedDialog(memacc);

                //db.saveMemAccPunchLogsData(memacc);
                //refreshFragment();
            }
        } else {
            Boast.makeText(this, "Session is not valid to flash card", Toast.LENGTH_SHORT).show();

            /*MemberAccessLogs memacc = new MemberAccessLogs();

            if (empId != -1 && empId > -1) {
                memacc.setMemAccLogsEmpId(empId);
            }
            if (cardNumber != null) {
                if (cardNumber.length() != 0)
                    memacc.setMemAccLogsCardNo(cardNumber);
            }
            if (readerId != -1)
                memacc.setMemAccLogsReaderId(readerId);
            if (sessionId != -1)
                memacc.setMemAccLogsSessionId(sessionId);
            memacc.setMemAccLogsPunchDate(punchDateTime);
            memacc.setMemAccLogsIsSync(0);

            memacc.setMemAccLogsAccessGrant(1); // not having door accessed for this time interval
            //memacc.setMemAccLogsAreaId(-1);
            if (musterId >= 0) {
                //memacc.setMemAccLogsMusteringId(musterId);
            } else {
                //memacc.setMemAccLogsMusteringId(-1);
            }

            db.saveMemAccPunchLogsData(memacc);

            refreshFragment();*/
        }
    }

    public void refreshFragment() {
        Fragment fragment = adapter.getCurrentFragment();
        if (fragment instanceof AllowedMembersList) {
            ((AllowedMembersList) fragment).fragmentBecameVisible(NFCMemberAccessMainActivity.this);
        } else if (fragment instanceof AccessedMembersList) {
            ((AccessedMembersList) fragment).fragmentBecameVisible(NFCMemberAccessMainActivity.this);
        } else if (fragment instanceof DeniedMembersList) {
            ((DeniedMembersList) fragment).fragmentBecameVisible(NFCMemberAccessMainActivity.this);
        } else if (fragment instanceof PendingMembersList) {
            ((PendingMembersList) fragment).fragmentBecameVisible(NFCMemberAccessMainActivity.this);
        }
    }

    public BottomSheetDialogFragment bottomSheetDialogFragment = null;
    public void showAccessGrantedDialog (MemberAccessLogs memacclogs) {

        // check - dismiss fragment readersAdapter BSD
        if (adapter != null) {
            Fragment fragment = adapter.getCurrentFragment();
            if (fragment instanceof AllowedMembersList) {
                if (((AllowedMembersList) fragment).adapter != null) {
                    /*if (((AllowedMembersList) fragment).adapter.bottomSheetDialogFragment != null) {
                        ((AllowedMembersList) fragment).adapter.bottomSheetDialogFragment.dismiss();
                    }*/
                }
            } else if (fragment instanceof AccessedMembersList) {
                if (((AccessedMembersList) fragment).adapter != null) {
                    if (((AccessedMembersList) fragment).adapter.bottomSheetDialogFragment != null) {
                        ((AccessedMembersList) fragment).adapter.bottomSheetDialogFragment.dismiss();
                    }
                }
            } else if (fragment instanceof DeniedMembersList) {
                if (((DeniedMembersList) fragment).adapter != null) {
                    if (((DeniedMembersList) fragment).adapter.bottomSheetDialogFragment != null) {
                        ((DeniedMembersList) fragment).adapter.bottomSheetDialogFragment.dismiss();
                    }
                }
            } else if (fragment instanceof PendingMembersList) {
                if (((PendingMembersList) fragment).adapter != null) {
                    /*if (((PendingMembersList) fragment).adapter.bottomSheetDialogFragment != null) {
                        ((PendingMembersList) fragment).adapter.bottomSheetDialogFragment.dismiss();
                    }*/
                }
            }
        }
        // check - dismiss and create BSD for scan card
        if (bottomSheetDialogFragment == null) {
            bottomSheetDialogFragment = new MemAccLogDetailFragment();
        } else {
            bottomSheetDialogFragment.dismiss();
            bottomSheetDialogFragment = new MemAccLogDetailFragment();
        }

        ((MemAccLogDetailFragment)bottomSheetDialogFragment).setMemAccData(memacclogs);
        ((MemAccLogDetailFragment)bottomSheetDialogFragment).isForMemberAccess("");
        ((MemAccLogDetailFragment)bottomSheetDialogFragment).isFromScan(true);
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());

        /*if (access_dialog == null) {
            access_dialog = new AlertDialog.Builder(NFCMemberAccessMainActivity.this);
        }else {
            access_alert.dismiss();
            access_dialog = new AlertDialog.Builder(NFCMemberAccessMainActivity.this);
        }

        LayoutInflater inflater = LayoutInflater.from(NFCMemberAccessMainActivity.this);
        View dialogLayout = inflater.inflate(com.irizid.mess.R.layout.dialog_access_granted, null);
        access_dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ImageView ivAGEmpPhoto = (ImageView) dialogLayout.findViewById(com.irizid.mess.R.id.ivAGEmpPhoto);
        FloatingActionButton fabAG = (FloatingActionButton) dialogLayout.findViewById(com.irizid.mess.R.id.fabAG);
        TextView tvAGLabel = (TextView) dialogLayout.findViewById(com.irizid.mess.R.id.tvAGLabel);
        TextView tvAGEmpName = (TextView) dialogLayout.findViewById(com.irizid.mess.R.id.tvAGEmpName);
        TextView tvAGEmpCardNo = (TextView) dialogLayout.findViewById(com.irizid.mess.R.id.tvAGEmpCardNo);
        TextView tvAGPunchTime = (TextView) dialogLayout.findViewById(com.irizid.mess.R.id.tvAGPunchTime);
        TextView tvAGReaderName = (TextView) dialogLayout.findViewById(com.irizid.mess.R.id.tvAGReaderName);
        TextView tvAGSessionName = (TextView) dialogLayout.findViewById(com.irizid.mess.R.id.tvAGSessionName);

        int access_grant = memacclogs.getMemAccLogsAccessGrant();
        if (access_grant == 0) {
            fabAG.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_green_dark)));
            fabAG.setImageResource(com.irizid.mess.R.mipmap.ic_action_done);
            tvAGLabel.setText(Html.fromHtml(getResources().getString(com.irizid.mess.R.string.access_grant_msg)));
            tvAGLabel.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
            ivAGEmpPhoto.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
        } else if (access_grant == 1) {
            fabAG.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_red_dark)));
            fabAG.setImageResource(com.irizid.mess.R.mipmap.ic_close_app);
            tvAGLabel.setText(Html.fromHtml(getResources().getString(com.irizid.mess.R.string.access_deny_msg)+getString(com.irizid.mess.R.string.access_deny_no_door_access)));
            tvAGLabel.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            ivAGEmpPhoto.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
        } else if (access_grant == 2) {
            fabAG.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_red_dark)));
            fabAG.setImageResource(com.irizid.mess.R.mipmap.ic_close_app);
            tvAGLabel.setText(Html.fromHtml(getResources().getString(com.irizid.mess.R.string.access_deny_msg)+getString(com.irizid.mess.R.string.access_deny_duplicate_access)));
            tvAGLabel.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            ivAGEmpPhoto.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
        }

        int emp_id = memacclogs.getMemAccLogsEmpId();
        String emp_photo ="";
        if (emp_id != -1) {
            DatabaseHandler db = new DatabaseHandler(NFCMemberAccessMainActivity.this);
            emp_photo = db.getImagePathFromEmpId(NFCMemberAccessMainActivity.this, emp_id);
            if (db.isEmployeeAlreadyExists(db.getWritableDatabase(), emp_id)) {
                Employee emp = db.getEmployeeFromEmpId(emp_id);
                if (emp != null) {
                    tvAGEmpName.setText(emp.getSLN_Employee() + " - " + emp.getEmployeeName());
                } else {
                    tvAGEmpName.setText(getResources().getString(com.irizid.mess.R.string.unknown_str));
                }
            }
        } else {
            tvAGEmpName.setText(getResources().getString(com.irizid.mess.R.string.unknown_str));
        }

        String punchTime = memacclogs.getMemAccLogsPunchDate();
        tvAGPunchTime.setVisibility(View.GONE);
        if (punchTime.length() > 0) {
            tvAGPunchTime.setVisibility(View.VISIBLE);
            tvAGPunchTime.setText(getString(com.irizid.mess.R.string.punch_time_label, punchTime));
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NFCMemberAccessMainActivity.this);
        String reader_name = prefs.getString(PreferencesManager.KEY_SET_READER_NAME, "");
        String session_name = prefs.getString(PreferencesManager.KEY_SET_SESSION_NAME, "");
        tvAGReaderName.setText(getString(com.irizid.mess.R.string.access_point_label, reader_name));
        tvAGSessionName.setText(getString(com.irizid.mess.R.string.session_label, session_name));

        String card_no = memacclogs.getMemAccLogsCardNo();
        if (card_no.length() > 0) {
            tvAGEmpCardNo.setText(getString(com.irizid.mess.R.string.card_number_label, card_no));
        }

        Glide
            .with(NFCMemberAccessMainActivity.this)
            .load(emp_photo)
            //.placeholder(R.mipmap.ic_launcher)
            .error(com.irizid.mess.R.mipmap.ic_profile_pic)
            .crossFade()
            .into(ivAGEmpPhoto);

        access_dialog.setNegativeButton(getString(com.irizid.mess.R.string.ok_label), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
            }
        });

        access_dialog.setOnDismissListener(onAccessDialogDismiss);

        access_alert = access_dialog.create();
        access_alert.show();*/
    }

    public DialogInterface.OnDismissListener onAccessDialogDismiss = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
            refreshFragment();
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == HomeScreen.SET_READER && resultCode == HomeScreen.SET_READER) {
            String readerName = data.getExtras().getString("Selected_Reader");
            String sessionName = data.getExtras().getString("Selected_Session");

            tvMemAccReaderName.setText(getResources().getString(R.string.access_point_label, readerName));
            tvMemAccSessionName.setText(getResources().getString(R.string.session_label, sessionName));

            Fragment fragment = adapter.getCurrentFragment();
            if (fragment instanceof AllowedMembersList) {
                ((AllowedMembersList) fragment).fragmentBecameVisible(this);
            } else if (fragment instanceof AccessedMembersList) {
                ((AccessedMembersList) fragment).fragmentBecameVisible(this);
            } else if (fragment instanceof DeniedMembersList) {
                ((DeniedMembersList) fragment).fragmentBecameVisible(this);
            } else if (fragment instanceof PendingMembersList) {
                ((PendingMembersList) fragment).fragmentBecameVisible(this);
            }

            doPunchEntries(NFCMemberAccessMainActivity.this, "32142705");
        }
    }

    //to display progress dialog while any async task has been called from the fragment so we can freez the screen
    ProgressDialog pDialog;
    public void setProgress (boolean progressStatus) {
        if (progressStatus) {
            if (pDialog == null) {
                pDialog = new ProgressDialog(NFCMemberAccessMainActivity.this, com.smartportable.demo2.R.style.MyTheme);
            }
            pDialog.setCancelable(false);
            pDialog.show();
        } else {
            if (pDialog != null) {
                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }
            }
        }
    }

    // Read the logs and display file in Dialog
    public void DisplayDeleteLogsFileInDialog (String logs) {
        if (access_dialog == null) {
            access_dialog = new AlertDialog.Builder(NFCMemberAccessMainActivity.this);
        }else {
            access_alert.dismiss();
            access_dialog = new AlertDialog.Builder(NFCMemberAccessMainActivity.this);
        }

        LayoutInflater inflater = LayoutInflater.from(NFCMemberAccessMainActivity.this);
        View dialogLayout = inflater.inflate(com.smartportable.demo2.R.layout.dialog_old_mem_acc_logs_exists, null);
        access_dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        TextView tvWarningMessage = (TextView) dialogLayout.findViewById(com.smartportable.demo2.R.id.warningMsg);
        tvWarningMessage.setText(logs);
        tvWarningMessage.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
        access_dialog.setPositiveButton(getString(com.smartportable.demo2.R.string.okay_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                access_alert.dismiss();
            }
        });


        access_alert = access_dialog.create();
        access_alert.show();

    }

    // Display the dialog for the UnSync Dialog
    public void DisplayWarningForUnSyncedDialog () {
        if (access_dialog == null) {
            access_dialog = new AlertDialog.Builder(NFCMemberAccessMainActivity.this);
        }else {
            access_alert.dismiss();
            access_dialog = new AlertDialog.Builder(NFCMemberAccessMainActivity.this);
        }

        LayoutInflater inflater = LayoutInflater.from(NFCMemberAccessMainActivity.this);
        View dialogLayout = inflater.inflate(com.smartportable.demo2.R.layout.dialog_old_mem_acc_logs_exists, null);
        access_dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        TextView tvWarningMessage = (TextView) dialogLayout.findViewById(com.smartportable.demo2.R.id.warningMsg);
        tvWarningMessage.setText(getApplicationContext().getResources().getString(com.smartportable.demo2.R.string.mem_acc_not_sync_logs_warning));

        access_dialog.setPositiveButton(getString(com.smartportable.demo2.R.string.okay_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });


        access_alert = access_dialog.create();
        access_alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                DatabaseHandler dbh = new DatabaseHandler(NFCMemberAccessMainActivity.this);
                int oldEntriesCount = dbh.isOldSyncedMemAccLogsExist();
                if (oldEntriesCount > 0) {
                    Log.d(TAG, "Old Entries Exists");
                    DisplayOldEntriesExistWarningDialog(oldEntriesCount);
                } else {
                    startActivityForResult(new Intent(NFCMemberAccessMainActivity.this, SetReader.class), HomeScreen.SET_READER);
                }
            }
        });
        access_alert.setCancelable(false);
        access_alert.show();

    }

    public void DisplayOldEntriesExistWarningDialog (final int noOfRecords) {
        if (access_dialog == null) {
            access_dialog = new AlertDialog.Builder(NFCMemberAccessMainActivity.this);
        }else {
            access_alert.dismiss();
            access_dialog = new AlertDialog.Builder(NFCMemberAccessMainActivity.this);
        }

        LayoutInflater inflater = LayoutInflater.from(NFCMemberAccessMainActivity.this);
        View dialogLayout = inflater.inflate(com.smartportable.demo2.R.layout.dialog_old_mem_acc_logs_exists, null);
        access_dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        TextView tvAGLabel = (TextView) dialogLayout.findViewById(com.smartportable.demo2.R.id.tvAGLabel);

        access_dialog.setPositiveButton(getString(com.smartportable.demo2.R.string.yes_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(TAG, "Proceed for deleting");
                new DeleteOldmemAccLogs(noOfRecords).execute();
            }
        });
        access_dialog.setNegativeButton(getString(com.smartportable.demo2.R.string.no_label), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                startActivityForResult(new Intent(NFCMemberAccessMainActivity.this, SetReader.class), HomeScreen.SET_READER);
            }
        });

        //access_dialog.setOnDismissListener(onOldLogsDialogDismiss);

        access_alert = access_dialog.create();
        access_alert.show();
    }

    public DialogInterface.OnDismissListener onOldLogsDialogDismiss = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
            //refreshFragment();

        }
    };

    public class DeleteOldmemAccLogs extends AsyncTask<Void, Void, Void> {
        ProgressDialog pDialogDelete;
        DatabaseHandler dbh;
        int totalRecords;

        public DeleteOldmemAccLogs (int totalRecords) {
            this.totalRecords = totalRecords;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialogDelete = new ProgressDialog(NFCMemberAccessMainActivity.this, com.smartportable.demo2.R.style.MyTheme);
            pDialogDelete.setCancelable(false);
            pDialogDelete.show();

            dbh = new DatabaseHandler(NFCMemberAccessMainActivity.this);

        }

        @Override
        protected Void doInBackground(Void... params) {

            dbh.deleteOldMemAccLogs();
            String str = getString(com.smartportable.demo2.R.string.total_deleted_records_label, ""+totalRecords) + ", ";
            dbh.saveLogsForDeletedMemAccLogs(NFCMemberAccessMainActivity.this, str);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pDialogDelete.isShowing())
                pDialogDelete.dismiss();

            startActivityForResult(new Intent(NFCMemberAccessMainActivity.this, SetReader.class), HomeScreen.SET_READER);
        }
    }
}