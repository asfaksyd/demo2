package com.smartportable.demo2.maingateaccess;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidscade.nfckotlindemo.NFCUtil;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;
import com.smartportable.demo2.HomeScreen;
import com.smartportable.demo2.MyApplication;
import com.smartportable.demo2.R;
import com.smartportable.demo2.SetReader;
import com.smartportable.demo2.adapters.MainGateAccessLogsAdapter;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.fragments.MGALogFragment;
import com.smartportable.demo2.fragments.MainGateAccLogDetailFragment;
import com.smartportable.demo2.fragments.MemAccLogDetailFragment;
import com.smartportable.demo2.helper.Config;
import com.smartportable.demo2.helper.GlobalClass;
import com.smartportable.demo2.helper.MemberAccessLogs;
import com.smartportable.demo2.helper.PreferencesManager;
import com.smartportable.demo2.models.MainGateAccLogs;
import com.smartportable.demo2.models.MainGateLogs;
import com.smartportable.demo2.models.Student;
import com.smartportable.demo2.models.StudentInfoModel;
import com.smartportable.demo2.services.SendMainGateWorker;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import fr.coppernic.sdk.utils.debug.Log;
import fr.coppernic.sdk.utils.helpers.CpcOs;

/**
 * Created by Ananth on 3/19/2017.
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
public class MainGateAccessActivity extends AppCompatActivity implements NfcAdapter.ReaderCallback {

    public String TAG = this.getClass().getCanonicalName();

    Toolbar toolbar;
    SharedPreferences prefsMifare;
    String cardData = null; // for the Mifare card from NFC reader
    Snackbar snackbarNFC;
    RelativeLayout rlMAG;
    RecyclerView rvMAG;
    TextView tvMGACurrGate, tvMGACurrPunchType;
    AlertDialog.Builder access_dialog;
    AlertDialog access_alert;
    MainGateAccessLogsAdapter adapter;
    DatabaseHandler db;
    public static Integer SCHEDULE_TIME = 15;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_main_gate_access);

        db = new DatabaseHandler(MainGateAccessActivity.this);
        prefsMifare = PreferenceManager.getDefaultSharedPreferences(this);

        rlMAG = (RelativeLayout) findViewById(R.id.rlMAG);
        rvMAG = (RecyclerView) findViewById(R.id.rvMAG);
        tvMGACurrGate = (TextView) findViewById(R.id.tvMGACurrGate);
        tvMGACurrPunchType = (TextView) findViewById(R.id.tvMGACurrPunchType);

        setCurrentAccess();

        Log.d(TAG, "onCreate");
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            try {
                ViewConfiguration config = ViewConfiguration.get(this);
                Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
                if(menuKeyField != null) {
                    menuKeyField.setAccessible(true);
                    menuKeyField.setBoolean(config, false);
                }
            } catch (Exception ex) {
                // Ignore
            }
        }
        //toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int nfcStatus = URLCollections.isDeviceHaveNFC(this);
            if (CpcOs.isC5() && !CpcOs.isCone()) {
                //configureLedWidget();
                if (nfcStatus == 1) {
                    enableReaderMode();
                } else if (nfcStatus == 2) {
                    //Boast.makeText(this, "Please ON your NFC from settings", Toast.LENGTH_LONG).show();
                    snackbarNFC = Snackbar
                            .make(rlMAG, getResources().getString(R.string.nfc_on_msg), Snackbar.LENGTH_INDEFINITE);
                    snackbarNFC.setAction(getResources().getString(R.string.nfc_on_action), mOnSnackBarONClickListener);
                    snackbarNFC.setActionTextColor(Color.GREEN);
                    View snackbarView = snackbarNFC.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbarNFC.show();
                } else {
                    //Boast.makeText(this, "Your device doesn't have NFC", Toast.LENGTH_LONG).show();
                    snackbarNFC = Snackbar
                            .make(rlMAG, getResources().getString(R.string.nfc_not_available), Snackbar.LENGTH_LONG);
                    //snackbarNFC.setAction(getResources().getString(R.string.nfc_on_action), mOnSnackBarONClickListener);
                    //snackbarNFC.setActionTextColor(Color.GREEN);
                    View snackbarView = snackbarNFC.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbarNFC.show();
                }
                /*Intent intent = new Intent(this, ScanCardServiceBroadcastReceiver.class);
                alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
                alarmMgr.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), Config.SCAN_CARD_SERVICE_INTERVAL, alarmIntent);*/
            } else {
                Log.d(TAG, "Other device");
                if (nfcStatus == 1) {
                    enableReaderMode();
                } else if (nfcStatus == 2) {
                    //Boast.makeText(this, "Please ON your NFC from settings", Toast.LENGTH_LONG).show();
                    snackbarNFC = Snackbar
                            .make(rlMAG, getResources().getString(R.string.nfc_on_msg), Snackbar.LENGTH_INDEFINITE);
                    snackbarNFC.setAction(getResources().getString(R.string.nfc_on_action), mOnSnackBarONClickListener);
                    snackbarNFC.setActionTextColor(Color.GREEN);
                    View snackbarView = snackbarNFC.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbarNFC.show();
                } else {
                    //Boast.makeText(this, "Your device doesn't have NFC", Toast.LENGTH_LONG).show();
                    snackbarNFC = Snackbar
                            .make(rlMAG, getResources().getString(R.string.nfc_not_available), Snackbar.LENGTH_LONG);
                    //snackbarNFC.setAction(getResources().getString(R.string.nfc_on_action), mOnSnackBarONClickListener);
                    //snackbarNFC.setActionTextColor(Color.GREEN);
                    View snackbarView = snackbarNFC.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbarNFC.show();
                }
            }

            ArrayList<MainGateAccLogs.Data> listMainGateLogs = db.getAllMainGatePunchLogsData(0);//GlobalClass.getInstance().getMainGateLogsList(this, 0); // 0- ALL
            // update the readersAdapter
            rvMAG.setHasFixedSize(true);
            rvMAG.setLayoutManager(new LinearLayoutManager(this));
            // Connect the recycler to the scroller (to let the scroller scroll the list)
            adapter = new MainGateAccessLogsAdapter(this, this, listMainGateLogs, rvMAG, getSupportFragmentManager());
            rvMAG.setAdapter(adapter);
        }

        startSendLogsService();

    }

    public void startSendLogsService() {
        final WorkManager mWorkManager = WorkManager.getInstance();
        Constraints constraints = new  Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();
        final PeriodicWorkRequest mRequest = new PeriodicWorkRequest.Builder(SendMainGateWorker.class, SCHEDULE_TIME, TimeUnit.MINUTES).setConstraints(constraints).build();
        mWorkManager.enqueueUniquePeriodicWork("SendMainGateLogs", ExistingPeriodicWorkPolicy.KEEP, mRequest);

        /*mWorkManager.getWorkInfoByIdLiveData(mRequest.getId()).observe(this, new Observer<WorkInfo>() {
            @Override
            public void onChanged(@Nullable WorkInfo workInfo) {
                if (workInfo != null) {
                    WorkInfo.State state = workInfo.getState();
                    tvStatus.append(state.toString() + "\n");

                }
            }
        });*/
    }

    public void setCurrentAccess() {
        String gate_name = prefsMifare.getString(PreferencesManager.KEY_SET_GATE_NAME, "");
        int punch_type = prefsMifare.getInt(PreferencesManager.KEY_SET_PUNCH_TYPE, -1);
        tvMGACurrGate.setText(gate_name);
        String punchType="";
        if (punch_type == 1) {
            punchType = getResources().getString(R.string.punch_in_label);
        } else {
            punchType = getResources().getString(R.string.punch_out_label);
        }
        tvMGACurrPunchType.setText(punchType);
    }
    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("onResume isC5: "+ CpcOs.isC5());
        if (CpcOs.isC5()) {
            enableReaderMode();

            int nfcst = URLCollections.isDeviceHaveNFC(MainGateAccessActivity.this);
            if (nfcst == 1) {

            } else if (nfcst == 2) {
                snackbarNFC = Snackbar
                        .make(rlMAG, getResources().getString(R.string.nfc_on_msg), Snackbar.LENGTH_INDEFINITE);
                snackbarNFC.setAction(getResources().getString(R.string.nfc_on_action), mOnSnackBarONClickListener);
                snackbarNFC.setActionTextColor(Color.GREEN);
                View snackbarView = snackbarNFC.getView();
                snackbarView.setBackgroundColor(Color.DKGRAY);
                TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbarNFC.show();
            } else {
                snackbarNFC = Snackbar
                        .make(rlMAG, getResources().getString(R.string.nfc_not_available), Snackbar.LENGTH_INDEFINITE);
                //snackbarNFC.setAction(getResources().getString(R.string.nfc_on_action), mOnSnackBarONClickListener);
                //snackbarNFC.setActionTextColor(Color.GREEN);
                View snackbarView = snackbarNFC.getView();
                snackbarView.setBackgroundColor(Color.DKGRAY);
                TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbarNFC.show();
            }
        }

        // To refresh the data if it has synced with background service Name: SyncDataService
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean loc_st = prefs.getBoolean(PreferencesManager.KEY_LOCATION_SYNCED, false);
        if (loc_st) {
            // update the location sync flag as page refreshed
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean(PreferencesManager.KEY_LOCATION_SYNCED, false);
            edit.commit();

            android.util.Log.d(TAG, "Synced");
            ArrayList<Integer> catIdList = MyApplication.getInstance().cetegoriesIdList;
            ArrayList<Integer> areaIdList = MyApplication.getInstance().getAreaIdList();
            //new NFCMemberAccessMainActivity.GetEmployeeLocationOnFilter(getApplicationContext()).execute(catIdList, areaIdList);
        } else {
            Log.d(TAG, "Not synced");
        }
    }

    // Enable NFC reader mode
    private void enableReaderMode() {
        android.util.Log.i(TAG, "Enabling reader mode");
        NfcAdapter nfc = NfcAdapter.getDefaultAdapter(this);
        if (nfc != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                nfc.enableReaderMode(this, this, URLCollections.READER_FLAGS, null);
            }
        }
    }

    public View.OnClickListener mOnSnackBarONClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            android.util.Log.d(TAG, "SnackBar On Click");
            Intent intent = new Intent(Settings.ACTION_NFC_SETTINGS);
            startActivityForResult(intent, 11);
        }
    };

    @Override
    public void onBackPressed() {

        /*int backCount = fm.getBackStackEntryCount();
        Log.d(TAG, "BackStack Count"+backCount);
        if (backCount > 1) {
            fm.popBackStack();
        } else {
            if (backCount == 1)
                fm.popBackStack();

            super.onBackPressed();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }*/
        MainGateAccessActivity.this.finish();
        if(URLCollections.whichDirection(MainGateAccessActivity.this) == URLCollections.DEV_DIR_RTL) {
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
        } else {
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            //setResult(SyncActivity.REQUEST_SYNC_EMPLOYEES);
            MainGateAccessActivity.this.finish();
            if(URLCollections.whichDirection(MainGateAccessActivity.this) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
            }
            return true;
        }
        if (item.getItemId() == R.id.menuItemChangeMGA) {
            Intent i = new Intent(MainGateAccessActivity.this, MainGateAccessDialog.class);
            startActivityForResult(i, 2);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static Menu mMenu;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_gate, menu);
        mMenu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    public void onFilterItemClick (MenuItem item) {
        item.setChecked(true);
        ArrayList<MainGateAccLogs.Data> listMainGateLogs = new ArrayList<MainGateAccLogs.Data>();
        if (item.getItemId() == R.id.filter_all) {
            listMainGateLogs = db.getAllMainGatePunchLogsData(0);//GlobalClass.getInstance().getMainGateLogsList(this, 0);
        } else if (item.getItemId() == R.id.filter_punchin) {
            listMainGateLogs = db.getAllMainGatePunchLogsData(1);//GlobalClass.getInstance().getMainGateLogsList(this, 1); // 1 - Punch IN
        } else {
            listMainGateLogs = db.getAllMainGatePunchLogsData(2);;//GlobalClass.getInstance().getMainGateLogsList(this, 2); // 2 - Punch OUT
        }
        adapter.updateStudentList(listMainGateLogs);
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onTagDiscovered(Tag tag) {
        Log.d(TAG, "New tag discovered"+tag);
        byte[] id = tag.getId();
        android.util.Log.d(TAG, "TAG ID:" +id);
        int total_length = id.length-1;
        Log.d("TAG", "tag "+id+"tag length"+total_length);

        // Read card first with tag based on authentication key
        final MifareClassic mifareClassic = MifareClassic.get(tag);
        final IsoDep isoDep = IsoDep.get(tag);
        android.util.Log.d(TAG, "Mifare CLassic : " + mifareClassic);
        if (mifareClassic != null) {
            try {
                mifareClassic.connect();
                Log.d(TAG, "Transceive Length: " + mifareClassic.getMaxTransceiveLength());
                Log.d(TAG, "Selector count: " + mifareClassic.getSectorCount());
                int blockCount = mifareClassic.getBlockCount();
                Log.d(TAG, "SectorToBlock: " + mifareClassic.sectorToBlock(blockCount));

                boolean auth = false;
                // 5.2) and get the number of sectors this card has..and loop thru these sectors
                int secCount = mifareClassic.getSectorCount();
                int bCount = 0;
                int bIndex = 0;
                for (int j = 0; j < URLCollections.SECTOR_COUNT_FOR_MIFARE; j++) {
                    // 6.1) authenticate the sector
                    auth = NFCUtil.INSTANCE.mifareKeyAuthentication(mifareClassic, j);
                    if (auth) {
                        // 6.2) In each sector - get the block count
                        bCount = mifareClassic.getBlockCountInSector(j);
                        bIndex = 0;
                        for (int i = 0; i < URLCollections.BYTE_COUNT_FOR_MIFARE; i++) {
                            bIndex = mifareClassic.sectorToBlock(j);
                            // 6.3) Read the block
                            byte[] data = mifareClassic.readBlock(bIndex);
                            // 7) Convert the data into a string from Hex format.
                            cardData = ByteArrayToHexString(data, Config.MIFARE);
                            android.util.Log.d(TAG, cardData);//, data.length));
                            bIndex++;
                        }
                    } else { // Authentication failed - Handle it using directly reading TAG
                        android.util.Log.d(TAG, "Authentication Failed!");
                        cardData = ByteArrayToHexString(id, Config.MIFARE);

                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if (isoDep != null) {
            android.util.Log.d(TAG, "Mifare DESFire Card");
            cardData = ByteArrayToHexString(id, Config.DESFIRE);
            android.util.Log.d(TAG, "Card Number: "+cardData);
        }
        // check card number and update in table
        if (cardData != null && cardData.length() > 0 && checkAlphNumericString(cardData)) {
            Log.d(TAG, "Card Number: "+cardData);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    /*if (cardData.equals("6DA40014")) {
                        ArrayList<Student> stList = GlobalClass.getInstance().searchStudentRecord(cardData);
                        if (stList.size() > 0 & stList.size() == 1) {
                            BottomSheetDialogFragment bottomSheetDialogFragment = new MGALogFragment();
                            ((MGALogFragment) bottomSheetDialogFragment).setStudentDetail(stList.get(0));
                            ((MGALogFragment) bottomSheetDialogFragment).isAccessGranted(true);
                            //((EmployeeDetailFragment)bottomSheetDialogFragment).isForMemberAccess(forMemAccess);
                            bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                        }
                    } else {
                        cardData = "5A5B5C5D";
                        ArrayList<Student> stList = GlobalClass.getInstance().searchStudentRecord(cardData);
                        if (stList.size() > 0 & stList.size() == 1) {
                            BottomSheetDialogFragment bottomSheetDialogFragment = new MGALogFragment();
                            ((MGALogFragment) bottomSheetDialogFragment).setStudentDetail(stList.get(0));
                            ((MGALogFragment) bottomSheetDialogFragment).isAccessGranted(false);
                            //((EmployeeDetailFragment)bottomSheetDialogFragment).isForMemberAccess(forMemAccess);
                            bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                        }
                    }*/

                    //doPunchEntries(MainGateAccessActivity.this, cardData);
                    doPunchForGateAccLogs(MainGateAccessActivity.this, cardData);
                }
            });

        }else {
            Log.d(TAG, "CardNumber null");
        }
    }

    /**
     * Utility class to convert a byte array to a hexadecimal string.
     *
     * @param bytes Bytes to convert
     * @return String, containing hexadecimal representation.
     */
    public String ByteArrayToHexString(byte[] bytes, String typeStr) {
        final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[URLCollections.BYTE_LENGTH_FOR_MIFARE * 2];
        int v;

        // do not forgot to pass default values as second arguments other wise it will return empty string and condition will not be satisfied
        String mifare_reading_type = prefsMifare.getString(PreferencesManager.KEY_SET_MIFARE_READING_TYPE, PreferencesManager.DEFAULT_MIFARE_READING_TYPE_LSB);
        if (mifare_reading_type != null) {
            if (mifare_reading_type.length() != 0 ) {
                if (mifare_reading_type.equals(Config.LSB)) {
                    for (int j = 0; j < URLCollections.BYTE_LENGTH_FOR_MIFARE; j++) {
                        //for ( int j = 3; j ==0 ; j-- ) {
                        v = bytes[j] & 0xFF;
                        hexChars[(URLCollections.BYTE_LENGTH_FOR_MIFARE_TO_REVERSE - j) * 2] = hexArray[v >>> 4];
                        hexChars[(URLCollections.BYTE_LENGTH_FOR_MIFARE_TO_REVERSE - j) * 2 + 1] = hexArray[v & 0x0F];
                    }
                } else {
                    for (int j = 0; j < URLCollections.BYTE_LENGTH_FOR_MIFARE; j++) {
                        v = bytes[j] & 0xFF;
                        hexChars[j * 2] = hexArray[v >>> 4];
                        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
                    }
                }
            }
        }

        String str = new String(hexChars);
        if (typeStr.equals(Config.DESFIRE) && str.length() > 8) {
            str = str.substring(0, 7);
            return str;
        } else {
            return str;
        }
    }

    private boolean checkAlphNumericString(String str) {
        return str.matches("[A-Za-z0-9]+");
    }


    public BottomSheetDialogFragment bottomSheetDialogFragment = null;
    public void showAccessGrantedDialog (MainGateAccLogs.Data mainGateAccLogs) {
        // check - dismiss and create BSD for scan card
        if (bottomSheetDialogFragment == null) {
            bottomSheetDialogFragment = new MainGateAccLogDetailFragment();
        } else {
            bottomSheetDialogFragment.dismiss();
            bottomSheetDialogFragment = new MainGateAccLogDetailFragment();
        }

        ((MainGateAccLogDetailFragment)bottomSheetDialogFragment).setMemAccData(MainGateAccessActivity.this, mainGateAccLogs);
        ((MainGateAccLogDetailFragment)bottomSheetDialogFragment).isForMemberAccess("");
        ((MainGateAccLogDetailFragment)bottomSheetDialogFragment).isFromScan(true);
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    public DialogInterface.OnDismissListener onAccessDialogDismiss = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
            //refreshFragment();
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == HomeScreen.SET_READER && resultCode == HomeScreen.SET_READER) {
            String readerName = data.getExtras().getString("Selected_Reader");
            String sessionName = data.getExtras().getString("Selected_Session");

            /*Fragment fragment = readersAdapter.getCurrentFragment();
            if (fragment instanceof AllowedMembersList) {
                ((AllowedMembersList) fragment).fragmentBecameVisible(this);
            } else if (fragment instanceof AccessedMembersList) {
                ((AccessedMembersList) fragment).fragmentBecameVisible(this);
            } else if (fragment instanceof DeniedMembersList) {
                ((DeniedMembersList) fragment).fragmentBecameVisible(this);
            } else if (fragment instanceof PendingMembersList) {
                ((PendingMembersList) fragment).fragmentBecameVisible(this);
            }*/

        } else if (requestCode == HomeScreen.SET_GATE && resultCode == HomeScreen.SET_GATE) {
            String readerName = data.getExtras().getString("Selected_Gate");
            int punch_type = data.getExtras().getInt("Selected_Punch_Type");
            String punchType = "";
            if (punch_type == 1) {
                punchType = getResources().getString(R.string.punch_in_label);
            } else {
                punchType = getResources().getString(R.string.punch_out_label);
            }
            Snackbar snackbar = Snackbar
                    .make(rlMAG, getApplicationContext().getResources().getString(R.string.set_gate, readerName, punchType), Snackbar.LENGTH_LONG);
            View snackbarView = snackbar.getView();
            snackbarView.setBackgroundColor(Color.DKGRAY);
            TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();

            mMenu.findItem(R.id.filter_all).setChecked(true);
            onFilterItemClick(mMenu.findItem(R.id.filter_all));//mMenu.findItem(R.id.filter_all));
            setCurrentAccess();
            doPunchForGateAccLogs(MainGateAccessActivity.this, "D39CE1C6");
            /*snackbar.setCallback(new Snackbar.Callback() {
                @Override
                public void onDismissed(Snackbar snackbar, int event) {
                    super.onDismissed(snackbar, event);

                    // after set area change the activity
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        startActivity(new Intent(MainGateAccessActivity.this, MainGateAccessActivity.class));
                        overridePendingTransition(R.anim.anim_slide_in_left,
                                R.anim.anim_slide_out_left);
                    }
                }
            });*/
        }
    }

    //to display progress dialog while any async task has been called from the fragment so we can freez the screen
    ProgressDialog pDialog;
    public void setProgress (boolean progressStatus) {
        if (progressStatus) {
            if (pDialog == null) {
                pDialog = new ProgressDialog(MainGateAccessActivity.this, R.style.MyTheme);
            }
            pDialog.setCancelable(false);
            pDialog.show();
        } else {
            if (pDialog != null) {
                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }
            }
        }
    }

    // Read the logs and display file in Dialog
    public void DisplayDeleteLogsFileInDialog (String logs) {
        if (access_dialog == null) {
            access_dialog = new AlertDialog.Builder(MainGateAccessActivity.this);
        }else {
            access_alert.dismiss();
            access_dialog = new AlertDialog.Builder(MainGateAccessActivity.this);
        }

        LayoutInflater inflater = LayoutInflater.from(MainGateAccessActivity.this);
        View dialogLayout = inflater.inflate(R.layout.dialog_old_mem_acc_logs_exists, null);
        access_dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        TextView tvWarningMessage = (TextView) dialogLayout.findViewById(R.id.warningMsg);
        tvWarningMessage.setText(logs);
        tvWarningMessage.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
        access_dialog.setPositiveButton(getString(R.string.okay_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                access_alert.dismiss();
            }
        });


        access_alert = access_dialog.create();
        access_alert.show();

    }

    // Display the dialog for the UnSync Dialog
    public void DisplayWarningForUnSyncedDialog () {
        if (access_dialog == null) {
            access_dialog = new AlertDialog.Builder(MainGateAccessActivity.this);
        }else {
            access_alert.dismiss();
            access_dialog = new AlertDialog.Builder(MainGateAccessActivity.this);
        }

        LayoutInflater inflater = LayoutInflater.from(MainGateAccessActivity.this);
        View dialogLayout = inflater.inflate(R.layout.dialog_old_mem_acc_logs_exists, null);
        access_dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        TextView tvWarningMessage = (TextView) dialogLayout.findViewById(R.id.warningMsg);
        tvWarningMessage.setText(getApplicationContext().getResources().getString(R.string.mem_acc_not_sync_logs_warning));

        access_dialog.setPositiveButton(getString(R.string.okay_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });


        access_alert = access_dialog.create();
        access_alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                DatabaseHandler dbh = new DatabaseHandler(MainGateAccessActivity.this);
                int oldEntriesCount = dbh.isOldSyncedMemAccLogsExist();
                if (oldEntriesCount > 0) {
                    Log.d(TAG, "Old Entries Exists");
                    DisplayOldEntriesExistWarningDialog(oldEntriesCount);
                } else {
                    startActivityForResult(new Intent(MainGateAccessActivity.this, SetReader.class), HomeScreen.SET_READER);
                }
            }
        });
        access_alert.setCancelable(false);
        access_alert.show();

    }

    public void DisplayOldEntriesExistWarningDialog (final int noOfRecords) {
        if (access_dialog == null) {
            access_dialog = new AlertDialog.Builder(MainGateAccessActivity.this);
        }else {
            access_alert.dismiss();
            access_dialog = new AlertDialog.Builder(MainGateAccessActivity.this);
        }

        LayoutInflater inflater = LayoutInflater.from(MainGateAccessActivity.this);
        View dialogLayout = inflater.inflate(R.layout.dialog_old_mem_acc_logs_exists, null);
        access_dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        TextView tvAGLabel = (TextView) dialogLayout.findViewById(R.id.tvAGLabel);

        access_dialog.setPositiveButton(getString(R.string.yes_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(TAG, "Proceed for deleting");
                new DeleteOldmemAccLogs(noOfRecords).execute();
            }
        });
        access_dialog.setNegativeButton(getString(R.string.no_label), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                startActivityForResult(new Intent(MainGateAccessActivity.this, SetReader.class), HomeScreen.SET_READER);
            }
        });

        //access_dialog.setOnDismissListener(onOldLogsDialogDismiss);

        access_alert = access_dialog.create();
        access_alert.show();
    }

    public DialogInterface.OnDismissListener onOldLogsDialogDismiss = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
            //refreshFragment();

        }
    };

    public void doPunchForGateAccLogs(Context mContext, String cardNumber) {
        DatabaseHandler db = new DatabaseHandler(mContext);
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => "+c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//, Config.DATETIME_FORMAT_LOCAL);
        df.setTimeZone(TimeZone.getDefault());
        String punchDateTime = df.format(c.getTime());
        int gateId = prefsMifare.getInt(PreferencesManager.KEY_SET_GATE_ID, -1);
        String gateName = prefsMifare.getString(PreferencesManager.KEY_SET_GATE_NAME, "");
        int punchType = prefsMifare.getInt(PreferencesManager.KEY_SET_PUNCH_TYPE, -1);

        String stuId = db.getStudentIdFromCardNumber(cardNumber);
        String stuName = db.getStudentNameFromCardNumber(cardNumber);
        StudentInfoModel.DataBean student = db.getStudentFromCardNumber(cardNumber);

        MainGateAccLogs.Data mainGateAccLogs = new MainGateAccLogs().new Data();
        mainGateAccLogs.setStudentId(stuId);
        mainGateAccLogs.setStudentName(stuName);
        mainGateAccLogs.setStudentMealNumber(stuId);
        mainGateAccLogs.setCardNumber(cardNumber);
        mainGateAccLogs.setGateId(gateId);
        mainGateAccLogs.setGateName(gateName);
        mainGateAccLogs.setPunchDateTime(punchDateTime);
        mainGateAccLogs.setPunchType(punchType);
        mainGateAccLogs.setSync(0);

        showAccessGrantedDialog(mainGateAccLogs);

        db.saveMainGatePunchLogsData(mainGateAccLogs);
    }

    public class DeleteOldmemAccLogs extends AsyncTask<Void, Void, Void> {
        ProgressDialog pDialogDelete;
        DatabaseHandler dbh;
        int totalRecords;

        public DeleteOldmemAccLogs (int totalRecords) {
            this.totalRecords = totalRecords;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialogDelete = new ProgressDialog(MainGateAccessActivity.this, R.style.MyTheme);
            pDialogDelete.setCancelable(false);
            pDialogDelete.show();

            dbh = new DatabaseHandler(MainGateAccessActivity.this);

        }

        @Override
        protected Void doInBackground(Void... params) {

            dbh.deleteOldMemAccLogs();
            String str = getString(R.string.total_deleted_records_label, ""+totalRecords) + ", ";
            dbh.saveLogsForDeletedMemAccLogs(MainGateAccessActivity.this, str);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pDialogDelete.isShowing())
                pDialogDelete.dismiss();

            startActivityForResult(new Intent(MainGateAccessActivity.this, SetReader.class), HomeScreen.SET_READER);
        }
    }
}