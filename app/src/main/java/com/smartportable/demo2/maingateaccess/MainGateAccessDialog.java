package com.smartportable.demo2.maingateaccess;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.smartportable.demo2.ChangeReaderDialog;
import com.smartportable.demo2.HomeScreen;
import com.smartportable.demo2.R;
import com.smartportable.demo2.SetReader;
import com.smartportable.demo2.adapters.MainGateAccessItemAdapter;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.GlobalClass;
import com.smartportable.demo2.helper.PreferencesManager;
import com.smartportable.demo2.models.AGInfoModel;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

/**
 * Created by Ananth on 2/1/2017.
 */

public class MainGateAccessDialog extends AppCompatActivity implements View.OnClickListener {

    Integer gateId = 0;
    String readerName;
    Integer punchType = 0;
    RadioGroup rgPunchType;
    ImageView ivDialogCloseAG;
    Button btnSetGate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_main_gate_access);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbarGA);
        setSupportActionBar(myToolbar);

        DatabaseHandler db = new DatabaseHandler(this);

        //final LinearLayout llSetReaderList = (LinearLayout) findViewById(R.id.llSetReaderList);
        Spinner spMainGateAccess = (Spinner) findViewById(R.id.spMainGateAccess);
        btnSetGate = (Button) findViewById(R.id.btnSetGate);
        rgPunchType = (RadioGroup) findViewById(R.id.rgPunchType);
        RadioButton rbPunchIn = (RadioButton) findViewById(R.id.rbPunchIn);
        RadioButton rbPunchOut = (RadioButton)findViewById(R.id.rbPunchOut);

        ivDialogCloseAG = (ImageView) findViewById(R.id.ivDialogCloseGA);

        btnSetGate.setOnClickListener(this);
        ivDialogCloseAG.setOnClickListener(this);

        //lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final ArrayList<AGInfoModel.DataBean> mainGatesList = db.getAllMainGatesAGInfo();//GlobalClass.getInstance().mainGateAccessPoints(this);//db.getAllReaders();

        final ArrayList<String> readerList = new ArrayList<>();
        if (mainGatesList.size() != 0) {
            int readerId = prefs.getInt(PreferencesManager.KEY_SET_GATE_ID, -1);
            int punchType = prefs.getInt(PreferencesManager.KEY_SET_PUNCH_TYPE, -1);

            // set Punch type from preferences
            if (punchType > 0) {
                if (punchType == 1) {
                    rbPunchIn.setChecked(true);
                } else {
                    rbPunchOut.setChecked(true);
                }
            }
            int indexOfStoreReader = -1;
            for (int i = 0; i < mainGatesList.size(); i++) {
                //readerArray[i] = readerList.get(i).getReader_Name();
                readerList.add(mainGatesList.get(i).getAgName());
                if (readerId != -1 && readerId >= 0 && readerId == mainGatesList.get(i).getAgId()) {
                    indexOfStoreReader = i;
                }
            }

            final String[] mainGateArray = new String[readerList.size()];
            for (int i = 0; i < readerList.size(); i++) {
                mainGateArray[i] = readerList.get(i);
            }

            /*ArrayAdapter<CharSequence> spAdapter = ArrayAdapter.createFromResource(this, mainGateArray, android.R.layout.simple_spinner_item);
            // Specify the layout to use when the list of choices appears
            spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the readersAdapter to the spinner
            spMainGateAccess.setAdapter(spAdapter);*/

            final MainGateAccessItemAdapter adapter = new MainGateAccessItemAdapter(MainGateAccessDialog.this, readerList, indexOfStoreReader);
            adapter.enableCheckBox(true);
            //lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            spMainGateAccess.setAdapter(adapter);

            spMainGateAccess.setSelection(indexOfStoreReader);

            spMainGateAccess.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.d("Position", "item position: " + position);
                    gateId = mainGatesList.get(position).getAgId();
                    readerName = (String) adapter.getItem(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } else {
            // after set area change the activity
            //startActivity(new Intent(ChangeReaderDialog.this, MainActivity.class));
            //overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnSetGate) {
            int punch_type = rgPunchType.getCheckedRadioButtonId();
            if (punch_type == R.id.rbPunchIn) {
                punchType = 1;
            } else if (punch_type == R.id.rbPunchOut) {
                punchType = 2;
            } else {
                punchType = 0;
            }

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainGateAccessDialog.this);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt(PreferencesManager.KEY_SET_GATE_ID, gateId);
            editor.putString(PreferencesManager.KEY_SET_GATE_NAME, readerName);
            editor.putInt(PreferencesManager.KEY_SET_PUNCH_TYPE, punchType);
            editor.commit();

            Intent i = new Intent();
            i.putExtra("Selected_Gate", readerName);
            i.putExtra("Selected_Punch_Type", punchType);
            setResult(HomeScreen.SET_GATE, i);
            finish();
        } else if (v.getId() == R.id.ivDialogCloseGA) {
            finish();
        }
    }
}
