package com.smartportable.demo2.maingateaccess;

import android.app.Dialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.smartportable.demo2.FilterActivity;
import com.smartportable.demo2.R;
import com.smartportable.demo2.adapters.MGAMultipleLogsAdapter2;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.helper.GlobalClass;
import com.smartportable.demo2.models.MainGateLogs;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class MainGateAccessDetailActivity2 extends AppCompatActivity {

    RelativeLayout rlCollapsibleView;
    CircleImageView ivMAGDetailStPhoto;
    CollapsingToolbarLayout ctlMGAD;
    TextView tvStDetailStName, tvStDetailStCardNo, tvStDetailStDeptName;
    AppBarLayout ablMGAD;
    MainGateLogs log;
    String stName;
    int stPhoto;
    TextView tvStDetailStGender, tvStDetailStDOB, tvStDetailStAdmission, tvStDetailStCollege, tvStDetailStCampus, tvStDetailStIssueDate, tvStDetailStExpiryDate;
    ListView lvReadersList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_gate_access_detail);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivMAGDetailStPhoto = (CircleImageView) findViewById(R.id.ivMAGDetailStPhoto);
        ctlMGAD = (CollapsingToolbarLayout) findViewById(R.id.ctlMGAD);
        ablMGAD = (AppBarLayout) findViewById(R.id.ablMGAD);
        rlCollapsibleView = (RelativeLayout) findViewById(R.id.rlCollapsibleView);
        tvStDetailStName = (TextView) findViewById(R.id.tvStDetailStName);
        tvStDetailStCardNo = (TextView) findViewById(R.id.tvStDetailStCardNo);
        tvStDetailStDeptName = (TextView) findViewById(R.id.tvStDetailStDeptName);

        tvStDetailStGender = (TextView) findViewById(R.id.tvStDetailStGender);
        tvStDetailStDOB = (TextView) findViewById(R.id.tvStDetailStDOB);
        tvStDetailStAdmission = (TextView) findViewById(R.id.tvStDetailStAdmission);
        tvStDetailStCollege = (TextView) findViewById(R.id.tvStDetailStCollege);
        tvStDetailStCampus = (TextView) findViewById(R.id.tvStDetailStCampus);
        tvStDetailStIssueDate =(TextView) findViewById(R.id.tvStDetailStIssueDate);
        tvStDetailStExpiryDate = (TextView) findViewById(R.id.tvStDetailStExpiryDate);

        lvReadersList = (ListView) findViewById(R.id.lvReadersList);

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // back button pressed
                MainGateAccessDetailActivity2.this.finish();

                if(URLCollections.whichDirection(MainGateAccessDetailActivity2.this) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                }
            }
        });

        try {
            if (getIntent().getExtras()!= null) {
                int stId = getIntent().getExtras().getInt("AccessLog");
                System.out.println("StId" +stId);
                ArrayList<MainGateLogs> list = GlobalClass.getInstance().getMainGateLogsList(this, 0);
                for (int i=0; i<list.size();i++) {
                    log = list.get(i);
                    if (log.getMglStId() == stId) {
                        ivMAGDetailStPhoto.setImageResource(log.getMglStPhoto());
                        stName = log.getMglStId()+" - "+log.getMglStName();
                        stPhoto = log.getMglStPhoto();
                        ctlMGAD.setTitle(stName);
                        tvStDetailStName.setText(stName);
                        tvStDetailStCardNo.setText("Card No.: "+log.getMglStCardNo());
                        tvStDetailStDeptName.setText("Dept. Name: "+log.getMglStDeptName());

                        tvStDetailStGender.setText("Male");
                        tvStDetailStDOB.setText("january 18, 1993");
                        tvStDetailStAdmission.setText("Civil And Envi. Eng./MSc");
                        tvStDetailStCollege.setText("AAiT Regular");
                        tvStDetailStCampus.setText("Amist Kilo Campus");
                        tvStDetailStIssueDate.setText("7 May, 2017");
                        tvStDetailStExpiryDate.setText("6 May, 2019");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // toolbar collapse/expand listener
        ablMGAD.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset)-appBarLayout.getTotalScrollRange() == 0)
                {
                    //  Collapsed
                    ctlMGAD.setTitle(stName);
                    hideShowViews(View.GONE);
                }
                else
                {
                    //Expanded
                    ctlMGAD.setTitle(" ");
                    hideShowViews(View.VISIBLE);
                }

            }
        });

        //ArrayAdapter<String> readersAdapter =  new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
        ArrayList<MainGateLogs> list = GlobalClass.getInstance().getMGAMultiLogs(this, 0);
        MGAMultipleLogsAdapter2 adapter = new MGAMultipleLogsAdapter2(this, list);
        lvReadersList.setAdapter(adapter);
        //Utility.setListViewHeightBasedOnChildren(lvReadersList);
        FilterActivity.ListUtils.setDynamicHeight(lvReadersList);

        ivMAGDetailStPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageDialog(stPhoto);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            MainGateAccessDetailActivity2.this.finish(); // close this activity and return to preview activity (if there is any)

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        MainGateAccessDetailActivity2.this.finish();
        if(URLCollections.whichDirection(MainGateAccessDetailActivity2.this) == URLCollections.DEV_DIR_RTL) {
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
        } else {
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
        //super.onBackPressed();
    }

    public void hideShowViews(int hide_show) {
        /*ivMAGDetailStPhoto.setVisibility(hide_show);
        tvStDetailStName.setVisibility(hide_show);
        tvStDetailStCardNo.setVisibility(hide_show);*/
        rlCollapsibleView.setVisibility(hide_show);

    }

    private void showImageDialog (int resId) {
        final Dialog mSplashDialog = new Dialog(this, android.R.style.Theme_Material_NoActionBar_Fullscreen);
        //mSplashDialog.requestWindowFeature((int) window.FEATURE_NO_TITLE);
        mSplashDialog.setContentView(R.layout.dialog_profile_photo);
        //Drawable d = mContext.getResources().getDrawable(resId);
        //Bitmap b = ((BitmapDrawable) d ).getBitmap();
        ImageView ivPPFull = ((ImageView)mSplashDialog.findViewById(R.id.ivStPPFull));
        ImageView ivClosePPFull = (ImageView) mSplashDialog.findViewById(R.id.ivClosePPFull);

        ivPPFull.setImageResource(resId);//Bitmap(b);

        PhotoViewAttacher photoViewAttacher = new PhotoViewAttacher(ivPPFull);

        photoViewAttacher.update();

        ivClosePPFull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSplashDialog.cancel();
            }
        });

        mSplashDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mSplashDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mSplashDialog.setCancelable(true);
        mSplashDialog.show();

    }

}
