package com.smartportable.demo2.maingateaccess;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.smartportable.demo2.HomeScreen;
import com.smartportable.demo2.R;
import com.smartportable.demo2.SetReader;
import com.smartportable.demo2.adapters.MainGateAccessLogsAdapter;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.GlobalClass;
import com.smartportable.demo2.models.MainGateAccLogs;
import com.smartportable.demo2.models.MainGateLogs;

import java.lang.reflect.Field;
import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import fr.coppernic.sdk.utils.debug.Log;

/**
 * Created by Ananth on 3/19/2017.
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
public class MainGateAccessDetailActivity extends AppCompatActivity {

    public String TAG = this.getClass().getCanonicalName();

    Toolbar toolbar;
    SharedPreferences prefsMifare;
    String cardData = null; // for the Mifare card from NFC reader
    Snackbar snackbarNFC;
    RelativeLayout rlMAG;
    RecyclerView rvMAG;
    AlertDialog.Builder access_dialog;
    AlertDialog access_alert;
    MainGateAccessLogsAdapter adapter;
    DatabaseHandler db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_main_gate_access);

        rlMAG = (RelativeLayout) findViewById(R.id.rlMAG);
        rvMAG = (RecyclerView) findViewById(R.id.rvMAG);
        db = new DatabaseHandler(this);

        Log.d(TAG, "onCreate");
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            try {
                ViewConfiguration config = ViewConfiguration.get(this);
                Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
                if(menuKeyField != null) {
                    menuKeyField.setAccessible(true);
                    menuKeyField.setBoolean(config, false);
                }
            } catch (Exception ex) {
                // Ignore
            }
        }
        //toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        prefsMifare = PreferenceManager.getDefaultSharedPreferences(this);


        ArrayList<MainGateAccLogs.Data> listMainGateLogs = db.getAllMainGatePunchLogsData(0);//GlobalClass.getInstance().getMainGateLogsList(this, 0); // 0- ALL
        // update the readersAdapter
        rvMAG.setHasFixedSize(true);
        rvMAG.setLayoutManager(new LinearLayoutManager(this));
        // Connect the recycler to the scroller (to let the scroller scroll the list)
        adapter = new MainGateAccessLogsAdapter(this, this, listMainGateLogs, rvMAG, getSupportFragmentManager());
        rvMAG.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }



    @Override
    public void onBackPressed() {


        MainGateAccessDetailActivity.this.finish();
        if(URLCollections.whichDirection(MainGateAccessDetailActivity.this) == URLCollections.DEV_DIR_RTL) {
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
        } else {
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            //setResult(SyncActivity.REQUEST_SYNC_EMPLOYEES);
            MainGateAccessDetailActivity.this.finish();
            if(URLCollections.whichDirection(MainGateAccessDetailActivity.this) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    Menu mMenu;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public void onFilterItemClick (MenuItem item) {
        item.setChecked(true);
        ArrayList<MainGateAccLogs.Data> listMainGateLogs = new ArrayList<MainGateAccLogs.Data>();
        if (item.getItemId() == R.id.filter_all) {
            listMainGateLogs = db.getAllMainGatePunchLogsData(0);//GlobalClass.getInstance().getMainGateLogsList(this, 0);

        } else if (item.getItemId() == R.id.filter_punchin) {
            listMainGateLogs = db.getAllMainGatePunchLogsData(1);//GlobalClass.getInstance().getMainGateLogsList(this, 1); // 1 - Punch IN
        } else {
            listMainGateLogs = db.getAllMainGatePunchLogsData(2);//GlobalClass.getInstance().getMainGateLogsList(this, 2); // 2 - Punch OUT
        }
        adapter.updateStudentList(listMainGateLogs);
        adapter.notifyDataSetChanged();
    }

    public DialogInterface.OnDismissListener onAccessDialogDismiss = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
            //refreshFragment();
        }
    };


    //to display progress dialog while any async task has been called from the fragment so we can freez the screen
    ProgressDialog pDialog;
    public void setProgress (boolean progressStatus) {
        if (progressStatus) {
            if (pDialog == null) {
                pDialog = new ProgressDialog(MainGateAccessDetailActivity.this, R.style.MyTheme);
            }
            pDialog.setCancelable(false);
            pDialog.show();
        } else {
            if (pDialog != null) {
                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }
            }
        }
    }

    // Read the logs and display file in Dialog
    public void DisplayDeleteLogsFileInDialog (String logs) {
        if (access_dialog == null) {
            access_dialog = new AlertDialog.Builder(MainGateAccessDetailActivity.this);
        }else {
            access_alert.dismiss();
            access_dialog = new AlertDialog.Builder(MainGateAccessDetailActivity.this);
        }

        LayoutInflater inflater = LayoutInflater.from(MainGateAccessDetailActivity.this);
        View dialogLayout = inflater.inflate(R.layout.dialog_old_mem_acc_logs_exists, null);
        access_dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        TextView tvWarningMessage = (TextView) dialogLayout.findViewById(R.id.warningMsg);
        tvWarningMessage.setText(logs);
        tvWarningMessage.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
        access_dialog.setPositiveButton(getString(R.string.okay_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                access_alert.dismiss();
            }
        });


        access_alert = access_dialog.create();
        access_alert.show();

    }

    // Display the dialog for the UnSync Dialog
    public void DisplayWarningForUnSyncedDialog () {
        if (access_dialog == null) {
            access_dialog = new AlertDialog.Builder(MainGateAccessDetailActivity.this);
        }else {
            access_alert.dismiss();
            access_dialog = new AlertDialog.Builder(MainGateAccessDetailActivity.this);
        }

        LayoutInflater inflater = LayoutInflater.from(MainGateAccessDetailActivity.this);
        View dialogLayout = inflater.inflate(R.layout.dialog_old_mem_acc_logs_exists, null);
        access_dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        TextView tvWarningMessage = (TextView) dialogLayout.findViewById(R.id.warningMsg);
        tvWarningMessage.setText(getApplicationContext().getResources().getString(R.string.mem_acc_not_sync_logs_warning));

        access_dialog.setPositiveButton(getString(R.string.okay_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });


        access_alert = access_dialog.create();
        access_alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                DatabaseHandler dbh = new DatabaseHandler(MainGateAccessDetailActivity.this);
                int oldEntriesCount = dbh.isOldSyncedMemAccLogsExist();
                if (oldEntriesCount > 0) {
                    Log.d(TAG, "Old Entries Exists");
                    DisplayOldEntriesExistWarningDialog(oldEntriesCount);
                } else {
                    startActivityForResult(new Intent(MainGateAccessDetailActivity.this, SetReader.class), HomeScreen.SET_READER);
                }
            }
        });
        access_alert.setCancelable(false);
        access_alert.show();

    }

    public void DisplayOldEntriesExistWarningDialog (final int noOfRecords) {
        if (access_dialog == null) {
            access_dialog = new AlertDialog.Builder(MainGateAccessDetailActivity.this);
        }else {
            access_alert.dismiss();
            access_dialog = new AlertDialog.Builder(MainGateAccessDetailActivity.this);
        }

        LayoutInflater inflater = LayoutInflater.from(MainGateAccessDetailActivity.this);
        View dialogLayout = inflater.inflate(R.layout.dialog_old_mem_acc_logs_exists, null);
        access_dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        TextView tvAGLabel = (TextView) dialogLayout.findViewById(R.id.tvAGLabel);

        access_dialog.setPositiveButton(getString(R.string.yes_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(TAG, "Proceed for deleting");
                new DeleteOldmemAccLogs(noOfRecords).execute();
            }
        });
        access_dialog.setNegativeButton(getString(R.string.no_label), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                startActivityForResult(new Intent(MainGateAccessDetailActivity.this, SetReader.class), HomeScreen.SET_READER);
            }
        });

        //access_dialog.setOnDismissListener(onOldLogsDialogDismiss);

        access_alert = access_dialog.create();
        access_alert.show();
    }

    public DialogInterface.OnDismissListener onOldLogsDialogDismiss = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
            //refreshFragment();

        }
    };

    public class DeleteOldmemAccLogs extends AsyncTask<Void, Void, Void> {
        ProgressDialog pDialogDelete;
        DatabaseHandler dbh;
        int totalRecords;

        public DeleteOldmemAccLogs (int totalRecords) {
            this.totalRecords = totalRecords;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialogDelete = new ProgressDialog(MainGateAccessDetailActivity.this, R.style.MyTheme);
            pDialogDelete.setCancelable(false);
            pDialogDelete.show();

            dbh = new DatabaseHandler(MainGateAccessDetailActivity.this);

        }

        @Override
        protected Void doInBackground(Void... params) {

            dbh.deleteOldMemAccLogs();
            String str = getString(R.string.total_deleted_records_label, ""+totalRecords) + ", ";
            dbh.saveLogsForDeletedMemAccLogs(MainGateAccessDetailActivity.this, str);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pDialogDelete.isShowing())
                pDialogDelete.dismiss();

            startActivityForResult(new Intent(MainGateAccessDetailActivity.this, SetReader.class), HomeScreen.SET_READER);
        }
    }
}