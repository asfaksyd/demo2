package com.smartportable.demo2.db;


import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.request.target.Target;
import com.smartportable.demo2.R;
import com.smartportable.demo2.helper.Areas;
import com.smartportable.demo2.helper.Categories;
import com.smartportable.demo2.helper.Config;
import com.smartportable.demo2.helper.Employee;
import com.smartportable.demo2.helper.MemberAccessLogs;
import com.smartportable.demo2.helper.MusteringReaders;
import com.smartportable.demo2.helper.PunchLogs;
import com.smartportable.demo2.helper.SaveImages;
import com.smartportable.demo2.helper.Sessions;
import com.smartportable.demo2.helper.Statistics;
import com.smartportable.demo2.models.AGInfoModel;
import com.smartportable.demo2.models.AccessListModel;
import com.smartportable.demo2.models.DoorAccessInfoModel;
import com.smartportable.demo2.models.MainGateAccLogs;
import com.smartportable.demo2.models.MainGateAccLogsModel;
import com.smartportable.demo2.models.MemAccLogsInfoModel;
import com.smartportable.demo2.models.ReaderInfoModel;
import com.smartportable.demo2.models.SessionsInfoModel;
import com.smartportable.demo2.models.StaffAccessListModel;
import com.smartportable.demo2.models.StaffInfoModel;
import com.smartportable.demo2.models.StudentInfoModel;
import com.smartportable.demo2.models.VisitorInfoModel;
import com.smartportable.demo2.utils.AppUtilsKt;
import com.smartportable.demo2.utils.KeysKt;
import com.smartportable.demo2.utils.SharedPref;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by asfak on 8/19/2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 3;

    // Database Name
    public static final String DATABASE_NAME = "sCampusManager";

    // readers table name
    private static final String TABLE_READERS = "readers";
    // categpories table name
    private static final String TABLE_CATEGORIES = "categories";
    // areas table name
    private static final String TABLE_AREAS = "areas";
    // employees table name
    private static final String TABLE_EMPLOYEES = "employees";
    // student info table name
    private static final String TABLE_STU_INFO = "student_info";
    // staff info table name
    private static final String TABLE_STAFF_INFO = "staff_info";
    // employees current location name
    private static final String TABLE_PUNCHLOGS = "employees_punchlogs";
    // Access List table name
    private static final String TABLE_ACCESS_LIST = "access_list";
    // Access List table name
    private static final String TABLE_STAFF_ACCESS_LIST = "staff_access_list";
    // member access table name
    private static final String TABLE_MEM_ACC = "member_access";
    // session table name
    private static final String TABLE_SESSION = "session";
    // member access logs table name
    private static final String TABLE_MEM_ACC_LOGS = "member_access_logs";
    private static final String TABLE_SESS = "sessions";
    // visitor table name
    private static final String TABLE_VISITOR_INFO = "visitors";
    // AG table name
    private static final String TABLE_AG_INFO = "ag_info";
    // MainGateAccess Logs
    private static final String TABLE_MAIN_GATE_LOGS = "main_gate_logs";

    // Readers Table Columns names
    private static final String KEY_READER_AUTO_ID = "id";
    private static final String KEY_READER_ID = "ReaderId";
    private static final String KEY_READER_NAME = "ReaderName";
    private static final String KEY_READER_TYPE = "ReaderType";
    private static final String KEY_MUSTERING_ID = "MusteringId";

    // Category Table Columns names
    private static final String KEY_CAT_AUTO_ID = "id";
    private static final String KEY_CAT_ID = "MusteringId";
    private static final String KEY_CAT_NAME = "MusteringName";

    // Area Table Column Names
    private static final String KEY_AREA_AUTO_ID = "id";
    private static final String KEY_AREA_ID = "AreaId";
    private static final String KEY_AREA_NAME = "AreaName";

    // Employee Table Columns names
    private static final String KEY_EMP_AUTO_ID = "id";
    private static final String KEY_EMP_ID = "EmployeeId";
    private static final String KEY_EMP_NAME = "EmployeeName";
    private static final String KEY_EMP_PHOTO = "Photo";
    private static final String KEY_EMP_CARD_NO = "CardNumber";
    private static final String KEY_EMP_LOC_ID = "LocationId";

    // Student Info Table Columns names
    private static final String KEY_STU_AUTO_ID = "id";
    private static final String KEY_STU_ID = "StudentId";
    private static final String KEY_STU_FIRST_NAME = "StudentFirstName";
    private static final String KEY_STU_FATHER_NAME = "StudentFatherName";
    private static final String KEY_STU_GRAND_FATHER_NAME = "GrandFatherName";
    private static final String KEY_STU_GENDER = "Gender";
    private static final String KEY_STU_DOB = "DateOfBirth";
    private static final String KEY_STU_SIG = "Signature";
    private static final String KEY_STU_COLLEGE = "College";
    private static final String KEY_STU_DEPT = "Department";
    private static final String KEY_STU_CAMPUS = "Campus";
    private static final String KEY_STU_PROGRAM = "Program";
    private static final String KEY_STU_DEGREE_TYPE = "DegreeType";
    private static final String KEY_STU_ADD_TYPE = "AdmissionType";
    private static final String KEY_STU_ADD_TYPE_SHORT = "AdmissionTypeShort" ;
    private static final String KEY_STU_VALID_DATE_UNTIL = "ValidDateUntil";
    private static final String KEY_STU_ISSUE_DATE = "IssueDate";
    private static final String KEY_STU_MEAL_NO = "MealNumber";
    private static final String KEY_STU_UNIQE_NO = "UniqueNo";
    private static final String KEY_STU_STATUS = "Status";
    private static final String KEY_STU_ISACTIVE = "Isactive";
    private static final String KEY_STU_CARD_NO = "CardNumber";
    private static final String KEY_STU_CARD_ST = "CardStatus";
    private static final String KEY_STU_ID_NO = "ID_Number";
    private static final String KEY_STU_IMAGE = "StudentImage";

    // Staff Info Table Columns names
    private static final String KEY_STAFF_AUTO_ID = "id";
    private static final String KEY_STAFF_ID = "StaffId";
    private static final String KEY_STAFF_APP_NO = "StaffAppNo";
    private static final String KEY_STAFF_SL_NO = "StaffSlNo";
    private static final String KEY_STAFF_UID = "StaffUID";
    private static final String KEY_STAFF_FULL_NAME = "StaffFullName";
    private static final String KEY_STAFF_GENDER = "StaffGender";
    private static final String KEY_STAFF_DOB = "StaffDOB";
    private static final String KEY_STAFF_DEPT = "StaffDepartment";
    private static final String KEY_STAFF_JOB_TITLE = "StaffJobTitle";
    private static final String KEY_STAFF_EMP_PHOTO = "StaffEmpPhoto";
    private static final String KEY_STAFF_SIG = "StaffSignature";
    private static final String KEY_STAFF_ADD = "StaffAddress";
    private static final String KEY_STAFF_ID_NO = "StaffIdNo";
    private static final String KEY_STAFF_ISSUE_DATE = "StaffIssueDate";
    private static final String KEY_STAFF_CARD_NO = "StaffCardNumber";
    private static final String KEY_STAFF_CARD_ST = "StaffCardStatus";;
    private static final String KEY_STAFF_EMAIL_ID = "StaffEmailId";
    private static final String KEY_STAFF_ISACTIVE = "StaffIsactive";
    private static final String KEY_STAFF_PASSWORD = "StaffPassword";
    private static final String KEY_STAFF_ID_NUMBER = "StaffId_Number";


    // Employees location and punch logs entries
    private static final String KEY_PUNCHLOGS_AUTO_ID = "id";
    private static final String KEY_PUNCHLOGS_SLN_PUNCH = "PunchlogsSLNPunch";
    private static final String KEY_PUNCHLOGS_EMP_ID = "PunchlogsEmployeeId";
    private static final String KEY_PUNCHLOGS_CARD_NO = "PunchlogsCardNumber";
    private static final String KEY_PUNCHLOGS_SLN_READER = "PunchlogsSLN_Reader";
    private static final String KEY_PUNCHLOGS_READER_NAME = "PunchlogsReaderName";
    private static final String KEY_PUNCHLOGS_DATETIME = "PunchlogsDateTime";
    private static final String KEY_PUNCHLOGS_GRANT_DENY = "PunchlogsGrantDeny";
    private static final String KEY_PUNCHLOGS_LOC_ID = "PunchlogsLocId";
    private static final String KEY_PUNCHLOGS_AREA_ID = "PunchlogsAreaId";
    private static final String KEY_PUNCHLOGS_CAT_ID = "PunchlogsCatId";

    // Member access entries
    private static final String KEY_MEM_ACC_AUTO_ID = "id";
    private static final String KEY_MEM_ACC_EMP_ID = "MemAccEmployeeId";
    private static final String KEY_MEM_ACC_CARD_NO = "MemAccEmployeeCardNumber";
    private static final String KEY_MEM_ACC_READER_ID = "MemAccReaderId";

    // Session entries
    private static final String KEY_SESSION_AUTO_ID = "id";
    private static final String KEY_SESSION_ID = "SessionId";
    private static final String KEY_SESSION_NAME = "SessionName";
    private static final String KEY_SESSION_DATE = "SessionDate";
    private static final String KEY_SESSION_FROM_DATETIME = "SessionFromDatetime";
    private static final String KEY_SESSION_TO_DATETIME = "SessionToDatetime";

    // Member access logs entries
    private static final String KEY_MEM_ACC_LOGS_AUTO_ID = "id";
    private static final String KEY_MEM_ACC_LOGS_STU_ID = "MemAccLogsStuId";
    private static final String KEY_MEM_ACC_LOGS_CARD_NO = "MemAccLogsStuCardNumber";
    private static final String KEY_MEM_ACC_LOGS_READER_ID = "MemAccLogsReaderId";
    private static final String KEY_MEM_ACC_LOGS_READER_NAME = "MemAccLogsReaderName";
    private static final String KEY_MEM_ACC_LOGS_SESSION_ID = "MemAccLogsSessionId";
    private static final String KEY_MEM_ACC_LOGS_SESSION_NAME = "MemAccLogsSessionName";
    private static final String KEY_MEM_ACC_LOGS_PUNCH_TIME = "MemAccLogsPunchTime";
    private static final String KEY_MEM_ACC_LOGS_ACCESS_GRANT = "MemAccLogsAccessGrant";
    private static final String KEY_MEM_ACC_LOGS_IS_SYNC = "MemAccLogsIsSync";

    // Access List logs entries
    private static final String KEY_ACC_LIST_AUTO_ID = "id";
    private static final String KEY_ACC_LIST_READERS_ID = "ReadersId";
    private static final String KEY_ACC_LIST_SESSIONS_ID = "SessionsId";
    private static final String KEY_ACC_LIST_CANTEEN_ID = "CanteenId";
    private static final String KEY_ACC_LIST_CANTEEN_NAME = "CanteenName";
    private static final String KEY_ACC_LIST_AG_ID = "AgId";
    private static final String KEY_ACC_LIST_STUDENTS_ID = "StudentsId";

    // Staff Access List logs entries
    private static final String KEY_STAFF_ACC_LIST_AUTO_ID = "id";
    private static final String KEY_STAFF_ACC_LIST_READERS_ID = "ReadersId";
    private static final String KEY_STAFF_ACC_LIST_SESSIONS_ID = "SessionsId";
    private static final String KEY_STAFF_ACC_LIST_STAFF_ID = "StaffsId";
    private static final String KEY_STAFF_ACC_LIST_CANTEEN_ID = "StaffsCanteenId";
    private static final String KEY_STAFF_ACC_LIST_CANTEEN_NAME = "StaffsCanteenName";
    private static final String KEY_STAFF_ACC_LIST_AG_ID = "StaffsAGId";

    // Sessions List entries
    private static final String KEY_SESS_AUTO_ID = "id";
    private static final String KEY_SESS_ID = "SessionId";
    private static final String KEY_SESS_NAME = "SessionName";
    private static final String KEY_SESS_DES= "Description";
    private static final String KEY_SESS_START_TIME = "StartTime";
    private static final String KEY_SESS_END_TIME = "EndTime";

    // Visitor entities
    private static final String KEY_VIS_AUTO_ID = "id";
    private static final String KEY_VIS_REG_NO = "VisitorRegNo";
    private static final String KEY_VIS_FIRST_NAME = "VisitorFirstName";
    private static final String KEY_VIS_LAST_NAME = "VisitorLasttName";
    private static final String KEY_VIS_COMPANY_NAME= "CompanyName";
    private static final String KEY_VIS_TYPE = "VisitorType";
    private static final String KEY_VIS_REASON= "VisitReason";
    private static final String KEY_VIS_PHONE_NO = "PhoneNo";
    private static final String KEY_VIS_EMAIL_ID = "EmailId";
    private static final String KEY_VIS_PHOTO = "VisitorPhoto";
    private static final String KEY_VIS_ACC_LEVEL = "VisitorAccessLevel";
    private static final String KEY_VIS_CHECK_IN_TIME = "VisitorCheckInTime";
    private static final String KEY_VIS_CHECK_OUT_TIME = "VisitorCheckOutTime";
    private static final String KEY_VIS_CARD_NO = "VisitorCardNumber";
    private static final String KEY_VIS_CARD_ST = "VisitorCardStatus";
    private static final String KEY_VIS_ST = "VisitorStatus";

    // AG entities
    private static final String KEY_AG_AUTO_ID = "id";
    private static final String KEY_AG_ID = "AGId";
    private static final String KEY_AG_NAME = "AGName";
    private static final String KEY_AG_DESC = "AGDescription";
    private static final String KEY_AG_IS_CANTEEN = "IsCanteen";
    private static final String KEY_AG_DGID = "DGId";
    private static final String KEY_AG_CANTEEN_TYPE = "CanteenType";
    private static final String KEY_AG_SESSION_ID = "SessionId";

    // MainGate access logs
    private static final String KEY_MAL_AUTO_ID = "id";
    private static final String KEY_MAL_ID = "MALId";
    private static final String KEY_MAL_STUD_ID = "MALStudentId";
    private static final String KEY_MAL_STUD_NAME = "MALStudentName";
    private static final String KEY_MAL_STUD_MEAL_NO = "MALStudentMealNo";
    private static final String KEY_MAL_STUD_CARD_NO = "AGId";
    private static final String KEY_MAL_GATE_ID = "MALGateId";
    private static final String KEY_MAL_GATE_NAME = "MALGateName";
    private static final String KEY_MAL_PUNCH_DATETIME = "MALPunchDateTime";
    private static final String KEY_MAL_PUNCH_TYPE = "MALPunchType";
    private static final String KEY_MAL_IS_SYNC = "MALISSync";

    // Directory name for the member access deleted logs
    public static String logsDir = "Logs";
    public static String logsFileNameMemAcc = "memacc_logs";
    public static final String TAG = "DatabaseHandler";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_READER_TABLE = "CREATE TABLE " + TABLE_READERS + "("
                + KEY_READER_AUTO_ID + "INTEGER PRIMARY KEY," + KEY_READER_ID + " INTEGER," + KEY_READER_NAME + " TEXT," + KEY_READER_TYPE + " TEXT,"  + KEY_MUSTERING_ID + " INTEGER" + ")";

        String CREATE_CAT_TABLE = "CREATE TABLE " + TABLE_CATEGORIES + "("
                + KEY_CAT_AUTO_ID + " INTEGER PRIMARY KEY," + KEY_CAT_ID + " INTEGER," + KEY_CAT_NAME + " TEXT" + ")";

        String CREATE_AREA_TABLE = "CREATE TABLE " + TABLE_AREAS + "("
                + KEY_AREA_AUTO_ID + " INTEGER PRIMARY KEY," + KEY_AREA_ID + " INTEGER,"
                + KEY_AREA_NAME + " TEXT" + ")";

        String CREATE_EMP_TABLE = "CREATE TABLE " + TABLE_EMPLOYEES + "("
                + KEY_EMP_AUTO_ID + " INTEGER PRIMARY KEY," + KEY_EMP_ID + " INTEGER,"
                + KEY_EMP_NAME + " TEXT," + KEY_EMP_PHOTO + " TEXT," + KEY_EMP_CARD_NO + " TEXT," + KEY_EMP_LOC_ID + " INTEGER" + ")";

        String CREATE_STU_INFO_TABLE = "CREATE TABLE " + TABLE_STU_INFO + "("
                + KEY_STU_AUTO_ID + " INTEGER PRIMARY KEY," + KEY_STU_ID + " INTEGER," + KEY_STU_FIRST_NAME + " TEXT," + KEY_STU_FATHER_NAME + " Text,"
                + KEY_STU_GRAND_FATHER_NAME + " TEXT," + KEY_STU_GENDER + " Text," + KEY_STU_DOB + " TEXT," + KEY_STU_SIG + " TEXT,"
                + KEY_STU_COLLEGE + " TEXT," + KEY_STU_DEPT + " TEXT," + KEY_STU_CAMPUS + " TEXT," + KEY_STU_PROGRAM + " TEXT,"
                + KEY_STU_DEGREE_TYPE + " TEXT," + KEY_STU_ADD_TYPE + " TEXT," + KEY_STU_ADD_TYPE_SHORT + " TEXT," + KEY_STU_VALID_DATE_UNTIL + " TEXT,"
                + KEY_STU_ISSUE_DATE + " TEXT," + KEY_STU_MEAL_NO + " TEXT," + KEY_STU_UNIQE_NO + " TEXT," + KEY_STU_STATUS + " TEXT,"
                + KEY_STU_ISACTIVE + " TEXT," + KEY_STU_CARD_NO + " TEXT," + KEY_STU_CARD_ST + " TEXT," + KEY_STU_ID_NO + " TEXT,"
                + KEY_STU_IMAGE + " TEXT" + ")";

        String CREATE_STAFF_INFO_TABLE = "CREATE TABLE " + TABLE_STAFF_INFO + "("
                + KEY_STAFF_AUTO_ID + " INTEGER PRIMARY KEY," + KEY_STAFF_ID + " INTEGER," + KEY_STAFF_APP_NO + " TEXT," + KEY_STAFF_SL_NO + " TEXT," + KEY_STAFF_UID + " Text,"
                + KEY_STAFF_FULL_NAME + " TEXT," + KEY_STAFF_GENDER + " Text," + KEY_STAFF_DOB + " TEXT," + KEY_STAFF_DEPT + " TEXT,"
                + KEY_STAFF_JOB_TITLE + " TEXT," + KEY_STAFF_EMP_PHOTO + " TEXT," + KEY_STAFF_SIG + " TEXT," + KEY_STAFF_ADD + " TEXT,"
                + KEY_STAFF_ID_NO + " TEXT," + KEY_STAFF_ISSUE_DATE + " TEXT," + KEY_STAFF_CARD_NO + " TEXT," + KEY_STAFF_CARD_ST + " TEXT,"
                + KEY_STAFF_EMAIL_ID + " TEXT," + KEY_STAFF_ISACTIVE + " TEXT," + KEY_STAFF_PASSWORD + " TEXT," + KEY_STAFF_ID_NUMBER + " TEXT" + ")";

        String CREATE_PUNCHLOGS_TABLE = "CREATE TABLE " + TABLE_PUNCHLOGS + "(" + KEY_PUNCHLOGS_AUTO_ID + " INTEGER PRIMARY KEY,"
                + KEY_PUNCHLOGS_SLN_PUNCH + " INTEGER," + KEY_PUNCHLOGS_EMP_ID + " INTEGER," + KEY_PUNCHLOGS_CARD_NO + " TEXT,"
                + KEY_PUNCHLOGS_SLN_READER + " INTEGER," + KEY_PUNCHLOGS_READER_NAME + " TEXT," + KEY_PUNCHLOGS_DATETIME + " TEXT," + KEY_PUNCHLOGS_GRANT_DENY + " INTEGER,"
                + KEY_PUNCHLOGS_LOC_ID + " INTEGER," + KEY_PUNCHLOGS_AREA_ID + " INTEGER," + KEY_PUNCHLOGS_CAT_ID + " INTEGER" + ")";

        String CREATE_MEM_ACC_TABLE = "CREATE TABLE " + TABLE_MEM_ACC + "("
                + KEY_MEM_ACC_AUTO_ID + " INTEGER PRIMARY KEY," + KEY_MEM_ACC_EMP_ID + " INTEGER,"
                + KEY_MEM_ACC_CARD_NO + " TEXT," + KEY_MEM_ACC_READER_ID + " INTEGER" + ")";

        String CREATE_SESSION_TABLE = "CREATE TABLE " + TABLE_SESSION + "("
                + KEY_SESSION_AUTO_ID + " INTEGER PRIMARY KEY," + KEY_SESSION_ID + " INTEGER," + KEY_SESSION_NAME + " TEXT," + KEY_SESSION_DATE + " INTEGER,"
                + KEY_SESSION_FROM_DATETIME + " INTEGER," + KEY_SESSION_TO_DATETIME + " INTEGER" + ")";

        String CREATE_MEM_ACC_LOGS_TABLE = "CREATE TABLE " + TABLE_MEM_ACC_LOGS + "("
                + KEY_MEM_ACC_LOGS_AUTO_ID + " INTEGER PRIMARY KEY," + KEY_MEM_ACC_LOGS_STU_ID + " TEXT,"
                + KEY_MEM_ACC_LOGS_CARD_NO + " TEXT," + KEY_MEM_ACC_LOGS_READER_ID + " INTEGER,"
                + KEY_MEM_ACC_LOGS_READER_NAME + " TEXT," + KEY_MEM_ACC_LOGS_SESSION_ID + " INTEGER,"
                + KEY_MEM_ACC_LOGS_SESSION_NAME + " TEXT,"+ KEY_MEM_ACC_LOGS_PUNCH_TIME + " TEXT,"
                + KEY_MEM_ACC_LOGS_ACCESS_GRANT + " INTEGER," + KEY_MEM_ACC_LOGS_IS_SYNC + " INTEGER" + ")";

        String CREATE_ACCESS_LIST_TABLE = "CREATE TABLE " + TABLE_ACCESS_LIST + "("
                + KEY_ACC_LIST_AUTO_ID + " INTEGER PRIMARY KEY," + KEY_ACC_LIST_READERS_ID + " TEXT," + KEY_ACC_LIST_SESSIONS_ID + " TEXT," + KEY_ACC_LIST_CANTEEN_ID + " TEXT," + KEY_ACC_LIST_CANTEEN_NAME + " TEXT," + KEY_ACC_LIST_AG_ID + " TEXT," + KEY_ACC_LIST_STUDENTS_ID + " TEXT" + ")";

        String CREATE_STAFF_ACCESS_LIST_TABLE = "CREATE TABLE " + TABLE_STAFF_ACCESS_LIST + "("
                + KEY_STAFF_ACC_LIST_AUTO_ID + " INTEGER PRIMARY KEY," + KEY_STAFF_ACC_LIST_READERS_ID + " TEXT," + KEY_STAFF_ACC_LIST_SESSIONS_ID + " TEXT," + KEY_STAFF_ACC_LIST_CANTEEN_ID + " TEXT," + KEY_STAFF_ACC_LIST_CANTEEN_NAME + " TEXT," + KEY_STAFF_ACC_LIST_AG_ID + " TEXT," + KEY_STAFF_ACC_LIST_STAFF_ID + " TEXT" + ")";

        String CREATE_SES_TABLE = "CREATE TABLE " + TABLE_SESS + "("
                + KEY_SESS_AUTO_ID + " INTEGER PRIMARY KEY," + KEY_SESS_ID + " TEXT," + KEY_SESS_NAME + " TEXT,"+ KEY_SESS_DES + " TEXT," + KEY_SESS_START_TIME + " Text," + KEY_SESS_END_TIME + " TEXT" + ")";

        String CREATE_VISITOR_TABLE = "CREATE TABLE " + TABLE_VISITOR_INFO + "("
                + KEY_VIS_AUTO_ID + " INTEGER PRIMARY KEY," + KEY_VIS_REG_NO + " TEXT," + KEY_VIS_FIRST_NAME + " TEXT,"+ KEY_VIS_LAST_NAME + " TEXT," + KEY_VIS_COMPANY_NAME + " Text," + KEY_VIS_TYPE + " Text,"
                + KEY_VIS_REASON + " TEXT," + KEY_VIS_PHONE_NO + " TEXT," + KEY_VIS_EMAIL_ID +" TEXT," + KEY_VIS_PHOTO + " TEXT," + KEY_VIS_ACC_LEVEL + " TEXT," + KEY_VIS_CHECK_IN_TIME + " TEXT," + KEY_VIS_CHECK_OUT_TIME + " TEXT," + KEY_VIS_CARD_NO  + " TEXT," + KEY_VIS_CARD_ST + " TEXT,"
                + KEY_VIS_ST + " TEXT" + ")";

        String CREATE_AG_TABLE = "CREATE TABLE " + TABLE_AG_INFO + "("
                + KEY_AG_AUTO_ID + " INTEGER PRIMARY KEY," + KEY_AG_ID + " INTEGER," + KEY_AG_NAME + " TEXT,"+ KEY_AG_DESC + " TEXT," + KEY_AG_IS_CANTEEN + " Text," + KEY_AG_DGID + " Text," + KEY_AG_CANTEEN_TYPE + " Text,"
                + KEY_AG_SESSION_ID + " TEXT" + ")";

        String CREATE_MAL_TABLE = "CREATE TABLE " + TABLE_MAIN_GATE_LOGS + "("
                + KEY_MAL_AUTO_ID + " INTEGER PRIMARY KEY, " + KEY_MAL_ID + " INTEGER," + KEY_MAL_STUD_ID + " TEXT," + KEY_MAL_STUD_NAME + " TEXT,"
                + KEY_MAL_STUD_MEAL_NO + " TEXT," + KEY_MAL_STUD_CARD_NO + " TEXT," + KEY_MAL_GATE_ID + " INTEGER," + KEY_MAL_GATE_NAME + " TEXT,"
                + KEY_MAL_PUNCH_DATETIME + " TEXT," + KEY_MAL_PUNCH_TYPE + " TEXT,"+ KEY_MAL_IS_SYNC + " INTEGER" + ")";


        db.execSQL(CREATE_READER_TABLE);
        db.execSQL(CREATE_CAT_TABLE);
        db.execSQL(CREATE_AREA_TABLE);
        db.execSQL(CREATE_EMP_TABLE);
        db.execSQL(CREATE_STU_INFO_TABLE);
        db.execSQL(CREATE_STAFF_INFO_TABLE);
        db.execSQL(CREATE_PUNCHLOGS_TABLE);
        db.execSQL(CREATE_MEM_ACC_TABLE);
        db.execSQL(CREATE_SESSION_TABLE);
        db.execSQL(CREATE_MEM_ACC_LOGS_TABLE);
        db.execSQL(CREATE_ACCESS_LIST_TABLE);
        db.execSQL(CREATE_STAFF_ACCESS_LIST_TABLE);
        db.execSQL(CREATE_SES_TABLE);
        db.execSQL(CREATE_VISITOR_TABLE);
        db.execSQL(CREATE_AG_TABLE);
        db.execSQL(CREATE_MAL_TABLE);

    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_READERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AREAS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EMPLOYEES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STU_INFO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STAFF_INFO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PUNCHLOGS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEM_ACC);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SESSION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEM_ACC_LOGS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCESS_LIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STAFF_ACCESS_LIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SESS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VISITOR_INFO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AG_INFO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MAIN_GATE_LOGS);
        // Create tables again
        onCreate(db);
    }

    // -------------------------------------------- FOR READERS -----------------------------------------------------------------
    public void addReaders(PunchLogs reader) {
        SQLiteDatabase db = this.getWritableDatabase();
        // content values for categories
        ContentValues values_cat = new ContentValues();
        values_cat.put(KEY_READER_ID, reader.getSLN_Reader());
        values_cat.put(KEY_READER_NAME, reader.getReader_Name());
        values_cat.put(KEY_MUSTERING_ID, reader.getMusteringId());
        if (!isReaderAlreadyExists(db, reader.getSLN_Reader())) {
            db.insert(TABLE_READERS, null, values_cat);
        } else {
            db.update(TABLE_READERS, values_cat, KEY_READER_ID + "=" + reader.getSLN_Reader(), null);
        }
        db.close();
    }
    // Batch Insert for readers
    /*public void saveReaders (ArrayList<PunchLogs> data) {
        SQLiteDatabase db = this.getWritableDatabase();
        // content values for categories
        ContentValues values_cat = new ContentValues();
        db.beginTransaction();
        for (int i = 0; i < data.size(); i++) {
            PunchLogs reader = data.get(i);//new Categories();
            int reader_id = reader.getSLN_Reader();//dataObj.getInt(APIParam.RES_CAT_ID);
            Log.d(TAG, "ReaderId: " + reader_id);
            String reader_name = reader.getReader_Name();//dataObj.getString(APIParam.RES_CAT_NAME);
            Log.d(TAG, "ReaderName: " + reader_name);
            int mustering_id = reader.getMusteringId();
            Log.d(TAG, "Mustering ID: "+mustering_id);


            values_cat.put(KEY_READER_ID, reader.getSLN_Reader());
            values_cat.put(KEY_READER_NAME, reader.getReader_Name());
            values_cat.put(KEY_MUSTERING_ID, reader.getMusteringId());
            if (!isReaderAlreadyExists(db, reader.getSLN_Reader())) {
                db.insert(TABLE_READERS, null, values_cat);
            } else {
                db.update(TABLE_READERS, values_cat, KEY_READER_ID + "=" + reader.getSLN_Reader(), null);
            }


            //categoriesListArray[i] = cat_name;
            //db.addReaders(reader);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }*/

    // Batch Insert for readers
    public void saveReaders (ArrayList<ReaderInfoModel.DataBean> data) {
        SQLiteDatabase db = this.getWritableDatabase();
        //deleteAllReaders();
        // content values for categories
        ContentValues values_cat = new ContentValues();
        db.beginTransaction();
        for (int i = 0; i < data.size(); i++) {
            ReaderInfoModel.DataBean reader = data.get(i);//new Categories();
            int reader_id = reader.getReaderId();
            Log.d(TAG, "ReaderId: " + reader_id);
            String reader_name = reader.getReaderName();//dataObj.getString(APIParam.RES_CAT_NAME);
            Log.d(TAG, "ReaderName: " + reader_name);
            String reader_type = reader.getReaderType();
            //int mustering_id = reader.getMusteringId();
            //Log.d(TAG, "Mustering ID: "+mustering_id);


            values_cat.put(KEY_READER_ID, reader_id);
            values_cat.put(KEY_READER_NAME, reader_name);
            values_cat.put(KEY_READER_TYPE, reader_type);
            if (!isReaderAlreadyExists(db, reader.getReaderId())) {
                db.insert(TABLE_READERS, null, values_cat);
            } else {
                db.update(TABLE_READERS, values_cat, KEY_READER_ID + "=" + reader.getReaderId(), null);
            }


            //categoriesListArray[i] = cat_name;
            //db.addReaders(reader);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public boolean checkReaders() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_READERS, null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public boolean isReaderAlreadyExists(SQLiteDatabase db, int reader_id) {
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_READERS + " WHERE " + KEY_READER_ID + "=" + reader_id, null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public void deleteAllReaders() {
        SQLiteDatabase db = this.getWritableDatabase();
        if (checkReaders()) {
            db.delete(TABLE_READERS, null, null);
        }
    }

    /*public ArrayList<PunchLogs> getAllReaders() {
        ArrayList<PunchLogs> reader_list = new ArrayList<PunchLogs>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_READERS + " ORDER BY " + KEY_MUSTERING_ID, null);
        if (cur != null && cur.getCount() > 0) {
            PunchLogs catall = new PunchLogs();
            //catall.setSLN_Reader(-1);
            //catall.setMusteringName("All");
            //cat_list.add(catall);
            if (cur.moveToFirst()) {
                do {
                    PunchLogs reader = new PunchLogs();
                    reader.setSLN_Reader(cur.getInt(cur.getColumnIndexOrThrow(KEY_READER_ID)));
                    reader.setReader_Name(cur.getString(cur.getColumnIndexOrThrow(KEY_READER_NAME)));
                    reader.setMusteringId(cur.getInt(cur.getColumnIndexOrThrow(KEY_MUSTERING_ID)));
                    reader_list.add(reader);
                } while (cur.moveToNext());
            }
            //return cat_list;
        }
        cur.close();
        db.close();
        return reader_list;
    }*/

    public ArrayList<ReaderInfoModel.DataBean> getAllReaders() {
        ArrayList<ReaderInfoModel.DataBean> reader_list = new ArrayList<ReaderInfoModel.DataBean>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_READERS, null);
        if (cur != null && cur.getCount() > 0) {
            //PunchLogs catall = new PunchLogs();
            //catall.setSLN_Reader(-1);
            //catall.setMusteringName("All");
            //cat_list.add(catall);
            if (cur.moveToFirst()) {
                do {
                    ReaderInfoModel.DataBean reader = new ReaderInfoModel.DataBean();
                    reader.setReaderId(cur.getInt(cur.getColumnIndexOrThrow(KEY_READER_ID)));
                    reader.setReaderName(cur.getString(cur.getColumnIndexOrThrow(KEY_READER_NAME)));
                    reader.setReaderType(cur.getString(cur.getColumnIndexOrThrow(KEY_READER_TYPE)));
                    reader_list.add(reader);
                } while (cur.moveToNext());
            }
            //return cat_list;
        }
        cur.close();
        db.close();
        return reader_list;
    }

    // -------------------------------------------- FOR CATEGORIES ------------------------------------------------------------------------
    public void addCategories(Categories categories) {
        SQLiteDatabase db = this.getWritableDatabase();
        // content values for categories
        ContentValues values_cat = new ContentValues();
        values_cat.put(KEY_CAT_ID, categories.getMusteringId());
        values_cat.put(KEY_CAT_NAME, categories.getMusteringName());
        if (!isCategoryAlreadyExists(db, categories.getMusteringId())) {
            db.insert(TABLE_CATEGORIES, null, values_cat);
        } else {
            db.update(TABLE_CATEGORIES, values_cat, KEY_CAT_ID + "=" + categories.getMusteringId(), null);
        }
        db.close();
    }
    // Barch insert categories
    public void saveCategories (ArrayList<Categories> data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values_cat = new ContentValues();
        db.beginTransaction();
        for (Categories categories: data) {

            values_cat.put(KEY_CAT_ID, categories.getMusteringId());
            values_cat.put(KEY_CAT_NAME, categories.getMusteringName());
            if (!isCategoryAlreadyExists(db, categories.getMusteringId())) {
                db.insert(TABLE_CATEGORIES, null, values_cat);
            } else {
                db.update(TABLE_CATEGORIES, values_cat, KEY_CAT_ID + "=" + categories.getMusteringId(), null);
            }

        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public boolean checkCategories() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_CATEGORIES, null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public boolean isCategoryAlreadyExists(SQLiteDatabase db, int cat_id) {
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_CATEGORIES + " WHERE " + KEY_CAT_ID + "=" + cat_id, null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public void deleteAllCategories() {
        SQLiteDatabase db = this.getWritableDatabase();
        if (checkCategories()) {
            db.delete(TABLE_CATEGORIES, null, null);
        }
    }

    public ArrayList<Categories> getCategories() {
        ArrayList<Categories> cat_list = new ArrayList<Categories>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_CATEGORIES, null);
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    Categories cat = new Categories();
                    cat.setMusteringId(cur.getInt(cur.getColumnIndexOrThrow(KEY_CAT_ID)));
                    cat.setMusteringName(cur.getString(cur.getColumnIndexOrThrow(KEY_CAT_NAME)));
                    cat_list.add(cat);
                } while (cur.moveToNext());
            }
            //return cat_list;
        }
        cur.close();
        //db.close();
        return cat_list;
    }

    public ArrayList<Categories> getAllCategories() {
        ArrayList<Categories> cat_list = new ArrayList<Categories>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_CATEGORIES, null);
        if (cur != null && cur.getCount() > 0) {
            Categories catall = new Categories();
            catall.setMusteringId(-1);
            catall.setMusteringName("All");
            cat_list.add(catall);
            if (cur.moveToFirst()) {
                do {
                    Categories cat = new Categories();
                    cat.setMusteringId(cur.getInt(cur.getColumnIndexOrThrow(KEY_CAT_ID)));
                    cat.setMusteringName(cur.getString(cur.getColumnIndexOrThrow(KEY_CAT_NAME)));
                    cat_list.add(cat);
                } while (cur.moveToNext());
            }
            //return cat_list;
        }
        cur.close();
        //db.close();
        return cat_list;
    }

    public boolean isAreaAlreadyExists(SQLiteDatabase db, int area_id) {
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_AREAS + " WHERE " + KEY_AREA_ID + "=" + area_id, null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public void addAreas(Areas areas) {
        SQLiteDatabase db = this.getWritableDatabase();
        // content values for categories
        ContentValues values_area = new ContentValues();
        values_area.put(KEY_AREA_ID, areas.getSLN_Area_ID());
        values_area.put(KEY_AREA_NAME, areas.getArea_Name());
        if (!isAreaAlreadyExists(db, areas.getSLN_Area_ID())) {
            db.insert(TABLE_AREAS, null, values_area);
        } else {
            db.update(TABLE_AREAS, values_area, KEY_AREA_ID + "=" + areas.getSLN_Area_ID(), null);
        }
        db.close();
    }
    // batch insert aread
    public void saveAreas (ArrayList<Areas> data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values_area = new ContentValues();
        db.beginTransaction();
        for (Areas areas : data) {
            values_area.put(KEY_AREA_ID, areas.getSLN_Area_ID());
            values_area.put(KEY_AREA_NAME, areas.getArea_Name());
            if (!isAreaAlreadyExists(db, areas.getSLN_Area_ID())) {
                db.insert(TABLE_AREAS, null, values_area);
            } else {
                db.update(TABLE_AREAS, values_area, KEY_AREA_ID + "=" + areas.getSLN_Area_ID(), null);
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public boolean checkAreas() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_AREAS, null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public void deleteAllAreas() {
        SQLiteDatabase db = this.getWritableDatabase();
        if (checkAreas()) {
            db.delete(TABLE_AREAS, null, null);
        }
    }

    public ArrayList<Areas> getAreas() {
        ArrayList<Areas> area_list = new ArrayList<Areas>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_AREAS, null);
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    Areas area = new Areas();
                    area.setSLN_Area_ID(cur.getInt(cur.getColumnIndexOrThrow(KEY_AREA_ID)));
                    area.setArea_Name(cur.getString(cur.getColumnIndexOrThrow(KEY_AREA_NAME)));
                    area_list.add(area);
                } while (cur.moveToNext());
            }
            //return cat_list;
        }
        cur.close();
        db.close();
        return area_list;
    }

    public ArrayList<Areas> getAllAreas() {
        ArrayList<Areas> area_list = new ArrayList<Areas>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_AREAS, null);
        if (cur != null && cur.getCount() > 0) {
            Areas area_all = new Areas();
            area_all.setSLN_Area_ID(-1);
            area_all.setArea_Name("All");
            area_list.add(area_all);
            if (cur.moveToFirst()) {
                do {
                    Areas area = new Areas();
                    area.setSLN_Area_ID(cur.getInt(cur.getColumnIndexOrThrow(KEY_AREA_ID)));
                    area.setArea_Name(cur.getString(cur.getColumnIndexOrThrow(KEY_AREA_NAME)));
                    area_list.add(area);
                } while (cur.moveToNext());
            }
            //return cat_list;
        }
        cur.close();
        //db.close();
        return area_list;
    }

    // ------------------------- AG ----------------------------------
    public void saveAGInfo(Context mContext, ArrayList<AGInfoModel.DataBean> data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values_ag = new ContentValues();
        db.beginTransaction();

        String st_image = "";
        for (int i = 0; i < data.size(); i++) {
            AGInfoModel.DataBean agInfo = data.get(i);

            int agId = agInfo.getAgId();
            String agName = agInfo.getAgName();
            String agDesc = agInfo.getAgDescription();
            String isCanteen = agInfo.getIsCanteen();
            String dgId = agInfo.getDgId();
            String canteenType = agInfo.getCanteenType();
            String sessionId = agInfo.getSessionId();

            if (sessionId != null) {
                if (sessionId.contains(",")) {
                    String[] sessionID = sessionId.split(",");
                    for (int j = 0; j < sessionID.length; j++) {
                        String session_id = sessionID[j];

                        values_ag.put(KEY_AG_ID, agId);
                        values_ag.put(KEY_AG_NAME, agName);
                        values_ag.put(KEY_AG_DESC, agDesc);
                        values_ag.put(KEY_AG_IS_CANTEEN, isCanteen);
                        values_ag.put(KEY_AG_DGID, dgId);
                        values_ag.put(KEY_AG_CANTEEN_TYPE, canteenType);
                        values_ag.put(KEY_AG_SESSION_ID, session_id);
                        if (!isAGAlreadyExists(db, agId, session_id)) {
                            db.insert(TABLE_AG_INFO, null, values_ag);
                        } else {
                            db.update(TABLE_AG_INFO, values_ag, KEY_AG_ID + "=" + agId + " AND " + KEY_AG_SESSION_ID + "='" + session_id + "'", null);
                        }

                    }
                } else {
                    String session_id = sessionId;
                    values_ag.put(KEY_AG_ID, agId);
                    values_ag.put(KEY_AG_NAME, agName);
                    values_ag.put(KEY_AG_DESC, agDesc);
                    values_ag.put(KEY_AG_IS_CANTEEN, isCanteen);
                    values_ag.put(KEY_AG_DGID, dgId);
                    values_ag.put(KEY_AG_CANTEEN_TYPE, canteenType);
                    values_ag.put(KEY_AG_SESSION_ID, session_id);
                    if (!isAGAlreadyExists(db, agId, session_id)) {
                        db.insert(TABLE_AG_INFO, null, values_ag);
                    } else {
                        db.update(TABLE_AG_INFO, values_ag, KEY_AG_ID + "=" + agId + " AND " + KEY_AG_SESSION_ID + "='" + session_id + "'", null);
                    }
                }
            } else {
                values_ag.put(KEY_AG_ID, agId);
                values_ag.put(KEY_AG_NAME, agName);
                values_ag.put(KEY_AG_DESC, agDesc);
                values_ag.put(KEY_AG_IS_CANTEEN, isCanteen);
                values_ag.put(KEY_AG_DGID, dgId);
                values_ag.put(KEY_AG_CANTEEN_TYPE, canteenType);
                values_ag.put(KEY_AG_SESSION_ID, ""); // is sessionId is null inster empty value

                if (!isAGAlreadyExists(db, agId)) {
                    db.insert(TABLE_AG_INFO, null, values_ag);
                } else {
                    db.update(TABLE_AG_INFO, values_ag, KEY_AG_ID + "=" + agId + " AND " + KEY_AG_SESSION_ID + "=''", null);
                }
            }
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public boolean isAGAlreadyExists (SQLiteDatabase db, int agId, String sessionId) {
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_AG_INFO + " WHERE " + KEY_AG_ID + "=" + agId + " AND "+ KEY_AG_SESSION_ID + "='" + sessionId + "'", null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public boolean isAGAlreadyExists (SQLiteDatabase db, int agId) {
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_AG_INFO + " WHERE " + KEY_AG_ID + "=" + agId, null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public boolean checkAGInfo() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_AG_INFO, null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            //db.close();
            return true;
        }
        cur.close();
        //db.close();
        return false;
    }

    public void deleteAllAGs() {
        SQLiteDatabase db = this.getWritableDatabase();
        if (checkAGInfo()) {
            db.delete(TABLE_AG_INFO, null, null);
        }
        //db.close();
    }

    public ArrayList<AGInfoModel.DataBean> getAllAGInfo () {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<AGInfoModel.DataBean> ag_list = new ArrayList<AGInfoModel.DataBean>();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_AG_INFO , null);
        if (cur != null && cur.getCount() > 0) {

            if  (cur.moveToFirst()) {
                do {
                    AGInfoModel.DataBean aim = new AGInfoModel.DataBean();
                    aim.setAgId(cur.getInt(cur.getColumnIndexOrThrow(KEY_AG_ID)));
                    aim.setAgName(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_NAME)));
                    aim.setAgDescription(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_DESC)));
                    aim.setIsCanteen(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_IS_CANTEEN)));
                    aim.setDgId(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_DGID)));
                    aim.setCanteenType(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_CANTEEN_TYPE)));
                    aim.setSessionId(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_SESSION_ID)));

                    ag_list.add(aim);

                } while(cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return ag_list;
    }

    public ArrayList<AGInfoModel.DataBean> getAllUniqueAGInfo () {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<AGInfoModel.DataBean> ag_list = new ArrayList<AGInfoModel.DataBean>();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_AG_INFO + " GROUP BY " + KEY_AG_ID, null);
        if (cur != null && cur.getCount() > 0) {

            if  (cur.moveToFirst()) {
                do {
                    AGInfoModel.DataBean aim = new AGInfoModel.DataBean();
                    aim.setAgId(cur.getInt(cur.getColumnIndexOrThrow(KEY_AG_ID)));
                    aim.setAgName(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_NAME)));
                    aim.setAgDescription(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_DESC)));
                    aim.setIsCanteen(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_IS_CANTEEN)));
                    aim.setDgId(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_DGID)));
                    aim.setCanteenType(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_CANTEEN_TYPE)));
                    aim.setSessionId(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_SESSION_ID)));

                    ag_list.add(aim);

                } while(cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return ag_list;
    }

    // Get only Gates AG
    public ArrayList<AGInfoModel.DataBean> getAllMainGatesAGInfo () {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<AGInfoModel.DataBean> ag_list = new ArrayList<AGInfoModel.DataBean>();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_AG_INFO + " WHERE " + KEY_AG_IS_CANTEEN + "='0'" + " GROUP BY " + KEY_AG_ID, null);
        if (cur != null && cur.getCount() > 0) {

            if  (cur.moveToFirst()) {
                do {
                    AGInfoModel.DataBean aim = new AGInfoModel.DataBean();
                    aim.setAgId(cur.getInt(cur.getColumnIndexOrThrow(KEY_AG_ID)));
                    aim.setAgName(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_NAME)));
                    aim.setAgDescription(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_DESC)));
                    aim.setIsCanteen(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_IS_CANTEEN)));
                    aim.setDgId(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_DGID)));
                    aim.setCanteenType(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_CANTEEN_TYPE)));
                    aim.setSessionId(cur.getString(cur.getColumnIndexOrThrow(KEY_AG_SESSION_ID)));

                    ag_list.add(aim);

                } while(cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return ag_list;
    }

    // ------------------------- Student ---------------------------------
    String img_path = "";
    public void saveStudentInfo(Context mContext, ArrayList<StudentInfoModel.DataBean> data) {

        //deleteAllStudents();
        // content values for categories
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values_cat = new ContentValues();
        db.beginTransaction();

        String st_image = "";
        for (int i = 0; i < data.size(); i++) {
            StudentInfoModel.DataBean studInfo = data.get(i);

            String stud_id = studInfo.getStudentID();
            String stud_id1 = stud_id.replaceAll("/", "_");
            String stud_image = studInfo.getStudentImage();

            if (stud_image != null) {
                if (!stud_image.isEmpty()) {
                    byte[] byteImage = Base64.decode(stud_image, Base64.DEFAULT);
                    //Bitmap bmp = BitmapFactory.decodeByteArray(byteImage, 0, byteImage.length);
                    SaveImages si = new SaveImages();
                    img_path = si.saveToInternalStorage(mContext, si.decodeSampledBitmapFromByte(byteImage, 100, 100), stud_id.replaceAll("/", "_"));
                } else {
                    img_path = "";
                }
            } else {
                img_path = "";
            }

            /*String baseUrl = SharedPref.INSTANCE.getStringValue(mContext, KeysKt.keyBaseUrl, "");
            String stud_image1 = ""*//*baseUrl + stud_image*//*;

            if(stud_image!=null && !stud_image.isEmpty())
            {
                stud_image1 = baseUrl + stud_image ;

                AppUtilsKt.ImageToLocal(mContext,stud_image1,null,"StudentImage");

            }
            else
                stud_image1 = "";

            System.out.println("Student Image:"+stud_image1);*/



            Log.d(TAG, "StudentId: " + studInfo.getStudentID());
            String stud_firstname = studInfo.getFirstName();
            Log.d(TAG, "StudentName: " + stud_firstname);
            String stud_fathername = studInfo.getFatherName();
            String stud_grandfathername = studInfo.getGrandFatherName();
            String stud_gender = studInfo.getGender();
            String stud_dob = studInfo.getDateOfBirth();
            if (stud_dob != null) {
                if (stud_dob.equals("1900-01-01T00:00:00")) {
                    stud_dob = "";
                }
            }
            String stud_signature = studInfo.getSignature();
            String stud_College = studInfo.getCollege();
            String stud_Department = studInfo.getDepartment();
            String stud_Campus = studInfo.getCampus();
            String stud_Program = studInfo.getProgram();
            String stud_DegreeType = studInfo.getDegreeType();
            String stud_admissiontype = studInfo.getAdmissionType();
            String stud_admissionTypeShort = studInfo.getAdmissionTypeShort();
            String stud_validDateUntil = studInfo.getValidDateUntil();
            String stud_issueDate = studInfo.getIssueDate();
            String stud_mealno = studInfo.getMealNumber();
            String stud_uniqueno = studInfo.getUniqueNo();
            String stud_status = studInfo.getStatus();
            String stud_isactive = studInfo.getIsactive();
            String stud_cardno = studInfo.getCardNumber();
            String stud_cardst = studInfo.getCardstatus();


            //ContentValues values_cat = new ContentValues();
            values_cat.put(KEY_STU_ID, studInfo.getStudentID());
            values_cat.put(KEY_STU_FIRST_NAME, stud_firstname);
            values_cat.put(KEY_STU_FATHER_NAME, stud_fathername);
            values_cat.put(KEY_STU_GRAND_FATHER_NAME, stud_grandfathername);
            values_cat.put(KEY_STU_GENDER, stud_gender);
            values_cat.put(KEY_STU_DOB, stud_dob);
            values_cat.put(KEY_STU_SIG, stud_signature);
            values_cat.put(KEY_STU_COLLEGE, stud_College);
            values_cat.put(KEY_STU_DEPT, stud_Department);
            values_cat.put(KEY_STU_CAMPUS, stud_Campus);
            values_cat.put(KEY_STU_PROGRAM, stud_Program);
            values_cat.put(KEY_STU_DEGREE_TYPE, stud_DegreeType);
            values_cat.put(KEY_STU_ADD_TYPE, stud_admissiontype);
            values_cat.put(KEY_STU_ADD_TYPE_SHORT, stud_admissionTypeShort);
            values_cat.put(KEY_STU_VALID_DATE_UNTIL, stud_validDateUntil);
            values_cat.put(KEY_STU_ISSUE_DATE, stud_issueDate);
            values_cat.put(KEY_STU_MEAL_NO, stud_mealno);
            values_cat.put(KEY_STU_UNIQE_NO, stud_uniqueno);
            values_cat.put(KEY_STU_STATUS, stud_status);
            values_cat.put(KEY_STU_ISACTIVE, stud_isactive);
            values_cat.put(KEY_STU_CARD_NO, stud_cardno);
            values_cat.put(KEY_STU_CARD_ST, stud_cardst);
            values_cat.put(KEY_STU_IMAGE, img_path);


            if (!isStudentAlreadyExist(db, studInfo.getStudentID())) {
                db.insert(TABLE_STU_INFO, null, values_cat);
            } else {
                db.update(TABLE_STU_INFO, values_cat, KEY_STU_ID + "='" + studInfo.getStudentID() + "'", null);
            }

            //SQLiteDatabase db = this.getWritableDatabase();
            //new DownloadImagesTask(mContext, studInfo, db).execute(stud_id);
            //FutureTarget<Bitmap> ft = Glide.with(mContext).asBitmap().load(stud_image1).submit(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
            /*img_path = "";


                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            Bitmap bmp = ft.get();
                            SaveImages si = new SaveImages();
                            img_path = si.saveToInternalStorage(mContext, bmp, stud_id.replaceAll("/", "_"));
                            System.out.println("Local Image: "+img_path);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();*/


        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public class DownloadImagesTask extends AsyncTask<String, Void, String> {

        Context mContext;
        StudentInfoModel.DataBean studInfo;
        SQLiteDatabase db;
        String studId;
        public DownloadImagesTask(Context mContext, StudentInfoModel.DataBean studInfo, final SQLiteDatabase db) {
            this.mContext = mContext;
            this.studInfo = studInfo;
            this.db = mContext.getApplicationContext().openOrCreateDatabase(DATABASE_NAME, SQLiteDatabase.OPEN_READWRITE, null);


        }
        @Override
        protected String doInBackground(String... strings) {
            studId = studInfo.getStudentID();
            String stud_image = studInfo.getStudentImage();
            String baseUrl = SharedPref.INSTANCE.getStringValue(mContext, KeysKt.keyBaseUrl, "");
            String stud_image1 = baseUrl + stud_image;
            System.out.println("Student Image:"+stud_image1);

            FutureTarget<Bitmap> ft = Glide.with(mContext).asBitmap().load(stud_image1).submit(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
            SaveImages si = new SaveImages();
            try {
                img_path = si.saveToInternalStorage(mContext, ft.get(), studId.replaceAll("/", "_"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return img_path;
        }

        @Override
        protected void onPostExecute(String imgPath) {
            super.onPostExecute(imgPath);
            System.out.println("Local Image: "+imgPath);

            Log.d(TAG, "StudentId: " + studInfo.getStudentID());
            String stud_firstname = studInfo.getFirstName();
            Log.d(TAG, "StudentName: " + stud_firstname);
            String stud_fathername = studInfo.getFatherName();
            String stud_grandfathername = studInfo.getGrandFatherName();
            String stud_gender = studInfo.getGender();
            String stud_dob = studInfo.getDateOfBirth();
            String stud_signature = studInfo.getSignature();
            String stud_College = studInfo.getCollege();
            String stud_Department = studInfo.getDepartment();
            String stud_Campus = studInfo.getCampus();
            String stud_Program = studInfo.getProgram();
            String stud_DegreeType = studInfo.getDegreeType();
            String stud_admissiontype = studInfo.getAdmissionType();
            String stud_admissionTypeShort = studInfo.getAdmissionTypeShort();
            String stud_validDateUntil = studInfo.getValidDateUntil();
            String stud_issueDate = studInfo.getIssueDate();
            String stud_mealno = studInfo.getMealNumber();
            String stud_uniqueno = studInfo.getUniqueNo();
            String stud_status = studInfo.getStatus();
            String stud_isactive = studInfo.getIsactive();
            String stud_cardno = studInfo.getCardNumber();
            String stud_cardst = studInfo.getCardstatus();


            ContentValues values_cat = new ContentValues();
            values_cat.put(KEY_STU_ID, studInfo.getStudentID());
            values_cat.put(KEY_STU_FIRST_NAME, stud_firstname);
            values_cat.put(KEY_STU_FATHER_NAME, stud_fathername);
            values_cat.put(KEY_STU_GRAND_FATHER_NAME, stud_grandfathername);
            values_cat.put(KEY_STU_GENDER, stud_gender);
            values_cat.put(KEY_STU_DOB, stud_dob);
            values_cat.put(KEY_STU_SIG, stud_signature);
            values_cat.put(KEY_STU_COLLEGE, stud_College);
            values_cat.put(KEY_STU_DEPT, stud_Department);
            values_cat.put(KEY_STU_CAMPUS, stud_Campus);
            values_cat.put(KEY_STU_PROGRAM, stud_Program);
            values_cat.put(KEY_STU_DEGREE_TYPE, stud_DegreeType);
            values_cat.put(KEY_STU_ADD_TYPE, stud_admissiontype);
            values_cat.put(KEY_STU_ADD_TYPE_SHORT, stud_admissionTypeShort);
            values_cat.put(KEY_STU_VALID_DATE_UNTIL, stud_validDateUntil);
            values_cat.put(KEY_STU_ISSUE_DATE, stud_issueDate);
            values_cat.put(KEY_STU_MEAL_NO, stud_mealno);
            values_cat.put(KEY_STU_UNIQE_NO, stud_uniqueno);
            values_cat.put(KEY_STU_STATUS, stud_status);
            values_cat.put(KEY_STU_ISACTIVE, stud_isactive);
            values_cat.put(KEY_STU_CARD_NO, stud_cardno);
            values_cat.put(KEY_STU_CARD_ST, stud_cardst);
            values_cat.put(KEY_STU_IMAGE, img_path);


            if (!isStudentAlreadyExist(db, studInfo.getStudentID())) {
                db.insert(TABLE_STU_INFO, null, values_cat);
            } else {
                db.update(TABLE_STU_INFO, values_cat, KEY_STU_ID + "='" + studInfo.getStudentID() + "'", null);
            }
            db.close();
        }
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isStudentAlreadyExist(SQLiteDatabase db, String student_id) {
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_STU_INFO + " WHERE " + KEY_STU_ID + "='" + student_id + "'", null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public boolean checkStudentInfo() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_STU_INFO, null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            //db.close();
            return true;
        }
        cur.close();
        //db.close();
        return false;
    }

    public void deleteAllStudents() {
        SQLiteDatabase db = this.getWritableDatabase();
        if (checkStudentInfo()) {
            db.delete(TABLE_STU_INFO, null, null);
        }
        //db.close();
    }

    public ArrayList<StudentInfoModel.DataBean> getAllStudentInfo () {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<StudentInfoModel.DataBean> stu_list = new ArrayList<StudentInfoModel.DataBean>();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_STU_INFO , null);
        if (cur != null && cur.getCount() > 0) {

            if  (cur.moveToFirst()) {
                do {
                    StudentInfoModel.DataBean sim = new StudentInfoModel.DataBean();
                    sim.setStudentID(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ID)));
                    sim.setFirstName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FIRST_NAME)));
                    sim.setFatherName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FATHER_NAME)));
                    sim.setGrandFatherName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GRAND_FATHER_NAME)));
                    sim.setGender(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GENDER)));
                    sim.setDateOfBirth(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DOB)));
                    sim.setSignature(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_SIG)));
                    sim.setCollege(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_COLLEGE)));
                    sim.setDepartment(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DEPT)));
                    sim.setCampus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CAMPUS)));
                    sim.setProgram(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_PROGRAM)));
                    sim.setDegreeType(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DEGREE_TYPE)));
                    sim.setAdmissionType(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ADD_TYPE)));
                    sim.setAdmissionTypeShort(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ADD_TYPE_SHORT)));
                    sim.setValidDateUntil(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_VALID_DATE_UNTIL)));
                    sim.setIssueDate(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ISSUE_DATE)));
                    sim.setMealNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_MEAL_NO)));
                    sim.setUniqueNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_UNIQE_NO)));
                    sim.setStatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_STATUS)));
                    sim.setIsactive(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ISACTIVE)));
                    sim.setCardNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_NO)));
                    sim.setCardstatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_ST)));
                    sim.setStudentImage(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_IMAGE)));

                    stu_list.add(sim);

                } while(cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return stu_list;
    }


    public ArrayList<StudentInfoModel.DataBean> getStudentsBySearch (String searchText) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<StudentInfoModel.DataBean> stu_list = new ArrayList<StudentInfoModel.DataBean>();
        //Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_STU_INFO , null);
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_STU_INFO + " WHERE " + KEY_STU_FIRST_NAME + " LIKE '%" + searchText + "%' OR "
                + KEY_STU_FATHER_NAME + " LIKE '%" + searchText + "%' OR " + KEY_STU_GRAND_FATHER_NAME + " LIKE '%" + searchText + "%' OR "
                + KEY_STU_ID + " LIKE '%" +searchText + "%' OR " + KEY_STU_MEAL_NO + " LIKE '%" + searchText + "%' OR "
                + KEY_STU_CARD_NO + " LIKE '%" +searchText + "%'", null);
        if (cur != null && cur.getCount() > 0) {

            if  (cur.moveToFirst()) {
                do {
                    StudentInfoModel.DataBean sim = new StudentInfoModel.DataBean();
                    sim.setStudentID(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ID)));
                    sim.setFirstName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FIRST_NAME)));
                    sim.setFatherName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FATHER_NAME)));
                    sim.setGrandFatherName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GRAND_FATHER_NAME)));
                    sim.setGender(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GENDER)));
                    sim.setDateOfBirth(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DOB)));
                    sim.setSignature(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_SIG)));
                    sim.setCollege(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_COLLEGE)));
                    sim.setDepartment(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DEPT)));
                    sim.setCampus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CAMPUS)));
                    sim.setProgram(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_PROGRAM)));
                    sim.setDegreeType(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DEGREE_TYPE)));
                    sim.setAdmissionType(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ADD_TYPE)));
                    sim.setAdmissionTypeShort(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ADD_TYPE_SHORT)));
                    sim.setValidDateUntil(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_VALID_DATE_UNTIL)));
                    sim.setIssueDate(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ISSUE_DATE)));
                    sim.setMealNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_MEAL_NO)));
                    sim.setUniqueNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_UNIQE_NO)));
                    sim.setStatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_STATUS)));
                    sim.setIsactive(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ISACTIVE)));
                    sim.setCardNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_NO)));
                    sim.setCardstatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_ST)));
                    sim.setStudentImage(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_IMAGE)));

                    stu_list.add(sim);

                } while(cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return stu_list;
    }

    // get student detail form card no.
    public StudentInfoModel.DataBean getStudentByCardNo(String cardNo) {
        SQLiteDatabase db = this.getReadableDatabase();
        StudentInfoModel.DataBean sim = new StudentInfoModel.DataBean();
        //Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_STU_INFO , null);
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_STU_INFO + " WHERE "
                + KEY_STU_CARD_NO + " = '" +cardNo + "'", null);
        if (cur != null && cur.getCount() > 0) {

            if  (cur.moveToFirst()) {
                do {

                    sim.setStudentID(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ID)));
                    sim.setFirstName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FIRST_NAME)));
                    sim.setFatherName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FATHER_NAME)));
                    sim.setGrandFatherName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GRAND_FATHER_NAME)));
                    sim.setGender(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GENDER)));
                    sim.setDateOfBirth(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DOB)));
                    sim.setSignature(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_SIG)));
                    sim.setCollege(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_COLLEGE)));
                    sim.setDepartment(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DEPT)));
                    sim.setCampus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CAMPUS)));
                    sim.setProgram(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_PROGRAM)));
                    sim.setDegreeType(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DEGREE_TYPE)));
                    sim.setAdmissionType(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ADD_TYPE)));
                    sim.setAdmissionTypeShort(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ADD_TYPE_SHORT)));
                    sim.setValidDateUntil(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_VALID_DATE_UNTIL)));
                    sim.setIssueDate(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ISSUE_DATE)));
                    sim.setMealNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_MEAL_NO)));
                    sim.setUniqueNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_UNIQE_NO)));
                    sim.setStatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_STATUS)));
                    sim.setIsactive(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ISACTIVE)));
                    sim.setCardNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_NO)));
                    sim.setCardstatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_ST)));
                    sim.setStudentImage(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_IMAGE)));


                } while(cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return sim;
    }

    public int getCardStatusFromStudentId(String student_id) {
        int studentSt = -1;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT " + KEY_STU_CARD_ST + " FROM " + TABLE_STU_INFO + " WHERE " + KEY_STU_ID + "='" + student_id + "'", null);
        if (cur != null && cur.getCount() > 0) {

            if (cur.moveToFirst()) {
                do {
                    studentSt = Integer.parseInt(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_ST)));
                } while (cur.moveToNext());
            }
        }
        cur.close();
        return studentSt;
    }

    public int getCardStatusFromStudentCardNo(String card_no) {
        int studentSt = -1;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT " + KEY_STU_CARD_ST + " FROM " + TABLE_STU_INFO + " WHERE " + KEY_STU_CARD_NO + "='" + card_no + "'", null);
        if (cur != null && cur.getCount() > 0) {

            if (cur.moveToFirst()) {
                do {
                    studentSt = Integer.parseInt(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_ST)));
                } while (cur.moveToNext());
            }
        }
        cur.close();
        return studentSt;
    }

    public String getStudentImageFromStudentId(String student_id) {
        String studentImage = "";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT " + KEY_STU_IMAGE + " FROM " + TABLE_STU_INFO + " WHERE " + KEY_STU_ID + "='" + student_id + "'", null);
        if (cur != null && cur.getCount() > 0) {

            if (cur.moveToFirst()) {
                do {
                    studentImage = cur.getString(cur.getColumnIndexOrThrow(KEY_STU_IMAGE));
                } while (cur.moveToNext());
            }
        }
        cur.close();
        return studentImage;
    }

    //-------------------------------------- STAFF -------------------------------------------------

    public void saveStaffInfo(ArrayList<StaffInfoModel.DataBean> data, Context mContext) {
        SQLiteDatabase db = this.getWritableDatabase();
        //deleteAllStaff();
        // content values for categories
        ContentValues values_cat = new ContentValues();
        db.beginTransaction();
        for (int i = 0; i < data.size(); i++)
        {
            StaffInfoModel.DataBean staffInfo = data.get(i);
            String staff_id = staffInfo.getStaffID();
            Log.d(TAG, "StaffId: " + staff_id);

            String staff_app_no = staffInfo.getAppNo();
            String staff_sl_no = staffInfo.getSlNo();
            String staff_uid = staffInfo.getUID();
            String staff_fullname = staffInfo.getFullName();

            String staff_gender = staffInfo.getGender();
            String staff_dob = staffInfo.getDob();
            String staff_department = staffInfo.getDepartment();
            String staff_job_title = staffInfo.getJobTitle();
            String staff_emp_photo = staffInfo.getImage64byte();

            /*if(staffInfo.getEmpPhoto() != null && !staffInfo.getEmpPhoto().isEmpty())
            {
                String baseUrl = SharedPref.INSTANCE.getStringValue(mContext, KeysKt.keyBaseUrl, "");

                staff_emp_photo = baseUrl + staffInfo.getEmpPhoto();

                //Log.e(TAG, "Target---saveStaffInfo--StaffName: if--- " + staff_fullname +" pic-->>>"+staff_emp_photo);

                AppUtilsKt.ImageToLocal(mContext,staff_emp_photo,null,"StaffImage");

            }
            else
                staff_emp_photo = "";*/

            if (staff_emp_photo != null) {
                if (!staff_emp_photo.isEmpty()) {
                    byte[] byteImage = Base64.decode(staff_emp_photo, Base64.DEFAULT);
                    //Bitmap bmp = BitmapFactory.decodeByteArray(byteImage, 0, byteImage.length);
                    SaveImages si = new SaveImages();
                    img_path = si.saveToInternalStorage(mContext, si.decodeSampledBitmapFromByte(byteImage, 100, 100), staff_id.replaceAll("/", "_"));
                } else {
                    img_path = "";
                }
            } else {
                img_path = "";
            }

            String staff_signature = staffInfo.getSignature();
            String staff_address = staffInfo.getAddress();

            //Log.d(TAG, "StaffName: " + staff_fullname+"<>>"+staffInfo+" pic-->>>"+staffInfo.getEmpPhoto());

            String staff_idno = staffInfo.getIdNo();
            String staff_issueDate = staffInfo.getIssueDate();
            String staff_cardst = staffInfo.getCardstatus();
            String staff_cardno = staffInfo.getCardNumber();
            String staff_emailid = staffInfo.getEmailId();
            String staff_isactive = staffInfo.getIsactive();
            String staff_password = staffInfo.getPassword();
            String staff_id_number = staffInfo.getIdNumber();


            values_cat.put(KEY_STAFF_ID, staff_id);
            values_cat.put(KEY_STAFF_APP_NO, staff_app_no);
            values_cat.put(KEY_STAFF_SL_NO, staff_sl_no);
            values_cat.put(KEY_STAFF_UID, staff_uid);
            values_cat.put(KEY_STAFF_FULL_NAME, staff_fullname);
            values_cat.put(KEY_STAFF_GENDER, staff_gender);
            values_cat.put(KEY_STAFF_DOB, staff_dob);
            values_cat.put(KEY_STAFF_DEPT, staff_department);
            values_cat.put(KEY_STAFF_JOB_TITLE, staff_job_title);
            values_cat.put(KEY_STAFF_EMP_PHOTO, img_path);
            values_cat.put(KEY_STAFF_SIG, staff_signature);
            values_cat.put(KEY_STAFF_ADD, staff_address);
            values_cat.put(KEY_STAFF_ID_NO, staff_idno);
            values_cat.put(KEY_STAFF_ISSUE_DATE, staff_issueDate);
            values_cat.put(KEY_STAFF_CARD_ST, staff_cardst);
            values_cat.put(KEY_STAFF_CARD_NO, staff_cardno);
            values_cat.put(KEY_STAFF_EMAIL_ID, staff_emailid);
            values_cat.put(KEY_STAFF_ISACTIVE, staff_isactive);
            values_cat.put(KEY_STAFF_PASSWORD, staff_password);
            //values_cat.put(KEY_STAFF_ID_NO, staff_id_number);


            if (!isStaffAlreadyExist(db, staffInfo.getStaffID())) {
                db.insert(TABLE_STAFF_INFO, null, values_cat);
            } else {
                db.update(TABLE_STAFF_INFO, values_cat, KEY_STAFF_ID + "='" + staff_id + "'", null);
            }

        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }


    public boolean isStaffAlreadyExist(SQLiteDatabase db, String staff_id) {
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_STAFF_INFO + " WHERE " + KEY_STAFF_ID + "='" + staff_id + "'", null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public boolean checkStaffInfo() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_STAFF_INFO, null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            //db.close();
            return true;
        }
        cur.close();
        //db.close();
        return false;
    }

    public void deleteAllStaff() {
        SQLiteDatabase db = this.getWritableDatabase();
        if (checkStaffInfo()) {
            db.delete(TABLE_STAFF_INFO, null, null);
        }
        //db.close();
    }

    public ArrayList<StaffInfoModel.DataBean> getAllStaffInfo () {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<StaffInfoModel.DataBean> stu_list = new ArrayList<StaffInfoModel.DataBean>();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_STAFF_INFO , null);
        if (cur != null && cur.getCount() > 0) {

            if  (cur.moveToFirst()) {
                do {
                    StaffInfoModel.DataBean sim = new StaffInfoModel.DataBean();
                    sim.setStaffID(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ID)));
                    sim.setAppNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_APP_NO)));
                    sim.setSlNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_SL_NO)));
                    sim.setUID(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_UID)));
                    sim.setFullName(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_FULL_NAME)));
                    sim.setGender(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_GENDER)));
                    sim.setDob(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_DOB)));
                    sim.setDepartment(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_DEPT)));
                    sim.setJobTitle(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_JOB_TITLE)));
                    sim.setEmpPhoto(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_EMP_PHOTO)));
                    sim.setSignature(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_SIG)));
                    sim.setAddress(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ADD)));
                    sim.setIdNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ID_NO)));
                    sim.setIssueDate(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ISSUE_DATE)));
                    sim.setCardstatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_CARD_ST)));
                    sim.setCardNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_CARD_NO)));
                    sim.setIsactive(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ISACTIVE)));
                    sim.setEmailId(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_EMAIL_ID)));
                    sim.setPassword(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_PASSWORD)));
                    sim.setIdNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ID_NUMBER)));


                    stu_list.add(sim);

                } while(cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return stu_list;
    }

    public ArrayList<StaffInfoModel.DataBean> getStaffsBySearch (String searchText) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<StaffInfoModel.DataBean> stu_list = new ArrayList<StaffInfoModel.DataBean>();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_STAFF_INFO + " WHERE " + KEY_STAFF_FULL_NAME + " LIKE '%" + searchText + "%' OR "
                + KEY_STAFF_APP_NO + " LIKE '%" + searchText + "%' OR " + KEY_STAFF_SL_NO + " LIKE '%" + searchText + "%' OR "
                + KEY_STAFF_UID + " LIKE '%" +searchText + "%' OR " + KEY_STAFF_ID + " LIKE '%" + searchText + "%' OR "
                + KEY_STAFF_CARD_NO + " LIKE '%" +searchText + "%' OR " + KEY_STAFF_EMAIL_ID + " LIKE '%" +searchText + "%'", null);
        if (cur != null && cur.getCount() > 0) {

            if  (cur.moveToFirst()) {
                do {
                    StaffInfoModel.DataBean sim = new StaffInfoModel.DataBean();
                    sim.setStaffID(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ID)));
                    sim.setAppNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_APP_NO)));
                    sim.setSlNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_SL_NO)));
                    sim.setUID(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_UID)));
                    sim.setFullName(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_FULL_NAME)));
                    sim.setGender(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_GENDER)));
                    sim.setDob(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_DOB)));
                    sim.setDepartment(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_DEPT)));
                    sim.setJobTitle(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_JOB_TITLE)));
                    sim.setEmpPhoto(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_EMP_PHOTO)));
                    sim.setSignature(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_SIG)));
                    sim.setAddress(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ADD)));
                    sim.setIdNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ID_NO)));
                    sim.setIssueDate(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ISSUE_DATE)));
                    sim.setCardstatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_CARD_ST)));
                    sim.setCardNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_CARD_NO)));
                    sim.setIsactive(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ISACTIVE)));
                    sim.setEmailId(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_EMAIL_ID)));
                    sim.setPassword(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_PASSWORD)));
                    sim.setIdNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ID_NUMBER)));


                    stu_list.add(sim);

                } while(cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return stu_list;
    }

    // get student detail form card no.
    public StaffInfoModel.DataBean getStaffByCardNumber(String cardNo) {
        SQLiteDatabase db = this.getReadableDatabase();
        StaffInfoModel.DataBean sim = new StaffInfoModel.DataBean();
        //Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_STU_INFO , null);
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_STAFF_INFO + " WHERE "
                + KEY_STAFF_CARD_NO + " = '" +cardNo + "'", null);
        if (cur != null && cur.getCount() > 0) {

            if  (cur.moveToFirst()) {
                do {
                    //StaffInfoModel.DataBean sim = new StaffInfoModel.DataBean();
                    sim.setStaffID(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ID)));
                    sim.setAppNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_APP_NO)));
                    sim.setSlNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_SL_NO)));
                    sim.setUID(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_UID)));
                    sim.setFullName(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_FULL_NAME)));
                    sim.setGender(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_GENDER)));
                    sim.setDob(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_DOB)));
                    sim.setDepartment(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_DEPT)));
                    sim.setJobTitle(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_JOB_TITLE)));
                    sim.setEmpPhoto(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_EMP_PHOTO)));
                    sim.setSignature(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_SIG)));
                    sim.setAddress(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ADD)));
                    sim.setIdNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ID_NO)));
                    sim.setIssueDate(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ISSUE_DATE)));
                    sim.setCardstatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_CARD_ST)));
                    sim.setCardNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_CARD_NO)));
                    sim.setIsactive(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ISACTIVE)));
                    sim.setEmailId(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_EMAIL_ID)));
                    sim.setPassword(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_PASSWORD)));
                    sim.setIdNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ID_NUMBER)));




                } while(cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return sim;
    }

    public int getCardStatusFromStaffId(String staff_id) {
        int staffSt = -1;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT " + KEY_STAFF_CARD_ST + " FROM " + TABLE_STAFF_INFO + " WHERE " + KEY_STAFF_ID + "='" + staff_id + "'", null);
        if (cur != null && cur.getCount() > 0) {

            if (cur.moveToFirst()) {
                do {
                    staffSt = Integer.parseInt(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_CARD_ST)));
                } while (cur.moveToNext());
            }
        }
        cur.close();
        return staffSt;
    }


    public int getCardStatusFromStaffCardNo(String staff_card_no) {
        int staffSt = -1;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT " + KEY_STAFF_CARD_ST + " FROM " + TABLE_STAFF_INFO + " WHERE " + KEY_STAFF_CARD_NO + "='" + staff_card_no + "'", null);
        if (cur != null && cur.getCount() > 0) {

            if (cur.moveToFirst()) {
                do {
                    staffSt = Integer.parseInt(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_CARD_ST)));
                } while (cur.moveToNext());
            }
        }
        cur.close();
        return staffSt;
    }

    public String getStaffImageFromStaffId (String staff_id) {
        String staffImg = "";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT " + KEY_STAFF_EMP_PHOTO + " FROM " + TABLE_STAFF_INFO + " WHERE " + KEY_STAFF_ID + "='" + staff_id + "'", null);
        if (cur != null && cur.getCount() > 0) {

            if (cur.moveToFirst()) {
                do {
                    staffImg = cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_EMP_PHOTO));
                } while (cur.moveToNext());
            }
        }
        cur.close();
        return staffImg;

    }

    public void addEmployees(Context mContext, Employee employees) {
        SQLiteDatabase db = this.getWritableDatabase();
        String imgPath = "";
        if (employees.getEmployee_Photo() != null) {
            SaveImages si = SaveImages.getInstance();
            byte[] data = null;
            try {
                data = Base64.decode(employees.getEmployee_Photo().getBytes(), Base64.DEFAULT);//getBytesFromInputStream(ins), Base64.DEFAULT);//Base64.decode(imgStr.toString().getBytes(), Base64.DEFAULT);
            } catch (Exception e) {

            }
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inMutable = true;
            Bitmap bm = BitmapFactory.decodeByteArray(data, 0, data.length, options);

            //imgPath = si.saveToInternalStorage(mContext, bm, employees.getSLN_Employee());
        } else {

        }
        // content values for categories
        ContentValues values_employee = new ContentValues();
        values_employee.put(KEY_EMP_ID, employees.getSLN_Employee());
        values_employee.put(KEY_EMP_NAME, employees.getEmployeeName());

        String card_number = "";
        if (employees.getCard_Number() != null) {
            if (!employees.getCard_Number().equals("null") && employees.getCard_Number().length() > 0)
                card_number = employees.getCard_Number();
        }

        values_employee.put(KEY_EMP_CARD_NO, card_number);
        values_employee.put(KEY_EMP_LOC_ID, employees.getSLN_Location());

        if (!isEmployeeAlreadyExists(db, employees.getSLN_Employee())) {
            values_employee.put(KEY_EMP_PHOTO, imgPath);
            db.insert(TABLE_EMPLOYEES, null, values_employee);
        } else {
            ContextWrapper cw = new ContextWrapper(mContext);
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir(SaveImages.imgDir, Context.MODE_PRIVATE);
            // Create imageDir
            File mypath = new File(directory, "" + employees.getSLN_Employee() + ".jpg");
            imgPath = mypath.getAbsolutePath();
            values_employee.put(KEY_EMP_PHOTO, imgPath);
            db.update(TABLE_EMPLOYEES, values_employee, KEY_EMP_ID + "=" + employees.getSLN_Employee(), null);
        }
        db.close();
    }

    // batch insert for employees
    public void saveEmployees (Activity mActivity, int req_mode, ArrayList<Employee> data) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ContentValues values_employee = new ContentValues();
        for (int i = 0; i < data.size(); i++) {
            Employee emp = data.get(i);//new Categories();
            int emp_id = emp.getSLN_Employee();//dataObj.getInt(APIParam.RES_CAT_ID);
            //Log.d(TAG, "EmpId: " + emp_id);
            String emp_name = emp.getEmployeeName();//dataObj.getString(APIParam.RES_CAT_NAME);
            //Log.d(TAG, "EmpName: " + emp_name);
            String card_number = emp.getCard_Number();
            //Log.d(TAG, "EmpCardNo: " + card_number);
            String emp_photo = emp.getEmployee_Photo();
            //Log.d(TAG, "EMpPhoto: "+emp_photo);
            int loc_id = emp.getSLN_Location();
            //Log.d(TAG, "EmpLocationId: "+loc_id);
            //categoriesListArray[i] = cat_name;

            if (req_mode == 0 || req_mode == 1) {

                String imgPath = "";
                if (emp_photo != null) {
                    SaveImages si = SaveImages.getInstance();
                    byte[] img_data = null;
                    try {
                        img_data = Base64.decode(emp_photo.getBytes(), Base64.DEFAULT);//getBytesFromInputStream(ins), Base64.DEFAULT);//Base64.decode(imgStr.toString().getBytes(), Base64.DEFAULT);
                    } catch (Exception e) {

                    }
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inMutable = true;
                    Bitmap bm = BitmapFactory.decodeByteArray(img_data, 0, img_data.length, options);

                    //imgPath = si.saveToInternalStorage(mActivity, bm, emp_id);
                }

                //addEmployees(mActivity, emp);
                values_employee.put(KEY_EMP_ID, emp_id);
                values_employee.put(KEY_EMP_NAME, emp_name);

                if (card_number != null) {
                    if (card_number.equals("null") && card_number.length() > 0) {
                        card_number = "";
                        continue;
                    }

                }else {
                    card_number = "";
                    continue;
                }

                values_employee.put(KEY_EMP_CARD_NO, card_number);
                values_employee.put(KEY_EMP_LOC_ID, loc_id);

                if (!isEmployeeAlreadyExists(db, emp_id)) {
                    values_employee.put(KEY_EMP_PHOTO, imgPath);
                    db.insert(TABLE_EMPLOYEES, null, values_employee);
                } else {
                    ContextWrapper cw = new ContextWrapper(mActivity);
                    // path to /data/data/yourapp/app_data/imageDir
                    File directory = cw.getDir(SaveImages.imgDir, Context.MODE_PRIVATE);
                    // Create imageDir
                    File mypath = new File(directory, "" + emp_id + ".jpg");
                    imgPath = mypath.getAbsolutePath();
                    values_employee.put(KEY_EMP_PHOTO, imgPath);
                    db.update(TABLE_EMPLOYEES, values_employee, KEY_EMP_ID + "=" + emp_id, null);
                }
            } else if (req_mode == 2) {
                if (emp_photo != null) {
                    deleteAvtarFromFileName(mActivity, emp_id);
                }
                updateEmployees(mActivity, emp);
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    // update the employee in case of update selected list of employees
    public void updateEmployees(Context mContext, Employee employees) {
        SQLiteDatabase db = this.getWritableDatabase();
        String imgPath = "";
        if (employees.getEmployee_Photo() != null) {
            SaveImages si = SaveImages.getInstance();
            byte[] data = null;
            try {
                data = Base64.decode(employees.getEmployee_Photo().getBytes(), Base64.DEFAULT);//getBytesFromInputStream(ins), Base64.DEFAULT);//Base64.decode(imgStr.toString().getBytes(), Base64.DEFAULT);
            } catch (Exception e) {

            }
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inMutable = true;
            Bitmap bm = BitmapFactory.decodeByteArray(data, 0, data.length, options);

            //imgPath = si.saveToInternalStorage(mContext, bm, employees.getSLN_Employee());
        } else {
            ContextWrapper cw = new ContextWrapper(mContext);
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir(SaveImages.imgDir, Context.MODE_PRIVATE);
            // Create imageDir
            File mypath = new File(directory, "" + employees.getSLN_Employee() + ".jpg");
            imgPath = mypath.getAbsolutePath();
        }
        // content values for categories
        ContentValues values_employee = new ContentValues();
        values_employee.put(KEY_EMP_ID, employees.getSLN_Employee());
        values_employee.put(KEY_EMP_NAME, employees.getEmployeeName());
        values_employee.put(KEY_EMP_PHOTO, imgPath);

        String card_number = "";
        if (employees.getCard_Number() != null) {
            if (!employees.getCard_Number().equals("null") && employees.getCard_Number().length() > 0)
                card_number = employees.getCard_Number();
        }

        values_employee.put(KEY_EMP_CARD_NO, card_number);
        values_employee.put(KEY_EMP_LOC_ID, employees.getSLN_Location());

        if (!isEmployeeAlreadyExists(db, employees.getSLN_Employee())) {
            //db.insert(TABLE_EMPLOYEES, null, values_employee);
        } else {
            db.update(TABLE_EMPLOYEES, values_employee, KEY_EMP_ID + "=" + employees.getSLN_Employee(), null);
        }
        db.close();
    }

    public boolean isEmployeeAlreadyExists(SQLiteDatabase db, int employee_id) {
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_EMPLOYEES + " WHERE " + KEY_EMP_ID + "=" + employee_id, null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public Employee getEmployeeFromEmpId(int employee_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Employee employee = new Employee();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_EMPLOYEES + " WHERE " + KEY_EMP_ID + "=" + employee_id, null);
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    employee.setSLN_Employee(cur.getInt(cur.getColumnIndexOrThrow(KEY_EMP_ID)));
                    employee.setEmployeeName(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_NAME)));
                    employee.setEmployee_Photo(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_PHOTO)));
                    employee.setCard_Number(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_CARD_NO)));
                    employee.setSLN_Location(cur.getInt(cur.getColumnIndexOrThrow(KEY_EMP_LOC_ID)));
                } while (cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return employee;
    }

    public ArrayList<Employee> getAllEmployees() {
        ArrayList<Employee> employee_list = new ArrayList<Employee>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_EMPLOYEES, null);
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    Employee employee = new Employee();
                    employee.setSLN_Employee(cur.getInt(cur.getColumnIndexOrThrow(KEY_EMP_ID)));
                    employee.setEmployeeName(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_NAME)));
                    employee.setEmployee_Photo(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_PHOTO)));
                    employee.setCard_Number(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_CARD_NO)));
                    employee.setSLN_Location(cur.getInt(cur.getColumnIndexOrThrow(KEY_EMP_LOC_ID)));
                    employee_list.add(employee);
                } while (cur.moveToNext());
            }
            //return cat_list;
        }
        cur.close();
        db.close();
        return employee_list;
    }

    public ArrayList<Employee> searchEmployeeFromFilter (String searchText) {
        ArrayList<Employee> employee_list = new ArrayList<Employee>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_EMPLOYEES + " WHERE " + KEY_EMP_NAME + " LIKE '%" + searchText + "%' OR " + KEY_EMP_ID + " LIKE '%" + searchText + "%' OR " + KEY_EMP_CARD_NO + " LIKE '%" + searchText + "%'", null);
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    Employee employee = new Employee();
                    employee.setSLN_Employee(cur.getInt(cur.getColumnIndexOrThrow(KEY_EMP_ID)));
                    employee.setEmployeeName(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_NAME)));
                    employee.setEmployee_Photo(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_PHOTO)));
                    employee.setCard_Number(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_CARD_NO)));
                    employee.setSLN_Location(cur.getInt(cur.getColumnIndexOrThrow(KEY_EMP_LOC_ID)));
                    employee_list.add(employee);
                } while (cur.moveToNext());
            }
            //return cat_list;
        }
        cur.close();
        db.close();
        return employee_list;
    }

    // to get employees whise having card enroll with them
    public ArrayList<Employee> getAllEmployeesOnlyHavingCard() {
        ArrayList<Employee> employee_list = new ArrayList<Employee>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_EMPLOYEES, null);
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    Employee employee = new Employee();
                    String card_number = cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_CARD_NO));
                    // condition to check card details whether employees enroll with card or not
                    if (card_number != null) { // only add to the list if it has employees
                        if (card_number != "" && card_number.length() > 0) {
                            employee.setSLN_Employee(cur.getInt(cur.getColumnIndexOrThrow(KEY_EMP_ID)));
                            employee.setEmployeeName(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_NAME)));
                            employee.setEmployee_Photo(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_PHOTO)));
                            employee.setCard_Number(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_CARD_NO)));
                            employee.setSLN_Location(cur.getInt(cur.getColumnIndexOrThrow(KEY_EMP_LOC_ID)));
                            employee_list.add(employee);
                        }
                    }
                } while (cur.moveToNext());
            }
            //return cat_list;
        }
        cur.close();
        db.close();
        return employee_list;
    }

    // to fetch employees with page number limited 100 records
    public ArrayList<Employee> getAllEmployees(int first, int last) {
        ArrayList<Employee> employee_list = new ArrayList<Employee>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_EMPLOYEES + " WHERE " + KEY_EMP_AUTO_ID + ">=" + first + " AND " + KEY_EMP_AUTO_ID + "<=" + last, null);
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    Employee employee = new Employee();
                    employee.setSLN_Employee(cur.getInt(cur.getColumnIndexOrThrow(KEY_EMP_ID)));
                    employee.setEmployeeName(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_NAME)));
                    employee.setEmployee_Photo(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_PHOTO)));
                    employee.setCard_Number(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_CARD_NO)));
                    employee.setSLN_Location(cur.getInt(cur.getColumnIndexOrThrow(KEY_EMP_LOC_ID)));
                    employee_list.add(employee);
                } while (cur.moveToNext());
            }
            //return cat_list;
        }
        cur.close();
        db.close();
        return employee_list;
    }


    public void deleteAllEmployees(Context mContext) {
        SQLiteDatabase db = this.getWritableDatabase();
        if (checkEmployees()) {
            db.delete(TABLE_EMPLOYEES, null, null);
            deleteAvtarsDirectory(mContext);
        }
    }

    public boolean checkEmployees() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_EMPLOYEES, null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public void deleteAvtarsDirectory(Context mContext) {
        ContextWrapper cw = new ContextWrapper(mContext);
        File directory = cw.getDir(SaveImages.imgDir, Context.MODE_PRIVATE);
        if (directory.isDirectory())
            directory.delete();
    }

    public void deleteAvtarFromFileName(Context mContext, int empId) {
        ContextWrapper cw = new ContextWrapper(mContext);
        File directory = cw.getDir(SaveImages.imgDir, Context.MODE_PRIVATE);
        if (directory.isDirectory()) {
            //String imgPath = directory.getAbsolutePath() + empId + ".jpg";
            File mypath = new File(directory, "" + empId + ".jpg");
            mypath.delete();
        }
    }

    // fetch image path from emnployee id
    public String getImagePathFromEmpId(Context mContext, int emp_id) {
        String imgPath = "";
        ContextWrapper cw = new ContextWrapper(mContext);
        File directory = cw.getDir(SaveImages.imgDir, Context.MODE_PRIVATE);
        if (directory.isDirectory()) {
            //String imgPath = directory.getAbsolutePath() + empId + ".jpg";
            File mypath = new File(directory, "" + emp_id + ".jpg");
            if (!mypath.isDirectory()) {
                imgPath = mypath.getAbsolutePath();
            }
        }
        return imgPath;
    }

    // employees locations

    public void addEmployeesLocation(PunchLogs pl) {
        SQLiteDatabase db = this.getWritableDatabase();
        String punch_date = pl.getPunch_Date();
        String punchDate = "", punchTime = "";
        if (punch_date.contains("T")) {
            String[] splitdatetime = punch_date.split("T");
            punchDate = splitdatetime[0];
            punchTime = splitdatetime[1];
            if (punchTime.contains(".")) {
                String[] split_punch_time = punchTime.split("\\.");
                punchTime = split_punch_time[0];
            }
            punch_date = punchDate + " " + punchTime;
        }

        // content values for categories
        ContentValues values_employee = new ContentValues();
        values_employee.put(KEY_PUNCHLOGS_SLN_PUNCH, pl.getSLN_Punch());
        values_employee.put(KEY_PUNCHLOGS_EMP_ID, pl.getSLN_Employee());
        values_employee.put(KEY_PUNCHLOGS_CARD_NO, pl.getCard_Number());
        values_employee.put(KEY_PUNCHLOGS_SLN_READER, pl.getSLN_Reader());
        values_employee.put(KEY_PUNCHLOGS_READER_NAME, pl.getReader_Name());
        values_employee.put(KEY_PUNCHLOGS_DATETIME, punch_date);
        values_employee.put(KEY_PUNCHLOGS_GRANT_DENY, pl.getIsPunchSynced());//pl.isGrant_Deny() ? 1 : 0);
        values_employee.put(KEY_PUNCHLOGS_LOC_ID, pl.getSLN_Location());
        values_employee.put(KEY_PUNCHLOGS_AREA_ID, pl.getSLN_Area_ID());
        values_employee.put(KEY_PUNCHLOGS_CAT_ID, pl.getMusteringId());

        //if (!isEmployeeAlreadyExists(db, pl.getSLN_Employee())) {
        db.insert(TABLE_PUNCHLOGS, null, values_employee);
        /*} else {
            db.update(TABLE_PUNCHLOGS, values_employee, KEY_PUNCHLOGS_EMP_ID+"="+pl.getSLN_Employee() +" AND "+KEY_PUNCHLOGS_CARD_NO + "=" + pl.getCard_Number(), null);
        }*/
        db.close();
    }
    // save batch employee location
    public void saveEmployeesLocation(ArrayList<PunchLogs> data) {
        // content values for categories
        ContentValues values_employee = new ContentValues();
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for (PunchLogs pl : data) {
            String punch_date = pl.getPunch_Date();
            String punchDate = "", punchTime = "";
            if (punch_date.contains("T")) {
                String[] splitdatetime = punch_date.split("T");
                punchDate = splitdatetime[0];
                punchTime = splitdatetime[1];
                if (punchTime.contains(".")) {
                    String[] split_punch_time = punchTime.split("\\.");
                    punchTime = split_punch_time[0];
                }
                punch_date = punchDate + " " + punchTime;
            }


            values_employee.put(KEY_PUNCHLOGS_SLN_PUNCH, pl.getSLN_Punch());
            values_employee.put(KEY_PUNCHLOGS_EMP_ID, pl.getSLN_Employee());
            values_employee.put(KEY_PUNCHLOGS_CARD_NO, pl.getCard_Number());
            values_employee.put(KEY_PUNCHLOGS_SLN_READER, pl.getSLN_Reader());
            values_employee.put(KEY_PUNCHLOGS_READER_NAME, pl.getReader_Name());
            values_employee.put(KEY_PUNCHLOGS_DATETIME, punch_date);
            values_employee.put(KEY_PUNCHLOGS_GRANT_DENY, pl.isGrant_Deny() ? 1 : 0);
            values_employee.put(KEY_PUNCHLOGS_LOC_ID, pl.getSLN_Location());
            values_employee.put(KEY_PUNCHLOGS_AREA_ID, pl.getSLN_Area_ID());
            values_employee.put(KEY_PUNCHLOGS_CAT_ID, pl.getMusteringId());

            //if (!isEmployeeAlreadyExists(db, pl.getSLN_Employee())) {
            db.insert(TABLE_PUNCHLOGS, null, values_employee);
            /*} else {
                db.update(TABLE_PUNCHLOGS, values_employee, KEY_PUNCHLOGS_EMP_ID+"="+pl.getSLN_Employee() +" AND "+KEY_PUNCHLOGS_CARD_NO + "=" + pl.getCard_Number(), null);
            }*/
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public void deleteEmployeesLocation() {
        SQLiteDatabase db = this.getWritableDatabase();
        if (checkEmployeesLocation()) {
            db.delete(TABLE_PUNCHLOGS, null, null);
        }
    }

    public boolean checkEmployeesLocation() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_PUNCHLOGS, null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public int getEmployeesLocationCount() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_PUNCHLOGS, null);
        if (cur != null && cur.getCount() > 0) {
            return cur.getCount();
        }
        cur.close();
        return 0;
    }

    public ArrayList<PunchLogs> getAllEmployeesLocation(Context mContext, int first, int last) {
        ArrayList<PunchLogs> punchlogs_list = new ArrayList<PunchLogs>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT *, " + TABLE_EMPLOYEES + "." + KEY_EMP_NAME + " AS emp_name, " + TABLE_EMPLOYEES + "." + KEY_EMP_PHOTO + " AS emp_photo" + " FROM " + TABLE_PUNCHLOGS + " LEFT JOIN " + TABLE_EMPLOYEES + " ON " + TABLE_PUNCHLOGS + "." + KEY_PUNCHLOGS_EMP_ID + "=" + TABLE_EMPLOYEES + "." + KEY_EMP_ID + " WHERE " + TABLE_PUNCHLOGS + "." + KEY_PUNCHLOGS_AUTO_ID + ">=" + first + " AND " + TABLE_PUNCHLOGS + "." + KEY_PUNCHLOGS_AUTO_ID + "<=" + last + " ORDER BY " + TABLE_PUNCHLOGS + "." + KEY_PUNCHLOGS_DATETIME + " DESC";
        Cursor cur = db.rawQuery(query, null);//"SELECT * FROM " + TABLE_PUNCHLOGS + " WHERE "+ KEY_PUNCHLOGS_AUTO_ID + ">=" + first + " AND " + KEY_PUNCHLOGS_AUTO_ID + "<=" + last  + " ORDER BY " + KEY_PUNCHLOGS_DATETIME  +" DESC" , null);
        //Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_PUNCHLOGS + " ORDER BY " + KEY_PUNCHLOGS_DATETIME  +" DESC," + KEY_PUNCHLOGS_AUTO_ID + " DESC" , null);
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    int sln_punch = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_SLN_PUNCH));
                    int punchlogs_emp_id = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_EMP_ID));
                    String punchlogs_card_number = cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_CARD_NO));
                    int sln_reader = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_SLN_READER));
                    String reader_name = cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_READER_NAME));
                    String punch_date = cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_DATETIME));
                    int grant_deny = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_GRANT_DENY));// > 0;
                    int sln_location = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_LOC_ID));
                    int sln_area_id = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_AREA_ID));
                    int category_id = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_CAT_ID));
                    //String sln_emp_photo = getImagePathFromEmpId(mContext, punchlogs_emp_id);
                    String sln_emp_name = cur.getString(cur.getColumnIndexOrThrow("emp_name"));
                    String sln_emp_photo = cur.getString(cur.getColumnIndexOrThrow("emp_photo"));

                    PunchLogs pl = new PunchLogs();
                    pl.setSLN_Punch(sln_punch);
                    //pl.setSLN_Employee(punchlogs_emp_id);
                    //pl.setSLN_EmployeeName(cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_EMP_NAME)));
                    //pl.setSLN_EmployeePhoto(cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_EMP_PHOTO)));
                    pl.setCard_Number(punchlogs_card_number);
                    pl.setSLN_Reader(sln_reader);
                    pl.setReader_Name(reader_name);
                    pl.setPunch_Date(punch_date);
                    pl.setIsPunchSynced(grant_deny);
                    pl.setSLN_Location(sln_location);
                    pl.setSLN_Area_ID(sln_area_id);
                    pl.setMusteringId(category_id);
                    pl.setSLN_EmployeeName(sln_emp_name);
                    pl.setSLN_EmployeePhoto(sln_emp_photo);
                    //PunchLogs pl_update = getDetailsFromEmployees(pl);
                    Log.d("Database Handler", "GetAllEmployeeLocation List Emp Id" + pl.getSLN_Employee() + " Emp Name: " + pl.getSLN_EmployeeName() + " Emp Photo: " + pl.getSLN_EmployeePhoto() + " Emp CardNo: " + pl.getCard_Number() + " Emp Loc: " + pl.getSLN_Location() + " Punchdate: " + pl.getPunch_Date());
                    punchlogs_list.add(pl);//pl_update);
                } while (cur.moveToNext());
            }
            //return cat_list;
        }
        cur.close();
        db.close();
        return punchlogs_list;
    }

    public PunchLogs getDetailsFromEmployees(PunchLogs pl) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_EMPLOYEES + " WHERE " + KEY_EMP_CARD_NO + "=" + pl.getCard_Number() + " AND " + KEY_EMP_ID + "=" + pl.getSLN_Employee(), null);
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    pl.setSLN_EmployeeName(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_NAME)));
                    pl.setSLN_EmployeePhoto(cur.getString(cur.getColumnIndexOrThrow(KEY_EMP_PHOTO)));
                } while (cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return pl;
    }

    // to get employeeId from Card number
    public int getEmployeeIdFromCardNumber(String cardNumber) {
        int empId = -1;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT " + KEY_EMP_ID + " FROM " + TABLE_EMPLOYEES + " WHERE " + KEY_EMP_CARD_NO + "='" + cardNumber + "' COLLATE NOCASE", null);
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    empId = cur.getInt(cur.getColumnIndexOrThrow(KEY_EMP_ID));
                } while (cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return empId;
    }


    // to get StudentId from Card number
    public String getStudentIdFromCardNumber(String cardNumber) {
        String stuId = "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT " + KEY_STU_ID + " FROM " + TABLE_STU_INFO + " WHERE " + KEY_STU_CARD_NO + "='" + cardNumber + "' COLLATE NOCASE", null);
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    stuId = cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ID));
                } while (cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return stuId;
    }

    // to get StudentId from Card number
    public String getStudentNameFromCardNumber(String cardNumber) {
        String stuName = "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT " + KEY_STU_FIRST_NAME + ", " + KEY_STU_FATHER_NAME +", "+ KEY_STU_GRAND_FATHER_NAME + " FROM " + TABLE_STU_INFO + " WHERE " + KEY_STU_CARD_NO + "='" + cardNumber + "' COLLATE NOCASE", null);
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    String firstName = cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FIRST_NAME));
                    String fatherName = cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FATHER_NAME));
                    String grandFatherName = cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GRAND_FATHER_NAME));
                    stuName = firstName + " " + fatherName + " " + grandFatherName;
                } while (cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return stuName;
    }

    // to get StudentId from Card number
    public StudentInfoModel.DataBean getStudentFromCardNumber(String cardNumber) {
        StudentInfoModel.DataBean student = new StudentInfoModel.DataBean();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * " + " FROM " + TABLE_STU_INFO + " WHERE " + KEY_STU_CARD_NO + "='" + cardNumber + "' COLLATE NOCASE", null);
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {

                    student.setStudentID(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ID)));
                    student.setFirstName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FIRST_NAME)));
                    student.setFatherName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FATHER_NAME)));
                    student.setGrandFatherName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GRAND_FATHER_NAME)));
                    student.setMealNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_MEAL_NO)));
                    student.setCardNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_NO)));
                    student.setCardstatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_ST)));
                    student.setStudentImage(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_IMAGE)));

                } while (cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return student;
    }



    // To update the PunchLogs data
    public void updatePunchLogsData(PunchLogs pl) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values_employee = new ContentValues();
        values_employee.put(KEY_PUNCHLOGS_EMP_ID, pl.getSLN_Employee());
        values_employee.put(KEY_PUNCHLOGS_CARD_NO, pl.getCard_Number());
        values_employee.put(KEY_PUNCHLOGS_SLN_READER, pl.getSLN_Reader());
        values_employee.put(KEY_PUNCHLOGS_READER_NAME, pl.getReader_Name());
        values_employee.put(KEY_PUNCHLOGS_DATETIME, pl.getPunch_Date());
        values_employee.put(KEY_PUNCHLOGS_GRANT_DENY, pl.getIsPunchSynced());
        values_employee.put(KEY_PUNCHLOGS_LOC_ID, pl.getSLN_Location());
        values_employee.put(KEY_PUNCHLOGS_AREA_ID, pl.getSLN_Area_ID());
        values_employee.put(KEY_PUNCHLOGS_CAT_ID, pl.getMusteringId());

        if (!isLocationAlreadyExist(pl.getSLN_Reader(), pl.getCard_Number())) {
            db.insert(TABLE_PUNCHLOGS, null, values_employee);
        } else {
            //db.update(TABLE_PUNCHLOGS, values_employee, KEY_PUNCHLOGS_SLN_READER+"=" + pl.getSLN_Reader() +" AND " + KEY_PUNCHLOGS_CARD_NO + "=" + pl.getCard_Number(), null);
            db.update(TABLE_PUNCHLOGS, values_employee, KEY_PUNCHLOGS_CARD_NO + "='" + pl.getCard_Number() + "' COLLATE NOCASE", null);
        }
        db.close();
    }

    public boolean isLocationAlreadyExist(int readerId, String cardNumber) {
        SQLiteDatabase db = this.getWritableDatabase();
        //Cursor cur = db.rawQuery("SELECT * FROM "+ TABLE_PUNCHLOGS +" WHERE " + KEY_PUNCHLOGS_SLN_READER + "=" + readerId + " AND " + KEY_PUNCHLOGS_CARD_NO + "=" + cardNumber, null);
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_PUNCHLOGS + " WHERE " + KEY_PUNCHLOGS_CARD_NO + "='" + cardNumber.toString() + "' COLLATE NOCASE", null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public ArrayList<PunchLogs> getEmployeesLocationBasedOnFilter(ArrayList<Integer> categoryID, ArrayList<Integer> areaID) {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<PunchLogs> list = new ArrayList<PunchLogs>();
        Cursor cur;
        String query = "SELECT *, " + TABLE_EMPLOYEES + "." + KEY_EMP_NAME + " AS emp_name, " + TABLE_EMPLOYEES + "." + KEY_EMP_PHOTO + " AS emp_photo" + " FROM " + TABLE_PUNCHLOGS + " LEFT JOIN " + TABLE_EMPLOYEES + " ON " + TABLE_PUNCHLOGS + "." + KEY_PUNCHLOGS_EMP_ID + "=" + TABLE_EMPLOYEES + "." + KEY_EMP_ID;// + " WHERE " + KEY_PUNCHLOGS_CAT_ID + "=" + categoryID + " AND " + KEY_PUNCHLOGS_AREA_ID + "=" + areaID;
        // for categories
        if (categoryID.size() > 0) {
            query = query + " WHERE " + KEY_PUNCHLOGS_CAT_ID + " IN (";
            for (int k = 0; k < categoryID.size(); k++) {
                int catID = categoryID.get(k);
                if (categoryID.size() == 1) {
                    query = query + catID;
                } else {
                    if (k == 0) {
                        query = query + catID;
                    } else {
                        query = query + "," + catID;
                    }
                }
            }
            query = query + ") AND ";
        }
        // for areas
        if (areaID.size() > 0) {
            query = query + KEY_PUNCHLOGS_AREA_ID + " IN (";
            for (int k = 0; k < areaID.size(); k++) {
                int areaId = areaID.get(k);
                if (areaID.size() == 1) {
                    query = query + areaId;
                } else {
                    if (k == 0) {
                        query = query + areaId;
                    } else {
                        query = query + "," + areaId;
                    }
                }
            }
            query = query + ") ORDER BY " + KEY_PUNCHLOGS_DATETIME + " DESC";
        }
        Log.d("SQL Query", "Select Query : " + query);
        cur = db.rawQuery(query, null);
        System.out.println("Cursor count" + cur.getCount());
        /*if (areaID > -1) {
            cur = db.rawQuery("SELECT * FROM " + TABLE_PUNCHLOGS + " WHERE " + KEY_PUNCHLOGS_CAT_ID + "=" + categoryID + " AND " + KEY_PUNCHLOGS_AREA_ID + "=" + areaID, null);
        } else {
            cur = db.rawQuery("SELECT * FROM " + TABLE_PUNCHLOGS + " WHERE " + KEY_PUNCHLOGS_CAT_ID + "=" + categoryID , null);
        }*/
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    int sln_punch = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_SLN_PUNCH));
                    int punchlogs_emp_id = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_EMP_ID));
                    String punchlogs_card_number = cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_CARD_NO));
                    int sln_reader = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_SLN_READER));
                    String reader_name = cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_READER_NAME));
                    String punch_date = cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_DATETIME));
                    int grant_deny = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_GRANT_DENY));// > 0;
                    int sln_location = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_LOC_ID));
                    int sln_area_id = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_AREA_ID));
                    int category_id = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_CAT_ID));
                    String emp_name = cur.getString(cur.getColumnIndexOrThrow("emp_name"));
                    String emp_photo = cur.getString(cur.getColumnIndexOrThrow("emp_photo"));

                    PunchLogs pl = new PunchLogs();
                    pl.setSLN_Punch(sln_punch);
                    //pl.setSLN_Employee(punchlogs_emp_id);
                    pl.setSLN_EmployeeName(emp_name);//cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_EMP_NAME)));
                    pl.setSLN_EmployeePhoto(emp_photo);//cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_EMP_PHOTO)));
                    pl.setCard_Number(punchlogs_card_number);
                    pl.setSLN_Reader(sln_reader);
                    pl.setReader_Name(reader_name);
                    pl.setPunch_Date(punch_date);
                    pl.setIsPunchSynced(grant_deny);
                    pl.setSLN_Location(sln_location);
                    pl.setSLN_Area_ID(sln_area_id);
                    pl.setMusteringId(category_id);

                    //PunchLogs pl_update = getDetailsFromEmployees(pl);
                    Log.d("Database Handler", "PL List Emp Id" + pl.getSLN_Employee() + " Emp Name: " + pl.getSLN_EmployeeName() + " Emp Photo: " + pl.getSLN_EmployeePhoto() + " Emp CardNo: " + pl.getCard_Number() + " Emp Loc: " + pl.getSLN_Location());
                    list.add(pl);
                } while (cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return list;
    }

    // to check whether punch sync or not
    public boolean checkNotSyncedPunchEntries() {
        boolean st = false;
        SQLiteDatabase db = getWritableDatabase();
        String query = "Select * FROM " + TABLE_PUNCHLOGS + " WHERE " + KEY_PUNCHLOGS_GRANT_DENY + "=" + 0;
        Cursor cur = db.rawQuery(query, null);
        if (cur != null && cur.getCount() > 0) {
            st = true;
        }
        cur.close();
        return st;
    }

    // to get Not synch punch entries
    public ArrayList<PunchLogs> getNotSyncPunchEntries() {
        ArrayList<PunchLogs> list = new ArrayList<PunchLogs>();
        SQLiteDatabase db = getWritableDatabase();
        String query = "Select * FROM " + TABLE_MEM_ACC_LOGS + " WHERE " + KEY_MEM_ACC_LOGS_IS_SYNC + "=" + 0 + " ORDER BY " + KEY_MEM_ACC_LOGS_PUNCH_TIME + " LIMIT 10";
        Cursor cur = db.rawQuery(query, null);
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    int sln_punch = cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_AUTO_ID));
                    String punchlogs_emp_id = cur.getString(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_STU_ID));
                    String punchlogs_card_number = cur.getString(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_CARD_NO));
                    int sln_reader = cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_READER_ID));
                    int sln_session = cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_SESSION_ID));
                    String reader_name = cur.getString(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_READER_NAME));
                    String session_name = cur.getString(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_SESSION_NAME));
                    String punch_date = cur.getString(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_PUNCH_TIME));
                    int accessCode = cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_ACCESS_GRANT));// > 0;
                    int isSync = cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_IS_SYNC));

                    PunchLogs pl = new PunchLogs();
                    pl.setSLN_Punch(sln_punch);
                    pl.setSLN_Employee(punchlogs_emp_id);
                    //pl.setSLN_EmployeeName(cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_EMP_NAME)));
                    //pl.setSLN_EmployeePhoto(cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_EMP_PHOTO)));
                    pl.setCard_Number(punchlogs_card_number);
                    pl.setSLN_Reader(sln_reader);
                    pl.setReader_Name(reader_name);
                    pl.setSessionId(sln_session);
                    pl.setSessionName(session_name);
                    pl.setPunch_Date(punch_date);
                    pl.setIsPunchSynced(isSync);
                    pl.setAccessCode(accessCode);
                    pl.setSLN_Location(0);
                    pl.setSLN_Area_ID(0);
                    pl.setMusteringId(0);

                    //PunchLogs pl_update = getDetailsFromEmployees(pl);
                    Log.d("Database Handler", "PL List Emp Id" + pl.getSLN_Employee() + " Emp Name: " + pl.getSLN_EmployeeName() + " Emp Photo: " + pl.getSLN_EmployeePhoto() + " Emp CardNo: " + pl.getCard_Number() + " Emp Loc: " + pl.getSLN_Location());
                    list.add(pl);
                } while (cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return list;
    }

    public ArrayList<PunchLogs> searchEmployeeName(String searchText) {
        ArrayList<PunchLogs> list = new ArrayList<PunchLogs>();
        SQLiteDatabase db = getWritableDatabase();
        String query = "Select *, " + TABLE_EMPLOYEES + "." + KEY_EMP_NAME + " AS emp_name, " + TABLE_EMPLOYEES + "." + KEY_EMP_PHOTO + " AS emp_photo" + " FROM " + TABLE_PUNCHLOGS + " LEFT JOIN " + TABLE_EMPLOYEES + " ON " + TABLE_PUNCHLOGS + "." + KEY_PUNCHLOGS_EMP_ID + "=" + TABLE_EMPLOYEES + "." + KEY_EMP_ID + " WHERE " + TABLE_PUNCHLOGS + "." + KEY_PUNCHLOGS_EMP_ID + " IN (Select " + KEY_EMP_ID + " FROM " + TABLE_EMPLOYEES + " WHERE " + KEY_EMP_NAME + " LIKE '%" + searchText + "%' OR " + KEY_EMP_ID + " LIKE '%" + searchText + "%') OR " + TABLE_PUNCHLOGS + "." + KEY_PUNCHLOGS_CARD_NO + " LIKE '%" + searchText + "%'";
        System.out.println("SearchEmployeeQuery: " + query);
        Cursor cur = db.rawQuery(query, null);//new String[] {"%"+searchText+"%"});
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    int sln_punch = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_SLN_PUNCH));
                    int punchlogs_emp_id = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_EMP_ID));
                    String punchlogs_card_number = cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_CARD_NO));
                    int sln_reader = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_SLN_READER));
                    String reader_name = cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_READER_NAME));
                    String punch_date = cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_DATETIME));
                    int grant_deny = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_GRANT_DENY));// > 0;
                    int sln_location = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_LOC_ID));
                    int sln_area_id = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_AREA_ID));
                    int category_id = cur.getInt(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_CAT_ID));
                    String emp_name = cur.getString(cur.getColumnIndexOrThrow("emp_name"));
                    String emp_photo = cur.getString(cur.getColumnIndexOrThrow("emp_photo"));

                    PunchLogs pl = new PunchLogs();
                    //pl.setSLN_Punch(sln_punch);
                    //pl.setSLN_Employee(punchlogs_emp_id);
                    pl.setSLN_EmployeeName(emp_name);//cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_EMP_NAME)));
                    pl.setSLN_EmployeePhoto(emp_photo);//cur.getString(cur.getColumnIndexOrThrow(KEY_PUNCHLOGS_EMP_PHOTO)));
                    pl.setCard_Number(punchlogs_card_number);
                    pl.setSLN_Reader(sln_reader);
                    pl.setReader_Name(reader_name);
                    pl.setPunch_Date(punch_date);
                    pl.setIsPunchSynced(grant_deny);
                    pl.setSLN_Location(sln_location);
                    pl.setSLN_Area_ID(sln_area_id);
                    pl.setMusteringId(category_id);

                    //PunchLogs pl_update = getDetailsFromEmployees(pl);
                    Log.d("Database Handler", "PL List Emp Id" + pl.getSLN_Employee() + " Emp Name: " + pl.getSLN_EmployeeName() + " Emp Photo: " + pl.getSLN_EmployeePhoto() + " Emp CardNo: " + pl.getCard_Number() + " Emp Loc: " + pl.getSLN_Location() + " Emp Punch : " + pl.getPunch_Date());
                    list.add(pl);
                } while (cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return list;
    }

    public void updateMemAccLogs (List<MemAccLogsInfoModel.DataBean> list) {
        SQLiteDatabase db = this.getWritableDatabase();

        for (int i=0; i<list.size();i++) {
            String student_id = list.get(i).getStudentID();
            String query = "UPDATE " + TABLE_MEM_ACC_LOGS + " SET " + KEY_MEM_ACC_LOGS_IS_SYNC +"=1" + " WHERE " + KEY_MEM_ACC_LOGS_STU_ID + "='" + student_id + "'";
            Cursor cur = db.rawQuery(query, null);//new String[] {"%"+searchText+"%"});
            if (cur != null && cur.getCount() > 0) {

            }


        }
    }


    public void updateEmployeesLocations(PunchLogs pl) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values_employee = new ContentValues();
        values_employee.put(KEY_PUNCHLOGS_EMP_ID, pl.getSLN_Employee());
        values_employee.put(KEY_PUNCHLOGS_CARD_NO, pl.getCard_Number());
        values_employee.put(KEY_PUNCHLOGS_SLN_READER, pl.getSLN_Reader());
        values_employee.put(KEY_PUNCHLOGS_READER_NAME, pl.getReader_Name());
        values_employee.put(KEY_PUNCHLOGS_DATETIME, pl.getPunch_Date());
        values_employee.put(KEY_PUNCHLOGS_GRANT_DENY, pl.getIsPunchSynced());
        values_employee.put(KEY_PUNCHLOGS_LOC_ID, pl.getSLN_Location());
        values_employee.put(KEY_PUNCHLOGS_AREA_ID, pl.getSLN_Area_ID());
        values_employee.put(KEY_PUNCHLOGS_CAT_ID, pl.getMusteringId());

        db.update(TABLE_PUNCHLOGS, values_employee, KEY_PUNCHLOGS_CARD_NO + "='" + pl.getCard_Number() + "' COLLATE NOCASE", null);
    }

    // Based on mustering type get employees count
    public ArrayList<Statistics> getCountMusteringTypeBased() {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Statistics> list = new ArrayList<Statistics>();
        ArrayList<Categories> must_list = getCategories();
        if (must_list.size() > 0) {
            for (int j = 0; j < must_list.size(); j++) {
                Statistics st = new Statistics();
                int must_id = must_list.get(j).getMusteringId();
                String must_name = must_list.get(j).getMusteringName();

                st.setMusterId(must_id);
                st.setMusterName(must_name);

                String query = "SELECT * FROM " + TABLE_PUNCHLOGS + " WHERE " + KEY_PUNCHLOGS_CAT_ID + "=" + must_id;
                Cursor cur = db.rawQuery(query, null);
                if (cur != null) {
                    int must_count = cur.getCount();
                    st.setMusterCount(must_count);
                } else {
                    st.setMusterCount(0);
                }
                list.add(st);
            }
        }
        return list;
    }

    //----------------------------------------------- Student Access List ------------------------------------------------------------------

    public void saveAccessList (ArrayList<AccessListModel.DataBean> acc_list_data) {
        SQLiteDatabase db = this.getWritableDatabase();
        // content values for categories
        ContentValues values_acc_list = new ContentValues();
        db.beginTransaction();
        for (AccessListModel.DataBean acclist : acc_list_data) {
            //String readersID = acclist.getReadersId();
            String sessionsID = acclist.getSessionsId();
            String studentId = acclist.getStudentsIds();
            String canteenId = acclist.getCanteenId();
            String canteenName = acclist.getCanteenName();
            String agIds = acclist.getAgIds();

            if (agIds == null) {
                agIds = "";
            }

            if (canteenId == null) {
                canteenId = "";
            }

            if (canteenName == null) {
                canteenName = "";
            }

            if (agIds.contains(",")) {
                String[] agID = agIds.split(",");
                if (sessionsID.contains(",")) {
                    String[] sessionID = sessionsID.split(",");
                    for (int i=0;i<agID.length;i++) {
                        for (int j=0;j<sessionID.length;j++) {
                            String ag_id = agID[i];
                            String session_id = sessionID[j];

                            values_acc_list.put(KEY_ACC_LIST_AG_ID, ag_id);
                            values_acc_list.put(KEY_ACC_LIST_STUDENTS_ID, studentId);
                            values_acc_list.put(KEY_ACC_LIST_SESSIONS_ID, session_id);
                            values_acc_list.put(KEY_ACC_LIST_CANTEEN_ID, canteenId);
                            values_acc_list.put(KEY_ACC_LIST_CANTEEN_NAME, canteenName);
                            if (!isStaffAccessListAlreadyExists(db, studentId, ag_id, session_id)) {
                                db.insert(TABLE_ACCESS_LIST, null, values_acc_list);
                            } else {
                                db.update(TABLE_ACCESS_LIST, values_acc_list, KEY_ACC_LIST_STUDENTS_ID + "='" + acclist.getStudentsIds() + "' AND " + KEY_ACC_LIST_READERS_ID + "='" + acclist.getReadersId() + "' AND " + KEY_ACC_LIST_SESSIONS_ID + "'=" + session_id + "'", null);
                            }

                        }
                    }
                } else {
                    for (int i=0;i<agID.length;i++) {
                        String ag_id = agID[i];
                        String session_id = sessionsID;
                        values_acc_list.put(KEY_ACC_LIST_AG_ID, ag_id);
                        values_acc_list.put(KEY_ACC_LIST_STUDENTS_ID, studentId);
                        values_acc_list.put(KEY_ACC_LIST_SESSIONS_ID, session_id);
                        values_acc_list.put(KEY_ACC_LIST_CANTEEN_ID, canteenId);
                        values_acc_list.put(KEY_ACC_LIST_CANTEEN_NAME, canteenName);
                        if (!isStaffAccessListAlreadyExists(db, studentId, ag_id, session_id)) {
                            db.insert(TABLE_ACCESS_LIST, null, values_acc_list);
                        } else {
                            db.update(TABLE_ACCESS_LIST, values_acc_list, KEY_ACC_LIST_STUDENTS_ID + "='" + acclist.getStudentsIds() + "' AND " + KEY_ACC_LIST_READERS_ID + "='" + acclist.getReadersId() + "' AND " + KEY_ACC_LIST_SESSIONS_ID + "'=" + session_id + "'", null);
                        }
                    }
                }

            } else {
                if (sessionsID.contains(",")) {
                    String[] sessionID = sessionsID.split(",");

                    for (int j = 0; j < sessionID.length; j++) {
                        String ag_id = agIds;
                        String session_id = sessionID[j];

                        values_acc_list.put(KEY_ACC_LIST_AG_ID, ag_id);
                        values_acc_list.put(KEY_ACC_LIST_STUDENTS_ID, studentId);
                        values_acc_list.put(KEY_ACC_LIST_SESSIONS_ID, session_id);
                        values_acc_list.put(KEY_ACC_LIST_CANTEEN_ID, canteenId);
                        values_acc_list.put(KEY_ACC_LIST_CANTEEN_NAME, canteenName);
                        if (!isStaffAccessListAlreadyExists(db, studentId, ag_id, session_id)) {
                            db.insert(TABLE_ACCESS_LIST, null, values_acc_list);
                        } else {
                            db.update(TABLE_ACCESS_LIST, values_acc_list, KEY_ACC_LIST_STUDENTS_ID + "='" + acclist.getStudentsIds() + "' AND " + KEY_ACC_LIST_READERS_ID + "='" + acclist.getReadersId() +  "' AND " + KEY_ACC_LIST_SESSIONS_ID + "'=" + session_id + "'", null);
                        }

                    }
                } else {
                    String ag_id = agIds;
                    String session_id = sessionsID;
                    values_acc_list.put(KEY_ACC_LIST_AG_ID, ag_id);
                    values_acc_list.put(KEY_ACC_LIST_STUDENTS_ID, studentId);
                    values_acc_list.put(KEY_ACC_LIST_SESSIONS_ID, session_id);
                    values_acc_list.put(KEY_ACC_LIST_CANTEEN_ID, canteenId);
                    values_acc_list.put(KEY_ACC_LIST_CANTEEN_NAME, canteenName);
                    if (!isStaffAccessListAlreadyExists(db, studentId, ag_id, session_id)) {
                        db.insert(TABLE_ACCESS_LIST, null, values_acc_list);
                    } else {
                        db.update(TABLE_ACCESS_LIST, values_acc_list, KEY_ACC_LIST_STUDENTS_ID + "='" + acclist.getStudentsIds() + "' AND " + KEY_ACC_LIST_READERS_ID + "='" + acclist.getReadersId() + "' AND " + KEY_ACC_LIST_SESSIONS_ID + "'=" + session_id + "'", null);
                    }
                }
            }


        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    // get student list which sync in student access list
    public ArrayList<StudentInfoModel.DataBean> getAllStudentInfoInStudentAccess () {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<StudentInfoModel.DataBean> stu_list = new ArrayList<StudentInfoModel.DataBean>();
        Cursor cur = db.rawQuery("SELECT *,* FROM " + TABLE_STU_INFO + " INNER JOIN " + TABLE_ACCESS_LIST + " ON " + TABLE_STU_INFO + "." + KEY_STU_ID + "=" + TABLE_ACCESS_LIST + "." + KEY_ACC_LIST_STUDENTS_ID + " GROUP BY " + TABLE_STU_INFO + "." + KEY_STU_ID, null);
        if (cur != null && cur.getCount() > 0) {

            if  (cur.moveToFirst()) {
                do {
                    StudentInfoModel.DataBean sim = new StudentInfoModel.DataBean();
                    sim.setStudentID(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ID)));
                    sim.setFirstName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FIRST_NAME)));
                    sim.setFatherName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FATHER_NAME)));
                    sim.setGrandFatherName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GRAND_FATHER_NAME)));
                    sim.setGender(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GENDER)));
                    sim.setDateOfBirth(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DOB)));
                    sim.setSignature(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_SIG)));
                    sim.setCollege(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_COLLEGE)));
                    sim.setDepartment(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DEPT)));
                    sim.setCampus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CAMPUS)));
                    sim.setProgram(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_PROGRAM)));
                    sim.setDegreeType(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DEGREE_TYPE)));
                    sim.setAdmissionType(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ADD_TYPE)));
                    sim.setAdmissionTypeShort(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ADD_TYPE_SHORT)));
                    sim.setValidDateUntil(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_VALID_DATE_UNTIL)));
                    sim.setIssueDate(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ISSUE_DATE)));
                    sim.setMealNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_MEAL_NO)));
                    sim.setUniqueNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_UNIQE_NO)));
                    sim.setStatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_STATUS)));
                    sim.setIsactive(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ISACTIVE)));
                    sim.setCardNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_NO)));
                    sim.setCardstatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_ST)));
                    sim.setStudentImage(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_IMAGE)));

                    stu_list.add(sim);

                } while(cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return stu_list;
    }



    public boolean isAccessListAlreadyExists(SQLiteDatabase db, String studentIds, String readerId, String sessionId) {
        Cursor cur = db.rawQuery("SELECT * FROM "+TABLE_ACCESS_LIST + " WHERE " + KEY_ACC_LIST_STUDENTS_ID + "='" + studentIds + "' AND " + KEY_ACC_LIST_AG_ID + "='" + readerId + "' AND " + KEY_ACC_LIST_SESSIONS_ID + "='" + sessionId + "'", null);
        //System.out.println("SearchEmployeeQuery: " + query);
        //Cursor cur = db.rawQuery(query, null);//new String[] {"%"+searchText+"%"});
        if (cur != null && cur.getCount() > 0) {
            return true;
        }
        cur.close();
        return false;
    }

    public boolean checkAccessList() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM "+TABLE_ACCESS_LIST, null);
        //System.out.println("SearchEmployeeQuery: " + query);
        //Cursor cur = db.rawQuery(query, null);//new String[] {"%"+searchText+"%"});
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            //db.close();
            return true;
        }
        cur.close();
        //db.close();
        return false;
    }

    public void deleteAllAccessList() {
        SQLiteDatabase db = this.getWritableDatabase();
        if (checkAccessList()) {
            db.delete(TABLE_ACCESS_LIST, null, null);
        }
        //db.close();
    }

    //----------------------------------------------- Staff Access List ------------------------------------------------------------------

    public void saveStaffAccessList (ArrayList<StaffAccessListModel.DataBean> acc_list_data) {

        SQLiteDatabase db = this.getWritableDatabase();
        // content values for categories
        ContentValues values_acc_list = new ContentValues();
        db.beginTransaction();
        for (StaffAccessListModel.DataBean acclist : acc_list_data) {
            String agIDs = acclist.getStaffAgIds();
            String sessionsID = acclist.getSessionsId();
            String staffId = acclist.getStaffId();

            if (agIDs == null)
                agIDs = "";

            if (sessionsID == null)
                sessionsID = "";

            if (agIDs.contains(",")) {
                String[] agID = agIDs.split(",");
                if (sessionsID.contains(",")) {
                    String[] sessionID = sessionsID.split(",");
                    for (int i=0;i<agID.length;i++) {
                        for (int j=0;j<sessionID.length;j++) {
                            String ag_id = agID[i];
                            String session_id = sessionID[j];

                            values_acc_list.put(KEY_STAFF_ACC_LIST_AG_ID, ag_id);
                            values_acc_list.put(KEY_STAFF_ACC_LIST_STAFF_ID, staffId);
                            values_acc_list.put(KEY_STAFF_ACC_LIST_SESSIONS_ID, session_id);
                            if (!isStaffAccessListAlreadyExists(db, staffId, ag_id, session_id)) {
                                db.insert(TABLE_STAFF_ACCESS_LIST, null, values_acc_list);
                            } else {
                                db.update(TABLE_STAFF_ACCESS_LIST, values_acc_list, KEY_STAFF_ACC_LIST_STAFF_ID + "='" + acclist.getStaffId() + "' AND " + KEY_STAFF_ACC_LIST_AG_ID + "='" + ag_id + "' AND " + KEY_ACC_LIST_SESSIONS_ID + "'=" + session_id + "'", null);
                            }

                        }
                    }
                } else {
                    for (int i=0;i<agID.length;i++) {
                        String ag_id = agID[i];
                        String session_id = sessionsID;
                        values_acc_list.put(KEY_STAFF_ACC_LIST_AG_ID, ag_id);
                        values_acc_list.put(KEY_STAFF_ACC_LIST_STAFF_ID, staffId);
                        values_acc_list.put(KEY_STAFF_ACC_LIST_SESSIONS_ID, session_id);
                        if (!isStaffAccessListAlreadyExists(db, staffId, ag_id, session_id)) {
                            db.insert(TABLE_STAFF_ACCESS_LIST, null, values_acc_list);
                        } else {
                            db.update(TABLE_STAFF_ACCESS_LIST, values_acc_list, KEY_STAFF_ACC_LIST_STAFF_ID + "='" + acclist.getStaffId() + "' AND " + KEY_STAFF_ACC_LIST_AG_ID + "='" + ag_id + "' AND " + KEY_STAFF_ACC_LIST_SESSIONS_ID + "'=" + session_id + "'", null);
                        }
                    }
                }

            } else {
                if (sessionsID.contains(",")) {
                    String[] sessionID = sessionsID.split(",");

                    for (int j = 0; j < sessionID.length; j++) {
                        String ag_id = agIDs;
                        String session_id = sessionID[j];

                        values_acc_list.put(KEY_STAFF_ACC_LIST_AG_ID, ag_id);
                        values_acc_list.put(KEY_STAFF_ACC_LIST_STAFF_ID, staffId);
                        values_acc_list.put(KEY_STAFF_ACC_LIST_SESSIONS_ID, session_id);
                        if (!isStaffAccessListAlreadyExists(db, staffId, ag_id, session_id)) {
                            db.insert(TABLE_STAFF_ACCESS_LIST, null, values_acc_list);
                        } else {
                            db.update(TABLE_STAFF_ACCESS_LIST, values_acc_list, KEY_STAFF_ACC_LIST_STAFF_ID + "='" + acclist.getStaffId() + "' AND " + KEY_STAFF_ACC_LIST_AG_ID + "='" + ag_id +  "' AND " + KEY_STAFF_ACC_LIST_SESSIONS_ID + "'=" + session_id + "'", null);
                        }

                    }
                } else {
                    String ag_id = agIDs;
                    String session_id = sessionsID;
                    values_acc_list.put(KEY_STAFF_ACC_LIST_READERS_ID, ag_id);
                    values_acc_list.put(KEY_STAFF_ACC_LIST_STAFF_ID, staffId);
                    values_acc_list.put(KEY_STAFF_ACC_LIST_SESSIONS_ID, session_id);
                    if (!isStaffAccessListAlreadyExists(db, staffId, ag_id, session_id)) {
                        db.insert(TABLE_STAFF_ACCESS_LIST, null, values_acc_list);
                    } else {
                        db.update(TABLE_STAFF_ACCESS_LIST, values_acc_list, KEY_STAFF_ACC_LIST_STAFF_ID + "='" + acclist.getStaffId() + "' AND " + KEY_STAFF_ACC_LIST_AG_ID + "='" + ag_id + "' AND " + KEY_STAFF_ACC_LIST_SESSIONS_ID + "'=" + session_id + "'", null);
                    }
                }
            }


        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public ArrayList<StaffInfoModel.DataBean> getAllStaffInfoInStaffAccess () {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<StaffInfoModel.DataBean> stu_list = new ArrayList<StaffInfoModel.DataBean>();
        Cursor cur = db.rawQuery("SELECT *,* FROM " + TABLE_STAFF_INFO + " INNER JOIN " + TABLE_STAFF_ACCESS_LIST + " ON " + TABLE_STAFF_INFO + "." + KEY_STAFF_ID + "=" + TABLE_STAFF_ACCESS_LIST + "." + KEY_STAFF_ACC_LIST_STAFF_ID + " GROUP BY " + TABLE_STAFF_INFO + "." + KEY_STAFF_ID, null);
        if (cur != null && cur.getCount() > 0) {

            if  (cur.moveToFirst()) {
                do {
                    StaffInfoModel.DataBean sim = new StaffInfoModel.DataBean();
                    sim.setStaffID(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ID)));
                    sim.setAppNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_APP_NO)));
                    sim.setSlNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_SL_NO)));
                    sim.setUID(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_UID)));
                    sim.setFullName(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_FULL_NAME)));
                    sim.setGender(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_GENDER)));
                    sim.setDob(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_DOB)));
                    sim.setDepartment(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_DEPT)));
                    sim.setJobTitle(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_JOB_TITLE)));
                    sim.setEmpPhoto(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_EMP_PHOTO)));
                    sim.setSignature(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_SIG)));
                    sim.setAddress(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ADD)));
                    sim.setIdNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ID_NO)));
                    sim.setIssueDate(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ISSUE_DATE)));
                    sim.setCardstatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_CARD_ST)));
                    sim.setCardNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_CARD_NO)));
                    sim.setIsactive(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ISACTIVE)));
                    sim.setEmailId(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_EMAIL_ID)));
                    sim.setPassword(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_PASSWORD)));
                    sim.setIdNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ID_NUMBER)));


                    stu_list.add(sim);

                } while(cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return stu_list;
    }


    public ArrayList<String> getAllReadersOfStaffAccList (String staffId) {

        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> list = new ArrayList<String>();
        String query = "SELECT DISTINCT(" + KEY_STAFF_ACC_LIST_READERS_ID + ") FROM " + TABLE_STAFF_ACCESS_LIST + "  WHERE " + KEY_STAFF_ACC_LIST_STAFF_ID + "='" + staffId + "'";
        Cursor cur = db.rawQuery(query, null);
        System.out.println("Reader Count "+cur.getCount());
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    String readerId = cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ACC_LIST_READERS_ID));
                    System.out.println("ReaderId : "+readerId);
                    String readerName = getReaderNameFromId(readerId);
                    System.out.println("Readername:"+readerName);
                    if (readerName.length() > 0) {
                        list.add(readerName);
                    }
                } while (cur.moveToNext());
            }

        }

        return list;
    }


    public boolean isStaffAccessListAlreadyExists(SQLiteDatabase db, String staffId, String readerId, String sessionId) {
        Cursor cur = db.rawQuery("SELECT * FROM "+TABLE_STAFF_ACCESS_LIST + " WHERE " + KEY_STAFF_ACC_LIST_STAFF_ID + "='" + staffId + "' AND " + KEY_STAFF_ACC_LIST_AG_ID + "='" + readerId + "' AND " + KEY_STAFF_ACC_LIST_SESSIONS_ID + "='" + sessionId + "'", null);
        //System.out.println("SearchEmployeeQuery: " + query);
        //Cursor cur = db.rawQuery(query, null);//new String[] {"%"+searchText+"%"});
        if (cur != null && cur.getCount() > 0) {
            return true;
        }
        cur.close();
        return false;
    }

    public boolean checkStaffAccessList() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM "+TABLE_STAFF_ACCESS_LIST, null);
        //System.out.println("SearchEmployeeQuery: " + query);
        //Cursor cur = db.rawQuery(query, null);//new String[] {"%"+searchText+"%"});
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            //db.close();
            return true;
        }
        cur.close();
        //db.close();
        return false;
    }

    public void deleteAllStaffAccessList() {
        SQLiteDatabase db = this.getWritableDatabase();
        if (checkAccessList()) {
            db.delete(TABLE_STAFF_ACCESS_LIST, null, null);
        }
        //db.close();
    }

    //----------------------------------------------- Member Access -----------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------
    public void addMemberData(MusteringReaders mustReader) {
        SQLiteDatabase db = this.getWritableDatabase();
        // content values for categories
        ContentValues mem_data = new ContentValues();
        mem_data.put(KEY_MEM_ACC_EMP_ID, mustReader.getSLN_Employee());
        mem_data.put(KEY_MEM_ACC_CARD_NO, mustReader.getCard_Number());
        mem_data.put(KEY_MEM_ACC_READER_ID, mustReader.getSLN_Reader());
        //if (!isReaderAlreadyExists(db, reader.getSLN_Reader())) {
        db.insert(TABLE_MEM_ACC, null, mem_data);
        /*} else {
            db.update(TABLE_READERS, values_cat, KEY_READER_ID + "=" + reader.getSLN_Reader(), null);
        }*/
        db.close();
    }
    // batch insert for the member access
    public void saveMemberData(ArrayList<MusteringReaders> data) {
        SQLiteDatabase db = this.getWritableDatabase();
        // content values for categories
        ContentValues mem_data = new ContentValues();
        db.beginTransaction();
        for (MusteringReaders mustReader : data) {
            mem_data.put(KEY_MEM_ACC_EMP_ID, mustReader.getSLN_Employee());
            mem_data.put(KEY_MEM_ACC_CARD_NO, mustReader.getCard_Number());

            mem_data.put(KEY_MEM_ACC_READER_ID, mustReader.getSLN_Reader());
            //if (!isReaderAlreadyExists(db, reader.getSLN_Reader())) {
            db.insert(TABLE_MEM_ACC, null, mem_data);
            /*} else {
                db.update(TABLE_READERS, values_cat, KEY_READER_ID + "=" + reader.getSLN_Reader(), null);
            }*/
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public boolean checkMemberData () {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_MEM_ACC, null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public void deleteMemberData () {
        if (checkMemberData()) {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_MEM_ACC, null, null);
        }
    }

    public ArrayList<Employee> getAllMembersData (int firstVal, int lastVal) {
        ArrayList<Employee> employee_list = new ArrayList<Employee>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT *, "+ TABLE_EMPLOYEES + "." + KEY_EMP_NAME + " AS emp_name, " + TABLE_EMPLOYEES + "." + KEY_EMP_PHOTO + " AS emp_photo" +" FROM " + TABLE_MEM_ACC + " LEFT JOIN " + TABLE_EMPLOYEES + " ON " + TABLE_MEM_ACC + "." + KEY_MEM_ACC_EMP_ID + "=" + TABLE_EMPLOYEES + "." + KEY_EMP_ID + " WHERE " + TABLE_EMPLOYEES + "." + KEY_EMP_AUTO_ID + ">=" + firstVal + " AND " + TABLE_EMPLOYEES + "." + KEY_EMP_AUTO_ID + "<=" + lastVal + " GROUP BY " + TABLE_EMPLOYEES + "." + KEY_EMP_ID;
        Cursor cur = db.rawQuery(query, null);
        Log.d("Cursor ", "cur count : "+cur.getCount());
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    Employee employee = new Employee();
                    employee.setSLN_Employee(cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_EMP_ID)));
                    employee.setEmployeeName(cur.getString(cur.getColumnIndexOrThrow("emp_name")));
                    employee.setEmployee_Photo(cur.getString(cur.getColumnIndexOrThrow("emp_photo")));
                    employee.setCard_Number(cur.getString(cur.getColumnIndexOrThrow(KEY_MEM_ACC_CARD_NO)));
                    employee.setSLN_Reader(cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_READER_ID)));
                    employee_list.add(employee);
                } while (cur.moveToNext());
            }
            //return cat_list;
        }
        cur.close();
        db.close();
        return employee_list;
    }

    public ArrayList<Employee> getAllMembersDataFromSearch (String searchStr) {
        ArrayList<Employee> employee_list = new ArrayList<Employee>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT *, "+ TABLE_EMPLOYEES + "." + KEY_EMP_NAME + " AS emp_name, " + TABLE_EMPLOYEES + "." + KEY_EMP_PHOTO + " AS emp_photo" +" FROM " + TABLE_MEM_ACC + " LEFT JOIN " + TABLE_EMPLOYEES + " ON " + TABLE_MEM_ACC + "." + KEY_MEM_ACC_EMP_ID + "=" + TABLE_EMPLOYEES + "." + KEY_EMP_ID + " WHERE " + TABLE_EMPLOYEES + "." + KEY_EMP_ID + " LIKE '%" + searchStr + "%' OR " + TABLE_EMPLOYEES + "." + KEY_EMP_NAME + " LIKE '%" + searchStr + "%' OR " + TABLE_EMPLOYEES + "." + KEY_EMP_CARD_NO + " LIKE '%" + searchStr + "%' GROUP BY " + TABLE_EMPLOYEES + "." + KEY_EMP_ID;
        Cursor cur = db.rawQuery(query, null);
        Log.d("Cursor ", "cur count : "+cur.getCount());
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    Employee employee = new Employee();
                    employee.setSLN_Employee(cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_EMP_ID)));
                    employee.setEmployeeName(cur.getString(cur.getColumnIndexOrThrow("emp_name")));
                    employee.setEmployee_Photo(cur.getString(cur.getColumnIndexOrThrow("emp_photo")));
                    employee.setCard_Number(cur.getString(cur.getColumnIndexOrThrow(KEY_MEM_ACC_CARD_NO)));
                    employee.setSLN_Reader(cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_READER_ID)));
                    employee_list.add(employee);
                } while (cur.moveToNext());
            }
            //return cat_list;
        }
        cur.close();
        db.close();
        return employee_list;
    }

    public ArrayList<String> getAllReadersOfMemberAccess (int empId, String cardNo) {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<String> list = new ArrayList<String>();
        // DISTINCT added to more filter the result and ignore if any duplicate reader id for any employee if available
        String query = "SELECT DISTINCT " + KEY_MEM_ACC_READER_ID + " FROM " + TABLE_MEM_ACC  + " WHERE " + KEY_MEM_ACC_EMP_ID + "=" + empId + " AND " + KEY_MEM_ACC_CARD_NO + "='" + cardNo+"'";
        Cursor cur = db.rawQuery(query, null);
        Log.d("Cursor ", "cur count : "+cur.getCount());
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    String reader_id = cur.getString(cur.getColumnIndexOrThrow(KEY_MEM_ACC_READER_ID));
                    if (reader_id != null) {
                        if (reader_id.length() > 0) {
                            String query_reader_info = "SELECT " + KEY_READER_NAME + " FROM " + TABLE_READERS + " WHERE " + KEY_READER_ID + "=" + reader_id;
                            Cursor cur_reader = db.rawQuery(query_reader_info, null);
                            //Log.d("Cursor ", "cur count : "+cur_reader.getCount());
                            if (cur_reader != null && cur_reader.getCount() > 0) {
                                if (cur_reader.moveToFirst()) {
                                    do {
                                        String reader_name = cur_reader.getString(cur_reader.getColumnIndexOrThrow(KEY_READER_NAME));
                                        Log.d("ReaderName", reader_name);
                                        list.add(reader_name);
                                    } while (cur_reader.moveToNext());
                                }
                            }
                        }
                    }
                } while (cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return list;
    }

    public ArrayList<String> getAllReadersOfAccList (String studentId) {

        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> list = new ArrayList<String>();
        String query = "SELECT DISTINCT(" + KEY_ACC_LIST_READERS_ID + ") FROM " + TABLE_ACCESS_LIST + "  WHERE " + KEY_ACC_LIST_STUDENTS_ID + "='" + studentId + "'";
        Cursor cur = db.rawQuery(query, null);
        System.out.println("Reader Count "+cur.getCount());
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    String readerId = cur.getString(cur.getColumnIndexOrThrow(KEY_ACC_LIST_READERS_ID));
                    System.out.println("ReaderId : "+readerId);
                    String readerName = getReaderNameFromId(readerId);
                    System.out.println("Readername:"+readerName);
                    if (readerName.length() > 0) {
                        list.add(readerName);
                    }
                } while (cur.moveToNext());
            }

        }

        return list;
    }

    public String getReaderNameFromId (String readerId) {
        SQLiteDatabase db = this.getWritableDatabase();

        String query_reader_info = "SELECT " + KEY_READER_NAME + " FROM " + TABLE_READERS + " WHERE " + KEY_READER_ID + "=" + Integer.parseInt(readerId);
        Cursor cur_reader = db.rawQuery(query_reader_info, null);
        //Log.d("Cursor ", "cur count : "+cur_reader.getCount());
        if (cur_reader != null && cur_reader.getCount() > 0) {
            if (cur_reader.moveToFirst()) {
                do {
                    String reader_name = cur_reader.getString(cur_reader.getColumnIndexOrThrow(KEY_READER_NAME));
                    Log.d("ReaderName", reader_name);
                    cur_reader.close();
                    db.close();
                    return reader_name;
                } while (cur_reader.moveToNext());
            }
        }
        cur_reader.close();
        db.close();
        return "";
    }

    // get assigned ags list from student access list
    public ArrayList<String> getAllAGsOfAccList (String studentId) {

        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> list = new ArrayList<String>();
        String query = "SELECT DISTINCT(" + KEY_ACC_LIST_AG_ID + ")," + KEY_ACC_LIST_CANTEEN_ID + "," + KEY_ACC_LIST_CANTEEN_NAME + " FROM " + TABLE_ACCESS_LIST + "  WHERE " + KEY_ACC_LIST_STUDENTS_ID + "='" + studentId + "'";
        Cursor cur = db.rawQuery(query, null);
        System.out.println("AG Count "+cur.getCount());
        int ind = 0;
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    String agId = cur.getString(cur.getColumnIndexOrThrow(KEY_ACC_LIST_AG_ID));
                    if (ind == 0) {
                        String canteenName = cur.getString(cur.getColumnIndexOrThrow(KEY_ACC_LIST_CANTEEN_NAME));
                        list.add(canteenName);
                    }
                    System.out.println("AGID : " + agId);
                    if (!agId.equals("")) {
                        String agName = getAGNameFromId(agId);
                        System.out.println("AGName:" + agName);
                        if (agName.length() > 0) {
                            list.add(agName);
                        }
                    }

                    ind++;
                } while (cur.moveToNext());
            }

        }

        return list;
    }


    // get assigned ags list from staff access list
    public ArrayList<String> getAllAGsOfStaffAccList (String staffId) {

        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> list = new ArrayList<String>();
        String query = "SELECT DISTINCT(" + KEY_STAFF_ACC_LIST_AG_ID + ")" + " FROM " + TABLE_STAFF_ACCESS_LIST + "  WHERE " + KEY_STAFF_ACC_LIST_STAFF_ID + "='" + staffId + "'";
        Cursor cur = db.rawQuery(query, null);
        System.out.println("AG Count "+cur.getCount());
        int ind = 0;
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    String agId = cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ACC_LIST_AG_ID));

                    if (agId != null) {
                        if (!agId.equals("")) {
                            String agName = getAGNameFromId(agId);
                            System.out.println("AGName:" + agName);
                            if (agName.length() > 0) {
                                list.add(agName);
                            }
                        }
                    }

                    ind++;
                } while (cur.moveToNext());
            }

        }

        return list;
    }

    public String getAGNameFromId (String agId) {
        SQLiteDatabase db = this.getWritableDatabase();

        String query_reader_info = "SELECT " + KEY_AG_NAME + " FROM " + TABLE_AG_INFO + " WHERE " + KEY_AG_ID + "=" + Integer.parseInt(agId);
        Cursor cur_reader = db.rawQuery(query_reader_info, null);
        //Log.d("Cursor ", "cur count : "+cur_reader.getCount());
        if (cur_reader != null && cur_reader.getCount() > 0) {
            if (cur_reader.moveToFirst()) {
                do {
                    String ag_name = cur_reader.getString(cur_reader.getColumnIndexOrThrow(KEY_AG_NAME));
                    Log.d("AGName", ag_name);
                    cur_reader.close();
                    db.close();
                    return ag_name;
                } while (cur_reader.moveToNext());
            }
        }
        cur_reader.close();
        db.close();
        return "";
    }

    // to get all allowed members list
    public ArrayList<StudentInfoModel.DataBean> getAllowedMembersData (int readerId, int sessionId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<StudentInfoModel.DataBean> list = new ArrayList<StudentInfoModel.DataBean>();
        String query = "SELECT *, *  FROM " + TABLE_ACCESS_LIST + " INNER JOIN " + TABLE_STU_INFO + " ON " + TABLE_ACCESS_LIST + "." + KEY_ACC_LIST_STUDENTS_ID + "=" + TABLE_STU_INFO + "." + KEY_STU_ID
                + " WHERE " + TABLE_ACCESS_LIST + "." + KEY_ACC_LIST_CANTEEN_ID + "=" + readerId + " AND " + TABLE_ACCESS_LIST + "." + KEY_ACC_LIST_SESSIONS_ID + "=" + sessionId
                +" GROUP BY " + TABLE_STU_INFO + "." + KEY_STU_ID;
        Cursor cur = db.rawQuery(query, null);
        Log.d("Cursor ", "cur count : "+cur.getCount());
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {

                    StudentInfoModel.DataBean sim = new StudentInfoModel.DataBean();
                    sim.setStudentID(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ID)));
                    sim.setFirstName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FIRST_NAME)));
                    sim.setFatherName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FATHER_NAME)));
                    sim.setGrandFatherName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GRAND_FATHER_NAME)));
                    sim.setGender(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GENDER)));
                    sim.setDateOfBirth(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DOB)));
                    sim.setSignature(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_SIG)));
                    sim.setCollege(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_COLLEGE)));
                    sim.setDepartment(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DEPT)));
                    sim.setCampus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CAMPUS)));
                    sim.setProgram(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_PROGRAM)));
                    sim.setDegreeType(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DEGREE_TYPE)));
                    sim.setAdmissionType(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ADD_TYPE)));
                    sim.setAdmissionTypeShort(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ADD_TYPE_SHORT)));
                    sim.setValidDateUntil(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_VALID_DATE_UNTIL)));
                    sim.setIssueDate(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ISSUE_DATE)));
                    sim.setMealNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_MEAL_NO)));
                    sim.setUniqueNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_UNIQE_NO)));
                    sim.setStatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_STATUS)));
                    sim.setIsactive(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ISACTIVE)));
                    sim.setCardNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_NO)));
                    sim.setCardstatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_ST)));
                    sim.setStudentImage(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_IMAGE)));


                    list.add(sim);
                } while (cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return list;
    }



    // to get all accessed members list
    public ArrayList<MemberAccessLogs> getAccessedMembersData (int readerId, int sessionId, long sessionDate, long sessionStartTime, long sessionEndTime) {
        TimeZone.setDefault(null);
        System.setProperty("user.timezone", "");
        TimeZone.setDefault(null);
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        c.setTimeInMillis(sessionDate);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Config.DATETIME_FORMAT_LOCAL);
        String sessionOnlyDate = sdf.format(c.getTimeInMillis());
        SimpleDateFormat sdfTime = new SimpleDateFormat(" HH:mm:ss", Config.DATETIME_FORMAT_LOCAL);
        //sdfTime.setTimeZone(TimeZone.getTimeZone("UTC"));
        c.setTimeInMillis(sessionStartTime);
        String sessionOnlyStartTime = sdfTime.format(c.getTimeInMillis());
        c.setTimeInMillis(sessionEndTime);
        String sessionOnlyEndTime = sdfTime.format(c.getTimeInMillis());
        String sessionDateFrom = sessionOnlyDate + sessionOnlyStartTime;//" 00:00:00";
        String sessionDateTo = sessionOnlyDate + sessionOnlyEndTime;//" 23:59:59";
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<MemberAccessLogs> list = new ArrayList<MemberAccessLogs>();
        String query = "SELECT *, * FROM " + TABLE_MEM_ACC_LOGS + " INNER JOIN " + TABLE_STU_INFO + " ON " + TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_STU_ID + "=" + TABLE_STU_INFO + "." + KEY_STU_ID + " WHERE " + TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_READER_ID + "=" + readerId + " AND " +  TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_SESSION_ID + "=" + sessionId + " AND " +  TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_ACCESS_GRANT + "=0" + " AND " +  TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_PUNCH_TIME + ">='" + sessionDateFrom + "' AND " +  TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_PUNCH_TIME + "<='" + sessionDateTo + "' GROUP BY " + TABLE_STU_INFO + "." + KEY_STU_ID;
        Cursor cur = db.rawQuery(query, null);
        Log.d("Cursor ", "cur count : "+cur.getCount());
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    MemberAccessLogs memacclogs = new MemberAccessLogs();
                    memacclogs.setMemAccLogsStuId(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ID)));
                    String firstName = cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FIRST_NAME));
                    String fatherName = cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FATHER_NAME));
                    String grandFatherName = cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GRAND_FATHER_NAME));
                    memacclogs.setMemAccLogsEmpName(firstName + " " + fatherName + " " + grandFatherName);
                    memacclogs.setMemAccLogsEmpPhoto(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_IMAGE)));
                    memacclogs.setMemAccLogsCardNo(cur.getString(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_CARD_NO)));
                    memacclogs.setMemAccLogsReaderId(cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_READER_ID)));
                    memacclogs.setMemAccLogsPunchDate(cur.getString(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_PUNCH_TIME)));
                    memacclogs.setMemAccLogsSessionId(cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_SESSION_ID)));
                    memacclogs.setMemAccLogsAccessGrant(cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_ACCESS_GRANT)));
                    memacclogs.setMemAccLogsIsSync(cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_IS_SYNC)));
                    list.add(memacclogs);
                } while (cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return list;
    }

    // to get all denied members list
    public ArrayList<MemberAccessLogs> getDeniedMembersData (int readerId, int sessionId, long sessionDate, long sessionStartTime, long sessionEndTime) {
        TimeZone.setDefault(null);
        System.setProperty("user.timezone", "");
        TimeZone.setDefault(null);
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        c.setTimeInMillis(sessionDate);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Config.DATETIME_FORMAT_LOCAL);
        String sessionOnlyDate = sdf.format(c.getTimeInMillis());
        SimpleDateFormat sdfTime = new SimpleDateFormat(" HH:mm:ss", Config.DATETIME_FORMAT_LOCAL);
        //sdfTime.setTimeZone(TimeZone.getTimeZone("UTC"));
        c.setTimeInMillis(sessionStartTime);
        String sessionOnlyStartTime = sdfTime.format(c.getTimeInMillis());
        c.setTimeInMillis(sessionEndTime);
        String sessionOnlyEndTime = sdfTime.format(c.getTimeInMillis());
        String sessionDateFrom = sessionOnlyDate + sessionOnlyStartTime;//" 00:00:00";
        String sessionDateTo = sessionOnlyDate + sessionOnlyEndTime;//" 23:59:59";

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<MemberAccessLogs> list = new ArrayList<MemberAccessLogs>();
        String query = "SELECT *, * FROM " + TABLE_MEM_ACC_LOGS + " INNER JOIN " + TABLE_STU_INFO + " ON " + TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_STU_ID + "=" + TABLE_STU_INFO + "." + KEY_STU_ID + " WHERE " + TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_READER_ID + "=" + readerId + " AND " +  TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_SESSION_ID + "=" + sessionId + " AND " +  TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_ACCESS_GRANT + ">=1" + " AND " +  TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_PUNCH_TIME + ">='" + sessionDateFrom + "' AND " +  TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_PUNCH_TIME + "<='" + sessionDateTo + "' ORDER BY " + TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_PUNCH_TIME + " DESC";//"' GROUP BY " + TABLE_STU_INFO + "." + KEY_STU_ID;
        //String query = "SELECT *, "+ TABLE_EMPLOYEES + "." + KEY_EMP_NAME + " AS emp_name, " + TABLE_EMPLOYEES + "." + KEY_EMP_PHOTO + " AS emp_photo" +" FROM " + TABLE_MEM_ACC_LOGS + " LEFT JOIN " + TABLE_EMPLOYEES + " ON " + TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_STU_ID + "=" + TABLE_EMPLOYEES + "." + KEY_EMP_ID + " WHERE " + TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_READER_ID + "=" + readerId + " AND " +  TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_SESSION_ID + "=" + sessionId + " AND " +  TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_ACCESS_GRANT + ">=1" + " AND " +  TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_PUNCH_TIME + ">='" + sessionDateFrom + "' AND " + TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_PUNCH_TIME + "<='" + sessionDateTo + "' ORDER BY " + TABLE_MEM_ACC_LOGS + "." + KEY_MEM_ACC_LOGS_PUNCH_TIME + " DESC";// + TABLE_EMPLOYEES + "." + KEY_EMP_ID;
        Log.d("Database", "Query denied "+query);
        Cursor cur = db.rawQuery(query, null);
        Log.d("Cursor ", "cur count : "+cur.getCount());
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    MemberAccessLogs memacclogs = new MemberAccessLogs();
                    memacclogs.setMemAccLogsStuId(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ID)));
                    String firstName = cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FIRST_NAME));
                    String fatherName = cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FATHER_NAME));
                    String grandFatherName = cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GRAND_FATHER_NAME));
                    memacclogs.setMemAccLogsEmpName(firstName + " " + fatherName + " " + grandFatherName);
                    memacclogs.setMemAccLogsEmpPhoto(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_IMAGE)));
                    memacclogs.setMemAccLogsCardNo(cur.getString(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_CARD_NO)));
                    memacclogs.setMemAccLogsReaderId(cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_READER_ID)));
                    memacclogs.setMemAccLogsPunchDate(cur.getString(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_PUNCH_TIME)));
                    memacclogs.setMemAccLogsSessionId(cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_SESSION_ID)));
                    memacclogs.setMemAccLogsAccessGrant(cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_ACCESS_GRANT)));
                    memacclogs.setMemAccLogsIsSync(cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_IS_SYNC)));
                    list.add(memacclogs);
                } while (cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return list;
    }

    // to get all pending members list
    public ArrayList<StudentInfoModel.DataBean> getPendingMembersData (int readerId, int sessionId, long sessionDate, long sessionStartTime, long sessionEndTime) {
        TimeZone.setDefault(null);
        System.setProperty("user.timezone", "");
        TimeZone.setDefault(null);
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        c.setTimeInMillis(sessionDate);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Config.DATETIME_FORMAT_LOCAL);
        String sessionOnlyDate = sdf.format(c.getTimeInMillis());
        SimpleDateFormat sdfTime = new SimpleDateFormat(" HH:mm:ss", Config.DATETIME_FORMAT_LOCAL);
        c.setTimeInMillis(sessionStartTime);
        String sessionOnlyStartTime = sdfTime.format(c.getTimeInMillis());
        c.setTimeInMillis(sessionEndTime);
        String sessionOnlyEndTime = sdfTime.format(c.getTimeInMillis());
        String sessionDateFrom = sessionOnlyDate + sessionOnlyStartTime;//" 00:00:00";
        String sessionDateTo = sessionOnlyDate + sessionOnlyEndTime;//" 23:59:59";
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<StudentInfoModel.DataBean> list = new ArrayList<StudentInfoModel.DataBean>();
        /*String query = "SELECT *, "+ TABLE_EMPLOYEES + "." + KEY_EMP_NAME + " AS emp_name, " + TABLE_EMPL + "." + KEY_EMP_PHOTO + " AS emp_photo" +" FROM " + TABLE_MEM_ACC + " LEFT JOIN " + TABLE_EMPLOYEES
                + " ON " + TABLE_MEM_ACC + "." + KEY_MEM_ACC_EMP_ID + "=" + TABLE_EMPLOYEES + "." + KEY_EMP_ID + " WHERE " + TABLE_MEM_ACC + "." + KEY_MEM_ACC_READER_ID + "=" + readerId + " GROUP BY " + TABLE_EMPLOYEES + "." + KEY_EMP_ID;*/
        String query = "SELECT *, *  FROM " + TABLE_ACCESS_LIST + " INNER JOIN " + TABLE_STU_INFO + " ON " + TABLE_ACCESS_LIST + "." + KEY_ACC_LIST_STUDENTS_ID + "=" + TABLE_STU_INFO + "." + KEY_STU_ID
                + " WHERE " + TABLE_ACCESS_LIST + "." + KEY_ACC_LIST_CANTEEN_ID + "=" + readerId + " AND " + TABLE_ACCESS_LIST + "." + KEY_ACC_LIST_SESSIONS_ID + "=" + sessionId
                +" GROUP BY " + TABLE_STU_INFO + "." + KEY_STU_ID;
        Cursor cur = db.rawQuery(query, null);
        Log.d("Cursor ", "cur count : "+cur.getCount());
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    String stu_id = cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ID));
                    String query1 = "SELECT * FROM " + TABLE_MEM_ACC_LOGS + " WHERE " + KEY_MEM_ACC_LOGS_READER_ID + "="
                            + readerId + " AND " + KEY_MEM_ACC_LOGS_SESSION_ID + "=" + sessionId + " AND "
                            + KEY_MEM_ACC_LOGS_STU_ID + "='" + stu_id + "' AND " + KEY_MEM_ACC_LOGS_PUNCH_TIME + ">='" + sessionDateFrom + "' AND " + KEY_MEM_ACC_LOGS_PUNCH_TIME + "<='" + sessionDateTo + "'";

                    Cursor cur1 = db.rawQuery(query1, null);
                    if (cur1 != null) {
                        if (cur1.getCount() == 0) {
                            /*MemberAccessLogs memacclogs = new MemberAccessLogs();
                            memacclogs.setMemAccLogsStuId(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ID)));
                            String firstName = cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FIRST_NAME));
                            String fatherName = cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FATHER_NAME));
                            String grandFatherName = cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GRAND_FATHER_NAME));
                            String stu_full_name = firstName + " " + fatherName + " " + grandFatherName;
                            memacclogs.setMemAccLogsEmpName(stu_full_name);
                            memacclogs.setMemAccLogsEmpPhoto(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_IMAGE)));
                            memacclogs.setMemAccLogsCardNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_NO)));
                            memacclogs.setMemAccLogsReaderId(cur.getInt(cur.getColumnIndexOrThrow(KEY_ACC_LIST_READERS_ID)));
                            memacclogs.setMemAccLogsSessionId(cur.getInt(cur.getColumnIndexOrThrow(KEY_ACC_LIST_SESSIONS_ID)));
                            System.out.println("Card No.: "+ cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_NO)));

                            list.add(memacclogs);*/

                            StudentInfoModel.DataBean sim = new StudentInfoModel.DataBean();
                            sim.setStudentID(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ID)));
                            sim.setFirstName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FIRST_NAME)));
                            sim.setFatherName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_FATHER_NAME)));
                            sim.setGrandFatherName(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GRAND_FATHER_NAME)));
                            sim.setGender(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_GENDER)));
                            sim.setDateOfBirth(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DOB)));
                            sim.setSignature(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_SIG)));
                            sim.setCollege(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_COLLEGE)));
                            sim.setDepartment(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DEPT)));
                            sim.setCampus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CAMPUS)));
                            sim.setProgram(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_PROGRAM)));
                            sim.setDegreeType(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_DEGREE_TYPE)));
                            sim.setAdmissionType(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ADD_TYPE)));
                            sim.setAdmissionTypeShort(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ADD_TYPE_SHORT)));
                            sim.setValidDateUntil(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_VALID_DATE_UNTIL)));
                            sim.setIssueDate(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ISSUE_DATE)));
                            sim.setMealNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_MEAL_NO)));
                            sim.setUniqueNo(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_UNIQE_NO)));
                            sim.setStatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_STATUS)));
                            sim.setIsactive(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ISACTIVE)));
                            sim.setCardNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_NO)));
                            sim.setCardstatus(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_CARD_ST)));
                            sim.setStudentImage(cur.getString(cur.getColumnIndexOrThrow(KEY_STU_IMAGE)));


                            list.add(sim);
                        }
                    }
                    cur1.close();

                } while (cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return list;
    }

    public int checkCardHavingDoorAccess (int readerId, int sessionId, String cardNumber, String sessionDateFrom, String sessionDateTo) {
        int access = -1;
        SQLiteDatabase db = this.getWritableDatabase();

        if (checkMemberHaveAccessToDoor(db, readerId, sessionId, cardNumber)) {
            access = 0;
            // first check entries into Member access punch logs, if it is there then no need to check with table and send duplicate entries

            // first check whether the member acess logs have any enteries or not
            if (checkMemberAccessHavingData()) {
                //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Config.DATETIME_FORMAT_LOCAL);
                //String sessionONlyDate = sdf.format(sessionDate);
                //String sessionDateFrom = sessionONlyDate + " 00:00:00";
                //String sessionDateTo = sessionONlyDate + " 23:59:59";

                String query = "SELECT * FROM " + TABLE_MEM_ACC_LOGS + " WHERE " + KEY_MEM_ACC_LOGS_READER_ID + "=" + readerId + " AND "
                        + KEY_MEM_ACC_LOGS_SESSION_ID + "=" + sessionId + " AND " + KEY_MEM_ACC_LOGS_CARD_NO + "='" + cardNumber + "' AND "
                        + KEY_MEM_ACC_LOGS_ACCESS_GRANT + "=" + 0 + " AND " + KEY_MEM_ACC_LOGS_PUNCH_TIME + ">='" + sessionDateFrom + "' AND "
                        + KEY_MEM_ACC_LOGS_PUNCH_TIME + "<='" + sessionDateTo + "'"; // Access Grant = 0 means have already accessed
                Cursor cur = db.rawQuery(query, null);
                if (cur != null) {
                    Log.d("Have accessed session? ", "" + cur.getCount());
                    if (cur.getCount() > 0) {
                        //access = cur.getCount();
                        access = 2; // already accessed so move them to deny - Duplicate Entries
                    } else {
                        access = 0; // NOt yet accessed so let them access
                    }
                }
            }
        }
        return access;
    }

    public boolean checkMemberHaveAccessToDoor (SQLiteDatabase db, int readerId, int sessionId, String cardNumber) {
        String studentId = "";

        Cursor cur = db.rawQuery("SELECT " + KEY_STU_ID + " FROM " + TABLE_STU_INFO + " WHERE " + KEY_STU_CARD_NO + "='" + cardNumber + "' COLLATE NOCASE", null);
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    studentId = cur.getString(cur.getColumnIndexOrThrow(KEY_STU_ID));
                } while (cur.moveToNext());
            }
        }
        cur.close();

        if (studentId.equals("")) {
            return false;
        }

        String query = "SELECT * FROM " + TABLE_ACCESS_LIST + " WHERE " + KEY_ACC_LIST_CANTEEN_ID + "=" + readerId + " AND " + KEY_ACC_LIST_SESSIONS_ID + "=" + sessionId + " AND " + KEY_ACC_LIST_STUDENTS_ID+"='" + studentId + "'";
        Log.d("CheckMemAcc ", "Query "+query);
        Cursor cur1 = db.rawQuery(query, null);
        if (cur != null) {
            Log.d("Have Door Access", "" + cur1.getCount());
            if (cur1.getCount() > 0) {
                return true;
            }
        }
        cur1.close();
        return false;
    }
    public boolean checkMemberAccessHavingData() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_MEM_ACC_LOGS;
        Cursor cur = db.rawQuery(query, null);
        if (cur != null) {
            if (cur.getCount() > 0) {
                return true;
            }
            return false;
        }
        return false;
    }

    // save logs of all members
    public void saveMemAccPunchLogsData (MemberAccessLogs memberAccessLogs) {
        SQLiteDatabase db = this.getWritableDatabase();
        // content values for categories
        ContentValues mem_logs_data = new ContentValues();
        mem_logs_data.put(KEY_MEM_ACC_LOGS_STU_ID, memberAccessLogs.getMemAccLogsStuId());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_CARD_NO, memberAccessLogs.getMemAccLogsCardNo());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_READER_ID, memberAccessLogs.getMemAccLogsReaderId());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_SESSION_ID, memberAccessLogs.getMemAccLogsSessionId());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_READER_NAME, memberAccessLogs.getMemAccLogsReaderName());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_SESSION_NAME, memberAccessLogs.getMemAccLogsSessionName());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_PUNCH_TIME, memberAccessLogs.getMemAccLogsPunchDate());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_IS_SYNC, memberAccessLogs.getMemAccLogsIsSync());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_ACCESS_GRANT, memberAccessLogs.getMemAccLogsAccessGrant());
        //if (!isReaderAlreadyExists(db, reader.getSLN_Reader())) {
        db.insert(TABLE_MEM_ACC_LOGS, null, mem_logs_data);
        /*} else {
            db.update(TABLE_READERS, values_cat, KEY_READER_ID + "=" + reader.getSLN_Reader(), null);
        }*/
        db.close();
    }

    // save logs of all members
    public void updateMemAccPunchLogsData (MemberAccessLogs memberAccessLogs) {
        SQLiteDatabase db = this.getWritableDatabase();
        // content values for categories
        ContentValues mem_logs_data = new ContentValues();
        mem_logs_data.put(KEY_MEM_ACC_LOGS_STU_ID, memberAccessLogs.getMemAccLogsEmpId());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_CARD_NO, memberAccessLogs.getMemAccLogsCardNo());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_READER_ID, memberAccessLogs.getMemAccLogsReaderId());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_SESSION_ID, memberAccessLogs.getMemAccLogsSessionId());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_PUNCH_TIME, memberAccessLogs.getMemAccLogsPunchDate());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_IS_SYNC, memberAccessLogs.getMemAccLogsIsSync());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_ACCESS_GRANT, memberAccessLogs.getMemAccLogsAccessGrant());
        //if (!isReaderAlreadyExists(db, reader.getSLN_Reader())) {
        db.update(TABLE_MEM_ACC_LOGS, mem_logs_data, KEY_MEM_ACC_LOGS_IS_SYNC + "=" + 0 + " AND " + KEY_MEM_ACC_LOGS_CARD_NO + "='" + memberAccessLogs.getMemAccLogsCardNo() + "' COLLATE NOCASE AND " + KEY_MEM_ACC_LOGS_STU_ID + "=" + memberAccessLogs.getMemAccLogsEmpId() + " AND " + KEY_MEM_ACC_LOGS_PUNCH_TIME + "='" + memberAccessLogs.getMemAccLogsPunchDate() + "'", null);
        /*} else {
            db.update(TABLE_READERS, values_cat, KEY_READER_ID + "=" + reader.getSLN_Reader(), null);
        }*/
        db.close();
    }

    // delete the memaccesslogs which is synced
    public void deleteMemAccPunchLogsData (MemberAccessLogs memberAccessLogs) {
        SQLiteDatabase db = this.getWritableDatabase();
        // content values for categories
        ContentValues mem_logs_data = new ContentValues();
        mem_logs_data.put(KEY_MEM_ACC_LOGS_STU_ID, memberAccessLogs.getMemAccLogsEmpId());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_CARD_NO, memberAccessLogs.getMemAccLogsCardNo());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_READER_ID, memberAccessLogs.getMemAccLogsReaderId());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_SESSION_ID, memberAccessLogs.getMemAccLogsSessionId());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_PUNCH_TIME, memberAccessLogs.getMemAccLogsPunchDate());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_IS_SYNC, memberAccessLogs.getMemAccLogsIsSync());
        mem_logs_data.put(KEY_MEM_ACC_LOGS_ACCESS_GRANT, memberAccessLogs.getMemAccLogsAccessGrant());
        //if (!isReaderAlreadyExists(db, reader.getSLN_Reader())) {
        db.delete(TABLE_MEM_ACC_LOGS, KEY_MEM_ACC_LOGS_IS_SYNC + "=" + 1, null);
        /*} else {
            db.update(TABLE_READERS, values_cat, KEY_READER_ID + "=" + reader.getSLN_Reader(), null);
        }*/
        db.close();
    }

    // update Mem access logs data

    //---------------------------------------------------- Session --------------------------------------------------------------
    // to add session
    public boolean checkSessionDuplicate(String session_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_SESSION  + " WHERE " + KEY_SESSION_NAME + "='" + session_name + "'";
        Cursor cur = db.rawQuery(query, null);
        Log.d("Cursor ", "cur count : "+cur.getCount());
        if (cur != null && cur.getCount() > 0) {
            return true;
        }
        return false;
    }
    // to delete session
    public void saveSession(Sessions session, int sessionId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KEY_SESSION_ID, session.getSessionId());
        cv.put(KEY_SESSION_NAME, session.getSessionName());
        cv.put(KEY_SESSION_DATE, session.getSessionDate());
        cv.put(KEY_SESSION_FROM_DATETIME, session.getSessionFromDate());
        cv.put(KEY_SESSION_TO_DATETIME, session.getSessionToDate());

        if (sessionId != -1) {
            db.update(TABLE_SESSION, cv, KEY_SESSION_ID + "=" + sessionId, null);
        } else {
            if (checkSessionDuplicate(session.getSessionName())) {
                db.update(TABLE_SESSION, cv, KEY_SESSION_NAME + "='" + session.getSessionName() + "'", null);
            } else {
                db.insert(TABLE_SESSION, null, cv);
            }
        }
        db.close();
    }
    // to fetch all session
    public ArrayList<SessionsInfoModel.DataBean> getAllSessions() {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<SessionsInfoModel.DataBean> list = new ArrayList<SessionsInfoModel.DataBean>();
        String query = "SELECT * FROM " + TABLE_SESS;
        Cursor cur = db.rawQuery(query, null);
        Log.d("Cursor ", "cur count : "+cur.getCount());
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToNext()) {
                do {

                    SessionsInfoModel.DataBean session = new SessionsInfoModel.DataBean();
                    String sessionId = cur.getString(cur.getColumnIndexOrThrow(KEY_SESS_ID));
                    String sessionName = cur.getString(cur.getColumnIndexOrThrow(KEY_SESS_NAME));
                    String sessionDesc = cur.getString(cur.getColumnIndexOrThrow(KEY_SESS_DES));
                    String sessionStartTime = cur.getString(cur.getColumnIndexOrThrow(KEY_SESS_START_TIME));
                    String sessionEndTime = cur.getString(cur.getColumnIndexOrThrow(KEY_SESS_END_TIME));
                    session.setSessionId(sessionId);
                    session.setSessionName(sessionName);
                    session.setSessionDesc(sessionDesc);
                    session.setSessionStartTime(sessionStartTime);
                    session.setSessionEndTime(sessionEndTime);
                    list.add(session);
                } while(cur.moveToNext());
            }
        }
        cur.close();
        db.close();

        return list;
    }

    public Sessions getSessionFromId(int id) {
        Sessions session = new Sessions();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_SESSION + " WHERE " + KEY_SESSION_ID + "=" + id;
        Cursor cur = db.rawQuery(query, null);
        if (cur != null) {
            if (cur.getCount() > 0) {
                if (cur.moveToFirst()) {
                    do {
                        session.setSessionId(cur.getInt(cur.getColumnIndexOrThrow(KEY_SESSION_ID)));
                        session.setSessionName(cur.getString(cur.getColumnIndexOrThrow(KEY_SESSION_NAME)));
                        session.setSessionDate(cur.getLong(cur.getColumnIndexOrThrow(KEY_SESSION_DATE)));
                        session.setSessionFromDate(cur.getLong(cur.getColumnIndexOrThrow(KEY_SESSION_FROM_DATETIME)));
                        session.setSessionToDate(cur.getLong(cur.getColumnIndexOrThrow(KEY_SESSION_TO_DATETIME)));
                    } while(cur.moveToNext());
                }
            }
        }
        return session;
    }

    public void deleteSessionFromSessionId(int sessionId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SESSION, KEY_SESSION_ID + "=" + sessionId, null);
        db.close();
    }

    // get nearest Session based on the current time
    public int getSessionIdAsPerCurrentTIme () {
        int sessionId = -1;
        Calendar calCurrentTime = Calendar.getInstance();
        long current_time_mili = calCurrentTime.getTimeInMillis();
        System.out.println("SessionId: Current Time: "+new SimpleDateFormat("HH:mm").format(current_time_mili));
        String current_time = new SimpleDateFormat("HH:mm").format(current_time_mili);
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + KEY_SESS_ID + " FROM " +TABLE_SESS + " WHERE " + KEY_SESS_START_TIME + "<='" + current_time + "' AND " + KEY_SESS_END_TIME + ">='" + current_time + "' LIMIT 1";
        //String query = "SELECT * FROM " +TABLE_SESS;// + " WHERE " + KEY_SESS_START_TIME + "<=" + current_time_mili + " AND " + KEY_SESS_END_TIME + ">=" + current_time_mili + " LIMIT 1";
        Cursor cur = db.rawQuery(query, null);
        if (cur != null) {
            if (cur.getCount() > 0) {
                if (cur.moveToFirst()) {
                    do {
                        sessionId = cur.getInt(cur.getColumnIndex(KEY_SESS_ID));
                        //String startTime = cur.getString(cur.getColumnIndex(KEY_SESS_START_TIME));
                        //String endTime = cur.getString(cur.getColumnIndex(KEY_SESS_END_TIME));


                    } while (cur.moveToNext());
                }
            }
        }
        return sessionId;
    }

    //check for the member access logs entries
    public boolean checkMemAccLogs() {
        boolean st = false;
        SQLiteDatabase db = getWritableDatabase();
        String query = "Select * FROM " + TABLE_MEM_ACC_LOGS + " WHERE " + KEY_MEM_ACC_LOGS_IS_SYNC +"=" + 0;
        Cursor cur = db.rawQuery(query, null);
        if (cur != null && cur.getCount() > 0) {
            st = true;
        }
        cur.close();
        db.close();
        return st;
    }

    // check for entries which yet not synced
    public ArrayList<MemberAccessLogs> isOldNotSyncedMemAccLogsExist () {
        ArrayList<MemberAccessLogs> list = new ArrayList<MemberAccessLogs>();
        /*Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Config.DATETIME_FORMAT_LOCAL);
        String currentDateTime = sdf.format(cal.getTime()) + " 00:00:00";*/

        SQLiteDatabase db = this.getWritableDatabase();
        // + KEY_MEM_ACC_LOGS_PUNCH_TIME + "<'" + currentDateTime+
        String query = "SELECT * FROM " + TABLE_MEM_ACC_LOGS + " WHERE " + KEY_MEM_ACC_LOGS_IS_SYNC +"=" + 0 + " ORDER BY " + KEY_MEM_ACC_LOGS_PUNCH_TIME + " LIMIT 10";
        Log.d("isNotSynced", " Query: "+query);
        Cursor cur = db.rawQuery(query, null);
        Log.d("isNotSynced", ""+cur.getCount());
        if (cur != null) {
            int count = cur.getCount();
            if (count > 0) {
                if (cur.moveToFirst()) {
                    do{

                        int memacc_empId = cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_STU_ID));
                        String memacc_cardno = cur.getString(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_CARD_NO));
                        int memacc_readerId = cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_READER_ID));
                        String memacc_readerName = cur.getString(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_READER_NAME));
                        int memacc_sessionId = cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_SESSION_ID));
                        String memacc_sessionName = cur.getString(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_SESSION_NAME));
                        int memacc_issync = cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_IS_SYNC));
                        int memacc_accgrant = cur.getInt(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_ACCESS_GRANT));
                        String memacc_punchtime = cur.getString(cur.getColumnIndexOrThrow(KEY_MEM_ACC_LOGS_PUNCH_TIME));

                        MemberAccessLogs memacclogs = new MemberAccessLogs();
                        memacclogs.setMemAccLogsEmpId(memacc_empId);
                        memacclogs.setMemAccLogsCardNo(memacc_cardno);
                        memacclogs.setMemAccLogsReaderId(memacc_readerId);
                        memacclogs.setMemAccLogsReaderName(memacc_readerName);
                        memacclogs.setMemAccLogsSessionId(memacc_sessionId);
                        memacclogs.setMemAccLogsSessionName(memacc_sessionName);
                        memacclogs.setMemAccLogsIsSync(memacc_issync);
                        memacclogs.setMemAccLogsAccessGrant(memacc_accgrant);
                        memacclogs.setMemAccLogsPunchDate(memacc_punchtime);

                        list.add(memacclogs);

                    } while(cur.moveToNext());
                }
            }
        }
        cur.close();
        db.close();
        return list;
    }

    // check for yesterday and day before yesterdays entries
    public int isOldSyncedMemAccLogsExist () {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Config.DATETIME_FORMAT_LOCAL);
        String currentDateTime = sdf.format(cal.getTime()) + " 00:00:00";;

        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_MEM_ACC_LOGS + " WHERE " + KEY_MEM_ACC_LOGS_PUNCH_TIME + "<'" + currentDateTime+"' AND " + KEY_MEM_ACC_LOGS_IS_SYNC +"=" + 1;
        Log.d("isOldMemAccLogs", " Query: "+query);
        Cursor cur = db.rawQuery(query, null);
        Log.d("isOldMemAccLogs", ""+cur.getCount());
        if (cur != null) {
            int count = cur.getCount();
            if (count > 0) {
                return count;
            }
        }
        cur.close();
        db.close();
        return 0;
    }

    // delete the old enteries which is synced
    public int deleteOldMemAccLogs () {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Config.DATETIME_FORMAT_LOCAL);
        String currentDateTime = sdf.format(cal.getTime()) + " 00:00:00";

        SQLiteDatabase db = this.getWritableDatabase();
        int delete_st = db.delete(TABLE_MEM_ACC_LOGS, KEY_MEM_ACC_LOGS_PUNCH_TIME + "<'" + currentDateTime + "' AND " + KEY_MEM_ACC_LOGS_IS_SYNC + "=" + 1, null);
        Log.d("isOldMemAccLogs", ""+delete_st);
        db.close();
        return delete_st;
    }


    // to save the logs for the deleted records
    public void saveLogsForDeletedMemAccLogs(Context mContext, String logsStr) {
        StringBuilder str = new StringBuilder();
        ContextWrapper cw = new ContextWrapper(mContext);
        // path to /data/data/yourapp/app_data/imageDir
        File logsDirectory = cw.getDir(logsDir, Context.MODE_PRIVATE);
        // Create imageDir
        File logsFilePath=new File(logsDirectory,logsFileNameMemAcc+ ".logs");
        /*File backupPath = Environment.getExternalStorageDirectory();

        backupPath = new File(backupPath.getPath() + "/Android/data/com.maximusdev.bankrecord/files");*/

        if(!logsDirectory.exists()){
            logsDirectory.mkdirs();
        }

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:sss", Config.DATETIME_FORMAT_LOCAL);
        String logsDate = sdf.format(cal.getTime());
        String oldLogs = readLogsFile(mContext);
        /*if (oldLogs.length() > 0) {
            str.append(oldLogs);
        }*/

        str.append(oldLogs + logsStr + mContext.getResources().getString(R.string.on_label) + logsDate);

        FileOutputStream fos;
        try {
            fos = new FileOutputStream(logsFilePath);

            fos.write(str.toString().getBytes());
            fos.write("\n".getBytes());

            fos.close();

            Log.d("DBHandler", "Logs Taken");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readLogsFile(Context mContext) {
        ContextWrapper cw = new ContextWrapper(mContext);
        // path to /data/data/yourapp/app_data/logsDir
        File directory = cw.getDir(logsDir, Context.MODE_PRIVATE);

        if(!directory.exists()){
            directory.mkdirs();
        }

        // Create imageDir
        File mypath = new File(directory, "" + logsFileNameMemAcc + ".logs");

        String logsFilePath = mypath.getAbsolutePath();
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(logsFilePath));
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return text.toString();
    }

    // save session for students
    public void saveSessions() {

    }

    public ArrayList<DoorAccessInfoModel> getAllReadersOfStudent (String studentId) {

        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<DoorAccessInfoModel> list = new ArrayList<DoorAccessInfoModel>();
        String query = "SELECT DISTINCT(" + KEY_ACC_LIST_READERS_ID + ") FROM " + TABLE_ACCESS_LIST + "  WHERE " + KEY_ACC_LIST_STUDENTS_ID + "='" + studentId + "'";
        Cursor cur = db.rawQuery(query, null);
        System.out.println("Reader Count "+cur.getCount());
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    DoorAccessInfoModel doorModel = new DoorAccessInfoModel();
                    String readerId = cur.getString(cur.getColumnIndexOrThrow(KEY_ACC_LIST_READERS_ID));
                    System.out.println("ReaderId : "+readerId);
                    String readerName = getReaderNameFromId(readerId);
                    System.out.println("Readername:"+readerName);
                    if (readerName.length() > 0) {
                        doorModel.setDoorId(Integer.parseInt(readerId));
                        doorModel.setDoorName(readerName);
                        list.add(doorModel);
                    }
                } while (cur.moveToNext());
            }

        }

        return list;
    }

    public ArrayList<DoorAccessInfoModel> getAllAGsOfStudent (String studentId) {

        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<DoorAccessInfoModel> list = new ArrayList<DoorAccessInfoModel>();
        String query = "SELECT DISTINCT(" + KEY_ACC_LIST_AG_ID + ")," +KEY_ACC_LIST_CANTEEN_ID + "," + KEY_ACC_LIST_CANTEEN_NAME + " FROM " + TABLE_ACCESS_LIST + "  WHERE " + KEY_ACC_LIST_STUDENTS_ID + "='" + studentId + "'";
        Cursor cur = db.rawQuery(query, null);
        System.out.println("Reader Count "+cur.getCount());
        int ind = 0;
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    DoorAccessInfoModel doorModel = new DoorAccessInfoModel();
                    String agId = cur.getString(cur.getColumnIndexOrThrow(KEY_ACC_LIST_AG_ID));
                    System.out.println("AGId : "+agId);

                    if (ind == 0) {
                        String canteenId = cur.getString(cur.getColumnIndexOrThrow(KEY_ACC_LIST_CANTEEN_ID));
                        String canteenName = cur.getString(cur.getColumnIndexOrThrow(KEY_ACC_LIST_CANTEEN_NAME));
                        DoorAccessInfoModel doorModel1 = new DoorAccessInfoModel();
                        doorModel1.setDoorId(Integer.parseInt(canteenId));
                        doorModel1.setDoorName(canteenName);
                        list.add(doorModel1);
                    }

                    if (!agId.equals("")) {
                        String agName = getReaderNameFromId(agId);
                        System.out.println("AGname:" + agName);
                        if (agName.length() > 0) {
                            doorModel.setDoorId(Integer.parseInt(agId));
                            doorModel.setDoorName(agName);
                            list.add(doorModel);
                        }
                    }
                    ind++;
                } while (cur.moveToNext());
            }

        }

        return list;
    }


    public ArrayList<DoorAccessInfoModel> getAllAGsOfStaff (String staffId) {

        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<DoorAccessInfoModel> list = new ArrayList<DoorAccessInfoModel>();
        String query = "SELECT DISTINCT(" + KEY_STAFF_ACC_LIST_AG_ID + ") FROM " + TABLE_STAFF_ACCESS_LIST + "  WHERE " + KEY_STAFF_ACC_LIST_STAFF_ID + "='" + staffId + "'";
        Cursor cur = db.rawQuery(query, null);
        System.out.println("Reader Count "+cur.getCount());
        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToFirst()) {
                do {
                    DoorAccessInfoModel doorModel = new DoorAccessInfoModel();
                    String agId = cur.getString(cur.getColumnIndexOrThrow(KEY_STAFF_ACC_LIST_AG_ID));
                    System.out.println("AGId : "+agId);
                    if (agId != null) {
                        if (!agId.equals("")) {
                            String readerName = getAGNameFromId(agId);
                            System.out.println("Readername:" + readerName);
                            if (readerName.length() > 0) {
                                doorModel.setDoorId(Integer.parseInt(agId));
                                doorModel.setDoorName(readerName);
                                list.add(doorModel);
                            }
                        }
                    }
                } while (cur.moveToNext());
            }

        }

        return list;
    }

    // Batch Insert for readers
    public void saveSessions (ArrayList<SessionsInfoModel.DataBean> data) {
        SQLiteDatabase db = this.getWritableDatabase();
        //deleteAllSessions();
        // content values for categories
        ContentValues values_cat = new ContentValues();
        db.beginTransaction();
        for (int i = 0; i < data.size(); i++) {
            SessionsInfoModel.DataBean session = data.get(i);
            String session_id = session.getSessionId();
            Log.d(TAG, "SessionId: " + session_id);
            String session_name = session.getSessionName();
            Log.d(TAG, "SessionName: " + session_name);
            String session_desc = session.getSessionDesc();
            String session_starttime = AppUtilsKt.convert12To24FormatTime(session.getSessionStartTime());
            String session_endtime = AppUtilsKt.convert12To24FormatTime(session.getSessionEndTime());

            values_cat.put(KEY_SESS_ID, session_id);
            values_cat.put(KEY_SESS_NAME, session_name);
            values_cat.put(KEY_SESS_DES, session_desc);
            values_cat.put(KEY_SESS_START_TIME, session_starttime);
            values_cat.put(KEY_SESS_END_TIME, session_endtime);
            if (!isSessionAlreadyExists(db, session_id)) {
                db.insert(TABLE_SESS, null, values_cat);
            } else {
                db.update(TABLE_SESS, values_cat, KEY_SESS_ID + "='" + session_id + "'", null);
            }

        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public boolean isSessionAlreadyExists(SQLiteDatabase db, String session_id) {
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_SESS + " WHERE " + KEY_SESS_ID + "='" + session_id + "'", null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public boolean checkSession() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_SESS, null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public void deleteAllSessions() {
        SQLiteDatabase db = this.getWritableDatabase();
        if (checkSession()) {
            db.delete(TABLE_SESS, null, null);
        }
        //db.close();
    }

    // ------------------------------ Visitor ----------------------------------
    String vis_path = "";
    public void saveVisitorInfo(Context mContext, ArrayList<VisitorInfoModel.DataBean> data) {

        //deleteAllStudents();
        // content values for categories
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values_cat = new ContentValues();
        db.beginTransaction();

        String vis_image = "";
        for (int i = 0; i < data.size(); i++) {
            VisitorInfoModel.DataBean visInfo = data.get(i);

            String visRegNo = visInfo.getVisitorRegNo();
            String visImage = visInfo.getVisitorPhoto();

            String baseUrl = SharedPref.INSTANCE.getStringValue(mContext, KeysKt.keyBaseUrl, "");
            String visImage1 = ""/*baseUrl + stud_image*/;

            if(visImage!=null && !visImage.isEmpty())
            {
                char visFirst = visImage.charAt(0);
                if (visFirst == '~') {
                    visImage1 = visImage.substring(1, visImage.length());
                } else {
                    visImage1 = visImage;
                }
                visImage1 = baseUrl + visImage1 ;

                //AppUtilsKt.ImageToLocal(mContext,visImage1,null,"StudentImage");

            }
            else
                visImage1 = "";

            System.out.println("Student Image:"+visImage1);

            Log.d(TAG, "StudentId: " + visInfo.getVisitorRegNo());
            String vis_firstname = visInfo.getVisitorFirstName();
            Log.d(TAG, "StudentName: " + vis_firstname);
            String vis_lastname = visInfo.getVisitorLasttName();
            String vis_companyname = visInfo.getCompanyName();
            String vis_type = visInfo.getVisitorType();
            String vis_reason = visInfo.getVisitReason();
            String vis_phoneno = visInfo.getVisitorPhoneNo();
            String vis_emailid = visInfo.getVisitorEmailId();
            String vis_photo = visInfo.getVisitorPhoto();
            String vis_acc_level = visInfo.getVisitorAccessLevel();
            String vis_check_in_time = visInfo.getVisitorCheckInTime();
            String vis_check_out_time = visInfo.getVisitorCheckOutTime();
            String vis_card_no = visInfo.getVisitorCardNumber();
            String vis_card_st = visInfo.getVisitorCardStatus();
            String vis_st = visInfo.getVisitorStatus();


            //ContentValues values_cat = new ContentValues();
            values_cat.put(KEY_VIS_REG_NO, visInfo.getVisitorRegNo());
            values_cat.put(KEY_VIS_FIRST_NAME, vis_firstname);
            values_cat.put(KEY_VIS_LAST_NAME, vis_lastname);
            values_cat.put(KEY_VIS_COMPANY_NAME, vis_companyname);
            values_cat.put(KEY_VIS_TYPE, vis_type);
            values_cat.put(KEY_VIS_REASON, vis_reason);
            values_cat.put(KEY_VIS_PHONE_NO, vis_phoneno);
            values_cat.put(KEY_VIS_EMAIL_ID, vis_emailid);
            values_cat.put(KEY_VIS_PHOTO, visImage1);
            values_cat.put(KEY_VIS_ACC_LEVEL, vis_acc_level);
            values_cat.put(KEY_VIS_CHECK_IN_TIME, vis_check_in_time);
            values_cat.put(KEY_VIS_CHECK_OUT_TIME, vis_check_out_time);
            values_cat.put(KEY_VIS_CARD_NO, vis_card_no);
            values_cat.put(KEY_VIS_CARD_ST, vis_card_st);
            values_cat.put(KEY_VIS_ST, vis_st);

            if (!isVisitorAlreadyExist(db, visInfo.getVisitorRegNo())) {
                db.insert(TABLE_VISITOR_INFO, null, values_cat);
            } else {
                db.update(TABLE_VISITOR_INFO, values_cat, KEY_VIS_REG_NO + "='" + visInfo.getVisitorRegNo() + "'", null);
            }

            //SQLiteDatabase db = this.getWritableDatabase();
            //new DownloadImagesTask(mContext, studInfo, db).execute(stud_id);
            //FutureTarget<Bitmap> ft = Glide.with(mContext).asBitmap().load(stud_image1).submit(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
            /*img_path = "";


                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            Bitmap bmp = ft.get();
                            SaveImages si = new SaveImages();
                            img_path = si.saveToInternalStorage(mContext, bmp, stud_id.replaceAll("/", "_"));
                            System.out.println("Local Image: "+img_path);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();*/


        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public boolean isVisitorAlreadyExist(SQLiteDatabase db, String vis_reg_no) {
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_VISITOR_INFO + " WHERE " + KEY_VIS_REG_NO + "='" + vis_reg_no + "'", null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    public boolean checkVisitorInfo() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_VISITOR_INFO, null);
        if (cur != null && cur.getCount() > 0) {
            cur.close();
            //db.close();
            return true;
        }
        cur.close();
        //db.close();
        return false;
    }

    public void deleteAllVisitors() {
        SQLiteDatabase db = this.getWritableDatabase();
        if (checkVisitorInfo()) {
            db.delete(TABLE_VISITOR_INFO, null, null);
        }
        //db.close();
    }

    public ArrayList<VisitorInfoModel.DataBean> getAllVisitorInfo () {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<VisitorInfoModel.DataBean> vis_list = new ArrayList<VisitorInfoModel.DataBean>();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_VISITOR_INFO, null);
        if (cur != null && cur.getCount() > 0) {

            if  (cur.moveToFirst()) {
                do {
                    VisitorInfoModel.DataBean vim = new VisitorInfoModel.DataBean();
                    vim.setVisitorRegNo(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_REG_NO)));
                    vim.setVisitorFirstName(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_FIRST_NAME)));
                    vim.setVisitorLasttName(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_LAST_NAME)));
                    vim.setCompanyName(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_COMPANY_NAME)));
                    vim.setVisitorType(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_TYPE)));
                    vim.setVisitReason(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_REASON)));
                    vim.setVisitorPhoneNo(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_PHONE_NO)));
                    vim.setVisitorEmailId(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_EMAIL_ID)));
                    vim.setVisitorPhoto(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_PHOTO)));
                    vim.setVisitorAccessLevel(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_ACC_LEVEL)));
                    vim.setVisitorCheckInTime(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_CHECK_IN_TIME)));
                    vim.setVisitorCheckOutTime(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_CHECK_OUT_TIME)));
                    vim.setVisitorCardNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_CARD_NO)));
                    vim.setVisitorCardStatus(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_ST)));
                    vim.setVisitorStatus(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_ST)));

                    vis_list.add(vim);

                } while(cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return vis_list;
    }

    public ArrayList<VisitorInfoModel.DataBean> getVisitorsBySearch (String searchText) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<VisitorInfoModel.DataBean> vis_list = new ArrayList<VisitorInfoModel.DataBean>();
        Cursor cur = db.rawQuery("SELECT * FROM " + TABLE_VISITOR_INFO + " WHERE " + KEY_VIS_FIRST_NAME + " LIKE '%" + searchText + "%' OR "
                + KEY_VIS_LAST_NAME + " LIKE '%" + searchText + "%' OR " + KEY_VIS_REG_NO + " LIKE '%" + searchText + "%' OR "
                + KEY_VIS_CARD_NO + " LIKE '%" +searchText + "%'", null);
        if (cur != null && cur.getCount() > 0) {

            if  (cur.moveToFirst()) {
                do {
                    VisitorInfoModel.DataBean vim = new VisitorInfoModel.DataBean();
                    vim.setVisitorRegNo(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_REG_NO)));
                    vim.setVisitorFirstName(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_FIRST_NAME)));
                    vim.setVisitorLasttName(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_LAST_NAME)));
                    vim.setCompanyName(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_COMPANY_NAME)));
                    vim.setVisitorType(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_TYPE)));
                    vim.setVisitReason(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_REASON)));
                    vim.setVisitorPhoneNo(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_PHONE_NO)));
                    vim.setVisitorEmailId(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_EMAIL_ID)));
                    vim.setVisitorPhoto(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_PHOTO)));
                    vim.setVisitorAccessLevel(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_ACC_LEVEL)));
                    vim.setVisitorCheckInTime(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_CHECK_IN_TIME)));
                    vim.setVisitorCheckOutTime(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_CHECK_OUT_TIME)));
                    vim.setVisitorCardNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_CARD_NO)));
                    vim.setVisitorCardStatus(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_ST)));
                    vim.setVisitorStatus(cur.getString(cur.getColumnIndexOrThrow(KEY_VIS_ST)));

                    vis_list.add(vim);

                } while(cur.moveToNext());
            }
        }
        cur.close();
        db.close();
        return vis_list;
    }


    public void saveMainGatePunchLogsData(MainGateAccLogs.Data mainGateAccLogs) {
        SQLiteDatabase db = this.getReadableDatabase();

        ContentValues main_gate_logs_data = new ContentValues();
        main_gate_logs_data.put(KEY_MAL_STUD_ID, mainGateAccLogs.getStudentId());
        main_gate_logs_data.put(KEY_MAL_STUD_NAME, mainGateAccLogs.getStudentName());
        main_gate_logs_data.put(KEY_MAL_STUD_CARD_NO, mainGateAccLogs.getCardNumber());
        main_gate_logs_data.put(KEY_MAL_STUD_MEAL_NO, mainGateAccLogs.getStudentMealNumber());
        main_gate_logs_data.put(KEY_MAL_GATE_ID, mainGateAccLogs.getGateId());
        main_gate_logs_data.put(KEY_MAL_GATE_NAME, mainGateAccLogs.getGateName());
        main_gate_logs_data.put(KEY_MAL_PUNCH_DATETIME, mainGateAccLogs.getPunchDateTime());
        main_gate_logs_data.put(KEY_MAL_PUNCH_TYPE, mainGateAccLogs.getPunchType());
        main_gate_logs_data.put(KEY_MAL_IS_SYNC, mainGateAccLogs.isSync());

        db.insert(TABLE_MAIN_GATE_LOGS, null, main_gate_logs_data);
        db.close();

    }

    public ArrayList<MainGateAccLogs.Data> getAllMainGatePunchLogsData(int punchType) {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<MainGateAccLogs.Data> main_gate_list = new ArrayList<MainGateAccLogs.Data>();
        String query = "SELECT * FROM " + TABLE_MAIN_GATE_LOGS;
        if (punchType == 1) {
            query = query + " WHERE " + KEY_MAL_PUNCH_TYPE + "=" + 1;
        } else if (punchType == 2) {
            query = query + " WHERE " + KEY_MAL_PUNCH_TYPE + "=" + 2;
        } else {
            //query = query + " WHERE " + KEY_MAL_PUNCH_TYPE + "=" + 1;
        }
        query = query + " ORDER BY " + KEY_MAL_PUNCH_DATETIME + " DESC";
        Cursor cur = db.rawQuery(query, null);
        if (cur != null && cur.getCount() > 0) {

            if (cur.moveToFirst()) {
                do {
                    MainGateAccLogs.Data mainGateAccLog = new MainGateAccLogs().new Data();
                    String stu_id = cur.getString(cur.getColumnIndexOrThrow(KEY_MAL_STUD_ID));
                    mainGateAccLog.setStudentId(stu_id);
                    mainGateAccLog.setStudentName(cur.getString(cur.getColumnIndexOrThrow(KEY_MAL_STUD_NAME)));
                    mainGateAccLog.setCardNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_MAL_STUD_CARD_NO)));
                    mainGateAccLog.setStudentMealNumber(cur.getString(cur.getColumnIndexOrThrow(KEY_MAL_STUD_MEAL_NO)));
                    mainGateAccLog.setGateId(cur.getInt(cur.getColumnIndexOrThrow(KEY_MAL_GATE_ID)));
                    mainGateAccLog.setGateName(cur.getString(cur.getColumnIndexOrThrow(KEY_MAL_GATE_NAME)));
                    mainGateAccLog.setPunchDateTime(cur.getString(cur.getColumnIndexOrThrow(KEY_MAL_PUNCH_DATETIME)));
                    mainGateAccLog.setPunchType(cur.getInt(cur.getColumnIndexOrThrow(KEY_MAL_PUNCH_TYPE)));
                    mainGateAccLog.setSync(cur.getInt(cur.getColumnIndexOrThrow(KEY_MAL_IS_SYNC)));
                    String stu_image = getStudentImageFromStudentId(stu_id);
                    mainGateAccLog.setStudentImage(stu_image);

                    main_gate_list.add(mainGateAccLog);
                } while (cur.moveToNext());
            }
        }
        db.close();
        db.close();
        return main_gate_list;
    }

    public void updateMainGateAccLogs(ArrayList<MainGateAccLogsModel.Data> list) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = null;
        for (int i=0; i<list.size(); i++) {
            MainGateAccLogsModel.Data data = list.get(i);//new MainGateAccLogsModel().new Data();

            ContentValues values_main_gate_logs = new ContentValues();
            values_main_gate_logs.put(KEY_MAL_IS_SYNC, 1);

            db.update(TABLE_MAIN_GATE_LOGS, values_main_gate_logs, KEY_MAL_STUD_ID + "='" + data.getStudentId() + "' AND "+KEY_MAL_GATE_ID + "="+data.getAgId() + " AND "+ KEY_MAL_PUNCH_DATETIME+"='"+data.getPunchDatetime()+"'", null);

        }
        if (cur!= null)
            cur.close();
        db.close();
    }
}