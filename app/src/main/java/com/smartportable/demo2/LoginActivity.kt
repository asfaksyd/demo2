package com.smartportable.demo2

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.fineapp.android.retrofitClient.RetrofitClientSingleton
import com.google.gson.JsonObject
import com.smartportable.demo2.helper.GlobalClass
import com.smartportable.demo2.models.LoginModel
import com.smartportable.demo2.utils.*
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Response

class LoginActivity: AppCompatActivity() {

    lateinit var btnLogin: Button
    lateinit var strStaffId:String
    lateinit var strPassword: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btnLogin = findViewById<Button>(R.id.btnLogin)

        btnLogin.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                /*val intent = Intent(this@LoginActivity, HomeScreen::class.java)
                startActivity(intent)*/

                if (etStaffId.text.toString().trim().isEmpty()) {
                    toast(this@LoginActivity, getString(R.string.enter_username_or_email))
                } else if (etPassword.text.toString().trim().isEmpty()) {
                    toast(this@LoginActivity, getString(R.string.enter_pass))
                } else {
                    strStaffId = etStaffId.text.toString()
                    strPassword = etPassword?.text.toString()
                    if (!isNetworkAvailable(this@LoginActivity)) {
                        toast(this@LoginActivity, getString(R.string.network_error))
                    } else {
                        btnLogin.hideKeyboard()
                        doLogin()
                    }

                }
            }
        })
    }

    private fun doLogin() {
        showProgressDialog(this@LoginActivity)

        var jObj : JsonObject = JsonObject()

        /*jObj.addProperty("Token", "1")
        jObj.addProperty("NextPageIndex", "1")*/

        var jData : JsonObject = JsonObject()
        if (strStaffId.contains("/") || GlobalClass.isNumeric(strStaffId)) {
            jData.addProperty("Staffid", strStaffId) //email
        } else {                                            // OR
            jData.addProperty("Email", strStaffId) // staff ID
        }
        jData.addProperty("Password", strPassword)

        // include the fcm parameter into login api
        //System.out.println("DeviceId: "+getDeviceToken())
        jData.addProperty(DEVICEID, "Testt6yujhhhhhhhhd")
        jData.addProperty(DEVICETYPE, DEVICETYPE_VAL)

        jObj.add("data", jData)
        System.out.println("Data"+ jObj.toString())


        val baseUrl = SharedPref.getStringValue(this@LoginActivity , keyAPIBaseUrl)
        RetrofitClientSingleton.getInstance(baseUrl).getUserLogin(jObj)
                .enqueue(object : retrofit2.Callback<LoginModel> {
                    override fun onFailure(call: Call<LoginModel>, t: Throwable) {
                        stopProgress()
                        toast(this@LoginActivity, getString(R.string.something_went_wrong)+" Error: "+t.localizedMessage)
                    }

                    override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>) {
                        stopProgress()
                        System.out.println("Response Code: "+response.code())
                        if (response.isSuccessful) {
                            val userDataResponse =  response.body()
                            //System.out.println(TAG+ "Login Response: Id "+userDataResponse?.data!!.id)
                            if (userDataResponse?.status == 1) {
                                //SharedPref.saveLogin(this@AuthActivity, userDataResponse!!)
                                toast(this@LoginActivity, userDataResponse?.message)

                                SharedPref.saveLogin(this@LoginActivity, userDataResponse!!)

                                val intent = Intent(this@LoginActivity, HomeScreen::class.java)
                                startActivity(intent)
                                finish()
                            } else if (userDataResponse!!.status == 2 || userDataResponse!!.status == 3) {
                                toast(this@LoginActivity, userDataResponse!!.message)
                            }


                        } else {
                            toast(this@LoginActivity, R.string.something_went_wrong)
                        }

                    }
                })
    }
}