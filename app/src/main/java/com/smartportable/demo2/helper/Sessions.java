package com.smartportable.demo2.helper;

/**
 * Created by Ananth on 3/27/2017.
 */

public class Sessions {
    int sessionId;
    String sessionName;
    long sessionDate, sessionFromDate, sessionToDate;

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public long getSessionDate() {
        return sessionDate;
    }

    public void setSessionDate(long sessionDate) {
        this.sessionDate = sessionDate;
    }

    public long getSessionFromDate() {
        return sessionFromDate;
    }

    public void setSessionFromDate(long sessionFromDate) {
        this.sessionFromDate = sessionFromDate;
    }

    public long getSessionToDate() {
        return sessionToDate;
    }

    public void setSessionToDate(long sessionToDate) {
        this.sessionToDate = sessionToDate;
    }

}
