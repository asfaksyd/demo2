package com.smartportable.demo2.helper;

import java.util.Locale;

/**
 * Created by Ananth on 12/27/2016.
 */

public class Config {
    public static int RECORDS_PER_PAGE = 1000;
    public static int SCAN_CARD_SERVICE_INTERVAL = 2 * 1000;
    public static int SEND_PUNCH_DATA_SERVICE_INTERVAL = 60 * 1000;
    public static int SYNC_DATA_SERVICE_INTERVAL = 300 * 1000;
    public static int LED_NOTIFICATION_ID = 0;
    public static int LED_ON_MS = 1000;
    public static int LED_OFF_MS = 100;

    public static int MUSTERING_ID = 3;
    public static int AREA_ID = 1;
    public static int ReaderId = 1; // For Mobile

    public static String MSB = "MSB-First";
    public static String LSB = "LSB-First";

    public static String ATR_VALUE_DESFIRE = "7577810280";
    public static String DESFIRE = "DESFire";
    public static String MIFARE = "Mifare";

    // for the email
    public static String FROM_EMAIL = "scampus11@gmail.com";
    public static String FROM_PASS = "qazxdr54321";
    public static String TO_EMAIL_LIST = "scampus11@gmail.com";//",raheman@dfsme.ae";

    public static String DEBUG_DB_PACKAGE = "com.asfaksaiyed.DebugDB";
    public static String MIFARE_NXP_PACKAGE = "com.nxp.mifare";

    public static Locale DATETIME_FORMAT_LOCAL = Locale.getDefault();

    public static final int REQUEST_CODE_ASK_PERMISSIONS = 123;
    public static final int REQUEST_PERMISSION_SETTING = 124;
}
