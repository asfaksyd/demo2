package com.smartportable.demo2.helper;

import android.app.ActivityManager;
import android.content.Context;

/**
 * Created by Ananth on 12/28/2016.
 */

public class ServiceHelper {
    public static ServiceHelper instance;
    public static String KEY_SERVICE_ACTION = "service_action";
    public static int START_SERVICE = 0;
    public static int STOP_SERVICE = 1;
    public static int RESTART_SERVICE = 2;

    // Variables values from where this service has been started
    public static final String FROM_MEM_ACC = "MemberAccess";
    public static final String FROM_MUSTERING = "Mustering";

    public static ServiceHelper getInstance() {
        if (instance == null)
            instance = new ServiceHelper();
        return instance;
    }

    public boolean isMyServiceRunning(Context mContext, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
