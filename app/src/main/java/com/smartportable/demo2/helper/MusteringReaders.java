package com.smartportable.demo2.helper;

import java.util.ArrayList;

/**
 * Created by Ananth on 3/19/2017.
 */

public class MusteringReaders {

    int Status;
    String Message;
    ArrayList<com.smartportable.demo2.helper.MusteringReaders> Data;

    int SLN_Employee;

    int SLN_Reader;
    String Card_Number, MusteringReaders;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<com.smartportable.demo2.helper.MusteringReaders> getData() {
        return Data;
    }

    public void setData(ArrayList<com.smartportable.demo2.helper.MusteringReaders> data) {
        Data = data;
    }

    public int getSLN_Reader() {
        return SLN_Reader;
    }

    public void setSLN_Reader(int SLN_Reader) {
        this.SLN_Reader = SLN_Reader;
    }

    public int getSLN_Employee() {
        return SLN_Employee;
    }

    public void setSLN_Employee(int SLN_Employee) {
        this.SLN_Employee = SLN_Employee;
    }

    public String getCard_Number() {
        return Card_Number;
    }

    public void setCard_Number(String card_Number) {
        Card_Number = card_Number;
    }

    public String getMusteringReaders() {
        return MusteringReaders;
    }

    public void setMusteringReaders(String musteringReaders) {
        MusteringReaders = musteringReaders;
    }

}
