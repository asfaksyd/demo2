package com.smartportable.demo2.helper;

/**
 * Created by Ananth on 3/23/2017.
 */

public class SyncType {

    int syncTypeImage;
    String syncTypeName;

    public int getSyncTypeImage() {
        return syncTypeImage;
    }

    public void setSyncTypeImage(int syncTypeImage) {
        this.syncTypeImage = syncTypeImage;
    }

    public String getSyncTypeName() {
        return syncTypeName;
    }

    public void setSyncTypeName(String syncTypeName) {
        this.syncTypeName = syncTypeName;
    }

}
