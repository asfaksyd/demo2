package com.smartportable.demo2.helper;

import java.util.ArrayList;

/**
 * Created by Ananth on 12/18/2016.
 */

public class Employee{
    int Status;
    String Message;
    ArrayList<Employee> Data;

    int SLN_Employee;
    int SLN_Location;
    int SLN_Reader;

    String EmployeeName, Employee_Photo, Card_Number;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        this.Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<Employee> getData() {
        return Data;
    }

    public void setData(ArrayList<Employee> data) {
        Data = data;
    }

    public int getSLN_Employee() {
        return SLN_Employee;
    }

    public void setSLN_Employee(int SLN_Employee) {
        this.SLN_Employee = SLN_Employee;
    }

    public int getSLN_Location() {
        return SLN_Location;
    }

    public void setSLN_Location(int SLN_Location) {
        this.SLN_Location = SLN_Location;
    }

    public int getSLN_Reader() {
        return SLN_Reader;
    }

    public void setSLN_Reader(int SLN_Reader) {
        this.SLN_Reader = SLN_Reader;
    }

    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        EmployeeName = employeeName;
    }

    public String getEmployee_Photo() {
        return Employee_Photo;
    }

    public void setEmployee_Photo(String employee_Photo) {
        Employee_Photo = employee_Photo;
    }

    public String getCard_Number() {
        return Card_Number;
    }

    public void setCard_Number(String card_Number) {
        Card_Number = card_Number;
    }
}
