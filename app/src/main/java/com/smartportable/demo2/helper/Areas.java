package com.smartportable.demo2.helper;

import java.util.ArrayList;

/**
 * Created by Ananth on 12/29/2016.
 */

public class Areas {
    int Status;
    String Message;
    ArrayList<Areas> Data;

    String Area_Name;

    int SLN_Area_ID;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        this.Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<Areas> getData() {
        return Data;
    }

    public void setData(ArrayList<Areas> Data) {
        this.Data = Data;
    }

    public int getSLN_Area_ID() {
        return SLN_Area_ID;
    }

    public void setSLN_Area_ID(int SLN_Area_ID) {
        this.SLN_Area_ID = SLN_Area_ID;
    }

    public String getArea_Name() {
        return Area_Name;
    }

    public void setArea_Name(String Area_Name) {
        this.Area_Name = Area_Name;
    }
}
