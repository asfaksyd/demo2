package com.smartportable.demo2.helper;

import java.util.ArrayList;

/**
 * Created by Ananth on 12/29/2016.
 */

public class Categories {
    int Status;
    String Message;
    ArrayList<Categories> Data;

    public int MusteringId;
    public String MusteringName;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        this.Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<Categories> getData() {
        return Data;
    }

    public void setData(ArrayList<Categories> Data) {
        this.Data = Data;
    }

    public int getMusteringId() {
        return MusteringId;
    }

    public void setMusteringId(int CategoryId) {
        this.MusteringId = CategoryId;
    }

    public String getMusteringName() {
        return MusteringName;
    }

    public void setMusteringName(String CategoryName) {
        this.MusteringName = CategoryName;
    }
}
