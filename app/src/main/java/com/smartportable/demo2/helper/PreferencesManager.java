package com.smartportable.demo2.helper;

/**
 * Created by Ananth on 1/9/2017.
 */

public class PreferencesManager {
    public static String KEY_SET_MUSTER_ID = "set_muster_id";
    public static String KEY_SET_MUSTER_NAME = "set_museter_name";

    public static String KEY_SET_READER_ID = "set_reader_id";
    public static String KEY_SET_READER_NAME = "set_reader_name";

    public static String KEY_SET_GATE_ID =  "Set_Gate_Id";
    public static String KEY_SET_GATE_NAME = "Set_Gate_Name";
    public static String KEY_SET_PUNCH_TYPE = "Set_Punch_Type";


    // Session
    public static String KEY_SET_SESSION_ID = "set_session_id";
    public static String KEY_SET_SESSION_NAME = "set_session_name";
    public static String KEY_SET_SESSION_DATE = "set_session_date";
    public static String KEY_SET_SESSION_START_TIME = "set_session_start_time";
    public static String KEY_SET_SESSION_END_TIME = "set_session_end_time";

    // for last sync date for all employees to store
    public static String KEY_SET_LAST_SYNCED_TIMESTAMP = "last_synced_timestamp";

    // for iclass reader baxkground service
    public static String KEY_CARD_SERVICE = "pref_key_run_in_bg";

    // for connection sctring from Preference Manager
    public static String KEY_SERVER_URL = "pref_key_server_url";

    // for the change language
    public static String KEY_LANGUAGE_HAS_CHANGE = "pref_key_lang_has_change";

    public static String KEY_CHANGE_LANG = "pref_key_change_lang";

    // for selected choose device in setting
    public static String KEY_SET_DEVICE = "pref_key_device_settings";
    public static String KEY_LED_SWITCH = "pref_key_led_on_off";

    public static String KEY_CAT_LED_SWITCH = "pref_key_cat_led_on_off";

    // for the mifare setting
    public static String KEY_SET_MIFARE_READING_TYPE = "pref_key_mifare_reading_type";
    public static String DEFAULT_MIFARE_READING_TYPE_LSB = "LSB-First";
    public static String DEFAULT_MIFARE_READING_TYPE_MSB = "MSB-First";

    // Employees Location Sync
    public static String KEY_LOCATION_SYNCED = "pref_key_loc_sync";

    // Sync interval
    public static String KEY_SYNC_INTERVAL = "pref_key_sync_interval";

    // Main Gate Access / Antipassback
    public static String KEY_MAIN_GATE_ACC_ANTIPASSBACK = "pref_key_anipassback";

    // Javax Exception
    public static String KEY_JAVAX_EXCEPTION = "pref_javax_exception";

    // TO check how the service has been started
    public static String KEY_SERVICE_STARTED_FROM = "pref_key_service_started_from";

    // to set the value that there is NFC or non NFC activity loaded
    public static String KEY_WHICH_ACTIVITY = "pref_key_which_act";

    public static String KEY_SESSION_START_TIME= "pref_key_session_stime";
    public static String KEY_SESSION_END_TIME = "pref_key_session_etime";
    public static String KEY_SESSION_DUE_DATE = "pref_key_session_due_date";
    public static String KEY_SESSION_READER_ID = "pref_key_session_render_id";
    public static String KEY_SESSION_ID = "pref_key_session_id";


}
