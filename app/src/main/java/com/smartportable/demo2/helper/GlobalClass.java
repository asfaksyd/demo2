package com.smartportable.demo2.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.smartportable.demo2.R;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.models.MainGateLogs;
import com.smartportable.demo2.models.Student;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Ananth on 1/4/2017.
 */

public class GlobalClass {
    public static String TAG = "GlobalClass";
    public static GlobalClass instance;
    int[] stArray = new int[] {R.drawable.st1, R.drawable.st2, R.drawable.st2, R.drawable.st4, R.drawable.st5};

    public static GlobalClass getInstance () {
        if (instance == null) {
            instance = new GlobalClass();
        }
        return instance;
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null
                && activeNetworkInfo.isConnectedOrConnecting();
    }

    public boolean ping(String url) {
        String str = "";
        try {
            Process process = Runtime.getRuntime().exec(
                    "/system/bin/ping -c 8 " + url);
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    process.getInputStream()));
            try {
                int mExitValue = process.waitFor();
                System.out.println(" mExitValue "+mExitValue);
                if(mExitValue==0){
                    Log.d(TAG, "mExitValue: true");
                    return true;
                }else{
                    Log.d(TAG, "mExitValue: false");
                    return false;
                }
            }
            catch (InterruptedException ignore)
            {
                ignore.printStackTrace();
                System.out.println(" Exception:"+ignore);
            }
            int i;
            char[] buffer = new char[4096];
            StringBuffer output = new StringBuffer();
            while ((i = reader.read(buffer)) > 0)
                output.append(buffer, 0, i);
            reader.close();

            // body.append(output.toString()+"\n");
            str = output.toString();
            Log.d(TAG, "Ping str: "+str);
        } catch (IOException e) {
            // body.append("Error\n");
            e.printStackTrace();
        }
        return false;
        //return str;
    }

    /**
     * Utility class to convert a byte array to a hexadecimal string.
     *
     * @param bytes Bytes to convert
     * @return String, containing hexadecimal representation.
     */
    public String ByteArrayToHexString(byte[] bytes, String typeStr, SharedPreferences prefsMifare) {
        final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[URLCollections.BYTE_LENGTH_FOR_MIFARE * 2];
        int v;

        // do not forgot to pass default values as second arguments other wise it will return empty string and condition will not be satisfied
        String mifare_reading_type = prefsMifare.getString(PreferencesManager.KEY_SET_MIFARE_READING_TYPE, PreferencesManager.DEFAULT_MIFARE_READING_TYPE_LSB);
        if (mifare_reading_type != null) {
            if (mifare_reading_type.length() != 0 ) {
                if (mifare_reading_type.equals(Config.LSB)) {
                    for (int j = 0; j < URLCollections.BYTE_LENGTH_FOR_MIFARE; j++) {
                        //for ( int j = 3; j ==0 ; j-- ) {
                        v = bytes[j] & 0xFF;
                        hexChars[(URLCollections.BYTE_LENGTH_FOR_MIFARE_TO_REVERSE - j) * 2] = hexArray[v >>> 4];
                        hexChars[(URLCollections.BYTE_LENGTH_FOR_MIFARE_TO_REVERSE - j) * 2 + 1] = hexArray[v & 0x0F];
                    }
                } else {
                    for (int j = 0; j < URLCollections.BYTE_LENGTH_FOR_MIFARE; j++) {
                        v = bytes[j] & 0xFF;
                        hexChars[j * 2] = hexArray[v >>> 4];
                        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
                    }
                }
            }
        }

        String str = new String(hexChars);
        if (typeStr.equals(Config.DESFIRE) && str.length() > 8) {
            str = str.substring(0, 7);
            return str;
        } else {
            return str;
        }
    }

    /*
    * Check for Mifare Format Card Number into AlphaNumeric key
     */
    public boolean checkAlphNumericString(String str) {
        return str.matches("[A-Za-z0-9]+");
    }


    /*
    * Prepare Student ArrayList
     */

    public ArrayList<Student> populateStudentData() {
        ArrayList<Student> stList = new ArrayList<Student>();
        if (stList.size() != 0) {
            stList.clear();
        }
        Student st1 = new Student();
        st1.setStId(1001);
        st1.setStName("George Mevrik");
        st1.setStCardno("6DA40014");
        st1.setStDeptName("Dept. Of IT");
        st1.setStPhoto(stArray[0]);

        Student st2 = new Student();
        st2.setStId(1002);
        st2.setStName("Peter Tondon");
        st2.setStCardno("5A5B5C5D");
        st2.setStDeptName("Dept. Of IT");
        st2.setStPhoto(stArray[1]);

        Student st3 = new Student();
        st3.setStId(1003);
        st3.setStName("Abdul Raheman");
        st3.setStCardno("4A4B4C4D");
        st3.setStDeptName("Dept. Of IT");
        st3.setStPhoto(stArray[2]);

        Student st4 = new Student();
        st4.setStId(1004);
        st4.setStName("Randa Restnick");
        st4.setStCardno("3A3B3C3D");
        st4.setStDeptName("Dept. Of IT");
        st4.setStPhoto(stArray[3]);

        Student st5 = new Student();
        st5.setStId(1005);
        st5.setStName("Julia Peter");
        st5.setStCardno("2A2B2C2D");
        st5.setStDeptName("Dept. Of IT");
        st5.setStPhoto(stArray[4]);

        stList.add(st1);
        stList.add(st2);
        stList.add(st3);
        stList.add(st4);
        stList.add(st5);

        return stList;
    }

    public ArrayList<Student> searchStudentRecord (String queryStr) {

        ArrayList<Student> stList = populateStudentData();

        ArrayList<Student> searchList = new ArrayList<Student>();
        /*if (searchList.size() > 0) {
            searchList.clear();
        }*/

        for (int i=0;i<stList.size();i++) {
            Student student = stList.get(i);
            if ((""+student.getStId()).toLowerCase().contains(queryStr.toLowerCase())) {
                searchList.add(student);
            } else if (student.getStName().toLowerCase().contains(queryStr.toLowerCase())) {
                searchList.add(student);
            } else if (student.getStCardno().toLowerCase().contains(queryStr.toLowerCase())) {
                searchList.add(student);
            } else if (student.getStDeptName().toLowerCase().contains(queryStr.toLowerCase())) {
                searchList.add(student);
            } else {
                //stList.add(student);
            }
        }

        return searchList;
    }

    public ArrayList<String> mainGateAccessPoints (Context mContext) {
        int[] accIds = mContext.getResources().getIntArray(R.array.readerIds);
        String[] accNames = mContext.getResources().getStringArray(R.array.readerNames);
        ArrayList<String> list = new ArrayList<String>();
        list.add(accIds[0] + " - " + accNames[0]);
        list.add(accIds[1] + " - " + accNames[1]);
        list.add(accIds[2] + " - " + accNames[2]);
        list.add(accIds[3] + " - " + accNames[3]);

        list.add(accIds[4] + " - " + accNames[4]);
        list.add(accIds[5] + " - " + accNames[5]);
        list.add(accIds[6] + " - " + accNames[6]);
        list.add(accIds[7] + " - " + accNames[7]);

        return list;
    }


    // Type 0 - ALl, 1 - Punch IN , 2 - Punch OUT
    public ArrayList<MainGateLogs> getMainGateLogsList (Context mContext, int type) {
        ArrayList<MainGateLogs> list = new ArrayList<MainGateLogs>();
        int[] campusIds = mContext.getResources().getIntArray(R.array.campusListId);
        String[] campusNames = mContext.getResources().getStringArray(R.array.campusList);
        int[] accIds = mContext.getResources().getIntArray(R.array.readerIds);
        String[] accNames = mContext.getResources().getStringArray(R.array.readerNames);

        MainGateLogs logs1 = new MainGateLogs();
        logs1.setMglStId(1001);
        logs1.setMglStName("George Mevrik");
        logs1.setMglStCardNo("6DA40014");
        logs1.setMglStDeptName("Dept. Of IT");
        logs1.setMglStPhoto(stArray[0]);
        logs1.setMglStCampusId(campusIds[0]);
        logs1.setMglStCampusName(campusNames[0]);
        logs1.setMglStMainGateAccId(accIds[1]);
        logs1.setMglStMainGateAccName(accNames[1]);
        logs1.setMglStPunchTime("2018-07-05 02:20 pm");

        MainGateLogs logs2 = new MainGateLogs();
        logs2.setMglStId(1002);
        logs2.setMglStName("Peter Tondon");
        logs2.setMglStCardNo("5A5B5C5D");
        logs2.setMglStDeptName("Dept. Of IT");
        logs2.setMglStPhoto(stArray[1]);
        logs2.setMglStCampusId(campusIds[0]);
        logs2.setMglStCampusName(campusNames[0]);
        logs2.setMglStMainGateAccId(accIds[1]);
        logs2.setMglStMainGateAccName(accNames[1]);
        logs2.setMglStPunchTime("2018-07-05 02:22 pm");

        MainGateLogs logs3 = new MainGateLogs();
        logs3.setMglStId(1003);
        logs3.setMglStName("George Peter");
        logs3.setMglStCardNo("4A4B4C4D");
        logs3.setMglStDeptName("Dept. Of IT");
        logs3.setMglStPhoto(stArray[2]);
        logs3.setMglStCampusId(campusIds[0]);
        logs3.setMglStCampusName(campusNames[0]);
        logs3.setMglStMainGateAccId(accIds[5]);
        logs3.setMglStMainGateAccName(accNames[5]);
        logs3.setMglStPunchTime("2018-07-05 02:25 pm");

        MainGateLogs logs4 = new MainGateLogs();
        logs4.setMglStId(1004);
        logs4.setMglStName("Randa Restnick");
        logs4.setMglStCardNo("3A3B3C3D");
        logs4.setMglStDeptName("Dept. Of IT");
        logs4.setMglStPhoto(stArray[3]);
        logs4.setMglStCampusId(campusIds[1]);
        logs4.setMglStCampusName(campusNames[1]);
        logs4.setMglStMainGateAccId(accIds[5]);
        logs4.setMglStMainGateAccName(accNames[5]);
        logs4.setMglStPunchTime("2018-07-05 02:30 pm");

        MainGateLogs logs5 = new MainGateLogs();
        logs5.setMglStId(1005);
        logs5.setMglStName("Julia Peter");
        logs5.setMglStCardNo("2A2B2C2D");
        logs5.setMglStDeptName("Dept. Of IT");
        logs5.setMglStPhoto(stArray[4]);
        logs5.setMglStCampusId(campusIds[1]);
        logs5.setMglStCampusName(campusNames[1]);
        logs5.setMglStMainGateAccId(accIds[5]);
        logs5.setMglStMainGateAccName(accNames[5]);
        logs5.setMglStPunchTime("2018-07-05 02:35 pm");

        if (type==0) {
            list.add(logs1);
            list.add(logs2);
            list.add(logs3);
            list.add(logs4);
        } else if (type == 1) {
            list.add(logs1);
            list.add(logs2);
        } else {
            list.add(logs3);
            list.add(logs4);
        }

        return list;
    }

    // Type 0 - ALl, 1 - Punch IN , 2 - Punch OUT
    public ArrayList<MainGateLogs> getMGAMultiLogs(Context mContext, int type) {
        ArrayList<MainGateLogs> list = new ArrayList<MainGateLogs>();
        int[] campusIds = mContext.getResources().getIntArray(R.array.campusListId);
        String[] campusNames = mContext.getResources().getStringArray(R.array.campusList);
        int[] accIds = mContext.getResources().getIntArray(R.array.readerIds);
        String[] accNames = mContext.getResources().getStringArray(R.array.readerNames);

        MainGateLogs logs1 = new MainGateLogs();
        logs1.setMglStId(1001);
        logs1.setMglStName("George Mevrik");
        logs1.setMglStCardNo("6DA40014");
        logs1.setMglStDeptName("Dept. Of IT");
        logs1.setMglStPhoto(stArray[0]);
        logs1.setMglStCampusId(campusIds[0]);
        logs1.setMglStCampusName(campusNames[0]);
        logs1.setMglStMainGateAccId(accIds[1]);
        logs1.setMglStMainGateAccName(accNames[1]);
        logs1.setMglStPunchTime("2018-07-05 02:20 pm");

        MainGateLogs logs2 = new MainGateLogs();
        logs2.setMglStId(1001);
        logs2.setMglStName("George Mevrik");
        logs2.setMglStCardNo("6DA40014");
        logs2.setMglStDeptName("Dept. Of IT");
        logs2.setMglStPhoto(stArray[0]);
        logs2.setMglStCampusId(campusIds[0]);
        logs2.setMglStCampusName(campusNames[0]);
        logs2.setMglStMainGateAccId(accIds[4]);
        logs2.setMglStMainGateAccName(accNames[4]);
        logs2.setMglStPunchTime("2018-07-05 02:22 pm");

        MainGateLogs logs3 = new MainGateLogs();
        logs3.setMglStId(1001);
        logs3.setMglStName("George Mevrik");
        logs3.setMglStCardNo("6DA40014");
        logs3.setMglStDeptName("Dept. Of IT");
        logs3.setMglStPhoto(stArray[0]);
        logs3.setMglStCampusId(campusIds[0]);
        logs3.setMglStCampusName(campusNames[0]);
        logs3.setMglStMainGateAccId(accIds[1]);
        logs3.setMglStMainGateAccName(accNames[1]);
        logs3.setMglStPunchTime("2018-07-05 02:25 pm");

        MainGateLogs logs4 = new MainGateLogs();
        logs4.setMglStId(1001);
        logs4.setMglStName("George Mevrik");
        logs4.setMglStCardNo("6DA40014");
        logs4.setMglStDeptName("Dept. Of IT");
        logs4.setMglStPhoto(stArray[0]);
        logs4.setMglStCampusId(campusIds[0]);
        logs4.setMglStCampusName(campusNames[0]);
        logs4.setMglStMainGateAccId(accIds[4]);
        logs4.setMglStMainGateAccName(accNames[4]);
        logs4.setMglStPunchTime("2018-07-05 02:27 pm");

        MainGateLogs logs5 = new MainGateLogs();
        logs5.setMglStId(1005);
        logs5.setMglStName("Julia Peter");
        logs5.setMglStCardNo("2A2B2C2D");
        logs5.setMglStDeptName("Dept. Of IT");
        logs5.setMglStPhoto(stArray[4]);
        logs5.setMglStCampusId(campusIds[1]);
        logs5.setMglStCampusName(campusNames[1]);
        logs5.setMglStMainGateAccId(accIds[5]);
        logs5.setMglStMainGateAccName(accNames[5]);
        logs5.setMglStPunchTime("2018-07-05 02:35 pm");

        if (type==0) {
            list.add(logs1);
            list.add(logs2);
            list.add(logs3);
            list.add(logs4);
        } else if (type == 1) {
            list.add(logs1);
            list.add(logs2);
        } else {
            list.add(logs3);
            list.add(logs4);
        }

        return list;
    }

    public static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(.\\d+)?");
    }
}
