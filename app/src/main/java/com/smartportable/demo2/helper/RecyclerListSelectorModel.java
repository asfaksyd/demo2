package com.smartportable.demo2.helper;

/**
 * Created by Ananth on 1/29/2017.
 */

public class RecyclerListSelectorModel {
    private boolean isSelected = false;

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }
}
