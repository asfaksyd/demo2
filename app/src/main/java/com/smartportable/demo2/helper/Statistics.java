package com.smartportable.demo2.helper;

/**
 * Created by Ananth on 2/5/2017.
 */

public class Statistics {
    int musterId;
    String musterName;
    int musterCount;

    public int getMusterId() {
        return musterId;
    }

    public void setMusterId(int musterId) {
        this.musterId = musterId;
    }

    public String getMusterName() {
        return musterName;
    }

    public void setMusterName(String musterName) {
        this.musterName = musterName;
    }

    public int getMusterCount() {
        return musterCount;
    }

    public void setMusterCount(int musterCount) {
        this.musterCount = musterCount;
    }

}
