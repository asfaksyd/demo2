package com.smartportable.demo2.helper;

import java.util.ArrayList;

/**
 * Created by Ananth on 1/8/2017.
 */

public class PunchLogs {
    int Status;
    String Message;
    ArrayList<PunchLogs> Data;

    int SLN_Punch;
    String SLN_Employee;
    int SLN_Reader;
    int SLN_Location;
    int SLN_Area_ID;
    int MusteringId;
    String Card_Number, Punch_Date;
    boolean Grant_Deny;
    int isPunchSynced;
    String SLN_EmployeeName;
    String SLN_EmployeePhoto;

    String Reader_Name;
    String PunchDateTime;

    int sessionId;
    String sessionName;
    int accessCode;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        this.Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<PunchLogs> getData() {
        return Data;
    }

    public void setData(ArrayList<PunchLogs> data) {
        Data = data;
    }

    public int getSLN_Punch() {
        return SLN_Punch;
    }

    public void setSLN_Punch(int SLN_Punch) {
        this.SLN_Punch = SLN_Punch;
    }

    public int getSLN_Reader() {
        return SLN_Reader;
    }

    public void setSLN_Reader(int SLN_Reader) {
        this.SLN_Reader = SLN_Reader;
    }

    public String getReader_Name() {
        return Reader_Name;
    }

    public void setReader_Name(String reader_Name) {
        Reader_Name = reader_Name;
    }

    public int getSLN_Area_ID() {
        return SLN_Area_ID;
    }

    public void setSLN_Area_ID(int SLN_Area_ID) {
        this.SLN_Area_ID = SLN_Area_ID;
    }

    public int getMusteringId() {
        return MusteringId;
    }

    public void setMusteringId(int musteringId) {
        MusteringId = musteringId;
    }

    public String getPunch_Date() {
        return Punch_Date;
    }

    public void setPunch_Date(String punch_Date) {
        Punch_Date = punch_Date;
    }

    public boolean isGrant_Deny() {
        return Grant_Deny;
    }

    public void setGrant_Deny(boolean grant_Deny) {
        Grant_Deny = grant_Deny;
    }

    public int getIsPunchSynced () {
        return isPunchSynced;
    }

    public void setIsPunchSynced (int isPunchSynced) {
        this.isPunchSynced = isPunchSynced;
    }

    public String getSLN_Employee() {
        return SLN_Employee;
    }

    public void setSLN_Employee(String SLN_Employee) {
        this.SLN_Employee = SLN_Employee;
    }

    public String getSLN_EmployeeName() {
        return SLN_EmployeeName;
    }

    public void setSLN_EmployeeName(String SLN_EmployeeName) {
        this.SLN_EmployeeName = SLN_EmployeeName;
    }

    public String getSLN_EmployeePhoto() {
        return SLN_EmployeePhoto;
    }

    public void setSLN_EmployeePhoto(String SLN_EmployeePhoto) {
        this.SLN_EmployeePhoto = SLN_EmployeePhoto;
    }

    public int getSLN_Location() {
        return SLN_Location;
    }

    public void setSLN_Location(int SLN_Location) {
        this.SLN_Location = SLN_Location;
    }

    public String getCard_Number() {
        return Card_Number;
    }

    public void setCard_Number(String card_Number) {
        Card_Number = card_Number;
    }


    public String getPunchDateTime() {
        // for current date time
        return PunchDateTime;
    }

    public void setPunchDateTime(String punchDateTime) {
        PunchDateTime = punchDateTime;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }


    public int getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(int accessCode) {
        this.accessCode = accessCode;
    }
}
