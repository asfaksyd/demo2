package com.smartportable.demo2.helper;

/**
 * Created by Ananth on 3/27/2017.
 */

public class MemberAccessLogs {
    int MemAccLogsEmpId, MemAccLogsReaderId, MemAccLogsSessionId, MemAccLogsAccessGrant, MemAccLogsIsSync;
    String MemAccLogsStuId;
    String MemAccLogsEmpName;
    String MemAccLogsEmpPhoto;
    String MemAccLogsCardNo;
    String MemAccLogsPunchDate;

    String MemAccLogsReaderName;
    String MemAccLogsSessionName;


    public int getMemAccLogsEmpId() {
        return MemAccLogsEmpId;
    }

    public void setMemAccLogsEmpId(int memAccLogsEmpId) {
        MemAccLogsEmpId = memAccLogsEmpId;
    }

    public String getMemAccLogsStuId() {
        return MemAccLogsStuId;
    }

    public void setMemAccLogsStuId(String memAccLogsStuId) {
        MemAccLogsStuId = memAccLogsStuId;
    }


    public int getMemAccLogsReaderId() {
        return MemAccLogsReaderId;
    }

    public void setMemAccLogsReaderId(int memAccLogsReaderId) {
        MemAccLogsReaderId = memAccLogsReaderId;
    }

    public int getMemAccLogsSessionId() {
        return MemAccLogsSessionId;
    }

    public void setMemAccLogsSessionId(int memAccLogsSessionId) {
        MemAccLogsSessionId = memAccLogsSessionId;
    }

    public int getMemAccLogsAccessGrant() {
        return MemAccLogsAccessGrant;
    }

    public void setMemAccLogsAccessGrant(int memAccLogsAccessGrant) {
        MemAccLogsAccessGrant = memAccLogsAccessGrant;
    }

    public int getMemAccLogsIsSync() {
        return MemAccLogsIsSync;
    }

    public void setMemAccLogsIsSync(int memAccLogsIsSync) {
        MemAccLogsIsSync = memAccLogsIsSync;
    }

    public String getMemAccLogsEmpName() {
        return MemAccLogsEmpName;
    }

    public void setMemAccLogsEmpName(String memAccLogsEmpName) {
        MemAccLogsEmpName = memAccLogsEmpName;
    }

    public String getMemAccLogsEmpPhoto() {
        return MemAccLogsEmpPhoto;
    }

    public void setMemAccLogsEmpPhoto(String memAccLogsEmpPhoto) {
        MemAccLogsEmpPhoto = memAccLogsEmpPhoto;
    }

    public String getMemAccLogsCardNo() {
        return MemAccLogsCardNo;
    }

    public void setMemAccLogsCardNo(String memAccLogsCardNo) {
        MemAccLogsCardNo = memAccLogsCardNo;
    }

    public String getMemAccLogsPunchDate() {
        return MemAccLogsPunchDate;
    }

    public void setMemAccLogsPunchDate(String memAccLogsPunchDate) {
        MemAccLogsPunchDate = memAccLogsPunchDate;
    }

    public String getMemAccLogsReaderName() {
        return MemAccLogsReaderName;
    }

    public void setMemAccLogsReaderName(String memAccLogsReaderName) {
        MemAccLogsReaderName = memAccLogsReaderName;
    }

    public String getMemAccLogsSessionName() {
        return MemAccLogsSessionName;
    }

    public void setMemAccLogsSessionName(String memAccLogsSessionName) {
        MemAccLogsSessionName = memAccLogsSessionName;
    }
}
