package com.smartportable.demo2;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.tabs.TabLayout;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.fragments.AccessedMembersList;
import com.smartportable.demo2.fragments.AllowedMembersList;
import com.smartportable.demo2.fragments.DeniedMembersList;
import com.smartportable.demo2.fragments.PendingMembersList;
import com.smartportable.demo2.helper.Boast;
import com.smartportable.demo2.helper.Config;
import com.smartportable.demo2.helper.CustomViewPager;
import com.smartportable.demo2.helper.HandlerHelper;
import com.smartportable.demo2.fragments.MemAccLogDetailFragment;
import com.smartportable.demo2.helper.MemberAccessLogs;
import com.smartportable.demo2.helper.PreferencesManager;
import com.smartportable.demo2.helper.ServiceHelper;
import com.smartportable.demo2.interfaces.FragmentChangeInterface;
import com.smartportable.demo2.receiver.SendPunchDataServiceBroadcastReceiver;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import fr.coppernic.cpcframework.cpcpowermgmt.cone.PowerMgmt;
import fr.coppernic.sdk.hid.iclassProx.BaudRate;
import fr.coppernic.sdk.hid.iclassProx.Card;
import fr.coppernic.sdk.hid.iclassProx.CascadeLevel;
import fr.coppernic.sdk.hid.iclassProx.ErrorCodes;
import fr.coppernic.sdk.hid.iclassProx.FrameProtocol;
import fr.coppernic.sdk.hid.iclassProx.OnGetReaderInstanceListener;
import fr.coppernic.sdk.hid.iclassProx.Reader;
import fr.coppernic.sdk.utils.core.CpcDefinitions;
import fr.coppernic.sdk.utils.debug.Log;
import fr.coppernic.sdk.utils.helpers.CpcOs;

/**
 * Created by Ananth on 3/19/2017.
 */

public class MemberAccessMainActivity extends AppCompatActivity {

    FragmentManager fm;
    FragmentTransaction ft;
    FrameLayout frameContainerMemAcc;
    Fragment currentFragment;

    public String TAG = this.getClass().getCanonicalName();
    RelativeLayout rlAllMemberAccess, rlAllowedMemberAccess;
    RelativeLayout viewAllMember, viewAllowMember;

    Toolbar toolbar;
    CustomViewPager viewPager;
    ViewPagerAdapter adapter;
    TabLayout tabLayout;
    AlarmManager alarmMgr;
    PendingIntent alarmIntent;
    AlertDialog.Builder access_dialog;
    AlertDialog access_alert;

    Handler mHandlerCardScan;
    boolean threadRunning = false;
    int count = 1;

    private PowerMgmt mPowerMgmt;
    MediaPlayer mediaPlayer;
    Reader sReader;
    Card card = new Card();
    PowerMgmt.InterfacesCone interfaceCone = null;
    SharedPreferences prefsMifare;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_member_access_main);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            try {
                ViewConfiguration config = ViewConfiguration.get(this);
                Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
                if(menuKeyField != null) {
                    menuKeyField.setAccessible(true);
                    menuKeyField.setBoolean(config, false);
                }
            } catch (Exception ex) {
                // Ignore
            }
        }
        //toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        prefsMifare = PreferenceManager.getDefaultSharedPreferences(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (CustomViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        viewPager.beginFakeDrag();

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorHeight((int) (4 * getResources().getDisplayMetrics().density));
        viewPager.setOffscreenPageLimit(1);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                FragmentChangeInterface inteface = (FragmentChangeInterface) adapter.instantiateItem(viewPager, position);
                if (inteface != null) {
                    inteface.fragmentBecameVisible(MemberAccessMainActivity.this);
                }

                // to change the tab indicator color
                if (position == 0) {
                    tabLayout.setSelectedTabIndicatorColor(getApplicationContext().getResources().getColor(R.color.color_cyan));
                } else if (position == 1) {
                    tabLayout.setSelectedTabIndicatorColor(getApplicationContext().getResources().getColor(R.color.color_green));
                } else if (position == 2) {
                    tabLayout.setSelectedTabIndicatorColor(getApplicationContext().getResources().getColor(android.R.color.holo_red_dark));
                } else if (position == 3) {
                    tabLayout.setSelectedTabIndicatorColor(getApplicationContext().getResources().getColor(android.R.color.darker_gray));
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        // to start service
        alarmMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);

        if (CpcOs.isCone()) {

            // to set that service started from "Member Access"
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putString(PreferencesManager.KEY_SERVICE_STARTED_FROM, ServiceHelper.FROM_MEM_ACC);
            edit.commit();

            /*Intent intent = new Intent(this, ScanCardServiceBroadcastReceiver.class);
            alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);*/
            //alarmMgr.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), Config.SCAN_CARD_SERVICE_INTERVAL, alarmIntent);

        } else {
            Log.d(TAG, "Other device");
        }

        Intent intent_send_punch = new Intent(this, SendPunchDataServiceBroadcastReceiver.class);
        PendingIntent piSendPunchData = PendingIntent.getBroadcast(this, 0, intent_send_punch, 0);
        alarmMgr.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), Config.SEND_PUNCH_DATA_SERVICE_INTERVAL, piSendPunchData);


        if (CpcOs.isCone()) {
            // Media player pobject
            mediaPlayer = MediaPlayer.create(this, R.raw.censored_beep_mastercard);//R.raw.a_tone_his_self);
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    android.util.Log.d(TAG, "on media player completion");
                    // to start LED1 blue fir cone eid
                    mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.Led, PowerMgmt.ManufacturersCone.BuiltIn, PowerMgmt.ModelsCone.Led1, PowerMgmt.InterfacesCone.ExpansionPort, false);
                }
            });

            Reader.getInstance(this, new OnGetReaderInstanceListener() {
                @Override
                public void OnGetReaderInstance(Reader instance) {
                    sReader = instance;
                }
            });

            if (mPowerMgmt == null) {
                mPowerMgmt = new PowerMgmt(this);
            }

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
            String device_value = prefs.getString(PreferencesManager.KEY_SET_DEVICE, "");

            interfaceCone = PowerMgmt.InterfacesCone.ExpansionPort;
            if (device_value != null && device_value.length() != 0) {
                if (device_value.equals(URLCollections.DEVICE_CONE)) {
                    interfaceCone = PowerMgmt.InterfacesCone.ExpansionPort;
                } else {
                    interfaceCone = PowerMgmt.InterfacesCone.UsbGpioPort;
                }
            }

            mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.RfidSc, PowerMgmt.ManufacturersCone.Hid, PowerMgmt.ModelsCone.IClassProx, interfaceCone, true);

            // to start the card scan runnable
            mHandlerCardScan = new Handler();
            if (!threadRunning) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startScanCardsRunnable();
                    }
                }, 2000);

            }
        }
    }

    public void startScanCardsRunnable () {
        sReader.open(CpcDefinitions.HID_ICLASS_PROX_READER_PORT, BaudRate.B9600);//(BaudRate) mSpBaudRate.getSelectedItem());
        sReader.samCommandSuspendAutonomousMode();

        mStartScanCards.run();
    }

    public void stopScanCardsRunnable () {
        threadRunning = false;
        mHandlerCardScan.removeCallbacks(mStartScanCards);
        sReader.close();
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new AllowedMembersList(), getResources().getString(R.string.label_allowed));
        adapter.addFrag(new AccessedMembersList(), getResources().getString(R.string.label_accessed));
        adapter.addFrag(new DeniedMembersList(), getResources().getString(R.string.label_denied));
        adapter.addFrag(new PendingMembersList(), getResources().getString(R.string.label_pending));

        viewPager.setAdapter(adapter);
    }

    // runnable to read / scan cards interval of 2 seconds
    Runnable mStartScanCards = new Runnable() {
        @Override
        public void run() {
            try {
                /*sReader.open(CpcDefinitions.HID_ICLASS_PROX_READER_PORT, BaudRate.B9600);//(BaudRate) mSpBaudRate.getSelectedItem());

                // this code will be executed after 0.7 seconds*/
                //sReader.samCommandSuspendAutonomousMode();
                Card card = new Card();
                final FrameProtocol[] fp = new FrameProtocol[2];
                fp[0] = FrameProtocol.PICO15693;
                fp[1] = FrameProtocol.ISO14443A;

                ErrorCodes er_scan = sReader.samCommandScanFieldForCard(fp, CascadeLevel.CASCADE_LEVEL_DISABLED, card);

                if (er_scan == ErrorCodes.ER_OK) {
                    String card_number = "";
                    System.out.println("Card protocol:"+card.getFrameProtocol());
                    if (card.getFrameProtocol() == FrameProtocol.PICO15693) {
                        sReader.getCardNumber(card);
                        System.out.println("Card No.: "+card.getCardNumber());
                        if (card.getCardNumber() != -1) {
                            //if (card1.getCardNumber() != -1) {
                            if (mediaPlayer != null) {
                                //System.out.println("Card mediaPlayer not null");
                                try {
                                    mediaPlayer = MediaPlayer.create(MemberAccessMainActivity.this, R.raw.censored_beep_mastercard);//R.raw.a_tone_his_self);
                                    mediaPlayer.start();

                                    // to ON LED1 blue fir cone eid
                                    mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.Led, PowerMgmt.ManufacturersCone.BuiltIn, PowerMgmt.ModelsCone.Led1, PowerMgmt.InterfacesCone.ExpansionPort, true);

                                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                        @Override
                                        public void onCompletion(MediaPlayer mediaPlayer) {
                                            //Log.d(TAG, "on media player completion");
                                            // to start LED1 blue fir cone eid
                                            mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.Led, PowerMgmt.ManufacturersCone.BuiltIn, PowerMgmt.ModelsCone.Led1, PowerMgmt.InterfacesCone.ExpansionPort, false);
                                        }
                                    });

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            // to store punch entry in database for Mustering
                            //byte[] card_byte = card.getCardSerialNumber();
                            card_number = ""+card.getCardNumber();
                            doPunchEntriesMemAcc(MemberAccessMainActivity.this, "" + card_number);//card1.getCardNumber());
                        }
                    } else if (card.getFrameProtocol() == FrameProtocol.ISO14443A) {
                        if (card.getCardSerialNumber() != null) {
                            //if (card1.getCardNumber() != -1) {
                            if (mediaPlayer != null) {
                                //System.out.println("Card mediaPlayer not null");
                                try {
                                    mediaPlayer = MediaPlayer.create(MemberAccessMainActivity.this, R.raw.censored_beep_mastercard);//R.raw.a_tone_his_self);
                                    mediaPlayer.start();

                                    // to ON LED1 blue fir cone eid
                                    mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.Led, PowerMgmt.ManufacturersCone.BuiltIn, PowerMgmt.ModelsCone.Led1, PowerMgmt.InterfacesCone.ExpansionPort, true);

                                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                        @Override
                                        public void onCompletion(MediaPlayer mediaPlayer) {
                                            //Log.d(TAG, "on media player completion");
                                            // to start LED1 blue fir cone eid
                                            mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.Led, PowerMgmt.ManufacturersCone.BuiltIn, PowerMgmt.ModelsCone.Led1, PowerMgmt.InterfacesCone.ExpansionPort, false);
                                        }
                                    });

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            // to store punch entry in database for Mustering
                            byte[] card_byte = card.getCardSerialNumber();
                            byte[] card_atr = card.getAtr();
                            if (card_atr == null) {
                                card_number = ByteArrayToHexString(card_byte, Config.MIFARE);//getCardNumber();
                            } else {
                                if (card_atr.equals(Config.ATR_VALUE_DESFIRE)) {
                                    System.out.println("DESFIRE ATR Values"+card_atr);
                                    card_number = ByteArrayToHexString(card_byte, Config.DESFIRE);
                                } else {
                                    System.out.println("MIFARE ATR Values"+card_atr);
                                    card_number = ByteArrayToHexString(card_byte, Config.MIFARE);
                                }
                            }
                            doPunchEntriesMemAcc(MemberAccessMainActivity.this, "" + card_number);//card1.getCardNumber());
                        }
                    }
                    System.out.println("Card Number: " + card_number);
                }
            } finally {
                mHandlerCardScan.postDelayed(mStartScanCards, Config.SCAN_CARD_SERVICE_INTERVAL);
            }
        }
    };

    public void doPunchEntriesMemAcc(Context mContext, String cardNumber) {
        DatabaseHandler db = new DatabaseHandler(mContext);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        // content values for PunchLogs entries
        TimeZone.setDefault(null);
        System.setProperty("user.timezone", "");
        TimeZone.setDefault(null);
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        //System.out.println("Current time => "+c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        String punchDateTime = df.format(c.getTime());
        android.util.Log.d(TAG, "MemAcc "+punchDateTime + " Timezone : "+c.getTimeZone());
        int readerId = prefs.getInt(PreferencesManager.KEY_SET_READER_ID, -1);
        int sessionId = prefs.getInt(PreferencesManager.KEY_SET_SESSION_ID, -1);
        long sessionDate = prefs.getLong(PreferencesManager.KEY_SET_SESSION_DATE, -1);
        long sessionStartTime = prefs.getLong(PreferencesManager.KEY_SET_SESSION_START_TIME, -1);
        long sessionEndTime = prefs.getLong(PreferencesManager.KEY_SET_SESSION_END_TIME, -1);
        String readerName = prefs.getString(PreferencesManager.KEY_SET_READER_NAME, "");
        int musterId = prefs.getInt(PreferencesManager.KEY_SET_MUSTER_ID, -1);

        int empId = db.getEmployeeIdFromCardNumber(cardNumber);

        // to get sesstion start //yyyy-MM-dd HH:mm:ss
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd", Config.DATETIME_FORMAT_LOCAL);
        String session_date = sdfDate.format(sessionDate);

        SimpleDateFormat sdfStartTime = new SimpleDateFormat("HH:mm:ss", Locale.US);
        //sdfStartTime.setTimeZone(TimeZone.getTimeZone("UTC"));
        String session_start_time = sdfStartTime.format(sessionStartTime);
        String session_end_time = sdfStartTime.format(sessionEndTime);

        int fromDateDiff = punchDateTime.compareTo(session_date+" "+session_start_time);
        int toDateDiff = punchDateTime.compareTo(session_date+" "+session_end_time);
        android.util.Log.d("Date Diff", "startDate "+session_date+" "+session_start_time+" DIFF "+fromDateDiff+" EndDate "+session_date+" "+session_end_time+" DIFF " + toDateDiff);
        // check whether the flash card is allow for these session time interval or not
        if (fromDateDiff >= 0 && toDateDiff <= 0) {
            fr.coppernic.sdk.utils.debug.Log.d(TAG, "Card punch time is correct");

            int access_status = db.checkCardHavingDoorAccess(readerId, sessionId, cardNumber, session_date+" "+session_start_time, session_date+" "+session_end_time);
            // 0 - Can access / Allowed
            // 1 - Already accessed / Not allowed
            // 2 -
            if (access_status > -1) {
                //Boast.makeText(this, "Having door access", Toast.LENGTH_SHORT).show();

                MemberAccessLogs memacc = new MemberAccessLogs();

                if (empId != -1 && empId > -1) {
                    memacc.setMemAccLogsEmpId(empId);
                } else {
                    memacc.setMemAccLogsEmpId(empId);
                }

                if (cardNumber != null) {
                    if (cardNumber.length() != 0)
                        memacc.setMemAccLogsCardNo(cardNumber);
                }
                if (readerId != -1)
                    memacc.setMemAccLogsReaderId(readerId);
                if (sessionId != -1)
                    memacc.setMemAccLogsSessionId(sessionId);

                memacc.setMemAccLogsPunchDate(punchDateTime);
                memacc.setMemAccLogsIsSync(0);


                memacc.setMemAccLogsAccessGrant(access_status);
                if (musterId >= 0) {
                    //memacc.setMemAccLogsMusteringId(musterId);
                } else {
                    //memacc.setMemAccLogsMusteringId(-1);
                }

                if (access_status == 0) {
                    android.util.Log.d(TAG, "First entry");
                    showAccessGrantedDialog(memacc);
                    //EventBus.getDefault().post(new MemberAccessMainActivity.MessageEvent(memacc));
                } else if (access_status == 2) {
                    android.util.Log.d(TAG, "Duplicate Entries");
                    showAccessGrantedDialog(memacc);
                    //EventBus.getDefault().post(new MemberAccessMainActivity.MessageEvent(memacc));
                }

                db.saveMemAccPunchLogsData(memacc);

            } else {
                if (access_status == -1) {
                    access_status = 1;
                }
                //Boast.makeText(this, "Not having door access!!!", Toast.LENGTH_SHORT).show();
                MemberAccessLogs memacc = new MemberAccessLogs();

                if (empId != -1 && empId > -1) {
                    memacc.setMemAccLogsEmpId(empId);
                } else {
                    memacc.setMemAccLogsEmpId(empId);
                }

                if (cardNumber != null) {
                    if (cardNumber.length() != 0)
                        memacc.setMemAccLogsCardNo(cardNumber);
                }
                if (readerId != -1)
                    memacc.setMemAccLogsReaderId(readerId);
                if (sessionId != -1)
                    memacc.setMemAccLogsSessionId(sessionId);
                memacc.setMemAccLogsPunchDate(punchDateTime);
                memacc.setMemAccLogsIsSync(0);

                /*if (access_status == 0) {
                    memacc.setMemAccLogsAccessGrant(0);
                } else {
                    memacc.setMemAccLogsAccessGrant(1);
                }*/
                memacc.setMemAccLogsAccessGrant(access_status);
                //memacc.setMemAccLogsAreaId(-1);
                if (musterId >= 0) {
                    //memacc.setMemAccLogsMusteringId(musterId);
                } else {
                    //memacc.setMemAccLogsMusteringId(-1);
                }

                Log.d(TAG, "No Door Access Entries");
                showAccessGrantedDialog(memacc);
                //EventBus.getDefault().post(new MemberAccessMainActivity.MessageEvent(this, HandlerHelper.MEM_ACC_NO_DOOR_ACCESS));
                //EventBus.getDefault().post(new MemberAccessMainActivity.MessageEvent(memacc));
                db.saveMemAccPunchLogsData(memacc);
                //refreshFragment();
            }
        } else {
            Boast.makeText(this, "Session is not valid to flash card", Toast.LENGTH_SHORT).show();
            //EventBus.getDefault().post(new MemberAccessMainActivity.MessageEvent(this, HandlerHelper.MEM_ACC_INVALID_SESSION));
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private Fragment mCurrentFragment;

        public Fragment getCurrentFragment() {
            return mCurrentFragment;
        }

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            if (getCurrentFragment() != object) {
                mCurrentFragment = ((Fragment) object);
            }
            super.setPrimaryItem(container, position, object);
        }
    }

    @Override
    public void onBackPressed() {

        /*int backCount = fm.getBackStackEntryCount();
        Log.d(TAG, "BackStack Count"+backCount);
        if (backCount > 1) {
            fm.popBackStack();
        } else {
            if (backCount == 1)
                fm.popBackStack();

            super.onBackPressed();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }*/
        MemberAccessMainActivity.this.finish();
        if(URLCollections.whichDirection(MemberAccessMainActivity.this) == URLCollections.DEV_DIR_RTL) {
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
        } else {
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            //setResult(SyncActivity.REQUEST_SYNC_EMPLOYEES);
            MemberAccessMainActivity.this.finish();
            if(URLCollections.whichDirection(MemberAccessMainActivity.this) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
            }
            return true;
        }
        if (item.getItemId() == R.id.action_session) {
            startActivity(new Intent(this, SessionsActivity.class));
            if(URLCollections.whichDirection(MemberAccessMainActivity.this) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
            } else {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            }

            return true;
        }
        if (item.getItemId() == R.id.action_set_reader) {
            //startActivityForResult(new Intent(this, SetReader.class), HomeScreen.SET_READER);
            DatabaseHandler dbh = new DatabaseHandler(this);
            if (dbh.checkMemAccLogs()) {
                DisplayWarningForUnSyncedDialog();
            } else {
                int oldEntriesCount = dbh.isOldSyncedMemAccLogsExist();
                if (oldEntriesCount > 0) {
                    Log.d(TAG, "Old Entries Exists");
                    DisplayOldEntriesExistWarningDialog(oldEntriesCount);
                } else {
                    startActivityForResult(new Intent(this, SetReader.class), HomeScreen.SET_READER);
                }
            }
            //overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
            return true;
        }

        if (item.getItemId() == R.id.action_read_logs) {
            DatabaseHandler dbh = new DatabaseHandler(MemberAccessMainActivity.this);
            String logs = dbh.readLogsFile(MemberAccessMainActivity.this);
            Log.d(TAG, "Read File Data: "+logs);
            if (logs.length() > 0) {
                DisplayDeleteLogsFileInDialog(logs);
            } else {
                Boast.makeText(MemberAccessMainActivity.this, getString(R.string.no_logs_available), Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_member_access, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /*@Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.rlAllMembersAccess:
                if ((currentFragment instanceof AllowedMembersList)) {
                    //rlAllMemberAccess.setBackgroundColor(getResources().getColor(R.color.color_button_press_bg));
                    //rlAllowedMemberAccess.setBackgroundColor(getResources().getColor(R.color.color_button_bg));

                    viewAllMember.setBackgroundColor(getResources().getColor(android.R.color.white));
                    viewAllowMember.setBackgroundColor(getResources().getColor(R.color.color_button_bg));
                    removeAllFragments();
                    currentFragment = new AllowedMembersList();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.frameContainerMemAcc, currentFragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
                break;

            case R.id.rlAllowedMemberAccess:
                if ((currentFragment instanceof AllowedMembersList)) {
                    //rlAllMemberAccess.setBackgroundColor(getResources().getColor(R.color.color_button_bg));
                    //rlAllowedMemberAccess.setBackgroundColor(getResources().getColor(R.color.color_button_press_bg));

                    viewAllMember.setBackgroundColor(getResources().getColor(R.color.color_button_bg));
                    viewAllowMember.setBackgroundColor(getResources().getColor(android.R.color.white));
                    removeAllFragments();
                    currentFragment = new AllowedMembersList();
                    ft = fm.beginTransaction();
                    ft.replace(R.id.frameContainerMemAcc, currentFragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
                break;

            default :

                break;
        }
    }*/


    public void removeAllFragments () {
        int count = fm.getBackStackEntryCount();
        if (count > 0) {
            for (int i = 0; i < count; i++) {
                fm.popBackStack();
            }
        }
    }

    public static class MessageEvent {
        MemberAccessLogs memacclogs;
        int door_access;
        /* Additional fields if needed */

        public MessageEvent(MemberAccessLogs memacclogs) {
            //Log.d(TAG, "Message Event Constructor"+message);
            this.memacclogs = memacclogs;
        }

        public MessageEvent(Context mContext, int door_access) {
            //Log.d(TAG, "Message Event Constructor"+message);
            this.door_access = door_access;
        }
    }


    public void refreshFragment() {
        Fragment fragment = adapter.getCurrentFragment();
        if (fragment instanceof AllowedMembersList) {
            ((AllowedMembersList) fragment).fragmentBecameVisible(MemberAccessMainActivity.this);
        } else if (fragment instanceof AccessedMembersList) {
            ((AccessedMembersList) fragment).fragmentBecameVisible(MemberAccessMainActivity.this);
        } else if (fragment instanceof DeniedMembersList) {
            ((DeniedMembersList) fragment).fragmentBecameVisible(MemberAccessMainActivity.this);
        } else if (fragment instanceof PendingMembersList) {
            ((PendingMembersList) fragment).fragmentBecameVisible(MemberAccessMainActivity.this);
        }
    }

    public BottomSheetDialogFragment bottomSheetDialogFragment = null;
    public void showAccessGrantedDialog (MemberAccessLogs memacclogs) {

        // check - dismiss fragment readersAdapter BSD
        if (adapter != null) {
            Fragment fragment = adapter.getCurrentFragment();
            if (fragment instanceof AllowedMembersList) {
                if (((AllowedMembersList) fragment).adapter != null) {
                    /*if (((AllowedMembersList) fragment).adapter.bottomSheetDialogFragment != null) {
                        ((AllowedMembersList) fragment).adapter.bottomSheetDialogFragment.dismiss();
                    }*/
                }
            } else if (fragment instanceof AccessedMembersList) {
                if (((AccessedMembersList) fragment).adapter != null) {
                    if (((AccessedMembersList) fragment).adapter.bottomSheetDialogFragment != null) {
                        ((AccessedMembersList) fragment).adapter.bottomSheetDialogFragment.dismiss();
                    }
                }
            } else if (fragment instanceof DeniedMembersList) {
                if (((DeniedMembersList) fragment).adapter != null) {
                    if (((DeniedMembersList) fragment).adapter.bottomSheetDialogFragment != null) {
                        ((DeniedMembersList) fragment).adapter.bottomSheetDialogFragment.dismiss();
                    }
                }
            } else if (fragment instanceof PendingMembersList) {
                if (((PendingMembersList) fragment).adapter != null) {
                    /*if (((PendingMembersList) fragment).adapter.bottomSheetDialogFragment != null) {
                        ((PendingMembersList) fragment).adapter.bottomSheetDialogFragment.dismiss();
                    }*/
                }
            }
        }

        if (bottomSheetDialogFragment == null) {
            bottomSheetDialogFragment = new MemAccLogDetailFragment();
        } else {
            bottomSheetDialogFragment.dismiss();
            bottomSheetDialogFragment = new MemAccLogDetailFragment();
        }

        ((MemAccLogDetailFragment)bottomSheetDialogFragment).setMemAccData(memacclogs);
        ((MemAccLogDetailFragment)bottomSheetDialogFragment).isForMemberAccess("");
        ((MemAccLogDetailFragment)bottomSheetDialogFragment).isFromScan(true);
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());

        /*if (access_dialog == null) {
            access_dialog = new AlertDialog.Builder(MemberAccessMainActivity.this);
        }else {
            access_alert.dismiss();
            access_dialog = new AlertDialog.Builder(MemberAccessMainActivity.this);
        }

        LayoutInflater inflater = LayoutInflater.from(MemberAccessMainActivity.this);
        View dialogLayout = inflater.inflate(R.layout.dialog_access_granted, null);
        access_dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ImageView ivAGEmpPhoto = (ImageView) dialogLayout.findViewById(R.id.ivAGEmpPhoto);
        FloatingActionButton fabAG = (FloatingActionButton) dialogLayout.findViewById(R.id.fabAG);
        TextView tvAGLabel = (TextView) dialogLayout.findViewById(R.id.tvAGLabel);
        TextView tvAGEmpName = (TextView) dialogLayout.findViewById(R.id.tvAGEmpName);
        TextView tvAGEmpCardNo = (TextView) dialogLayout.findViewById(R.id.tvAGEmpCardNo);
        TextView tvAGPunchTime = (TextView) dialogLayout.findViewById(R.id.tvAGPunchTime);
        TextView tvAGReaderName = (TextView) dialogLayout.findViewById(R.id.tvAGReaderName);
        TextView tvAGSessionName = (TextView) dialogLayout.findViewById(R.id.tvAGSessionName);

        int access_grant = memacclogs.getMemAccLogsAccessGrant();
        if (access_grant == 0) {
            fabAG.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_green_dark)));
            fabAG.setImageResource(R.mipmap.ic_action_done);
            tvAGLabel.setText(Html.fromHtml(getResources().getString(R.string.access_grant_msg)));
            tvAGLabel.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
            ivAGEmpPhoto.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
        } else if (access_grant == 1) {
            fabAG.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_red_dark)));
            fabAG.setImageResource(R.mipmap.ic_close_app);
            tvAGLabel.setText(Html.fromHtml(getResources().getString(R.string.access_deny_msg)+getString(R.string.access_deny_no_door_access)));
            tvAGLabel.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            ivAGEmpPhoto.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
        } else if (access_grant == 2) {
            fabAG.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_red_dark)));
            fabAG.setImageResource(R.mipmap.ic_close_app);
            tvAGLabel.setText(Html.fromHtml(getResources().getString(R.string.access_deny_msg)+getString(R.string.access_deny_duplicate_access)));
            tvAGLabel.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            ivAGEmpPhoto.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
        }

        int emp_id = memacclogs.getMemAccLogsEmpId();
        String emp_photo ="";
        if (emp_id != -1) {
            DatabaseHandler db = new DatabaseHandler(MemberAccessMainActivity.this);
            emp_photo = db.getImagePathFromEmpId(MemberAccessMainActivity.this, emp_id);
            if (db.isEmployeeAlreadyExists(db.getWritableDatabase(), emp_id)) {
                Employee emp = db.getEmployeeFromEmpId(emp_id);
                if (emp != null) {
                    tvAGEmpName.setText(emp.getSLN_Employee() + " - " + emp.getEmployeeName());
                } else {
                    tvAGEmpName.setText(getResources().getString(R.string.unknown_str));
                }
            }
        } else {
            tvAGEmpName.setText(getResources().getString(R.string.unknown_str));
        }

        String punchTime = memacclogs.getMemAccLogsPunchDate();
        tvAGPunchTime.setVisibility(View.GONE);
        if (punchTime.length() > 0) {
            //tvAGPunchTime.setVisibility(View.VISIBLE);
            tvAGPunchTime.setText(getString(R.string.punch_time_label, punchTime));
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MemberAccessMainActivity.this);
        String reader_name = prefs.getString(PreferencesManager.KEY_SET_READER_NAME, "");
        String session_name = prefs.getString(PreferencesManager.KEY_SET_SESSION_NAME, "");
        tvAGReaderName.setText(getString(R.string.access_point_label, reader_name));
        tvAGSessionName.setText(getString(R.string.session_label, session_name));

        String card_no = memacclogs.getMemAccLogsCardNo();
        if (card_no.length() > 0) {
            tvAGEmpCardNo.setText(getString(R.string.card_number_label, card_no));
        }

        Glide
                .with(MemberAccessMainActivity.this)
                .load(emp_photo)
                //.placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_profile_pic)
                .crossFade()
                .into(ivAGEmpPhoto);

        access_dialog.setNegativeButton(getString(R.string.ok_label), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
            }
        });

        access_dialog.setOnDismissListener(onAccessDialogDismiss);

        access_alert = access_dialog.create();
        access_alert.show();*/
    }

    public DialogInterface.OnDismissListener onAccessDialogDismiss = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
            refreshFragment();
        }
    };


    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        //startScanCardsRunnable();
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        //stopScanCardsRunnable();
        super.onStop();

    }

    /**
     * Utility class to convert a byte array to a hexadecimal string.
     *
     * @param bytes Bytes to convert
     * @return String, containing hexadecimal representation.
     */
    public String ByteArrayToHexString(byte[] bytes, String typeStr) {
        final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[URLCollections.BYTE_LENGTH_FOR_MIFARE * 2];
        int v;

        // do not forgot to pass default values as second arguments other wise it will return empty string and condition will not be satisfied
        String mifare_reading_type = prefsMifare.getString(PreferencesManager.KEY_SET_MIFARE_READING_TYPE, PreferencesManager.DEFAULT_MIFARE_READING_TYPE_LSB);
        if (mifare_reading_type != null) {
            if (mifare_reading_type.length() != 0 ) {
                if (mifare_reading_type.equals(Config.LSB)) {
                    for (int j = 0; j < URLCollections.BYTE_LENGTH_FOR_MIFARE; j++) {
                        //for ( int j = 3; j ==0 ; j-- ) {
                        v = bytes[j] & 0xFF;
                        hexChars[(URLCollections.BYTE_LENGTH_FOR_MIFARE_TO_REVERSE - j) * 2] = hexArray[v >>> 4];
                        hexChars[(URLCollections.BYTE_LENGTH_FOR_MIFARE_TO_REVERSE - j) * 2 + 1] = hexArray[v & 0x0F];
                    }
                } else {
                    for (int j = 0; j < URLCollections.BYTE_LENGTH_FOR_MIFARE; j++) {
                        v = bytes[j] & 0xFF;
                        hexChars[j * 2] = hexArray[v >>> 4];
                        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
                    }
                }
            }
        }

        String str = new String(hexChars);
        if (typeStr.equals(Config.DESFIRE) && str.length() > 8) {
            str = str.substring(0, 7);
            return str;
        } else {
            return str;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (CpcOs.isCone()) {
            //stopScanCardsRunnable();
            mPowerMgmt.setPower(PowerMgmt.PeripheralTypesCone.RfidSc, PowerMgmt.ManufacturersCone.Hid, PowerMgmt.ModelsCone.IClassProx, interfaceCone, false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CpcOs.isCone()  && !threadRunning) {
            startScanCardsRunnable();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (CpcOs.isCone()) {
            stopScanCardsRunnable();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        Log.d(TAG, "OnMessageEvent");
        if (event.memacclogs instanceof MemberAccessLogs) {
            DatabaseHandler db = new DatabaseHandler(this);
            db.saveMemAccPunchLogsData(event.memacclogs);
            showAccessGrantedDialog(event.memacclogs);
        } else if (event.door_access == HandlerHelper.MEM_ACC_NO_DOOR_ACCESS) {
            //Boast.makeText(MemberAccessMainActivity.this, "Not having door access!!!", Toast.LENGTH_SHORT).show();
        } else if (event.door_access == HandlerHelper.MEM_ACC_INVALID_SESSION) {
            Boast.makeText(MemberAccessMainActivity.this, getString(R.string.session_not_valid), Toast.LENGTH_SHORT).show();
        }
    }

    //to display progress dialog while any async task has been called from the fragment so we can freez the screen
    ProgressDialog pDialog;
    public void setProgress (boolean progressStatus) {
        if (progressStatus) {
            if (pDialog == null) {
                pDialog = new ProgressDialog(MemberAccessMainActivity.this, R.style.MyTheme);
            }
            pDialog.setCancelable(false);
            pDialog.show();
        } else {
            if (pDialog != null) {
                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }
            }
        }
    }

    // Read the logs and display file in Dialog
    public void DisplayDeleteLogsFileInDialog (String logs) {
        if (access_dialog == null) {
            access_dialog = new AlertDialog.Builder(MemberAccessMainActivity.this);
        }else {
            access_alert.dismiss();
            access_dialog = new AlertDialog.Builder(MemberAccessMainActivity.this);
        }

        LayoutInflater inflater = LayoutInflater.from(MemberAccessMainActivity.this);
        View dialogLayout = inflater.inflate(R.layout.dialog_old_mem_acc_logs_exists, null);
        access_dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        TextView tvWarningMessage = (TextView) dialogLayout.findViewById(R.id.warningMsg);
        tvWarningMessage.setText(logs);
        //tvWarningMessage.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
        access_dialog.setPositiveButton(getString(R.string.okay_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                access_alert.dismiss();
            }
        });


        access_alert = access_dialog.create();
        access_alert.show();

    }

    // Display the dialog for the UnSync Dialog
    public void DisplayWarningForUnSyncedDialog () {
        if (access_dialog == null) {
            access_dialog = new AlertDialog.Builder(MemberAccessMainActivity.this);
        }else {
            access_alert.dismiss();
            access_dialog = new AlertDialog.Builder(MemberAccessMainActivity.this);
        }

        LayoutInflater inflater = LayoutInflater.from(MemberAccessMainActivity.this);
        View dialogLayout = inflater.inflate(R.layout.dialog_old_mem_acc_logs_exists, null);
        access_dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        TextView tvWarningMessage = (TextView) dialogLayout.findViewById(R.id.warningMsg);
        tvWarningMessage.setText(getApplicationContext().getResources().getString(R.string.mem_acc_not_sync_logs_warning));

        access_dialog.setPositiveButton(R.string.okay_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });


        access_alert = access_dialog.create();
        access_alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                DatabaseHandler dbh = new DatabaseHandler(MemberAccessMainActivity.this);
                int oldEntriesCount = dbh.isOldSyncedMemAccLogsExist();
                if (oldEntriesCount > 0) {
                    Log.d(TAG, "Old Entries Exists");
                    DisplayOldEntriesExistWarningDialog(oldEntriesCount);
                } else {
                    startActivityForResult(new Intent(MemberAccessMainActivity.this, SetReader.class), HomeScreen.SET_READER);
                }
            }
        });
        access_alert.setCancelable(false);
        access_alert.show();

    }

    public void DisplayOldEntriesExistWarningDialog (final int totalRecords) {
        if (access_dialog == null) {
            access_dialog = new AlertDialog.Builder(MemberAccessMainActivity.this);
        }else {
            access_alert.dismiss();
            access_dialog = new AlertDialog.Builder(MemberAccessMainActivity.this);
        }

        LayoutInflater inflater = LayoutInflater.from(MemberAccessMainActivity.this);
        View dialogLayout = inflater.inflate(R.layout.dialog_old_mem_acc_logs_exists, null);
        access_dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        TextView tvAGLabel = (TextView) dialogLayout.findViewById(R.id.tvAGLabel);

        access_dialog.setPositiveButton(R.string.yes_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(TAG, "Proceed for deleting");
                new DeleteOldmemAccLogs(totalRecords).execute();
            }
        });
        access_dialog.setNegativeButton(R.string.no_label, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                startActivityForResult(new Intent(MemberAccessMainActivity.this, SetReader.class), HomeScreen.SET_READER);
            }
        });

        //access_dialog.setOnDismissListener(onOldLogsDialogDismiss);

        access_alert = access_dialog.create();
        access_alert.show();
    }

    public DialogInterface.OnDismissListener onOldLogsDialogDismiss = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
            //refreshFragment();

        }
    };

    public class DeleteOldmemAccLogs extends AsyncTask<Void, Void, Void> {
        ProgressDialog pDialogDelete;
        DatabaseHandler dbh;
        int totalRecords;

        public DeleteOldmemAccLogs (int totalRecords) {
            this.totalRecords = totalRecords;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialogDelete = new ProgressDialog(MemberAccessMainActivity.this, R.style.MyTheme);
            pDialogDelete.setCancelable(false);
            pDialogDelete.show();

            dbh = new DatabaseHandler(MemberAccessMainActivity.this);

        }

        @Override
        protected Void doInBackground(Void... params) {

            dbh.deleteOldMemAccLogs();
            // to save the logs of deleted entries
            String str = getString(R.string.total_deleted_records_label, ""+totalRecords);
            dbh.saveLogsForDeletedMemAccLogs(MemberAccessMainActivity.this, str);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pDialogDelete.isShowing())
                pDialogDelete.dismiss();
            startActivityForResult(new Intent(MemberAccessMainActivity.this, SetReader.class), HomeScreen.SET_READER);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == HomeScreen.SET_READER && resultCode == HomeScreen.SET_READER) {
            refreshFragment();
        }
    }
}