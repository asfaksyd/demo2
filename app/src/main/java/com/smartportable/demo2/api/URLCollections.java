package com.smartportable.demo2.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.nfc.tech.MifareClassic;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import com.smartportable.demo2.helper.PreferencesManager;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.Locale;

import fr.coppernic.cpcframework.cpcpowermgmt.cone.PowerMgmt;

/**
 * Created by Ananth on 12/29/2016.
 */

public class URLCollections {
    public static int READER_FLAGS =
            NfcAdapter.FLAG_READER_NFC_A | NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK;

    public static String C5_MODEL_NAME = "C-five(WOS)";
    public static byte[] MIFARE_AUTH_KEY = MifareClassic.KEY_DEFAULT;
    public static int SECTOR_COUNT_FOR_MIFARE = 1;
    public static int BYTE_COUNT_FOR_MIFARE = 1;
    public static int BYTE_LENGTH_FOR_MIFARE = 4;
    public static int BYTE_LENGTH_FOR_MIFARE_TO_REVERSE = 3;

    public static String READER_ENABLED_DEVICE_MODEL = "C-One";

    public static String DEVICE_CONE = "C-One";
    public static String DEVICE_CONE_EID = "C-One e-ID";
    public static String DEVICE_TYPE = DEVICE_CONE;//DEVICE_CONE;//DEVICE_CONE_EID;

    public static String LANGUAGE_CODE_ENGLISH = "en";
    public static String LANGUAGE_CODE_ARABIC = "ar";
    public static int DEV_DIR_LTR = 0;
    public static int DEV_DIR_RTL = 1;

    public static int CARD_MODE = 0;

    public static String IP_ADDRR = "";
    public static String BASE_URL = "http://smartcampus.wiseportable.com";

    public static String CATEGORY_REQUEST_NAME = "Catrgories/GetCatrgories";
    public static String CATEGORIES_URL = BASE_URL + CATEGORY_REQUEST_NAME;

    public static String MEM_ACC_TYPE_ALLOWED = "ALLOWED";
    public static String MEM_ACC_TYPE_ACCESSED = "ACCESSED";
    public static String MEM_ACC_TYPE_DENIED = "DENIED";
    public static String MEM_ACC_TYPE_PENDING = "PENDING";


    public static PowerMgmt.InterfacesCone getInterfaceConeType() {
        if (DEVICE_TYPE.equals(DEVICE_CONE)) {
            return PowerMgmt.InterfacesCone.ExpansionPort;
        }else {
            return PowerMgmt.InterfacesCone.UsbGpioPort;
        }
    }

    public static int isDeviceHaveNFC (Context context) {
        int nfcSt = 0; // NFC is not available
        NfcManager manager = (NfcManager) context.getSystemService(Context.NFC_SERVICE);
        NfcAdapter adapter = manager.getDefaultAdapter();
        if (adapter != null) {
            if(adapter.isEnabled()) {
                nfcSt = 1; // NFC is avaialble and ON
            } else {
                nfcSt = 2; // NFC is avaialble and OFF
            }
            //Yes NFC available
        }else{
            nfcSt = 0; // NFC is not avaialbel
            //Your device doesn't support NFC
        }
        return nfcSt;
    }

    public static boolean isC5() {
        if (Build.MODEL.equals(C5_MODEL_NAME)) {
            return true;
        } else {
            return false;
        }
    }

    public static String getBaseURLFromPreference (Context mContext) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        String url = prefs.getString(PreferencesManager.KEY_SERVER_URL, "");
        return url;
    }

    public static String getHostAddressFromURL (String url) {
        String host = "";
        try {
            //InetAddress address = InetAddress.getByName(new URL(url).getHost());
            host = new URL(url).getHost();
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }
        return host;
    }
    public static boolean isValidUrl (String url) {
        return android.util.Patterns.WEB_URL.matcher(url).matches();//android.webkit.URLUtil.isValidUrl(url) && android.webkit.URLUtil.isHttpsUrl(url);
    }

    // to check which local / language is set for device right now and based on that decide the direction
    public static int whichDirection(Context mContext) {
        /*SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        String lang = prefs.getString(PreferencesManager.KEY_CHANGE_LANG, mContext.getResources().getString(R.string.english_lang_label));
        Log.d("URLCollections", "Lang Code: "+lang);
        if (lang.equals(mContext.getResources().getString(R.string.arabic_lang_label))) {
            return DEV_DIR_RTL;
        }
        return DEV_DIR_LTR;*/

        String lang = Locale.getDefault().getLanguage();
        Log.d("URLCollections", "Lang Code: "+lang);
        if (lang.equals(URLCollections.LANGUAGE_CODE_ARABIC)) {
            return DEV_DIR_RTL;
        }
        return DEV_DIR_LTR;
    }

    public static NumberFormat getNumberFormat () {
        NumberFormat nf = NumberFormat.getInstance(new Locale("en"));
        String lang = Locale.getDefault().getLanguage();
        Log.d("URLCollections", "Lang Code: "+lang);
        if (lang.equals(URLCollections.LANGUAGE_CODE_ARABIC)) {
            nf = NumberFormat.getInstance(new Locale("ar"));
        }
        return nf;
    }

}
