package com.smartportable.demo2.api;

import com.smartportable.demo2.helper.Areas;
import com.smartportable.demo2.helper.Categories;
import com.smartportable.demo2.helper.Employee;
import com.smartportable.demo2.helper.MusteringReaders;
import com.smartportable.demo2.helper.PunchLogs;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;


public interface ApiInterface {
    //@GET("movie/top_rated")
    //@GET("movie/upcoming")
    @PUT("PunchLogs/GetReadersList")
    Call<PunchLogs> getReadersList(@Query("MusteringId") String mustId);

    @GET("MusteringTypes/GetMusteringTypes")
    Call<Categories> getAllCategories();

    @GET("Areas/GetAreas")
    Call<Areas> getAllAreas();

    @GET("Employee/GetEmployees")
    Call<Employee> getAllEmployees(@Query("Mode") int mode, @Query("send_Photo") int photo_type, @Query("TimeStamp") String timStamp, @Query("PageNumber") int pageNumber, @Query("EmployeeId") String empId, @Query("CardMode") int cardMode);

    @GET("PunchLogs/GetEmployeeTrackPunchLoaction")
    Call<PunchLogs> getEmployeesLocation(); //@Query("Mode") int mode, @Query("EmployeeID") int timStamp

    @POST("PunchLogs/SendPunchData")
    Call<ResponseBody> sendPunchData(@Header("strPunchInfo") String puncInfo);

    @POST("MusteringTypes/GetMusteringReaders")
    Call<MusteringReaders> getMusteringReaders(@Query("PageNumber") int pageNumber);

}
