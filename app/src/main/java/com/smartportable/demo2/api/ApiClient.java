package com.smartportable.demo2.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.smartportable.demo2.R;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {
    public static final String BASE_URL = URLCollections.BASE_URL;//"http://api.themoviedb.org/3/";
    private static Retrofit retrofit = null;
    static Context mContext;
    private static final int CONNECT_TIMEOUT = 60;
    private static final int READ_TIMEOUT = 120;

    public static Retrofit getClient() {
        //if (retrofit==null) {
            try {
                String url = URLCollections.getBaseURLFromPreference(mContext);
                Log.d("ApiClient Base Url: ", url);
                OkHttpClient client = new OkHttpClient.Builder()
                        .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                        .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS).build();
                retrofit = new Retrofit.Builder()
                        .baseUrl(URLCollections.getBaseURLFromPreference(mContext))
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client)
                        .build();
            } catch (IllegalArgumentException exception) {
                Toast.makeText(mContext, R.string.url_not_configure, Toast.LENGTH_SHORT).show();
            }
        //}
        return retrofit;
    }

    public ApiClient (Context mContext) {
        this.mContext = mContext;
    }
}
