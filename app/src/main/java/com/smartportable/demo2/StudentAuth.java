package com.smartportable.demo2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.smartportable.demo2.activities.AuthActivity;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.fragments.StudentDetailFragment;
import com.smartportable.demo2.helper.Config;
import com.smartportable.demo2.helper.GlobalClass;
import com.smartportable.demo2.models.Student;

import java.util.ArrayList;

import javax.annotation.Nullable;

import androidx.appcompat.app.AppCompatActivity;
import fr.coppernic.sdk.utils.helpers.CpcOs;

import static com.smartportable.demo2.utils.KeysKt.AUTH_TYPE;
import static com.smartportable.demo2.utils.KeysKt.READ_BARCODE;
import static com.smartportable.demo2.utils.KeysKt.READ_CARD_BY_SEARCH;
import static com.smartportable.demo2.utils.KeysKt.READ_CARD_NUMBER;
import static com.smartportable.demo2.utils.KeysKt.READ_CARD_NUMBER_OFF;
import static com.smartportable.demo2.utils.KeysKt.READ_CARD_OFFLINE;

/**
 * Created by Ananth on 5/3/2017.
 */

public class StudentAuth extends AppCompatActivity implements View.OnClickListener, NfcAdapter.ReaderCallback{

    Animation set, set1;
    ImageView ivCfive, ivCard;
    RelativeLayout rlStAuth, rlReadCardData, rlOffReadCardNo, rlReadCardNo, rlBarcode, rlSearch;
    private static String TAG = StudentAuth.class.getCanonicalName();
    private Snackbar snackbarNFC;
    private String cardData;
    SharedPreferences prefsMifare;
    Switch swOnOff;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_auth);

        ivCfive = (ImageView) findViewById(R.id.ivCfive);
        ivCard = (ImageView) findViewById(R.id.ivCard);

        rlStAuth = (RelativeLayout) findViewById(R.id.rlStAuth);
        rlReadCardData = (RelativeLayout) findViewById(R.id.rlReadCardData);
        rlOffReadCardNo = (RelativeLayout) findViewById(R.id.rlOffReadCardNo);
        rlReadCardNo = (RelativeLayout) findViewById(R.id.rlReadCardNo);
        rlBarcode = (RelativeLayout) findViewById(R.id.rlBarcode);
        rlSearch = (RelativeLayout) findViewById(R.id.rlSearch);

        rlReadCardData.setOnClickListener(this);
        rlOffReadCardNo.setOnClickListener(this);
        rlReadCardNo.setOnClickListener(this);
        rlBarcode.setOnClickListener(this);
        rlSearch.setOnClickListener(this);

        swOnOff = (Switch)findViewById(R.id.swOnOff);
        hideShowViews(swOnOff.isChecked());
        swOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                hideShowViews(swOnOff.isChecked());
            }
        });

        prefsMifare = PreferenceManager.getDefaultSharedPreferences(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int nfcStatus = URLCollections.isDeviceHaveNFC(this);
            if (CpcOs.isC5() && !CpcOs.isCone()) {
                //configureLedWidget();
                if (nfcStatus == 1) {
                    enableReaderMode();
                } else if (nfcStatus == 2) {
                    //Boast.makeText(this, "Please ON your NFC from settings", Toast.LENGTH_LONG).show();
                    snackbarNFC = Snackbar
                            .make(rlStAuth, getResources().getString(com.smartportable.demo2.R.string.nfc_on_msg), Snackbar.LENGTH_INDEFINITE);
                    snackbarNFC.setAction(getResources().getString(com.smartportable.demo2.R.string.nfc_on_action), mOnSnackBarONClickListener);
                    snackbarNFC.setActionTextColor(Color.GREEN);
                    View snackbarView = snackbarNFC.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbarNFC.show();
                } else {
                    //Boast.makeText(this, "Your device doesn't have NFC", Toast.LENGTH_LONG).show();
                    snackbarNFC = Snackbar
                            .make(rlStAuth, getResources().getString(com.smartportable.demo2.R.string.nfc_not_available), Snackbar.LENGTH_LONG);
                    //snackbarNFC.setAction(getResources().getString(R.string.nfc_on_action), mOnSnackBarONClickListener);
                    //snackbarNFC.setActionTextColor(Color.GREEN);
                    View snackbarView = snackbarNFC.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbarNFC.show();
                }
                /*Intent intent = new Intent(this, ScanCardServiceBroadcastReceiver.class);
                alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
                alarmMgr.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), Config.SCAN_CARD_SERVICE_INTERVAL, alarmIntent);*/
            } else {
                fr.coppernic.sdk.utils.debug.Log.d(TAG, "Other device");
                if (nfcStatus == 1) {
                    enableReaderMode();
                } else if (nfcStatus == 2) {
                    //Boast.makeText(this, "Please ON your NFC from settings", Toast.LENGTH_LONG).show();
                    snackbarNFC = Snackbar.make(rlStAuth, getResources().getString(com.smartportable.demo2.R.string.nfc_on_msg), Snackbar.LENGTH_INDEFINITE);
                    snackbarNFC.setAction(getResources().getString(com.smartportable.demo2.R.string.nfc_on_action), mOnSnackBarONClickListener);
                    snackbarNFC.setActionTextColor(Color.GREEN);
                    View snackbarView = snackbarNFC.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbarNFC.show();
                } else {
                    //Boast.makeText(this, "Your device doesn't have NFC", Toast.LENGTH_LONG).show();
                    snackbarNFC = Snackbar
                            .make(rlStAuth, getResources().getString(com.smartportable.demo2.R.string.nfc_not_available), Snackbar.LENGTH_LONG);
                    //snackbarNFC.setAction(getResources().getString(R.string.nfc_on_action), mOnSnackBarONClickListener);
                    //snackbarNFC.setActionTextColor(Color.GREEN);
                    View snackbarView = snackbarNFC.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbarNFC.show();
                }
            }
        }


    }

    public void hideShowViews(boolean b) {
        if (b) {
            swOnOff.setText("(ONLINE)   ");

            rlReadCardData.setVisibility(View.GONE);
            rlOffReadCardNo.setVisibility(View.GONE);
            rlReadCardNo.setVisibility(View.VISIBLE);
            rlBarcode.setVisibility(View.VISIBLE);
            rlSearch.setVisibility(View.VISIBLE);
        }
        else {
            swOnOff.setText("(OFFLINE)   ");

            rlReadCardData.setVisibility(View.GONE);
            rlOffReadCardNo.setVisibility(View.VISIBLE);
            rlReadCardNo.setVisibility(View.GONE);
            rlBarcode.setVisibility(View.GONE);
            rlSearch.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlReadCardData:
                Intent read_card_data_auth_intent = new Intent(this, AuthActivity.class);
                read_card_data_auth_intent.putExtra(AUTH_TYPE, READ_CARD_OFFLINE);
                startActivity(read_card_data_auth_intent);
                break;

            case R.id.rlOffReadCardNo:
                Intent read_card_no_auth_intent_off = new Intent(this, AuthActivity.class);
                read_card_no_auth_intent_off.putExtra(AUTH_TYPE, READ_CARD_NUMBER_OFF);
                startActivity(read_card_no_auth_intent_off);
                break;

            case R.id.rlReadCardNo:
                Intent read_card_no_auth_intent = new Intent(this, AuthActivity.class);
                read_card_no_auth_intent.putExtra(AUTH_TYPE, READ_CARD_NUMBER);
                startActivity(read_card_no_auth_intent);
                break;

            case R.id.rlBarcode:
                //Toast.makeText(this, "Barcode", Toast.LENGTH_SHORT).show();

                Intent barcode_auth_intent = new Intent(this, AuthActivity.class);
                barcode_auth_intent.putExtra(AUTH_TYPE, READ_BARCODE);
                startActivity(barcode_auth_intent);

                break;
            case R.id.rlSearch:
                //Toast.makeText(this, "Search", Toast.LENGTH_SHORT).show();
                Intent search_auth_intent = new Intent(this, AuthActivity.class);
                search_auth_intent.putExtra(AUTH_TYPE, READ_CARD_BY_SEARCH);
                startActivity(search_auth_intent);

                break;
        }
        if (URLCollections.whichDirection(StudentAuth.this) == URLCollections.DEV_DIR_RTL) {
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        } else {
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        set =  AnimationUtils.loadAnimation(this, R.anim.anim_slide_in_middle_from_left);
        set1 = AnimationUtils.loadAnimation(this, R.anim.anim_slide_in_middle_from_right);
        ivCfive.startAnimation(set);
        ivCard.startAnimation(set1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            //setResult(SyncActivity.REQUEST_SYNC_EMPLOYEES);
            StudentAuth.this.finish();
            if(URLCollections.whichDirection(StudentAuth.this) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public View.OnClickListener mOnSnackBarONClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            android.util.Log.d(TAG, "SnackBar On Click");
            Intent intent = new Intent(Settings.ACTION_NFC_SETTINGS);
            startActivityForResult(intent, 11);
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(URLCollections.whichDirection(StudentAuth.this) == URLCollections.DEV_DIR_RTL) {
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
        } else {
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(!set.hasEnded()) {
            set.cancel();
            set1.cancel();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (CpcOs.isC5()) {
            enableReaderMode();

            int nfcst = URLCollections.isDeviceHaveNFC(StudentAuth.this);
            if (nfcst == 1) {

            } else if (nfcst == 2) {
                snackbarNFC = Snackbar
                        .make(rlStAuth, getResources().getString(com.smartportable.demo2.R.string.nfc_on_msg), Snackbar.LENGTH_INDEFINITE);
                snackbarNFC.setAction(getResources().getString(com.smartportable.demo2.R.string.nfc_on_action), mOnSnackBarONClickListener);
                snackbarNFC.setActionTextColor(Color.GREEN);
                View snackbarView = snackbarNFC.getView();
                snackbarView.setBackgroundColor(Color.DKGRAY);
                TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbarNFC.show();
            } else {
                snackbarNFC = Snackbar
                        .make(rlStAuth, getResources().getString(com.smartportable.demo2.R.string.nfc_not_available), Snackbar.LENGTH_INDEFINITE);
                //snackbarNFC.setAction(getResources().getString(R.string.nfc_on_action), mOnSnackBarONClickListener);
                //snackbarNFC.setActionTextColor(Color.GREEN);
                View snackbarView = snackbarNFC.getView();
                snackbarView.setBackgroundColor(Color.DKGRAY);
                TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbarNFC.show();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        disableReaderMode();
    }

    // Enable NFC reader mode
    private void enableReaderMode() {
        Log.i(TAG, "Enabling reader mode");
        NfcAdapter nfc = NfcAdapter.getDefaultAdapter(this);
        if (nfc != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                nfc.enableReaderMode(this, this, NfcAdapter.FLAG_READER_NFC_A | NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK, null);
            }
        }
    }

    // Disable NFC reader Mode
    private void disableReaderMode() {
        Log.i(TAG, "Disabling reader mode");
        NfcAdapter nfc = NfcAdapter.getDefaultAdapter(this);
        if (nfc != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                nfc.disableReaderMode(this);
            }
        }
    }

    @Override
    public void onTagDiscovered(Tag tag) {
        byte[] id = tag.getId();
        Log.d(TAG, "TAG ID:" +id);
        int total_length = id.length-1;
        Log.d("TAG", "tag "+id+"tag length"+total_length);

        // Read card first with tag based on authentication key
        final MifareClassic mifareClassic = MifareClassic.get(tag);
        final IsoDep isoDep = IsoDep.get(tag);
        Log.d(TAG, "Mifare CLassic : " + mifareClassic);
        if (mifareClassic != null) {
            try {
                mifareClassic.connect();
                fr.coppernic.sdk.utils.debug.Log.d(TAG, "Transceive Length: " + mifareClassic.getMaxTransceiveLength());
                fr.coppernic.sdk.utils.debug.Log.d(TAG, "Selector count: " + mifareClassic.getSectorCount());
                int blockCount = mifareClassic.getBlockCount();
                fr.coppernic.sdk.utils.debug.Log.d(TAG, "SectorToBlock: " + mifareClassic.sectorToBlock(blockCount));

                boolean auth = false;
                // 5.2) and get the number of sectors this card has..and loop thru these sectors
                int secCount = mifareClassic.getSectorCount();
                int bCount = 0;
                int bIndex = 0;
                for (int j = 0; j < URLCollections.SECTOR_COUNT_FOR_MIFARE; j++) {
                    // 6.1) authenticate the sector
                    auth = mifareClassic.authenticateSectorWithKeyA(j, URLCollections.MIFARE_AUTH_KEY);
                    if (auth) {
                        // 6.2) In each sector - get the block count
                        bCount = mifareClassic.getBlockCountInSector(j);
                        bIndex = 0;
                        for (int i = 0; i < URLCollections.BYTE_COUNT_FOR_MIFARE; i++) {
                            bIndex = mifareClassic.sectorToBlock(j);
                            // 6.3) Read the block
                            byte[] data = mifareClassic.readBlock(bIndex);
                            // 7) Convert the data into a string from Hex format.
                            cardData = GlobalClass.getInstance().ByteArrayToHexString(data, Config.MIFARE, prefsMifare);
                            android.util.Log.d(TAG, cardData);//, data.length));
                            bIndex++;
                        }
                    } else { // Authentication failed - Handle it using directly reading TAG
                        android.util.Log.d(TAG, "Authentication Failed!");
                        cardData = GlobalClass.getInstance().ByteArrayToHexString(id, Config.MIFARE, prefsMifare);

                    }
                }
                if (cardData != null && cardData.length() > 0 && GlobalClass.getInstance().checkAlphNumericString(cardData)) {
                    Log.d(TAG, "Card Number: "+cardData);
                }else {
                    Log.d(TAG, "CardNumber null");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }/* else if (isoDep != null) {
            android.util.Log.d(TAG, "Mifare DESFire Card");
            cardData = ByteArrayToHexString(id, Config.DESFIRE);
            android.util.Log.d(TAG, "Card Number: "+cardData);

            if (cardData != null && cardData.length() > 0 && checkAlphNumericString(cardData)) {
                Log.d(TAG, "Card Number: "+cardData);
            }else {
                Log.d(TAG, "CardNumber null");
            }
        }*/

        if (cardData != null) {
            if (cardData.length() > 0) {
                ArrayList<Student> stList = GlobalClass.getInstance().searchStudentRecord(cardData);
                if (stList.size() > 0 & stList.size() == 1) {
                    BottomSheetDialogFragment bottomSheetDialogFragment = new StudentDetailFragment();
                    //((StudentDetailFragment) bottomSheetDialogFragment).setStudentDetail(stList.get(0));
                    //((EmployeeDetailFragment)bottomSheetDialogFragment).isForMemberAccess(forMemAccess);
                    bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                }
            }
        }
    }
}
