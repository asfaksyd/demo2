package com.smartportable.demo2;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.Boast;
import com.smartportable.demo2.helper.Config;
import com.smartportable.demo2.helper.Sessions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by Ananth on 3/28/2017.
 */

public class AddSessionActivity extends AppCompatActivity {
    EditText etSessionId, etSessionName, etSessionDate, etSessionStartTime, etSessionEndTime;
    Calendar myCalendar, calStartTime, calEndTime;
    DatabaseHandler db;
    long session_date_mili, session_start_time_mili, session_end_time_mili;
    DatePickerDialog dialog;
    TimePickerDialog startTimeDialog, endTimeDialog;
    int pos = -1;
    String org_session_name = "";
    ActionBar ab;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.smartportable.demo2.R.layout.activity_add_session);

        ab = getSupportActionBar();

        etSessionId = (EditText) findViewById(com.smartportable.demo2.R.id.etSessionId);
        etSessionName = (EditText) findViewById(com.smartportable.demo2.R.id.etSessionName);
        etSessionDate = (EditText) findViewById(com.smartportable.demo2.R.id.etSessionDate);
        etSessionStartTime = (EditText) findViewById(com.smartportable.demo2.R.id.etSessionStartTime);
        etSessionEndTime = (EditText) findViewById(com.smartportable.demo2.R.id.etSessionEndTime);

        myCalendar = Calendar.getInstance();
        db = new DatabaseHandler(this);

        try {
            pos = getIntent().getExtras().getInt(SessionsActivity.SESSION_POSITION);
            if (pos != -1) {
                ab.setTitle(com.smartportable.demo2.R.string.edit_session_label);
                new LoadExistingSession().execute(pos);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        etSessionDate.setVisibility(View.GONE);
        etSessionDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (session_date_mili > 0.0)
                    myCalendar.setTimeInMillis(session_date_mili);

                dialog = new DatePickerDialog(AddSessionActivity.this, datePicker, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                if (session_date_mili == 0.0)
                    dialog.getDatePicker().setMinDate(System.currentTimeMillis());

                dialog.show();
            }
        });
        etSessionStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calStartTime = Calendar.getInstance();
                if (session_start_time_mili > 0.0 ) {
                    calStartTime.setTimeInMillis(session_start_time_mili);
                }
                startTimeDialog = new TimePickerDialog(AddSessionActivity.this, startTimePicker, calStartTime
                        .get(Calendar.HOUR_OF_DAY), calStartTime.get(Calendar.MINUTE), true);
                startTimeDialog.show();
            }
        });

        etSessionEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calEndTime = Calendar.getInstance();
                if (session_end_time_mili > 0.0 ) {
                    calEndTime.setTimeInMillis(session_end_time_mili);
                }
                endTimeDialog = new TimePickerDialog(AddSessionActivity.this, endTimePicker, calEndTime
                        .get(Calendar.HOUR_OF_DAY), calEndTime.get(Calendar.MINUTE), true);
                endTimeDialog.show();
            }
        });

    }

    DatePickerDialog.OnDateSetListener datePicker = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateSessionDateLabel();
        }

    };

    private void updateSessionDateLabel() {

        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Config.DATETIME_FORMAT_LOCAL);
        session_date_mili = myCalendar.getTimeInMillis();
        etSessionDate.setText(sdf.format(myCalendar.getTime()));
    }

    TimePickerDialog.OnTimeSetListener startTimePicker = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            calStartTime = Calendar.getInstance();
            calStartTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calStartTime.set(Calendar.MINUTE, minute);

            String myFormat = "HH:mm"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat);//, Config.DATETIME_FORMAT_LOCAL);
            //sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            session_start_time_mili = calStartTime.getTimeInMillis();
            etSessionStartTime.setText(sdf.format(calStartTime.getTime()));
        }
    };

    TimePickerDialog.OnTimeSetListener endTimePicker = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            calEndTime = Calendar.getInstance();
            calEndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calEndTime.set(Calendar.MINUTE, minute);
            calEndTime.set(Calendar.AM_PM, calEndTime.get(Calendar.AM_PM));

            String myFormat = "HH:mm"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat);//, Config.DATETIME_FORMAT_LOCAL);
            //sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            session_end_time_mili = calEndTime.getTimeInMillis();
            etSessionEndTime.setText(sdf.format(calEndTime.getTime()));
        }
    };

    public boolean checkValidation() {
        String session_id = etSessionId.getText().toString();
        String session_name = etSessionName.getText().toString();
        String session_date = etSessionDate.getText().toString();
        String session_start_time = etSessionStartTime.getText().toString();
        String session_end_time = etSessionEndTime.getText().toString();

        if (session_id.length() == 0) {
            etSessionId.setError(getString(com.smartportable.demo2.R.string.session_id_require));
            return false;
        } else if (session_name.length() == 0) {
            etSessionName.setError(getString(com.smartportable.demo2.R.string.session_name_require));
            return false;
        } else if (db.checkSessionDuplicate(session_name.trim()) && pos == -1) {
            etSessionName.setError(getString(com.smartportable.demo2.R.string.session_name_not_be_same));
            return false;
        } /*else if (session_date_mili.length() == 0) {
            etSessionName.setError(null);
            Boast.makeText(this, "Please select session date", Toast.LENGTH_LONG).show();
            return false;
        } */else if (session_start_time.length() == 0) {
            Boast.makeText(this, getString(com.smartportable.demo2.R.string.sel_session_start_time), Toast.LENGTH_LONG).show();
            return false;
        } else if (session_end_time.length() == 0) {
            Boast.makeText(this, getString(com.smartportable.demo2.R.string.sel_session_end_time), Toast.LENGTH_LONG).show();
            return false;
        } else if (!compareStartEndTime(session_start_time_mili, session_end_time_mili)) {
            Boast.makeText(this, getString(com.smartportable.demo2.R.string.sel_proper_time_interval), Toast.LENGTH_LONG).show();
            return false;
        } else if (pos != -1) {
            if (org_session_name.length() > 0){
                if (session_name.equals(org_session_name)) {
                    return true;
                } else {
                    if (db.checkSessionDuplicate(session_name.trim())) {
                        etSessionName.setError(getString(com.smartportable.demo2.R.string.session_name_not_be_same));
                        return false;
                    } else {
                        return true;
                    }
                }
            }
            return false;
        } else {
            return true;
        }
    }

    public boolean compareStartEndTime (long startTime, long endTime) {
        String selectedDate = etSessionDate.getText().toString().trim();
        try {
            Calendar calStart = Calendar.getInstance();
            calStart.setTimeInMillis(startTime);
            Calendar calEnd = Calendar.getInstance();
            calEnd.setTimeInMillis(endTime);

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Config.DATETIME_FORMAT_LOCAL);
            Date startDate = sdf.parse(sdf.format(calStart.getTime()));
            Date endDate = sdf.parse(sdf.format(calEnd.getTime()));

            int compareDate = startDate.compareTo(endDate);
            Log.d("compareDate", ""+compareDate);
            Log.d("compare boolean", ""+startDate.after(endDate));
            if (compareDate < 0) {
                // end date is greater then start date
                return true;
            } else {
                // end date is
                // 0 - end date is equals to start date
                // -1 - end date is less then start date
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.smartportable.demo2.R.menu.menu_add_session, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == com.smartportable.demo2.R.id.action_save_session) {
            Log.d("SessionActivity", "Save Session");
            if (checkValidation()) {
                new SaveSessionActivity().execute();

                return true;
            } else {
                Log.d("AddSession", "Validation failed!");
            }
        }

        if (item.getItemId() == android.R.id.home) {
            Log.d("SessionActivity", "Home Button Click");
            AddSessionActivity.this.finish();
            if (URLCollections.whichDirection(AddSessionActivity.this) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(com.smartportable.demo2.R.anim.anim_slide_in_left,
                        com.smartportable.demo2.R.anim.anim_slide_out_left);
            } else {
                overridePendingTransition(com.smartportable.demo2.R.anim.anim_slide_in_right,
                        com.smartportable.demo2.R.anim.anim_slide_out_right);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class SaveSessionActivity extends AsyncTask<Void, Void, Void> {
        ProgressDialog pDialog;
        String session_id = "", session_name = "";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AddSessionActivity.this, com.smartportable.demo2.R.style.MyTheme);
            pDialog.setCancelable(false);
            pDialog.show();

            session_id = etSessionId.getText().toString();
            session_name = etSessionName.getText().toString();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Sessions session = new Sessions();
            session.setSessionId(Integer.parseInt(session_id));
            session.setSessionName(session_name);
            session.setSessionDate(System.currentTimeMillis());//session_date_mili);
            session.setSessionFromDate(session_start_time_mili);
            session.setSessionToDate(session_end_time_mili);

            db.saveSession(session, pos);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            Boast.makeText(AddSessionActivity.this, getString(com.smartportable.demo2.R.string.session_saved), Toast.LENGTH_SHORT).show();

            setResult(SessionsActivity.ADD_SESSION);
            AddSessionActivity.this.finish();
            if (URLCollections.whichDirection(AddSessionActivity.this) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(com.smartportable.demo2.R.anim.anim_slide_in_left,
                        com.smartportable.demo2.R.anim.anim_slide_out_left);
            } else {
                overridePendingTransition(com.smartportable.demo2.R.anim.anim_slide_in_right,
                        com.smartportable.demo2.R.anim.anim_slide_out_right);
            }

        }
    }

    public class LoadExistingSession extends AsyncTask<Integer, Void, Void> {
        ProgressDialog pDialog;
        Sessions session = null;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AddSessionActivity.this, com.smartportable.demo2.R.style.MyTheme);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Integer... params) {
            int position = params[0];
            Log.d("Position", ""+position);
            session = db.getSessionFromId(position);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pDialog.isShowing())
                pDialog.dismiss();

            if (session != null) {
                int session_id = session.getSessionId();
                String session_name = session.getSessionName();
                org_session_name = session_name;
                long session_date = session.getSessionDate();
                long session_from_date = session.getSessionFromDate();
                long session_to_date = session.getSessionToDate();

                if (session_id > -1) {
                    etSessionId.setText(""+session_id);
                }

                if (session_name.length() > 0) {
                    etSessionName.setText(session_name);
                }
                if (session_date > 0.0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Config.DATETIME_FORMAT_LOCAL);
                    String sess_date = sdf.format(session_date);
                    etSessionDate.setText(""+sess_date);
                    session_date_mili = session_date;
                }

                // to rest the timezone to device current timezone
                TimeZone.setDefault(null);
                System.setProperty("user.timezone", "");
                TimeZone.setDefault(null);

                if (session_from_date > 0.0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");//, Config.DATETIME_FORMAT_LOCAL);
                    String sess_from_date = sdf.format(session_from_date);
                    etSessionStartTime.setText(""+sess_from_date);
                    session_start_time_mili = session_from_date;
                }
                if (session_to_date > 0.0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");//, Config.DATETIME_FORMAT_LOCAL);
                    String sess_to_date = sdf.format(session_to_date);
                    etSessionEndTime.setText(""+sess_to_date);
                    session_end_time_mili = session_to_date;
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(URLCollections.whichDirection(AddSessionActivity.this) == URLCollections.DEV_DIR_RTL) {
            overridePendingTransition(com.smartportable.demo2.R.anim.anim_slide_in_left,
                    com.smartportable.demo2.R.anim.anim_slide_out_left);
        } else {
            overridePendingTransition(com.smartportable.demo2.R.anim.anim_slide_in_right,
                    com.smartportable.demo2.R.anim.anim_slide_out_right);
        }
    }
}
