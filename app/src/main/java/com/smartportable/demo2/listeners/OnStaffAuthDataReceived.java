package com.smartportable.demo2.listeners;

import com.smartportable.demo2.models.MifareSettingModel;
import com.smartportable.demo2.models.StaffInfoModel;

import java.util.ArrayList;

public interface OnStaffAuthDataReceived {

    void onStaffAuthDataReceived(ArrayList<MifareSettingModel> list, String cardNo);
    void onStaffAuthApiDataReceived(StaffInfoModel.DataBean cardData);


}
