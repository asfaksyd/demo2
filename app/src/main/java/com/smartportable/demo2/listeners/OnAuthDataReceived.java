package com.smartportable.demo2.listeners;

import com.smartportable.demo2.models.MifareSettingModel;
import com.smartportable.demo2.models.StudentInfoModel;

import java.util.ArrayList;

public interface OnAuthDataReceived {

    void onAuthDataReceived(ArrayList<MifareSettingModel> list, String cardNo);
    void onAuthApiDataReceived(StudentInfoModel.DataBean cardData);


}
