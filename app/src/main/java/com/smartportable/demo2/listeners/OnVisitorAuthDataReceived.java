package com.smartportable.demo2.listeners;

import com.smartportable.demo2.models.MifareSettingModel;
import com.smartportable.demo2.models.VisitorInfoModel;

import java.util.ArrayList;

public interface OnVisitorAuthDataReceived {

    void onVisitorAuthDataReceived(ArrayList<MifareSettingModel> list, String cardNo);
    void onVisitorAuthApiDataReceived(VisitorInfoModel.DataBean cardData);


}
