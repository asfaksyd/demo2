package com.smartportable.demo2;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smartportable.demo2.activities.SyncAccessAreasActivity;
import com.smartportable.demo2.activities.SyncAccessListActivity;
import com.smartportable.demo2.activities.SyncReadersActivity;
import com.smartportable.demo2.activities.SyncSessionsActivity;
import com.smartportable.demo2.activities.SyncStaffAccessActivity;
import com.smartportable.demo2.activities.SyncStaffInfoActivity;
import com.smartportable.demo2.activities.SyncStudentInfoActivity;
import com.smartportable.demo2.activities.SyncVisitorInfoActivity;
import com.smartportable.demo2.adapters.SyncGridAdapter;
import com.smartportable.demo2.api.ApiInterface;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.Areas;
import com.smartportable.demo2.helper.Categories;
import com.smartportable.demo2.helper.Employee;
import com.smartportable.demo2.helper.GlobalClass;
import com.smartportable.demo2.helper.PunchLogs;
import com.smartportable.demo2.helper.SyncType;
import com.smartportable.demo2.prefs.MyPreferencesActivity;
import com.smartportable.demo2.utils.KeysKt;
import com.smartportable.demo2.utils.SharedPref;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Created by Ananth on 1/2/2017.
 */

public class SyncActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    public static String TAG = "SyncActivity";
    List<Categories> categoriesList = new ArrayList<Categories>();
    //String[] categoriesListArray = new String[0];
    List<Areas> areasList = new ArrayList<Areas>();
    String[] areasListArray = new String[0];
    DatabaseHandler db;
    Button btnSyncCompleted, btnSyncReader, btnSyncCategories, btnSyncAreas, btnSyncEmployees, btnSyncMusteringReaders, btnPingConnection, btnCloseApp, btnContinueApp;
    TextView tvSkipSync, tvPingStatus;
    RelativeLayout rlSyncActivity;

    ApiInterface apiService;
    ApiInterface apiAreaService;
    ApiInterface apiEmployeeService;
    Call<Areas> call_area;
    Call<Employee> call_employee;
    public static int REQUEST_SYNC_CATEGORIES = 1;
    public static int REQUEST_SYNC_AREAS = 2;
    public static int REQUEST_SYNC_EMPLOYEES = 3;
    public static int REQUEST_SYNC_MUSTERING_READER = 4;
    public static int REQUEST_SYNC_ACCESS_AREAS = 5;
    ArrayList<PunchLogs> empList = new ArrayList<PunchLogs>();

    GridView gvSyncList;
    ArrayList<SyncType> listSync = new ArrayList<SyncType>();
    Snackbar sbPingConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);

        ActionBar ab = getSupportActionBar();
        ab.setHomeButtonEnabled(true);

        db = new DatabaseHandler(this);

        gvSyncList = (GridView) findViewById(R.id.gvSyncList);
        rlSyncActivity = (RelativeLayout) findViewById(R.id.rlSyncActivity);
        int[] imageArray = /*{R.drawable.db_con_white_256,*/{ R.drawable.mustering_point_white /*,R.drawable.mustering_point*/, R.drawable.canteen_512, R.drawable.sync_employee_white, R.drawable.ic_stu_access, R.drawable.ic_staff_512, R.drawable.ic_staff_access2, R.drawable.ic_visitor_sync};
        int[] syncNames = /*{R.string.ping_server,*/ {R.string.sync_sessions /*,R.string.sync_readers*/, R.string.sync_access_area, R.string.sync_employees, R.string.sync_student_access, R.string.sync_staff, R.string.sync_staff_access, R.string.sync_visitor};

        for (int i=0; i<imageArray.length; i++) {
            SyncType st = new SyncType();
            st.setSyncTypeImage(imageArray[i]);
            st.setSyncTypeName(getResources().getString(syncNames[i]));
            listSync.add(st);
        }

        SyncGridAdapter adapter = new SyncGridAdapter(SyncActivity.this, listSync);
        gvSyncList.setAdapter(adapter);

        gvSyncList.setOnItemClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            SyncActivity.this.finish();
            if(URLCollections.whichDirection(SyncActivity.this) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            /*case 0: // ping status connection
                *//*tvPingStatus.setVisibility(View.VISIBLE);
                tvPingStatus.setTextColor(getResources().getColor(R.color.color_button_bg));
                tvPingStatus.setText("checking connection...");*//*

                sbPingConnection = Snackbar
                        .make(rlSyncActivity, getString(R.string.checking_server_conn), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getResources().getString(R.string.snack_action_refresh), null);
                sbPingConnection.setActionTextColor(Color.GREEN);
                View sbViewPingConnection = sbPingConnection.getView();
                sbViewPingConnection.setBackgroundColor(Color.DKGRAY);
                TextView tvPing = (TextView) sbViewPingConnection.findViewById(com.google.android.material.R.id.snackbar_text);
                tvPing.setTextColor(Color.WHITE);
                sbPingConnection.show();


                Thread areaThread = new Thread(pingRunnable);
                areaThread.start();

                break;*/

            /*case 1: //sync mustering type
                //startActivityForResult(new Intent(SyncActivity.this, SyncCategories.class), REQUEST_SYNC_CATEGORIES);
                startActivityForResult(new Intent(SyncActivity.this, SyncSessionsActivity.class), REQUEST_SYNC_CATEGORIES);
                break;*/

            case 0: //sync sessions
                startActivityForResult(new Intent(SyncActivity.this, SyncSessionsActivity.class), REQUEST_SYNC_AREAS);
                if (URLCollections.whichDirection(SyncActivity.this) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                }

                break;

            case -1: //sync readers
                //if (db.checkCategories()) {
                startActivity(new Intent(SyncActivity.this, SyncReadersActivity.class));

                if (URLCollections.whichDirection(SyncActivity.this) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                }
                /*} else {
                    Snackbar snackbar = Snackbar
                            .make(rlSyncActivity, R.string.sync_must_types_first, Snackbar.LENGTH_LONG);
                    //.setAction(getResources().getString(R.string.snack_action_refresh), null);
                    //snackbarNFC.setActionTextColor(Color.GREEN);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();
                }*/

                break;

            case 1: // sync access areas
                startActivityForResult(new Intent(SyncActivity.this, SyncAccessAreasActivity.class), REQUEST_SYNC_ACCESS_AREAS);
                if (URLCollections.whichDirection(SyncActivity.this) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                }
                break;

            case 2: // sync student
                startActivityForResult(new Intent(SyncActivity.this, SyncStudentInfoActivity.class), REQUEST_SYNC_EMPLOYEES);
                if (URLCollections.whichDirection(SyncActivity.this) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                }
                break;

            case 3: // sync student access
                if (db.checkStudentInfo()) {
                    startActivityForResult(new Intent(SyncActivity.this, SyncAccessListActivity.class), REQUEST_SYNC_MUSTERING_READER);
                    if (URLCollections.whichDirection(SyncActivity.this) == URLCollections.DEV_DIR_RTL) {
                        overridePendingTransition(R.anim.anim_slide_in_right,
                                R.anim.anim_slide_out_right);
                    } else {
                        overridePendingTransition(R.anim.anim_slide_in_left,
                                R.anim.anim_slide_out_left);
                    }
                } else {
                    Snackbar snackbar = Snackbar
                            .make(rlSyncActivity, R.string.sync_stu_first, Snackbar.LENGTH_LONG);
                    //.setAction(getResources().getString(R.string.snack_action_refresh), null);
                    //snackbarNFC.setActionTextColor(Color.GREEN);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();
                }

                break;

            case 4: // sync Staff master
                startActivityForResult(new Intent(SyncActivity.this, SyncStaffInfoActivity.class), REQUEST_SYNC_MUSTERING_READER);
                if (URLCollections.whichDirection(SyncActivity.this) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                }

                break;

            case 5: // sync staff access list master
                startActivityForResult(new Intent(SyncActivity.this, SyncStaffAccessActivity.class), REQUEST_SYNC_MUSTERING_READER);
                if (URLCollections.whichDirection(SyncActivity.this) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                }

                break;

            case 6: // sync visitor info
                startActivityForResult(new Intent(SyncActivity.this, SyncVisitorInfoActivity.class), REQUEST_SYNC_MUSTERING_READER);
                if (URLCollections.whichDirection(SyncActivity.this) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                }
                break;

            default:

                break;
        }
    }

    public View.OnClickListener mOnSBPingClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            System.out.println("SnackBar Refreshed Click");
            sbPingConnection.dismiss();
        }
    };

    /*@Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSyncReaders:
                if (db.checkCategories()) {
                    startActivity(new Intent(SyncActivity.this, SyncReaders.class));
                } else {
                    Snackbar snackbar = Snackbar
                            .make(rlSyncActivity, "Sync mustering type first", Snackbar.LENGTH_LONG);
                    //.setAction(getResources().getString(R.string.snack_action_refresh), null);
                    //snackbarNFC.setActionTextColor(Color.GREEN);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.DKGRAY);
                    TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar.show();
                }
                break;
            case R.id.btnSyncCategories:
                startActivityForResult(new Intent(SyncActivity.this, SyncCategories.class), REQUEST_SYNC_CATEGORIES);
                break;
            case R.id.btnSyncAreas:
                startActivityForResult(new Intent(SyncActivity.this, SyncAreas.class), REQUEST_SYNC_AREAS);
                break;
            case R.id.btnSyncEmployees:
                startActivityForResult(new Intent(SyncActivity.this, SyncAllEmployees.class), REQUEST_SYNC_EMPLOYEES);
                break;
            case R.id.btnSyncMusteringReaders:
                startActivityForResult(new Intent(SyncActivity.this, SyncMusteringReaders.class), REQUEST_SYNC_MUSTERING_READER);
                break;
            case R.id.btnPingConnection:
                tvPingStatus.setVisibility(View.VISIBLE);
                tvPingStatus.setTextColor(getResources().getColor(R.color.color_button_bg));
                tvPingStatus.setText("checking connection...");

                Thread areaThread = new Thread(pingRunnable);
                areaThread.start();

                break;
            default:
                break;

        }
    }*/

    private Runnable pingRunnable = new Runnable() {
        @Override
        public void run() {
            String url = SharedPref.INSTANCE.getStringValue(SyncActivity.this, KeysKt.keyBaseUrl, "");
            //permissionStatus = PreferenceManager.getDefaultSharedPreferences(this);


            if (url == null || url.equals("") || !URLCollections.isValidUrl(url)) {

                Snackbar snackbar = Snackbar
                        .make(rlSyncActivity, getResources().getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getResources().getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener);
                snackbar.setActionTextColor(Color.GREEN);
                View snackbarView = snackbar.getView();
                snackbarView.setBackgroundColor(Color.DKGRAY);
                TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.show();
            } else {
                String ping_host = URLCollections.getHostAddressFromURL(url);
                Log.d("Ping IP", "HostAddress " + ping_host);
                final boolean ping_status = GlobalClass.getInstance().ping(ping_host);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (sbPingConnection.isShown())
                            sbPingConnection.dismiss();

                        if (ping_status) {
                            //tvPingStatus.setTextColor(getResources().getColor(R.color.color_button_bg));
                            //tvPingStatus.setText("Connection is proper");

                            sbPingConnection = Snackbar
                                    .make(rlSyncActivity, R.string.conn_is_proper, Snackbar.LENGTH_INDEFINITE)
                                    .setAction(getString(R.string.ok_label), mOnSBPingClickListener);
                            sbPingConnection.setActionTextColor(Color.GREEN);
                        } else {
                            //tvPingStatus.setTextColor(getResources().getColor(android.R.color.holo_red_light));
                            //tvPingStatus.setText("Connection is not proper");

                            sbPingConnection = Snackbar
                                    .make(rlSyncActivity, R.string.conn_is_not_proper, Snackbar.LENGTH_INDEFINITE)
                                    .setAction(getString(R.string.ok_label), mOnSBPingClickListener);
                            sbPingConnection.setActionTextColor(Color.RED);
                        }


                        View sbViewPingConnection = sbPingConnection.getView();
                        sbViewPingConnection.setBackgroundColor(Color.DKGRAY);
                        TextView tvPing = (TextView) sbViewPingConnection.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvPing.setTextColor(Color.WHITE);
                        sbPingConnection.show();

                    }
                });
            }
        }
    };

    public View.OnClickListener mOnSnackBarSetUrlClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            System.out.println("SnackBar Set URL Click");
            Intent int_set_url = new Intent(SyncActivity.this, MyPreferencesActivity.class);
            int_set_url.putExtra(HomeScreen.ACTION_STR, HomeScreen.SET_URL);
            startActivity(int_set_url);
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(URLCollections.whichDirection(SyncActivity.this) == URLCollections.DEV_DIR_RTL) {
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
        } else {
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }
}