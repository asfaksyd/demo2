package com.smartportable.demo2.Sync;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.smartportable.demo2.R;
import com.smartportable.demo2.HomeScreen;
import com.smartportable.demo2.adapters.ListItemAdapter;
import com.smartportable.demo2.api.ApiClient;
import com.smartportable.demo2.api.ApiInterface;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.Config;
import com.smartportable.demo2.helper.Employee;
import com.smartportable.demo2.helper.GlobalClass;
import com.smartportable.demo2.helper.MusteringReaders;
import com.smartportable.demo2.helper.PreferencesManager;
import com.smartportable.demo2.helper.PunchLogs;
import com.smartportable.demo2.helper.RecyclerListSelectorModel;
import com.smartportable.demo2.interfaces.OnLoadMoreListener;
import com.smartportable.demo2.interfaces.SelectedListListener;
import com.smartportable.demo2.prefs.MyPreferencesActivity;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.danoz.recyclerviewfastscroller.vertical.VerticalRecyclerViewFastScroller;

/**
 * Created by Ananth on 3/19/2017.
 */

public class SyncMusteringReaders extends AppCompatActivity implements SelectedListListener, SearchView.OnQueryTextListener {

    ApiInterface apiMusteringReadersService;
    private static String TAG = "SyncAllEmployees";
    DatabaseHandler db;
    ArrayList<Employee> empList = new ArrayList<Employee>();
    ArrayList<RecyclerListSelectorModel> listBoolean = new ArrayList<RecyclerListSelectorModel>();
    TextView tvAllEmployeeTitle;
    //ListView
    RecyclerView lvAllEmployee;
    VerticalRecyclerViewFastScroller fastScrollerAllEmp;
    Button btnAllEmployeeOkay;
    ListItemAdapter employee_adapter;
    RelativeLayout rlAllEmployeeSync, rlAllEmpCount;
    TextView tvMustReaderCount;
    Handler mHandler = null;
    Runnable mRunnable;

    ProgressDialog pDialog;
    int progressPercentage;
    int firstVal = 0;
    int lastVal = 0;
    double totalRecords = 0;

    // request part
    int req_mode = 0;
    String req_timestamp = "";
    int currentSyncPage = 1;
    int req_send_Photo = 0;
    String req_emp_id = "Test";
    int req_card_mode = URLCollections.CARD_MODE;

    ActionBar ab;
    Menu menu;
    boolean flag_loading = false;
    View footerView;
    TextView tcPBTitle;
    ProgressBar pbLoadMore;
    boolean displayDoneOption = false;
    LoadAllMemberAccessDataFromDB loadMemAcc;
    boolean running = false;
    SearchView searchView;
    MenuItem searchMenuItem;

    @Override
    public void onListChanged(ArrayList<RecyclerListSelectorModel> list) {
        listBoolean = list;
        for (int i=0; i<list.size();i++) {
            if (list.get(i).isSelected()) {
                displayDoneOption = true;
                break;
            }
        }
        invalidateOptionsMenu();
    }

    @Override
    public void onPunchListChanged(ArrayList<PunchLogs> list) {

    }

    @Override
    public void onItemRemoved(String card_no) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_all_employee);

        // footer view
        footerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_layout, null, false);
        tcPBTitle = (TextView) footerView.findViewById(R.id.tvPBTitle);
        pbLoadMore = (ProgressBar) footerView.findViewById(R.id.pbLoadMore);

        ab = getSupportActionBar();
        ab.setHomeButtonEnabled(true);

        db = new DatabaseHandler(this);

        rlAllEmpCount = (RelativeLayout) findViewById(R.id.rlAllEmpCount);
        tvMustReaderCount = (TextView) rlAllEmpCount.findViewById(R.id.tvAllEmpCount);
        tvAllEmployeeTitle = (TextView) findViewById(R.id.tvAllEmployeeTitle);
        btnAllEmployeeOkay = (Button) findViewById(R.id.btnAllEmployeeOkay);
        btnAllEmployeeOkay.setOnClickListener(mBtnOnClickListener);
        lvAllEmployee = (RecyclerView) findViewById(R.id.lvAllEmployee);
        fastScrollerAllEmp = (VerticalRecyclerViewFastScroller) findViewById(R.id.fastScrollerAllEmp);
        tvAllEmployeeTitle.setText("");

        // main relative layout touch event to hide and show the total count of the employee in the list
        lvAllEmployee.setOnTouchListener(mOnTouchListener);

        //lvAllEmployee.addFooterView(footerView);

        //lvAllEmployee.setFastScrollEnabled(true);

        lvAllEmployee.setHasFixedSize(true);
        lvAllEmployee.setLayoutManager(new LinearLayoutManager(this));

        // Connect the recycler to the scroller (to let the scroller scroll the list)
        fastScrollerAllEmp.setRecyclerView(lvAllEmployee);

        // Connect the scroller to the recycler (to let the recycler scroll the scroller's handle)
        lvAllEmployee.setOnScrollListener(fastScrollerAllEmp.getOnScrollListener());


        employee_adapter = new ListItemAdapter(this, empList, lvAllEmployee, listBoolean, getSupportFragmentManager());
        lvAllEmployee.setAdapter(employee_adapter);
        employee_adapter.setListener(this);

        if (loadMemAcc == null)
        loadMemAcc = new LoadAllMemberAccessDataFromDB();

        running = true;

        employee_adapter.setOnLoadMoreListener(mLoadMoreListener);
        /*employee_adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override public void onLoadMore() {
                Log.e("hint", "Load More");
                //if (empList.size() >= 100)
                empList.add(null);

                employee_adapter.notifyItemInserted(empList.size() - 1);
                //Load more data for reyclerview
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {
                        Log.e("hint", "Load More 2");
                        //Remove loading item
                        empList.remove(empList.size() - 1);
                        employee_adapter.notifyItemRemoved(empList.size()+1);

                        flag_loading = true;
                        firstVal = lastVal + 1;
                        lastVal = firstVal + 99;
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //loadAllMemberAccessDataFromDB();
                            if (running) {
                                new LoadAllMemberAccessDataFromDB().execute();
                            }
                            }
                        }, 1000);
                    }
                }, 1000);
            }
        });*/
        /*lvAllEmployee.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount!=0)
                {
                    if(flag_loading == false)
                    {
                        lvAllEmployee.addFooterView(footerView);
                        //footerView.setVisibility(View.VISIBLE);
                        flag_loading = true;
                        firstVal = lastVal + 1;
                        lastVal = firstVal + 99;
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    loadAllEmployeeFromDB();
                                                }
                                            }, 1000);

                    }
                }
            }
        });*/
        rlAllEmployeeSync = (RelativeLayout) findViewById(R.id.rlAllEmployeeSync);
        if (!db.checkMemberData()) {
            // Alert Dialog
            AlertDialog.Builder mDialog = new AlertDialog.Builder(this);
            LayoutInflater adbInflater = LayoutInflater.from(this);
            /*View eulaLayout = adbInflater.inflate(R.layout.dialog_checkbox, null);

            final CheckBox sync_with_image = (CheckBox) eulaLayout.findViewById(R.id.sync_with_image);
            mDialog.setView(eulaLayout);*/
            mDialog.setTitle(getString(R.string.sync_warning_title));
            mDialog.setMessage(Html.fromHtml(getString(R.string.sync_warning_must_readers)));
            mDialog.setCancelable(false);

            mDialog.setPositiveButton(getString(R.string.yes_label), new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pDialog = new ProgressDialog(SyncMusteringReaders.this);//, R.style.MyTheme);
                    pDialog.setMessage(getString(R.string.fetch_mem_acc_data));
                    pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pDialog.setIndeterminate(false);
                    pDialog.setProgress(progressPercentage);
                    pDialog.setCancelable(false);
                    pDialog.setProgressNumberFormat(null);
                    pDialog.show();
                    /*SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncMusteringReaders.this);
                    String last_synced_date = prefs.getString(PreferencesManager.KEY_SET_LAST_SYNCED_TIMESTAMP, "");
                    if (last_synced_date != null  && last_synced_date.length() > 0) {
                        req_timestamp = last_synced_date ;
                    }else {
                        // to set for all employees
                        // for current date
                        Calendar c = Calendar.getInstance();
                        //System.out.println("Current time => "+c.getTime());
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        req_timestamp = df.format(c.getTime());
                    }
                    Log.d(TAG, "Current date: "+req_timestamp);
                    if(sync_with_image.isChecked()) {
                        req_send_Photo = 1;
                    }*/
                    onMusteringReadersAPICall();
                }
            }).setNegativeButton(getString(R.string.no_label), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.d(TAG, "Negative button click");
                }
            });
            mDialog.show();
        } else {
            //lvAllEmployee.addFooterView(footerView);
            //footerView.setVisibility(View.VISIBLE);
            firstVal = 0;
            lastVal = 0;
            firstVal = lastVal + 1;
            lastVal = firstVal + 99;
            //lastVal = 23000;
            //loadAllMemberAccessDataFromDB();

            new LoadAllMemberAccessDataFromDB().execute();
        }
    }

    public OnLoadMoreListener mLoadMoreListener = new OnLoadMoreListener() {
        @Override public void onLoadMore() {
            Log.e("hint", "Load More");
            //if (empList.size() >= 100)
            empList.add(null);

            employee_adapter.notifyItemInserted(empList.size() - 1);
            //Load more data for reyclerview
            new Handler().postDelayed(new Runnable() {
                @Override public void run() {
                    Log.e("hint", "Load More 2");
                    //Remove loading item
                    empList.remove(empList.size() - 1);
                    employee_adapter.notifyItemRemoved(empList.size()+1);

                    flag_loading = true;
                    firstVal = lastVal + 1;
                    lastVal = firstVal + 99;
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //loadAllMemberAccessDataFromDB();
                            if (running) {
                                new LoadAllMemberAccessDataFromDB().execute();
                            }
                        }
                    }, 1000);
                }
            }, 1000);
        }
    };

    /*@Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, v.getId(), 0, "Sync with photos");
        menu.add(0, v.getId(), 1, "Sync without photos");
        super.onCreateContextMenu(menu, v, menuInfo);
    }*/

    public View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Log.d(TAG, "onTouch"+event.getAction());
            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:
                    /*int count = employee_adapter.getItemCount();
                    if (count > 0) {*/
                    if (mHandler != null) {
                        mHandler.removeCallbacks(mRunnable);
                    }
                    rlAllEmpCount.setVisibility(View.VISIBLE);
                    //}
                    return false;
                case MotionEvent.ACTION_DOWN:
                    if (mHandler != null) {
                        mHandler.removeCallbacks(mRunnable);
                    }
                    rlAllEmpCount.setVisibility(View.VISIBLE);
                    break;
                case MotionEvent.ACTION_UP:
                    if (mHandler != null) {
                        mHandler.removeCallbacks(mRunnable);
                    }
                    mHandler = new Handler();
                    mRunnable = new Runnable() {
                        @Override
                        public void run() {
                            rlAllEmpCount.setVisibility(View.GONE);
                        }
                    };
                    mHandler.postDelayed(mRunnable, 3000);
            }
            return false;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_sync_member_access, menu);

        final MenuItem actionRefresh = menu.findItem(R.id.action_acc_mem_refresh);

        // for search query
        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);

        searchMenuItem = menu.findItem(R.id.action_acc_mem_search);
        searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint(getString(R.string.search_query_hint));

        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            //MenuItem menuCategory = menu.findItem(R.id.action_category);
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                //searchMenuItem.expandActionView();
                //menuCategory.setVisible(false);
                actionRefresh.setVisible(false);
                searchView.setQuery("", false);
                searchView.setIconified(false);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                //searchMenuItem.collapseActionView();
                //menuCategory.setVisible(true);
                actionRefresh.setVisible(true);
                searchView.setQuery("", false);
                searchView.setIconified(true);

                if (empList.size() > 0)
                    empList.clear();

                firstVal = 0;
                lastVal = 0;
                firstVal = lastVal + 1;
                lastVal = firstVal + 99;

                new LoadAllMemberAccessDataFromDB().execute();

                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

            if (!ab.getTitle().equals(getString(R.string.sync_mustering_readers))) {
                firstVal = 0;
                lastVal = 0;
                employee_adapter.setOnLoadMoreListener(mLoadMoreListener);
                employee_adapter.notifyDataSetChanged();

                int count = employee_adapter.getItemCount();
                if (count > 0) {
                    tvMustReaderCount.setText("" + URLCollections.getNumberFormat().format(count));
                }

                if (mHandler != null) {
                    mHandler.removeCallbacks(mRunnable);
                }

                mHandler = new Handler();
                mRunnable = new Runnable() {
                    @Override
                    public void run() {
                        rlAllEmpCount.setVisibility(View.GONE);
                    }
                };
                mHandler.postDelayed(mRunnable, 3000);

                ab.setTitle(getResources().getString(R.string.sync_mustering_readers));
                ab.setDisplayHomeAsUpEnabled(true);
                invalidateOptionsMenu();
            } else {
                SyncMusteringReaders.this.finish();
            }
            return true;
        } else if (item.getItemId() == R.id.action_acc_mem_search) {
            searchMenuItem.expandActionView();
        }
        else if (item.getItemId() == R.id.action_acc_mem_refresh) { // all employee without photos
            // Alert to show before syc start
            AlertDialog.Builder mDialog = new AlertDialog.Builder(this);
            mDialog.setTitle(getString(R.string.sync_warning_title));
            mDialog.setMessage(Html.fromHtml(getString(R.string.sync_warning_must_readers)));
            mDialog.setCancelable(false);

            mDialog.setPositiveButton(getString(R.string.yes_label), new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    /*SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncMusteringReaders.this);
                    String last_synced_date = prefs.getString(PreferencesManager.KEY_SET_LAST_SYNCED_TIMESTAMP, "");
                    if (last_synced_date != null  && last_synced_date.length() > 0) {
                        req_timestamp = last_synced_date ;
                    }else {
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        req_timestamp = df.format(c.getTime());
                    }
                    Log.d(TAG, "Current date: "+req_timestamp);

                    req_mode = 0;
                    req_send_Photo = 0;
                    currentSyncPage = 1;
                    req_emp_id = "Test";*/

                    pDialog = new ProgressDialog(SyncMusteringReaders.this);//, R.style.MyTheme);
                    pDialog.setMessage(getString(R.string.fetch_mem_acc_data));
                    pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pDialog.setIndeterminate(false);
                    pDialog.setProgress(progressPercentage);
                    pDialog.setCancelable(false);
                    pDialog.setProgressNumberFormat(null);
                    pDialog.show();

                    currentSyncPage = 1;
                    onMusteringReadersAPICall();
                }
            }).setNegativeButton(getString(R.string.no_label), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.d(TAG, "Negative nutton click");
                }
            });
            mDialog.show();
            return true;
        } /*else if (item.getItemId() == R.id.action_all_emp_with_photo) { // all employee with photos
            // Alert to show before syc start
            AlertDialog.Builder mDialog = new AlertDialog.Builder(this);
            mDialog.setTitle("Sync warning!");
            mDialog.setMessage(Html.fromHtml("<h5>Would you like to sync now ?</h5>This process will sync all the employees data and it will take few minutes to finished."));
            mDialog.setCancelable(false);

            mDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    req_mode = 0;
                    req_send_Photo = 1;
                    currentSyncPage = 1;
                    req_emp_id = "Test";
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncMusteringReaders.this);
                    String last_synced_date = prefs.getString(PreferencesManager.KEY_SET_LAST_SYNCED_TIMESTAMP, "");
                    if (last_synced_date != null  && last_synced_date.length() > 0) {
                        req_timestamp = last_synced_date ;
                    }else {
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        req_timestamp = df.format(c.getTime());
                    }
                    Log.d(TAG, "Current date: "+req_timestamp);
                    pDialog = new ProgressDialog(SyncMusteringReaders.this);//, R.style.MyTheme);
                    pDialog.setMessage("Fetching employees data");
                    pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pDialog.setIndeterminate(false);
                    pDialog.setProgress(progressPercentage);
                    pDialog.setCancelable(false);
                    pDialog.setProgressNumberFormat(null);
                    pDialog.show();
                    onMusteringReadersAPICall();
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.d(TAG, "Negative nutton click");
                }
            });
            mDialog.show();
            return true;
        } else if (item.getItemId() == R.id.action_new_emp_without_photo) { // new employee without photos
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncMusteringReaders.this);
            String last_synced_date = prefs.getString(PreferencesManager.KEY_SET_LAST_SYNCED_TIMESTAMP, "");
            if (last_synced_date != null  && last_synced_date.length() > 0) {
                req_timestamp = last_synced_date ;
            }else {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                req_timestamp = df.format(c.getTime());
            }
            Log.d(TAG, "Current date: "+req_timestamp);

            req_mode = 1;
            req_send_Photo = 0;
            currentSyncPage = 1;
            req_emp_id = "Test";
            pDialog = new ProgressDialog(this);//, R.style.MyTheme);
            pDialog.setMessage("Fetching member access data");
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setIndeterminate(false);
            pDialog.setProgress(progressPercentage);
            pDialog.setCancelable(false);
            pDialog.setProgressNumberFormat(null);
            pDialog.show();
            onMusteringReadersAPICall();
            return true;
        } else if (item.getItemId() == R.id.action_new_emp_with_photo) { // new employee with photos
            req_mode = 1;
            req_send_Photo = 1;
            currentSyncPage = 1;
            req_emp_id = "Test";
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncMusteringReaders.this);
            String last_synced_date = prefs.getString(PreferencesManager.KEY_SET_LAST_SYNCED_TIMESTAMP, "");
            if (last_synced_date != null  && last_synced_date.length() > 0) {
                req_timestamp = last_synced_date ;
            }else {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");
                req_timestamp = df.format(c.getTime());
            }
            Log.d(TAG, "Current date: "+req_timestamp);
            pDialog = new ProgressDialog(this);//, R.style.MyTheme);
            pDialog.setMessage("Fetching member access data");
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setIndeterminate(false);
            pDialog.setProgress(progressPercentage);
            pDialog.setCancelable(false);
            pDialog.setProgressNumberFormat(null);
            pDialog.show();
            onMusteringReadersAPICall();
            return true;
        }else if (item.getItemId() == R.id.action_selected_emp_without_photo) { // selected employee without photos
            //lvAllEmployee.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            if (listBoolean.size() > 0) {
                listBoolean.clear();
            }
            for (int k=0; k<empList.size(); k++) {
                RecyclerListSelectorModel model = new RecyclerListSelectorModel();
                model.setSelected(false);
                listBoolean.add(model);
            }
            employee_adapter.updateBooleanLList(listBoolean);
            employee_adapter.enableCheckBox(true);
            employee_adapter.notifyDataSetChanged();
            ab.setHomeButtonEnabled(true);
            ab.setTitle("Select Employees");

            invalidateOptionsMenu();

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncMusteringReaders.this);
            String last_synced_date = prefs.getString(PreferencesManager.KEY_SET_LAST_SYNCED_TIMESTAMP, "");
            if (last_synced_date != null  && last_synced_date.length() > 0) {
                req_timestamp = last_synced_date ;
            }else {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                req_timestamp = df.format(c.getTime());
            }
            Log.d(TAG, "Current date: "+req_timestamp);

            req_mode = 2;
            req_send_Photo = 0;
            currentSyncPage = 1;
            *//*
            pDialog = new ProgressDialog(this);//, R.style.MyTheme);
            pDialog.setMessage("Fetching selected employees data");
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setIndeterminate(false);
            pDialog.setProgress(progressPercentage);
            pDialog.setCancelable(false);
            pDialog.setProgressNumberFormat(null);
            pDialog.show();
            onMusteringReadersAPICall();*//*
            return true;
        } else if (item.getItemId() == R.id.action_selected_emp_with_photo) { // selected employee with photos
            //lvAllEmployee.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            if (listBoolean.size() > 0) {
                listBoolean.clear();
            }
            for (int k=0; k<empList.size(); k++) {
                RecyclerListSelectorModel model = new RecyclerListSelectorModel();
                model.setSelected(false);
                listBoolean.add(model);
            }
            employee_adapter.updateBooleanLList(listBoolean);
            employee_adapter.enableCheckBox(true);
            employee_adapter.notifyDataSetChanged();
            ab.setHomeButtonEnabled(true);
            ab.setTitle("Select Employees");

            invalidateOptionsMenu();

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncMusteringReaders.this);
            String last_synced_date = prefs.getString(PreferencesManager.KEY_SET_LAST_SYNCED_TIMESTAMP, "");
            if (last_synced_date != null  && last_synced_date.length() > 0) {
                req_timestamp = last_synced_date ;
            }else {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");
                req_timestamp = df.format(c.getTime());
            }
            Log.d(TAG, "Current date: "+req_timestamp);

            req_mode = 2;
            req_send_Photo = 1;
            currentSyncPage = 1;
            return true;
        } else if (item.getItemId() == R.id.action_selected_done) {
            //listBoolean = employee_adapter.getBooleanList();
            int total_count = listBoolean.size();
            Log.d(TAG, "Total Count"+total_count);
            System.out.println("Total Count"+total_count+" Contains: "+listBoolean.contains(true));

            if (total_count > 0) {
                *//*SparseBooleanArray itemArray = lvAllEmployee.getCheckedItemPositions();
                long[] ids = lvAllEmployee.getCheckedItemIds();
                Log.d(TAG, "POS"+ids.length);
                for (int j=0; j<ids.length;j++) {
                    int pos = Long.valueOf(ids[j]).intValue();
                    Log.d(TAG, "POS: "+pos);
                }
                for (int i=0; i<itemArray.size();i++) {
                    boolean p = itemArray.get(i);
                    Log.d(TAG, "IDs : "+itemArray.get(i));
                }*//*

                for (int k=0; k<empList.size();k++) {
                    if (listBoolean.get(k).isSelected()) {
                        int empID = empList.get(k).getSLN_Employee();
                        Log.d(TAG, "EmployeeID: " + empID);
                        if (req_emp_id.equals("Test")) {
                            req_emp_id = ""+empID;
                        }else {
                            req_emp_id = req_emp_id + "," + empID;
                        }
                        RecyclerListSelectorModel model = new RecyclerListSelectorModel();
                        model.setSelected(false);
                        listBoolean.set(k, model);
                    }
                }
                // to hide the checkbox and back to main page
                //emply the current list
                *//*lvAllEmployee.clearChoices();
                lvAllEmployee.setChoiceMode(ListView.CHOICE_MODE_NONE);*//*
                *//*listBoolean.clear();
                for (int k=0; k<empList.size();k++) {
                    RecyclerListSelectorModel model = new RecyclerListSelectorModel();
                    model.setSelected(false);
                    listBoolean.add(model);
                }*//*
                employee_adapter.updateBooleanLList(listBoolean);

                employee_adapter.enableCheckBox(false);
                employee_adapter.notifyDataSetChanged();

                ab.setTitle(getResources().getString(R.string.sync_employees));
                ab.setDisplayHomeAsUpEnabled(true);
                invalidateOptionsMenu();

                pDialog = new ProgressDialog(this);//, R.style.MyTheme);
                pDialog.setMessage("Fetching member access data");
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setIndeterminate(false);
                pDialog.setProgress(progressPercentage);
                pDialog.setCancelable(false);
                pDialog.setProgressNumberFormat(null);
                pDialog.show();
                onMusteringReadersAPICall();
            } else {
                Snackbar snackbar = Snackbar
                        .make(rlReaderSync, "Please select member data from list", Snackbar.LENGTH_LONG);
                //.setAction(getResources().getString(R.string.snack_action_refresh), mOnSBAddSessionClick);
                //snackbar.setActionTextColor(Color.GREEN);
                View snackbarView = snackbar.getView();
                snackbarView.setBackgroundColor(Color.DKGRAY);
                TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.show();
            }

            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        System.out.println("Query text submit." + query);
        //searchMenuItem.collapseActionView();
        //searchView.setIconified(true);
        new SearchEmployees().execute(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            //friendListView.clearTextFilter();
            //System.out.println("Search text emplty.");
        } else {
            //friendListView.setFilterText(newText.toString());
            //System.out.println("Searched text: " + newText);
        }

        return true;
    }

    public View.OnClickListener mOnSnackBarClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            System.out.println("SnackBar Refreshed Click");
            onMusteringReadersAPICall();
        }
    };

    public void onMusteringReadersAPICall() {

        final int records_per_page = Config.RECORDS_PER_PAGE;
        String url = URLCollections.getBaseURLFromPreference(SyncMusteringReaders.this);
        if (url == null || url.equals("") || !URLCollections.isValidUrl(url)) {
            if(pDialog.isShowing())
                pDialog.dismiss();

            tvAllEmployeeTitle.setText(getResources().getString(R.string.conn_string_not_define));
            lvAllEmployee.setVisibility(View.INVISIBLE);
            btnAllEmployeeOkay.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(rlAllEmpCount, getResources().getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getResources().getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener);
            snackbar.setActionTextColor(Color.GREEN);
            View snackbarView = snackbar.getView();
            snackbarView.setBackgroundColor(Color.DKGRAY);
            TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        } else if (GlobalClass.getInstance().isNetworkAvailable(this)) {
            //lvAllEmployee.setVisibility(View.INVISIBLE);
            //btnAllEmployeeOkay.setVisibility(View.INVISIBLE);
            tvAllEmployeeTitle.setText(R.string.wait_mem_acc_syncing);
            // to fetch the data from the API for categories
            apiMusteringReadersService = new
                    ApiClient(SyncMusteringReaders.this).getClient().create(ApiInterface.class);

            //Log.d(TAG, "CurrentPage: "+ currentSyncPage);
            Log.d(TAG, "currentSyncPage : "+ currentSyncPage);
            final Call<MusteringReaders> call_musteringreaders = apiMusteringReadersService.getMusteringReaders(currentSyncPage);
            call_musteringreaders.enqueue(new Callback<MusteringReaders>() {
                @Override
                public void onResponse(Call<MusteringReaders> call, Response<MusteringReaders> response) {
                    int statusCode = response.code();
                    Log.d(TAG, "response code: " + statusCode+" Response Message: "+response.message()+" Response success: "+response.isSuccessful()+" Response body: "+response.body());
                    if (statusCode != 200) {
                        if(pDialog.isShowing())
                            pDialog.dismiss();

                        lvAllEmployee.setVisibility(View.INVISIBLE);
                        btnAllEmployeeOkay.setVisibility(View.GONE);
                        tvAllEmployeeTitle.setText(Html.fromHtml(getString(R.string.problem_with_sync)));
                    }

                    if (response.isSuccessful()) {
                        int status = 0;
                        String message = "";
                        ArrayList<MusteringReaders> data;
                        MusteringReaders responseObj = response.body();
                        status = responseObj.getStatus();
                        message = responseObj.getMessage();
                        data = responseObj.getData();
                        if (status == 1 && data != null && data.size() > 0) {
                            Log.d(TAG, "All Employee Message: " + message);
                            Log.d(TAG, "All Employee Size: " + data.size());

                            if (message.contains("*")) {
                                message = message.replace("*", ",");
                                String[] recordsDetails = message.split(",");
                                totalRecords = Integer.parseInt(recordsDetails[1]);
                                if (currentSyncPage == 1) {
                                    db.deleteMemberData();
                                    empList.clear();
                                }
                            } else {
                                if (message.length() == 0 && data.size() > 0 && data.size() <= records_per_page) {

                                }
                            }
                            // to clear employee data every time

                            /*for (int i = 0; i < data.size(); i++) {
                                MusteringReaders musteringReader = new MusteringReaders();
                                MusteringReaders mustReaders = data.get(i);//new Categories();
                                int emp_id = mustReaders.getSLN_Employee();//dataObj.getInt(APIParam.RES_CAT_ID);
                                musteringReader.setSLN_Employee(emp_id);
                                //Log.d(TAG, "EmpId: " + emp_id);
                                String card_number = mustReaders.getCard_Number();
                                musteringReader.setCard_Number(card_number);

                                int readerId = mustReaders.getSLN_Reader();
                                musteringReader.setSLN_Reader(readerId);

                                // to add the data
                                db.addMemberData(musteringReader);
                            }*/
                            db.saveMemberData(data);

                            // before update current page count fetch first added data from database only once for first page
                            if (currentSyncPage == 1) {
                                //lvAllEmployee.addFooterView(footerView);
                                //footerView.setVisibility(View.VISIBLE);
                                firstVal = 0;
                                lastVal = 0;
                                firstVal = lastVal + 1;
                                lastVal = firstVal + 99;
                                //loadAllMemberAccessDataFromDB();

                                new LoadAllMemberAccessDataFromDB().execute();

                            }
                            //currentSyncPage = currentSyncPage + 1;
                            if (totalRecords > 0 && totalRecords > records_per_page) {
                                double value = totalRecords / records_per_page;
                                progressPercentage = (int)((100 * currentSyncPage) / value);

                                Log.d(TAG, "CurrentPercentage: "+progressPercentage);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pDialog.setProgress(progressPercentage);
                                    }
                                });
                                onMusteringReadersAPICall();
                            } else if (totalRecords > 0 && totalRecords <= records_per_page) {
                                progressPercentage = 100;
                                Log.d(TAG, "CurrentPercentage: "+progressPercentage);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pDialog.setProgress(progressPercentage);
                                    }
                                });

                                if(pDialog.isShowing())
                                    pDialog.dismiss();

                                lvAllEmployee.setVisibility(View.VISIBLE);
                                btnAllEmployeeOkay.setVisibility(View.GONE);
                                tvAllEmployeeTitle.setText(Html.fromHtml(getString(R.string.sync_done_mem_acc)));
                                currentSyncPage = 0;
                                setProgress(100);
                                totalRecords = 0;
                            } else {
                                if (totalRecords == 0 && data.size() > 0 && data.size() <= records_per_page) {
                                    progressPercentage = 100;
                                    Log.d(TAG, "CurrentPercentage: "+progressPercentage);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            pDialog.setProgress(progressPercentage);
                                        }
                                    });

                                    if(pDialog.isShowing())
                                        pDialog.dismiss();

                                    lvAllEmployee.setVisibility(View.VISIBLE);
                                    btnAllEmployeeOkay.setVisibility(View.GONE);
                                    tvAllEmployeeTitle.setText(Html.fromHtml(getString(R.string.sync_done_mem_acc)));
                                    currentSyncPage = 0;
                                    setProgress(100);
                                    totalRecords = 0;
                                }
                            }
                            currentSyncPage = currentSyncPage + 1;

                        } else if (status == -1) {
                            if(pDialog.isShowing())
                                pDialog.dismiss();

                            Log.d(TAG, "Member access exception Message: " + message);
                            lvAllEmployee.setVisibility(View.INVISIBLE);
                            btnAllEmployeeOkay.setVisibility(View.GONE);
                            tvAllEmployeeTitle.setText(Html.fromHtml(getString(R.string.problem_with_sync) + message));
                        } else if (status == 102) {
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncMusteringReaders.this);
                            SharedPreferences.Editor edit = prefs.edit();
                            edit.putString(PreferencesManager.KEY_SET_LAST_SYNCED_TIMESTAMP, ""+req_timestamp);
                            edit.commit();

                            if(pDialog.isShowing())
                                pDialog.dismiss();

                            lvAllEmployee.setVisibility(View.VISIBLE);
                            btnAllEmployeeOkay.setVisibility(View.GONE);
                            tvAllEmployeeTitle.setText(Html.fromHtml(getString(R.string.sync_done_mem_acc)));
                            currentSyncPage = 0;
                            setProgress(100);
                            totalRecords = 0;
                            //loadAllEmployeeFromDB();
                        } else {
                            if(pDialog.isShowing())
                                pDialog.dismiss();

                            Log.d(TAG, "Member access exception Message: ");
                            lvAllEmployee.setVisibility(View.INVISIBLE);
                            btnAllEmployeeOkay.setVisibility(View.GONE);
                            if (data.size()!=0) {
                                tvAllEmployeeTitle.setText(Html.fromHtml(getString(R.string.problem_with_sync) + message));
                            }else {
                                tvAllEmployeeTitle.setText(Html.fromHtml(getString(R.string.no_records_label)));
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<MusteringReaders> call, Throwable t) {
                    // Log error here since request failed
                    lvAllEmployee.setVisibility(View.INVISIBLE);
                    btnAllEmployeeOkay.setVisibility(View.GONE);
                    Log.e(TAG, "Member Access Response error : " + t.toString());
                    tvAllEmployeeTitle.setText(Html.fromHtml(getString(R.string.problem_with_sync)+getString(R.string.try_later_label)));

                    if(pDialog.isShowing())
                        pDialog.dismiss();
                }
            });
        } else {
            if(pDialog.isShowing())
                pDialog.dismiss();

            tvAllEmployeeTitle.setText(getResources().getString(R.string.no_conn));
            lvAllEmployee.setVisibility(View.INVISIBLE);
            btnAllEmployeeOkay.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(rlAllEmployeeSync, getResources().getString(R.string.no_conn), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getResources().getString(R.string.snack_action_refresh), mOnSnackBarClickListener);
            snackbar.setActionTextColor(Color.GREEN);
            View snackbarView = snackbar.getView();
            snackbarView.setBackgroundColor(Color.DKGRAY);
            TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    public View.OnClickListener mOnSnackBarSetUrlClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            System.out.println("SnackBar Set URL Click");
            Intent int_set_url = new Intent(SyncMusteringReaders.this, MyPreferencesActivity.class);
            int_set_url.putExtra(HomeScreen.ACTION_STR, HomeScreen.SET_URL);
            startActivity(int_set_url);
        }
    };

    public View.OnClickListener mBtnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            System.out.println("BtnOkCLick");
            //setResult(SyncActivity.REQUEST_SYNC_CATEGORIES);
            SyncMusteringReaders.this.finish();
        }
    };

    public void loadAllMemberAccessDataFromDB () {
        System.out.println(TAG+"first: "+firstVal+" & last: "+lastVal);
        ArrayList<Employee> list = db.getAllMembersData(firstVal, lastVal);
        for (int k = 0; k < list.size(); k++) {
            Employee emp = list.get(k);
            empList.add(emp);
            RecyclerListSelectorModel model = new RecyclerListSelectorModel();
            model.setSelected(false);
            listBoolean.add(model);
        }

        if (empList != null && empList.size() > 0) {
            employee_adapter.updateEmployeeList(empList);
            employee_adapter.updateBooleanLList(listBoolean);
            employee_adapter.isForMemberAccess(true);
            lvAllEmployee.setVisibility(View.VISIBLE);
            btnAllEmployeeOkay.setVisibility(View.GONE);
            employee_adapter.notifyDataSetChanged();

            rlAllEmpCount.setVisibility(View.VISIBLE);
            int count = employee_adapter.getItemCount();
            if (count > 0) {
                tvMustReaderCount.setText(""+ URLCollections.getNumberFormat().format(count));
            }

            if (mHandler != null) {
                mHandler.removeCallbacks(mRunnable);
            }

            mHandler = new Handler();
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    rlAllEmpCount.setVisibility(View.GONE);
                }
            };
            mHandler.postDelayed(mRunnable, 3000);

            employee_adapter.setLoaded();

            //lvAllEmployee.removeFooterView(footerView);
            //footerView.setVisibility(View.GONE);
            tvAllEmployeeTitle.setText(Html.fromHtml(getString(R.string.sync_done_mem_acc)));
            System.out.println(TAG+" EmpSize() : "+empList.size());
            if (lastVal <= empList.size()) {
                flag_loading = false;
            }
        } else {
            //lvAllEmployee.removeFooterView(footerView);
            //lvAllEmployee.setVisibility(View.INVISIBLE);
            //btnAllEmployeeOkay.setVisibility(View.INVISIBLE);
            //tvAllEmployeeTitle.setText(Html.fromHtml("<h4> Sync is not Done </h4> </br> No employees data available! Please try again."));
        }
    }

    public class LoadAllMemberAccessDataFromDB extends AsyncTask<Void, Void, Void> {
        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SyncMusteringReaders.this, R.style.MyTheme);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            System.out.println(TAG+"first: "+firstVal+" & last: "+lastVal);
            ArrayList<Employee> list = db.getAllMembersData(firstVal, lastVal);
            for (int k = 0; k < list.size(); k++) {
                Employee emp = list.get(k);
                empList.add(emp);
                RecyclerListSelectorModel model = new RecyclerListSelectorModel();
                model.setSelected(false);
                listBoolean.add(model);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }

            if (empList != null && empList.size() > 0) {
                employee_adapter.updateEmployeeList(empList);
                employee_adapter.updateBooleanLList(listBoolean);
                employee_adapter.isForMemberAccess(true);
                lvAllEmployee.setVisibility(View.VISIBLE);
                btnAllEmployeeOkay.setVisibility(View.GONE);
                employee_adapter.setOnLoadMoreListener(mLoadMoreListener);
                employee_adapter.notifyDataSetChanged();

                rlAllEmpCount.setVisibility(View.VISIBLE);
                int count = empList.size();//employee_adapter.getItemCount();
                if (count > 0) {
                    tvMustReaderCount.setText(""+ URLCollections.getNumberFormat().format(count));
                }

                if (mHandler != null) {
                    mHandler.removeCallbacks(mRunnable);
                }

                mHandler = new Handler();
                mRunnable = new Runnable() {
                    @Override
                    public void run() {
                        rlAllEmpCount.setVisibility(View.GONE);
                    }
                };
                mHandler.postDelayed(mRunnable, 3000);

                employee_adapter.setLoaded();

                //lvAllEmployee.removeFooterView(footerView);
                //footerView.setVisibility(View.GONE);
                tvAllEmployeeTitle.setText(Html.fromHtml(getString(R.string.sync_done_mem_acc)));
                System.out.println(TAG+" EmpSize() : "+empList.size());
                if (lastVal <= empList.size()) {
                    flag_loading = false;
                }
            } else {
                rlAllEmployeeSync.setVisibility(View.GONE);
                //lvAllEmployee.removeFooterView(footerView);
                //lvAllEmployee.setVisibility(View.INVISIBLE);
                //btnAllEmployeeOkay.setVisibility(View.INVISIBLE);
                //tvAllEmployeeTitle.setText(Html.fromHtml("<h4> Sync is not Done </h4> </br> No employees data available! Please try again."));
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (pDialog != null) {
            if (pDialog.isShowing()) {
                if (progressPercentage > 0) {
                    setProgress(progressPercentage);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        /*if (loadMemAcc != null) {
            if (!loadMemAcc.isCancelled()) {
                loadMemAcc.cancel(true);
            }
        }*/
        if (!ab.getTitle().equals(getString(R.string.sync_mustering_readers))) {
            //lvAllEmployee.setChoiceMode(ListView.CHOICE_MODE_NONE);
            //listBoolean.clear();
            employee_adapter.enableCheckBox(false);
            employee_adapter.setOnLoadMoreListener(mLoadMoreListener);
            employee_adapter.notifyDataSetChanged();

            rlAllEmpCount.setVisibility(View.VISIBLE);
            int count = empList.size();//employee_adapter.getItemCount();
            if (count > 0) {
                tvMustReaderCount.setText(""+ URLCollections.getNumberFormat().format(count));
            }

            if (mHandler != null) {
                mHandler.removeCallbacks(mRunnable);
            }

            mHandler = new Handler();
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    rlAllEmpCount.setVisibility(View.GONE);
                }
            };
            mHandler.postDelayed(mRunnable, 3000);

            ab.setTitle(getResources().getString(R.string.sync_mustering_readers));
            ab.setDisplayHomeAsUpEnabled(true);
            invalidateOptionsMenu();
        } else {
            invalidateOptionsMenu();
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        /*if (loadMemAcc != null) {
            if (!loadMemAcc.isCancelled()) {
                loadMemAcc.cancel(true);
            }
        }*/
        running = false;
        super.onDestroy();
    }

    // Async task for the Employees search
    public class SearchEmployees extends AsyncTask<String, Void, Void> {
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SyncMusteringReaders.this, R.style.MyTheme);
            pDialog.setCancelable(false);
            pDialog.show();

            empList.clear();
        }

        @Override
        protected Void doInBackground(String... params) {

            empList = db.getAllMembersDataFromSearch(params[0]);

            for (int k = 0; k < empList.size(); k++) {
                RecyclerListSelectorModel model = new RecyclerListSelectorModel();
                model.setSelected(false);
                listBoolean.add(model);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (empList.size() > 0) {
                employee_adapter.updateEmployeeList(empList);
                employee_adapter.updateBooleanLList(listBoolean);
                employee_adapter.isForMemberAccess(true);
                employee_adapter.setOnLoadMoreListener(null);
                lvAllEmployee.setVisibility(View.VISIBLE);
                btnAllEmployeeOkay.setVisibility(View.GONE);
                employee_adapter.notifyDataSetChanged();

                rlAllEmpCount.setVisibility(View.VISIBLE);
                int count = employee_adapter.getItemCount();
                if (count > 0) {
                    tvMustReaderCount.setText(""+ URLCollections.getNumberFormat().format(count));
                }

                if (mHandler != null) {
                    mHandler.removeCallbacks(mRunnable);
                }

                mHandler = new Handler();
                mRunnable = new Runnable() {
                    @Override
                    public void run() {
                        rlAllEmpCount.setVisibility(View.GONE);
                    }
                };
                mHandler.postDelayed(mRunnable, 3000);

                employee_adapter.setLoaded();
            } else {
                rlAllEmpCount.setVisibility(View.VISIBLE);

                lvAllEmployee.setVisibility(View.GONE);
                //tvNoRecords.setVisibility(View.VISIBLE);
            }
        }
    }

}
