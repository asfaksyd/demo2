package com.smartportable.demo2.Sync;

/**
 * Created by Ananth on 1/4/2017.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.smartportable.demo2.R;
import com.smartportable.demo2.HomeScreen;
import com.smartportable.demo2.SyncActivity;
import com.smartportable.demo2.api.ApiClient;
import com.smartportable.demo2.api.ApiInterface;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.Areas;
import com.smartportable.demo2.helper.GlobalClass;
import com.smartportable.demo2.prefs.MyPreferencesActivity;

import java.util.ArrayList;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ananth on 1/3/2017.
 */

public class SyncAreas extends AppCompatActivity {
    ApiInterface apiAreaService;
    private static String TAG = "SyncAreas";
    DatabaseHandler db;
    ArrayList<String> areasList = new ArrayList<String>();
    TextView tvAreaTitle;
    ListView lvArea;
    Button btnAreaOkay;
    RelativeLayout rlAreaSync;
    ArrayAdapter<String> area_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_category);

        ActionBar ab = getSupportActionBar();
        ab.setHomeButtonEnabled(true);

        db = new DatabaseHandler(this);

        tvAreaTitle = (TextView) findViewById(R.id.tvCategoryTitle);
        btnAreaOkay = (Button) findViewById(R.id.btnCategoryOkay);
        btnAreaOkay.setOnClickListener(mBtnOnClickListener);
        lvArea = (ListView) findViewById(R.id.lvCategory);
        tvAreaTitle.setText("");
        area_adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.row_category_item, R.id.tvCatName, areasList);
        lvArea.setAdapter(area_adapter);

        rlAreaSync = (RelativeLayout) findViewById(R.id.rlCategorySync);

        if (!db.checkAreas()) {
            onAeasAPICall();
        } else {
            loadAreaFromDB();
        }
    }
    // load areas from database
    public void loadAreaFromDB () {
        ArrayList<Areas> list = db.getAreas();
        for (int k = 0; k < list.size(); k++) {
            areasList.add(list.get(k).getArea_Name());
        }
        if (areasList != null && areasList.size() > 0) {
            lvArea.setVisibility(View.VISIBLE);
            btnAreaOkay.setVisibility(View.GONE);
            area_adapter.notifyDataSetChanged();
            tvAreaTitle.setText(Html.fromHtml(getString(R.string.sync_done_areas)));
        } else {
            lvArea.setVisibility(View.INVISIBLE);
            btnAreaOkay.setVisibility(View.GONE);
            tvAreaTitle.setText(Html.fromHtml(getString(R.string.sync_not_done_areas)));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cat_sync, menu);
        return super.onCreateOptionsMenu(menu);
    }
    // Call API to fetch areas
    public void onAeasAPICall () {
        final ProgressDialog pDialog = new ProgressDialog(this, R.style.MyTheme);
        pDialog.setCancelable(false);
        pDialog.show();

        String url = URLCollections.getBaseURLFromPreference(SyncAreas.this);
        if (url == null || url.equals("") || !URLCollections.isValidUrl(url)) {
            if(pDialog.isShowing())
                pDialog.dismiss();

            tvAreaTitle.setText(getResources().getString(R.string.conn_string_not_define));
            lvArea.setVisibility(View.INVISIBLE);
            btnAreaOkay.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(rlAreaSync, getResources().getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getResources().getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener);
            snackbar.setActionTextColor(Color.GREEN);
            View snackbarView = snackbar.getView();
            snackbarView.setBackgroundColor(Color.DKGRAY);
            TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        } else if (GlobalClass.getInstance().isNetworkAvailable(this)) {
            lvArea.setVisibility(View.INVISIBLE);
            btnAreaOkay.setVisibility(View.GONE);
            tvAreaTitle.setText(R.string.wait_areas_syncing);
            // to fetch the data from the API for categories
            apiAreaService = new
                    ApiClient(SyncAreas.this).getClient().create(ApiInterface.class);

            Call<Areas> call_areas = apiAreaService.getAllAreas();
            call_areas.enqueue(new Callback<Areas>() {
                @Override
                public void onResponse(Call<Areas> call, Response<Areas> response) {
                    int statusCode = response.code();
                    Log.d(TAG, "response code" + statusCode);
                    if (statusCode != 200) {
                        if(pDialog.isShowing())
                            pDialog.dismiss();

                        lvArea.setVisibility(View.INVISIBLE);
                        btnAreaOkay.setVisibility(View.GONE);
                        tvAreaTitle.setText(Html.fromHtml(getString(R.string.problem_with_sync)));
                    }

                    if (response.isSuccessful()) {
                        int status = 0;
                        String message = "";
                        ArrayList<Areas> data;
                        Areas responseObj = response.body();
                        status = responseObj.getStatus();
                        message = responseObj.getMessage();
                        data = responseObj.getData();
                        if (status == 1 && data != null && data.size() > 0) {
                            Log.d(TAG, "Area Message: " + message);
                            Log.d(TAG, "areaSize: " + data.size());
                            // to delete the table data before insert new
                            db.deleteAllAreas();
                            areasList.clear();
                            /*for (int i = 0; i < data.size(); i++) {
                                Areas area = data.get(i);//new Categories();
                                int area_id = area.getSLN_Area_ID();//dataObj.getInt(APIParam.RES_CAT_ID);
                                Log.d(TAG, "AreaId: " + area_id);
                                String area_name = area.getArea_Name();//dataObj.getString(APIParam.RES_CAT_NAME);
                                Log.d(TAG, "AreaName: " + area_name);
                                //categoriesListArray[i] = cat_name;
                                db.addAreas(area);
                            }*/
                            db.saveAreas(data);

                            loadAreaFromDB();

                            /*ArrayList<Areas> list = db.getAreas();
                            for (int k = 0; k < list.size(); k++) {
                                areasList.add(list.get(k).getArea_Name());
                            }
                            if (areasList != null && areasList.size() > 0) {
                                lvArea.setVisibility(View.VISIBLE);
                                btnAreaOkay.setVisibility(View.VISIBLE);
                                area_adapter.notifyDataSetChanged();
                                tvAreaTitle.setText(Html.fromHtml("<h4> Sync Done </h4> </br> Here are the list of areas"));
                            } else {
                                lvArea.setVisibility(View.INVISIBLE);
                                btnAreaOkay.setVisibility(View.INVISIBLE);
                                tvAreaTitle.setText(Html.fromHtml("<h4> Sync is not Done </h4> </br> No Areas data available! Please try again."));
                            }*/

                        } else if (status == -1) {
                            Log.d(TAG, "Areas exception Message: " + message);
                            lvArea.setVisibility(View.INVISIBLE);
                            btnAreaOkay.setVisibility(View.GONE);
                            tvAreaTitle.setText(Html.fromHtml(getString(R.string.problem_with_sync) + message));
                        }
                        if(pDialog.isShowing())
                            pDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Areas> call, Throwable t) {
                    // Log error here since request failed
                    lvArea.setVisibility(View.INVISIBLE);
                    btnAreaOkay.setVisibility(View.GONE);
                    Log.e(TAG, "Area Response error : " + t.toString());
                    tvAreaTitle.setText(Html.fromHtml(getResources().getString(R.string.problem_with_sync)+getResources().getString(R.string.try_later_label)));

                    if(pDialog.isShowing())
                        pDialog.dismiss();
                }
            });
        } else {
            if(pDialog.isShowing())
                pDialog.dismiss();

            tvAreaTitle.setText(getResources().getString(R.string.no_conn));
            lvArea.setVisibility(View.INVISIBLE);
            btnAreaOkay.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(rlAreaSync, getResources().getString(R.string.no_conn), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getResources().getString(R.string.snack_action_refresh), mOnSnackBarClickListener);
            snackbar.setActionTextColor(Color.GREEN);
            View snackbarView = snackbar.getView();
            snackbarView.setBackgroundColor(Color.DKGRAY);
            TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(SyncActivity.REQUEST_SYNC_AREAS);
            SyncAreas.this.finish();
            return true;
        } else if (item.getItemId() == R.id.action_cat_refresh) {
            onAeasAPICall();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public View.OnClickListener mOnSnackBarClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            System.out.println("SnackBar Refreshed Click");
            onAeasAPICall();
        }
    };

    public View.OnClickListener mOnSnackBarSetUrlClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            System.out.println("SnackBar Set URL Click");
            Intent int_set_url = new Intent(SyncAreas.this, MyPreferencesActivity.class);
            int_set_url.putExtra(HomeScreen.ACTION_STR, HomeScreen.SET_URL);
            startActivity(int_set_url);
        }
    };

    public View.OnClickListener mBtnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            System.out.println("BtnOkCLick");
            setResult(SyncActivity.REQUEST_SYNC_AREAS);
            SyncAreas.this.finish();
        }
    };
}

