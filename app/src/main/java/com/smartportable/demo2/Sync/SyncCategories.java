package com.smartportable.demo2.Sync;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smartportable.demo2.R;
import com.smartportable.demo2.HomeScreen;
import com.smartportable.demo2.SyncActivity;
import com.smartportable.demo2.api.ApiClient;
import com.smartportable.demo2.api.ApiInterface;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.Categories;
import com.smartportable.demo2.helper.GlobalClass;
import com.smartportable.demo2.prefs.MyPreferencesActivity;

import java.util.ArrayList;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ananth on 1/3/2017.
 */

public class SyncCategories extends AppCompatActivity {
    ApiInterface apiCategoryService;
    private static String TAG = "SyncCategories";
    DatabaseHandler db;
    ArrayList<String> empList = new ArrayList<String>();
    TextView tvCategoryTitle;
    ListView lvCategory;
    Button btnCategoryOkay;
    RelativeLayout rlCategorySync;
    ArrayAdapter<String> cat_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_category);

        ActionBar ab = getSupportActionBar();
        ab.setHomeButtonEnabled(true);

        db = new DatabaseHandler(this);

        tvCategoryTitle = (TextView) findViewById(R.id.tvCategoryTitle);
        btnCategoryOkay = (Button) findViewById(R.id.btnCategoryOkay);
        btnCategoryOkay.setOnClickListener(mBtnOnClickListener);
        lvCategory = (ListView) findViewById(R.id.lvCategory);
        tvCategoryTitle.setText("");
        cat_adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.row_category_item, R.id.tvCatName, empList);
        lvCategory.setAdapter(cat_adapter);

        rlCategorySync = (RelativeLayout) findViewById(R.id.rlCategorySync);

        if (!db.checkCategories()) {
            onCategoriesAPICall();
        } else {
            loadCategoriesFromDB();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cat_sync, menu);
        return super.onCreateOptionsMenu(menu);
    }


    public void onCategoriesAPICall () {
        final ProgressDialog pDialog = new ProgressDialog(this, R.style.MyTheme);
        pDialog.setCancelable(false);
        pDialog.show();

        String url = URLCollections.getBaseURLFromPreference(SyncCategories.this);
        if (url == null || url.equals("") || !URLCollections.isValidUrl(url)) {
            if(pDialog.isShowing())
                pDialog.dismiss();

            tvCategoryTitle.setText(getResources().getString(R.string.conn_string_not_define));
            lvCategory.setVisibility(View.INVISIBLE);
            btnCategoryOkay.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(rlCategorySync, getResources().getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getResources().getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener);
            snackbar.setActionTextColor(Color.GREEN);
            View snackbarView = snackbar.getView();
            snackbarView.setBackgroundColor(Color.DKGRAY);
            TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        } else if (GlobalClass.getInstance().isNetworkAvailable(this)) {
            lvCategory.setVisibility(View.INVISIBLE);
            btnCategoryOkay.setVisibility(View.GONE);
            tvCategoryTitle.setText(R.string.wait_must_types_syncing);
            // to fetch the data from the API for categories
            apiCategoryService =
                    new ApiClient(SyncCategories.this).getClient().create(ApiInterface.class);

            Call<Categories> call_categories = apiCategoryService.getAllCategories();
            call_categories.enqueue(new Callback<Categories>() {
                @Override
                public void onResponse(Call<Categories> call, Response<Categories> response) {
                    int statusCode = response.code();
                    Log.d(TAG, "response code" + statusCode);
                    if (statusCode != 200) {
                        if(pDialog.isShowing())
                            pDialog.dismiss();

                        lvCategory.setVisibility(View.INVISIBLE);
                        btnCategoryOkay.setVisibility(View.GONE);
                        tvCategoryTitle.setText(Html.fromHtml(getString(R.string.problem_with_sync)));
                    }

                    if (response.isSuccessful()) {
                        int status = 0;
                        String message = "";
                        ArrayList<Categories> data;
                        Categories responseObj = response.body();
                        status = responseObj.getStatus();
                        message = responseObj.getMessage();
                        data = responseObj.getData();
                        if (status == 1 && data != null && data.size() > 0) {
                            Log.d(TAG, "Mustering Message: " + message);
                            Log.d(TAG, "MusteringSize: " + data.size());
                            // to delete the table data before insert new
                            db.deleteAllCategories();
                            empList.clear();
                            /*for (int i = 0; i < data.size(); i++) {
                                Categories category = data.get(i);//new Categories();
                                int cat_id = category.getMusteringId();//dataObj.getInt(APIParam.RES_CAT_ID);
                                Log.d(TAG, "MustId: " + cat_id);
                                String cat_name = category.getMusteringName();//dataObj.getString(APIParam.RES_CAT_NAME);
                                Log.d(TAG, "MustName: " + cat_name);
                                //categoriesListArray[i] = cat_name;
                                db.addCategories(category);
                            }*/
                            db.saveCategories(data);

                            loadCategoriesFromDB();

                        } else if (status == -1) {
                            Log.d(TAG, "Mustering exception Message: " + message);
                            lvCategory.setVisibility(View.INVISIBLE);
                            btnCategoryOkay.setVisibility(View.GONE);
                            tvCategoryTitle.setText(Html.fromHtml(getString(R.string.problem_with_sync) + message));
                        }
                        // to call next API with incremented page
                        if(pDialog.isShowing())
                            pDialog.dismiss();
                    }

                }

                @Override
                public void onFailure(Call<Categories> call, Throwable t) {
                    // Log error here since request failed
                    lvCategory.setVisibility(View.INVISIBLE);
                    btnCategoryOkay.setVisibility(View.GONE);
                    Log.e(TAG, "Mustering Types Response error : " + t.toString());
                    tvCategoryTitle.setText(Html.fromHtml(getString(R.string.problem_with_sync)+getString(R.string.try_later_label)));
                    // to call next API with incremented page
                    if(pDialog.isShowing())
                        pDialog.dismiss();
                }
            });
        } else {
            if(pDialog.isShowing())
                pDialog.dismiss();

            tvCategoryTitle.setText(getResources().getString(R.string.no_conn));
            lvCategory.setVisibility(View.INVISIBLE);
            btnCategoryOkay.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(rlCategorySync, getResources().getString(R.string.no_conn), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getResources().getString(R.string.snack_action_refresh), mOnSnackBarClickListener);
            snackbar.setActionTextColor(Color.GREEN);
            View snackbarView = snackbar.getView();
            snackbarView.setBackgroundColor(Color.DKGRAY);
            TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    public void loadCategoriesFromDB () {
        ArrayList<Categories> list = db.getCategories();
        for (int k = 0; k < list.size(); k++) {
            empList.add(list.get(k).getMusteringName());
        }
        if (empList != null && empList.size() > 0) {
            lvCategory.setVisibility(View.VISIBLE);
            btnCategoryOkay.setVisibility(View.GONE);
            cat_adapter.notifyDataSetChanged();
            tvCategoryTitle.setText(Html.fromHtml(getString(R.string.sync_done_must_types)));
        } else {
            lvCategory.setVisibility(View.INVISIBLE);
            btnCategoryOkay.setVisibility(View.GONE);
            tvCategoryTitle.setText(Html.fromHtml(getString(R.string.sync_not_done_must_types)));
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(SyncActivity.REQUEST_SYNC_CATEGORIES);
            SyncCategories.this.finish();
            return true;
        } else if (item.getItemId() == R.id.action_cat_refresh) {
            onCategoriesAPICall();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public View.OnClickListener mOnSnackBarClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            System.out.println("SnackBar Refreshed Click");
            onCategoriesAPICall();
        }
    };

    public View.OnClickListener mOnSnackBarSetUrlClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            System.out.println("SnackBar Set URL Click");
            Intent int_set_url = new Intent(SyncCategories.this, MyPreferencesActivity.class);
            int_set_url.putExtra(HomeScreen.ACTION_STR, HomeScreen.SET_URL);
            startActivity(int_set_url);
        }
    };

    public View.OnClickListener mBtnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            System.out.println("BtnOkCLick");
            setResult(SyncActivity.REQUEST_SYNC_CATEGORIES);
            SyncCategories.this.finish();
        }
    };
}
