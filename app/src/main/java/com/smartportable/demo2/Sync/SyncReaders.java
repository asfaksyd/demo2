package com.smartportable.demo2.Sync;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.smartportable.demo2.R;
import com.smartportable.demo2.HomeScreen;
import com.smartportable.demo2.SyncActivity;
import com.smartportable.demo2.api.ApiClient;
import com.smartportable.demo2.api.ApiInterface;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.Categories;
import com.smartportable.demo2.helper.GlobalClass;
import com.smartportable.demo2.helper.PreferencesManager;
import com.smartportable.demo2.helper.PunchLogs;
import com.smartportable.demo2.interfaces.MultiChoiceListDialogInterface;
import com.smartportable.demo2.prefs.MyPreferencesActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ananth on 1/19/2017.
 */

public class SyncReaders extends AppCompatActivity implements MultiChoiceListDialogInterface {
    public static final String TAG = "SyncReaders";
    DatabaseHandler db;
    TextView tvReaderTitle;
    Button btnReaderOkay;
    ListView lvReader;
    ArrayAdapter reader_adapter;
    ArrayList<String> readersList = new ArrayList<String>();
    ApiInterface apiReaderService;
    List<Categories> must_list = new ArrayList<Categories>();
    ArrayList<Integer> selectedItemsIndexList = new ArrayList<Integer>();
    MultiChoiceListDialogInterface dialogInterface;
    String MusterID;

    RelativeLayout rlReaderSync, rlReaderCount;
    TextView tvReaderCount;
    Handler mHandler = null;
    Runnable mRunnable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_category);

        ActionBar ab = getSupportActionBar();
        ab.setHomeButtonEnabled(true);

        db = new DatabaseHandler(this);
        dialogInterface = this;

        rlReaderCount = (RelativeLayout) findViewById(R.id.rlAllEmpCount);
        tvReaderCount = (TextView) rlReaderCount.findViewById(R.id.tvAllEmpCount);

        tvReaderTitle = (TextView) findViewById(R.id.tvCategoryTitle);
        btnReaderOkay = (Button) findViewById(R.id.btnCategoryOkay);
        btnReaderOkay.setOnClickListener(mBtnReaderClickListener);
        lvReader = (ListView) findViewById(R.id.lvCategory);
        tvReaderTitle.setText("");
        reader_adapter = new ArrayAdapter<String>(SyncReaders.this, R.layout.row_category_item, R.id.tvCatName, readersList);
        lvReader.setAdapter(reader_adapter);

        lvReader.setOnTouchListener(mOnTouchListener);

        rlReaderSync = (RelativeLayout) findViewById(R.id.rlCategorySync);

        //checkSingleMusteringAndCallAPI();
        if (!db.checkReaders()) {
            checkMultiMusteringAndCallAPI();
        } else {
            loadReaderFromDB();
        }
    }

    public void checkSingleMusteringAndCallAPI () {
        if (db.checkCategories()) {
            must_list = db.getCategories();
            if (must_list.size() > 0) {
                Log.d(TAG, "Mustering Size"+must_list.size());
                final String[] mustArray = new String[must_list.size()];
                for (int i=0; i< must_list.size();i++) {
                    mustArray[i] = must_list.get(i).getMusteringName();
                }
                if (mustArray.length > 0) {
                    final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(SyncReaders.this);
                    //alt_bld.setIcon(R.drawable.icon);
                    alt_bld.setTitle("Set your mustering type first");
                    alt_bld.setSingleChoiceItems(mustArray, -1, new DialogInterface
                            .OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            Log.d(TAG, "Selected Mustering Id: " + mustArray[item]);
                            final int mustId = must_list.get(item).getMusteringId();
                            String mustName = must_list.get(item).getMusteringName();
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putInt(PreferencesManager.KEY_SET_MUSTER_ID, mustId);
                            editor.putString(PreferencesManager.KEY_SET_MUSTER_NAME, mustName);
                            editor.commit();

                            Snackbar snackbar = Snackbar
                                    .make(rlReaderSync, getApplicationContext().getResources().getString(R.string.set_muster_type, mustArray[item]), Snackbar.LENGTH_SHORT);
                            //.setAction(getResources().getString(R.string.snack_action_refresh), mOnSBAddSessionClick);
                            //snackbar.setActionTextColor(Color.WHITE);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.DKGRAY);
                            TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            textView.setTextColor(Color.WHITE);
                            snackbar.show();

                            snackbar.setCallback(new Snackbar.Callback() {
                                @Override
                                public void onDismissed(Snackbar snackbar, int event) {
                                    super.onDismissed(snackbar, event);
                                    // after set area change the activity
                                    onReadersAPICall(""+mustId);
                                }
                            });
                            // after set area change the activity
                            //startActivity(new Intent(LoadEmployeesLocation.this, MainActivity.class));

                            dialog.dismiss();// dismiss the alertbox after chose option

                        }
                    });
                    AlertDialog alert = alt_bld.create();
                    alert.show();
                }
            }
        } else {
            Log.d(TAG, "Mustering is not synced!");
        }
    }

    public void checkMultiMusteringAndCallAPI () {
        if (db.checkCategories()) {
            must_list = db.getCategories();
            if (must_list.size() > 0) {
                Log.d(TAG, "Mustering Size"+must_list.size());
                final String[] mustArray = new String[must_list.size()];

                for (int i=0; i< must_list.size();i++) {
                    mustArray[i] = must_list.get(i).getMusteringName();
                }
                if (mustArray.length > 0) {
                    final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    final boolean[] isSelectedArray = new boolean[mustArray.length];
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle(R.string.select_must_types)
                            .setMultiChoiceItems(mustArray, isSelectedArray, new DialogInterface.OnMultiChoiceClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                    if (isChecked) {
                                        selectedItemsIndexList.add(which);
                                    } else if (selectedItemsIndexList.contains(which)) {
                                        selectedItemsIndexList.remove(Integer.valueOf(which));
                                    }
                                }
                            })
                            .setNegativeButton(R.string.cancel_label, new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialogInterface.onCancel();
                                }
                            })
                            .setPositiveButton(getString(R.string.ok_label), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialogInterface.onOkay(selectedItemsIndexList);
                                }
                            });

                    builder.show();
                }
            }
        } else {
            Log.d(TAG, "Mustering is not synced!");
        }
    }

    @Override
    public void onCancel() {
        SyncReaders.this.finish();
    }

    @Override
    public void onOkay(ArrayList<Integer> arrayList) {
        if (arrayList.size() > 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                if (i == 0) {
                    MusterID = ""+must_list.get(arrayList.get(i)).getMusteringId();
                } else {
                    MusterID = MusterID + "," + must_list.get(arrayList.get(i)).getMusteringId();
                }
            }
            Log.d(TAG, "MusteringId" + MusterID);
            onReadersAPICall(MusterID);
        } else {
            Log.d(TAG, "No Muster Selected");
        }
        //new GetEmployeeLocationOnFilter(MainActivity.this).execute(categoryID, areaID);
    }

    public View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Log.d(TAG, "onTouch"+event.getAction());
            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:
                    /*int count = employee_adapter.getItemCount();
                    if (count > 0) {*/
                    if (mHandler != null) {
                        mHandler.removeCallbacks(mRunnable);
                    }
                    rlReaderCount.setVisibility(View.VISIBLE);
                    //}
                    return false;
                case MotionEvent.ACTION_DOWN:
                    if (mHandler != null) {
                        mHandler.removeCallbacks(mRunnable);
                    }
                    rlReaderCount.setVisibility(View.VISIBLE);
                    break;
                case MotionEvent.ACTION_UP:
                    if (mHandler != null) {
                        mHandler.removeCallbacks(mRunnable);
                    }
                    mHandler = new Handler();
                    mRunnable = new Runnable() {
                        @Override
                        public void run() {
                            rlReaderCount.setVisibility(View.GONE);
                        }
                    };
                    mHandler.postDelayed(mRunnable, 3000);
            }
            return false;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cat_sync, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public View.OnClickListener mBtnReaderClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SyncReaders.this.finish();
        }
    };

    private class LoadReaderFromDB extends AsyncTask {
        ProgressDialog pDialog;
        ArrayList<PunchLogs> data;
        public LoadReaderFromDB (ArrayList<PunchLogs> data, ProgressDialog pDialog) {
            this.pDialog = pDialog;
            this.data = data;
        }
        @Override
        protected Object doInBackground(Object[] params) {

            db.deleteAllReaders();
            readersList.clear();
            for (int i = 0; i < data.size(); i++) {
                PunchLogs reader = data.get(i);//new Categories();
                int reader_id = reader.getSLN_Reader();//dataObj.getInt(APIParam.RES_CAT_ID);
                Log.d(TAG, "ReaderId: " + reader_id);
                String reader_name = reader.getReader_Name();//dataObj.getString(APIParam.RES_CAT_NAME);
                Log.d(TAG, "ReaderName: " + reader_name);
                int mustering_id = reader.getMusteringId();
                Log.d(TAG, "Mustering ID: "+mustering_id);
                //categoriesListArray[i] = cat_name;
                db.addReaders(reader);
            }
            // Also clear the set reader id, name and mustering id
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncReaders.this);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt(PreferencesManager.KEY_SET_READER_ID, -1);
            editor.putString(PreferencesManager.KEY_SET_READER_NAME, "");
            editor.putInt(PreferencesManager.KEY_SET_MUSTER_ID, -2);
            editor.putInt(PreferencesManager.KEY_SET_SESSION_ID, -1);
            editor.putString(PreferencesManager.KEY_SET_SESSION_NAME, "");
            editor.putInt(PreferencesManager.KEY_SET_SESSION_START_TIME, 0);
            editor.putInt(PreferencesManager.KEY_SET_SESSION_END_TIME, 0);
            editor.putString(PreferencesManager.KEY_SET_SESSION_NAME, "");
            editor.commit();

            ArrayList<PunchLogs> list = new ArrayList<PunchLogs>();//db.getAllReaders();
            for (int k = 0; k < list.size(); k++) {
                readersList.add(list.get(k).getReader_Name());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (readersList != null && readersList.size() > 0) {
                lvReader.setVisibility(View.VISIBLE);
                btnReaderOkay.setVisibility(View.GONE);
                reader_adapter.notifyDataSetChanged();
                rlReaderCount.setVisibility(View.VISIBLE);
                int count = readersList.size();
                if (count > 0) {
                    tvReaderCount.setText(""+ URLCollections.getNumberFormat().format(count));
                }

                if (mHandler != null) {
                    mHandler.removeCallbacks(mRunnable);
                }

                mHandler = new Handler();
                mRunnable = new Runnable() {
                    @Override
                    public void run() {
                        rlReaderCount.setVisibility(View.GONE);
                    }
                };
                mHandler.postDelayed(mRunnable, 3000);

                tvReaderTitle.setText(Html.fromHtml(getString(R.string.sync_done_must_points)));
            } else {
                lvReader.setVisibility(View.INVISIBLE);
                btnReaderOkay.setVisibility(View.GONE);
                tvReaderTitle.setText(Html.fromHtml(getString(R.string.sync_not_done_must_points)));
            }


        }
    }
    // load reader from db
    public void loadReaderFromDB() {
        ArrayList<PunchLogs> list = new ArrayList<PunchLogs>(); //db.getAllReaders();
        for (int k = 0; k < list.size(); k++) {
            readersList.add(list.get(k).getReader_Name());
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (readersList != null && readersList.size() > 0) {
                    lvReader.setVisibility(View.VISIBLE);
                    btnReaderOkay.setVisibility(View.GONE);
                    reader_adapter.notifyDataSetChanged();
                    rlReaderCount.setVisibility(View.VISIBLE);
                    int count = readersList.size();
                    if (count > 0) {
                        tvReaderCount.setText(""+ URLCollections.getNumberFormat().format(count));
                    }

                    if (mHandler != null) {
                        mHandler.removeCallbacks(mRunnable);
                    }

                    mHandler = new Handler();
                    mRunnable = new Runnable() {
                        @Override
                        public void run() {
                            rlReaderCount.setVisibility(View.GONE);
                        }
                    };
                    mHandler.postDelayed(mRunnable, 3000);

                    tvReaderTitle.setText(Html.fromHtml(getString(R.string.sync_done_must_points)));
                } else {
                    lvReader.setVisibility(View.INVISIBLE);
                    btnReaderOkay.setVisibility(View.GONE);
                    tvReaderTitle.setText(Html.fromHtml(getString(R.string.sync_not_done_must_points)));
                }
            }
        });
    }

    String message = "";
    public void onReadersAPICall(String mustID) {
        final ProgressDialog pDialog = new ProgressDialog(this, R.style.MyTheme);
        pDialog.setCancelable(false);
        pDialog.show();

        String url = URLCollections.getBaseURLFromPreference(SyncReaders.this);
        if (url == null || url.equals("") || !URLCollections.isValidUrl(url)) {
            if(pDialog.isShowing())
                pDialog.dismiss();

            tvReaderTitle.setText(getResources().getString(R.string.conn_string_not_define));
            lvReader.setVisibility(View.INVISIBLE);
            btnReaderOkay.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(rlReaderSync, getResources().getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getResources().getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener);
            snackbar.setActionTextColor(Color.GREEN);
            View snackbarView = snackbar.getView();
            snackbarView.setBackgroundColor(Color.DKGRAY);
            TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        } else if (GlobalClass.getInstance().isNetworkAvailable(this)) {
            lvReader.setVisibility(View.INVISIBLE);
            btnReaderOkay.setVisibility(View.GONE);
            tvReaderTitle.setText(R.string.wait_must_readers_syncing);
            // to fetch the data from the API for categories
            apiReaderService = new ApiClient(SyncReaders.this).getClient().create(ApiInterface.class);

            Call<PunchLogs> call_readers = apiReaderService.getReadersList(mustID);

            call_readers.enqueue(new Callback<PunchLogs>() {
                @Override
                public void onResponse(Call<PunchLogs> call, Response<PunchLogs> response) {
                    int statusCode = response.code();
                    Log.d(TAG, "response code" + statusCode);

                    if (statusCode != 200) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(pDialog.isShowing())
                                    pDialog.dismiss();

                                lvReader.setVisibility(View.INVISIBLE);
                                btnReaderOkay.setVisibility(View.GONE);
                                tvReaderTitle.setText(Html.fromHtml(getResources().getString(R.string.problem_with_sync)));
                            }
                        });

                    }


                    if (response.isSuccessful()) {
                        int status = 0;

                        ArrayList<PunchLogs> data;
                        PunchLogs responseObj = response.body();
                        status = responseObj.getStatus();
                        message = responseObj.getMessage();
                        data = responseObj.getData();
                        if (status == 1 && data != null && data.size() > 0) {
                            Log.d(TAG, "Category Message: " + message);
                            Log.d(TAG, "categorySize: " + data.size());
                            // to delete the table data before insert new
                            /*db.deleteAllReaders();
                            readersList.clear();
                            for (int i = 0; i < data.size(); i++) {
                                PunchLogs reader = data.get(i);//new Categories();
                                int reader_id = reader.getSLN_Reader();//dataObj.getInt(APIParam.RES_CAT_ID);
                                Log.d(TAG, "ReaderId: " + reader_id);
                                String reader_name = reader.getReader_Name();//dataObj.getString(APIParam.RES_CAT_NAME);
                                Log.d(TAG, "ReaderName: " + reader_name);
                                int mustering_id = reader.getMusteringId();
                                Log.d(TAG, "Mustering ID: "+mustering_id);
                                //categoriesListArray[i] = cat_name;
                                db.addReaders(reader);
                            }
                            // Also clear the set reader id, name and mustering id
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncReaders.this);
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putInt(PreferencesManager.KEY_SET_READER_ID, -1);
                            editor.putString(PreferencesManager.KEY_SET_READER_NAME, "");
                            editor.putInt(PreferencesManager.KEY_SET_MUSTER_ID, -2);
                            editor.putInt(PreferencesManager.KEY_SET_SESSION_ID, -1);
                            editor.putString(PreferencesManager.KEY_SET_SESSION_NAME, "");
                            editor.putInt(PreferencesManager.KEY_SET_SESSION_START_TIME, 0);
                            editor.putInt(PreferencesManager.KEY_SET_SESSION_END_TIME, 0);
                            editor.putString(PreferencesManager.KEY_SET_SESSION_NAME, "");
                            editor.commit();*/

                            /*runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(pDialog.isShowing())
                                        pDialog.dismiss();
                                }
                            });*/

                            //loadReaderFromDB();
                            new LoadReaderFromDB(data, pDialog).execute();

                        } else if (status == -1) {
                            Log.d(TAG, "Readers exception Message: " + message);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    lvReader.setVisibility(View.INVISIBLE);
                                    btnReaderOkay.setVisibility(View.GONE);
                                    tvReaderTitle.setText(Html.fromHtml(getString(R.string.problem_with_sync) + message));
                                }
                            });

                        }
                        // to call next API with incremented page

                        /*runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(pDialog.isShowing())
                                    pDialog.dismiss();
                            }
                        });*/

                    }
                }

                @Override
                public void onFailure(Call<PunchLogs> call, Throwable t) {
                    // Log error here since request failed
                    lvReader.setVisibility(View.INVISIBLE);
                    btnReaderOkay.setVisibility(View.GONE);
                    Log.e(TAG, "Reader Response error : " + t.toString());
                    tvReaderTitle.setText(Html.fromHtml(getString(R.string.problem_with_sync)+getString(R.string.try_later_label)));
                    // to call next API with incremented page
                    if(pDialog.isShowing())
                        pDialog.dismiss();
                }
            });
        } else {
            if(pDialog.isShowing())
                pDialog.dismiss();

            tvReaderTitle.setText(getResources().getString(R.string.no_conn));
            lvReader.setVisibility(View.INVISIBLE);
            btnReaderOkay.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(rlReaderSync, getResources().getString(R.string.no_conn), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getResources().getString(R.string.snack_action_refresh), mOnSnackBarClickListener);
            snackbar.setActionTextColor(Color.GREEN);
            View snackbarView = snackbar.getView();
            snackbarView.setBackgroundColor(Color.DKGRAY);
            TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(SyncActivity.REQUEST_SYNC_CATEGORIES);
            SyncReaders.this.finish();
            return true;
        } else if (item.getItemId() == R.id.action_cat_refresh) {
            //checkSingleMusteringAndCallAPI();
            checkMultiMusteringAndCallAPI();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public View.OnClickListener mOnSnackBarClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            System.out.println("SnackBar Refreshed Click");
            //checkSingleMusteringAndCallAPI();
            checkMultiMusteringAndCallAPI();
        }
    };

    public View.OnClickListener mOnSnackBarSetUrlClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            System.out.println("SnackBar Set URL Click");
            Intent int_set_url = new Intent(SyncReaders.this, MyPreferencesActivity.class);
            int_set_url.putExtra(HomeScreen.ACTION_STR, HomeScreen.SET_URL);
            startActivity(int_set_url);
        }
    };

    public View.OnClickListener mBtnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            System.out.println("BtnOkCLick");
            setResult(SyncActivity.REQUEST_SYNC_CATEGORIES);
            SyncReaders.this.finish();
        }
    };
}
