package com.smartportable.demo2.Sync;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import com.google.android.material.snackbar.Snackbar;

import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.smartportable.demo2.R;
import com.smartportable.demo2.HomeScreen;
import com.smartportable.demo2.adapters.ListItemAdapter;
import com.smartportable.demo2.api.ApiClient;
import com.smartportable.demo2.api.ApiInterface;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.Config;
import com.smartportable.demo2.helper.Employee;
import com.smartportable.demo2.helper.GlobalClass;
import com.smartportable.demo2.helper.PreferencesManager;
import com.smartportable.demo2.helper.PunchLogs;
import com.smartportable.demo2.helper.RecyclerListSelectorModel;
import com.smartportable.demo2.interfaces.OnLoadMoreListener;
import com.smartportable.demo2.interfaces.SelectedListListener;
import com.smartportable.demo2.prefs.MyPreferencesActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.annotation.Nullable;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.danoz.recyclerviewfastscroller.vertical.VerticalRecyclerViewFastScroller;

/**
 * Created by Ananth on 1/5/2017.
 */

public class SyncAllEmployees extends AppCompatActivity implements SelectedListListener, SearchView.OnQueryTextListener {

    ApiInterface apiAllEmployeeService;
    private static String TAG = "SyncAllEmployees";
    DatabaseHandler db;
    ArrayList<Employee> empList = new ArrayList<Employee>();
    ArrayList<RecyclerListSelectorModel> listBoolean = new ArrayList<RecyclerListSelectorModel>();
    TextView tvAllEmployeeTitle;
    //ListView
    RecyclerView lvAllEmployee;
    VerticalRecyclerViewFastScroller fastScrollerAllEmp;
    Button btnAllEmployeeOkay;
    ListItemAdapter employee_adapter;

    ProgressDialog pDialog;
    int progressPercentage;
    int firstVal = 0;
    int lastVal = 0;
    double totalRecords = 0;

    // request part
    int req_mode = 0;
    String req_timestamp = "";
    int currentSyncPage = 1;
    int req_send_Photo = 0;
    String req_emp_id = "Test";
    int req_card_mode = URLCollections.CARD_MODE;

    ActionBar ab;
    Menu menu;
    boolean flag_loading = false;
    View footerView;
    TextView tcPBTitle;
    ProgressBar pbLoadMore;
    boolean displayDoneOption = false;
    MenuItem searchMenuItem, moreMenuItem;
    SearchView searchView;
    RelativeLayout rlAllEmployeeSync, rlAllEmpCount;
    TextView tvAllEmpCount;
    Handler mHandler = null;
    Runnable mRunnable;

    @Override
    public void onListChanged(ArrayList<RecyclerListSelectorModel> list) {
        listBoolean = list;
        for (int i=0; i<list.size();i++) {
            if (list.get(i).isSelected()) {
                displayDoneOption = true;
                break;
            }
        }
        invalidateOptionsMenu();
    }

    @Override
    public void onPunchListChanged(ArrayList<PunchLogs> list) {

    }

    @Override
    public void onItemRemoved(String card_no) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_all_employee);

        // footer view
        footerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_layout, null, false);
        tcPBTitle = (TextView) footerView.findViewById(R.id.tvPBTitle);
        pbLoadMore = (ProgressBar) footerView.findViewById(R.id.pbLoadMore);

        ab = getSupportActionBar();
        ab.setHomeButtonEnabled(true);

        db = new DatabaseHandler(this);

        rlAllEmpCount = (RelativeLayout) findViewById(R.id.rlAllEmpCount);
        tvAllEmpCount = (TextView) rlAllEmpCount.findViewById(R.id.tvAllEmpCount);
        rlAllEmployeeSync = (RelativeLayout) findViewById(R.id.rlAllEmployeeSync);
        tvAllEmployeeTitle = (TextView) findViewById(R.id.tvAllEmployeeTitle);
        btnAllEmployeeOkay = (Button) findViewById(R.id.btnAllEmployeeOkay);
        btnAllEmployeeOkay.setOnClickListener(mBtnOnClickListener);
        lvAllEmployee = (RecyclerView) findViewById(R.id.lvAllEmployee);
        fastScrollerAllEmp = (VerticalRecyclerViewFastScroller) findViewById(R.id.fastScrollerAllEmp);
        tvAllEmployeeTitle.setText("");

        // main relative layout touch event to hide and show the total count of the employee in the list
        lvAllEmployee.setOnTouchListener(mOnTouchListener);

        /*lvAllEmployee.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                obtainMotionEventObject(lvAllEmployee);
            }
        });*/

        //lvAllEmployee.addFooterView(footerView);

        //lvAllEmployee.setFastScrollEnabled(true);

        lvAllEmployee.setHasFixedSize(true);
        lvAllEmployee.setLayoutManager(new LinearLayoutManager(this));

        // Connect the recycler to the scroller (to let the scroller scroll the list)
        fastScrollerAllEmp.setRecyclerView(lvAllEmployee);

        // Connect the scroller to the recycler (to let the recycler scroll the scroller's handle)
        //lvAllEmployee.setOnScrollListener(fastScrollerAllEmp.getOnScrollListener());
        lvAllEmployee.addOnScrollListener(fastScrollerAllEmp.getOnScrollListener());

        employee_adapter = new ListItemAdapter(this, empList, lvAllEmployee, listBoolean, getSupportFragmentManager());
        lvAllEmployee.setAdapter(employee_adapter);

        employee_adapter.setListener(this);

        employee_adapter.setOnLoadMoreListener(mLoadMoreListener);
        /*lvAllEmployee.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount!=0)
                {
                    if(flag_loading == false)
                    {
                        lvAllEmployee.addFooterView(footerView);
                        //footerView.setVisibility(View.VISIBLE);
                        flag_loading = true;
                        firstVal = lastVal + 1;
                        lastVal = firstVal + 99;
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    loadAllEmployeeFromDB();
                                                }
                                            }, 1000);

                    }
                }
            }
        });*/
        rlAllEmployeeSync = (RelativeLayout) findViewById(R.id.rlAllEmployeeSync);
        if (!db.checkEmployees()) {
            // Alert Dialog
            AlertDialog.Builder mDialog = new AlertDialog.Builder(this);
            LayoutInflater adbInflater = LayoutInflater.from(this);
            View eulaLayout = adbInflater.inflate(R.layout.dialog_checkbox, null);

            final CheckBox sync_with_image = (CheckBox) eulaLayout.findViewById(R.id.sync_with_image);
            mDialog.setView(eulaLayout);
            mDialog.setTitle(getResources().getString(R.string.sync_warning_title));
            mDialog.setMessage(Html.fromHtml(getString(R.string.sync_warning_all_emp_message)));
            mDialog.setCancelable(false);

            mDialog.setPositiveButton(getResources().getString(R.string.yes_label), new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pDialog = new ProgressDialog(SyncAllEmployees.this);//, R.style.MyTheme);
                    pDialog.setMessage(getString(R.string.fetch_emp_data_label));
                    pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pDialog.setIndeterminate(false);
                    pDialog.setProgress(progressPercentage);
                    pDialog.setCancelable(false);
                    pDialog.setProgressNumberFormat(null);
                    pDialog.show();
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncAllEmployees.this);
                    String last_synced_date = prefs.getString(PreferencesManager.KEY_SET_LAST_SYNCED_TIMESTAMP, "");
                    if (last_synced_date != null  && last_synced_date.length() > 0) {
                        req_timestamp = last_synced_date ;
                    }else {
                        // to set for all employees
                        // for current date
                        Calendar c = Calendar.getInstance();
                        //System.out.println("Current time => "+c.getTime());
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Config.DATETIME_FORMAT_LOCAL);
                        req_timestamp = df.format(c.getTime());
                    }
                    Log.d(TAG, "Current date: "+req_timestamp);
                    if(sync_with_image.isChecked()) {
                        req_send_Photo = 1;
                    }
                    onEmployeesAPICall();
                }
            }).setNegativeButton(getResources().getString(R.string.no_label), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.d(TAG, "Negative button click");
                }
            });
            mDialog.show();
        } else {
            //lvAllEmployee.addFooterView(footerView);
            //footerView.setVisibility(View.VISIBLE);
            firstVal = 0;
            lastVal = 0;
            firstVal = lastVal + 1;
            lastVal = firstVal + 99;
            //lastVal = 23000;
            loadAllEmployeeFromDB();
        }
    }

    public void obtainMotionEventObject (View view) {
        long downTime = System.currentTimeMillis();
        long eventTime = System.currentTimeMillis() + 100;
        float x = 0.0f;
        float y = 0.0f;
        // List of meta states found here:     developer.android.com/reference/android/view/KeyEvent.html#getMetaState()
        int metaState = 0;
        MotionEvent motionEvent = MotionEvent.obtain(
                downTime,
                eventTime,
                MotionEvent.ACTION_UP,
                x,
                y,
                metaState
        );

        // Dispatch touch event to view
        mOnTouchListener.onTouch(view, motionEvent);
    }

    public View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Log.d(TAG, "onTouch"+event.getAction());
            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:
                    /*int count = employee_adapter.getItemCount();
                    if (count > 0) {*/
                    if (mHandler != null) {
                        mHandler.removeCallbacks(mRunnable);
                    }
                    rlAllEmpCount.setVisibility(View.VISIBLE);
                    //}
                    return false;
                case MotionEvent.ACTION_DOWN:
                    if (mHandler != null) {
                        mHandler.removeCallbacks(mRunnable);
                    }
                    rlAllEmpCount.setVisibility(View.VISIBLE);
                    break;
                case MotionEvent.ACTION_UP:
                    if (mHandler != null) {
                        mHandler.removeCallbacks(mRunnable);
                    }
                    mHandler = new Handler();
                    mRunnable = new Runnable() {
                        @Override
                        public void run() {
                            rlAllEmpCount.setVisibility(View.GONE);
                        }
                    };
                    mHandler.postDelayed(mRunnable, 3000);
            }
            return false;
        }
    };

    /*@Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, v.getId(), 0, "Sync with photos");
        menu.add(0, v.getId(), 1, "Sync without photos");
        super.onCreateContextMenu(menu, v, menuInfo);
    }*/

    public OnLoadMoreListener mLoadMoreListener = new OnLoadMoreListener() {
        @Override public void onLoadMore() {
            Log.e("haint", "Load More");
            empList.add(null);
            employee_adapter.notifyItemInserted(empList.size() - 1);
            //Load more data for reyclerview
            new Handler().postDelayed(new Runnable() {
                @Override public void run() {
                    Log.e("haint", "Load More 2");
                    //Remove loading item
                    empList.remove(empList.size() - 1);
                    employee_adapter.notifyItemRemoved(empList.size()+1);


                    flag_loading = true;
                    firstVal = lastVal + 1;
                    lastVal = firstVal + 99;
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadAllEmployeeFromDB();
                        }
                    }, 1000);
                }
            }, 1000);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_employee, menu);

        // for search query
        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);

        moreMenuItem = menu.findItem(R.id.action_employee_more_menu);
        searchMenuItem = menu.findItem(R.id.action_employee_search);
        searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint(getString(R.string.search_query_hint));

        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            //MenuItem menuCategory = menu.findItem(R.id.action_category);
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                //searchMenuItem.expandActionView();
                //menuCategory.setVisible(false);
                moreMenuItem.setVisible(true);
                searchView.setQuery("", false);
                searchView.setIconified(false);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                //searchMenuItem.collapseActionView();
                //menuCategory.setVisible(true);
                moreMenuItem.setVisible(true);
                searchView.setQuery("", false);
                searchView.setIconified(true);

                if (db.checkEmployees()) {
                    firstVal = 0;
                    lastVal = 0;
                    firstVal = lastVal + 1;
                    lastVal = firstVal + 99;
                    //lastVal = 23000;
                    if (empList.size() > 0)
                        empList.clear();

                    loadAllEmployeeFromDB();
                }

                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //boolean selectedEmp = listBoolean.contains(true);
        if (ab.getTitle().equals(getString(R.string.select_emp))) {
            menu.findItem(R.id.action_employee_more_menu).setVisible(false);
            //menu.findItem(R.id.action_new_emp_refresh).setVisible(false);
            //menu.findItem(R.id.action_selected_emp_refresh).setVisible(false);
            if (displayDoneOption) {
                menu.findItem(R.id.action_selected_done).setVisible(true);
                displayDoneOption = false;
            } else {
                menu.findItem(R.id.action_selected_done).setVisible(false);
            }
        } else {
            menu.findItem(R.id.action_employee_more_menu).setVisible(true);
            //menu.findItem(R.id.action_new_emp_refresh).setVisible(true);
            //menu.findItem(R.id.action_selected_emp_refresh).setVisible(true);
            menu.findItem(R.id.action_selected_done).setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            //setResult(SyncActivity.REQUEST_SYNC_EMPLOYEES);
            if (ab.getTitle().equals(getString(R.string.select_emp))) {
                //lvAllEmployee.setChoiceMode(ListView.CHOICE_MODE_NONE);
                /*if (this.listBoolean.contains(true)) {
                    for (int k=0; k<listBoolean.size(); k++) {
                        if (listBoolean.get(k).isSelected()) {
                            RecyclerListSelectorModel model = new RecyclerListSelectorModel();
                            model.setSelected(false);
                            listBoolean.set(k, model);
                        }
                    }
                }
                employee_adapter.updateBooleanLList(listBoolean);*/
                employee_adapter.enableCheckBox(false);
                employee_adapter.setOnLoadMoreListener(mLoadMoreListener);
                employee_adapter.notifyDataSetChanged();

                int count = employee_adapter.getItemCount();
                if (count > 0) {
                    tvAllEmpCount.setText(""+ URLCollections.getNumberFormat().format(count));
                }

                if (mHandler != null) {
                    mHandler.removeCallbacks(mRunnable);
                }

                mHandler = new Handler();
                mRunnable = new Runnable() {
                    @Override
                    public void run() {
                        rlAllEmpCount.setVisibility(View.GONE);
                    }
                };
                mHandler.postDelayed(mRunnable, 3000);

                ab.setTitle(getResources().getString(R.string.sync_employees));
                ab.setDisplayHomeAsUpEnabled(true);
                invalidateOptionsMenu();
            } else {
                SyncAllEmployees.this.finish();
            }
            return true;
        } else if (item.getItemId() == R.id.action_employee_search) {
            searchMenuItem.expandActionView();
        } else if (item.getItemId() == R.id.action_all_emp_without_photo) { // all employee without photos
            // Alert to show before syc start
            AlertDialog.Builder mDialog = new AlertDialog.Builder(this);
            mDialog.setTitle(getString(R.string.sync_warning_title));
            mDialog.setMessage(Html.fromHtml(getString(R.string.sync_warning_all_emp_message)));
            mDialog.setCancelable(false);

            mDialog.setPositiveButton(getString(R.string.yes_label), new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncAllEmployees.this);
                    String last_synced_date = prefs.getString(PreferencesManager.KEY_SET_LAST_SYNCED_TIMESTAMP, "");
                    if (last_synced_date != null  && last_synced_date.length() > 0) {
                        req_timestamp = last_synced_date ;
                    }else {
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Config.DATETIME_FORMAT_LOCAL);
                        req_timestamp = df.format(c.getTime());
                    }
                    Log.d(TAG, "Current date: "+req_timestamp);

                    req_mode = 0;
                    req_send_Photo = 0;
                    currentSyncPage = 1;
                    req_emp_id = "Test";

                    pDialog = new ProgressDialog(SyncAllEmployees.this);//, R.style.MyTheme);
                    pDialog.setMessage(getString(R.string.fetch_emp_data_label));
                    pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pDialog.setIndeterminate(false);
                    pDialog.setProgress(progressPercentage);
                    pDialog.setCancelable(false);
                    pDialog.setProgressNumberFormat(null);
                    pDialog.show();
                    onEmployeesAPICall();
                }
            }).setNegativeButton(getString(R.string.no_label), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.d(TAG, "Negative nutton click");
                }
            });
            mDialog.show();
            return true;
        } else if (item.getItemId() == R.id.action_all_emp_with_photo) { // all employee with photos
            // Alert to show before syc start
            AlertDialog.Builder mDialog = new AlertDialog.Builder(this);
            mDialog.setTitle(getString(R.string.sync_warning_title));
            mDialog.setMessage(Html.fromHtml(getString(R.string.sync_warning_all_emp_message)));
            mDialog.setCancelable(false);

            mDialog.setPositiveButton(getString(R.string.yes_label), new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    req_mode = 0;
                    req_send_Photo = 1;
                    currentSyncPage = 1;
                    req_emp_id = "Test";
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncAllEmployees.this);
                    String last_synced_date = prefs.getString(PreferencesManager.KEY_SET_LAST_SYNCED_TIMESTAMP, "");
                    if (last_synced_date != null  && last_synced_date.length() > 0) {
                        req_timestamp = last_synced_date ;
                    }else {
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Config.DATETIME_FORMAT_LOCAL);
                        req_timestamp = df.format(c.getTime());
                    }
                    Log.d(TAG, "Current date: "+req_timestamp);
                    pDialog = new ProgressDialog(SyncAllEmployees.this);//, R.style.MyTheme);
                    pDialog.setMessage(getString(R.string.fetch_emp_data_label));
                    pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pDialog.setIndeterminate(false);
                    pDialog.setProgress(progressPercentage);
                    pDialog.setCancelable(false);
                    pDialog.setProgressNumberFormat(null);
                    pDialog.show();
                    onEmployeesAPICall();
                }
            }).setNegativeButton(getString(R.string.no_label), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.d(TAG, "Negative nutton click");
                }
            });
            mDialog.show();
            return true;
        } else if (item.getItemId() == R.id.action_new_emp_without_photo) { // new employee without photos
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncAllEmployees.this);
            String last_synced_date = prefs.getString(PreferencesManager.KEY_SET_LAST_SYNCED_TIMESTAMP, "");
            if (last_synced_date != null  && last_synced_date.length() > 0) {
                req_timestamp = last_synced_date ;
            }else {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Config.DATETIME_FORMAT_LOCAL);
                req_timestamp = df.format(c.getTime());
            }
            Log.d(TAG, "Current date: "+req_timestamp);

            req_mode = 1;
            req_send_Photo = 0;
            currentSyncPage = 1;
            req_emp_id = "Test";
            pDialog = new ProgressDialog(this);//, R.style.MyTheme);
            pDialog.setMessage(getString(R.string.fetch_emp_data_label));
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setIndeterminate(false);
            pDialog.setProgress(progressPercentage);
            pDialog.setCancelable(false);
            pDialog.setProgressNumberFormat(null);
            pDialog.show();
            onEmployeesAPICall();
            return true;
        } else if (item.getItemId() == R.id.action_new_emp_with_photo) { // new employee with photos
            req_mode = 1;
            req_send_Photo = 1;
            currentSyncPage = 1;
            req_emp_id = "Test";
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncAllEmployees.this);
            String last_synced_date = prefs.getString(PreferencesManager.KEY_SET_LAST_SYNCED_TIMESTAMP, "");
            if (last_synced_date != null  && last_synced_date.length() > 0) {
                req_timestamp = last_synced_date ;
            }else {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss", Config.DATETIME_FORMAT_LOCAL);
                req_timestamp = df.format(c.getTime());
            }
            Log.d(TAG, "Current date: "+req_timestamp);
            pDialog = new ProgressDialog(this);//, R.style.MyTheme);
            pDialog.setMessage(getString(R.string.fetch_emp_data_label));
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setIndeterminate(false);
            pDialog.setProgress(progressPercentage);
            pDialog.setCancelable(false);
            pDialog.setProgressNumberFormat(null);
            pDialog.show();
            onEmployeesAPICall();
            return true;
        }else if (item.getItemId() == R.id.action_selected_emp_without_photo) { // selected employee without photos
            //lvAllEmployee.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            if (listBoolean.size() > 0) {
                listBoolean.clear();
            }
            for (int k=0; k<empList.size(); k++) {
                RecyclerListSelectorModel model = new RecyclerListSelectorModel();
                model.setSelected(false);
                listBoolean.add(model);
            }
            employee_adapter.updateBooleanLList(listBoolean);
            employee_adapter.enableCheckBox(true);
            employee_adapter.setOnLoadMoreListener(mLoadMoreListener);
            employee_adapter.notifyDataSetChanged();
            int count = employee_adapter.getItemCount();
            if (count > 0) {
                tvAllEmpCount.setText(""+ URLCollections.getNumberFormat().format(count));
            }

            if (mHandler != null) {
                mHandler.removeCallbacks(mRunnable);
            }

            mHandler = new Handler();
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    rlAllEmpCount.setVisibility(View.GONE);
                }
            };
            mHandler.postDelayed(mRunnable, 3000);

            ab.setHomeButtonEnabled(true);
            ab.setTitle(getString(R.string.select_emp));

            invalidateOptionsMenu();

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncAllEmployees.this);
            String last_synced_date = prefs.getString(PreferencesManager.KEY_SET_LAST_SYNCED_TIMESTAMP, "");
            if (last_synced_date != null  && last_synced_date.length() > 0) {
                req_timestamp = last_synced_date ;
            }else {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Config.DATETIME_FORMAT_LOCAL);
                req_timestamp = df.format(c.getTime());
            }
            Log.d(TAG, "Current date: "+req_timestamp);

            req_mode = 2;
            req_send_Photo = 0;
            currentSyncPage = 1;
            /*
            pDialog = new ProgressDialog(this);//, R.style.MyTheme);
            pDialog.setMessage("Fetching selected employees data");
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setIndeterminate(false);
            pDialog.setProgress(progressPercentage);
            pDialog.setCancelable(false);
            pDialog.setProgressNumberFormat(null);
            pDialog.show();
            onMusteringReadersAPICall();*/
            return true;
        } else if (item.getItemId() == R.id.action_selected_emp_with_photo) { // selected employee with photos
            //lvAllEmployee.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            if (listBoolean.size() > 0) {
                listBoolean.clear();
            }
            for (int k=0; k<empList.size(); k++) {
                RecyclerListSelectorModel model = new RecyclerListSelectorModel();
                model.setSelected(false);
                listBoolean.add(model);
            }
            employee_adapter.updateBooleanLList(listBoolean);
            employee_adapter.enableCheckBox(true);
            employee_adapter.setOnLoadMoreListener(mLoadMoreListener);
            employee_adapter.notifyDataSetChanged();
            int count = employee_adapter.getItemCount();
            if (count > 0) {
                tvAllEmpCount.setText(""+ URLCollections.getNumberFormat().format(count));
            }

            if (mHandler != null) {
                mHandler.removeCallbacks(mRunnable);
            }

            mHandler = new Handler();
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    rlAllEmpCount.setVisibility(View.GONE);
                }
            };
            mHandler.postDelayed(mRunnable, 3000);

            ab.setHomeButtonEnabled(true);
            ab.setTitle(getString(R.string.select_emp));

            invalidateOptionsMenu();

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncAllEmployees.this);
            String last_synced_date = prefs.getString(PreferencesManager.KEY_SET_LAST_SYNCED_TIMESTAMP, "");
            if (last_synced_date != null  && last_synced_date.length() > 0) {
                req_timestamp = last_synced_date ;
            }else {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss", Config.DATETIME_FORMAT_LOCAL);
                req_timestamp = df.format(c.getTime());
            }
            Log.d(TAG, "Current date: "+req_timestamp);

            req_mode = 2;
            req_send_Photo = 1;
            currentSyncPage = 1;
            return true;
        } else if (item.getItemId() == R.id.action_selected_done) {
            //listBoolean = employee_adapter.getBooleanList();
            int total_count = listBoolean.size();
            Log.d(TAG, "Total Count"+total_count);
            System.out.println("Total Count"+total_count+" Contains: "+listBoolean.contains(true));

            if (total_count > 0) {
                /*SparseBooleanArray itemArray = lvAllEmployee.getCheckedItemPositions();
                long[] ids = lvAllEmployee.getCheckedItemIds();
                Log.d(TAG, "POS"+ids.length);
                for (int j=0; j<ids.length;j++) {
                    int pos = Long.valueOf(ids[j]).intValue();
                    Log.d(TAG, "POS: "+pos);
                }
                for (int i=0; i<itemArray.size();i++) {
                    boolean p = itemArray.get(i);
                    Log.d(TAG, "IDs : "+itemArray.get(i));
                }*/

                for (int k=0; k<empList.size();k++) {
                    if (listBoolean.get(k).isSelected()) {
                        int empID = empList.get(k).getSLN_Employee();
                        Log.d(TAG, "EmployeeID: " + empID);
                        if (req_emp_id.equals("Test")) {
                            req_emp_id = ""+empID;
                        }else {
                            req_emp_id = req_emp_id + "," + empID;
                        }
                        RecyclerListSelectorModel model = new RecyclerListSelectorModel();
                        model.setSelected(false);
                        listBoolean.set(k, model);
                    }
                }
                // to hide the checkbox and back to main page
                //emply the current list
                /*lvAllEmployee.clearChoices();
                lvAllEmployee.setChoiceMode(ListView.CHOICE_MODE_NONE);*/
                /*listBoolean.clear();
                for (int k=0; k<empList.size();k++) {
                    RecyclerListSelectorModel model = new RecyclerListSelectorModel();
                    model.setSelected(false);
                    listBoolean.add(model);
                }*/
                employee_adapter.updateBooleanLList(listBoolean);

                employee_adapter.enableCheckBox(false);
                employee_adapter.setOnLoadMoreListener(mLoadMoreListener);
                employee_adapter.notifyDataSetChanged();
                int count = employee_adapter.getItemCount();
                if (count > 0) {
                    tvAllEmpCount.setText(""+ URLCollections.getNumberFormat().format(count));
                }

                if (mHandler != null) {
                    mHandler.removeCallbacks(mRunnable);
                }

                mHandler = new Handler();
                mRunnable = new Runnable() {
                    @Override
                    public void run() {
                        rlAllEmpCount.setVisibility(View.GONE);
                    }
                };
                mHandler.postDelayed(mRunnable, 3000);

                ab.setTitle(getResources().getString(R.string.sync_employees));
                ab.setDisplayHomeAsUpEnabled(true);
                invalidateOptionsMenu();

                pDialog = new ProgressDialog(this);//, R.style.MyTheme);
                pDialog.setMessage(getString(R.string.fetch_selected_emp_data_label));
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setIndeterminate(false);
                pDialog.setProgress(progressPercentage);
                pDialog.setCancelable(false);
                pDialog.setProgressNumberFormat(null);
                pDialog.show();
                onEmployeesAPICall();
            } else {
                Snackbar snackbar = Snackbar
                        .make(rlAllEmployeeSync, R.string.sel_emp_from_list, Snackbar.LENGTH_LONG);
                        //.setAction(getResources().getString(R.string.snack_action_refresh), mOnSBAddSessionClick);
                //snackbar.setActionTextColor(Color.GREEN);
                View snackbarView = snackbar.getView();
                snackbarView.setBackgroundColor(Color.DKGRAY);
                TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.show();
            }

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public View.OnClickListener mOnSnackBarClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            System.out.println("SnackBar Refreshed Click");
            onEmployeesAPICall();
        }
    };

    public View.OnClickListener mOnSnackBarSetUrlClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            System.out.println("SnackBar Set URL Click");
            Intent int_set_url = new Intent(SyncAllEmployees.this, MyPreferencesActivity.class);
            int_set_url.putExtra(HomeScreen.ACTION_STR, HomeScreen.SET_URL);
            startActivity(int_set_url);
        }
    };

    public void onEmployeesAPICall () {
        String url = URLCollections.getBaseURLFromPreference(SyncAllEmployees.this);
        if (url == null || url.equals("") || !URLCollections.isValidUrl(url)) {
            if(pDialog.isShowing())
                pDialog.dismiss();

            tvAllEmployeeTitle.setText(getResources().getString(R.string.conn_string_not_define));
            lvAllEmployee.setVisibility(View.INVISIBLE);
            btnAllEmployeeOkay.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(rlAllEmployeeSync, getResources().getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getResources().getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener);
            snackbar.setActionTextColor(Color.GREEN);
            View snackbarView = snackbar.getView();
            snackbarView.setBackgroundColor(Color.DKGRAY);
            TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        } else if (GlobalClass.getInstance().isNetworkAvailable(this)) {
            //lvAllEmployee.setVisibility(View.INVISIBLE);
            //btnAllEmployeeOkay.setVisibility(View.INVISIBLE);
            tvAllEmployeeTitle.setText(R.string.wait_all_emp_syncing);
            // to fetch the data from the API for categories
            apiAllEmployeeService =
                    new ApiClient(getApplicationContext()).getClient().create(ApiInterface.class);

            Log.d(TAG, "CurrentPage: "+ currentSyncPage);
            Log.d(TAG, "req_mode, req_send_Photo, req_timestamp, currentSyncPage, empId : "+req_mode + req_send_Photo + ""+ req_timestamp +""+ currentSyncPage +"" + req_emp_id);
            Call<Employee> call_employees = apiAllEmployeeService.getAllEmployees(req_mode, req_send_Photo, req_timestamp, currentSyncPage, req_emp_id, req_card_mode);
            call_employees.enqueue(new Callback<Employee>() {
                @Override
                public void onResponse(Call<Employee> call, Response<Employee> response) {
                    int statusCode = response.code();
                    //Log.d(TAG, "response code: " + statusCode+" Response Message: "+response.message()+" Response success: "+response.isSuccessful()+" Response body: "+response.body());
                    if (statusCode != 200) {
                        if(pDialog.isShowing())
                            pDialog.dismiss();

                        lvAllEmployee.setVisibility(View.INVISIBLE);
                        btnAllEmployeeOkay.setVisibility(View.GONE);
                        tvAllEmployeeTitle.setText(Html.fromHtml(getResources().getString(R.string.problem_with_sync)));
                    }

                    if (response.isSuccessful()) {
                        int status = 0;
                        String message = "";
                        ArrayList<Employee> data;
                        Employee responseObj = response.body();
                        status = responseObj.getStatus();
                        message = responseObj.getMessage();
                        data = responseObj.getData();
                        if (status == 1 && data != null && data.size() > 0) {
                            Log.d(TAG, "All Employee Message: " + message);
                            Log.d(TAG, "All Employee Size: " + data.size());
                            // to delete the table data before insert new
                                if (message.contains("*")) {
                                    message = message.replace("*", ",");
                                    String[] recordsDetails = message.split(",");
                                    totalRecords = Integer.parseInt(recordsDetails[1]);
                                    if (currentSyncPage == 1) {
                                        if (req_mode == 0) {
                                            db.deleteAllEmployees(SyncAllEmployees.this);
                                        }
                                        empList.clear();
                                    }

                                    if (req_mode == 2) {
                                        if (!req_emp_id.equals("Test")) {
                                            if (req_emp_id.contains(",")) {
                                                String[] empIdArray = req_emp_id.split(",");
                                                int empIdSize = empIdArray.length;
                                                for (int j = 0; j < empIdSize; j++) {
                                                    db.deleteAvtarFromFileName(SyncAllEmployees.this, Integer.parseInt(empIdArray[j]));
                                                }
                                            } else {
                                                db.deleteAvtarFromFileName(SyncAllEmployees.this, Integer.parseInt(req_emp_id));
                                            }
                                        }
                                    }
                                } else {
                                    if (message.length() == 0 && data.size() > 0 && data.size() <= 100) {
                                        if (currentSyncPage == 1) {
                                            if (req_mode == 0) {
                                                db.deleteAllEmployees(SyncAllEmployees.this);
                                            }
                                            empList.clear();
                                        }
                                    }
                                }
                            // to clear employee data every time

                            /*for (int i = 0; i < data.size(); i++) {
                                Employee emp = data.get(i);//new Categories();
                                int emp_id = emp.getSLN_Employee();//dataObj.getInt(APIParam.RES_CAT_ID);
                                //Log.d(TAG, "EmpId: " + emp_id);
                                String emp_name = emp.getEmployeeName();//dataObj.getString(APIParam.RES_CAT_NAME);
                                //Log.d(TAG, "EmpName: " + emp_name);
                                String card_number = emp.getCard_Number();
                                //Log.d(TAG, "EmpCardNo: " + card_number);
                                String emp_photo = emp.getEmployee_Photo();
                                //Log.d(TAG, "EMpPhoto: "+emp_photo);
                                int loc_id = emp.getSLN_Location();
                                //Log.d(TAG, "EmpLocationId: "+loc_id);
                                //categoriesListArray[i] = cat_name;

                                if (req_mode == 0 || req_mode == 1) {
                                    db.addEmployees(SyncAllEmployees.this, emp);
                                } else if (req_mode == 2) {
                                    if (emp_photo != null) {
                                        db.deleteAvtarFromFileName(SyncAllEmployees.this, emp_id);
                                    }
                                    db.updateEmployees(SyncAllEmployees.this, emp);
                                }
                            }*/
                            db.saveEmployees(SyncAllEmployees.this, req_mode, data);
                            // before update current page count fetch first added data from database only once for first page
                            if (currentSyncPage == 1) {
                                //lvAllEmployee.addFooterView(footerView);
                                //footerView.setVisibility(View.VISIBLE);
                                firstVal = 0;
                                lastVal = 0;
                                firstVal = lastVal + 1;
                                lastVal = firstVal + 99;
                                loadAllEmployeeFromDB();
                            }

                            //currentSyncPage = currentSyncPage + 1;
                            if (totalRecords > 0 && totalRecords > 100) {
                                double value = totalRecords / 100;
                                progressPercentage = (int)((100 * currentSyncPage) / value);

                                Log.d(TAG, "CurrentPercentage: "+progressPercentage);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pDialog.setProgress(progressPercentage);
                                    }
                                });
                                onEmployeesAPICall();
                            } else if (totalRecords > 0 && totalRecords <= 100) {
                                progressPercentage = 100;
                                Log.d(TAG, "CurrentPercentage: "+progressPercentage);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pDialog.setProgress(progressPercentage);
                                    }
                                });

                                if(pDialog.isShowing())
                                    pDialog.dismiss();

                                lvAllEmployee.setVisibility(View.VISIBLE);
                                btnAllEmployeeOkay.setVisibility(View.GONE);
                                tvAllEmployeeTitle.setText(Html.fromHtml(getString(R.string.sync_done_all_emp)));
                                currentSyncPage = 0;
                                setProgress(100);
                                totalRecords = 0;
                            } else {
                                if (totalRecords == 0 && data.size() > 0 && data.size() <= 100) {
                                    progressPercentage = 100;
                                    Log.d(TAG, "CurrentPercentage: "+progressPercentage);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            pDialog.setProgress(progressPercentage);
                                        }
                                    });

                                    if(pDialog.isShowing())
                                        pDialog.dismiss();

                                    lvAllEmployee.setVisibility(View.VISIBLE);
                                    btnAllEmployeeOkay.setVisibility(View.GONE);
                                    tvAllEmployeeTitle.setText(Html.fromHtml(getString(R.string.sync_done_all_emp)));
                                    currentSyncPage = 0;
                                    setProgress(100);
                                    totalRecords = 0;
                                }
                            }
                            currentSyncPage = currentSyncPage + 1;

                        } else if (status == -1) {
                            if(pDialog.isShowing())
                                pDialog.dismiss();

                            Log.d(TAG, "All employees exception Message: " + message);
                            lvAllEmployee.setVisibility(View.INVISIBLE);
                            btnAllEmployeeOkay.setVisibility(View.GONE);
                            tvAllEmployeeTitle.setText(Html.fromHtml(getString(R.string.problem_with_sync) + message));
                        } else if (status == 102) {
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SyncAllEmployees.this);
                            SharedPreferences.Editor edit = prefs.edit();
                            edit.putString(PreferencesManager.KEY_SET_LAST_SYNCED_TIMESTAMP, ""+req_timestamp);
                            edit.commit();

                            if(pDialog.isShowing())
                                pDialog.dismiss();

                            lvAllEmployee.setVisibility(View.VISIBLE);
                            btnAllEmployeeOkay.setVisibility(View.GONE);
                            tvAllEmployeeTitle.setText(Html.fromHtml(getResources().getString(R.string.sync_done_all_emp)));
                            currentSyncPage = 0;
                            setProgress(100);
                            totalRecords = 0;
                            //loadAllEmployeeFromDB();
                        } else {
                            if(pDialog.isShowing())
                                pDialog.dismiss();

                            Log.d(TAG, "All employees exception Message: ");
                            lvAllEmployee.setVisibility(View.INVISIBLE);
                            btnAllEmployeeOkay.setVisibility(View.GONE);
                            if (data.size()!=0) {
                                tvAllEmployeeTitle.setText(Html.fromHtml(getResources().getString(R.string.problem_with_sync) + message));
                            }else {
                                tvAllEmployeeTitle.setText(Html.fromHtml(getResources().getString(R.string.no_records_label)));
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<Employee> call, Throwable t) {
                    // Log error here since request failed
                    lvAllEmployee.setVisibility(View.INVISIBLE);
                    btnAllEmployeeOkay.setVisibility(View.GONE);
                    Log.e(TAG, "All employees Response error : " + t.toString());
                    tvAllEmployeeTitle.setText(Html.fromHtml(getResources().getString(R.string.problem_with_sync)+ getResources().getString(R.string.try_later_label)));

                    if(pDialog.isShowing())
                        pDialog.dismiss();
                }
            });
        } else {
            if(pDialog.isShowing())
                pDialog.dismiss();

            tvAllEmployeeTitle.setText(getResources().getString(R.string.no_conn));
            lvAllEmployee.setVisibility(View.INVISIBLE);
            btnAllEmployeeOkay.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar
                    .make(rlAllEmployeeSync, getResources().getString(R.string.no_conn), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getResources().getString(R.string.snack_action_refresh), mOnSnackBarClickListener);
            snackbar.setActionTextColor(Color.GREEN);
            View snackbarView = snackbar.getView();
            snackbarView.setBackgroundColor(Color.DKGRAY);
            TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
    }

    public View.OnClickListener mBtnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            System.out.println("BtnOkCLick");
            //setResult(SyncActivity.REQUEST_SYNC_CATEGORIES);
            SyncAllEmployees.this.finish();
        }
    };

    public void loadAllEmployeeFromDB () {
        System.out.println(TAG+"first: "+firstVal+" & last: "+lastVal);
        ArrayList<Employee> list = db.getAllEmployees(firstVal, lastVal);
        for (int k = 0; k < list.size(); k++) {
            Employee emp = list.get(k);
            empList.add(emp);
            RecyclerListSelectorModel model = new RecyclerListSelectorModel();
            model.setSelected(false);
            listBoolean.add(model);
        }

        if (empList != null && empList.size() > 0) {
            employee_adapter.updateEmployeeList(empList);
            employee_adapter.updateBooleanLList(listBoolean);
            lvAllEmployee.setVisibility(View.VISIBLE);
            btnAllEmployeeOkay.setVisibility(View.GONE);
            employee_adapter.setOnLoadMoreListener(mLoadMoreListener);
            employee_adapter.notifyDataSetChanged();
            rlAllEmpCount.setVisibility(View.VISIBLE);
            int count = employee_adapter.getItemCount();
            if (count > 0) {
                tvAllEmpCount.setText(""+ URLCollections.getNumberFormat().format(count));
            }

            if (mHandler != null) {
                mHandler.removeCallbacks(mRunnable);
            }

            mHandler = new Handler();
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    rlAllEmpCount.setVisibility(View.GONE);
                }
            };
            mHandler.postDelayed(mRunnable, 3000);

            employee_adapter.setLoaded();

            //lvAllEmployee.removeFooterView(footerView);
            //footerView.setVisibility(View.GONE);
            tvAllEmployeeTitle.setText(Html.fromHtml(getString(R.string.sync_done_all_emp)));
            System.out.println(TAG+" EmpSize() : "+empList.size());
            if (lastVal <= empList.size()) {
                flag_loading = false;
            }
        } else {
            //lvAllEmployee.removeFooterView(footerView);
            //lvAllEmployee.setVisibility(View.INVISIBLE);
            //btnAllEmployeeOkay.setVisibility(View.INVISIBLE);
            //tvAllEmployeeTitle.setText(Html.fromHtml("<h4> Sync is not Done </h4> </br> No employees data available! Please try again."));
        }
    }

    @Override
    public void onBackPressed() {
        if (ab.getTitle().equals("Select Employees")) {
            //lvAllEmployee.setChoiceMode(ListView.CHOICE_MODE_NONE);
            //listBoolean.clear();
            employee_adapter.enableCheckBox(false);
            employee_adapter.setOnLoadMoreListener(mLoadMoreListener);
            employee_adapter.notifyDataSetChanged();
            rlAllEmpCount.setVisibility(View.VISIBLE);
            int count = employee_adapter.getItemCount();
            if (count > 0) {
                tvAllEmpCount.setText(""+ URLCollections.getNumberFormat().format(count));
            }

            if (mHandler != null) {
                mHandler.removeCallbacks(mRunnable);
            }

            mHandler = new Handler();
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    rlAllEmpCount.setVisibility(View.GONE);
                }
            };
            mHandler.postDelayed(mRunnable, 3000);

            ab.setTitle(getResources().getString(R.string.sync_employees));
            ab.setDisplayHomeAsUpEnabled(true);
            invalidateOptionsMenu();
        } else {
            invalidateOptionsMenu();
            super.onBackPressed();
        }
    }

    // search query listerner's methods

    @Override
    public boolean onQueryTextSubmit(String query) {
        System.out.println("Query text submit." + query);
        new SearchEmployees().execute(query);
        //searchMenuItem.collapseActionView();
        //searchView.setIconified(true);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            //friendListView.clearTextFilter();
            System.out.println("Search text emplty.");
        } else {
            //friendListView.setFilterText(newText.toString());
            System.out.println("Searched text: " + newText);
        }

        return true;
    }

    // Async task for the Employees search
    public class SearchEmployees extends AsyncTask<String, Void, Void> {
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SyncAllEmployees.this, R.style.MyTheme);
            pDialog.setCancelable(false);
            pDialog.show();

            empList.clear();
        }

        @Override
        protected Void doInBackground(String... params) {

            empList = db.searchEmployeeFromFilter(params[0]);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (empList.size() > 0) {
                lvAllEmployee.setVisibility(View.VISIBLE);
                //.setVisibility(View.GONE);
                employee_adapter.updateEmployeeList(empList);
                employee_adapter.setOnLoadMoreListener(null);
                employee_adapter.notifyDataSetChanged();

                tvAllEmpCount.setText(""+ URLCollections.getNumberFormat().format(empList.size()));

                if (mHandler != null) {
                    mHandler.removeCallbacks(mRunnable);
                }

                mHandler = new Handler();
                mRunnable = new Runnable() {
                    @Override
                    public void run() {
                        rlAllEmpCount.setVisibility(View.GONE);
                    }
                };
                mHandler.postDelayed(mRunnable, 3000);
            } else {
                lvAllEmployee.setVisibility(View.GONE);
                //tvNoRecords.setVisibility(View.VISIBLE);
            }
        }
    }

}
