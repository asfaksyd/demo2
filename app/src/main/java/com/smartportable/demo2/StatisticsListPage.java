package com.smartportable.demo2;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.smartportable.demo2.adapters.StatisticItemAdapter;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.Statistics;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import java.util.ArrayList;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by Ananth on 2/5/2017.
 */

public class StatisticsListPage extends AppCompatActivity implements View.OnClickListener {
    ListView lvStatistic;
    TextView musterName, musterCount;
    GraphView gvLinearStatistic;
    View rlFooter = null;
    ArrayList<Statistics> statisticList = new ArrayList<Statistics>();
    MenuItem itemGraphView, itemListView;
    StaticLabelsFormatter staticLabelsFormatter;
    GridLabelRenderer glr;// = new GridLabelRenderer(gvLinearStatistic).getStyles();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic_list);

        ActionBar ab = getSupportActionBar();
        ab.setHomeButtonEnabled(true);

        lvStatistic = (ListView) findViewById(R.id.lvStatistic);
        rlFooter = (View) findViewById(R.id.includeTotalCount);
        musterName = (TextView) rlFooter.findViewById(R.id.tvMusterName);
        musterCount = (TextView) rlFooter.findViewById(R.id.tvMusterCount);
        gvLinearStatistic = (GraphView) findViewById(R.id.gvLinearStatistic);

        /*// activate horizontal zooming and scrolling
        gvLinearStatistic.getViewport().setScalable(true);

        // activate horizontal scrolling
        gvLinearStatistic.getViewport().setScrollable(true);

        // activate horizontal and vertical zooming and scrolling
        gvLinearStatistic.getViewport().setScalableY(true);

        // activate vertical scrolling
        gvLinearStatistic.getViewport().setScrollableY(true);*/


        // set manual X bounds
        gvLinearStatistic.getViewport().setXAxisBoundsManual(true);
        gvLinearStatistic.getViewport().setMinX(0);
        gvLinearStatistic.getViewport().setMaxX(3);

        // set manual Y bounds
        /*gvLinearStatistic.getViewport().setYAxisBoundsManual(true);
        gvLinearStatistic.getViewport().setMinY(0);*/
        //gvLinearStatistic.getViewport().setMaxY(8);

        // use static labels for horizontal and vertical labels
        staticLabelsFormatter = new StaticLabelsFormatter(gvLinearStatistic);

        glr = new GridLabelRenderer(gvLinearStatistic);

        new GetCountFromDBAsync().execute();
        /*for (int k=0; k<5; k++) {
            Statistics statistic = new Statistics();
            statistic.setMusterId(1);
            statistic.setMusterName("HeadCount");
            statistic.setMusterCount(5);
            statisticList.add(statistic);
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_statistic, menu);
        itemGraphView = menu.findItem(R.id.itemGraphView);
        itemListView = menu.findItem(R.id.itemListView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            StatisticsListPage.this.finish();
            return true;
        }
        else if (id == R.id.itemListView) {
            itemListView.setVisible(false);
            itemGraphView.setVisible(true);

            lvStatistic.setVisibility(View.VISIBLE);
            gvLinearStatistic.setVisibility(View.GONE);
            return true;
        } else if (id == R.id.itemGraphView) {
            itemListView.setVisible(true);
            itemGraphView.setVisible(false);

            lvStatistic.setVisibility(View.GONE);
            gvLinearStatistic.setVisibility(View.VISIBLE);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            /*case R.id. :

                break;
            case R.id. :

                break;
            default:
                break;*/
        }
    }

    public class GetCountFromDBAsync extends AsyncTask<Void, Void, Void> {
        ProgressDialog pDialog = null;
        int total = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(StatisticsListPage.this, R.style.MyTheme);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            DatabaseHandler db = new DatabaseHandler(StatisticsListPage.this);
            statisticList = db.getCountMusteringTypeBased();
            total = db.getEmployeesLocationCount();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (statisticList.size() > 0) {
                // graph representation
                DataPoint[] dpArray = new DataPoint[statisticList.size()];
                String[] horzData = new String[statisticList.size()];
                for (int i=0; i<statisticList.size();i++) {
                    DataPoint dp = new DataPoint(i,statisticList.get(i).getMusterCount());
                    dpArray[i] = dp;
                    String mustname = statisticList.get(i).getMusterName();
                    if (mustname.equals("Assembly Point")) {
                        mustname = mustname.replace(" ", " \n ");
                    }
                    horzData[i] = mustname;
                }
                staticLabelsFormatter.setHorizontalLabels(horzData);

                gvLinearStatistic.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
                //gvLinearStatistic.getViewport().setBackgroundColor(R.color.color_red_fr);
                final BarGraphSeries<DataPoint> series = new BarGraphSeries<>(dpArray);
                gvLinearStatistic.addSeries(series);
                // styling
                series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
                    @Override
                    public int get(DataPoint data) {
                        Log.e("SetValueDependentColor", ""+data.getX());
                        if ((int) data.getX() == 0) {
                            return Color.rgb(255,0,0);//Color.rgb((int) data.getX() * 255 / 4, (int) Math.abs(data.getY() * 255 / 6), 100);
                        } else if ((int) data.getX() == 1) {
                            return Color.rgb(0,0,102);
                        } else if ((int) data.getX() == 2) {
                            return Color.rgb(0,100,0);
                        } else {
                            return Color.rgb(0,210,0);
                        }
                    }
                });

                series.setSpacing(30);
                series.setAnimated(true);
                // draw values on top
                series.setDrawValuesOnTop(true);
                series.setValuesOnTopColor(Color.RED);

                // list representation
                StatisticItemAdapter adapter = new StatisticItemAdapter(StatisticsListPage.this, statisticList);
                lvStatistic.setAdapter(adapter);
                rlFooter.setBackgroundColor(StatisticsListPage.this.getResources().getColor(android.R.color.darker_gray));
                musterName.setText(R.string.total_head_count);
                musterCount.setText(""+ URLCollections.getNumberFormat().format(total));
            }
        }
    }
}
