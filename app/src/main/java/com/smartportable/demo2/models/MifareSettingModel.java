package com.smartportable.demo2.models;

/**
 * Created by Ananth on 4/6/2017.
 */

public class MifareSettingModel {

    String fieldName, fieldValue;
    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

}
