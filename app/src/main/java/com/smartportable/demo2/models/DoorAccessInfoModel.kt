package com.smartportable.demo2.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class DoorAccessInfoModel : Serializable {

    @SerializedName("DoorId")
    var doorId: Int? = null

    @SerializedName("DoorName")
    var doorName: String? = null

}
