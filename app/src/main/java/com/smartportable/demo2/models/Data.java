package com.smartportable.demo2.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Data implements Serializable{

	@SerializedName("StudentInfoModel")
	public StudentInfoModel address;

	@SerializedName("LicenseInformation")
	public DoorAccessInfoModel licenseInformation;

}