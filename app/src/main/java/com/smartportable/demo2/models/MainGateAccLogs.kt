package com.smartportable.demo2.models

import com.google.gson.annotations.SerializedName

class MainGateAccLogs {
    @SerializedName("Status")
    var status: Int ?= 0
    @SerializedName("Message")
    var message: String ?= ""
    @SerializedName("Data")
    var data: ArrayList<MainGateAccLogs.Data> ?= null

    inner class Data {
        @SerializedName("StudentId")
        var studentId : String ?= ""

        @SerializedName("StudentName")
        var studentName : String ?= ""

        @SerializedName("StudentCardNumber")
        var cardNumber : String ?= ""

        @SerializedName("StudentPunchDateTime")
        var punchDateTime : String ?= ""

        @SerializedName("MainGateId")
        var gateId : Int ?= 0

        @SerializedName("MainGateName")
        var gateName: String ?= ""

        @SerializedName("MainPunchType")
        var punchType: Int ?= 0

        @SerializedName("StudentMealNumber")
        var studentMealNumber : String ?= ""

        @SerializedName("PunchNote")
        var punchNote : String ?= ""

        @SerializedName("IsSync")
        var isSync: Int ?= 0

        @SerializedName("StudentImage")
        var studentImage : String ?= ""

    }

}