package com.smartportable.demo2.models

import com.google.gson.annotations.SerializedName
import com.smartportable.demo2.utils.*
import java.io.Serializable

class MemAccLogsInfoModel : Serializable {

    @SerializedName(API_STATUS)
    var status: Int?= null

    @SerializedName(API_MESSAGE)
    var message: String?= null

    @SerializedName(API_PAGE_COUNT)
    var pageCount : String?= null

    @SerializedName(API_DATA)
    var data: ArrayList<DataBean>? = null

    class DataBean {
        @SerializedName("Student_Id")
        var studentID: String? = null

        @SerializedName("Is_Sync")
        var isSync: Int? = null

        @SerializedName("Session_ID")
        var sessionId: String? = null

        @SerializedName("Reader_Id")
        var readerId: String? = null

    }
}