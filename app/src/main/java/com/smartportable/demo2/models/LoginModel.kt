package com.smartportable.demo2.models

import com.google.gson.annotations.SerializedName
import com.smartportable.demo2.utils.*

public class LoginModel {

    @SerializedName(API_STATUS)
    public var status: Int = 0

    @SerializedName(API_MESSAGE)
    public var message: String? = null

    @SerializedName(API_DATA)
    public var data: DataBean? = null

    class DataBean {
        @SerializedName(API_LOGIN_ID)
        public var id: Int ? = null
        @SerializedName(API_LOGIN_USER_ID)
        public var userId: Int = 0
        @SerializedName(API_TOKEN)
        public var token: String? = null
        @SerializedName(API_TOKEN_EXPIRE)
        public var tokenExpireTime:String = ""
        @SerializedName(API_LOGIN_EMAIL)
        public var email:String? = null
        @SerializedName(API_LOGIN_PASSWORD)
        public var password:String? = null
        @SerializedName(API_LOGIN_STAFFID)
        public var staffid: String? = null
        @SerializedName(API_LOGIN_FULLNAME)
        public var fullName:String? = null
        @SerializedName(API_LOGIN_PHOTO)
        public var photo:String? = null

        @SerializedName(API_LOGIN_CANTEEN_ID)
        public var canteenId:String? = null

        @SerializedName(API_LOGIN_CANTEEN_NAME)
        public var canteenName:String? = null

        @SerializedName(API_LOGIN_USER_TYPE_ID)
        public var userTypeId:String? = null

        @SerializedName(API_LOGIN_USER_TYPE_NAME)
        public var userTypeName:String? = null
    }







}