package com.smartportable.demo2.models;

public class Student {
    int stId;
    String stName;
    String stCardno;
    String stDeptName;
    int stPhoto;

    public int getStId() {
        return stId;
    }

    public void setStId(int stId) {
        this.stId = stId;
    }

    public String getStName() {
        return stName;
    }

    public void setStName(String stName) {
        this.stName = stName;
    }

    public String getStCardno() {
        return stCardno;
    }

    public void setStCardno(String stCardno) {
        this.stCardno = stCardno;
    }

    public String getStDeptName() {
        return stDeptName;
    }

    public void setStDeptName(String stDeptName) {
        this.stDeptName = stDeptName;
    }

    public int getStPhoto() {
        return stPhoto;
    }

    public void setStPhoto(int stPhoto) {
        this.stPhoto = stPhoto;
    }

}
