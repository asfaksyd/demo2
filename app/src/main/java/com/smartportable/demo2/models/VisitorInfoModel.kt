package com.smartportable.demo2.models

import com.google.gson.annotations.SerializedName
import com.smartportable.demo2.utils.*
import java.io.Serializable

class VisitorInfoModel : Serializable {

    @SerializedName(API_STATUS)
    var status: Int?= null

    @SerializedName(API_MESSAGE)
    var message: String?= null

    @SerializedName(API_PAGE_COUNT)
    var pageCount : String?= null

    @SerializedName(API_DATA)
    var data: ArrayList<DataBean>? = null

    class DataBean {
        @SerializedName("VisitorRegNo")
        var visitorRegNo: String? = null

        @SerializedName("VisitorFirstName")
        var visitorFirstName: String? = null

        @SerializedName("VisitorLastName")
        var visitorLasttName: String? = null

        @SerializedName("CompanyName")
        var companyName: String? = null

        @SerializedName("VisitorType")
        var visitorType: String? = null

        @SerializedName("VisitReason")
        var visitReason : String? = null

        @SerializedName("PhoneNo")
        var visitorPhoneNo: String? = null

        @SerializedName("EmailId")
        var visitorEmailId: String? = null

        @SerializedName("VisitorPhoto")
        var visitorPhoto: String? = null

        @SerializedName("VisitorAccessLevel")
        var visitorAccessLevel: String? = null

        @SerializedName("VisitorCheckInTime")
        var visitorCheckInTime: String?= null

        @SerializedName("VisitorCheckOutTime")
        var visitorCheckOutTime: String?= null

        @SerializedName("VisitorCardNumber")
        var visitorCardNumber: String? = null

        @SerializedName("VisitorCardStatus")
        var visitorCardStatus: String? = null

        @SerializedName("VisitorStatus")
        var visitorStatus: String? = null

    }
}