package com.smartportable.demo2.models

import com.google.gson.annotations.SerializedName
import com.smartportable.demo2.utils.*
import java.io.Serializable

class SessionsInfoModel : Serializable {

    @SerializedName(API_STATUS)
    var status: Int?= null

    @SerializedName(API_MESSAGE)
    var message: String?= null

    @SerializedName(API_PAGE_COUNT)
    var pageCount : String?= null

    @SerializedName(API_DATA)
    var data: ArrayList<DataBean>? = null

    class DataBean {
        @SerializedName("SessionId")
        var sessionId: String? = null

        @SerializedName("SessionName")
        var sessionName: String? = null

        @SerializedName("Description")
        var sessionDesc: String? = null

        @SerializedName("StartTime")
        var sessionStartTime: String? = null

        @SerializedName("EndTime")
        var sessionEndTime: String? = null
    }
}