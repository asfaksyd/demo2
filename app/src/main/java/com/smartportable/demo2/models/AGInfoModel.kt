package com.smartportable.demo2.models

import com.google.gson.annotations.SerializedName
import com.smartportable.demo2.utils.*
import java.io.Serializable

class AGInfoModel : Serializable {

    @SerializedName(API_STATUS)
    var status: Int?= null

    @SerializedName(API_MESSAGE)
    var message: String?= null

    @SerializedName(API_PAGE_COUNT)
    var pageCount : String?= null

    @SerializedName(API_DATA)
    var data: ArrayList<DataBean>? = null

    class DataBean {
        @SerializedName("AGId")
        var agId: Int? = null

        @SerializedName("AGName")
        var agName: String? = null

        @SerializedName("AGDescription")
        var agDescription: String? = null

        @SerializedName("IsCanteen")
        var IsCanteen: String? = null

        @SerializedName("DGId")
        var dgId: String? = null

        @SerializedName("CanteenType")
        var canteenType: String? = null

        @SerializedName("SessionId")
        var sessionId : String? = null

    }
}