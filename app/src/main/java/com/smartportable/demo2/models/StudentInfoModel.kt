package com.smartportable.demo2.models

import com.google.gson.annotations.SerializedName
import com.smartportable.demo2.utils.*
import java.io.Serializable

class StudentInfoModel : Serializable {

    @SerializedName(API_STATUS)
    var status: Int?= null

    @SerializedName(API_MESSAGE)
    var message: String?= null

    @SerializedName(API_PAGE_COUNT)
    var pageCount : String?= null

    @SerializedName(API_DATA)
    var data: ArrayList<DataBean>? = null

    class DataBean {
        @SerializedName("StudentID")
        var studentID: String? = null

        @SerializedName("FirstName")
        var firstName: String? = null

        @SerializedName("FatherName")
        var fatherName: String? = null

        @SerializedName("GrandFatherName")
        var grandFatherName: String? = null

        @SerializedName("Gender")
        var gender: String? = null

        @SerializedName("DateOfBirth")
        var dateOfBirth : String? = null

        @SerializedName("Signature")
        var signature: String? = null

        @SerializedName("College")
        var college: String? = null

        @SerializedName("Department")
        var department: String? = null

        @SerializedName("Campus")
        var campus: String? = null

        @SerializedName("Program")
        var program: String? = null

        @SerializedName("DegreeType")
        var degreeType: String? = null

        @SerializedName("AdmissionType")
        var admissionType: String? = null

        @SerializedName("AdmissionTypeShort")
        var admissionTypeShort: String? = null

        @SerializedName("ValidDateUntil")
        var validDateUntil: String? = null

        @SerializedName("IssueDate")
        var issueDate: String? = null

        @SerializedName("MealNumber")
        var mealNumber: String? = null

        @SerializedName("UniqueNo")
        var uniqueNo: String? = null

        @SerializedName("Status")
        var status: String? = null

        @SerializedName("Isactive")
        var isactive: String? = null

        @SerializedName("CardNumber")
        var cardNumber: String? = null

        @SerializedName("CardStatus")
        var cardstatus: String? = null

        @SerializedName("Image64byte")
        var studentImage: String? = null

    }
}