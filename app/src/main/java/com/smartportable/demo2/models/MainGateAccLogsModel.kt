package com.smartportable.demo2.models

import com.google.gson.annotations.SerializedName

class MainGateAccLogsModel {
    @SerializedName("Status")
    var status: Int ?= 0
    @SerializedName("Message")
    var message: String ?= ""
    @SerializedName("Data")
    var data: ArrayList<MainGateAccLogsModel.Data> ?= null

    inner class Data {
        @SerializedName("Student_Id")
        var studentId : String ?= ""

        @SerializedName("Is_Sync")
        var isSync : Int ?= 0

        @SerializedName("Punch_Datetime")
        var punchDatetime : String ?= ""

        @SerializedName("AG_Id")
        var agId : String ?= ""

    }

}