package com.smartportable.demo2.models

import com.google.gson.annotations.SerializedName
import com.smartportable.demo2.utils.*
import java.io.Serializable

class StaffInfoModel : Serializable {

    @SerializedName(API_STATUS)
    var status: Int?= null

    @SerializedName(API_MESSAGE)
    var message: String?= null

    @SerializedName(API_PAGE_COUNT)
    var pageCount : String?= null

    @SerializedName(API_DATA)
    var data: ArrayList<DataBean>? = null

    class DataBean {
        @SerializedName("Staff_Id")
        var staffID: String? = null

        @SerializedName("Application_No")
        var appNo: String? = null

        @SerializedName("SL_No")
        var slNo: String? = null

        @SerializedName("UID")
        var uID: String? = null

        @SerializedName("Full_Name")
        var fullName: String? = null

        @SerializedName("Gender")
        var gender: String? = null

        @SerializedName("DOB")
        var dob : String? = null

        @SerializedName("Department")
        var department: String? = null

        @SerializedName("Job_Title")
        var jobTitle: String? = null

        @SerializedName("Emp_Photo")
        var empPhoto: String? = null

        @SerializedName("Signature")
        var signature: String? = null

        @SerializedName("Address")
        var address: String? = null

        @SerializedName("ID_no")
        var idNo: String? = null

        @SerializedName("Issue_Date")
        var issueDate: String? = null

        @SerializedName("cardstatus")
        var cardstatus: String? = null

        @SerializedName("cardnumber")
        var cardNumber: String? = null

        @SerializedName("Email_id")
        var emailId: String? = null

        @SerializedName("Isactive")
        var isactive: String? = null

        @SerializedName("Password")
        var password: String? = null

        @SerializedName("Id_Number")
        var idNumber: String? = null

        @SerializedName("Image64byte")
        var image64byte: String? = null

    }
}