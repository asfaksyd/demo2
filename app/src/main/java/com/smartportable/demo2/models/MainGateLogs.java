package com.smartportable.demo2.models;

public class MainGateLogs {
    int mglStId;
    String mglStName;
    String mglStCardNo;
    String mglStDeptName;
    String mglStPunchTime;
    int mglStPhoto;
    int mglStCampusId;
    String mglStCampusName;
    int mglStMainGateAccId;
    String mglStMainGateAccName;

    public int getMglStId() {
        return mglStId;
    }

    public void setMglStId(int mglStId) {
        this.mglStId = mglStId;
    }

    public String getMglStName() {
        return mglStName;
    }

    public void setMglStName(String mglStName) {
        this.mglStName = mglStName;
    }

    public String getMglStCardNo() {
        return mglStCardNo;
    }

    public void setMglStCardNo(String mglStCardNo) {
        this.mglStCardNo = mglStCardNo;
    }

    public String getMglStDeptName() {
        return mglStDeptName;
    }

    public void setMglStDeptName(String mglStDeptName) {
        this.mglStDeptName = mglStDeptName;
    }

    public String getMglStPunchTime() {
        return mglStPunchTime;
    }

    public void setMglStPunchTime(String mglStPunchTime) {
        this.mglStPunchTime = mglStPunchTime;
    }

    public int getMglStPhoto() {
        return mglStPhoto;
    }

    public void setMglStPhoto(int mglStPhoto) {
        this.mglStPhoto = mglStPhoto;
    }

    public int getMglStCampusId() {
        return mglStCampusId;
    }

    public void setMglStCampusId(int mglStCampusId) {
        this.mglStCampusId = mglStCampusId;
    }

    public String getMglStCampusName() {
        return mglStCampusName;
    }

    public void setMglStCampusName(String mglStCampusName) {
        this.mglStCampusName = mglStCampusName;
    }

    public int getMglStMainGateAccId() {
        return mglStMainGateAccId;
    }

    public void setMglStMainGateAccId(int mglStMainGateAccId) {
        this.mglStMainGateAccId = mglStMainGateAccId;
    }

    public String getMglStMainGateAccName() {
        return mglStMainGateAccName;
    }

    public void setMglStMainGateAccName(String mglStMainGateAccName) {
        this.mglStMainGateAccName = mglStMainGateAccName;
    }
}
