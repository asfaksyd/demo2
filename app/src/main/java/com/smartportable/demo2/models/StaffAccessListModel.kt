package com.smartportable.demo2.models

import com.google.gson.annotations.SerializedName
import com.smartportable.demo2.utils.*
import java.io.Serializable

class StaffAccessListModel : Serializable {

    @SerializedName(API_STATUS)
    var status: Int?= null

    @SerializedName(API_MESSAGE)
    var message: String?= null

    @SerializedName(API_PAGE_COUNT)
    var pageCount : String?= null

    @SerializedName(API_DATA)
    var data: ArrayList<DataBean>? = null

    class DataBean {
        @SerializedName("ReadersId")
        var readersId: String? = null

        @SerializedName("SessionId")
        var sessionsId: String? = null

        @SerializedName("StaffId")
        var staffId: String? = null

        @SerializedName("CanteenId")
        var staffCanteenId: String? = null

        @SerializedName("CanteenName")
        var staffCanteenName: String? = null

        @SerializedName("AgId")
        var staffAgIds: String? = null
    }
}