package com.smartportable.demo2.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.smartportable.demo2.R;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.fragments.EmployeeDetailFragment;
import com.smartportable.demo2.helper.Employee;
import com.smartportable.demo2.helper.RecyclerListSelectorModel;
import com.smartportable.demo2.helper.SaveImages;
import com.smartportable.demo2.helper.Utility;
import com.smartportable.demo2.interfaces.OnLoadMoreListener;
import com.smartportable.demo2.interfaces.SelectedListListener;

import java.util.ArrayList;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;


public class    ListItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    protected boolean showDynamicTags = false;
    ArrayList<Employee> list_employee = new ArrayList<Employee>();
    ArrayList<RecyclerListSelectorModel> list_boolean = new ArrayList<RecyclerListSelectorModel>();
    Context mContext;
    byte[] data = null;
    SaveImages saveImage;
    boolean enable = false, isLoading = false, forMemAccess = false;
    public OnLoadMoreListener mOnLoadMoreListener;
    public SelectedListListener listChangeListener;
    DatabaseHandler db;
    ProgressBar pbReaders;
    TextView tv_dialog_emp_reader_id;
    ListView lvReadersList;
    FragmentManager fm;

    private int lastVisibleItem, totalItemCount, visibleThreshold =5;

    public void setListener (SelectedListListener listChangeListener) {
        this.listChangeListener = listChangeListener;
    }

    public ListItemAdapter (Context mContext, ArrayList<Employee> list_employee, RecyclerView mRecyclerView, ArrayList<RecyclerListSelectorModel> list_boolean, FragmentManager fm) {
        this.mContext = mContext;
        this.list_employee = list_employee;
        this.list_boolean = list_boolean;
        saveImage = SaveImages.getInstance();
        db = new DatabaseHandler(mContext);
        this.fm = fm;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        if (linearLayoutManager != null) {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }
            });
        }
    }

    private View.OnClickListener onTagTvClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view instanceof  TextView && mOnTagClickedListener != null) {
                TextView tv = (TextView) view;
                final String tag = tv.getText().toString();
                mOnTagClickedListener.onTagClicked(tag);
            }
        }
    };
    private OnTagClickedListener mOnTagClickedListener = null;

    public ListItemAdapter (ArrayList<Employee> list_employee) {
        this.list_employee = list_employee;
    }

    @Override
    public int getItemViewType(int position) {
        return list_employee.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.employee_row_item, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View viewLoading = LayoutInflater.from(mContext).inflate(R.layout.footer_layout, parent, false);
            return new LoadingViewHolder(viewLoading);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, final int position) {
        if (mHolder instanceof UserViewHolder){
            final RecyclerListSelectorModel model = list_boolean.get(position);
            final UserViewHolder userHolder = (UserViewHolder) mHolder;
            Employee item = list_employee.get(position);
            int emp_id = item.getSLN_Employee();
            String emp_name = item.getEmployeeName();
            String empPhoto = item.getEmployee_Photo();
            String card_no = item.getCard_Number();
            int loc_id = item.getSLN_Location();
            System.out.println("Image Photo " + empPhoto);

            userHolder.tvEmpId.setTextColor(mContext.getResources().getColor(android.R.color.black));
            userHolder.tvEmpName.setTextColor(mContext.getResources().getColor(android.R.color.black));
            userHolder.tvEmpCardNo.setTextColor(mContext.getResources().getColor(android.R.color.black));
            userHolder.tvEmpLocId.setTextColor(mContext.getResources().getColor(android.R.color.black));

            userHolder.tvEmpId.setVisibility(View.GONE);
            // setborder for employee list to 0
            userHolder.ivEmpPhoto.setBorderWidth(0);

            if (enable) {
                userHolder.ivCheckBox.setVisibility(View.VISIBLE);
            } else {
                userHolder.ivCheckBox.setVisibility(View.GONE);
            }
            /*if (emp_id >= 0)
                userHolder.tvEmpId.setText("" + emp_id);
            if (emp_name != null && emp_name.length() > 0)
                userHolder.tvEmpName.setText("" + emp_name);*/
            if (emp_id >= 0 || emp_name != null && emp_name.length() > 0)
                userHolder.tvEmpName.setText(emp_id + " - " + emp_name);
            if (card_no != null && card_no.length() > 0) {
                userHolder.tvEmpCardNo.setVisibility(View.VISIBLE);
                userHolder.tvEmpCardNo.setText("" + card_no);
            } else {
                userHolder.tvEmpCardNo.setVisibility(View.GONE);
                userHolder.tvEmpCardNo.setText("");
            }
            if (loc_id >= 0)
                userHolder.tvEmpLocId.setText("" + loc_id);

            //userHolder.itemView.setBackgroundColor(model.isSelected() ? Color.CYAN : Color.WHITE);
            if (model.isSelected()) {
                userHolder.ivCheckBox.setChecked(true);
            } else{
                userHolder.ivCheckBox.setChecked(false);
            }
            userHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (enable) {
                        model.setSelected(!model.isSelected());
                        //userHolder.itemView.setBackgroundColor(model.isSelected() ? Color.CYAN : Color.WHITE);
                        if (model.isSelected()) {
                            userHolder.ivCheckBox.setChecked(true);
                        } else {
                            userHolder.ivCheckBox.setChecked(false);
                        }
                        list_boolean.set(position, model);

                        listChangeListener.onListChanged(list_boolean);
                    }

                    showEmployeeDetailDialog(position);
                }
            });

            userHolder.tvEmpPunchDateTime.setVisibility(View.GONE);
            // image click event
            //userHolder.ivEmpPhoto.setOnClickListener(imageClickListener);
            //userHolder.ivEmpPhoto.setTag(R.id.ivEmpPhoto, position);

            Glide
                    .with(mContext)
                    .asBitmap()
                    .load(empPhoto)
                    .centerCrop()
                    .dontAnimate()
                    .placeholder(R.mipmap.ic_profile_pic)
                    .into(new BitmapImageViewTarget(userHolder.ivEmpPhoto) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            userHolder.ivEmpPhoto.setImageDrawable(circularBitmapDrawable);
                        }
                    });

        } else if (mHolder instanceof LoadingViewHolder) {
            LoadingViewHolder loadHolder = (LoadingViewHolder) mHolder;

        }
    }

    @Override
    public int getItemCount() {
        return list_employee.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void isForMemberAccess(boolean forMemAccess) {
        this.forMemAccess = forMemAccess;
    }

    /*@Override
    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder mHolder;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        int emp_id = getItem(position).getSLN_Employee();
        String emp_name = getItem(position).getEmployeeName();
        String empPhoto = getItem(position).getEmployee_Photo();
        String card_no = getItem(position).getCard_Number();
        int loc_id = getItem(position).getSLN_Location();
        System.out.println("Image Photo "+empPhoto);
        if (view == null) {
            view = inflater.inflate(R.layout.employee_row_item, parent, false);

            mHolder = new ViewHolder();
            mHolder.tvEmpId = (TextView) view.findViewById(R.id.tvEmpId);
            mHolder.tvEmpName = (TextView) view.findViewById(R.id.tvEmpName);
            mHolder.ivEmpPhoto = (ImageView) view.findViewById(R.id.ivEmpPhoto);
            mHolder.tvEmpCardNo = (TextView) view.findViewById(R.id.tvEmpCardNo);
            mHolder.tvEmpPunchDateTime = (TextView) view.findViewById(R.id.tvEmpPunchDateTime);
            mHolder.tvEmpLocId = (TextView) view.findViewById(R.id.tvEmpLocId);
            mHolder.ivCheckBox = (ImageView) view.findViewById(R.id.ivCheckBox);

            mHolder.tvEmpId.setTextColor(mContext.getResources().getColor(android.R.color.black));
            mHolder.tvEmpName.setTextColor(mContext.getResources().getColor(android.R.color.black));
            mHolder.tvEmpCardNo.setTextColor(mContext.getResources().getColor(android.R.color.black));
            mHolder.tvEmpLocId.setTextColor(mContext.getResources().getColor(android.R.color.black));

            mHolder.tvEmpId.setVisibility(View.VISIBLE);

            view.setTag(mHolder);
        } else {
            mHolder = (ViewHolder) view.getTag();
        }
        if (enable) {
            mHolder.ivCheckBox.setVisibility(View.VISIBLE);
        }else {
            mHolder.ivCheckBox.setVisibility(View.GONE);
        }
        if (emp_id >= 0)
        mHolder.tvEmpId.setText(""+emp_id);
        if (emp_name != null && emp_name.length() > 0)
        mHolder.tvEmpName.setText(""+emp_name);
        if (card_no != null && card_no.length() > 0) {
            mHolder.tvEmpCardNo.setVisibility(View.VISIBLE);
            mHolder.tvEmpCardNo.setText("" + card_no);
        }else {
            mHolder.tvEmpCardNo.setVisibility(View.GONE);
            mHolder.tvEmpCardNo.setText("");
        }
        if (loc_id >= 0)
            mHolder.tvEmpLocId.setText("" + loc_id);

        mHolder.tvEmpPunchDateTime.setVisibility(View.GONE);
        // image click event
        mHolder.ivEmpPhoto.setOnClickListener(imageClickListener);
        mHolder.ivEmpPhoto.setTag(R.id.ivEmpPhoto, empPhoto);

        Glide
            .with(mContext)
            .load(empPhoto)
            .placeholder(R.mipmap.ic_profile_pic)
            .crossFade()
            .into(mHolder.ivEmpPhoto);

        return view;
    }

    public class ViewHolder {
        TextView tvEmpId, tvEmpName, tvEmpCardNo, tvEmpLocId, tvEmpPunchDateTime;
        ImageView ivEmpPhoto, ivCheckBox;
    }
    @Override
    public Employee getItem(int position) {
        return list_employee.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getSLN_Employee();
    }

    @Override
    public int getCount() {
        return list_employee.size();
    }*/

    public void setOnTagClickedListener(OnTagClickedListener listener) {
        this.mOnTagClickedListener = listener;
    }

    public interface OnTagClickedListener {
        void onTagClicked(String tag);
    }

    public void updateEmployeeList (ArrayList<Employee> list_employee) {
        //this.list_employee.clear();
        this.list_employee = list_employee;
    }

    public void updateBooleanLList (ArrayList<RecyclerListSelectorModel> list_boolean) {
        this.list_boolean = list_boolean;
    }

    public void enableCheckBox(boolean enable) {
        this.enable = enable;
    }

    public View.OnClickListener imageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //final String empPhoto = (String) v.getTag();
            final int position = (Integer) v.getTag(R.id.ivEmpPhoto);
            showEmployeeDetailDialog(position);
        }
    };

    public void showEmployeeDetailDialog (int position) {

        /*EmployeeDetailFragment empDetail = new EmployeeDetailFragment();
        empDetail.setEmployeeDetail(list_employee.get(position));
        empDetail.isForMemberAccess(forMemAccess);
        fm.beginTransaction().add(android.R.id.content, empDetail).commit();*/

        BottomSheetDialogFragment bottomSheetDialogFragment = new EmployeeDetailFragment();
        ((EmployeeDetailFragment)bottomSheetDialogFragment).setEmployeeDetail(list_employee.get(position));
        ((EmployeeDetailFragment)bottomSheetDialogFragment).isForMemberAccess(forMemAccess);
        bottomSheetDialogFragment.show(fm, bottomSheetDialogFragment.getTag());

        /*Employee item = list_employee.get(position);
        int emp_id = item.getSLN_Employee();
        String emp_name = item.getEmployeeName();
        String empPhoto = item.getEmployee_Photo();
        String card_no = item.getCard_Number();
        int loc_id = item.getSLN_Location();
        int readerId = item.getSLN_Reader();
        System.out.println("Image Photo " + empPhoto);

        //final String empPhoto = (String) v.getTag(R.id.ivEmpPhoto);
        //Log.d("PunchAdapter", "EmpPhoto: "+empPhoto);
        if (empPhoto != null && empPhoto.length() !=0 || forMemAccess) {
            Log.d(MainActivity.TAG, "readersAdapter: empPhoto: "+empPhoto);
            AlertDialog alertDialog;
            final AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);

            LayoutInflater inflater = LayoutInflater.from(mContext);
            View dialogLayout = inflater.inflate(R.layout.activity_scrolling, null);
            dialog.setView(dialogLayout);//, 20, 20, 20, 20);
            //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            FloatingActionButton fab = (FloatingActionButton) dialogLayout.findViewById(R.id.fabDialog);
            fab.setVisibility(View.GONE);
            ImageView image = (ImageView) dialogLayout.findViewById(R.id.ivFullScreen);
            TextView tv_dialog_emp_id = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_id);
            TextView tv_dialog_emp_name = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_name);
            TextView tv_dialog_emp_card = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_card);
            TextView tv_dialog_emp_loc_id = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_loc_id);
            tv_dialog_emp_reader_id = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_reader_id);
            TextView tv_dialog_emp_datetime = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_datetime);

            lvReadersList = (ListView) dialogLayout.findViewById(R.id.lvReadersList);

            ArrayList<String> list = new ArrayList<String>();
            list.add("Test");
            list.add("Test");
            list.add("Test");
            list.add("Test");
            list.add("Test");

            pbReaders = (ProgressBar) dialogLayout.findViewById(R.id.pbReaders);

            tv_dialog_emp_name.setVisibility(View.GONE);
            tv_dialog_emp_loc_id.setVisibility(View.GONE);
            tv_dialog_emp_reader_id.setVisibility(View.GONE);
            pbReaders.setVisibility(View.GONE);
            if (forMemAccess) {
                //pbReaders.setVisibility(View.VISIBLE);
                new getReadersList().execute(""+emp_id, card_no);
            }
            tv_dialog_emp_datetime.setVisibility(View.GONE);

            if (emp_id >= 0 && emp_name != null && emp_name.length() > 0)
                tv_dialog_emp_id.setText(emp_id + " - " + emp_name);
            if (emp_name != null && emp_name.length() > 0)
                tv_dialog_emp_name.setText("" + emp_name);
            if (card_no != null && card_no.length() > 0) {
                tv_dialog_emp_card.setVisibility(View.VISIBLE);
                tv_dialog_emp_card.setText("" + card_no);
            } else {
                tv_dialog_emp_card.setVisibility(View.GONE);
                tv_dialog_emp_card.setText("");
            }
            if (loc_id >= 0)
                tv_dialog_emp_loc_id.setText("" + loc_id);

                *//*if (readerId >= 0) {
                    tv_dialog_emp_reader_id.setText(""+readerId);
                }*//*
                *//*if (punch_date != null && punch_date.length() > 0) {
                    tv_dialog_emp_loc_id.setText(""+punch_date);
                }*//*

            dialog.setNegativeButton(R.string.ok_label, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Write your code here to execute after dialog closed
                }
            });

            Glide
                    .with(mContext)
                    .load(empPhoto)
                    //.placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_profile_pic)
                    .crossFade()
                    .into(image);

                *//*alertDialog = dialog.create();
                alertDialog.getWindow().getAttributes().windowAnimations = R.style.CustomAnimations_grow;
                alertDialog*//*
            dialog.show();

        }*/
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmpId, tvEmpName, tvEmpCardNo, tvEmpLocId, tvEmpPunchDateTime;
        CircleImageView ivEmpPhoto;
        CheckBox ivCheckBox;

        public UserViewHolder(View itemview) {
            //super(inflater.inflate(R.layout.employee_row_item, parent, false));
            super(itemview);
            tvEmpId = (TextView) itemView.findViewById(R.id.tvEmpId);
            tvEmpName = (TextView) itemView.findViewById(R.id.tvEmpName);
            ivEmpPhoto = (CircleImageView) itemView.findViewById(R.id.ivEmpPhoto);
            tvEmpCardNo = (TextView) itemView.findViewById(R.id.tvEmpCardNo);
            tvEmpPunchDateTime = (TextView) itemView.findViewById(R.id.tvEmpPunchDateTime);
            tvEmpLocId = (TextView) itemView.findViewById(R.id.tvEmpLocId);
            ivCheckBox = (CheckBox) itemView.findViewById(R.id.ivCheckBox);

        }
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar pbLoadMore;
        public TextView tvPBTitle;
        public LoadingViewHolder(View itemview) {
            super(itemview);
            tvPBTitle = (TextView) itemView.findViewById(R.id.tvPBTitle);
            pbLoadMore = (ProgressBar) itemView.findViewById(R.id.pbLoadMore);
        }
    }

    private class getReadersList extends AsyncTask<String , Void, ArrayList<String>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbReaders.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<String> doInBackground(String... params) {
            ArrayList<String> list_readers = db.getAllReadersOfMemberAccess(Integer.parseInt(params[0]), params[1]);
            return list_readers;
        }

        @Override
        protected void onPostExecute(ArrayList<String> list) {
            super.onPostExecute(list);
            pbReaders.setVisibility(View.GONE);
            tv_dialog_emp_reader_id.setVisibility(View.GONE);
            if (list != null) {
                if (list.size() > 0) {
                    /*for (int i=0; i<list.size();i++) {
                        String reader_ids = tv_dialog_emp_reader_id.getText().toString();
                        if (i==0) {
                            tv_dialog_emp_reader_id.setText(list.get(i));
                        }else {
                            tv_dialog_emp_reader_id.setText(reader_ids+" , "+list.get(i));
                        }
                    }*/

                    ArrayAdapter<String> adapter =  new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, list);
                    lvReadersList.setAdapter(adapter);
                    Utility.setListViewHeightBasedOnChildren(lvReadersList);
                }
            }

        }
    }
}
