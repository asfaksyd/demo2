package com.smartportable.demo2.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartportable.demo2.R;

import java.util.ArrayList;

/**
 * Created by Ananth on 2/1/2017.
 */

public class MainGateAccessItemAdapter extends BaseAdapter {
    Context mContext;
    LayoutInflater inflater;
    ArrayList<String> readerList;
    boolean enable = false;
    int indexOfSelectedReader = -1;
    boolean hideRadioOption = false;


    public MainGateAccessItemAdapter(Context mContext, ArrayList<String> readerList, int indexOfSelectedReader) {
        this.mContext = mContext;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.readerList = readerList;
        this.indexOfSelectedReader = indexOfSelectedReader;

    }

    public void enableHideRadioOption (boolean hideRadioOption) {
        this.hideRadioOption = hideRadioOption;
    }

    public void enableCheckBox(boolean enable) {
        this.enable = enable;
    }

    @Override
    public int getCount() {
        return readerList.size();
    }

    @Override
    public Object getItem(int position) {
        return readerList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.dialog_set_reader_row, parent, false);
            viewHolder = new ViewHolder();


            viewHolder.ivCheckedReader = (ImageView) convertView.findViewById(R.id.ivCheckedReader);
            viewHolder.tvReaderName = (TextView) convertView.findViewById(R.id.tvReaderName);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        String readerName = readerList.get(position);
        //viewHolder.rbReaderName.setText(readerName);
        viewHolder.tvReaderName.setText(readerName);//+" TestTestTestTestTestTestTestTestTestTestTestTestTestTestTest");


        //if (position >= 0 & position<4) {
            viewHolder.tvReaderName.setTextColor(mContext.getResources().getColor(R.color.color_green_ap));
            // to check first radio button needs to display or not
            viewHolder.ivCheckedReader.setVisibility(View.GONE);
            if (indexOfSelectedReader == position) {
                viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_green_check));
            } else {
                viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_green_uncheck));
            }
        /*} else {
            viewHolder.tvReaderName.setTextColor(mContext.getResources().getColor(R.color.color_red_fr));
            // to check first radio button needs to display or not
            viewHolder.ivCheckedReader.setVisibility(View.VISIBLE);
            if (indexOfSelectedReader == position) {
                viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_red_check));
            } else {
                viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_red_uncheck));
            }
        }*/


        /*int musteringId = readerList.get(position);
        if (musteringId == 1) { // Rrollcall
            viewHolder.tvReaderName.setTextColor(mContext.getResources().getColor(R.color.color_red_fr));
            // to check first radio button needs to display or not
            if (!hideRadioOption) {
                viewHolder.ivCheckedReader.setVisibility(View.VISIBLE);
                if (indexOfSelectedReader == position) {
                    viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_red_check));
                } else {
                    viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_red_uncheck));
                }
            } else {
                viewHolder.ivCheckedReader.setVisibility(View.GONE);
            }
            //convertView.setBackgroundColor(mContext.getResources().getColor(R.color.color_red_fr));
        } else if (musteringId == 2) { // Headcount
            viewHolder.tvReaderName.setTextColor(mContext.getResources().getColor(R.color.color_blue_hc));
            if (!hideRadioOption) {
                viewHolder.ivCheckedReader.setVisibility(View.VISIBLE);
                if (indexOfSelectedReader == position) {
                    viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_blue_check));
                } else {
                    viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_blue_uncheck));
                }
            } else {
                viewHolder.ivCheckedReader.setVisibility(View.GONE);
            }
            //convertView.setBackgroundColor(mContext.getResources().getColor(R.color.color_blue_hc));
        } else if (musteringId == 3) { // Assembly Points
            viewHolder.tvReaderName.setTextColor(mContext.getResources().getColor(R.color.color_green_ap));
            if (!hideRadioOption) {
                viewHolder.ivCheckedReader.setVisibility(View.VISIBLE);
                if (indexOfSelectedReader == position) {
                    viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_green_check));
                } else {
                    viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_green_uncheck));
                }
            } else {
                viewHolder.ivCheckedReader.setVisibility(View.GONE);
            }
            //convertView.setBackgroundColor(mContext.getResources().getColor(R.color.color_green_ap));
        } else if (musteringId == 4) {
            viewHolder.tvReaderName.setTextColor(mContext.getResources().getColor(R.color.color_light_green_ep));
            if (!hideRadioOption) {
                viewHolder.ivCheckedReader.setVisibility(View.VISIBLE);
                if (indexOfSelectedReader == position) {
                    viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_light_green_check));
                } else {
                    viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_light_green_uncheck));
                }
            } else {
                viewHolder.ivCheckedReader.setVisibility(View.GONE);
            }
        }*/
        return convertView;
    }

    public class ViewHolder {
        ImageView ivCheckedReader;
        TextView tvReaderName;
    }
}
