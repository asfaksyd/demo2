package com.smartportable.demo2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.smartportable.demo2.R
import com.smartportable.demo2.models.ReaderInfoModel
import kotlinx.android.synthetic.main.reader_list_row_item.view.*

class ReaderListAdapter(mContext: Context, readersList: ArrayList<ReaderInfoModel.DataBean>): RecyclerView.Adapter<ReaderListAdapter.ReaderViewHolder>() {
    var mContext : Context ?= mContext
    var readersList : ArrayList<ReaderInfoModel.DataBean> = readersList


    fun updateReadersList (readersList : ArrayList<ReaderInfoModel.DataBean>) {
        this.readersList = readersList
        System.out.println("Reader List: "+readersList.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReaderListAdapter.ReaderViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.reader_list_row_item, parent, false)
        return ReaderViewHolder(view)
    }

    class ReaderViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tvReaderId = itemView.tvReaderId
        val tvReaderName = itemView.tvReaderName1
        val ivReaderType = itemView.ivReaderType

    }

    override fun getItemCount(): Int {
        return readersList.size
    }

    override fun onBindViewHolder(holder: ReaderViewHolder, position: Int) {
        //holder.tvReaderId.text = ""+readersList.get(position).readerId + "  -  "
        holder.tvReaderName.text = readersList.get(position).readerName
        System.out.println("Reader Namne: "+readersList.get(position).readerName)
        if (readersList.get(position).readerType.equals("Entry")) {
            holder.ivReaderType.setImageResource(R.drawable.reader_in)
        } else {
            holder.ivReaderType.setImageResource(R.drawable.reader_out)
        }

    }
}