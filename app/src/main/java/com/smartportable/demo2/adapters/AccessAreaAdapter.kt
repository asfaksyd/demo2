package com.smartportable.demo2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.smartportable.demo2.R
import com.smartportable.demo2.models.AGInfoModel
import kotlinx.android.synthetic.main.activity_access_area.view.*

class AccessAreaAdapter(private val mContext: Context, mArrayList: java.util.ArrayList<AGInfoModel.DataBean?>) : RecyclerView.Adapter<AccessAreaAdapter.ViewHolder>() {
    private var mAGList = ArrayList<AGInfoModel.DataBean?>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.activity_access_area, parent, false)

        return ViewHolder(view)
    }

    fun setList (mAGList: ArrayList<AGInfoModel.DataBean?>) {
        this.mAGList.clear()
        this.mAGList.addAll(mAGList)
    }
    override fun getItemCount(): Int {
        return mAGList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        /* holder.mTvCatId.text = mArrayList[position].title
         holder.mTvCatDesc.text = mArrayList[position].description
         holder.mTvSubcatId.text = mArrayList[position].title
         holder.mTvSubcatDesc.text = mArrayList[position].description*/

        holder.tvAAName.text = ""+mAGList.get(position)!!.agId + " - " + mAGList.get(position)!!.agName
        holder.tvAADesc.text = ""+mAGList.get(position)!!.agDescription
        holder.tvAACanteenType.text = ""+mAGList.get(position)!!.canteenType
        val isCanteen = mAGList.get(position)!!.IsCanteen
        if (isCanteen.equals("0")) {
            holder.tvAACanteenType.visibility = View.GONE
            holder.ivAAIcon.setImageResource(R.drawable.area_512)
        } else {
            holder.tvAACanteenType.visibility = View.VISIBLE
            holder.ivAAIcon.setImageResource(R.drawable.canteen_512)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvAAName = view.tvAAName!!
        val tvAADesc = view.tvAADesc!!
        val tvAACanteenType = view.tvAANCanteenType!!
        val ivAAIcon = view.ivAAIcon!!
    }
}