package com.smartportable.demo2.adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.smartportable.demo2.R;
import com.smartportable.demo2.fragments.StudentDetailFragment;
import com.smartportable.demo2.models.Student;

import java.util.ArrayList;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class SearchStudentAdapter extends RecyclerView.Adapter<SearchStudentAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<Student> mList;
    FragmentManager fm;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvStId, tvStName, tvStCardNo, tvStDeptName;
        public ImageView ivStPhoto;
        View itemView;

        public MyViewHolder(View view) {
            super(view);
            itemView = view;
            tvStId = (TextView) view.findViewById(R.id.tvStId);
            tvStName = (TextView) view.findViewById(R.id.tvStName);
            tvStCardNo = (TextView) view.findViewById(R.id.tvStCardNo);
            tvStDeptName = (TextView) view.findViewById(R.id.tvStDeptName);
            ivStPhoto = (ImageView) view.findViewById(R.id.ivStPhoto);
        }
    }

    public SearchStudentAdapter (Context mContext, ArrayList<Student> mList, FragmentManager fm){
        this.mContext = mContext;
        this.mList = mList;
        this.fm = fm;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_st_item_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Student student = mList.get(position);
        holder.tvStId.setText(student.getStId()+" - "+student.getStName());
        holder.tvStName.setVisibility(View.GONE);
        holder.tvStCardNo.setText(student.getStCardno());
        holder.tvStDeptName.setText(student.getStDeptName());
        holder.ivStPhoto.setImageResource(student.getStPhoto());

        holder.ivStPhoto.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                System.out.println("Test CLick");
                //zoomImageFromThumb(holder.ivStPhoto, student.getStPhoto());
                showDialog(student.getStPhoto());
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BottomSheetDialogFragment bottomSheetDialogFragment = new StudentDetailFragment();
                //((StudentDetailFragment) bottomSheetDialogFragment).setStudentDetail(student);
                //((EmployeeDetailFragment)bottomSheetDialogFragment).isForMemberAccess(forMemAccess);
                bottomSheetDialogFragment.show(fm, bottomSheetDialogFragment.getTag());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void updateList (ArrayList<Student> mList) {
        this.mList.clear();
        this.mList = mList;
    }

    private void showDialog (int resId) {
        final Dialog mSplashDialog = new Dialog(mContext, android.R.style.Theme_Material_NoActionBar_Fullscreen);
        //mSplashDialog.requestWindowFeature((int) window.FEATURE_NO_TITLE);
        mSplashDialog.setContentView(R.layout.dialog_profile_photo);
        //Drawable d = mContext.getResources().getDrawable(resId);
        //Bitmap b = ((BitmapDrawable) d ).getBitmap();
        ImageView ivPPFull = ((ImageView)mSplashDialog.findViewById(R.id.ivStPPFull));
        ImageView ivClosePPFull = (ImageView) mSplashDialog.findViewById(R.id.ivClosePPFull);

        ivPPFull.setImageResource(resId);//Bitmap(b);

        PhotoViewAttacher photoViewAttacher = new PhotoViewAttacher(ivPPFull);

        photoViewAttacher.update();

        ivClosePPFull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSplashDialog.cancel();
            }
        });

        mSplashDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mSplashDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mSplashDialog.setCancelable(true);
        mSplashDialog.show();

    }

}
