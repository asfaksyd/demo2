package com.smartportable.demo2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.smartportable.demo2.R
import com.smartportable.demo2.fragments.StudentDetailFragment
import com.smartportable.demo2.helper.SaveImages
import com.smartportable.demo2.models.StudentInfoModel
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.row_item_stud_info.view.*
import kotlinx.android.synthetic.main.student_item_loading.view.*

class StudentInfoAdapter(mContext: Context, readersList: ArrayList<StudentInfoModel.DataBean?>, fm: FragmentManager): RecyclerView.Adapter<StudentInfoAdapter.StudentInfoViewHolder>() {
    var mContext : Context ?= mContext
    var studentList : ArrayList<StudentInfoModel.DataBean?> = readersList
    var forMemberAccess : Boolean = false
    var fm : FragmentManager = fm

    fun updateStudentInfo (readersList : ArrayList<StudentInfoModel.DataBean?>, forMemAcc: Boolean) {
        this.studentList = readersList
        forMemberAccess = forMemAcc
        //System.out.println("Reader List: "+readersList.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentInfoAdapter.StudentInfoViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_item_stud_info, parent, false)
        return StudentInfoAdapter.StudentInfoViewHolder(view)
    }

    class StudentInfoViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val ivStudPhoto = itemView.ivStudPhoto as CircleImageView
        val tvStudId = itemView.tvStudId
        val tvStudName = itemView.tvStudName
        val tvStudDept = itemView.tvStudDept
        val tvStudMealNo = itemView.tvStudMealNo
        val tvStudCardlNo = itemView.tvStudCardlNo

    }

    class StudentLoadingViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val progressBar = itemView.progressBar as ProgressBar
    }

    override fun getItemCount(): Int {
        return studentList.size
    }



    override fun onBindViewHolder(holder: StudentInfoAdapter.StudentInfoViewHolder, position: Int) {

        holder.tvStudId.text = "" + studentList.get(position)!!.studentID

        holder.tvStudName.text = studentList.get(position)!!.firstName + studentList.get(position)!!.fatherName + studentList.get(position)!!.grandFatherName
        //System.out.println("Name Namne: "+staffList.get(position).firstName)
        holder.tvStudDept.text = studentList.get(position)!!.department
        holder.tvStudMealNo.text = studentList.get(position)!!.mealNumber
        holder.tvStudCardlNo.text = studentList.get(position)!!.cardNumber

        holder.itemView.setOnClickListener(View.OnClickListener {

            showMemberAccDialog(position)
        })


        //var baseUrl = SharedPref.getStringValue(mContext!!, keyBaseUrl, "")
        var url = studentList.get(position)!!.studentImage
        System.out.println("Image Path: "+url)
        if (url != null) {
            var si = SaveImages()
            var img = si.loadImageFromStorage(url)

            Glide.with(mContext!!)
                    .asBitmap()
                    .load(img)
                    //.diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    //.onlyRetrieveFromCache(true)
                    //.placeholder(android.R.drawable.ic_menu_report_image)
                    .error(android.R.drawable.ic_menu_report_image)
                    .into(holder.ivStudPhoto)

        } else {
            Glide.with(mContext!!)
                    .asBitmap()
                    .load("")
                    .placeholder(android.R.drawable.ic_menu_report_image)
                    .error(android.R.drawable.ic_menu_report_image)
                    .into(holder.ivStudPhoto)
        }


        /*if(!url.isNullOrBlank())
        {
            *//** April 18 -19 **//*
            val fName_ = url?.substring(url.lastIndexOf('/') + 1)

            val path = Environment.getExternalStorageDirectory().toString() + "/ImageSave_play/StudentImage/"+fName_;

            var imgFile =  File(path);

            if(imgFile.exists())
            {
                 Glide.with(holder.ivStudPhoto.context)
                        .load(imgFile.absolutePath)
                         .error(android.R.drawable.ic_menu_report_image)
                        .into(holder.ivStudPhoto);

                *//*val myBitmap = BitmapFactory.decodeFile(imgFile.absolutePath)

                holder.ivStudPhoto.setImageBitmap(myBitmap)*//*


            }
            else
            {
                Log.e("Transform", "Transform----image--not---present--");

                if(isNetworkAvailable(holder.ivStudPhoto.context))
                    ImageToLocal(holder.ivStudPhoto.context,url,holder.ivStudPhoto,"StudentImage");
                else
                    holder.ivStudPhoto.setImageResource(android.R.drawable.ic_menu_report_image);
            }
        }
        else
            holder.ivStudPhoto.setImageResource(android.R.drawable.ic_menu_report_image);*/

    }


    fun showMemberAccDialog (position: Int) {
        val bottomSheetDialogFragment = StudentDetailFragment()
        bottomSheetDialogFragment.setStudentDetail(studentList.get(position))
        bottomSheetDialogFragment.isForMemberAccess(forMemberAccess)
        bottomSheetDialogFragment.show(fm, bottomSheetDialogFragment.tag)
    }
}