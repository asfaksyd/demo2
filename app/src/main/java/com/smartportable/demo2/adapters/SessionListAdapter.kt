package com.smartportable.demo2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.smartportable.demo2.R
import com.smartportable.demo2.models.SessionsInfoModel
import kotlinx.android.synthetic.main.session_list_row_item.view.*

class SessionListAdapter(mContext: Context, sessionsList: ArrayList<SessionsInfoModel.DataBean>): RecyclerView.Adapter<SessionListAdapter.ReaderViewHolder>() {
    var mContext : Context ?= mContext
    var sessionsList : ArrayList<SessionsInfoModel.DataBean> = sessionsList


    fun updateSessionsList (sessionsList : ArrayList<SessionsInfoModel.DataBean>) {
        this.sessionsList = sessionsList
        System.out.println("Reader List: "+sessionsList.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SessionListAdapter.ReaderViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.session_list_row_item, parent, false)
        return ReaderViewHolder(view)
    }

    class ReaderViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tvSessionId = itemView.tvSessionId
        val tvSessionName = itemView.tvSessionName
        //val ivReaderType = itemView.ivReaderType

    }

    override fun getItemCount(): Int {
        return sessionsList.size
    }

    override fun onBindViewHolder(holder: ReaderViewHolder, position: Int) {
        //holder.tvReaderId.text = ""+readersList.get(position).readerId + "  -  "
        holder.tvSessionName.text = sessionsList.get(position).sessionId + " - " + sessionsList.get(position).sessionName
        System.out.println("Reader Namne: "+sessionsList.get(position).sessionName)


    }
}