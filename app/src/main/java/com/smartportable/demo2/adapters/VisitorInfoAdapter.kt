package com.smartportable.demo2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import com.smartportable.demo2.R
import com.smartportable.demo2.fragments.VisitorDetailFragment
import com.smartportable.demo2.models.VisitorInfoModel
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.row_item_visitor_info.view.*

class VisitorInfoAdapter(mContext: Context, visitorList: ArrayList<VisitorInfoModel.DataBean?>, fm: FragmentManager): RecyclerView.Adapter<VisitorInfoAdapter.VisitorInfoViewHolder>() {
    var mContext : Context ?= mContext
    var visitorList : ArrayList<VisitorInfoModel.DataBean?> = ArrayList()
    var forMemberAccess : Boolean = false
    var fm : FragmentManager = fm


    fun updateVisitorInfo (visitorList : ArrayList<VisitorInfoModel.DataBean?>, forMemAcc: Boolean) {
        this.visitorList = visitorList
        forMemberAccess = forMemAcc
        //System.out.println("Reader List: "+accessAreaList.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VisitorInfoAdapter.VisitorInfoViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_item_visitor_info, parent, false)
        return VisitorInfoAdapter.VisitorInfoViewHolder(view)
    }

    class VisitorInfoViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val ivVisPhoto = itemView.ivVisPhoto as CircleImageView
        val tvVisRegNo = itemView.tvVisRegNo
        val tvVisName = itemView.tvVisName
        val tvVisComp = itemView.tvVisComp
        val tvVisCardNo = itemView.tvVisCardNo

    }

    override fun getItemCount(): Int {
        return visitorList.size
    }

    override fun onBindViewHolder(holder: VisitorInfoAdapter.VisitorInfoViewHolder, position: Int) {
        holder.tvVisRegNo.text = ""+visitorList.get(position)!!.visitorRegNo + " - " + visitorList.get(position)!!.visitorFirstName + visitorList.get(position)!!.visitorLasttName

        //holder.tvStudName.text = accessAreaList.get(position).fullName
        holder.tvVisName.visibility = View.GONE
        //System.out.println("Name Namne: "+accessAreaList.get(position).fullName)
        holder.tvVisComp.text = visitorList.get(position)!!.companyName
        holder.tvVisCardNo.text = visitorList.get(position)!!.visitorCardNumber

        holder.itemView.setOnClickListener(View.OnClickListener {

            showVisitorInfoDialog(position)
        })

        var visPhoto: String? = visitorList.get(position)!!.visitorPhoto
        if (!visPhoto.isNullOrEmpty()) {
            //var charStr = visPhoto!!.substring(0, 1)
            //visPhoto = visPhoto!!.removePrefix("~")

            Glide.with(mContext!!)
                    .load(visPhoto)
                    .placeholder(android.R.drawable.ic_menu_report_image)
                    .error(android.R.drawable.ic_menu_report_image)
                    .into(holder.ivVisPhoto)
        }


        /*var baseUrl = SharedPref.getStringValue(mContext!!, keyBaseUrl, "")

        var url = baseUrl + accessAreaList.get(position)!!.visitorPhoto;
*/

        /*Log.e("ldfjlkdfjf", "Target--333-saveStaffInfo--StaffName: "+accessAreaList.get(position)!!.visitorPhoto.isNullOrBlank()+"<<>>"+url+"<<>>"+accessAreaList.get(position)!!.visitorPhoto);

        if(!accessAreaList.get(position)!!.visitorPhoto.isNullOrBlank())
        {
            *//** April 18 -19 **//*
            val fName_ = url?.substring(url.lastIndexOf('/') + 1)

            val path = Environment.getExternalStorageDirectory().toString() + "/.wollega/StaffImage/"+fName_;

            var imgFile =  File(path);

            if(imgFile.exists())
            {
                Glide.with(holder.ivVisPhoto.context)
                        .load(imgFile.absolutePath)
                        .error(android.R.drawable.ic_menu_report_image)
                        .into(holder.ivVisPhoto);
            }
            else
            {
                Log.e("Transform", "Transform----image--not---present-StaffInfoAdapter-"+url);

                if(isNetworkAvailable(holder.ivVisPhoto.context))
                    ImageToLocal(holder.ivVisPhoto.context,url,holder.ivVisPhoto,"StaffImage")
                else
                    holder.ivVisPhoto.setImageResource(android.R.drawable.ic_menu_report_image);
            }
        }
        else
            holder.ivVisPhoto.setImageResource(android.R.drawable.ic_menu_report_image);*/



        /*Glide.with(mContext!!)
                .load(baseUrl + accessAreaList.get(position)!!.empPhoto)
                .placeholder(android.R.drawable.ic_menu_report_image)
                .error(android.R.drawable.ic_menu_report_image)
                .into(holder.ivStudPhoto)*/

    }

    fun showVisitorInfoDialog (position: Int) {
        val bottomSheetDialogFragment = VisitorDetailFragment()
        bottomSheetDialogFragment.setVisitorDetail(visitorList.get(position))
        bottomSheetDialogFragment.isForMemberAccess(forMemberAccess)
        bottomSheetDialogFragment.show(fm, bottomSheetDialogFragment.tag)
    }
}