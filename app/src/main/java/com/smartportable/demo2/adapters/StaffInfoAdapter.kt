package com.smartportable.demo2.adapters

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.smartportable.demo2.R
import com.smartportable.demo2.fragments.StaffDetailFragment
import com.smartportable.demo2.helper.SaveImages
import com.smartportable.demo2.models.StaffInfoModel
import com.smartportable.demo2.utils.SharedPref
import com.smartportable.demo2.utils.keyBaseUrl
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.row_item_stud_info.view.*

class StaffInfoAdapter(mContext: Context, staffList: ArrayList<StaffInfoModel.DataBean?>, fm: FragmentManager): RecyclerView.Adapter<StaffInfoAdapter.StaffInfoViewHolder>() {
    var mContext : Context ?= mContext
    var staffList : ArrayList<StaffInfoModel.DataBean?> = staffList
    var forMemberAccess : Boolean = false
    var fm : FragmentManager = fm


    fun updateStaffInfo (staffList : ArrayList<StaffInfoModel.DataBean?>, forMemAcc: Boolean) {
        this.staffList = staffList
        forMemberAccess = forMemAcc
        //System.out.println("Reader List: "+staffList.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StaffInfoAdapter.StaffInfoViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_item_stud_info, parent, false)
        return StaffInfoAdapter.StaffInfoViewHolder(view)
    }

    class StaffInfoViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val ivStudPhoto = itemView.ivStudPhoto as CircleImageView
        val tvStudId = itemView.tvStudId
        val tvStudName = itemView.tvStudName
        val tvStudDept = itemView.tvStudDept
        val tvStudMealNo = itemView.tvStudMealNo

    }

    override fun getItemCount(): Int {
        return staffList.size
    }

    override fun onBindViewHolder(holder: StaffInfoAdapter.StaffInfoViewHolder, position: Int) {
        holder.tvStudId.text = ""+staffList.get(position)!!.staffID + " - " + staffList.get(position)!!.fullName

        //holder.tvStudName.text = staffList.get(position).fullName
        holder.tvStudName.visibility = View.GONE
        //System.out.println("Name Namne: "+staffList.get(position).fullName)
        holder.tvStudDept.text = staffList.get(position)!!.department
        holder.tvStudMealNo.text = staffList.get(position)!!.jobTitle

        holder.itemView.setOnClickListener(View.OnClickListener {

            showMemberAccDialog(position)
        })


        var baseUrl = SharedPref.getStringValue(mContext!!, keyBaseUrl, "")

        var staffUrl = staffList.get(position)!!.empPhoto;


        /*Log.e("ldfjlkdfjf", "Target--333-saveStaffInfo--StaffName: "+staffList.get(position)!!.empPhoto.isNullOrBlank()+"<<>>"+url+"<<>>"+staffList.get(position)!!.empPhoto);

        if(!staffList.get(position)!!.empPhoto.isNullOrBlank())
        {
            /** April 18 -19 **/
            val fName_ = url?.substring(url.lastIndexOf('/') + 1)

            val path = Environment.getExternalStorageDirectory().toString() + "/ImageSave_play/StaffImage/"+fName_;

            var imgFile =  File(path);

            if(imgFile.exists())
            {
                Glide.with(holder.ivStudPhoto.context)
                        .load(imgFile.absolutePath)
                        .error(android.R.drawable.ic_menu_report_image)
                        .into(holder.ivStudPhoto);
            }
            else
            {
                Log.e("Transform", "Transform----image--not---present-StaffInfoAdapter-"+url);

                if(isNetworkAvailable(holder.ivStudPhoto.context))
                    ImageToLocal(holder.ivStudPhoto.context,url,holder.ivStudPhoto,"StaffImage")
                else
                    holder.ivStudPhoto.setImageResource(android.R.drawable.ic_menu_report_image);
            }
        }
        else
            holder.ivStudPhoto.setImageResource(android.R.drawable.ic_menu_report_image);*/

        if(staffUrl == null)
            staffUrl = ""

        if (!staffUrl.isNullOrEmpty()) {
            if (staffUrl.isNotEmpty()) {
                var staffBmp: Bitmap = SaveImages.getInstance().loadImageFromStorage(staffUrl)


                Glide.with(mContext!!)
                        .asBitmap()
                        .load(staffBmp)
                        .placeholder(android.R.drawable.ic_menu_report_image)
                        .error(android.R.drawable.ic_menu_report_image)
                        .into(holder.ivStudPhoto)
            } else {
                Glide.with(mContext!!)
                        .asBitmap()
                        .load("")
                        .placeholder(android.R.drawable.ic_menu_report_image)
                        .error(android.R.drawable.ic_menu_report_image)
                        .into(holder.ivStudPhoto)
            }
        } else {
            Glide.with(mContext!!)
                    .asBitmap()
                    .load("")
                    .placeholder(android.R.drawable.ic_menu_report_image)
                    .error(android.R.drawable.ic_menu_report_image)
                    .into(holder.ivStudPhoto)
        }


    }

    fun showMemberAccDialog (position: Int) {
        val bottomSheetDialogFragment = StaffDetailFragment()
        bottomSheetDialogFragment.setStaffDetail(staffList.get(position))
        bottomSheetDialogFragment.isForMemberAccess(forMemberAccess)
        bottomSheetDialogFragment.show(fm, bottomSheetDialogFragment.tag)
    }
}