package com.smartportable.demo2.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.smartportable.demo2.R;
import com.smartportable.demo2.fragments.PunchLogsDetailFragment;
import com.smartportable.demo2.helper.PunchLogs;
import com.smartportable.demo2.helper.SaveImages;
import com.smartportable.demo2.interfaces.OnLoadMoreListener;
import com.smartportable.demo2.interfaces.SelectedListListener;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;


public class PunchlogsItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected boolean showDynamicTags = false;
    ArrayList<PunchLogs> list_employee = new ArrayList<PunchLogs>();
    public ArrayList<String> itemsPendingRemoval = new ArrayList<String>();
    Context mContext;
    boolean whichAct = true;
    byte[] data = null;
    SaveImages saveImage;
    boolean imageProcessed = false;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    boolean isLoading = false, undoOn = false;
    public OnLoadMoreListener mOnLoadMoreListener;
    public SelectedListListener listChangeListener;
    private int lastVisibleItem, totalItemCount, visibleThreshold =5;
    public Handler handler = new Handler(); // hanlder for running delayed runnables
    public HashMap<String, Runnable> pendingRunnables = new HashMap<>(); // map of items to pending runnables,

    FragmentManager fm;

    public void setListener (SelectedListListener listChangeListener) {
        this.listChangeListener = listChangeListener;
    }


    public PunchlogsItemAdapter(Context mContext, ArrayList<PunchLogs> list_employee, boolean whichAct, FragmentManager fm) {
        this.mContext = mContext;
        this.list_employee = list_employee;
        saveImage = SaveImages.getInstance();
        this.whichAct = whichAct;
        this.fm = fm;
    }

    private View.OnClickListener onTagTvClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view instanceof  TextView && mOnTagClickedListener != null) {
                TextView tv = (TextView) view;
                final String tag = tv.getText().toString();
                mOnTagClickedListener.onTagClicked(tag);
            }
        }
    };
    private OnTagClickedListener mOnTagClickedListener = null;

    public PunchlogsItemAdapter(Context mContext, ArrayList<PunchLogs> list_employee, RecyclerView mRecyclerView, boolean whichAct, FragmentManager fm) {
        this.mContext = mContext;
        this.list_employee = list_employee;
        this.whichAct = whichAct;
        this.fm = fm;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        if (linearLayoutManager != null) {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return list_employee.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.employee_row_item, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View viewLoading = LayoutInflater.from(mContext).inflate(R.layout.footer_layout, parent, false);
            return new LoadingViewHolder(viewLoading);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, final int position) {
        if (mHolder instanceof UserViewHolder) {
            final UserViewHolder holder = (UserViewHolder) mHolder;
            final PunchLogs item = list_employee.get(position);
            String emp_id = item.getSLN_Employee();
            String emp_name = item.getSLN_EmployeeName();
            String empPhoto = item.getSLN_EmployeePhoto();
            final String card_no = item.getCard_Number();
            int loc_id = item.getSLN_Location();
            String datetime = item.getPunch_Date();
            int cat_id = item.getMusteringId();
            boolean grantDeny = item.isGrant_Deny();
            int grentDenyInt = item.getIsPunchSynced();

            if (itemsPendingRemoval.contains(card_no)) {
                // we need to show the "undo" state of the row
                holder.llItemView.setVisibility(View.INVISIBLE);
                holder.ivCheckBox.setVisibility(View.GONE);
                holder.itemView.setBackgroundColor(Color.RED);
                holder.btnUndo.setVisibility(View.VISIBLE);
                holder.btnUndo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // user wants to undo the removal, let's cancel the pending task
                        Runnable pendingRemovalRunnable = pendingRunnables.get(card_no);
                        pendingRunnables.remove(card_no);
                        if (pendingRemovalRunnable != null) handler.removeCallbacks(pendingRemovalRunnable);
                        itemsPendingRemoval.remove(card_no);
                        // this will rebind the row in "normal" state
                        notifyItemChanged(list_employee.indexOf(item));
                    }
                });
            } else {
                // we need to show the "normal" state
                holder.llItemView.setVisibility(View.VISIBLE);
                holder.ivCheckBox.setVisibility(View.VISIBLE);
                holder.itemView.setBackgroundColor(Color.WHITE);
                holder.btnUndo.setVisibility(View.GONE);
                holder.btnUndo.setOnClickListener(null);

                // for the background color based on category
                holder.itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));

                holder.statusColor.setVisibility(View.VISIBLE);

                if (!whichAct) { //  false means from recent track location
                    if (cat_id == -1 && grantDeny == false && grentDenyInt == 0 || grentDenyInt == 2) {
                        if (grentDenyInt == 0) {
                            holder.tvEmpQueued.setVisibility(View.VISIBLE);
                        }
                        //holder.itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
                        holder.statusColor.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
                        ((CircleImageView)holder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(android.R.color.darker_gray));
                    } else if (cat_id == 1) { // Rollcall
                        if (grentDenyInt == 0) {
                            holder.tvEmpQueued.setVisibility(View.VISIBLE);
                            //holder.itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            /*holder.tvEmpId.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            holder.tvEmpName.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            holder.tvEmpCardNo.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            holder.tvEmpLocId.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            holder.tvEmpPunchDateTime.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));*/

                            holder.statusColor.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            ((CircleImageView)holder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(android.R.color.darker_gray));
                        } else {
                            holder.tvEmpQueued.setVisibility(View.GONE);
                            /*holder.tvEmpId.setTextColor(mContext.getResources().getColor(R.color.color_red_fr));
                            holder.tvEmpName.setTextColor(mContext.getResources().getColor(R.color.color_red_fr));
                            holder.tvEmpCardNo.setTextColor(mContext.getResources().getColor(R.color.color_red_fr));
                            holder.tvEmpLocId.setTextColor(mContext.getResources().getColor(R.color.color_red_fr));
                            holder.tvEmpPunchDateTime.setTextColor(mContext.getResources().getColor(R.color.color_red_fr));*/

                            holder.statusColor.setBackgroundColor(mContext.getResources().getColor(R.color.color_red_fr));
                            ((CircleImageView)holder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(R.color.color_red_fr));
                            //holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.color_blue_hc));
                            //holder.itemView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.emp_fr_bg));
                        }
                    } else if (cat_id == 2) { // Headcount
                        if (grentDenyInt == 0) {
                            holder.tvEmpQueued.setVisibility(View.VISIBLE);
                            //holder.itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            /*holder.tvEmpId.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            holder.tvEmpName.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            holder.tvEmpCardNo.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            holder.tvEmpLocId.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            holder.tvEmpPunchDateTime.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));*/

                            holder.statusColor.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            ((CircleImageView)holder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(android.R.color.darker_gray));
                        } else {
                            holder.tvEmpQueued.setVisibility(View.GONE);
                            //holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.color_red_fr));
                            //holder.itemView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.emp_hc_bg));
                            /*holder.tvEmpId.setTextColor(mContext.getResources().getColor(R.color.color_blue_hc));
                            holder.tvEmpName.setTextColor(mContext.getResources().getColor(R.color.color_blue_hc));
                            holder.tvEmpCardNo.setTextColor(mContext.getResources().getColor(R.color.color_blue_hc));
                            holder.tvEmpLocId.setTextColor(mContext.getResources().getColor(R.color.color_blue_hc));
                            holder.tvEmpPunchDateTime.setTextColor(mContext.getResources().getColor(R.color.color_blue_hc));*/

                            holder.statusColor.setBackgroundColor(mContext.getResources().getColor(R.color.color_blue_hc));
                            ((CircleImageView)holder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(R.color.color_blue_hc));
                        }
                    } else if (cat_id == 3) { // 3 - Assembly Points
                        if (grentDenyInt == 0) {
                            holder.tvEmpQueued.setVisibility(View.VISIBLE);
                            //holder.itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            /*holder.tvEmpId.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            holder.tvEmpName.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            holder.tvEmpCardNo.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            holder.tvEmpLocId.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            holder.tvEmpPunchDateTime.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));*/
                            //holder.itemView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.emp_fr_bg));
                            holder.statusColor.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            ((CircleImageView)holder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(android.R.color.darker_gray));
                        } else {
                            holder.tvEmpQueued.setVisibility(View.GONE);
                            //holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.color_green_ap));
                            //holder.itemView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.emp_ap_bg));
                            /*holder.tvEmpId.setTextColor(mContext.getResources().getColor(R.color.color_green_ap));
                            holder.tvEmpName.setTextColor(mContext.getResources().getColor(R.color.color_green_ap));
                            holder.tvEmpCardNo.setTextColor(mContext.getResources().getColor(R.color.color_green_ap));
                            holder.tvEmpLocId.setTextColor(mContext.getResources().getColor(R.color.color_green_ap));
                            holder.tvEmpPunchDateTime.setTextColor(mContext.getResources().getColor(R.color.color_green_ap));*/

                            holder.statusColor.setBackgroundColor(mContext.getResources().getColor(R.color.color_green_ap));
                            ((CircleImageView)holder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(R.color.color_green_ap));
                        }
                    }  else if (cat_id == 4) { // 4 - Exit Point
                        if (grentDenyInt == 0) {
                            holder.tvEmpQueued.setVisibility(View.VISIBLE);
                            //holder.itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            /*holder.tvEmpId.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            holder.tvEmpName.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            holder.tvEmpCardNo.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            holder.tvEmpLocId.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            holder.tvEmpPunchDateTime.setTextColor(mContext.getResources().getColor(android.R.color.darker_gray));*/
                            //holder.itemView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.emp_fr_bg));
                            holder.statusColor.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
                            ((CircleImageView)holder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(android.R.color.darker_gray));
                        } else {
                            holder.tvEmpQueued.setVisibility(View.GONE);
                            //holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.color_green_ap));
                            //holder.itemView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.emp_ap_bg));
                            /*holder.tvEmpId.setTextColor(mContext.getResources().getColor(R.color.color_green_ap));
                            holder.tvEmpName.setTextColor(mContext.getResources().getColor(R.color.color_green_ap));
                            holder.tvEmpCardNo.setTextColor(mContext.getResources().getColor(R.color.color_green_ap));
                            holder.tvEmpLocId.setTextColor(mContext.getResources().getColor(R.color.color_green_ap));
                            holder.tvEmpPunchDateTime.setTextColor(mContext.getResources().getColor(R.color.color_green_ap));*/

                            holder.statusColor.setBackgroundColor(mContext.getResources().getColor(R.color.color_light_green_ep));
                            ((CircleImageView)holder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(R.color.color_light_green_ep));
                        }
                    }
                } else { // for the recent track pucnh location
                    if (cat_id == -1 && grantDeny == false && grentDenyInt == 0 || grentDenyInt == 2) {
                        if (grentDenyInt == 0) {
                            holder.tvEmpQueued.setVisibility(View.VISIBLE);
                        }
                        //holder.itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
                        holder.statusColor.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
                        ((CircleImageView)holder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(android.R.color.darker_gray));
                    } else if (cat_id == 1) { // Rollcall
                        holder.statusColor.setBackgroundColor(mContext.getResources().getColor(R.color.color_red_fr));
                        ((CircleImageView)holder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(R.color.color_red_fr));
                    } else if (cat_id == 2) { // Headcount
                        holder.statusColor.setBackgroundColor(mContext.getResources().getColor(R.color.color_blue_hc));
                        ((CircleImageView)holder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(R.color.color_blue_hc));
                    } else if (cat_id == 3) { // 3 - Assembly Points
                        holder.statusColor.setBackgroundColor(mContext.getResources().getColor(R.color.color_green_ap));
                        ((CircleImageView)holder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(R.color.color_green_ap));
                    } else if (cat_id == 4) { // 4 - Exit Point
                        holder.statusColor.setBackgroundColor(mContext.getResources().getColor(R.color.color_light_green_ep));
                        ((CircleImageView)holder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(R.color.color_light_green_ep));
                    }
                }
            }

            holder.statusColor.setVisibility(View.GONE);

            holder.ivCheckBox.setVisibility(View.GONE);
            holder.tvEmpId.setText(""+emp_id);
            if (emp_name != null && emp_name.length() > 0)
                holder.tvEmpName.setText(emp_id+" - "+emp_name);
            else
                holder.tvEmpName.setText(mContext.getResources().getString(R.string.unknown_str));

            if (card_no != null && card_no.length() >0)
                holder.tvEmpCardNo.setText(""+card_no);
            else
                holder.tvEmpCardNo.setText(mContext.getResources().getString(R.string.unknown_str));

            holder.tvEmpLocId.setText(""+loc_id);
            holder.tvEmpPunchDateTime.setText(datetime);
            // image click event
            holder.ivEmpPhoto.setOnClickListener(imageClickListener);
            holder.ivEmpPhoto.setTag(R.id.ivEmpPhoto, position);

            Glide
                    .with(mContext)
                    .asBitmap()
                    .load(empPhoto)
                    .centerCrop()
                    .dontAnimate()
                    .placeholder(R.mipmap.ic_profile_pic)
                    .into(new BitmapImageViewTarget(holder.ivEmpPhoto) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            holder.ivEmpPhoto.setImageDrawable(circularBitmapDrawable);
                        }
                    });

            /*// for the background color based on category
            if (cat_id == -1 && grantDeny == false && grentDenyInt == 0 || grentDenyInt == 2) {
                if (grentDenyInt == 0) {
                    holder.tvEmpQueued.setVisibility(View.VISIBLE);
                }
                holder.itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
            } else if (cat_id == 1) {
                holder.tvEmpQueued.setVisibility(View.GONE);
                holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.color_blue_hc));
            } else if (cat_id == 2) {
                holder.tvEmpQueued.setVisibility(View.GONE);
                holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.color_red_fr));
            } else {
                holder.tvEmpQueued.setVisibility(View.GONE);
                holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.color_green_ap));
            }*/
        } else if (mHolder instanceof LoadingViewHolder) {
            LoadingViewHolder loadHolder = (LoadingViewHolder) mHolder;
        }
    }

    @Override
    public int getItemCount() {
        return list_employee.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void setLoaded() {
        isLoading = false;
    }


    public void setOnTagClickedListener(OnTagClickedListener listener) {
        this.mOnTagClickedListener = listener;
    }

    public interface OnTagClickedListener {
        void onTagClicked(String tag);
    }

    public void updateEmployeeList (ArrayList<PunchLogs> list_employee) {
        //this.list_employee.clear();
        this.list_employee = list_employee;
    }

    public void setUndoOn(boolean undoOn) {
        this.undoOn = undoOn;
    }

    public boolean isUndoOn() {
        return undoOn;
    }

    /*public void pendingRemoval(final int pos) {
        PunchLogs pl = list_employee.get(pos);
        final String card_no = pl.getCard_Number();
        if (!itemsPendingRemoval.contains(card_no)) {
            itemsPendingRemoval.add(card_no);
            // this will redraw row in "undo" state
            notifyItemChanged(pos);
            // let's create, store and post a runnable to remove the item
            Runnable pendingRemovalRunnable = new Runnable() {
                @Override
                public void run() {
                    remove(pos);
                }
            };
            handler.postDelayed(pendingRemovalRunnable, 3000);
            pendingRunnables.put(card_no, pendingRemovalRunnable);
        }
    }*/
    public void pendingRemoval(final String card_no) {
        //PunchLogs pl = list_employee.get(pos);
        //final String card_no = pl.getCard_Number();
        if (!itemsPendingRemoval.contains(card_no)) {
            itemsPendingRemoval.add(card_no);
            // this will redraw row in "undo" state
            //notifyItemChanged(pos);
            notifyDataSetChanged();
            // let's create, store and post a runnable to remove the item
            Runnable pendingRemovalRunnable = new Runnable() {
                @Override
                public void run() {
                    remove(card_no);
                }
            };
            handler.postDelayed(pendingRemovalRunnable, 3000);
            pendingRunnables.put(card_no, pendingRemovalRunnable);
        }
    }

    /*public void remove(int position) {
        PunchLogs pl = list_employee.get(position);
        String card_no = pl.getCard_Number();
        if (itemsPendingRemoval.contains(card_no)) {
            itemsPendingRemoval.remove(card_no);
        }
        *//*if (list_employee.contains(pl)) {
            list_employee.remove(position);
            notifyItemRemoved(position);
        }*//*
        listChangeListener.onItemRemoved(position);
    }*/

    public void remove(String card_no) {
        //PunchLogs pl = list_employee.get(position);
        //String card_no = pl.getCard_Number();
        if (itemsPendingRemoval.contains(card_no)) {
            itemsPendingRemoval.remove(card_no);
        }
        /*if (list_employee.contains(pl)) {
            list_employee.remove(position);
            notifyItemRemoved(position);
        }*/
        listChangeListener.onItemRemoved(card_no);
    }

    public boolean isPendingRemoval(int position) {
        String cardNumber = list_employee.get(position).getCard_Number();
        int grant_deny = list_employee.get(position).getIsPunchSynced();
        if (grant_deny == 0)
            return true;
        return itemsPendingRemoval.contains(cardNumber);
    }

    public void setImageProcessing(boolean imageProcessed) {
        this.imageProcessed = imageProcessed;
    }

    public View.OnClickListener imageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            final int position = (Integer) v.getTag(R.id.ivEmpPhoto);

            BottomSheetDialogFragment bottomSheetDialogFragment = new PunchLogsDetailFragment();
            ((PunchLogsDetailFragment)bottomSheetDialogFragment).setPunchLogsDetail(list_employee.get(position));
            ((PunchLogsDetailFragment)bottomSheetDialogFragment).setItemPosition(position);
            ((PunchLogsDetailFragment)bottomSheetDialogFragment).setWhichActivity(whichAct);
            bottomSheetDialogFragment.show(fm, bottomSheetDialogFragment.getTag());

            //final String empPhoto = (String) v.getTag();
            /*final int position = (Integer) v.getTag(R.id.ivEmpPhoto);
            final PunchLogs item = list_employee.get(position);
            int emp_id = item.getSLN_Employee();
            String emp_name = item.getSLN_EmployeeName();
            String empPhoto = item.getSLN_EmployeePhoto();
            final String card_no = item.getCard_Number();
            int loc_id = item.getSLN_Location();
            String datetime = item.getPunch_Date();
            int cat_id = item.getMusteringId();
            boolean grantDeny = item.isGrant_Deny();
            int grentDenyInt = item.getIsPunchSynced();

            if (empPhoto != null && empPhoto.length() !=0) {
                Log.d(MainActivity.TAG, "readersAdapter: empPhoto: "+empPhoto);

                final AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
                LayoutInflater inflater = LayoutInflater.from(mContext);
                View dialogLayout = inflater.inflate(R.layout.activity_scrolling, null);
                dialog.setView(dialogLayout);//, 20, 20, 20, 20);
                //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                FloatingActionButton fab = (FloatingActionButton) dialogLayout.findViewById(R.id.fabDialog);
                fab.setVisibility(View.GONE);
                final ImageView image = (ImageView) dialogLayout.findViewById(R.id.ivFullScreen);
                TextView tv_dialog_emp_id = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_id);
                TextView tv_dialog_emp_name = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_name);
                TextView tv_dialog_emp_card = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_card);
                TextView tv_dialog_emp_loc_id = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_loc_id);
                TextView tv_dialog_emp_datetime = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_datetime);

                tv_dialog_emp_name.setVisibility(View.GONE);
                tv_dialog_emp_loc_id.setVisibility(View.GONE);

                if (emp_id >= 0)
                    tv_dialog_emp_id.setText(emp_id + mContext.getString(R.string.emp_detail_seperator) + emp_name);
                if (emp_name != null && emp_name.length() > 0)
                    tv_dialog_emp_name.setText("" + emp_name);
                if (card_no != null && card_no.length() > 0) {
                    tv_dialog_emp_card.setVisibility(View.VISIBLE);
                    tv_dialog_emp_card.setText("" + card_no);
                } else {
                    tv_dialog_emp_card.setVisibility(View.GONE);
                    tv_dialog_emp_card.setText("");
                }
                if (loc_id >= 0)
                    tv_dialog_emp_loc_id.setText("" + loc_id);

                if (datetime != null && datetime.length() > 0) {
                    tv_dialog_emp_datetime.setText(""+datetime);
                }

                dialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog closed
                    }
                });
                Glide
                        .with(mContext)
                        .load(empPhoto)
                        //.placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_profile_pic)
                        .crossFade()
                        .into(image);

                dialog.show();

            }*/
        }
    };

    public class UserViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmpId, tvEmpName, tvEmpQueued, tvEmpCardNo, tvEmpLocId, tvEmpPunchDateTime;
        ImageView ivEmpPhoto;
        CheckBox ivCheckBox;
        LinearLayout llItemView;
        Button btnUndo;
        View statusColor;
        public UserViewHolder (View itemview) {
            super(itemview);
            tvEmpId = (TextView) itemView.findViewById(R.id.tvEmpId);
            tvEmpName = (TextView) itemView.findViewById(R.id.tvEmpName);
            tvEmpQueued = (TextView) itemView.findViewById(R.id.tvEmpQueued);
            ivEmpPhoto = (ImageView) itemView.findViewById(R.id.ivEmpPhoto);
            tvEmpCardNo = (TextView) itemView.findViewById(R.id.tvEmpCardNo);
            tvEmpLocId = (TextView) itemView.findViewById(R.id.tvEmpLocId);
            tvEmpPunchDateTime = (TextView) itemView.findViewById(R.id.tvEmpPunchDateTime);
            ivCheckBox = (CheckBox) itemView.findViewById(R.id.ivCheckBox);
            llItemView = (LinearLayout) itemView.findViewById(R.id.llItemView);
            btnUndo = (Button) itemView.findViewById(R.id.undo_button);
            statusColor = (View) itemView.findViewById(R.id.statusColor);
        }
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar pbLoadMore;
        public TextView tvPBTitle;
        public LoadingViewHolder(View itemview) {
            super(itemview);
            tvPBTitle = (TextView) itemView.findViewById(R.id.tvPBTitle);
            pbLoadMore = (ProgressBar) itemView.findViewById(R.id.pbLoadMore);
        }
    }
}
