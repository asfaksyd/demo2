package com.smartportable.demo2.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.smartportable.demo2.R;
import com.smartportable.demo2.models.MainGateLogs;

import java.util.ArrayList;

/**
 * Created by Ananth on 2/1/2017.
 */

public class MGAMultipleLogsAdapter2 extends BaseAdapter {
    Context mContext;
    LayoutInflater inflater;
    ArrayList<MainGateLogs> readerList;
    boolean enable = false;
    int indexOfSelectedReader = -1;
    boolean hideRadioOption = false;

    public MGAMultipleLogsAdapter2(Context mContext, ArrayList<MainGateLogs> readerList) {
        this.mContext = mContext;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.readerList = readerList;
        this.indexOfSelectedReader = indexOfSelectedReader;

    }

    public void enableHideRadioOption (boolean hideRadioOption) {
        this.hideRadioOption = hideRadioOption;
    }

    public void enableCheckBox(boolean enable) {
        this.enable = enable;
    }

    @Override
    public int getCount() {
        return readerList.size();
    }

    @Override
    public Object getItem(int position) {
        return readerList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.mga_multi_logs_row_item, parent, false);
            viewHolder = new ViewHolder();

            viewHolder.tvMGAMultiLogsStAccName = (TextView) convertView.findViewById(R.id.tvMGAMultiLogsStAccName);
            viewHolder.tvMGAMultiLogsStCampusName = (TextView) convertView.findViewById(R.id.tvMGAMultiLogsStCapusName);
            viewHolder.tvMGAMultiLogsStPunchDateTime = (TextView) convertView.findViewById(R.id.tvMGAMultiLogsStPunchDateTime);
            viewHolder.btnMGAMultiLogsInOutSt = (Button) convertView.findViewById(R.id.btnMGAMultiLogsInOutSt );

            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        MainGateLogs mgl = readerList.get(position);
        int st_reader_id = mgl.getMglStMainGateAccId();
        String st_reader_name = mgl.getMglStMainGateAccName();

        viewHolder.tvMGAMultiLogsStAccName.setText(mgl.getMglStMainGateAccId() +" - "+mgl.getMglStMainGateAccName());
        viewHolder.tvMGAMultiLogsStCampusName.setText(mgl.getMglStCampusId() +" - "+mgl.getMglStCampusName());
        viewHolder.tvMGAMultiLogsStPunchDateTime.setText(mgl.getMglStPunchTime());
        String[] readerName = st_reader_name.split(" - ");
        if (readerName[1].equals("IN")) {
            viewHolder.btnMGAMultiLogsInOutSt.setText("Punch - IN");
            viewHolder.btnMGAMultiLogsInOutSt.setBackground(mContext.getResources().getDrawable(R.color.color_light_green_ep));
        } else {
            viewHolder.btnMGAMultiLogsInOutSt.setText("Punch - OUT");
            viewHolder.btnMGAMultiLogsInOutSt.setBackground(mContext.getResources().getDrawable(R.color.color_red_fr));
        }

        return convertView;
    }

    public class ViewHolder {
        TextView tvMGAMultiLogsStAccName, tvMGAMultiLogsStCampusName, tvMGAMultiLogsStPunchDateTime;
        Button btnMGAMultiLogsInOutSt;
    }
}
