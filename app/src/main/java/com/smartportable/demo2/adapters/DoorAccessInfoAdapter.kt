package com.smartportable.demo2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.smartportable.demo2.R
import com.smartportable.demo2.models.DoorAccessInfoModel
import kotlinx.android.synthetic.main.row_door_name.view.*

class DoorAccessInfoAdapter(private val mContext: Context, mArrayList: java.util.ArrayList<DoorAccessInfoModel>) :RecyclerView.Adapter<DoorAccessInfoAdapter.ViewHolder>() {
    private var mDoorAccessList = ArrayList<DoorAccessInfoModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_door_name, parent, false)

        return ViewHolder(view)
    }

    fun setList (doorAccessList: ArrayList<DoorAccessInfoModel>) {
        mDoorAccessList.clear()
        mDoorAccessList.addAll(doorAccessList)
        notifyDataSetChanged()
    }
    override fun getItemCount(): Int {
        return mDoorAccessList.size;
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        /* holder.mTvCatId.text = mArrayList[position].title
         holder.mTvCatDesc.text = mArrayList[position].description
         holder.mTvSubcatId.text = mArrayList[position].title
         holder.mTvSubcatDesc.text = mArrayList[position].description*/

        holder.mTvDoorName.text = ""+mDoorAccessList.get(position).doorId + " - " + mDoorAccessList.get(position).doorName
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val mTvDoorName = view.mTvDoorName!!
    }
}