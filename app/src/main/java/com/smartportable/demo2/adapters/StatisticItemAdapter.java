package com.smartportable.demo2.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smartportable.demo2.R;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.helper.Statistics;

import java.util.ArrayList;

/**
 * Created by Ananth on 2/1/2017.
 */

public class StatisticItemAdapter extends BaseAdapter {
    Context mContext;
    LayoutInflater inflater;
    ArrayList<Statistics> statisticList;
    boolean enable = false;
    int indexOfSelectedReader = -1;

    public StatisticItemAdapter(Context mContext, ArrayList<Statistics> statisticList) {
        this.mContext = mContext;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.statisticList = statisticList;
        this.indexOfSelectedReader = indexOfSelectedReader;
    }

    public void enableCheckBox(boolean enable) {
        this.enable = enable;
    }

    @Override
    public int getCount() {
        return statisticList.size();
    }

    @Override
    public Object getItem(int position) {
        return statisticList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.activity_statistic_list_item_row, parent, false);
            viewHolder = new ViewHolder();

            viewHolder.tvMusterName = (TextView) convertView.findViewById(R.id.tvMusterName);
            viewHolder.tvMusterCount = (TextView) convertView.findViewById(R.id.tvMusterCount);
            viewHolder.rlStatisticItem = (RelativeLayout) convertView.findViewById(R.id.rlStatisticItem);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        int musterId = statisticList.get(position).getMusterId();
        String musterName = statisticList.get(position).getMusterName();
        int musterCount = statisticList.get(position).getMusterCount();
        //viewHolder.rbReaderName.setText(readerName);
        viewHolder.tvMusterName.setText(musterName);//+" TestTestTestTestTestTestTestTestTestTestTestTestTestTestTest");
        viewHolder.tvMusterCount.setText("("+ URLCollections.getNumberFormat().format(musterCount)+")");

        if (musterId == 1) { // Rrollcall
            /*viewHolder.tvMusterName.setTextColor(mContext.getResources().getColor(R.color.color_red_fr));
            if (indexOfSelectedReader == position) {
                viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_red_check));
            } else {
                viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_red_uncheck));
            }*/
            convertView.setBackgroundColor(mContext.getResources().getColor(R.color.color_red_fr));
        } else if (musterId == 2) { // Headcount
            /*viewHolder.tvMusterName.setTextColor(mContext.getResources().getColor(R.color.color_blue_hc));
            if (indexOfSelectedReader == position) {
                viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_blue_check));
            } else {
                viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_blue_uncheck));
            }*/
            convertView.setBackgroundColor(mContext.getResources().getColor(R.color.color_blue_hc));
        } else if (musterId == 3) { // Assembly Points
            /*viewHolder.tvMusterName.setTextColor(mContext.getResources().getColor(R.color.color_green_ap));
            if (indexOfSelectedReader == position) {
                viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_green_check));
            } else {
                viewHolder.ivCheckedReader.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.ic_rb_green_uncheck));
            }*/
            convertView.setBackgroundColor(mContext.getResources().getColor(R.color.color_green_ap));
        } else if (musterId == 4) { // Exit Point
            convertView.setBackgroundColor(mContext.getResources().getColor(R.color.color_light_green_ep));
        }
        return convertView;
    }

    public class ViewHolder {
        TextView tvMusterName, tvMusterCount;
        RelativeLayout rlStatisticItem;
    }
}
