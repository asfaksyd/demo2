package com.smartportable.demo2.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smartportable.demo2.R;
import com.smartportable.demo2.helper.SyncType;

import java.util.ArrayList;

/**
 * Created by Ananth on 3/23/2017.
 */

public class SyncGridAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<SyncType> list = new ArrayList<SyncType>();
    LayoutInflater inflater;
    public SyncGridAdapter(Context mContext, ArrayList<SyncType> list) {
        this.mContext = mContext;
        this.list = list;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = inflater.inflate(R.layout.row_sync_list_item, parent, false);
        ViewHolder mHolder = null;
        if (mHolder == null) {
            mHolder = new ViewHolder();

            mHolder.ivSyncTypeImage = (ImageView) v.findViewById(R.id.ivSyncTypeImage);
            mHolder.tvSyncTypeName = (TextView) v.findViewById(R.id.tvSyncTypeName);
            mHolder.rlSyncTypeItem = (LinearLayout) v.findViewById(R.id.rlSyncTypeItem);
        }

        mHolder.ivSyncTypeImage.setImageResource(list.get(position).getSyncTypeImage());
        mHolder.ivSyncTypeImage.setBackgroundColor(0x0);
        mHolder.tvSyncTypeName.setText(list.get(position).getSyncTypeName());
        //mHolder.rlSyncTypeItem.setTag(position);
        return v;
    }

    @Override
    public SyncType getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public class ViewHolder {
        ImageView ivSyncTypeImage;
        TextView tvSyncTypeName;
        LinearLayout rlSyncTypeItem;
    }

}
