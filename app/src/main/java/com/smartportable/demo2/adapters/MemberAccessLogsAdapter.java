package com.smartportable.demo2.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.smartportable.demo2.R;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.fragments.MemAccLogDetailFragment;
import com.smartportable.demo2.helper.MemberAccessLogs;
import com.smartportable.demo2.helper.RecyclerListSelectorModel;
import com.smartportable.demo2.helper.SaveImages;
import com.smartportable.demo2.helper.Utility;
import com.smartportable.demo2.interfaces.OnLoadMoreListener;
import com.smartportable.demo2.interfaces.SelectedListListener;

import java.util.ArrayList;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;


public class MemberAccessLogsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    protected boolean showDynamicTags = false;
    ArrayList<MemberAccessLogs> list_access_logs = new ArrayList<MemberAccessLogs>();
    ArrayList<RecyclerListSelectorModel> list_boolean = new ArrayList<RecyclerListSelectorModel>();
    Context mContext;
    byte[] data = null;
    SaveImages saveImage;
    boolean enable = false, isLoading = false;
    String forMemAccess = "";

    public OnLoadMoreListener mOnLoadMoreListener;
    public SelectedListListener listChangeListener;
    DatabaseHandler db;
    ProgressBar pbReaders;
    TextView tv_dialog_emp_reader_id;
    ListView lvReadersList;
    FragmentManager fm;

    private int lastVisibleItem, totalItemCount, visibleThreshold =5;

    public void setListener (SelectedListListener listChangeListener) {
        this.listChangeListener = listChangeListener;
    }

    public MemberAccessLogsAdapter(Context mContext, ArrayList<MemberAccessLogs> list_access_logs, RecyclerView mRecyclerView, ArrayList<RecyclerListSelectorModel> list_boolean, FragmentManager fm) {
        this.mContext = mContext;
        this.list_access_logs = list_access_logs;
        this.list_boolean = list_boolean;
        saveImage = SaveImages.getInstance();
        db = new DatabaseHandler(mContext);
        this.fm = fm;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        if (linearLayoutManager != null) {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }
            });
        }
    }

    private View.OnClickListener onTagTvClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view instanceof  TextView && mOnTagClickedListener != null) {
                TextView tv = (TextView) view;
                final String tag = tv.getText().toString();
                mOnTagClickedListener.onTagClicked(tag);
            }
        }
    };
    private OnTagClickedListener mOnTagClickedListener = null;

    public MemberAccessLogsAdapter(ArrayList<MemberAccessLogs> list_access_logs) {
        this.list_access_logs = list_access_logs;
    }

    @Override
    public int getItemViewType(int position) {
        return list_access_logs.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.employee_row_item, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View viewLoading = LayoutInflater.from(mContext).inflate(R.layout.footer_layout, parent, false);
            return new LoadingViewHolder(viewLoading);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, final int position) {
        if (mHolder instanceof UserViewHolder) {
            final RecyclerListSelectorModel model = list_boolean.get(position);
            final UserViewHolder userHolder = (UserViewHolder) mHolder;
            MemberAccessLogs item = list_access_logs.get(position);
            int emp_id = item.getMemAccLogsEmpId();
            String stu_id = item.getMemAccLogsStuId();
            String emp_name = item.getMemAccLogsEmpName();
            String emp_photo = item.getMemAccLogsEmpPhoto();
            String card_no = item.getMemAccLogsCardNo();
            String punch_date = item.getMemAccLogsPunchDate();
            int reader_id = item.getMemAccLogsReaderId();
            int session_id = item.getMemAccLogsSessionId();
            int access_grant = item.getMemAccLogsAccessGrant();
            int is_sync = item.getMemAccLogsIsSync();

            System.out.println("Image Photo " + emp_photo + " PunchDate " + punch_date);

            userHolder.tvEmpId.setTextColor(mContext.getResources().getColor(android.R.color.black));
            userHolder.tvEmpName.setTextColor(mContext.getResources().getColor(android.R.color.black));
            userHolder.tvEmpCardNo.setTextColor(mContext.getResources().getColor(android.R.color.black));
            userHolder.tvEmpLocId.setTextColor(mContext.getResources().getColor(android.R.color.black));

            userHolder.tvEmpId.setVisibility(View.GONE);

            if (enable) {
                userHolder.ivCheckBox.setVisibility(View.VISIBLE);
            } else {
                userHolder.ivCheckBox.setVisibility(View.GONE);
            }
            /*if (emp_id >= 0)
                userHolder.tvEmpId.setText("" + emp_id);
            if (emp_name != null && emp_name.length() > 0)
                userHolder.tvEmpName.setText("" + emp_name);*/
            if (stu_id.length() >= 0 || emp_name != null && emp_name.length() > 0) {
                userHolder.tvEmpName.setText(stu_id + " - " + emp_name);
            } else {
                userHolder.tvEmpName.setText(mContext.getResources().getString(R.string.unknown_str));
            }
            if (card_no != null && card_no.length() > 0) {
                userHolder.tvEmpCardNo.setVisibility(View.VISIBLE);
                userHolder.tvEmpCardNo.setText("" + card_no);
            } else {
                userHolder.tvEmpCardNo.setVisibility(View.GONE);
                userHolder.tvEmpCardNo.setText("");
            }
            if (reader_id >= 0)
                userHolder.tvEmpLocId.setText("" + reader_id);

            if (punch_date != null) {
                if (punch_date.length() > 0) {
                    userHolder.tvEmpPunchDateTime.setVisibility(View.VISIBLE);
                    userHolder.tvEmpPunchDateTime.setText("" + punch_date);
                }
            } else {
                userHolder.tvEmpPunchDateTime.setVisibility(View.GONE);
            }

            if (forMemAccess.equals(URLCollections.MEM_ACC_TYPE_ACCESSED) || forMemAccess.equals(URLCollections.MEM_ACC_TYPE_DENIED)) {
                userHolder.statusColor.setVisibility(View.VISIBLE);
                if (access_grant == 0) {
                    userHolder.statusColor.setBackgroundColor(mContext.getResources().getColor(R.color.color_green));
                    ((CircleImageView) userHolder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(R.color.color_green));
                } else if (access_grant == 1) {
                    userHolder.statusColor.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_red_dark));
                    ((CircleImageView) userHolder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(android.R.color.holo_red_dark));
                } else if (access_grant == 2) {
                    userHolder.statusColor.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_red_dark));
                    ((CircleImageView) userHolder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(android.R.color.holo_red_dark));
                }
            } else if (forMemAccess.equals(URLCollections.MEM_ACC_TYPE_PENDING)) {
                userHolder.statusColor.setVisibility(View.VISIBLE);
                userHolder.statusColor.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
                ((CircleImageView) userHolder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(android.R.color.darker_gray));
            } else {
                userHolder.statusColor.setVisibility(View.VISIBLE);
                userHolder.statusColor.setBackgroundColor(mContext.getResources().getColor(R.color.color_cyan));
                ((CircleImageView) userHolder.ivEmpPhoto).setBorderColor(mContext.getResources().getColor(R.color.color_cyan));
            }
            // to permanent hide status bar
            userHolder.statusColor.setVisibility(View.GONE);

            //userHolder.itemView.setBackgroundColor(model.isSelected() ? Color.CYAN : Color.WHITE);
            if (model.isSelected()) {
                userHolder.ivCheckBox.setChecked(true);
            } else{
                userHolder.ivCheckBox.setChecked(false);
            }
            userHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (enable) {
                        model.setSelected(!model.isSelected());
                        //userHolder.itemView.setBackgroundColor(model.isSelected() ? Color.CYAN : Color.WHITE);
                        if (model.isSelected()) {
                            userHolder.ivCheckBox.setChecked(true);
                        } else {
                            userHolder.ivCheckBox.setChecked(false);
                        }
                        list_boolean.set(position, model);

                        listChangeListener.onListChanged(list_boolean);
                    } else {
                        imageClickListener.onClick(userHolder.listItem);
                    }
                }
            });

            // image click event
            //userHolder.ivEmpPhoto.setOnClickListener(imageClickListener);
            userHolder.ivEmpPhoto.setTag(R.id.ivEmpPhoto, position);
            userHolder.listItem.setTag(position);

            Glide
                    .with(mContext)
                    .asBitmap()
                    .load(emp_photo)
                    .centerCrop()
                    .dontAnimate()
                    .placeholder(R.mipmap.ic_profile_pic)
                    .into(new BitmapImageViewTarget(userHolder.ivEmpPhoto) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            userHolder.ivEmpPhoto.setImageDrawable(circularBitmapDrawable);
                        }
                    });

        } else if (mHolder instanceof LoadingViewHolder) {
            LoadingViewHolder loadHolder = (LoadingViewHolder) mHolder;

        }
    }

    @Override
    public int getItemCount() {
        return list_access_logs.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void isForMemberAccess(String forMemAccess) {
        this.forMemAccess = forMemAccess;
    }

    public void setOnTagClickedListener(OnTagClickedListener listener) {
        this.mOnTagClickedListener = listener;
    }

    public interface OnTagClickedListener {
        void onTagClicked(String tag);
    }

    public void updateEmployeeList (ArrayList<MemberAccessLogs> list_access_logs) {
        //this.list_employee.clear();
        this.list_access_logs = list_access_logs;
    }

    public void updateBooleanLList (ArrayList<RecyclerListSelectorModel> list_boolean) {
        this.list_boolean = list_boolean;
    }

    public void enableCheckBox(boolean enable) {
        this.enable = enable;
    }

    public View.OnClickListener imageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //final String empPhoto = (String) v.getTag();
            final int position = (Integer) v.getTag();//R.id.ivEmpPhoto);
            MemberAccessLogs item = list_access_logs.get(position);

            /*int emp_id = item.getMemAccLogsEmpId();
            String emp_name = item.getMemAccLogsEmpName();
            String empPhoto = item.getMemAccLogsEmpPhoto();
            String card_no = item.getMemAccLogsCardNo();
            String punch_date = item.getMemAccLogsPunchDate();
            int readerId = item.getMemAccLogsReaderId();
            int sessionId = item.getMemAccLogsSessionId();
            System.out.println("Image Photo " + empPhoto);*/

            //if (forMemAccess.equals(URLCollections.MEM_ACC_TYPE_ACCESSED) || forMemAccess.equals(URLCollections.MEM_ACC_TYPE_DENIED)) {
            showAccessGrantedDialog(item);
            //}

            //final String empPhoto = (String) v.getTag(R.id.ivEmpPhoto);
            //Log.d("PunchAdapter", "EmpPhoto: "+empPhoto);
            /*if (empPhoto != null && empPhoto.length() !=0 || forMemAccess) {
                Log.d(MainActivity.TAG, "readersAdapter: empPhoto: "+empPhoto);
                AlertDialog alertDialog;
                final AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);

                LayoutInflater inflater = LayoutInflater.from(mContext);
                View dialogLayout = inflater.inflate(R.layout.dialog_full_image, null);
                dialog.setView(dialogLayout);//, 20, 20, 20, 20);
                //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                ImageView image = (ImageView) dialogLayout.findViewById(R.id.ivFullScreen);
                TextView tv_dialog_emp_id = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_id);
                TextView tv_dialog_emp_name = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_name);
                TextView tv_dialog_emp_card = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_card);
                TextView tv_dialog_emp_loc_id = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_loc_id);
                tv_dialog_emp_reader_id = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_reader_id);
                TextView tv_dialog_emp_datetime = (TextView) dialogLayout.findViewById(R.id.tv_dialog_emp_datetime);

                lvReadersList = (ListView) dialogLayout.findViewById(R.id.lvReadersList);

                pbReaders = (ProgressBar) dialogLayout.findViewById(R.id.pbReaders);

                tv_dialog_emp_name.setVisibility(View.GONE);
                tv_dialog_emp_loc_id.setVisibility(View.GONE);
                tv_dialog_emp_reader_id.setVisibility(View.GONE);
                pbReaders.setVisibility(View.GONE);
                if (forMemAccess) {
                    //pbReaders.setVisibility(View.VISIBLE);
                    new getReadersList().execute(""+emp_id, card_no);
                }
                tv_dialog_emp_datetime.setVisibility(View.GONE);

                if (emp_id >= 0 && emp_name != null && emp_name.length() > 0)
                    tv_dialog_emp_id.setText(emp_id + " - " + emp_name);
                if (emp_name != null && emp_name.length() > 0)
                    tv_dialog_emp_name.setText("" + emp_name);
                if (card_no != null && card_no.length() > 0) {
                    tv_dialog_emp_card.setVisibility(View.VISIBLE);
                    tv_dialog_emp_card.setText("" + card_no);
                } else {
                    tv_dialog_emp_card.setVisibility(View.GONE);
                    tv_dialog_emp_card.setText("");
                }
                if (sessionId >= 0)
                    tv_dialog_emp_loc_id.setText("" + sessionId);

                *//*if (readerId >= 0) {
                    tv_dialog_emp_reader_id.setText(""+readerId);
                }*//*
                *//*if (punch_date != null && punch_date.length() > 0) {
                    tv_dialog_emp_loc_id.setText(""+punch_date);
                }*//*

                dialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog closed
                    }
                });

                Glide
                        .with(mContext)
                        .load(empPhoto)
                        //.placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_profile_pic)
                        .crossFade()
                        .into(image);

                *//*alertDialog = dialog.create();
                alertDialog.getWindow().getAttributes().windowAnimations = R.style.CustomAnimations_grow;
                alertDialog*//*
                dialog.show();

            }*/
        }
    };

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmpId, tvEmpName, tvEmpCardNo, tvEmpLocId, tvEmpPunchDateTime;
        ImageView ivEmpPhoto;
        CheckBox ivCheckBox;
        View listItem, statusColor;


        public UserViewHolder(View itemview) {
            //super(inflater.inflate(R.layout.employee_row_item, parent, false));
            super(itemview);
            tvEmpId = (TextView) itemView.findViewById(R.id.tvEmpId);
            tvEmpName = (TextView) itemView.findViewById(R.id.tvEmpName);
            ivEmpPhoto = (ImageView) itemView.findViewById(R.id.ivEmpPhoto);
            tvEmpCardNo = (TextView) itemView.findViewById(R.id.tvEmpCardNo);
            tvEmpPunchDateTime = (TextView) itemView.findViewById(R.id.tvEmpPunchDateTime);
            tvEmpLocId = (TextView) itemView.findViewById(R.id.tvEmpLocId);
            ivCheckBox = (CheckBox) itemView.findViewById(R.id.ivCheckBox);
            statusColor = (View) itemView.findViewById(R.id.statusColor);
            listItem = (View) itemView;

        }
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar pbLoadMore;
        public TextView tvPBTitle;
        public LoadingViewHolder(View itemview) {
            super(itemview);
            tvPBTitle = (TextView) itemView.findViewById(R.id.tvPBTitle);
            pbLoadMore = (ProgressBar) itemView.findViewById(R.id.pbLoadMore);
        }
    }

    private class getReadersList extends AsyncTask<String , Void, ArrayList<String>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbReaders.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<String> doInBackground(String... params) {
            ArrayList<String> list_readers = db.getAllReadersOfMemberAccess(Integer.parseInt(params[0]), params[1]);
            return list_readers;
        }

        @Override
        protected void onPostExecute(ArrayList<String> list) {
            super.onPostExecute(list);
            pbReaders.setVisibility(View.GONE);
            tv_dialog_emp_reader_id.setVisibility(View.GONE);
            if (list != null) {
                if (list.size() > 0) {
                    /*for (int i=0; i<list.size();i++) {
                        String reader_ids = tv_dialog_emp_reader_id.getText().toString();
                        if (i==0) {
                            tv_dialog_emp_reader_id.setText(list.get(i));
                        }else {
                            tv_dialog_emp_reader_id.setText(reader_ids+" , "+list.get(i));
                        }
                    }*/

                    ArrayAdapter<String> adapter =  new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, list);
                    lvReadersList.setAdapter(adapter);
                    Utility.setListViewHeightBasedOnChildren(lvReadersList);
                }
            }

        }
    }

    public BottomSheetDialogFragment bottomSheetDialogFragment = null;
    public void showAccessGrantedDialog (MemberAccessLogs memacclogs) {

        bottomSheetDialogFragment = new MemAccLogDetailFragment();
        ((MemAccLogDetailFragment)bottomSheetDialogFragment).setMemAccData(memacclogs);
        ((MemAccLogDetailFragment)bottomSheetDialogFragment).isForMemberAccess(forMemAccess);
        ((MemAccLogDetailFragment)bottomSheetDialogFragment).isFromScan(false);
        bottomSheetDialogFragment.show(fm, bottomSheetDialogFragment.getTag());

        /*final AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View dialogLayout = inflater.inflate(R.layout.dialog_access_granted, null);
        dialog.setView(dialogLayout);//, 20, 20, 20, 20);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ImageView ivAGEmpPhoto = (ImageView) dialogLayout.findViewById(R.id.ivAGEmpPhoto);
        FloatingActionButton fabAG = (FloatingActionButton) dialogLayout.findViewById(R.id.fabAG);
        TextView tvAGLabel = (TextView) dialogLayout.findViewById(R.id.tvAGLabel);
        TextView tvAGEmpName = (TextView) dialogLayout.findViewById(R.id.tvAGEmpName);
        TextView tvAGEmpCardNo = (TextView) dialogLayout.findViewById(R.id.tvAGEmpCardNo);
        TextView tvAGPunchTime = (TextView) dialogLayout.findViewById(R.id.tvAGPunchTime);
        TextView tvAGReaderName = (TextView) dialogLayout.findViewById(R.id.tvAGReaderName);
        TextView tvAGSessionName = (TextView) dialogLayout.findViewById(R.id.tvAGSessionName);

        if (forMemAccess.equals(URLCollections.MEM_ACC_TYPE_ALLOWED)) {
            fabAG.setVisibility(View.GONE);
            tvAGLabel.setVisibility(View.GONE);
            ivAGEmpPhoto.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
            tvAGPunchTime.setVisibility(View.GONE);
            tvAGReaderName.setVisibility(View.GONE);
            tvAGSessionName.setVisibility(View.GONE);
        } else if (forMemAccess.equals(URLCollections.MEM_ACC_TYPE_ACCESSED) || forMemAccess.equals(URLCollections.MEM_ACC_TYPE_DENIED)) {
            fabAG.setVisibility(View.VISIBLE);
            tvAGLabel.setVisibility(View.VISIBLE);
            tvAGPunchTime.setVisibility(View.VISIBLE);
            tvAGReaderName.setVisibility(View.VISIBLE);
            tvAGSessionName.setVisibility(View.VISIBLE);
            int access_grant = memacclogs.getMemAccLogsAccessGrant();
            if (access_grant == 0) {
                fabAG.setBackgroundTintList(ColorStateList.valueOf(mContext.getResources().getColor(android.R.color.holo_green_dark)));
                fabAG.setImageResource(R.mipmap.ic_action_done);
                tvAGLabel.setText(Html.fromHtml(mContext.getResources().getString(R.string.access_grant_msg)));
                tvAGLabel.setTextColor(mContext.getResources().getColor(android.R.color.holo_green_dark));
                ivAGEmpPhoto.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_green_light));
            } else if (access_grant == 1) {
                fabAG.setBackgroundTintList(ColorStateList.valueOf(mContext.getResources().getColor(android.R.color.holo_red_dark)));
                fabAG.setImageResource(R.mipmap.ic_close_app);
                tvAGLabel.setText(Html.fromHtml(mContext.getResources().getString(R.string.access_deny_msg) + mContext.getResources().getString(R.string.access_deny_no_door_access)));
                tvAGLabel.setTextColor(mContext.getResources().getColor(android.R.color.holo_red_dark));
                ivAGEmpPhoto.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_red_light));
            } else if (access_grant == 2) {
                fabAG.setBackgroundTintList(ColorStateList.valueOf(mContext.getResources().getColor(android.R.color.holo_red_dark)));
                fabAG.setImageResource(R.mipmap.ic_close_app);
                tvAGLabel.setText(Html.fromHtml(mContext.getResources().getString(R.string.access_deny_msg) + mContext.getResources().getString(R.string.access_deny_duplicate_access)));
                tvAGLabel.setTextColor(mContext.getResources().getColor(android.R.color.holo_red_dark));
                ivAGEmpPhoto.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_red_light));
            }
        } else if (forMemAccess.equals(URLCollections.MEM_ACC_TYPE_PENDING)) {
            fabAG.setVisibility(View.GONE);
            tvAGLabel.setVisibility(View.GONE);
            tvAGPunchTime.setVisibility(View.GONE);
            tvAGReaderName.setVisibility(View.GONE);
            tvAGSessionName.setVisibility(View.GONE);
            ivAGEmpPhoto.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
        }

        int emp_id = memacclogs.getMemAccLogsEmpId();
        String emp_photo ="";
        if (emp_id != -1) {
            DatabaseHandler db = new DatabaseHandler(mContext);
            emp_photo = db.getImagePathFromEmpId(mContext, emp_id);
            if (db.isEmployeeAlreadyExists(db.getWritableDatabase(), emp_id)) {
                Employee emp = db.getEmployeeFromEmpId(emp_id);
                if (emp != null) {
                    tvAGEmpName.setText(emp.getSLN_Employee() + " - " + emp.getEmployeeName());
                } else {
                    tvAGEmpName.setText(mContext.getResources().getString(R.string.unknown_str));
                }
            }
        } else {
            tvAGEmpName.setText(mContext.getResources().getString(R.string.unknown_str));
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        String reader_name = prefs.getString(PreferencesManager.KEY_SET_READER_NAME, "");
        String session_name = prefs.getString(PreferencesManager.KEY_SET_SESSION_NAME, "");
        tvAGReaderName.setText(mContext.getResources().getString(R.string.access_point_label, reader_name));
        tvAGSessionName.setText(mContext.getResources().getString(R.string.session_label, session_name));

        String card_no = memacclogs.getMemAccLogsCardNo();
        if (card_no.length() > 0) {
            tvAGEmpCardNo.setText(mContext.getResources().getString(R.string.card_number_label, card_no));
        }

        String punch_time = memacclogs.getMemAccLogsPunchDate();
        if (punch_time != null) {
            if (punch_time.length() > 0) {
                tvAGPunchTime.setText(mContext.getResources().getString(R.string.punch_time_label, punch_time));
            }
        }

        Glide
                .with(mContext)
                .load(emp_photo)
                //.placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_profile_pic)
                .crossFade()
                .into(ivAGEmpPhoto);

        dialog.setNegativeButton(mContext.getResources().getString(R.string.ok_label), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
            }
        });

        dialog.show();*/
    }
}
