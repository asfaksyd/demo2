package com.smartportable.demo2.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartportable.demo2.R;
import com.smartportable.demo2.AddSessionActivity;
import com.smartportable.demo2.SessionsActivity;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.helper.RecyclerListSelectorModel;
import com.smartportable.demo2.helper.Sessions;
import com.smartportable.demo2.interfaces.SelectedListListener;
import com.smartportable.demo2.interfaces.SessionSelected;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Ananth on 3/28/2017.
 */

public class SessionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    ArrayList<Sessions> list;
    ArrayList<RecyclerListSelectorModel> list_boolean = new ArrayList<RecyclerListSelectorModel>();
    boolean enable = false;
    SessionSelected sessionSelected;
    public SelectedListListener listChangeListener;


    public void enableCheckBox(boolean enable) {
        this.enable = enable;
    }

    public void updateListBoolean (ArrayList<RecyclerListSelectorModel> list_boolean) {
        this.list_boolean = list_boolean;
    }

    public void updateSessionList (ArrayList<Sessions> list) {
        this.list = list;
    }
    public SessionsAdapter (Context mContext, ArrayList<Sessions> list, ArrayList<RecyclerListSelectorModel> list_boolean) {
        this.list = list;
        this.mContext = mContext;
        this.list_boolean = list_boolean;
        sessionSelected = (SessionSelected) mContext;
        listChangeListener = (SelectedListListener) mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        View view = LayoutInflater.from(mContext).inflate(R.layout.session_row_item, parent, false);
        return new SessionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, final int position) {
        if (mHolder instanceof SessionViewHolder) {
            final SessionViewHolder sessionHolder = (SessionViewHolder) mHolder;
            final RecyclerListSelectorModel model = list_boolean.get(position);
            Sessions item = list.get(position);
            String sessionName = item.getSessionName();
            sessionHolder.tvSessionName.setText(sessionName);
            sessionHolder.ivEditSession.setTag(R.id.ivEditSession, position);
            sessionHolder.ivEditSession.setOnClickListener(mOnEditSessionClickListener);

            if (enable) {
                sessionHolder.viewItem.setOnLongClickListener(null);
                sessionHolder.cbSelectedSession.setVisibility(View.VISIBLE);
                sessionHolder.ivEditSession.setVisibility(View.GONE);
            } else {
                sessionHolder.viewItem.setOnLongClickListener(mItemLongClickListener);
                sessionHolder.cbSelectedSession.setVisibility(View.GONE);
                sessionHolder.ivEditSession.setVisibility(View.VISIBLE);
            }

            if (model.isSelected()) {
                sessionHolder.cbSelectedSession.setChecked(true);
            } else{
                sessionHolder.cbSelectedSession.setChecked(false);
            }

            //sessionHolder.viewItem.setOnLongClickListener(mItemLongClickListener);
            sessionHolder.viewItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (enable) {
                        model.setSelected(!model.isSelected());
                        //userHolder.itemView.setBackgroundColor(model.isSelected() ? Color.CYAN : Color.WHITE);
                        if (model.isSelected()) {
                            sessionHolder.cbSelectedSession.setChecked(true);
                        } else {
                            sessionHolder.cbSelectedSession.setChecked(false);
                        }
                        list_boolean.set(position, model);

                        listChangeListener.onListChanged(list_boolean);
                    }
                }
            });

            sessionHolder.cbSelectedSession.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (enable) {
                        model.setSelected(!model.isSelected());
                        //userHolder.itemView.setBackgroundColor(model.isSelected() ? Color.CYAN : Color.WHITE);
                        if (model.isSelected()) {
                            sessionHolder.cbSelectedSession.setChecked(true);
                        } else {
                            sessionHolder.cbSelectedSession.setChecked(false);
                        }
                        list_boolean.set(position, model);

                        listChangeListener.onListChanged(list_boolean);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class SessionViewHolder extends RecyclerView.ViewHolder {
        TextView tvSessionName;
        ImageView ivEditSession;
        CheckBox cbSelectedSession;
        View viewItem;

        public SessionViewHolder(View itemview) {
            //super(inflater.inflate(R.layout.employee_row_item, parent, false));
            super(itemview);
            viewItem = itemview;
            tvSessionName = (TextView) itemView.findViewById(R.id.tvRowSessionName);
            ivEditSession = (ImageView) itemView.findViewById(R.id.ivEditSession);
            cbSelectedSession = (CheckBox) itemView.findViewById(R.id.cbSelectedSession);
        }
    }

    public View.OnClickListener mOnEditSessionClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int pos = (Integer)v.getTag(R.id.ivEditSession);
            if (pos != -1) {
                int sessionId = list.get(pos).getSessionId();
                Intent i = new Intent(mContext, AddSessionActivity.class);
                i.putExtra(SessionsActivity.SESSION_POSITION, sessionId);
                ((Activity) mContext).startActivityForResult(i, SessionsActivity.ADD_SESSION);

                if (URLCollections.whichDirection(mContext) == URLCollections.DEV_DIR_RTL) {
                    ((Activity) mContext).overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right);
                } else {
                    ((Activity) mContext).overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                }
            }
        }
    };

    public View.OnLongClickListener mItemLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {

            enableCheckBox(true);
            notifyDataSetChanged();
            sessionSelected.onSessionSelected();
            return true;
        }
    };
}
