package com.smartportable.demo2.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.smartportable.demo2.R;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.RecyclerListSelectorModel;
import com.smartportable.demo2.helper.SaveImages;
import com.smartportable.demo2.helper.Utility;
import com.smartportable.demo2.interfaces.OnLoadMoreListener;
import com.smartportable.demo2.interfaces.SelectedListListener;
import com.smartportable.demo2.models.MainGateLogs;

import java.util.ArrayList;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import uk.co.senab.photoview.PhotoViewAttacher;


public class MGAMultipleLogsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    protected boolean showDynamicTags = false;
    ArrayList<MainGateLogs> list_access_logs = new ArrayList<MainGateLogs>();
    ArrayList<RecyclerListSelectorModel> list_boolean = new ArrayList<RecyclerListSelectorModel>();
    Context mContext;
    Activity mActivity;
    byte[] data = null;
    SaveImages saveImage;
    boolean enable = false, isLoading = false;
    String forMemAccess = "";

    public OnLoadMoreListener mOnLoadMoreListener;
    public SelectedListListener listChangeListener;
    DatabaseHandler db;
    ProgressBar pbReaders;
    TextView tv_dialog_emp_reader_id;
    ListView lvReadersList;
    FragmentManager fm;

    private int lastVisibleItem, totalItemCount, visibleThreshold =5;

    public void setListener (SelectedListListener listChangeListener) {
        this.listChangeListener = listChangeListener;
    }

    public MGAMultipleLogsAdapter(Activity mActivity, Context mContext, ArrayList<MainGateLogs> list_access_logs, RecyclerView mRecyclerView, FragmentManager fm) {
        this.mActivity = mActivity;
        this.mContext = mContext;
        this.list_access_logs = list_access_logs;
        saveImage = SaveImages.getInstance();
        db = new DatabaseHandler(mContext);
        this.fm = fm;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        if (linearLayoutManager != null) {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }
            });
        }
    }

    private View.OnClickListener onTagTvClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view instanceof  TextView && mOnTagClickedListener != null) {
                TextView tv = (TextView) view;
                final String tag = tv.getText().toString();
                mOnTagClickedListener.onTagClicked(tag);
            }
        }
    };
    private OnTagClickedListener mOnTagClickedListener = null;

    public MGAMultipleLogsAdapter(ArrayList<MainGateLogs> list_access_logs) {
        this.list_access_logs = list_access_logs;
    }

    @Override
    public int getItemViewType(int position) {
        return list_access_logs.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.mga_multi_logs_row_item, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View viewLoading = LayoutInflater.from(mContext).inflate(R.layout.footer_layout, parent, false);
            return new LoadingViewHolder(viewLoading);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder mHolder, final int position) {
        if (mHolder instanceof UserViewHolder) {
            //final RecyclerListSelectorModel model = list_boolean.get(position);
            final UserViewHolder userHolder = (UserViewHolder) mHolder;
            final MainGateLogs item = list_access_logs.get(position);
            String st_punch_date = item.getMglStPunchTime();
            int st_reader_id = item.getMglStMainGateAccId();
            String st_reader_name = item.getMglStMainGateAccName();
            int st_campus_id = item.getMglStCampusId();
            String st_campus_name = item.getMglStCampusName();

            userHolder.tvMGAMultiLogsStAccName.setTextColor(mContext.getResources().getColor(android.R.color.black));
            //userHolder.tvMGAStId.setVisibility(View.GONE);


            if (st_reader_id >= 0) {
                String[] readerName = st_reader_name.split(" - ");
                userHolder.tvMGAMultiLogsStAccName.setText(st_reader_id + " - " +readerName[0]);
                userHolder.tvMGAMultiLogsStCampusName.setText(st_campus_id+" - "+st_campus_name);
                if (readerName[1].equals("IN")) {
                    userHolder.btnMGAMultiLogsInOutSt.setText("Punch - IN");
                    userHolder.btnMGAMultiLogsInOutSt.setBackground(mContext.getResources().getDrawable(R.color.color_light_green_ep));
                } else {
                    userHolder.btnMGAMultiLogsInOutSt.setText("Punch - OUT");
                    userHolder.btnMGAMultiLogsInOutSt.setBackground(mContext.getResources().getDrawable(R.color.color_red_fr));
                }
            }

            if (st_punch_date != null) {
                if (st_punch_date.length() > 0) {
                    userHolder.tvMGAMultiLogsStPunchDateTime.setVisibility(View.VISIBLE);
                    userHolder.tvMGAMultiLogsStPunchDateTime.setText("" + st_punch_date);
                }
            } else {
                userHolder.tvMGAMultiLogsStPunchDateTime.setVisibility(View.GONE);
            }

            userHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                }
            });




        } else if (mHolder instanceof LoadingViewHolder) {
            LoadingViewHolder loadHolder = (LoadingViewHolder) mHolder;

        }
    }

    @Override
    public int getItemCount() {
        return list_access_logs.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void isForMemberAccess(String forMemAccess) {
        this.forMemAccess = forMemAccess;
    }

    public void setOnTagClickedListener(OnTagClickedListener listener) {
        this.mOnTagClickedListener = listener;
    }

    public interface OnTagClickedListener {
        void onTagClicked(String tag);
    }

    public void updateStudentList (ArrayList<MainGateLogs> list_access_logs) {
        //this.list_employee.clear();
        this.list_access_logs = list_access_logs;
    }

    public void updateBooleanLList (ArrayList<RecyclerListSelectorModel> list_boolean) {
        this.list_boolean = list_boolean;
    }

    public void enableCheckBox(boolean enable) {
        this.enable = enable;
    }

    public View.OnClickListener imageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //final String empPhoto = (String) v.getTag();
            final int position = (Integer) v.getTag();//R.id.ivEmpPhoto);
            MainGateLogs item = list_access_logs.get(position);



            //if (forMemAccess.equals(URLCollections.MEM_ACC_TYPE_ACCESSED) || forMemAccess.equals(URLCollections.MEM_ACC_TYPE_DENIED)) {
            showAccessGrantedDialog(item);
            //}
        }
    };

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        TextView tvMGAMultiLogsStAccName, tvMGAMultiLogsStCampusName, tvMGAMultiLogsStPunchDateTime;
        Button btnMGAMultiLogsInOutSt;

        public UserViewHolder(View itemview) {
            //super(inflater.inflate(R.layout.employee_row_item, parent, false));
            super(itemview);
            tvMGAMultiLogsStAccName = (TextView) itemView.findViewById(R.id.tvMGAMultiLogsStAccName);
            tvMGAMultiLogsStCampusName = (TextView) itemView.findViewById(R.id.tvMGAMultiLogsStCapusName);
            tvMGAMultiLogsStPunchDateTime = (TextView) itemView.findViewById(R.id.tvMGAMultiLogsStPunchDateTime);
            btnMGAMultiLogsInOutSt = (Button) itemView.findViewById(R.id.btnMGAMultiLogsInOutSt );
        }
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar pbLoadMore;
        public TextView tvPBTitle;
        public LoadingViewHolder(View itemview) {
            super(itemview);
            tvPBTitle = (TextView) itemView.findViewById(R.id.tvPBTitle);
            pbLoadMore = (ProgressBar) itemView.findViewById(R.id.pbLoadMore);
        }
    }

    private class getReadersList extends AsyncTask<String , Void, ArrayList<String>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbReaders.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<String> doInBackground(String... params) {
            ArrayList<String> list_readers = db.getAllReadersOfMemberAccess(Integer.parseInt(params[0]), params[1]);
            return list_readers;
        }

        @Override
        protected void onPostExecute(ArrayList<String> list) {
            super.onPostExecute(list);
            pbReaders.setVisibility(View.GONE);
            tv_dialog_emp_reader_id.setVisibility(View.GONE);
            if (list != null) {
                if (list.size() > 0) {
                    /*for (int i=0; i<list.size();i++) {
                        String reader_ids = tv_dialog_emp_reader_id.getText().toString();
                        if (i==0) {
                            tv_dialog_emp_reader_id.setText(list.get(i));
                        }else {
                            tv_dialog_emp_reader_id.setText(reader_ids+" , "+list.get(i));
                        }
                    }*/

                    ArrayAdapter<String> adapter =  new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, list);
                    lvReadersList.setAdapter(adapter);
                    Utility.setListViewHeightBasedOnChildren(lvReadersList);
                }
            }

        }
    }

    public BottomSheetDialogFragment bottomSheetDialogFragment = null;
    public void showAccessGrantedDialog (MainGateLogs maingatelogs) {

        /*bottomSheetDialogFragment = new MemAccLogDetailFragment();
        ((MemAccLogDetailFragment)bottomSheetDialogFragment).setMemAccData(maingatelogs);
        ((MemAccLogDetailFragment)bottomSheetDialogFragment).isForMemberAccess(forMemAccess);
        ((MemAccLogDetailFragment)bottomSheetDialogFragment).isFromScan(false);
        bottomSheetDialogFragment.show(fm, bottomSheetDialogFragment.getTag());*/

    }

    private void showDialog (int resId) {
        final Dialog mSplashDialog = new Dialog(mContext, android.R.style.Theme_Material_NoActionBar_Fullscreen);
        //mSplashDialog.requestWindowFeature((int) window.FEATURE_NO_TITLE);
        mSplashDialog.setContentView(R.layout.dialog_profile_photo);
        //Drawable d = mContext.getResources().getDrawable(resId);
        //Bitmap b = ((BitmapDrawable) d ).getBitmap();
        ImageView ivPPFull = ((ImageView)mSplashDialog.findViewById(R.id.ivStPPFull));
        ImageView ivClosePPFull = (ImageView) mSplashDialog.findViewById(R.id.ivClosePPFull);

        ivPPFull.setImageResource(resId);//Bitmap(b);

        PhotoViewAttacher photoViewAttacher = new PhotoViewAttacher(ivPPFull);

        photoViewAttacher.update();

        ivClosePPFull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSplashDialog.cancel();
            }
        });

        mSplashDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mSplashDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mSplashDialog.setCancelable(true);
        mSplashDialog.show();

    }
}
