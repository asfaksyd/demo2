package com.smartportable.demo2;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.smartportable.demo2.adapters.ReaderItemAdapter;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.Boast;
import com.smartportable.demo2.helper.Config;
import com.smartportable.demo2.helper.PreferencesManager;
import com.smartportable.demo2.models.ReaderInfoModel;
import com.smartportable.demo2.models.SessionsInfoModel;
import com.smartportable.demo2.utils.AppUtilsKt;
import com.smartportable.demo2.utils.SharedPref;

import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

/**
 * Created by Ananth on 3/29/2017.
 */

public class SetReader extends AppCompatActivity implements View.OnClickListener {
    Spinner spReader, spSession;
    EditText etSetReaderSessionDate, etSessionStartTime, etSessionEndTime;
    ImageView ivAddSession, ivDialogClose;
    Button btnSetReader;
    ReaderItemAdapter adapter;
    ArrayAdapter sessionAdapter;
    ArrayList<ReaderInfoModel.DataBean> readerList = new ArrayList<ReaderInfoModel.DataBean>();
    ArrayList<SessionsInfoModel.DataBean> sessionList = new ArrayList<SessionsInfoModel.DataBean>();
    long session_date_mili, session_start_time_mili, session_end_time_mili;
    Calendar calSessionDate, calSessionStartTime, calSessionEndTime;

    DatePickerDialog dialogDatePicker;
    TimePickerDialog dialogStartTimePicker, dialogEndTimePicker;
    ReaderInfoModel.DataBean final_reader;
    SessionsInfoModel.DataBean final_session;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_set_reader);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        spReader = (Spinner) findViewById(R.id.spReader);
        spSession = (Spinner) findViewById(R.id.spSession);

        etSetReaderSessionDate = (EditText) findViewById(R.id.etSetReaderSessionDate);
        etSessionStartTime = (EditText) findViewById(R.id.etSessionStartTime);
        etSessionEndTime = (EditText) findViewById(R.id.etSessionEndTime);

        ivDialogClose = (ImageView) findViewById(R.id.ivDialogClose);
        ivAddSession = (ImageView) findViewById(R.id.ivAddSession);
        ivAddSession.setVisibility(View.GONE);
        ivDialogClose.setOnClickListener(this);
        ivAddSession.setOnClickListener(this);
        btnSetReader = (Button) findViewById(R.id.btnSetReader);
        btnSetReader.setOnClickListener(this);

        //TimeZone.setDefault(TimeZone.getTimeZone(AppUtilsKt.getCurrentTimezoneOffset()));

        calSessionDate = Calendar.getInstance();
        calSessionStartTime = Calendar.getInstance();
        calSessionEndTime = Calendar.getInstance();

        spReader.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Position", "item position: " + position);
                ReaderInfoModel.DataBean reader = (ReaderInfoModel.DataBean) adapter.getItem(position);
                final_reader = reader;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spSession.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SessionsInfoModel.DataBean session = sessionList.get(position);
                final_session = session;

                // First check for the share preference value if it is not available the it will set
                // from the session which is selcted from the session
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SetReader.this);
                long sessionDate = prefs.getLong(PreferencesManager.KEY_SET_SESSION_DATE, 0L);
                //Log.d("SessionDate", ""+new SimpleDateFormat("dd/MM/yyyy").format(sessionDate));
               //long sessionStartTime = prefs.getLong(PreferencesManager.KEY_SET_SESSION_START_TIME, 0L);
                //long sessionEndTime = prefs.getLong(PreferencesManager.KEY_SET_SESSION_END_TIME, 0L);
                // For session start date



                SimpleDateFormat sdfSessionDate = new SimpleDateFormat("dd/MM/yyyy");//, Config.DATETIME_FORMAT_LOCAL);
                //Calendar calSessionDate = Calendar.getInstance();
                DateTime calSessionDate = new DateTime();
                sdfSessionDate.setTimeZone(TimeZone.getTimeZone("GMT"));
                /*if (sessionDate > 0) {
                    session_date_mili = sessionDate;
                } else {*/
                    session_date_mili = calSessionDate.getMillis();//session.getSessionDate();
                //}
                calSessionDate.withMillis(session_date_mili);
                String session_date = calSessionDate.toString("dd/MM/yyyy");//sdfSessionDate.format(calSessionDate.getTime());
                etSetReaderSessionDate.setText(session_date);

                SimpleDateFormat sdfSessionDateTime = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                sdfSessionDateTime.setTimeZone(TimeZone.getTimeZone("GMT"));
                // For session start time
                SimpleDateFormat sdfSessionStartTime = new SimpleDateFormat("HH:mm");//, Config.DATETIME_FORMAT_LOCAL);
                sdfSessionStartTime.setTimeZone(TimeZone.getTimeZone("GMT"));
                //sdfSessionStartTime.setTimeZone(TimeZone.getTimeZone("UTC"));
                Calendar calSessionStartTime = Calendar.getInstance();
                //DateTime calSessionStartTime = new DateTime();
                /*if (sessionStartTime > 0) {
                    session_start_time_mili = sessionStartTime;
                } else {*/

                    String start_time = session_date +" "+ session.getSessionStartTime();
                    try {
                        session_start_time_mili = sdfSessionDateTime.parse(start_time).getTime();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                //}
                calSessionStartTime.setTimeInMillis(session_start_time_mili);
                String session_start_time = sdfSessionStartTime.format(calSessionStartTime.getTimeInMillis());
                etSessionStartTime.setText(session_start_time);

                // For the session end time
                SimpleDateFormat sdfSessionEndTime = new SimpleDateFormat("HH:mm");//, Config.DATETIME_FORMAT_LOCAL);
                sdfSessionEndTime.setTimeZone(TimeZone.getTimeZone("GMT"));
                Calendar calSessionEndTime = Calendar.getInstance();


                /*if (sessionEndTime > 0) {
                    session_end_time_mili = sessionEndTime;
                } else {*/
                    String end_time = session_date +" "+ session.getSessionEndTime();
                    try {
                        session_end_time_mili = sdfSessionDateTime.parse(end_time).getTime();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                //}
                calSessionEndTime.setTimeInMillis(session_end_time_mili);
                String session_end_time = sdfSessionEndTime.format(calSessionEndTime.getTimeInMillis());
                etSessionEndTime.setText(session_end_time);



            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        etSetReaderSessionDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*TimeZone.setDefault(null);
                System.setProperty("user.timezone", "");
                TimeZone.setDefault(null);
                calSessionDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));*/
                calSessionDate = Calendar.getInstance();
                //calSessionDate = new DateTime();

                if (session_date_mili > 0.0)
                    calSessionDate.setTimeInMillis(session_date_mili);

                dialogDatePicker = new DatePickerDialog(SetReader.this, datePicker, calSessionDate
                        .get(Calendar.YEAR), calSessionDate.get(Calendar.MONTH),
                        calSessionDate.get(Calendar.DAY_OF_MONTH));
                if (session_date_mili == 0.0)
                    dialogDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());

                dialogDatePicker.show();
            }
        });
        etSessionStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*TimeZone.setDefault(null);
                System.setProperty("user.timezone", "");
                TimeZone.setDefault(null);*/
                calSessionStartTime = Calendar.getInstance();
                //calSessionStartTime = new DateTime();

                if (session_start_time_mili > 0.0 ) {
                    calSessionStartTime.setTimeInMillis(session_start_time_mili);
                }
                dialogStartTimePicker = new TimePickerDialog(SetReader.this, startTimePicker, calSessionStartTime
                        .get(Calendar.HOUR_OF_DAY), calSessionStartTime.get(Calendar.MINUTE), true);
                dialogStartTimePicker.show();
            }
        });

        etSessionEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*TimeZone.setDefault(null);
                System.setProperty("user.timezone", "");
                TimeZone.setDefault(null);*/
                calSessionEndTime = Calendar.getInstance();

                if (session_end_time_mili > 0.0 ) {
                    calSessionEndTime.setTimeInMillis(session_end_time_mili);
                }
                dialogEndTimePicker = new TimePickerDialog(SetReader.this, endTimePicker, calSessionEndTime
                        .get(Calendar.HOUR_OF_DAY), calSessionEndTime.get(Calendar.MINUTE), true);
                dialogEndTimePicker.show();
            }
        });

        // to get all readers list and set into the spinner
        new GetReadersList().execute();

        // to get all sessions list and set into the spinner
        new GetSessionsList().execute();


    }

    DatePickerDialog.OnDateSetListener datePicker = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            //calSessionDate = DateTime.parse(year+"/"+monthOfYear+"/"+dayOfMonth);
            calSessionDate.set(Calendar.YEAR, year);
            calSessionDate.set(Calendar.MONTH, monthOfYear);
            calSessionDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateSessionDateLabel();
        }

    };

    private void updateSessionDateLabel() {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Config.DATETIME_FORMAT_LOCAL);
        session_date_mili = calSessionDate.getTimeInMillis();
        etSetReaderSessionDate.setText(sdf.format(calSessionDate.getTime()));
    }

    TimePickerDialog.OnTimeSetListener startTimePicker = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            /*TimeZone.setDefault(null);
            System.setProperty("user.timezone", "");
            TimeZone.setDefault(null);
            calSessionStartTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));*/
            calSessionStartTime.setTimeZone(TimeZone.getTimeZone("GMT"));
            calSessionStartTime = Calendar.getInstance();

            calSessionStartTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calSessionStartTime.set(Calendar.MINUTE, minute);

            String myFormat = "HH:mm"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat);//, Config.DATETIME_FORMAT_LOCAL);
            session_start_time_mili = calSessionStartTime.getTimeInMillis();
            etSessionStartTime.setText(sdf.format(calSessionStartTime.getTime()));
        }
    };

    TimePickerDialog.OnTimeSetListener endTimePicker = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            /*TimeZone.setDefault(null);
            System.setProperty("user.timezone", "");
            TimeZone.setDefault(null);
            calSessionEndTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));*/
            calSessionEndTime.setTimeZone(TimeZone.getTimeZone("GMT"));
            calSessionEndTime = Calendar.getInstance();
            calSessionEndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calSessionEndTime.set(Calendar.MINUTE, minute);
            //calSessionEndTime.with(Calendar.AM_PM, calSessionEndTime.get(Calendar.AM_PM));

            String myFormat = "HH:mm"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat);//, Config.DATETIME_FORMAT_LOCAL);
            session_end_time_mili = calSessionEndTime.getTimeInMillis();
            etSessionEndTime.setText(sdf.format(calSessionEndTime.getTime()));
        }
    };

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnSetReader) {
            if (checkValidation()) {

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SetReader.this);
                SharedPreferences.Editor editor = prefs.edit();
                // for the reader
                editor.putInt(PreferencesManager.KEY_SET_READER_ID, final_reader.getReaderId());
                editor.putString(PreferencesManager.KEY_SET_READER_NAME, final_reader.getReaderName());
                //editor.putInt(PreferencesManager.KEY_SET_MUSTER_ID, final_.getMusteringId());
                // for the session
                editor.putInt(PreferencesManager.KEY_SET_SESSION_ID, Integer.parseInt(final_session.getSessionId()));
                editor.putString(PreferencesManager.KEY_SET_SESSION_NAME, final_session.getSessionName());
                editor.putLong(PreferencesManager.KEY_SET_SESSION_DATE, session_date_mili);//final_session.getSessionDate());
                editor.putLong(PreferencesManager.KEY_SET_SESSION_START_TIME, session_start_time_mili);
                editor.putLong(PreferencesManager.KEY_SET_SESSION_END_TIME, session_end_time_mili);

                editor.commit();

                Intent i = new Intent();
                i.putExtra("Selected_Reader", final_reader.getReaderName());
                i.putExtra("Selected_Session", final_session.getSessionName());
                setResult(HomeScreen.SET_READER, i);
                finish();
            }
        } else if (v.getId() == R.id.ivAddSession) {
            /*startActivity(new Intent(this, SessionsActivity.class));
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
            finish();*/
        } else if (v.getId() == R.id.ivDialogClose) {
            finish();
        }
    }

    public boolean checkValidation () {
        String sessionDate = etSetReaderSessionDate.getText().toString();
        String sessionStartTime = etSessionStartTime.getText().toString();
        String sessionEndTime = etSessionEndTime.getText().toString();

        if (spReader == null || adapter == null) {
            Boast.makeText(this, getString(R.string.no_reader_available), Toast.LENGTH_SHORT).show();
            return false;
        } else if (spReader.getSelectedItem().toString().length() == 0) {
            Boast.makeText(this, getString(R.string.sel_reader), Toast.LENGTH_SHORT).show();
            return false;
        } else if (spSession == null || sessionAdapter == null) {
            Boast.makeText(this, getString(R.string.no_session_available), Toast.LENGTH_SHORT).show();
            return false;
        } else if (spSession.getSelectedItem().toString().length() == 0) {
            Boast.makeText(this, getString(R.string.sel_session), Toast.LENGTH_SHORT).show();
            return false;
        } else if (sessionDate.length() == 0) {
            Boast.makeText(this, getString(R.string.set_session_date), Toast.LENGTH_SHORT).show();
            return false;
        } else if (sessionStartTime.length() == 0) {
            Boast.makeText(this, getString(R.string.set_start_time), Toast.LENGTH_SHORT).show();
            return false;
        } else if (sessionEndTime.length() == 0) {
            Boast.makeText(this, getString(R.string.set_end_time), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!compareStartEndTime(session_start_time_mili, session_end_time_mili)) {
            Boast.makeText(this, getString(R.string.sel_proper_time_interval), Toast.LENGTH_SHORT).show();
            return false;
        } else {

            return true;
        }
    }

    public class GetReadersList extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            DatabaseHandler db = new DatabaseHandler(SetReader.this);
            //readerList = db.getAllReaders();
            String canteenId = SharedPref.INSTANCE.getCanteenId(SetReader.this);
            String canteenName = SharedPref.INSTANCE.getCanteenName(SetReader.this);
            ReaderInfoModel.DataBean reader = new ReaderInfoModel.DataBean();
            reader.setReaderId(Integer.parseInt(canteenId));
            reader.setReaderName(canteenName);
            reader.setReaderType("");
            readerList.add(reader);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (readerList.size() != 0) {
                final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                int readerId = prefs.getInt(PreferencesManager.KEY_SET_READER_ID, -1);
                int indexOfStoreReader = -1;
                for (int i = 0; i < readerList.size(); i++) {
                    //readerArray[i] = readerList.get(i).getReader_Name();
                    if (readerId != -1 && readerId >= 0 && readerId == readerList.get(i).getReaderId()) {
                        indexOfStoreReader = i;
                    }
                }

                final String[] readerArray = new String[readerList.size()];
                for (int i = 0; i < readerList.size(); i++) {
                    readerArray[i] = readerList.get(i).getReaderName();
                }

                adapter = new ReaderItemAdapter(SetReader.this, readerList, indexOfStoreReader);
                adapter.enableCheckBox(true);
                adapter.enableHideRadioOption(true);
                spReader.setAdapter(adapter);

                spReader.setSelection(indexOfStoreReader);

            } else {
                /*// after set area change the activity
                startActivity(new Intent(SetReader.this, MainActivity.class));
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);*/
            }
        }
    }

    public class GetSessionsList extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            DatabaseHandler db = new DatabaseHandler(SetReader.this);
            sessionList.addAll(db.getAllSessions());// = db.getAllSessions();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (sessionList.size() != 0) {
                final String[] sessionsArray = new String[sessionList.size()];
                int indexOfSession = -1;
                /*SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SetReader.this);
                int sessionId = prefs.getInt(PreferencesManager.KEY_SET_SESSION_ID, -1);
                String sessionName = prefs.getString(PreferencesManager.KEY_SET_SESSION_NAME, "");*/

                // get session based on current time
                int sessionId = new DatabaseHandler(SetReader.this).getSessionIdAsPerCurrentTIme();
                Log.d("SetReader", "SessionId: "+sessionId);
                System.out.println("SessionId: " + sessionId );

                if (sessionId == -1) {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SetReader.this);
                    sessionId = prefs.getInt(PreferencesManager.KEY_SET_SESSION_ID, -1);
                    String sessionName = prefs.getString(PreferencesManager.KEY_SET_SESSION_NAME, "");
                }

                for (int i = 0; i < sessionList.size(); i++) {
                    sessionsArray[i] = sessionList.get(i).getSessionName();
                    if (sessionId != -1) {
                        if (sessionId == Integer.parseInt(sessionList.get(i).getSessionId())) {
                            indexOfSession = i;
                        }
                    }
                }

                ivAddSession.setVisibility(View.GONE);
                sessionAdapter = new ArrayAdapter(SetReader.this, android.R.layout.select_dialog_item, sessionsArray);
                spSession.setAdapter(sessionAdapter);
                spSession.setSelection(indexOfSession);

            } else {
                ivAddSession.setVisibility(View.VISIBLE);
                /*// after set area change the activity
                startActivity(new Intent(SetReader.this, MainActivity.class));
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);*/
            }
        }
    }


    public boolean compareStartEndTime (long startTime, long endTime) {
        String selectedDate = etSetReaderSessionDate.getText().toString().trim();
        try {
            Calendar calStart = Calendar.getInstance();
            calStart.setTimeInMillis(startTime);
            Calendar calEnd = Calendar.getInstance();
            calEnd.setTimeInMillis(endTime);

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Config.DATETIME_FORMAT_LOCAL);
            Date startDate = sdf.parse(sdf.format(calStart.getTime()));
            Date endDate = sdf.parse(sdf.format(calEnd.getTime()));
            Log.d("startDate", ""+startDate);
            int compareDate = startDate.compareTo(endDate);
            Log.d("compareDate", ""+compareDate);
            Log.d("compare boolean", ""+startDate.after(endDate));
            if (compareDate < 0) {
                // end date is greater then start date
                return true;
            } else {
                // end date is
                // 0 - end date is equals to start date
                // -1 - end date is less then start date
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }

    }
}
