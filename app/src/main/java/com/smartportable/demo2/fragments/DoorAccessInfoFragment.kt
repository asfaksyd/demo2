package com.smartportable.demo2.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.smartportable.demo2.R
import com.smartportable.demo2.listeners.OnAuthDataReceived
import java.util.ArrayList
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.smartportable.demo2.activities.AuthActivity
import com.smartportable.demo2.activities.StaffAuthActivity
import com.smartportable.demo2.adapters.DoorAccessInfoAdapter
import com.smartportable.demo2.db.DatabaseHandler
import com.smartportable.demo2.listeners.OnStaffAuthDataReceived
import com.smartportable.demo2.models.*
import com.smartportable.demo2.utils.keyStaffCode
import com.smartportable.demo2.utils.keyStudentId


class DoorAccessInfoFragment: Fragment(), View.OnClickListener, OnAuthDataReceived, OnStaffAuthDataReceived {
    val TAG : String = DoorAccessInfoFragment:: class.java.simpleName
    lateinit var rvMainGateList: RecyclerView
    //lateinit var rvCanteenList: RecyclerView

    lateinit var mMainGateAdapter: DoorAccessInfoAdapter
    lateinit var mCanteenAdapter: DoorAccessInfoAdapter

    lateinit var mainGateList: ArrayList<DoorAccessInfoModel>
    lateinit var canteenList: ArrayList<DoorAccessInfoModel>

    lateinit var db: DatabaseHandler
    lateinit var fromStr: String

    companion object {
        fun newInstance(msg : String) : DoorAccessInfoFragment {
            val f = DoorAccessInfoFragment ()
            // Pass index input as an argument.
            val args = Bundle()
            args.putString("from", msg)
            f.setArguments(args)
            return f
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_door_info,container,false)

        initView(view)
        return view
    }

    fun initView (view: View) {
        fromStr = arguments!!.getString("from")

        if (fromStr.equals("Student")){
            var mActivity = activity as AuthActivity
            mActivity.setAuthDataReceivedListenerDA(this)
        } else {
            var mActivity1 = activity as StaffAuthActivity
            mActivity1.setStaffAuthDataReceivedListenerDA(this)
        }



        rvMainGateList = view.findViewById(R.id.rvMainGateList) as RecyclerView
        //rvCanteenList = view.findViewById(R.id.rvCanteenList) as RecyclerView

        db = DatabaseHandler(this@DoorAccessInfoFragment.context)

        mainGateList = ArrayList<DoorAccessInfoModel>()
        canteenList = ArrayList<DoorAccessInfoModel>()

        rvMainGateList.setLayoutManager(LinearLayoutManager(requireActivity()))
        mMainGateAdapter = DoorAccessInfoAdapter(requireActivity(), mainGateList)
        rvMainGateList.setAdapter(mMainGateAdapter)
        rvMainGateList.setItemAnimator(DefaultItemAnimator())

        /*rvCanteenList.setLayoutManager(LinearLayoutManager(requireActivity()))
        mCanteenAdapter = DoorAccessInfoAdapter(requireActivity(), canteenList)
        rvCanteenList.setAdapter(mCanteenAdapter)
        rvCanteenList.setItemAnimator(DefaultItemAnimator())*/

        //fillData("")

    }

    fun fillData(id: String) {
        //var list : ArrayList<DoorAccessInfoModel> = ArrayList<DoorAccessInfoModel>();
        var list : ArrayList<DoorAccessInfoModel> ? = null
        if (fromStr.equals("Student")) {
            list = db.getAllAGsOfStudent(id)//db.getAllReadersOfStudent(id)
        } else {
            list = db.getAllAGsOfStaff(id)
        }


        mMainGateAdapter.setList(list)
        mMainGateAdapter.notifyDataSetChanged()
    }

    override fun onClick(v: View?) {

    }

    override fun onAuthApiDataReceived(cardData: StudentInfoModel.DataBean?) {
        System.out.println("DG onAuthApiDataReceived")
        val licenseData = cardData
        val studentId : String = ""+licenseData!!.studentID
        fillData(studentId)

    }

    override fun onAuthDataReceived(list: ArrayList<MifareSettingModel>?, cardNo: String?) {
        System.out.println("DG onAuthDataReceived")
        for (i in list!!) {
            val fieldName = i.fieldName
            val fieldValue = i.fieldValue
            if (fieldName.equals(keyStudentId)) {
                val studentId : String = fieldValue
                fillData(studentId)
            }
        }
    }

    override fun onStaffAuthApiDataReceived(cardData: StaffInfoModel.DataBean?) {
        System.out.println("DG onStaffAuthApiDataReceived")
        val licenseData = cardData
        val staffId : String = ""+licenseData!!.staffID
        fillData(staffId)
    }

    override fun onStaffAuthDataReceived(list: ArrayList<MifareSettingModel>?, cardNo: String?) {
        System.out.println("DG onStaffAuthDataReceived")
        for (i in list!!) {
            val fieldName = i.fieldName
            val fieldValue = i.fieldValue
            if (fieldName.equals(keyStaffCode)) {
                val staffId : String = fieldValue
                fillData(staffId)
            }
        }
    }
}