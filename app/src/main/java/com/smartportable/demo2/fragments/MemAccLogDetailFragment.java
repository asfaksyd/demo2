package com.smartportable.demo2.fragments;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.smartportable.demo2.MemberAccessMainActivity;
import com.smartportable.demo2.NFCMemberAccessMainActivity;
import com.smartportable.demo2.R;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.helper.MemberAccessLogs;
import com.smartportable.demo2.helper.PreferencesManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import de.hdodenhof.circleimageview.CircleImageView;
import fr.coppernic.sdk.utils.helpers.CpcOs;

/**
 * Created by Ananth on 10/11/2017.
 */

public class MemAccLogDetailFragment extends BottomSheetDialogFragment {

    BottomSheetBehavior behaviorBehavior;
    private MemberAccessLogs memacclog;
    private String forMemAccess = "";
    private boolean isFromScan = false;

    public void setMemAccData (MemberAccessLogs memacclog) {
        this.memacclog = memacclog;
    }

    public void isForMemberAccess(String forMemAccess) {
        this.forMemAccess = forMemAccess;
    }

    public void isFromScan (boolean isFromScan) {
        this.isFromScan = isFromScan;
    }

    View contentView = null;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
        /*contentView = inflater.inflate(R.layout.frag_bsd_memacc_detail, null);

        Toolbar tbAG = (Toolbar) contentView.findViewById(R.id.tbAG);

        ImageView ivDetailClose = (ImageView) tbAG.findViewById(R.id.ivDetailClose);
        TextView tvDetailTitle = (TextView) tbAG.findViewById(R.id.tvDetailTitle);
        tvDetailTitle.setText(memacclog.getMemAccLogsEmpName());
        ivDetailClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behaviorBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });


        View bottomSheet = contentView.findViewById(R.id.design_bottom_sheet);
        behaviorBehavior = BottomSheetBehavior.from(bottomSheet);
        behaviorBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_DRAGGING");
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_SETTLING");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_EXPANDED");
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_COLLAPSED");
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_HIDDEN");
                        dismiss();
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.i("BottomSheetCallback", "slideOffset: " + slideOffset);
            }
        });*/

        //return contentView;
    }


    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        if (behaviorBehavior != null) {
            if (behaviorBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                behaviorBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        }

        contentView = View.inflate(getActivity(), R.layout.frag_bsd_memacc_detail_main, null);
        dialog.setContentView(contentView);

        //View bottomSheet = contentView.findViewById(R.id.design_bottom_sheet);
        View bottomSheet = (View) contentView.getParent();
        bottomSheet.setFitsSystemWindows(true);

        behaviorBehavior = BottomSheetBehavior.from(bottomSheet);
        behaviorBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Log.e("BottomSheetCallback", "BottomSheetBehavior.STATE_DRAGGING");
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.e("BottomSheetCallback", "BottomSheetBehavior.STATE_SETTLING");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        Log.e("BottomSheetCallback", "BottomSheetBehavior.STATE_EXPANDED");
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        Log.e("BottomSheetCallback", "BottomSheetBehavior.STATE_COLLAPSED");
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Log.e("BottomSheetCallback", "BottomSheetBehavior.STATE_HIDDEN");
                        if (isFromScan) {
                            //if ((getActivity() instanceof NFCMemberAccessMainActivity)) {
                            if (CpcOs.isCone()) {
                                ((MemberAccessMainActivity) getActivity()).refreshFragment();
                            } else if ((getActivity() instanceof NFCMemberAccessMainActivity)) {
                                ((NFCMemberAccessMainActivity) getActivity()).refreshFragment();
                            }
                        }
                        dismiss();
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.i("BottomSheetCallback", "slideOffset: " + slideOffset);
            }
        });


        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        //CoordinatorLayout.Behavior behavior = params.getBehavior();

       /* View parent = (View) contentView.getParent();
        parent.setFitsSystemWindows(true);*/

        //final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(parent);
        //contentView.measure(0, 0);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenHeight = displaymetrics.heightPixels;
        behaviorBehavior.setPeekHeight(screenHeight);


        behaviorBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        params.height = screenHeight;
        bottomSheet.setLayoutParams(params);

        final AppBarLayout app_bar_memacc = (AppBarLayout) contentView.findViewById(R.id.app_bar_memacc);
        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) contentView.findViewById(R.id.collapsing_toolbar_layout_memacc);
        //collapsingToolbarLayout.setTitle(memacclog.getMemAccLogsEmpName());
        collapsingToolbarLayout.setExpandedTitleColor(getActivity().getResources().getColor(android.R.color.transparent));

        collapsingToolbarLayout.setContentScrimColor(getActivity().getResources().getColor(R.color.colorPrimary));
        collapsingToolbarLayout.setStatusBarScrimColor(getActivity().getResources().getColor(R.color.colorPrimary));

        Toolbar tbAG = (Toolbar) collapsingToolbarLayout.findViewById(R.id.tbAG);

        ImageView ivDetailClose = (ImageView) tbAG.findViewById(R.id.ivAGClose);
        TextView tvDetailTitle = (TextView) tbAG.findViewById(R.id.tvAGTitle);
        tvDetailTitle.setText(memacclog.getMemAccLogsEmpName());
        ivDetailClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("close click");
                    behaviorBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });

        final CircleImageView ivAGEmpPhoto = (CircleImageView) contentView.findViewById(R.id.ivAGEmpPhoto);
        FloatingActionButton fabAG = (FloatingActionButton) contentView.findViewById(R.id.fabAG);
        //fabAG.setVisibility(View.VISIBLE);

        TextView tvAGLabel = (TextView) contentView.findViewById(R.id.tvAGLabel);
        TextView tvAGEmpName = (TextView) contentView.findViewById(R.id.tvAGEmpName);
        TextView tvAGEmpCardNo = (TextView) contentView.findViewById(R.id.tvAGEmpCardNo);
        TextView tvAGPunchTime = (TextView) contentView.findViewById(R.id.tvAGPunchTime);
        TextView tvAGReaderName = (TextView) contentView.findViewById(R.id.tvAGReaderName);
        TextView tvAGSessionName = (TextView) contentView.findViewById(R.id.tvAGSessionName);

        View divFirst = (View) contentView.findViewById(R.id.divFirst);

        if (!isFromScan) {
            if (forMemAccess.equals(URLCollections.MEM_ACC_TYPE_ALLOWED)) {
                tvAGEmpName.setTextColor(getResources().getColor(android.R.color.black));
                fabAG.setVisibility(View.GONE);
                tvAGLabel.setVisibility(View.GONE);
                divFirst.setVisibility(View.GONE);
                ivAGEmpPhoto.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
                app_bar_memacc.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
                tvAGPunchTime.setVisibility(View.GONE);
                tvAGReaderName.setVisibility(View.GONE);
                tvAGSessionName.setVisibility(View.GONE);
            } else if (forMemAccess.equals(URLCollections.MEM_ACC_TYPE_ACCESSED) || forMemAccess.equals(URLCollections.MEM_ACC_TYPE_DENIED)) {
                tvAGEmpName.setTextColor(getResources().getColor(android.R.color.white));
                fabAG.setVisibility(View.VISIBLE);
                tvAGLabel.setVisibility(View.VISIBLE);
                divFirst.setVisibility(View.VISIBLE);
                tvAGPunchTime.setVisibility(View.VISIBLE);
                tvAGReaderName.setVisibility(View.VISIBLE);
                tvAGSessionName.setVisibility(View.VISIBLE);
                int access_grant = memacclog.getMemAccLogsAccessGrant();
                if (access_grant == 0) {
                    fabAG.setBackgroundTintList(ColorStateList.valueOf(getActivity().getResources().getColor(android.R.color.holo_green_dark)));
                    fabAG.setImageResource(R.mipmap.ic_action_done);
                    tvAGLabel.setText(Html.fromHtml(getActivity().getResources().getString(R.string.access_grant_msg)));
                    tvAGLabel.setTextColor(getActivity().getResources().getColor(android.R.color.holo_green_dark));
                    ivAGEmpPhoto.setBackgroundColor(getActivity().getResources().getColor(android.R.color.holo_green_light));
                    app_bar_memacc.setBackgroundColor(getActivity().getResources().getColor(android.R.color.holo_green_light));
                } else if (access_grant == 1) {
                    fabAG.setBackgroundTintList(ColorStateList.valueOf(getActivity().getResources().getColor(android.R.color.holo_red_dark)));
                    fabAG.setImageResource(R.mipmap.ic_close_app);
                    tvAGLabel.setText(Html.fromHtml(getActivity().getResources().getString(R.string.access_deny_msg) + getActivity().getResources().getString(R.string.access_deny_no_door_access)));
                    tvAGLabel.setTextColor(getActivity().getResources().getColor(android.R.color.holo_red_dark));
                    ivAGEmpPhoto.setBackgroundColor(getActivity().getResources().getColor(android.R.color.holo_red_light));
                    app_bar_memacc.setBackgroundColor(getActivity().getResources().getColor(android.R.color.holo_red_light));
                } else if (access_grant == 2) {
                    fabAG.setBackgroundTintList(ColorStateList.valueOf(getActivity().getResources().getColor(android.R.color.holo_red_dark)));
                    fabAG.setImageResource(R.mipmap.ic_close_app);
                    tvAGLabel.setText(Html.fromHtml(getActivity().getResources().getString(R.string.access_deny_msg) + getActivity().getResources().getString(R.string.access_deny_duplicate_access)));
                    tvAGLabel.setTextColor(getActivity().getResources().getColor(android.R.color.holo_red_dark));
                    ivAGEmpPhoto.setBackgroundColor(getActivity().getResources().getColor(android.R.color.holo_red_light));
                    app_bar_memacc.setBackgroundColor(getActivity().getResources().getColor(android.R.color.holo_red_light));
                }
            } else if (forMemAccess.equals(URLCollections.MEM_ACC_TYPE_PENDING)) {
                tvAGEmpName.setTextColor(getResources().getColor(android.R.color.black));
                fabAG.setVisibility(View.GONE);
                tvAGLabel.setVisibility(View.GONE);
                divFirst.setVisibility(View.GONE);
                tvAGPunchTime.setVisibility(View.GONE);
                tvAGReaderName.setVisibility(View.GONE);
                tvAGSessionName.setVisibility(View.GONE);
                ivAGEmpPhoto.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
                app_bar_memacc.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
            }
        } else {
            int access_grant = memacclog.getMemAccLogsAccessGrant();
            if (access_grant == 0) {
                fabAG.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_green_dark)));
                fabAG.setImageResource(com.smartportable.demo2.R.mipmap.ic_action_done);
                tvAGLabel.setText(Html.fromHtml(getResources().getString(com.smartportable.demo2.R.string.access_grant_msg)));
                tvAGLabel.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
                ivAGEmpPhoto.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
                app_bar_memacc.setBackgroundColor(getActivity().getResources().getColor(android.R.color.holo_green_light));
            } else if (access_grant == 1) {
                fabAG.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_red_dark)));
                fabAG.setImageResource(com.smartportable.demo2.R.mipmap.ic_close_app);
                tvAGLabel.setText(Html.fromHtml(getResources().getString(com.smartportable.demo2.R.string.access_deny_msg)+getString(com.smartportable.demo2.R.string.access_deny_no_door_access)));
                tvAGLabel.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                ivAGEmpPhoto.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
                app_bar_memacc.setBackgroundColor(getActivity().getResources().getColor(android.R.color.holo_red_light));
            } else if (access_grant == 2) {
                fabAG.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_red_dark)));
                fabAG.setImageResource(com.smartportable.demo2.R.mipmap.ic_close_app);
                tvAGLabel.setText(Html.fromHtml(getResources().getString(com.smartportable.demo2.R.string.access_deny_msg)+getString(com.smartportable.demo2.R.string.access_deny_duplicate_access)));
                tvAGLabel.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                ivAGEmpPhoto.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
                app_bar_memacc.setBackgroundColor(getActivity().getResources().getColor(android.R.color.holo_red_light));
            }

            // auto dismiss the dialog after 2 seconds
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (behaviorBehavior != null)
                        behaviorBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
            }, 2000);

        }

        int emp_id = memacclog.getMemAccLogsEmpId();
        String stu_id = memacclog.getMemAccLogsStuId();
        String stu_full_name = memacclog.getMemAccLogsEmpName();
        String emp_photo = memacclog.getMemAccLogsEmpPhoto();
        String emp_name = stu_full_name;
        /*if (emp_id != -1) {
            DatabaseHandler db = new DatabaseHandler(getActivity());
            emp_photo = db.getImagePathFromEmpId(getActivity(), emp_id);
            if (db.isEmployeeAlreadyExists(db.getWritableDatabase(), emp_id)) {
                Employee emp = db.getEmployeeFromEmpId(emp_id);
                if (emp != null) {
                    emp_name = emp//emp.getSLN_Employee() + " - " + emp.getEmployeeName();
                    tvAGEmpName.setText(emp_name);
                } else {
                    emp_name = getActivity().getResources().getString(R.string.unknown_str);
                    tvAGEmpName.setText(getActivity().getResources().getString(R.string.unknown_str));
                }
            }
        } else {
            emp_name = getActivity().getResources().getString(R.string.unknown_str);
            tvAGEmpName.setText(getActivity().getResources().getString(R.string.unknown_str));
        }*/
        if (stu_id == null)
            stu_id = "";

        if (emp_name == null)
            emp_name = "";

        if (!stu_id.isEmpty() && !emp_name.isEmpty()) {
            tvAGEmpName.setText(stu_id + " - " + emp_name);
        } else {
            tvAGEmpName.setText("Unknown");
        }

        collapsingToolbarLayout.setTitle(emp_name);
        tvDetailTitle.setText(emp_name);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String reader_name = prefs.getString(PreferencesManager.KEY_SET_READER_NAME, "");
        String session_name = prefs.getString(PreferencesManager.KEY_SET_SESSION_NAME, "");
        tvAGReaderName.setText(getActivity().getResources().getString(R.string.access_point_label, reader_name));
        tvAGSessionName.setText(getActivity().getResources().getString(R.string.session_label, session_name));

        String card_no = memacclog.getMemAccLogsCardNo();
        //if (card_no.length() > 0) {
            tvAGEmpCardNo.setText(getActivity().getResources().getString(R.string.card_number_label, card_no));
        //}

        String punch_time = memacclog.getMemAccLogsPunchDate();
        if (punch_time != null) {
            if (punch_time.length() > 0) {
                tvAGPunchTime.setText(getActivity().getResources().getString(R.string.punch_time_label, punch_time));
            }
        }

        Glide
                .with(getActivity())
                .asBitmap()
                .load(emp_photo)
                .centerCrop()
                .dontAnimate()
                .error(R.mipmap.ic_profile_pic)
                .into(ivAGEmpPhoto);/*new BitmapImageViewTarget(ivAGEmpPhoto) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        ivAGEmpPhoto.setImageDrawable(circularBitmapDrawable);
                    }
                });*/
    }
}
