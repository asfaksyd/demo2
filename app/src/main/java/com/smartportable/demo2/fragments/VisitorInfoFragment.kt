package com.smartportable.demo2.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.smartportable.demo2.R
import com.smartportable.demo2.activities.VisitorAuthActivity
import com.smartportable.demo2.db.DatabaseHandler
import com.smartportable.demo2.interfaces.ChangeCardStatus
import com.smartportable.demo2.listeners.OnVisitorAuthDataReceived
import com.smartportable.demo2.models.MifareSettingModel
import com.smartportable.demo2.models.VisitorInfoModel
import com.smartportable.demo2.utils.*


class VisitorInfoFragment : Fragment() , View.OnClickListener, OnVisitorAuthDataReceived {

    val TAG : String = VisitorInfoFragment:: class.java.simpleName
    lateinit var mTvVisRegNo : TextView
    lateinit var mTvVisFirstName : TextView
    lateinit var mTvVisLastName : TextView
    lateinit var mTvCompanyName : TextView
    lateinit var mTvVisitorType : TextView
    lateinit var mTvVisitReason : TextView
    lateinit var mTvVisPhoneNo : TextView
    lateinit var mTvVisEmailId : TextView
    lateinit var mTvJobTitle : TextView
    lateinit var mTvVisAccessLevel : TextView
    lateinit var mTvVisCheckInTime : TextView
    lateinit var mTvVisCheckOutTime : TextView
    lateinit var mTvVisCardNumber : TextView
    lateinit var mTvVisCardStatus : TextView
    lateinit var mTvVisStatus : TextView

    lateinit var llVisRegNo : LinearLayout
    lateinit var llVisFirstName : LinearLayout
    lateinit var llVisLastName : LinearLayout
    lateinit var llVisComapnyName : LinearLayout
    lateinit var llVisitorType : LinearLayout
    lateinit var llVisitReason : LinearLayout
    lateinit var llVisPhoneNo : LinearLayout
    lateinit var llVisEmailId : LinearLayout
    lateinit var llVisAccessLevel : LinearLayout
    lateinit var llVisCheckInTime : LinearLayout
    lateinit var llVisCheckOutTime : LinearLayout
    lateinit var llVisCardNumber : LinearLayout
    lateinit var llVisCardStatus : LinearLayout
    lateinit var llVisStatus : LinearLayout

    lateinit var viewVisRegNo : View
    lateinit var viewVisFirstName : View
    lateinit var viewVisLastName : View
    lateinit var viewComapnyName : View
    lateinit var viewVisitorType : View
    lateinit var viewVisiitReason : View
    lateinit var viewVisPhoneNo : View
    lateinit var viewVisEmailId : View
    lateinit var viewVisAccessLevel : View
    lateinit var viewVisCheckInTime : View
    lateinit var viewVisCheckOutTime : View
    lateinit var viewVisCardNumber : View
    lateinit var viewVisCardStatus : View
    lateinit var viewVisStatus : View

    lateinit var changeCardStListener : ChangeCardStatus

    lateinit var db: DatabaseHandler

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_visitor_info,container,false)

        initView(view)
        return view
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        changeCardStListener = activity as VisitorAuthActivity
    }

    private fun initView (view : View) {

        var mActivity = activity as VisitorAuthActivity
        mActivity.setVisitorAuthDataReceivedListenerLI(this)

        llVisRegNo = view.findViewById(R.id.llVisRegNo) as LinearLayout
        llVisFirstName = view.findViewById(R.id.llVisFirstName) as LinearLayout
        llVisLastName = view.findViewById(R.id.llVisLastName) as LinearLayout
        llVisComapnyName = view.findViewById(R.id.llCompanyName) as LinearLayout
        llVisitorType = view.findViewById(R.id.llVisType) as LinearLayout
        llVisitReason = view.findViewById(R.id.llVisitReason) as LinearLayout
        llVisPhoneNo = view.findViewById(R.id.llVisPhoneNo) as LinearLayout
        llVisEmailId = view.findViewById(R.id.llVisEmailId) as LinearLayout
        llVisAccessLevel = view.findViewById(R.id.llVisAccessLevel) as LinearLayout
        llVisCheckInTime = view.findViewById(R.id.llVisCheckInTime) as LinearLayout
        llVisCheckOutTime = view.findViewById(R.id.llVisCheckOutTime) as LinearLayout
        llVisCardStatus = view.findViewById(R.id.llVisCardStatus) as LinearLayout
        llVisStatus = view.findViewById(R.id.llVisStatus) as LinearLayout


        viewVisRegNo = view.findViewById(R.id.viewVisRegNo) as View
        viewVisFirstName = view.findViewById(R.id.viewVisFirstName) as View
        viewVisLastName = view.findViewById(R.id.viewVisLastName) as View
        viewComapnyName = view.findViewById(R.id.viewCompanyName) as View
        viewVisitorType = view.findViewById(R.id.viewVisType) as View
        viewVisiitReason = view.findViewById(R.id.viewVisitReason) as View
        viewVisPhoneNo = view.findViewById(R.id.viewVisPhoneNo) as View
        viewVisEmailId = view.findViewById(R.id.viewVisEmailId) as View
        viewVisAccessLevel = view.findViewById(R.id.viewVisAccessLevel) as View
        viewVisCheckInTime = view.findViewById(R.id.viewVisCheckInTime) as View
        viewVisCheckOutTime = view.findViewById(R.id.viewVisCheckOutTime) as View
        viewVisCardStatus = view.findViewById(R.id.viewVisCardStatus) as View
        viewVisStatus = view.findViewById(R.id.viewVisStatus) as View



        mTvVisRegNo = view.findViewById(R.id.mTvVisRegNo) as TextView
        mTvVisFirstName = view.findViewById(R.id.mTvVisFirstName) as TextView
        mTvVisLastName = view.findViewById(R.id.mTvVisLastName) as TextView
        mTvCompanyName = view.findViewById(R.id.mTvCompanyName) as TextView
        mTvVisitorType = view.findViewById(R.id.mTvVisType) as TextView
        mTvVisitReason = view.findViewById(R.id.mTvVisitReason) as TextView
        mTvVisPhoneNo = view.findViewById(R.id.mTvVisPhoneNo) as TextView
        mTvVisEmailId = view.findViewById(R.id.mTvVisEmailId) as TextView
        mTvVisAccessLevel = view.findViewById(R.id.mTvVisAccessLevel) as TextView
        mTvVisCheckInTime = view.findViewById(R.id.mTvVisCheckInTime) as TextView
        mTvVisCheckOutTime = view.findViewById(R.id.mTvVisCheckOutTime) as TextView
        mTvVisCardNumber = view.findViewById(R.id.mTvVisCardNumber) as TextView
        mTvVisCardStatus = view.findViewById(R.id.mTvVisCardStatus) as TextView
        mTvVisStatus = view.findViewById(R.id.mTvVisStatus) as TextView


        db = DatabaseHandler(requireActivity())
    }
    override fun onClick(v: View?) {


    }


        override fun onVisitorAuthDataReceived(list: ArrayList<MifareSettingModel>, uuidNumber : String ) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        /*for (i in list) {
            var vis_reg_no : String ?= null
            val fieldName = i.fieldName
            val fieldValue = i.fieldValue
            if (fieldName.equals(keyStaffCode)) {
                mTvVisRegNo.text = fieldValue
                vis_reg_no = fieldValue
            } else if (fieldName.equals(keyStaffFullName)) {
                mTvVisitorType.text = fieldValue
            } else if (fieldName.equals(keyStaffSex)) {
                mTvGender.text = fieldValue
            } else if (fieldName.equals(keyStaffJobTitle)) {
                mTvJobTitle.text = fieldValue
            } else if (fieldName.equals(keyStaffDept)) {
                mTvDepartment.text = fieldValue
            } else if (fieldName.equals(keyStaffCollege)) {
                //mTvCollege
            }
            mTvCardNo.text = uuidNumber
            *//*else if (fieldName.equals(keyCampus)) {
                mTvDepartment.text = fieldValue
            } else if (fieldName.equals(keyProgram)) {
                //mTvCampus.text = fieldValue
            } else if (fieldName.equals(keyDegreeType)) {
                //mTvProgram.text = fieldValue
            } else if (fieldName.equals(keyAdmissionType)) {
                //mTvDegreeType.text = fieldValue
            } else if (fieldName.equals(keyAdmissionTypeShort)) {
                //mTvAdmissionType.text = fieldValue
            } else if (fieldName.equals(keyValidDateUntil)) {
                //mTvValidUntil.text = fieldValue
            } else if (fieldName.equals(keyIssueDate)) {
                mTvIssueDate.text = fieldValue
            } else if (fieldName.equals(keyMealNumber)) {
                //mTvMealNumber.text = fieldValue
            } else if (fieldName.equals(keyUniqueNo)) {
                //mTvUniqueNo.text = fieldValue
            } else if (fieldName.equals(keyActive)) {
                //mTvStatus.text = fieldValue
            } else if (fieldName.equals(keyCardNo)) {
                //mTvUUIDNo.text = fieldValue
            }*//*
            llVisFirstName.visibility = View.GONE
            llVisLastName.visibility = View.GONE
            llVisComapnyName.visibility = View.GONE
            llVisPhoneNo.visibility = View.GONE
            llVisStatus.visibility = View.GONE
            llIdNo.visibility = View.GONE
            llIssueDate.visibility = View.GONE
            //llCardStatus.visibility = View.GONE
            llEmailId.visibility = View.GONE
            llIsActive.visibility = View.GONE

            viewVisFirstName.visibility = View.GONE
            viewVisLastName.visibility = View.GONE
            viewComapnyName.visibility = View.GONE
            viewVisPhoneNo.visibility = View.GONE
            viewVisStatus.visibility = View.GONE
            viewIdNo.visibility = View.GONE
            viewIssueDate.visibility = View.GONE
            //viewCardStatus.visibility = View.GONE
            viewEmailId.visibility = View.GONE


            //(activity as AuthActivity)!!.setCardStatus(licenseData!!.cardStatus.toInt())


            if (staff_id != null) {
                if (staff_id.isNotEmpty()) {
                    val staff_card_st: Int = db.getCardStatusFromStaffId(staff_id)
                    if (staff_card_st != null) {
                        if (changeCardStListener != null) {
                            changeCardStListener.onChangeCardStatus(activity, staff_card_st)
                            var cardStStr: String? = null
                            if (staff_card_st == CARD_ST_ACTIVE) {
                                cardStStr = "Active"
                            } else if (staff_card_st == CARD_ST_REVOKED) {
                                cardStStr = "Revoked"
                            } else if (staff_card_st == CARD_ST_LOST) {
                                cardStStr = "Lost"
                            } else if (staff_card_st == CARD_ST_SUSPENDED) {
                                cardStStr = "Suspended"
                            } else if (staff_card_st == CARD_ST_EXPIRED) {
                                cardStStr = "Expired"
                            }

                            mTvCardStatus.text = cardStStr
                        }
                    }

                    val staff_image: String = db.getStaffImageFromStaffId(staff_id)
                    if (staff_image != null) {
                        if (!staff_image.isEmpty()) {
                            if (changeCardStListener != null) {
                                changeCardStListener.onCardHolderImageIntoPellete(staff_image)
                            }
                        }
                    }

                }
            }
        }*/
    }

    override fun onVisitorAuthApiDataReceived(cardData: VisitorInfoModel.DataBean?) {
        val licenseData = cardData
        mTvVisRegNo.text = licenseData!!.visitorRegNo
        mTvVisFirstName.text = licenseData!!.visitorFirstName
        mTvVisLastName.text = licenseData!!.visitorLasttName
        mTvCompanyName.text = licenseData!!.companyName
        mTvVisitorType.text = licenseData.visitorType
        mTvVisitReason.text = licenseData!!.visitReason
        mTvVisPhoneNo.text = licenseData!!.visitorPhoneNo
        mTvVisEmailId.text = licenseData!!.visitorEmailId
        mTvVisAccessLevel.text = licenseData!!.visitorAccessLevel
        mTvVisCheckInTime.text = licenseData!!.visitorCheckInTime
        mTvVisCheckOutTime.text = licenseData!!.visitorCheckOutTime
        mTvVisCardNumber.text = licenseData!!.visitorCardNumber
        val vis_status  = Integer.parseInt(licenseData!!.visitorStatus)
        var visSTColor = R.color.colorPrimary
        var visStStr = ""
        if (vis_status == CARD_ST_ACTIVE) {
            visStStr = "Active"
            visSTColor = R.color.colorGreen
        } else if (vis_status == CARD_ST_REVOKED) {
            visStStr = "Revoked"
            visSTColor = R.color.color_red_fr
        } else if (vis_status == CARD_ST_LOST) {
            visStStr = "Lost"
            visSTColor = R.color.color_red_fr
        } else if (vis_status == CARD_ST_SUSPENDED) {
            visStStr = "Suspended"
            visSTColor = R.color.color_red_fr
        } else if (vis_status == CARD_ST_EXPIRED) {
            visStStr = "Expired"
            visSTColor = R.color.color_red_fr
        }

        mTvVisStatus.setText(visStStr)
        mTvVisStatus.setTextColor(resources.getColor(visSTColor))


        val card_st: String = ""+licenseData!!.visitorCardStatus
        if (!card_st.isEmpty()) {
            if (!card_st.equals("null")) {
                var vis_card_status: Int = Integer.parseInt(card_st)
                var visCardSTColor = R.color.colorPrimary
                var cardStStr = ""
                if (vis_card_status == VIS_CARD_ST_ACTIVE) {
                    //cardColor = R.color.colorGreen;
                    cardStStr = "Active"
                    visCardSTColor = R.color.colorGreen
                    //ardStIcon = R.mipmap.ic_action_done;
                } else if (vis_card_status == VIS_CARD_ST_IN_USE) {
                    //cardColor = R.color.colorGreen;
                    cardStStr = "In Used"
                    visCardSTColor = R.color.colorGreen
                    //cardStIcon = R.mipmap.ic_action_done;
                } else if (vis_card_status == VIS_CARD_ST_EXPIRED) {
                    //cardColor = R.color.colorRed;
                    cardStStr = "Expired"
                    visCardSTColor = R.color.color_red_fr
                    //cardStIcon = R.mipmap.ic_close_app;
                } else if (vis_card_status == VIS_CARD_ST_LOST) {
                    //cardColor = R.color.colorBlack;
                    cardStStr = "Lost"
                    visCardSTColor = R.color.colorBlack
                    //cardStIcon = R.mipmap.ic_close_app;
                }

                mTvVisCardStatus.text = cardStStr
                mTvVisCardStatus.setTextColor(ContextCompat.getColorStateList(this.requireContext(), visCardSTColor))
            }
        }

    }

}