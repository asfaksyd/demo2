package com.smartportable.demo2.fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.smartportable.demo2.R;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.SaveImages;
import com.smartportable.demo2.helper.Utility;
import com.smartportable.demo2.models.StudentInfoModel;
import com.smartportable.demo2.utils.KeysKt;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ananth on 10/10/2017.
 */

public class StudentDetailFragment extends BottomSheetDialogFragment {
    StudentInfoModel.DataBean studentInfo;
    ProgressBar pbReaders;
    TextView tv_dialog_emp_reader_id;
    ListView lvAllowedReadersList;
    boolean forMemAccess = false;
    AppBarLayout app_bar_stu;
    CollapsingToolbarLayout collapsingToolbarLayout;



    public void setStudentDetail (StudentInfoModel.DataBean studentInfo) {
        this.studentInfo = studentInfo;
    }

    public void isForMemberAccess(boolean forMemAccess) {
        this.forMemAccess = forMemAccess;
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        //Employee item = list_employee.get(position);
        String stu_id = studentInfo.getStudentID();
        String stu_name = studentInfo.getFirstName() + " " + studentInfo.getFatherName() + " " + studentInfo.getGrandFatherName();
        String stu_gender = studentInfo.getGender();
        String stu_dob = studentInfo.getDateOfBirth();
        String stu_meal_no = studentInfo.getMealNumber();
        String stu_dept = studentInfo.getDepartment();
        String stu_college = studentInfo.getCollege();
        String stu_campus = studentInfo.getCampus();
        String stu_program = studentInfo.getProgram();
        String stu_degreeType = studentInfo.getDegreeType();
        String stu_admissionType = studentInfo.getAdmissionType();
        String stu_addmissionTypeShort = studentInfo.getAdmissionTypeShort();
        String stu_validDateUntil = studentInfo.getValidDateUntil();
        String stu_issueDate = studentInfo.getIssueDate();
        String stu_uniqueNo = studentInfo.getUniqueNo();
        String stu_cardNumber = studentInfo.getCardNumber();
        int stu_cardStatus = Integer.parseInt(studentInfo.getCardstatus());

        //String baseUrl = SharedPref.INSTANCE.getStringValue(getActivity(), KeysKt.keyBaseUrl, "");
        String stu_image =  studentInfo.getStudentImage();

        View contentView = View.inflate(getActivity(), R.layout.frag_bsd_student_detail, null);
        dialog.setContentView(contentView);


        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        /*CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }*/

        View parent = (View) contentView.getParent();
        parent.setFitsSystemWindows(true);

        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(parent);
        contentView.measure(0, 0);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenHeight = displaymetrics.heightPixels;
        bottomSheetBehavior.setPeekHeight(screenHeight);

        if (params.getBehavior() instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior)params.getBehavior()).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        //params.height = screenHeight;
        parent.setLayoutParams(params);

        FloatingActionButton fab = (FloatingActionButton) contentView.findViewById(R.id.fabStuDialog);
        fab.setVisibility(View.GONE);

        app_bar_stu = (AppBarLayout) contentView.findViewById(R.id.app_bar_stu);
        collapsingToolbarLayout = (CollapsingToolbarLayout) contentView.findViewById(R.id.stu_collapsing_toolbar_layout);
        collapsingToolbarLayout.setTitle(stu_id);
        collapsingToolbarLayout.setExpandedTitleColor(getActivity().getResources().getColor(android.R.color.transparent));

        collapsingToolbarLayout.setContentScrimColor(getActivity().getResources().getColor(R.color.colorPrimary));
        collapsingToolbarLayout.setStatusBarScrimColor(getActivity().getResources().getColor(R.color.colorPrimary));

        Toolbar toolBar = (Toolbar) collapsingToolbarLayout.findViewById(R.id.tbStuDetail);
        ImageView ivDetailClose = (ImageView) toolBar.findViewById(R.id.ivStuDetailClose);
        TextView tvDetailTitle = (TextView) toolBar.findViewById(R.id.tvStuDetailTitle);
        TextView tvDetailAdmissionType = (TextView) toolBar.findViewById(R.id.tv_dialog_stu_admissionTypeShort);
        if (stu_id.length() >= 0) {
            tvDetailTitle.setText(stu_id + " - " + stu_name);
        } else {
            tvDetailTitle.setText("");
        }
        ivDetailClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Emp Detail close click");
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });

        final CircleImageView image= (CircleImageView) contentView.findViewById(R.id.ivStuFullScreen);
        TextView tv_dialog_stu_id = (TextView) contentView.findViewById(R.id.tv_dialog_stu_id);
        TextView tv_dialog_stu_name = (TextView) contentView.findViewById(R.id.tv_dialog_stu_name);
        TextView tv_dialog_stu_gender = (TextView) contentView.findViewById(R.id.tv_dialog_stu_gender);
        TextView tv_dialog_stu_dob = (TextView) contentView.findViewById(R.id.tv_dialog_stu_dob);
        TextView tv_dialog_stu_meal_no = (TextView) contentView.findViewById(R.id.tv_dialog_stu_meal_no);
        TextView tv_dialog_stu_dept = (TextView) contentView.findViewById(R.id.tv_dialog_stu_dept);
        TextView tv_dialog_stu_college = (TextView) contentView.findViewById(R.id.tv_dialog_stu_college);
        TextView tv_dialog_stu_campus = (TextView) contentView.findViewById(R.id.tv_dialog_stu_campus);
        TextView tv_dialog_stu_program = (TextView) contentView.findViewById(R.id.tv_dialog_stu_program);
        TextView tv_dialog_stu_degreeType = (TextView) contentView.findViewById(R.id.tv_dialog_stu_degreeType);
        TextView tv_dialog_stu_admissionType = (TextView) contentView.findViewById(R.id.tv_dialog_stu_admissionType);
        //TextView tv_dialog_stu_admissionTypeShort = (TextView) contentView.findViewById(R.id.tv_dialog_stu_admissionTypeShort);
        TextView tv_dialog_stu_validDateUntil = (TextView) contentView.findViewById(R.id.tv_dialog_stu_validDateUntil);
        TextView tv_dialog_stu_issueDate = (TextView) contentView.findViewById(R.id.tv_dialog_stu_issueDate);
        TextView tv_dialog_stu_UniqueNo = (TextView) contentView.findViewById(R.id.tv_dialog_stu_UniqueNo);
        TextView tv_dialog_stu_cardNumber = (TextView) contentView.findViewById(R.id.tv_dialog_stu_cardNumber);
        TextView tv_dialog_stu_card_status = (TextView) contentView.findViewById(R.id.tv_dialog_stu_card_status);
        TextView tv_dialog_stu_reader_label = (TextView) contentView.findViewById(R.id.tv_dialog_stu_reader_label);



        lvAllowedReadersList = (ListView) contentView.findViewById(R.id.lvAllowedReadersList);

        pbReaders = (ProgressBar) contentView.findViewById(R.id.pbStuReaders);

        tv_dialog_stu_name.setVisibility(View.VISIBLE);
        //tv_dialog_emp_loc_id.setVisibility(View.GONE);
        //tv_dialog_emp_reader_id.setVisibility(View.GONE);
        pbReaders.setVisibility(View.GONE);
        if (forMemAccess) {
            //pbReaders.setVisibility(View.VISIBLE);
            tv_dialog_stu_reader_label.setVisibility(View.VISIBLE);
            //new getReadersList().execute(""+stu_id);
            new getCanteensList().execute(""+stu_id);
        } else {
            tv_dialog_stu_reader_label.setVisibility(View.GONE);
        }
        //tv_dialog_emp_datetime.setVisibility(View.GONE);

        if (stu_id.length() >= 0 && stu_name != null && stu_name.length() > 0)
            tv_dialog_stu_id.setText(stu_id);
            //tv_dialog_stu_name.setText(stu_name);
        if (stu_name != null && stu_name.length() > 0)
            tv_dialog_stu_name.setText("" + stu_name);

        if (stu_gender.trim().equals("M")) {
            tv_dialog_stu_gender.setText("Gender : Male");
        } else {
            tv_dialog_stu_gender.setText("Gender : Female");
        }

        tv_dialog_stu_dob.setText("Date of birth : "+stu_dob);
        //tv_dialog_stu_dob.setVisibility(View.GONE);
        /*SimpleDateFormat output_sdf = new SimpleDateFormat("dd MMM, YYYY", Locale.US);
        SimpleDateFormat input_sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        try {
            Date date1 = input_sdf.parse(stu_dob);
            String dob = output_sdf.format(date1);
            tv_dialog_stu_dob.setText("Date of birth : "+dob);
        } catch (ParseException ex) {
            ex.printStackTrace();
            tv_dialog_stu_dob.setText("Date of birth : ");
        }*/


        tv_dialog_stu_dept.setText("Department: "+stu_dept);
        tv_dialog_stu_meal_no.setText("Meal Number: "+stu_meal_no);
        tv_dialog_stu_college.setText("College : "+stu_college);
        tv_dialog_stu_campus.setText("Campus : "+stu_campus);
        tv_dialog_stu_program.setText("Program : "+ stu_program);
        tv_dialog_stu_degreeType.setText("DegreeType : "+ stu_degreeType);
        tv_dialog_stu_admissionType.setText("AdmissionType : "+ stu_admissionType);
        tvDetailAdmissionType.setText(""+ stu_addmissionTypeShort);
        tv_dialog_stu_validDateUntil.setText("ValidDateUntil : "+stu_validDateUntil);
        tv_dialog_stu_issueDate.setText("Issue Date : "+ stu_issueDate);
        tv_dialog_stu_UniqueNo.setText("Unique No. : "+ stu_uniqueNo);
        tv_dialog_stu_cardNumber.setText("Card Number : "+stu_cardNumber);

        String cardStStr = "";
        if (stu_cardStatus == KeysKt.CARD_ST_ACTIVE) {
            cardStStr = "Active";
        } else if (stu_cardStatus == KeysKt.CARD_ST_REVOKED) {
            cardStStr = "Revoked";
        } else if (stu_cardStatus == KeysKt.CARD_ST_LOST) {
            cardStStr = "Lost";
        } else if (stu_cardStatus == KeysKt.CARD_ST_SUSPENDED) {
            cardStStr = "Suspended";
        } else if (stu_cardStatus == KeysKt.CARD_ST_EXPIRED) {
            cardStStr = "Expired";
        }

        tv_dialog_stu_card_status.setText("Card Status : " + cardStStr);
        setCardStatus(stu_cardStatus);

        /*if (card_no != null && card_no.length() > 0) {
            tv_dialog_emp_card.setVisibility(View.VISIBLE);
            tv_dialog_emp_card.setText("Card Number : " + card_no);
        } else {
            tv_dialog_emp_card.setVisibility(View.GONE);
            tv_dialog_emp_card.setText("");
        }
        if (loc_id >= 0)
            tv_dialog_emp_loc_id.setText("" + loc_id);*/

                /*if (readerId >= 0) {
                    tv_dialog_emp_reader_id.setText(""+readerId);
                }*/
                /*if (punch_date != null && punch_date.length() > 0) {
                    tv_dialog_emp_loc_id.setText(""+punch_date);
                }*/

        /*dialog.setNegativeButton(R.string.ok_label, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
            }
        });*/

        /** April 18 -19 **/
        /*String fName_ = stu_image.substring(stu_image.lastIndexOf('/') + 1);

        String path = Environment.getExternalStorageDirectory().toString() + "/ImageSave_play/StudentImage/"+fName_;

        File imgFile =  new File(path);

        if(stu_image!= null && !stu_image.isEmpty())
        {
            if(imgFile.exists())
            {
                Glide.with(getActivity())
                        .load(imgFile.getAbsolutePath())
                        .error(R.mipmap.ic_profile_pic)
                        .into(image);
            }
            else
            {
                Log.e("Transform", "Transform----image--not---present-StuDetail-");

                if(isNetworkAvailable(getActivity()))
                    ImageToLocal(getActivity(),stu_image,image,"StudentImage");
                else
                    image.setImageResource(R.mipmap.ic_profile_pic);
            }
        }
        else
            image.setImageResource(R.mipmap.ic_profile_pic);*/


        if (!stu_image.isEmpty()) {

            SaveImages si = new SaveImages();
            Bitmap bmp = si.loadImageFromStorage(stu_image);

            Glide
                    .with(getActivity())
                    .asBitmap()
                    .load(bmp)
                    //.centerCrop()
                    //.dontAnimate()
                    .error(R.mipmap.ic_profile_pic)
                    .into(image);
                /*.into(new BitmapImageViewTarget(image) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        image.setImageDrawable(circularBitmapDrawable);
                    }
                });*/
        }

    }

    private class getReadersList extends AsyncTask<String , Void, ArrayList<String>> {
        DatabaseHandler db;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbReaders.setVisibility(View.VISIBLE);
            db = new DatabaseHandler(getActivity());

        }

        @Override
        protected ArrayList<String> doInBackground(String... params) {
            ArrayList<String> list_readers = db.getAllReadersOfAccList(params[0]);
            return list_readers;
        }

        @Override
        protected void onPostExecute(ArrayList<String> list) {
            super.onPostExecute(list);
            pbReaders.setVisibility(View.GONE);
            //tv_dialog_emp_reader_id.setVisibility(View.GONE);
            if (list != null) {
                if (list.size() > 0) {
                    /*for (int i=0; i<list.size();i++) {
                        String reader_ids = tv_dialog_emp_reader_id.getText().toString();
                        if (i==0) {
                            tv_dialog_emp_reader_id.setText(list.get(i));
                        }else {
                            tv_dialog_emp_reader_id.setText(reader_ids+" , "+list.get(i));
                        }
                    }*/

                    ArrayAdapter<String> adapter =  new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, list);
                    lvAllowedReadersList.setAdapter(adapter);
                    Utility.setListViewHeightBasedOnChildren(lvAllowedReadersList);
                }
            }
            if (db != null)
                db.close();
        }
    }


    private class getCanteensList extends AsyncTask<String , Void, ArrayList<String>> {
        DatabaseHandler db;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbReaders.setVisibility(View.VISIBLE);
            db = new DatabaseHandler(getActivity());

        }

        @Override
        protected ArrayList<String> doInBackground(String... params) {
            ArrayList<String> list_ags = db.getAllAGsOfAccList(params[0]);
            return list_ags;
        }

        @Override
        protected void onPostExecute(ArrayList<String> list) {
            super.onPostExecute(list);
            pbReaders.setVisibility(View.GONE);
            //tv_dialog_emp_reader_id.setVisibility(View.GONE);
            if (list != null) {
                if (list.size() > 0) {
                    /*for (int i=0; i<list.size();i++) {
                        String reader_ids = tv_dialog_emp_reader_id.getText().toString();
                        if (i==0) {
                            tv_dialog_emp_reader_id.setText(list.get(i));
                        }else {
                            tv_dialog_emp_reader_id.setText(reader_ids+" , "+list.get(i));
                        }
                    }*/

                    ArrayAdapter<String> adapter =  new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, list);
                    lvAllowedReadersList.setAdapter(adapter);
                    Utility.setListViewHeightBasedOnChildren(lvAllowedReadersList);
                }
            }
            if (db != null)
                db.close();
        }
    }

    public void setCardStatus (int cardStCode) {
        int cardColor = -1;
        if (cardStCode == KeysKt.CARD_ST_ACTIVE) {
            cardColor = R.color.green;
        } else if (cardStCode == KeysKt.CARD_ST_REVOKED) {
            cardColor = android.R.color.darker_gray;
        } else if (cardStCode == KeysKt.CARD_ST_LOST) {
            cardColor = R.color.colorBlack;
        } else if (cardStCode == KeysKt.CARD_ST_SUSPENDED) {
            cardColor = R.color.colorYellow;
        } else if (cardStCode == KeysKt.CARD_ST_EXPIRED) {
            cardColor = R.color.red;
        }

        app_bar_stu.setBackgroundColor(ContextCompat.getColor(getActivity(), cardColor));
        collapsingToolbarLayout.setStatusBarScrimColor(ContextCompat.getColor(getActivity(), cardColor));
        collapsingToolbarLayout.setBackgroundColor(ContextCompat.getColor(getActivity(), cardColor));
        collapsingToolbarLayout.setContentScrimColor(ContextCompat.getColor(getActivity(), cardColor));
        collapsingToolbarLayout.setStatusBarScrimColor(ContextCompat.getColor( getActivity(), cardColor));
    }

}
