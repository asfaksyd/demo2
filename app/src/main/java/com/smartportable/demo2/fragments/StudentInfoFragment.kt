package com.smartportable.demo2.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.smartportable.demo2.R
import com.smartportable.demo2.activities.AuthActivity
import com.smartportable.demo2.db.DatabaseHandler
import com.smartportable.demo2.interfaces.ChangeCardStatus
import com.smartportable.demo2.listeners.OnAuthDataReceived
import com.smartportable.demo2.models.MifareSettingModel
import com.smartportable.demo2.models.StudentInfoModel
import com.smartportable.demo2.utils.*


class StudentInfoFragment : Fragment() , View.OnClickListener, OnAuthDataReceived {

    val TAG : String = StudentInfoFragment:: class.java.simpleName
    lateinit var llStudentId : LinearLayout
    lateinit var llIssueDate : LinearLayout
    lateinit var llFullName : LinearLayout
    lateinit var llGender : LinearLayout
    lateinit var llDateOfBirth : LinearLayout
    lateinit var llCollege : LinearLayout
    lateinit var llDepartment : LinearLayout
    lateinit var llCampus : LinearLayout
    lateinit var llProgram : LinearLayout
    lateinit var llDegreeType : LinearLayout
    lateinit var llAdmissionType : LinearLayout
    lateinit var llAdmissionTypeShort : LinearLayout
    lateinit var llValidUntil : LinearLayout
    lateinit var llMealNumber : LinearLayout
    lateinit var llUniqueNo : LinearLayout
    lateinit var llStatus : LinearLayout
    lateinit var llUUIDNo : LinearLayout


    lateinit var viewStudentId : View
    lateinit var viewIssueDate : View
    lateinit var viewFullName : View
    lateinit var viewGender : View
    lateinit var viewDateOfBirth : View
    lateinit var viewCollege : View
    lateinit var viewDepartment : View
    lateinit var viewCampus : View
    lateinit var viewProgram : View
    lateinit var viewDegreeType : View
    lateinit var viewAdmissionType : View
    lateinit var viewAdmissionTypeShort : View
    lateinit var viewValidUntil : View
    lateinit var viewMealNumber : View
    lateinit var viewUniqueNo : View
    lateinit var viewStatus : View
    lateinit var viewUUIDNo : View

    lateinit var mTvStudentId : TextView
    lateinit var mTvIssueDate : TextView
    lateinit var mTvFullName : TextView
    lateinit var mTvGender : TextView
    lateinit var mTvDateOfBirth : TextView
    lateinit var mTvCollege : TextView
    lateinit var mTvDepartment : TextView
    lateinit var mTvCampus : TextView
    lateinit var mTvProgram : TextView
    lateinit var mTvDegreeType : TextView
    lateinit var mTvAdmissionType : TextView
    lateinit var mTvAdmissionTypeShort : TextView
    lateinit var mTvValidUntil : TextView
    lateinit var mTvMealNumber : TextView
    lateinit var mTvUniqueNo : TextView
    lateinit var mTvStatus : TextView
    lateinit var mTvUUIDNo : TextView
    lateinit var db: DatabaseHandler

    lateinit var changeCardStListener : ChangeCardStatus

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_student_info,container,false)

        initView(view)
        return view
    }

    private fun initView (view : View) {

        var mActivity = activity as AuthActivity
        mActivity.setAuthDataReceivedListenerLI(this)


        llStudentId = view.findViewById(R.id.llStudentId) as LinearLayout
        llFullName = view.findViewById(R.id.llFullName) as LinearLayout
        llGender = view.findViewById(R.id.llGender) as LinearLayout
        llDateOfBirth = view.findViewById(R.id.llDateOfBirth) as LinearLayout
        llCollege = view.findViewById(R.id.llCollege) as LinearLayout
        llDepartment = view.findViewById(R.id.llDepartment) as LinearLayout
        llCampus = view.findViewById(R.id.llCampus) as LinearLayout
        llProgram = view.findViewById(R.id.llProgram) as LinearLayout
        llDegreeType = view.findViewById(R.id.llDegreeType) as LinearLayout
        llAdmissionType = view.findViewById(R.id.llAdmissionType) as LinearLayout
        llAdmissionTypeShort = view.findViewById(R.id.llAdmissionTypeShort) as LinearLayout
        llValidUntil = view.findViewById(R.id.llValidUntil) as LinearLayout
        llIssueDate = view.findViewById(R.id.llIssueDate) as LinearLayout
        llMealNumber = view.findViewById(R.id.llMealNumber) as LinearLayout
        llUniqueNo = view.findViewById(R.id.llUniqueNo) as LinearLayout
        llStatus = view.findViewById(R.id.llStatus) as LinearLayout
        llUUIDNo = view.findViewById(R.id.llUUIDNo) as LinearLayout

        viewStudentId = view.findViewById(R.id.viewStudentId) as View
        viewFullName = view.findViewById(R.id.viewFullName) as View
        viewGender = view.findViewById(R.id.viewGender) as View
        viewDateOfBirth = view.findViewById(R.id.viewDateOfBirth) as View
        viewCollege = view.findViewById(R.id.viewCollege) as View
        viewDepartment = view.findViewById(R.id.viewDepartment) as View
        viewCampus = view.findViewById(R.id.viewCampus) as View
        viewProgram = view.findViewById(R.id.viewProgram) as View
        viewDegreeType = view.findViewById(R.id.viewDegreeType) as View
        viewAdmissionType = view.findViewById(R.id.viewAdmissionType) as View
        viewAdmissionTypeShort = view.findViewById(R.id.viewAdmissionTypeShort) as View
        viewValidUntil = view.findViewById(R.id.viewValidUntil) as View
        viewIssueDate = view.findViewById(R.id.viewIssueDate) as View
        viewMealNumber = view.findViewById(R.id.viewMealNumber) as View
        viewUniqueNo = view.findViewById(R.id.viewUniqueNo) as View
        viewStatus = view.findViewById(R.id.viewStatus) as View
        viewUUIDNo = view.findViewById(R.id.viewUUIDNo) as View


        mTvStudentId = view.findViewById(R.id.mTvStudentId) as TextView
        mTvFullName = view.findViewById(R.id.mTvFullName) as TextView
        mTvGender = view.findViewById(R.id.mTvGender) as TextView
        mTvDateOfBirth = view.findViewById(R.id.mTvDateOfBirth) as TextView
        mTvCollege = view.findViewById(R.id.mTvCollege) as TextView
        mTvDepartment = view.findViewById(R.id.mTvDepartment) as TextView
        mTvCampus = view.findViewById(R.id.mTvCampus) as TextView
        mTvProgram = view.findViewById(R.id.mTvProgram) as TextView
        mTvDegreeType = view.findViewById(R.id.mTvDegreeType) as TextView
        mTvAdmissionType = view.findViewById(R.id.mTvAdmissionType) as TextView
        mTvAdmissionTypeShort = view.findViewById(R.id.mTvAdmissionTypeShort) as TextView
        mTvValidUntil = view.findViewById(R.id.mTvValidUntil) as TextView
        mTvIssueDate = view.findViewById(R.id.mTvIssueDate) as TextView
        mTvMealNumber = view.findViewById(R.id.mTvMealNumber) as TextView
        mTvUniqueNo = view.findViewById(R.id.mTvUniqueNo) as TextView
        mTvStatus = view.findViewById(R.id.mTvStatus) as TextView
        mTvUUIDNo = view.findViewById(R.id.mTvUUIDNo) as TextView

        db = DatabaseHandler(requireActivity())
    }
    override fun onClick(v: View?) {


    }


        override fun onAuthDataReceived(list: ArrayList<MifareSettingModel>, uuidNumber : String ) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        for (i in list) {
            var stu_id : String? = null
            val fieldName = i.fieldName
            val fieldValue = i.fieldValue
            if (fieldName.equals(keyStudentId)) {
                mTvStudentId.text = fieldValue
                stu_id  = fieldValue
            } else if (fieldName.equals(keyGender)) {
                mTvGender.text = fieldValue
            } else if (fieldName.equals(keyFullName)) {
                mTvFullName.text = fieldValue
            } else if (fieldName.equals(keyDateOfBirth)) {
                /*val trainCenterID = fieldValue
                val trainCenterName = if(!trainCenterID.isBlank()) {db.getTrainCenterNameFromId(trainCenterID.toInt())} else {""}
                mTvGender.text = if (trainCenterName.isBlank()) {fieldValue} else {trainCenterName}*/

                mTvDateOfBirth.text = fieldValue
            } else if (fieldName.equals(keyCollege)) {
                mTvCollege.text = fieldValue
            } else if (fieldName.equals(keyDepartment)) {
                /*val trainLangID = fieldValue
                val trainLangName = if(!trainLangID.isBlank()) {db.getTrainLangNameFromId(trainLangID.toInt())} else {""}
                mTvCollege.text = if (trainLangName.isBlank()) {fieldValue} else {trainLangName}*/
                mTvDepartment.text = fieldValue
            } else if (fieldName.equals(keyCampus)) {
                mTvCampus.text = fieldValue
            } /*else if (fieldName.equals(keyProgram)) {
                mTvCampus.text = fieldValue
            } else if (fieldName.equals(keyDegreeType)) {
                mTvProgram.text = fieldValue
            } else if (fieldName.equals(keyAdmissionType)) {
                mTvDegreeType.text = fieldValue
            } else if (fieldName.equals(keyAdmissionTypeShort)) {
                mTvAdmissionType.text = fieldValue
            } else if (fieldName.equals(keyValidDateUntil)) {
                mTvValidUntil.text = fieldValue
            } else if (fieldName.equals(keyIssueDate)) {
                mTvIssueDate.text = fieldValue
            } else if (fieldName.equals(keyMealNumber)) {
                mTvMealNumber.text = fieldValue
            } else if (fieldName.equals(keyUniqueNo)) {
                mTvUniqueNo.text = fieldValue
            } else if (fieldName.equals(keyActive)) {
                mTvStatus.text = fieldValue
            } else if (fieldName.equals(keyCardNo)) {
                mTvUUIDNo.text = fieldValue
            }*/
            mTvUUIDNo.text = uuidNumber

            llProgram.visibility = View.GONE
            llDegreeType.visibility = View.GONE
            llAdmissionType.visibility = View.GONE
            llAdmissionTypeShort.visibility = View.GONE
            llValidUntil.visibility = View.GONE
            llIssueDate.visibility = View.GONE
            llMealNumber.visibility = View.GONE
            llUniqueNo.visibility = View.GONE
            //llStatus.visibility = View.GONE
            //chemTvUUIDNo.visibility = View.GONE

            viewProgram.visibility = View.GONE
            viewDegreeType.visibility = View.GONE
            viewAdmissionType.visibility = View.GONE
            viewAdmissionTypeShort.visibility = View.GONE
            viewValidUntil.visibility = View.GONE
            viewIssueDate.visibility = View.GONE
            viewMealNumber.visibility = View.GONE
            viewUniqueNo.visibility = View.GONE



            if (stu_id != null) {
                if (stu_id.isNotEmpty()) {
                    val stu_card_st: Int = db.getCardStatusFromStudentId(stu_id)
                    if (activity is AuthActivity) {
                        if (::changeCardStListener.isInitialized) {
                            changeCardStListener.onChangeCardStatus(activity, stu_card_st)
                            //(activity as AuthActivity).setCardStatus(requireContext(), stu_card_st)
                            var cardStStr: String? = null
                            if (stu_card_st == CARD_ST_ACTIVE) {
                                cardStStr = "Active"
                            } else if (stu_card_st == CARD_ST_REVOKED) {
                                cardStStr = "Revoked"
                            } else if (stu_card_st == CARD_ST_LOST) {
                                cardStStr = "Lost"
                            } else if (stu_card_st == CARD_ST_SUSPENDED) {
                                cardStStr = "Suspended"
                            } else if (stu_card_st == CARD_ST_EXPIRED) {
                                cardStStr = "Expired"
                            }

                            mTvStatus.text = cardStStr
                        }
                    }

                    val stu_img : String = db.getStudentImageFromStudentId(stu_id)
                    if (stu_img != null) {
                        if (::changeCardStListener.isInitialized) {
                            (activity as AuthActivity).setCardHolderImageIntoPellete(stu_img)

                        }
                    }
                }

            }
            //(activity as AuthActivity)!!.setCardStatus(licenseData!!.cardStatus.toInt())
        }
    }

    override fun onAuthApiDataReceived(cardData: StudentInfoModel.DataBean?) {
        val licenseData = cardData
        mTvStudentId.text = licenseData!!.studentID
        mTvFullName.text = licenseData.firstName + " " + licenseData.fatherName + " " + licenseData.grandFatherName
        mTvIssueDate.text = licenseData!!.issueDate
        //mTvExpiryDate.text = licenseData!!.validDateUntil
        mTvGender.text = licenseData!!.gender
        mTvDateOfBirth.text = licenseData!!.dateOfBirth
        mTvCollege.text = licenseData!!.college
        mTvDepartment.text = licenseData!!.department
        mTvCampus.text = licenseData!!.campus
        mTvProgram.text = licenseData!!.program
        mTvDegreeType.text = licenseData!!.degreeType
        mTvAdmissionType.text = licenseData!!.admissionType
        mTvAdmissionTypeShort.text = licenseData!!.admissionTypeShort
        mTvValidUntil.text = ""+licenseData!!.validDateUntil
        mTvMealNumber.text = licenseData!!.mealNumber
        mTvUniqueNo.text = ""+licenseData!!.uniqueNo
        val card_st: String = ""+licenseData!!.cardstatus
        if (card_st != null) {
            if (!card_st.isEmpty()) {
                var stu_card_st: Int = Integer.parseInt(card_st)
                var cardStStr: String? = null
                if (stu_card_st == CARD_ST_ACTIVE) {
                    cardStStr = "Active"
                } else if (stu_card_st == CARD_ST_REVOKED) {
                    cardStStr = "Revoked"
                } else if (stu_card_st == CARD_ST_LOST) {
                    cardStStr = "Lost"
                } else if (stu_card_st == CARD_ST_SUSPENDED) {
                    cardStStr = "Suspended"
                } else if (stu_card_st == CARD_ST_EXPIRED) {
                    cardStStr = "Expired"
                }

                mTvStatus.text = cardStStr

                /*if (::changeCardStListener.isInitialized) {
                    changeCardStListener.onChangeCardStatus(requireActivity(), stu_card_st)
                }*/
            }
        }
        //mTvStatus.visibility = View.GONE

        mTvUUIDNo.text = licenseData!!.cardNumber


        //mTvMealNumber.text = licenseData!!.isactive
        //mTvId.text = licenseData!!.id
        //mTvCardStatus.text = licenseData!!.cardstatus

        /*val subCatCode = licenseData!!.subCategory

        if (subCatCode.isNotBlank()) {
            var sbSubCatCode : StringBuffer = StringBuffer()
            if (subCatCode.contains(",")) {
                val sub_array = subCatCode.split("'")
                sbSubCatCode.append("'")
                sbSubCatCode.append(subCatCode.replace(",", "','"))
                sbSubCatCode.append("'")
            } else {
                sbSubCatCode.append("'" + subCatCode + "'")
            }
            var list: ArrayList<SubCategoryModel.DataBean> = ArrayList<SubCategoryModel.DataBean>()
            list = if (subCatCode.isNotBlank()) {db.getLicenseTypeFromSubCatCode(sbSubCatCode.toString())} else {list}
            if (list.size > 0) {
                for (subCatData: SubCategoryModel.DataBean in list) {
                    Log.d(TAG, "SubCatCode: " + subCatData.categoryCode)
                }
            }
        }

        val cardStCode = licenseData!!.cardStatus.toInt()
        var cardColor: Int = -1
        if (cardStCode == CARD_ST_ACTIVE) {
            cardColor = R.color.green
        } else if (cardStCode == CARD_ST_DEACTIVE) {
            cardColor = R.color.garkerGray
        } else if (cardStCode == CARD_ST_EXPIRED) {
            cardColor = R.color.yellow
        } else if (cardStCode == CARD_ST_WANTED) {
            cardColor = R.color.red
        }

        llStudentId.setBackgroundColor(cardColor)
        Log.d(TAG, ""+licenseData!!.cardStatus.toInt())
        (activity as AuthActivity)!!.setCardStatus(licenseData!!.cardStatus.toInt())*/
    }
}