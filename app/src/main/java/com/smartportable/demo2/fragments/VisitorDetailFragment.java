package com.smartportable.demo2.fragments;

import android.app.Dialog;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.smartportable.demo2.R;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.Utility;
import com.smartportable.demo2.models.VisitorInfoModel;
import com.smartportable.demo2.utils.KeysKt;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ananth on 10/10/2017.
 */

public class VisitorDetailFragment extends BottomSheetDialogFragment {
    VisitorInfoModel.DataBean visInfo;
    ProgressBar pbReaders;
    TextView tv_dialog_emp_reader_id;
    ListView lvAllowedReadersList;
    boolean forMemAccess = false;

    AppBarLayout app_bar_vis;
    CollapsingToolbarLayout collapsingToolbarLayout;

    public void setVisitorDetail (VisitorInfoModel.DataBean visInfo) {
        this.visInfo = visInfo;
    }

    public void isForMemberAccess(boolean forMemAccess) {
        this.forMemAccess = forMemAccess;
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        //Employee item = list_employee.get(position);
        String vis_reg_no = visInfo.getVisitorRegNo();
        String vis_first_name = visInfo.getVisitorFirstName();
        String vis_last_name = visInfo.getVisitorLasttName();
        String vis_comp_name = visInfo.getCompanyName();
        String vis_type = visInfo.getVisitorType();
        String vis_reason = visInfo.getVisitReason();
        String vis_phone_no = visInfo.getVisitorPhoneNo();
        String vis_email_id = visInfo.getVisitorEmailId();
        String vis_photo = visInfo.getVisitorPhoto();
        String vis_acc_level = visInfo.getVisitorAccessLevel();
        String vis_check_in = visInfo.getVisitorCheckInTime();
        String vis_check_out = visInfo.getVisitorCheckOutTime();
        String vis_card_no = visInfo.getVisitorCardNumber();
        int vis_card_status = Integer.parseInt(visInfo.getVisitorCardStatus());
        int vis_status = Integer.parseInt(visInfo.getVisitorStatus());


        View contentView = View.inflate(getActivity(), R.layout.frag_bsd_vis_detail, null);
        dialog.setContentView(contentView);


        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();

        View parent = (View) contentView.getParent();
        parent.setFitsSystemWindows(true);

        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(parent);
        contentView.measure(0, 0);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenHeight = displaymetrics.heightPixels;
        bottomSheetBehavior.setPeekHeight(screenHeight);

        if (params.getBehavior() instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior)params.getBehavior()).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        //params.height = screenHeight;
        parent.setLayoutParams(params);

        FloatingActionButton fab = (FloatingActionButton) contentView.findViewById(R.id.fabBSDVisDetail);
        fab.setVisibility(View.GONE);

        app_bar_vis = (AppBarLayout) contentView.findViewById(R.id.app_bar_vis);
        collapsingToolbarLayout = (CollapsingToolbarLayout) contentView.findViewById(R.id.vis_collapsing_toolbar_layout);
        collapsingToolbarLayout.setTitle(vis_reg_no + " - " + vis_first_name + " " + vis_last_name);
        collapsingToolbarLayout.setExpandedTitleColor(getActivity().getResources().getColor(android.R.color.transparent));

        collapsingToolbarLayout.setContentScrimColor(getActivity().getResources().getColor(R.color.colorPrimary));
        collapsingToolbarLayout.setStatusBarScrimColor(getActivity().getResources().getColor(R.color.colorPrimary));

        Toolbar toolBar = (Toolbar) collapsingToolbarLayout.findViewById(R.id.tbVisDetail);
        ImageView ivVisDetailClose = (ImageView) toolBar.findViewById(R.id.ivVisDetailClose);
        TextView tvVisDetailTitle = (TextView) toolBar.findViewById(R.id.tvVisfDetailTitle);
        if (vis_reg_no.length() >= 0) {
            tvVisDetailTitle.setText(vis_reg_no + " - " + vis_first_name + " " + vis_last_name);
        } else {
            tvVisDetailTitle.setText("");
        }
        ivVisDetailClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Vis Detail close click");
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });

        final CircleImageView image = (CircleImageView) contentView.findViewById(R.id.ivVisDetailVisPhoto);
        TextView tv_dialog_vis_reg_no = (TextView) contentView.findViewById(R.id.tvVisRegNo);
        TextView tv_dialog_vis_name = (TextView) contentView.findViewById(R.id.tvVisDetailVisName);
        TextView tv_dialog_vis_company_name = (TextView) contentView.findViewById(R.id.tv_dialog_vis_company_name);
        TextView tv_dialog_vis_type = (TextView) contentView.findViewById(R.id.tv_dialog_vis_type);
        TextView tv_dialog_vis_reason = (TextView) contentView.findViewById(R.id.tv_dialog_vis_reason);
        TextView tv_dialog_vis_ph_no = (TextView) contentView.findViewById(R.id.tv_dialog_vis_ph_no);
        TextView tv_dialog_vis_email_id = (TextView) contentView.findViewById(R.id.tv_dialog_vis_email);
        TextView tv_dialog_vis_acc_level = (TextView) contentView.findViewById(R.id.tv_dialog_vis_acc_level);
        TextView tv_dialog_vis_check_in_time = (TextView) contentView.findViewById(R.id.tv_dialog_vis_check_in_time);
        TextView tv_dialog_vis_check_out_time = (TextView) contentView.findViewById(R.id.tv_dialog_vis_check_out_time);
        TextView tv_dialog_vis_card_number = (TextView) contentView.findViewById(R.id.tv_dialog_vis_card_number);
        TextView tv_dialog_vis_card_status = (TextView) contentView.findViewById(R.id.tv_dialog_vis_card_status);
        TextView tv_dialog_vis_status = (TextView) contentView.findViewById(R.id.tv_dialog_vis_status);
        TextView tv_dialog_vis_reader_label = (TextView) contentView.findViewById(R.id.tv_dialog_vis_reader_label);



        lvAllowedReadersList = (ListView) contentView.findViewById(R.id.lvAllowedReadersList);

        //pbReaders = (ProgressBar) contentView.findViewById(R.id.pbVisReaders);

        tv_dialog_vis_name.setVisibility(View.VISIBLE);
        //tv_dialog_emp_loc_id.setVisibility(View.GONE);
        //tv_dialog_emp_reader_id.setVisibility(View.GONE);
        //pbReaders.setVisibility(View.GONE);
        if (forMemAccess) {
            //pbReaders.setVisibility(View.VISIBLE);
            tv_dialog_vis_reader_label.setVisibility(View.VISIBLE);
            //new getReadersList().execute(""+staff_id);
        } else {
            tv_dialog_vis_reader_label.setVisibility(View.GONE);
        }
        //tv_dialog_emp_datetime.setVisibility(View.GONE);

        if (vis_reg_no.length() >= 0 && vis_first_name != null && vis_last_name.length() > 0)
            //tv_dialog_staff_id.setText(staff_id);
            tv_dialog_vis_name.setText(vis_reg_no+ " - " + vis_first_name + " " + vis_last_name);
        if (vis_first_name != null && vis_first_name.length() > 0)
            tv_dialog_vis_name.setText(vis_reg_no + " - " + vis_first_name + " " + vis_last_name);



        tv_dialog_vis_company_name.setText("Company Name : "+vis_comp_name);
        tv_dialog_vis_type.setText("Visitor Type: "+vis_type);
        tv_dialog_vis_reason.setText("Visit Reason: "+vis_reason);
        tv_dialog_vis_ph_no.setText("Phone No. : "+vis_phone_no);
        tv_dialog_vis_email_id.setText("ID No. : "+vis_email_id);
        tv_dialog_vis_acc_level.setText("Access Level : "+ vis_acc_level);
        tv_dialog_vis_check_in_time.setText("Check In Time : "+ vis_check_in);
        tv_dialog_vis_check_out_time.setText("Check Out Time : "+ vis_check_out);
        tv_dialog_vis_card_number.setText("Card Number : "+vis_card_no);


        int visSTColor = R.color.colorPrimary;
        String visStStr = "";
        if (vis_status == KeysKt.CARD_ST_ACTIVE) {
            visStStr = "Active";
            visSTColor = R.color.colorGreen;
        } else if (vis_status == KeysKt.CARD_ST_REVOKED) {
            visStStr = "Revoked";
            visSTColor = R.color.color_red_fr;
        } else if (vis_status == KeysKt.CARD_ST_LOST) {
            visStStr = "Lost";
            visSTColor = R.color.color_red_fr;
        } else if (vis_status == KeysKt.CARD_ST_SUSPENDED) {
            visStStr = "Suspended";
            visSTColor = R.color.color_red_fr;
        } else if (vis_status == KeysKt.CARD_ST_EXPIRED) {
            visStStr = "Expired";
            visSTColor = R.color.color_red_fr;
        }

        tv_dialog_vis_status.setText("Visitor Status : "+ visStStr);
        tv_dialog_vis_status.setTextColor(ContextCompat.getColor(getActivity(), visSTColor));

        int visCardSTColor = R.color.colorPrimary;
        String cardStStr = "";
        if (vis_card_status == KeysKt.VIS_CARD_ST_ACTIVE) {
            //cardColor = R.color.colorGreen;
            cardStStr = "Active";
            visCardSTColor = R.color.colorGreen;
            //ardStIcon = R.mipmap.ic_action_done;
        } else if (vis_card_status == KeysKt.VIS_CARD_ST_IN_USE) {
            //cardColor = R.color.colorGreen;
            cardStStr = "In Used";
            visCardSTColor = R.color.colorGreen;
            //cardStIcon = R.mipmap.ic_action_done;
        }  else if (vis_card_status == KeysKt.VIS_CARD_ST_EXPIRED) {
            //cardColor = R.color.colorRed;
            cardStStr = "Expired";
            visCardSTColor = R.color.color_red_fr;
            //cardStIcon = R.mipmap.ic_close_app;
        } else if (vis_card_status == KeysKt.VIS_CARD_ST_LOST) {
            //cardColor = R.color.colorBlack;
            cardStStr = "Lost";
            visCardSTColor = R.color.colorBlack;
            //cardStIcon = R.mipmap.ic_close_app;
        }
        tv_dialog_vis_card_status.setText("Card Status : "+cardStStr);
        tv_dialog_vis_card_status.setTextColor(ContextCompat.getColor(getActivity(), visCardSTColor));

        app_bar_vis.setBackgroundColor(ContextCompat.getColor(getActivity(), visCardSTColor));
        collapsingToolbarLayout.setStatusBarScrimColor(ContextCompat.getColor(getActivity(), visCardSTColor));
        collapsingToolbarLayout.setBackgroundColor(ContextCompat.getColor(getActivity(), visCardSTColor));
        collapsingToolbarLayout.setContentScrimColor(ContextCompat.getColor(getActivity(), visCardSTColor));

        /*String baseUrl = SharedPref.INSTANCE.getStringValue(getActivity(), KeysKt.keyBaseUrl, "");

        if(staff_emp_photo!=null && !staff_emp_photo.isEmpty())
        {
            String fName_ = staff_emp_photo.substring(staff_emp_photo.lastIndexOf('/') + 1);

            String path = Environment.getExternalStorageDirectory().toString() + "/ImageSave_play/StaffImage/"+fName_;

            File imgFile = new File(path);

            if(imgFile.exists())
            {
                Glide.with(getActivity())
                        .load(imgFile.getAbsolutePath())
                        .error(R.mipmap.ic_profile_pic)
                        .into(image);
            }
            else
            {
                Log.e("Transform", "Transform----image--not---present-StaffInfoAdapter-detail"+baseUrl+staff_emp_photo);

                if(isNetworkAvailable(image.getContext()))
                    ImageToLocal(image.getContext(),baseUrl+staff_emp_photo,image,"StaffImage");
                else
                    image.setImageResource(R.mipmap.ic_profile_pic);
            }
        }
        else
            image.setImageResource(R.mipmap.ic_profile_pic);*/

        if (!vis_photo.isEmpty()) {
            //var charStr = visPhoto!!.substring(0, 1)
            //visPhoto = visPhoto!!.removePrefix("~")

            Glide.with(getActivity())
                    .load(vis_photo)
                    .placeholder(android.R.drawable.ic_menu_report_image)
                    .error(android.R.drawable.ic_menu_report_image)
                    .into(image);
        }

    }

    private class getReadersList extends AsyncTask<String , Void, ArrayList<String>> {
        DatabaseHandler db;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //pbReaders.setVisibility(View.VISIBLE);
            db = new DatabaseHandler(getActivity());

        }

        @Override
        protected ArrayList<String> doInBackground(String... params) {
            ArrayList<String> list_readers = db.getAllReadersOfStaffAccList(params[0]);
            return list_readers;
        }

        @Override
        protected void onPostExecute(ArrayList<String> list) {
            super.onPostExecute(list);
            //pbReaders.setVisibility(View.GONE);
            //tv_dialog_emp_reader_id.setVisibility(View.GONE);
            if (list != null) {
                if (list.size() > 0) {
                    /*for (int i=0; i<list.size();i++) {
                        String reader_ids = tv_dialog_emp_reader_id.getText().toString();
                        if (i==0) {
                            tv_dialog_emp_reader_id.setText(list.get(i));
                        }else {
                            tv_dialog_emp_reader_id.setText(reader_ids+" , "+list.get(i));
                        }
                    }*/

                    ArrayAdapter<String> adapter =  new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, list);
                    lvAllowedReadersList.setAdapter(adapter);
                    Utility.setListViewHeightBasedOnChildren(lvAllowedReadersList);
                }
            }
            if (db != null)
                db.close();
        }
    }

    public void setCardStatus (int cardStCode) {
        int cardColor = -1;
        if (cardStCode == KeysKt.CARD_ST_ACTIVE) {
            cardColor = R.color.green;
        } else if (cardStCode == KeysKt.CARD_ST_REVOKED) {
            cardColor = android.R.color.darker_gray;
        } else if (cardStCode == KeysKt.CARD_ST_LOST) {
            cardColor = R.color.colorBlack;
        } else if (cardStCode == KeysKt.CARD_ST_SUSPENDED) {
            cardColor = R.color.colorYellow;
        } else if (cardStCode == KeysKt.CARD_ST_EXPIRED) {
            cardColor = R.color.red;
        }

        app_bar_vis.setBackgroundColor(ContextCompat.getColor(getActivity(), cardColor));
        collapsingToolbarLayout.setStatusBarScrimColor(ContextCompat.getColor(getActivity(), cardColor));
        collapsingToolbarLayout.setBackgroundColor(ContextCompat.getColor(getActivity(), cardColor));
        collapsingToolbarLayout.setContentScrimColor(ContextCompat.getColor(getActivity(), cardColor));
        collapsingToolbarLayout.setStatusBarScrimColor(ContextCompat.getColor( getActivity(), cardColor));
    }

}
