package com.smartportable.demo2.fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fineapp.android.retrofitClient.UrlEndPointsKt;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.smartportable.demo2.R;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.SaveImages;
import com.smartportable.demo2.helper.Utility;
import com.smartportable.demo2.models.StaffInfoModel;
import com.smartportable.demo2.utils.KeysKt;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ananth on 10/10/2017.
 */

public class StaffDetailFragment extends BottomSheetDialogFragment {
    StaffInfoModel.DataBean staffInfo;
    ProgressBar pbReaders;
    TextView tv_dialog_emp_reader_id;
    ListView lvAllowedReadersList;
    boolean forMemAccess = false;

    AppBarLayout app_bar_staff;
    CollapsingToolbarLayout collapsingToolbarLayout;

    public void setStaffDetail (StaffInfoModel.DataBean staffInfo) {
        this.staffInfo = staffInfo;
    }

    public void isForMemberAccess(boolean forMemAccess) {
        this.forMemAccess = forMemAccess;
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        //Employee item = list_employee.get(position);
        String staff_id = staffInfo.getStaffID();
        String staff_appno = staffInfo.getAppNo();
        String staff_slno = staffInfo.getSlNo();
        String staff_uid = staffInfo.getUID();
        String staff_fullname = staffInfo.getFullName();
        String staff_gender = staffInfo.getGender();
        String staff_dob = staffInfo.getDob();
        String staff_dept = staffInfo.getDepartment();
        String staff_jobtitle = staffInfo.getJobTitle();
        String staff_emp_photo = staffInfo.getEmpPhoto();
        String staff_signature = staffInfo.getSignature();
        String staff_address = staffInfo.getAddress();
        String staff_idno = staffInfo.getIdNo();
        String staff_issue_date = staffInfo.getIssueDate();
        int staff_cardStatus = Integer.parseInt(staffInfo.getCardstatus());
        String staff_cardNo = staffInfo.getCardNumber();
        String staff_emailId = staffInfo.getEmailId();
        String staff_isActive = staffInfo.getIsactive();
        String staff_password = staffInfo.getPassword();
        //String staff_idnumber = visInfo.getIdNumber();

        String stu_image = UrlEndPointsKt.imageBaseUrl+""+staffInfo.getEmpPhoto();

        Bitmap staff_image_bmp = new SaveImages().loadImageFromStorage(staff_emp_photo);



        View contentView = View.inflate(getActivity(), R.layout.frag_bsd_staff_detail, null);
        dialog.setContentView(contentView);


        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        /*CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }*/

        View parent = (View) contentView.getParent();
        parent.setFitsSystemWindows(true);

        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(parent);
        contentView.measure(0, 0);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenHeight = displaymetrics.heightPixels;
        bottomSheetBehavior.setPeekHeight(screenHeight);

        if (params.getBehavior() instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior)params.getBehavior()).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        //params.height = screenHeight;
        parent.setLayoutParams(params);

        FloatingActionButton fab = (FloatingActionButton) contentView.findViewById(R.id.fabBSDSTAFFDetail);
        fab.setVisibility(View.GONE);

        app_bar_staff = (AppBarLayout) contentView.findViewById(R.id.app_bar_staff);
        collapsingToolbarLayout = (CollapsingToolbarLayout) contentView.findViewById(R.id.staff_collapsing_toolbar_layout);
        collapsingToolbarLayout.setTitle(staff_id + " - " + staff_fullname);
        collapsingToolbarLayout.setExpandedTitleColor(getActivity().getResources().getColor(android.R.color.transparent));

        collapsingToolbarLayout.setContentScrimColor(getActivity().getResources().getColor(R.color.colorPrimary));
        collapsingToolbarLayout.setStatusBarScrimColor(getActivity().getResources().getColor(R.color.colorPrimary));

        Toolbar toolBar = (Toolbar) collapsingToolbarLayout.findViewById(R.id.tbStuDetail);
        ImageView ivDetailClose = (ImageView) toolBar.findViewById(R.id.ivDetailClose);
        TextView tvDetailTitle = (TextView) toolBar.findViewById(R.id.tvStaffDetailTitle);
        if (staff_id.length() >= 0) {
            tvDetailTitle.setText(staff_id + " - " + staff_fullname);
        } else {
            tvDetailTitle.setText("");
        }
        ivDetailClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Emp Detail close click");
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });

        final CircleImageView image = (CircleImageView) contentView.findViewById(R.id.ivStaffDetailStPhoto);
        TextView tv_dialog_staff_id = (TextView) contentView.findViewById(R.id.tvStaffDetailStId);
        TextView tv_dialog_staff_name = (TextView) contentView.findViewById(R.id.tvStaffDetailStName);
        TextView tv_dialog_staff_appno = (TextView) contentView.findViewById(R.id.tv_dialog_staff_appno);
        TextView tv_dialog_staff_slno = (TextView) contentView.findViewById(R.id.tv_dialog_staff_slno);
        TextView tv_dialog_staff_uid = (TextView) contentView.findViewById(R.id.tv_dialog_staff_uid);
        TextView tv_dialog_staff_gender = (TextView) contentView.findViewById(R.id.tv_dialog_staff_gender);
        TextView tv_dialog_staff_dob = (TextView) contentView.findViewById(R.id.tv_dialog_staff_dob);
        TextView tv_dialog_staff_job_title = (TextView) contentView.findViewById(R.id.tv_dialog_staff_job_title);
        TextView tv_dialog_staff_dept = (TextView) contentView.findViewById(R.id.tv_dialog_staff_dept);
        //TextView tv_dialog_staff_emp_photo = (TextView) contentView.findViewById(R.id.tv_dialog_staff_emp_photo);
        //TextView tv_dialog_staff_sig = (TextView) contentView.findViewById(R.id.tv_dialog_staff_sig);
        TextView tv_dialog_staff_add = (TextView) contentView.findViewById(R.id.tv_dialog_staff_add);
        TextView tv_dialog_staff_idno = (TextView) contentView.findViewById(R.id.tv_dialog_staff_idno);
        TextView tv_dialog_staff_issuedate = (TextView) contentView.findViewById(R.id.tv_dialog_staff_issuedate);
        TextView tv_dialog_staff_card_st = (TextView) contentView.findViewById(R.id.tv_dialog_staff_cardStatus);
        TextView tv_dialog_staff_card_no = (TextView) contentView.findViewById(R.id.tv_dialog_staff_cardNumber);
        TextView tv_dialog_staff_emailid = (TextView) contentView.findViewById(R.id.tv_dialog_staff_emailid);
        TextView tv_dialog_staff_idNumber = (TextView) contentView.findViewById(R.id.tv_dialog_staff_idNumber);
        TextView tv_dialog_staff_reader_label = (TextView) contentView.findViewById(R.id.tv_dialog_staff_reader_label);



        lvAllowedReadersList = (ListView) contentView.findViewById(R.id.lvAllowedReadersList);

        pbReaders = (ProgressBar) contentView.findViewById(R.id.pbStaffReaders);

        tv_dialog_staff_name.setVisibility(View.VISIBLE);
        //tv_dialog_emp_loc_id.setVisibility(View.GONE);
        //tv_dialog_emp_reader_id.setVisibility(View.GONE);
        pbReaders.setVisibility(View.GONE);
        if (forMemAccess) {
            //pbReaders.setVisibility(View.VISIBLE);
            tv_dialog_staff_reader_label.setVisibility(View.VISIBLE);
            //new getReadersList().execute(""+staff_id);
            new getAGsList().execute(""+staff_id);
        } else {
            tv_dialog_staff_reader_label.setVisibility(View.GONE);
        }
        //tv_dialog_emp_datetime.setVisibility(View.GONE);

        if (staff_id.length() >= 0 && staff_fullname != null && staff_fullname.length() > 0)
            //tv_dialog_staff_id.setText(staff_id);
            tv_dialog_staff_name.setText(staff_id + " - " + staff_fullname);
        if (staff_fullname != null && staff_fullname.length() > 0)
            tv_dialog_staff_name.setText(staff_id + " - " + staff_fullname);

        if (staff_gender != null) {
            if (staff_gender.equals("M")) {
                tv_dialog_staff_gender.setText("Gender : Male");
            } else {
                tv_dialog_staff_gender.setText("Gender : Female");
            }
        }


        if (staff_dob != null) {
            SimpleDateFormat output_sdf = new SimpleDateFormat("dd MMM, yyyy", Locale.US);
            SimpleDateFormat input_sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.US);
            try {
                Date date1 = input_sdf.parse(staff_dob);
                String dob = output_sdf.format(date1);
                tv_dialog_staff_dob.setText("Date of birth : " + dob);
            } catch (ParseException ex) {
                ex.printStackTrace();
                tv_dialog_staff_dob.setText("Date of birth : ");
            }
        }

        tv_dialog_staff_appno.setText("Application No.: "+staff_appno);
        tv_dialog_staff_appno.setVisibility(View.GONE);
        tv_dialog_staff_slno.setText("SL No.: "+staff_slno);
        tv_dialog_staff_slno.setVisibility(View.GONE);
        tv_dialog_staff_uid.setText("UID: "+staff_uid);
        tv_dialog_staff_uid.setVisibility(View.GONE);
        if (staff_gender!=null) {
            String gender = staff_gender.trim().equals("M") ? "Male" : "Female";
            tv_dialog_staff_gender.setText("Gender : " + gender);
        }
        tv_dialog_staff_dob.setText("DOB : "+staff_dob);
        tv_dialog_staff_dept.setText("Department: "+staff_dept);
        tv_dialog_staff_job_title.setText("Job Title: "+staff_jobtitle);
        tv_dialog_staff_add.setText("Address : "+staff_address);
        tv_dialog_staff_add.setVisibility(View.GONE);
        tv_dialog_staff_idno.setText("ID No. : "+staff_idno);
        tv_dialog_staff_idno.setVisibility(View.GONE);
        tv_dialog_staff_issuedate.setText("Issue Date : "+ staff_issue_date);
        tv_dialog_staff_issuedate.setVisibility(View.VISIBLE);;
        String cardStStr = "";
        if (staff_cardStatus == KeysKt.CARD_ST_ACTIVE) {
            cardStStr = "Active";
        } else if (staff_cardStatus == KeysKt.CARD_ST_REVOKED) {
            cardStStr = "Revoked";
        } else if (staff_cardStatus == KeysKt.CARD_ST_LOST) {
            cardStStr = "Lost";
        } else if (staff_cardStatus == KeysKt.CARD_ST_SUSPENDED) {
            cardStStr = "Suspended";
        } else if (staff_cardStatus == KeysKt.CARD_ST_EXPIRED) {
            cardStStr = "Expired";
        }

        tv_dialog_staff_card_st.setText("Card Status : "+ cardStStr);
        tv_dialog_staff_card_no.setText("Card Number : "+staff_cardNo);
        tv_dialog_staff_emailid.setText("Email Id : "+staff_emailId);
        tv_dialog_staff_emailid.setVisibility(View.GONE);
        //tv_dialog_staff_idNumber.setText("ID Number  : "+ tv_dialog_staff_idNumber);


        setCardStatus(staff_cardStatus);

        /*String baseUrl = SharedPref.INSTANCE.getStringValue(getActivity(), KeysKt.keyBaseUrl, "");

        if(staff_emp_photo!=null && !staff_emp_photo.isEmpty())
        {
            String fName_ = staff_emp_photo.substring(staff_emp_photo.lastIndexOf('/') + 1);

            String path = Environment.getExternalStorageDirectory().toString() + "/ImageSave_play/StaffImage/"+fName_;

            File imgFile = new File(path);

            if(imgFile.exists())
            {
                Glide.with(getActivity())
                        .load(imgFile.getAbsolutePath())
                        .error(R.mipmap.ic_profile_pic)
                        .into(image);
            }
            else
            {
                Log.e("Transform", "Transform----image--not---present-StaffInfoAdapter-detail"+baseUrl+staff_emp_photo);

                if(isNetworkAvailable(image.getContext()))
                    ImageToLocal(image.getContext(),baseUrl+staff_emp_photo,image,"StaffImage");
                else
                    image.setImageResource(R.mipmap.ic_profile_pic);
            }
        }
        else
            image.setImageResource(R.mipmap.ic_profile_pic);*/

        Glide
                .with(getActivity())
                .asBitmap()
                .load(staff_image_bmp)
                .centerCrop()
                .dontAnimate()
                .error(R.mipmap.ic_profile_pic)
                .into(image);

    }

    private class getReadersList extends AsyncTask<String , Void, ArrayList<String>> {
        DatabaseHandler db;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbReaders.setVisibility(View.VISIBLE);
            db = new DatabaseHandler(getActivity());

        }

        @Override
        protected ArrayList<String> doInBackground(String... params) {
            ArrayList<String> list_readers = db.getAllReadersOfStaffAccList(params[0]);
            return list_readers;
        }

        @Override
        protected void onPostExecute(ArrayList<String> list) {
            super.onPostExecute(list);
            pbReaders.setVisibility(View.GONE);
            //tv_dialog_emp_reader_id.setVisibility(View.GONE);
            if (list != null) {
                if (list.size() > 0) {
                    /*for (int i=0; i<list.size();i++) {
                        String reader_ids = tv_dialog_emp_reader_id.getText().toString();
                        if (i==0) {
                            tv_dialog_emp_reader_id.setText(list.get(i));
                        }else {
                            tv_dialog_emp_reader_id.setText(reader_ids+" , "+list.get(i));
                        }
                    }*/

                    ArrayAdapter<String> adapter =  new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, list);
                    lvAllowedReadersList.setAdapter(adapter);
                    Utility.setListViewHeightBasedOnChildren(lvAllowedReadersList);
                }
            }
            if (db != null)
                db.close();
        }
    }


    private class getAGsList extends AsyncTask<String , Void, ArrayList<String>> {
        DatabaseHandler db;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbReaders.setVisibility(View.VISIBLE);
            db = new DatabaseHandler(getActivity());

        }

        @Override
        protected ArrayList<String> doInBackground(String... params) {
            ArrayList<String> list_readers = db.getAllAGsOfStaffAccList(params[0]);
            return list_readers;
        }

        @Override
        protected void onPostExecute(ArrayList<String> list) {
            super.onPostExecute(list);
            pbReaders.setVisibility(View.GONE);
            //tv_dialog_emp_reader_id.setVisibility(View.GONE);
            if (list != null) {
                if (list.size() > 0) {
                    /*for (int i=0; i<list.size();i++) {
                        String reader_ids = tv_dialog_emp_reader_id.getText().toString();
                        if (i==0) {
                            tv_dialog_emp_reader_id.setText(list.get(i));
                        }else {
                            tv_dialog_emp_reader_id.setText(reader_ids+" , "+list.get(i));
                        }
                    }*/

                    ArrayAdapter<String> adapter =  new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, list);
                    lvAllowedReadersList.setAdapter(adapter);
                    Utility.setListViewHeightBasedOnChildren(lvAllowedReadersList);
                }
            }
            if (db != null)
                db.close();
        }
    }

    public void setCardStatus (int cardStCode) {
        int cardColor = -1;
        if (cardStCode == KeysKt.CARD_ST_ACTIVE) {
            cardColor = R.color.green;
        } else if (cardStCode == KeysKt.CARD_ST_REVOKED) {
            cardColor = android.R.color.darker_gray;
        } else if (cardStCode == KeysKt.CARD_ST_LOST) {
            cardColor = R.color.colorBlack;
        } else if (cardStCode == KeysKt.CARD_ST_SUSPENDED) {
            cardColor = R.color.colorYellow;
        } else if (cardStCode == KeysKt.CARD_ST_EXPIRED) {
            cardColor = R.color.red;
        }

        app_bar_staff.setBackgroundColor(ContextCompat.getColor(getActivity(), cardColor));
        collapsingToolbarLayout.setStatusBarScrimColor(ContextCompat.getColor(getActivity(), cardColor));
        collapsingToolbarLayout.setBackgroundColor(ContextCompat.getColor(getActivity(), cardColor));
        collapsingToolbarLayout.setContentScrimColor(ContextCompat.getColor(getActivity(), cardColor));
        collapsingToolbarLayout.setStatusBarScrimColor(ContextCompat.getColor( getActivity(), cardColor));
    }

}
