package com.smartportable.demo2.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.smartportable.demo2.R
import com.smartportable.demo2.activities.StaffAuthActivity
import com.smartportable.demo2.db.DatabaseHandler
import com.smartportable.demo2.interfaces.ChangeCardStatus
import com.smartportable.demo2.listeners.OnStaffAuthDataReceived
import com.smartportable.demo2.models.MifareSettingModel
import com.smartportable.demo2.models.StaffInfoModel
import com.smartportable.demo2.utils.*


class StaffInfoFragment : Fragment() , View.OnClickListener, OnStaffAuthDataReceived {

    val TAG : String = StaffInfoFragment:: class.java.simpleName
    lateinit var mTvStaffId : TextView
    lateinit var mTvAppNo : TextView
    lateinit var mTvSlNo : TextView
    lateinit var mTvUID : TextView
    lateinit var mTvFullName : TextView
    lateinit var mTvGender : TextView
    lateinit var mTvDOB : TextView
    lateinit var mTvDepartment : TextView
    lateinit var mTvJobTitle : TextView
    lateinit var mTvEmpPhoto : TextView
    lateinit var mTvSignature : TextView
    lateinit var mTvAddress : TextView
    lateinit var mTvIdNo : TextView
    lateinit var mTvIssueDate : TextView
    lateinit var mTvCardStatus : TextView
    lateinit var mTvCardNo : TextView
    lateinit var mTvEmailId : TextView
    lateinit var mTvIsActive : TextView
    lateinit var mTvPassword : TextView
    lateinit var mtvId_Number : TextView

    lateinit var llStaffId : LinearLayout
    lateinit var llAppNo : LinearLayout
    lateinit var llSlNo : LinearLayout
    lateinit var llUID : LinearLayout
    lateinit var llFullName : LinearLayout
    lateinit var llGender : LinearLayout
    lateinit var llDOB : LinearLayout
    lateinit var llDepartment : LinearLayout
    lateinit var llJobTitle : LinearLayout
    lateinit var llEmpPhoto : LinearLayout
    lateinit var llSignature : LinearLayout
    lateinit var llAddress : LinearLayout
    lateinit var llIdNo : LinearLayout
    lateinit var llIssueDate : LinearLayout
    lateinit var llCardStatus : LinearLayout
    lateinit var llCardNo : LinearLayout
    lateinit var llEmailId : LinearLayout
    lateinit var llIsActive : LinearLayout

    lateinit var viewStaffId : View
    lateinit var viewAppNo : View
    lateinit var viewSlNo : View
    lateinit var viewUID : View
    lateinit var viewFullName : View
    lateinit var viewGender : View
    lateinit var viewDOB : View
    lateinit var viewDepartment : View
    lateinit var viewJobTitle : View
    lateinit var viewEmpPhoto : View
    lateinit var viewSignature : View
    lateinit var viewAddress : View
    lateinit var viewIdNo : View
    lateinit var viewIssueDate : View
    lateinit var viewCardStatus : View
    lateinit var viewCardNo : View
    lateinit var viewEmailId : View
    lateinit var viewIsActive : View

    lateinit var changeCardStListener : ChangeCardStatus

    lateinit var db: DatabaseHandler

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_staff_info,container,false)

        initView(view)
        return view
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        changeCardStListener = activity as StaffAuthActivity
    }

    private fun initView (view : View) {

        var mActivity = activity as StaffAuthActivity
        mActivity.setStaffAuthDataReceivedListenerLI(this)

        llStaffId = view.findViewById(R.id.llStaffId) as LinearLayout
        llAppNo = view.findViewById(R.id.llAppNo) as LinearLayout
        llSlNo = view.findViewById(R.id.llSlNo) as LinearLayout
        llUID = view.findViewById(R.id.llUID) as LinearLayout
        llFullName = view.findViewById(R.id.llFullName) as LinearLayout
        llGender = view.findViewById(R.id.llGender) as LinearLayout
        llDOB = view.findViewById(R.id.llDOB) as LinearLayout
        llDepartment = view.findViewById(R.id.llDepartment) as LinearLayout
        llJobTitle = view.findViewById(R.id.llJobTitle) as LinearLayout
        llAddress = view.findViewById(R.id.llAddress) as LinearLayout
        llIdNo = view.findViewById(R.id.llIdNo) as LinearLayout
        llIssueDate = view.findViewById(R.id.llIssueDate) as LinearLayout
        llCardStatus = view.findViewById(R.id.llCardStatus) as LinearLayout
        llCardNo = view.findViewById(R.id.llCardNo) as LinearLayout
        llEmailId = view.findViewById(R.id.llEmailId) as LinearLayout
        llIsActive = view.findViewById(R.id.llIsActive) as LinearLayout


        viewStaffId = view.findViewById(R.id.viewStaffId) as View
        viewAppNo = view.findViewById(R.id.viewAppNo) as View
        viewSlNo = view.findViewById(R.id.viewSlNo) as View
        viewUID = view.findViewById(R.id.viewUID) as View
        viewFullName = view.findViewById(R.id.viewFullName) as View
        viewGender = view.findViewById(R.id.viewGender) as View
        viewDOB = view.findViewById(R.id.viewDOB) as View
        viewDepartment = view.findViewById(R.id.viewDepartment) as View
        viewJobTitle = view.findViewById(R.id.viewJobTitle) as View
        viewAddress = view.findViewById(R.id.viewAddress) as View
        viewIdNo = view.findViewById(R.id.viewIdNo) as View
        viewIssueDate = view.findViewById(R.id.viewIssueDate) as View
        viewCardStatus = view.findViewById(R.id.viewCardStatus) as View
        viewCardNo = view.findViewById(R.id.viewCardNo) as View
        viewEmailId = view.findViewById(R.id.viewEmailId) as View
        //viewIsActive = view.findViewByid(R.id.viewIs)


        mTvStaffId = view.findViewById(R.id.mTvStaffId) as TextView
        mTvAppNo = view.findViewById(R.id.mTvAppNo) as TextView
        mTvSlNo = view.findViewById(R.id.mTvSlNo) as TextView
        mTvUID = view.findViewById(R.id.mTvUID) as TextView
        mTvFullName = view.findViewById(R.id.mTvFullName) as TextView
        mTvGender = view.findViewById(R.id.mTvGender) as TextView
        mTvDOB = view.findViewById(R.id.mTvDOB) as TextView
        mTvDepartment = view.findViewById(R.id.mTvDepartment) as TextView
        mTvJobTitle = view.findViewById(R.id.mTvJobTitle) as TextView
        mTvAddress = view.findViewById(R.id.mTvAddress) as TextView
        mTvIdNo = view.findViewById(R.id.mTvIdNo) as TextView
        mTvIssueDate = view.findViewById(R.id.mTvIssueDate) as TextView
        mTvCardStatus = view.findViewById(R.id.mTvCardStatus) as TextView
        mTvCardNo = view.findViewById(R.id.mTvCardNo) as TextView
        mTvEmailId = view.findViewById(R.id.mTvEmailId) as TextView
        mTvIsActive = view.findViewById(R.id.mTvIsActive) as TextView
        //mTvUUIDNo = view.findViewById(R.id.mTvUUIDNo) as TextView

        db = DatabaseHandler(requireActivity())
    }
    override fun onClick(v: View?) {


    }


        override fun onStaffAuthDataReceived(list: ArrayList<MifareSettingModel>, uuidNumber : String ) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        for (i in list) {
            var staff_id : String ?= null
            val fieldName = i.fieldName
            val fieldValue = i.fieldValue
            if (fieldName.equals(keyStaffCode)) {
                mTvStaffId.text = fieldValue
                staff_id = fieldValue
            } else if (fieldName.equals(keyStaffFullName)) {
                mTvFullName.text = fieldValue
            } else if (fieldName.equals(keyStaffSex)) {
                mTvGender.text = fieldValue
            } else if (fieldName.equals(keyStaffJobTitle)) {
                mTvJobTitle.text = fieldValue
            } else if (fieldName.equals(keyStaffDept)) {
                mTvDepartment.text = fieldValue
            } else if (fieldName.equals(keyStaffCollege)) {
                //mTvCollege
            }
            mTvCardNo.text = uuidNumber
            /*else if (fieldName.equals(keyCampus)) {
                mTvDepartment.text = fieldValue
            } else if (fieldName.equals(keyProgram)) {
                //mTvCampus.text = fieldValue
            } else if (fieldName.equals(keyDegreeType)) {
                //mTvProgram.text = fieldValue
            } else if (fieldName.equals(keyAdmissionType)) {
                //mTvDegreeType.text = fieldValue
            } else if (fieldName.equals(keyAdmissionTypeShort)) {
                //mTvAdmissionType.text = fieldValue
            } else if (fieldName.equals(keyValidDateUntil)) {
                //mTvValidUntil.text = fieldValue
            } else if (fieldName.equals(keyIssueDate)) {
                mTvIssueDate.text = fieldValue
            } else if (fieldName.equals(keyMealNumber)) {
                //mTvMealNumber.text = fieldValue
            } else if (fieldName.equals(keyUniqueNo)) {
                //mTvUniqueNo.text = fieldValue
            } else if (fieldName.equals(keyActive)) {
                //mTvStatus.text = fieldValue
            } else if (fieldName.equals(keyCardNo)) {
                //mTvUUIDNo.text = fieldValue
            }*/
            llAppNo.visibility = View.GONE
            llSlNo.visibility = View.GONE
            llUID.visibility = View.GONE
            llDOB.visibility = View.GONE
            llAddress.visibility = View.GONE
            llIdNo.visibility = View.GONE
            llIssueDate.visibility = View.GONE
            //llCardStatus.visibility = View.GONE
            llEmailId.visibility = View.GONE
            llIsActive.visibility = View.GONE

            viewAppNo.visibility = View.GONE
            viewSlNo.visibility = View.GONE
            viewUID.visibility = View.GONE
            viewDOB.visibility = View.GONE
            viewAddress.visibility = View.GONE
            viewIdNo.visibility = View.GONE
            viewIssueDate.visibility = View.GONE
            //viewCardStatus.visibility = View.GONE
            viewEmailId.visibility = View.GONE


            //(activity as AuthActivity)!!.setCardStatus(licenseData!!.cardStatus.toInt())


            if (staff_id != null) {
                if (staff_id.isNotEmpty()) {
                    val staff_card_st: Int = db.getCardStatusFromStaffId(staff_id)
                    if (staff_card_st != null) {
                        if (changeCardStListener != null) {
                            changeCardStListener.onChangeCardStatus(activity, staff_card_st)
                            var cardStStr: String? = null
                            if (staff_card_st == CARD_ST_ACTIVE) {
                                cardStStr = "Active"
                            } else if (staff_card_st == CARD_ST_REVOKED) {
                                cardStStr = "Revoked"
                            } else if (staff_card_st == CARD_ST_LOST) {
                                cardStStr = "Lost"
                            } else if (staff_card_st == CARD_ST_SUSPENDED) {
                                cardStStr = "Suspended"
                            } else if (staff_card_st == CARD_ST_EXPIRED) {
                                cardStStr = "Expired"
                            }

                            mTvCardStatus.text = cardStStr
                        }
                    }

                    val staff_image: String = db.getStaffImageFromStaffId(staff_id)
                    if (staff_image != null) {
                        if (!staff_image.isEmpty()) {
                            if (changeCardStListener != null) {
                                changeCardStListener.onCardHolderImageIntoPellete(staff_image)
                            }
                        }
                    }

                }
            }
        }
    }

    override fun onStaffAuthApiDataReceived(cardData: StaffInfoModel.DataBean?) {
        val licenseData = cardData
        mTvStaffId.text = licenseData!!.staffID
        mTvAppNo.text = licenseData!!.appNo
        mTvSlNo.text = licenseData!!.slNo
        mTvUID.text = licenseData!!.uID
        mTvFullName.text = licenseData.fullName
        mTvIssueDate.text = licenseData!!.issueDate
        mTvGender.text = licenseData!!.gender
        mTvDOB.text = licenseData!!.dob
        mTvDepartment.text = licenseData!!.department
        mTvJobTitle.text = licenseData!!.jobTitle
        val card_st: String = ""+licenseData!!.cardstatus
        if (card_st != null) {
            if (!card_st.isEmpty()) {
                var staff_card_st : Int = Integer.parseInt(card_st)
                var cardStStr: String? = null
                if (staff_card_st == CARD_ST_ACTIVE) {
                    cardStStr = "Active"
                } else if (staff_card_st == CARD_ST_REVOKED) {
                    cardStStr = "Revoked"
                } else if (staff_card_st == CARD_ST_LOST) {
                    cardStStr = "Lost"
                } else if (staff_card_st == CARD_ST_SUSPENDED) {
                    cardStStr = "Suspended"
                } else if (staff_card_st == CARD_ST_EXPIRED) {
                    cardStStr = "Expired"
                }

                mTvCardStatus.text = cardStStr
            }
        }
        //mTvVisAccessLevel.text = licenseData!!.empPhoto
        //mTvSignature.text = licenseData!!.signature
        mTvAddress.text = licenseData!!.address
        mTvIdNo.text = licenseData!!.idNo
        mTvIssueDate.text = licenseData!!.issueDate
        //mTvCardStatus.text = licenseData!!.cardstatus
        mTvCardNo.text = licenseData!!.cardNumber
        mTvEmailId.text = licenseData!!.emailId

        llAppNo.visibility = View.GONE
        llSlNo.visibility = View.GONE
        llUID.visibility = View.GONE
        llDOB.visibility = View.GONE
        llAddress.visibility = View.GONE
        llIdNo.visibility = View.GONE
        llIssueDate.visibility = View.GONE
        llEmailId.visibility = View.GONE
        llIsActive.visibility = View.GONE

        viewAppNo.visibility = View.GONE
        viewSlNo.visibility = View.GONE
        viewUID.visibility = View.GONE
        viewDOB.visibility = View.GONE
        viewAddress.visibility = View.GONE
        viewIdNo.visibility = View.GONE
        viewIssueDate.visibility = View.GONE
        //viewCardStatus.visibility = View.GONE
        viewEmailId.visibility = View.GONE
        //viewIsActive.visibility = View.GONE

    }
}