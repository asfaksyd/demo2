package com.smartportable.demo2.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smartportable.demo2.R;
import com.smartportable.demo2.MemberAccessMainActivity;
import com.smartportable.demo2.NFCMemberAccessMainActivity;
import com.smartportable.demo2.adapters.MemberAccessLogsAdapter;
import com.smartportable.demo2.api.URLCollections;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.MemberAccessLogs;
import com.smartportable.demo2.helper.PreferencesManager;
import com.smartportable.demo2.helper.RecyclerListSelectorModel;
import com.smartportable.demo2.interfaces.FragmentChangeInterface;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import xyz.danoz.recyclerviewfastscroller.vertical.VerticalRecyclerViewFastScroller;

/**
 * Created by Ananth on 3/19/2017.
 */

public class AccessedMembersList extends Fragment implements FragmentChangeInterface {

    RecyclerView rvMembersList;
    TextView tvAllowedMemberWarningMsg;
    VerticalRecyclerViewFastScroller fastScrollerMembersList;
    DatabaseHandler db;
    public MemberAccessLogsAdapter adapter;
    ArrayList<MemberAccessLogs> list_access_logs = new ArrayList<MemberAccessLogs>();
    ArrayList<RecyclerListSelectorModel> listBoolean = new ArrayList<RecyclerListSelectorModel>();
    RelativeLayout rlAccessedMemberCount;
    TextView tvAccessedMemberCount;
    Handler mHandler;
    Runnable mRunnable;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_members_list, null);

        rlAccessedMemberCount = (RelativeLayout) v.findViewById(R.id.rlAllEmpCount);
        tvAccessedMemberCount = (TextView) rlAccessedMemberCount.findViewById(R.id.tvAllEmpCount);

        tvAllowedMemberWarningMsg = (TextView) v.findViewById(R.id.tvAllowedMemberWarningMsg);
        rvMembersList = (RecyclerView) v.findViewById(R.id.rvMembersList);
        fastScrollerMembersList = (VerticalRecyclerViewFastScroller) v.findViewById(R.id.fastScrollerMembersList);

        rvMembersList.setHasFixedSize(true);
        rvMembersList.setLayoutManager(new LinearLayoutManager(getActivity()));
        // Connect the recycler to the scroller (to let the scroller scroll the list)
        fastScrollerMembersList.setRecyclerView(rvMembersList);
        tvAllowedMemberWarningMsg.setVisibility(View.GONE);

        rvMembersList.setOnTouchListener(mOnTouchListener);

        // Connect the scroller to the recycler (to let the recycler scroll the scroller's handle)
        rvMembersList.setOnScrollListener(fastScrollerMembersList.getOnScrollListener());

        return v;

    }

    public View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Log.d("AllowedMemberList", "onTouch"+event.getAction());
            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:
                    /*int count = employee_adapter.getItemCount();
                    if (count > 0) {*/
                    if (mHandler != null) {
                        mHandler.removeCallbacks(mRunnable);
                    }
                    rlAccessedMemberCount.setVisibility(View.VISIBLE);
                    //}
                    return false;
                case MotionEvent.ACTION_DOWN:
                    if (mHandler != null) {
                        mHandler.removeCallbacks(mRunnable);
                    }
                    rlAccessedMemberCount.setVisibility(View.VISIBLE);
                    break;
                case MotionEvent.ACTION_UP:
                    if (mHandler != null) {
                        mHandler.removeCallbacks(mRunnable);
                    }
                    mHandler = new Handler();
                    mRunnable = new Runnable() {
                        @Override
                        public void run() {
                            rlAccessedMemberCount.setVisibility(View.GONE);
                        }
                    };
                    mHandler.postDelayed(mRunnable, 3000);
            }
            return false;
        }
    };

    public class GetAccessedMembersList extends AsyncTask<Void, Void, Void> {
        ProgressDialog pDialog;
        Context mContext;
        int from = -1;

        public GetAccessedMembersList(Context mContext) {
            this.mContext = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*pDialog = new ProgressDialog(mContext, R.style.MyTheme);
            pDialog.setCancelable(false);
            pDialog.show();*/

            //((NFCMemberAccessMainActivity) getActivity()).setProgress(true);
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
            from = prefs.getInt(PreferencesManager.KEY_WHICH_ACTIVITY, -1);

            if (from != 0) {
                if (from == 1) {
                    ((NFCMemberAccessMainActivity) getActivity()).setProgress(true);
                } else if (from == 2) {
                    ((MemberAccessMainActivity) getActivity()).setProgress(true);
                }
            }

            if(list_access_logs.size() > 0)
                list_access_logs.clear();

        }

        @Override
        protected Void doInBackground(Void... params) {
            final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            int readerId = prefs.getInt(PreferencesManager.KEY_SET_READER_ID, -1);
            int sessionId = prefs.getInt(PreferencesManager.KEY_SET_SESSION_ID, -1);
            long sessionDate = prefs.getLong(PreferencesManager.KEY_SET_SESSION_DATE, 0);
            long sessionStartTime = prefs.getLong(PreferencesManager.KEY_SET_SESSION_START_TIME, 0);
            long sessionEndTime = prefs.getLong(PreferencesManager.KEY_SET_SESSION_END_TIME, 0);
            if (readerId != -1 && sessionId != -1) {
                list_access_logs = db.getAccessedMembersData(readerId, sessionId, sessionDate, sessionStartTime, sessionEndTime);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            /*if (pDialog.isShowing()) {
                pDialog.dismiss();
            }*/

            //((NFCMemberAccessMainActivity) getActivity()).setProgress(false);

            if (from != 0) {
                if (from == 1) {
                    ((NFCMemberAccessMainActivity) getActivity()).setProgress(false);
                } else if (from == 2) {
                    ((MemberAccessMainActivity) getActivity()).setProgress(false);
                }
            }

            if (list_access_logs.size() > 0) {
                for (int k = 0; k < list_access_logs.size(); k++) {
                    RecyclerListSelectorModel model = new RecyclerListSelectorModel();
                    model.setSelected(false);
                    listBoolean.add(model);
                }
                if (list_access_logs.size() > 15) {
                    fastScrollerMembersList.setVisibility(View.VISIBLE);
                }
                rvMembersList.setVisibility(View.VISIBLE);
                tvAllowedMemberWarningMsg.setVisibility(View.GONE);
                adapter = new MemberAccessLogsAdapter(getActivity(), list_access_logs, rvMembersList, listBoolean, getActivity().getSupportFragmentManager());
                adapter.isForMemberAccess(URLCollections.MEM_ACC_TYPE_ACCESSED);
                rvMembersList.setAdapter(adapter);

                rlAccessedMemberCount.setVisibility(View.VISIBLE);
                int count = list_access_logs.size();
                if (count > 0) {
                    tvAccessedMemberCount.setText(""+ URLCollections.getNumberFormat().format(count));
                }

                if (mHandler != null) {
                    mHandler.removeCallbacks(mRunnable);
                }

                mHandler = new Handler();
                mRunnable = new Runnable() {
                    @Override
                    public void run() {
                        rlAccessedMemberCount.setVisibility(View.GONE);
                    }
                };
                mHandler.postDelayed(mRunnable, 3000);

            } else {
                rlAccessedMemberCount.setVisibility(View.GONE);
                rvMembersList.setVisibility(View.GONE);
                fastScrollerMembersList.setVisibility(View.GONE);
                tvAllowedMemberWarningMsg.setVisibility(View.VISIBLE);
                tvAllowedMemberWarningMsg.setText(getActivity().getResources().getString(R.string.no_records_label));
                Log.d("Allowed Member Fragment", "No Members list found");
            }

        }
    }

    @Override
    public void fragmentBecameVisible(Activity act) {
        Log.d("Accessed Members List", "Fragment visible");
        tvAllowedMemberWarningMsg.setVisibility(View.GONE);
        fastScrollerMembersList.setVisibility(View.GONE);
        db = new DatabaseHandler(getActivity());
        new GetAccessedMembersList(getActivity()).execute();
    }

}
