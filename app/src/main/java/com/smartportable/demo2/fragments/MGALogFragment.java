package com.smartportable.demo2.fragments;

import android.app.Dialog;
import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.smartportable.demo2.FilterActivity;
import com.smartportable.demo2.R;
import com.smartportable.demo2.adapters.MGAMultipleLogsAdapter2;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.GlobalClass;
import com.smartportable.demo2.helper.Utility;
import com.smartportable.demo2.models.MainGateLogs;
import com.smartportable.demo2.models.Student;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by Ananth on 10/10/2017.
 */

public class MGALogFragment extends BottomSheetDialogFragment {
    Student student;
    int st_photo;
    boolean isAccGranted = false;
    ProgressBar pbReaders;
    TextView tv_dialog_emp_reader_id;
    ListView lvReadersList;
    boolean forMemAccess = false;

    public void setStudentDetail (Student student) {
        this.student = student;
    }

    public void isAccessGranted(boolean isAccGranted) {
        this.isAccGranted = isAccGranted;
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        //Employee item = list_employee.get(position);
        int st_id = student.getStId();
        String st_name = student.getStName();
        String st_card_no = student.getStCardno();
        String st_dept_name = student.getStDeptName();
        st_photo = student.getStPhoto();


        View contentView = View.inflate(getActivity(), R.layout.frag_bsd_st_detail, null);
        dialog.setContentView(contentView);


        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        /*CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }*/

        View parent = (View) contentView.getParent();
        parent.setFitsSystemWindows(true);

        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(parent);
        contentView.measure(0, 0);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenHeight = displaymetrics.heightPixels;
        bottomSheetBehavior.setPeekHeight(screenHeight);

        if (params.getBehavior() instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior)params.getBehavior()).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        //params.height = screenHeight;
        parent.setLayoutParams(params);

        FloatingActionButton fabBSDSTDetail = (FloatingActionButton) contentView.findViewById(R.id.fabBSDSTDetail);
        fabBSDSTDetail.setVisibility(View.VISIBLE);

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) contentView.findViewById(R.id.collapsing_toolbar_layout);
        collapsingToolbarLayout.setTitle(st_id + " - " +st_name);
        collapsingToolbarLayout.setExpandedTitleColor(getActivity().getResources().getColor(android.R.color.transparent));

        collapsingToolbarLayout.setContentScrimColor(getActivity().getResources().getColor(R.color.colorPrimary));
        collapsingToolbarLayout.setStatusBarScrimColor(getActivity().getResources().getColor(R.color.colorPrimary));

        Toolbar toolBar = (Toolbar) collapsingToolbarLayout.findViewById(R.id.tbEmpDetail);
        ImageView ivDetailClose = (ImageView) toolBar.findViewById(R.id.ivDetailClose);
        TextView tvDetailTitle = (TextView) toolBar.findViewById(R.id.tvDetailTitle);
        if (st_id >= 0) {
            tvDetailTitle.setText(st_id + " - " + st_name);
        } else {
            tvDetailTitle.setText(""+st_name);
        }
        ivDetailClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Emp Detail close click");
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });

        final CircleImageView ivStDetailStPhoto = (CircleImageView) contentView.findViewById(R.id.ivStDetailStPhoto);
        TextView tvStDetailStId = (TextView) contentView.findViewById(R.id.tvStDetailStId);
        TextView tvStDetailStName = (TextView) contentView.findViewById(R.id.tvStDetailStName);
        TextView tvStDetailStCardNo = (TextView) contentView.findViewById(R.id.tvStDetailStCardNo);
        TextView tvStDetailStDeptName = (TextView) contentView.findViewById(R.id.tvStDetailStDeptName);

        TextView tvStDetailStPunchHistoryLabel = (TextView) contentView.findViewById(R.id.tvStDetailStPunchHistoryLabel);
        tvStDetailStPunchHistoryLabel.setVisibility(View.GONE);

        RelativeLayout rlBSDAD = (RelativeLayout) contentView.findViewById(R.id.rlBSDAD);

        rlBSDAD.setVisibility(View.GONE);

        lvReadersList = (ListView) contentView.findViewById(R.id.lvReadersList);

        ivStDetailStPhoto.setBorderColor(getResources().getColor(R.color.color_light_green_ep));
        ivStDetailStPhoto.setBorderWidth(5);

        if (isAccGranted) {
            fabBSDSTDetail.setBackgroundTintList(ColorStateList.valueOf(getActivity().getResources().getColor(android.R.color.holo_green_dark)));
            fabBSDSTDetail.setImageResource(R.mipmap.ic_action_done);
            ivStDetailStPhoto.setBorderColor(getResources().getColor(R.color.color_light_green_ep));
        } else {
            fabBSDSTDetail.setBackgroundTintList(ColorStateList.valueOf(getActivity().getResources().getColor(android.R.color.holo_red_dark)));
            fabBSDSTDetail.setImageResource(R.mipmap.ic_close_app);
            ivStDetailStPhoto.setBorderColor(getResources().getColor(android.R.color.holo_red_dark));
        }

        ArrayList<String> list = new ArrayList<String>();
        list.add("Test");
        list.add("Test");
        list.add("Test");
        list.add("Test");
        list.add("Test");

        pbReaders = (ProgressBar) contentView.findViewById(R.id.pbReaders);

        tvStDetailStName.setVisibility(View.GONE);
        pbReaders.setVisibility(View.GONE);


        if (st_id >= 0 && st_name != null && st_name.length() > 0)
            tvStDetailStId.setText(st_id + " - " + st_name);
        if (st_name != null && st_name.length() > 0)
            tvStDetailStName.setText("" + st_name);
        if (st_card_no != null && st_card_no.length() > 0) {
            tvStDetailStCardNo.setVisibility(View.VISIBLE);
            tvStDetailStCardNo.setText("Card Number : " + st_card_no);
        } else {
            tvStDetailStCardNo.setVisibility(View.GONE);
            tvStDetailStCardNo.setText("");
        }

        ivStDetailStPhoto.setImageResource(st_photo);

        ivStDetailStPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(student.getStPhoto());
            }
        });

        final ArrayList<MainGateLogs> list2 = GlobalClass.getInstance().getMGAMultiLogs(getActivity(), 0);
        final ArrayList<MainGateLogs> list1 = new ArrayList<MainGateLogs>();
        if (st_card_no.toLowerCase().equals("6da40014")) {
            list1.add(list2.get(0));
        }else {
            list1.add(list2.get(1));
        }


        MGAMultipleLogsAdapter2 adapter = new MGAMultipleLogsAdapter2(getActivity(), list1);
        lvReadersList.setAdapter(adapter);
        //Utility.setListViewHeightBasedOnChildren(lvReadersList);
        FilterActivity.ListUtils.setDynamicHeight(lvReadersList);

        ivStDetailStPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageDialog(list1.get(0).getMglStPhoto());
            }
        });


    }

    private void showImageDialog (int resId) {
        final Dialog mSplashDialog = new Dialog(getActivity(), android.R.style.Theme_Material_NoActionBar_Fullscreen);
        //mSplashDialog.requestWindowFeature((int) window.FEATURE_NO_TITLE);
        mSplashDialog.setContentView(R.layout.dialog_profile_photo);
        //Drawable d = mContext.getResources().getDrawable(resId);
        //Bitmap b = ((BitmapDrawable) d ).getBitmap();
        ImageView ivPPFull = ((ImageView)mSplashDialog.findViewById(R.id.ivStPPFull));
        ImageView ivClosePPFull = (ImageView) mSplashDialog.findViewById(R.id.ivClosePPFull);

        ivPPFull.setImageResource(resId);//Bitmap(b);

        PhotoViewAttacher photoViewAttacher = new PhotoViewAttacher(ivPPFull);

        photoViewAttacher.update();

        ivClosePPFull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSplashDialog.cancel();
            }
        });

        mSplashDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mSplashDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mSplashDialog.setCancelable(true);
        mSplashDialog.show();

    }


    private class getReadersList extends AsyncTask<String , Void, ArrayList<String>> {
        DatabaseHandler db;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbReaders.setVisibility(View.VISIBLE);
            db = new DatabaseHandler(getActivity());

        }

        @Override
        protected ArrayList<String> doInBackground(String... params) {
            ArrayList<String> list_readers = db.getAllReadersOfMemberAccess(Integer.parseInt(params[0]), params[1]);
            return list_readers;
        }

        @Override
        protected void onPostExecute(ArrayList<String> list) {
            super.onPostExecute(list);
            pbReaders.setVisibility(View.GONE);
            tv_dialog_emp_reader_id.setVisibility(View.GONE);
            if (list != null) {
                if (list.size() > 0) {
                    /*for (int i=0; i<list.size();i++) {
                        String reader_ids = tv_dialog_emp_reader_id.getText().toString();
                        if (i==0) {
                            tv_dialog_emp_reader_id.setText(list.get(i));
                        }else {
                            tv_dialog_emp_reader_id.setText(reader_ids+" , "+list.get(i));
                        }
                    }*/

                    ArrayAdapter<String> adapter =  new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, list);
                    lvReadersList.setAdapter(adapter);
                    Utility.setListViewHeightBasedOnChildren(lvReadersList);
                }
            }
            if (db != null)
                db.close();
        }
    }


    private void showDialog (int resId) {
        final Dialog mSplashDialog = new Dialog(getActivity(), android.R.style.Theme_Material_NoActionBar_Fullscreen);
        //mSplashDialog.requestWindowFeature((int) window.FEATURE_NO_TITLE);
        mSplashDialog.setContentView(R.layout.dialog_profile_photo);
        //Drawable d = mContext.getResources().getDrawable(resId);
        //Bitmap b = ((BitmapDrawable) d ).getBitmap();
        ImageView ivPPFull = ((ImageView)mSplashDialog.findViewById(R.id.ivStPPFull));
        ImageView ivClosePPFull = (ImageView) mSplashDialog.findViewById(R.id.ivClosePPFull);

        ivPPFull.setImageResource(resId);//Bitmap(b);

        PhotoViewAttacher photoViewAttacher = new PhotoViewAttacher(ivPPFull);

        photoViewAttacher.update();

        ivClosePPFull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSplashDialog.cancel();
            }
        });

        mSplashDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mSplashDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mSplashDialog.setCancelable(true);
        mSplashDialog.show();

    }

}
