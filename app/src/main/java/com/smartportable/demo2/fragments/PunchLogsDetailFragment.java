package com.smartportable.demo2.fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.smartportable.demo2.R;
import com.smartportable.demo2.helper.PunchLogs;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

/**
 * Created by Ananth on 10/11/2017.
 */

public class PunchLogsDetailFragment extends BottomSheetDialogFragment {
    private PunchLogs punchlogs;
    private int position;
    boolean whichAct;

    public void setPunchLogsDetail (PunchLogs punchlogs) {
        this.punchlogs = punchlogs;
    }

    public void setItemPosition (int position) {
        this.position = position;
    }

    public void setWhichActivity (boolean whichAct) {
        this.whichAct = whichAct;
    }
    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        int emp_id = 0;//punchlogs.getSLN_Employee();
        String emp_name = punchlogs.getSLN_EmployeeName();
        String empPhoto = punchlogs.getSLN_EmployeePhoto();
        final String card_no = punchlogs.getCard_Number();
        int loc_id = punchlogs.getSLN_Location();
        String datetime = punchlogs.getPunch_Date();
        int cat_id = punchlogs.getMusteringId();
        boolean grantDeny = punchlogs.isGrant_Deny();
        int grentDenyInt = punchlogs.getIsPunchSynced();

        if (empPhoto != null && empPhoto.length() !=0) {
            Log.d("PunchlogsFragment", "readersAdapter: empPhoto: " + empPhoto);


            View contentView = View.inflate(getActivity(), R.layout.frag_bsd_emp_detail_main, null);
            dialog.setContentView(contentView);


            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
            CoordinatorLayout.Behavior behavior = params.getBehavior();

            if (behavior != null && behavior instanceof BottomSheetBehavior) {
                ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
            }

            View parent = (View) contentView.getParent();
            parent.setFitsSystemWindows(true);

            final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(parent);
            contentView.measure(0, 0);
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int screenHeight = displaymetrics.heightPixels;
            bottomSheetBehavior.setPeekHeight(screenHeight);

            if (params.getBehavior() instanceof BottomSheetBehavior) {
                ((BottomSheetBehavior) params.getBehavior()).setBottomSheetCallback(mBottomSheetBehaviorCallback);
            }

            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

            //params.height = screenHeight;
            parent.setLayoutParams(params);

            FloatingActionButton fab = (FloatingActionButton) contentView.findViewById(R.id.fabDialog);
            if (!whichAct) {
                fab.setVisibility(View.VISIBLE);
            } else {
                fab.setVisibility(View.GONE);
            }
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*if (getActivity() instanceof NFCMainActivity) {
                        ((NFCMainActivity) getActivity()).takeLogManually(position);
                    }*/
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
            });

            CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) contentView.findViewById(R.id.collapsing_toolbar_layout);
            collapsingToolbarLayout.setTitle(emp_id + " - " + emp_name);
            collapsingToolbarLayout.setExpandedTitleColor(getActivity().getResources().getColor(android.R.color.transparent));

            collapsingToolbarLayout.setContentScrimColor(getActivity().getResources().getColor(R.color.colorPrimary));
            collapsingToolbarLayout.setStatusBarScrimColor(getActivity().getResources().getColor(R.color.colorPrimary));

            Toolbar toolBar = (Toolbar) collapsingToolbarLayout.findViewById(R.id.tbEmpDetail);
            ImageView ivDetailClose = (ImageView) toolBar.findViewById(R.id.ivDetailClose);
            TextView tvDetailTitle = (TextView) toolBar.findViewById(R.id.tvDetailTitle);
            if (emp_id >= 0) {
                tvDetailTitle.setText(emp_id + " - " + emp_name);
            } else {
                tvDetailTitle.setText(""+emp_name);
            }
            ivDetailClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
            });

            final ImageView image = (ImageView) contentView.findViewById(R.id.ivFullScreen);
            TextView tv_dialog_emp_id = (TextView) contentView.findViewById(R.id.tv_dialog_emp_id);
            TextView tv_dialog_emp_name = (TextView) contentView.findViewById(R.id.tv_dialog_emp_name);
            TextView tv_dialog_emp_card = (TextView) contentView.findViewById(R.id.tv_dialog_emp_card);
            TextView tv_dialog_emp_loc_id = (TextView) contentView.findViewById(R.id.tv_dialog_emp_loc_id);
            TextView tv_dialog_emp_datetime = (TextView) contentView.findViewById(R.id.tv_dialog_emp_datetime);

            tv_dialog_emp_name.setVisibility(View.GONE);
            tv_dialog_emp_loc_id.setVisibility(View.GONE);

            if (emp_id >= 0)
                tv_dialog_emp_id.setText(emp_id + getActivity().getString(R.string.emp_detail_seperator) + emp_name);
            if (emp_name != null && emp_name.length() > 0)
                tv_dialog_emp_name.setText("" + emp_name);
            if (card_no != null && card_no.length() > 0) {
                tv_dialog_emp_card.setVisibility(View.VISIBLE);
                tv_dialog_emp_card.setText("Card Number : " + card_no);
            } else {
                tv_dialog_emp_card.setVisibility(View.GONE);
                tv_dialog_emp_card.setText("");
            }
            if (loc_id >= 0)
                tv_dialog_emp_loc_id.setText("" + loc_id);

            if (datetime != null && datetime.length() > 0) {
                tv_dialog_emp_datetime.setText("Punch Time : " + datetime);
            }


            Glide
                    .with(getActivity())
                    .asBitmap()
                    .load(empPhoto)
                    .centerCrop()
                    .dontAnimate()
                    .error(R.mipmap.ic_profile_pic)
                    .into(new BitmapImageViewTarget(image) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            image.setImageDrawable(circularBitmapDrawable);
                        }
                    });;

        }
    }

}
