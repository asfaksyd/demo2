package com.smartportable.demo2.fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.smartportable.demo2.R;
import com.smartportable.demo2.db.DatabaseHandler;
import com.smartportable.demo2.helper.Employee;
import com.smartportable.demo2.helper.Utility;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ananth on 10/10/2017.
 */

public class EmployeeDetailFragment extends BottomSheetDialogFragment {
    Employee employee;
    ProgressBar pbReaders;
    TextView tv_dialog_emp_reader_id;
    ListView lvReadersList;
    boolean forMemAccess = false;

    public void setEmployeeDetail (Employee employee) {
        this.employee = employee;
    }

    public void isForMemberAccess(boolean forMemAccess) {
        this.forMemAccess = forMemAccess;
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        //Employee item = list_employee.get(position);
        int emp_id = employee.getSLN_Employee();
        String emp_name = employee.getEmployeeName();
        String empPhoto = employee.getEmployee_Photo();
        String card_no = employee.getCard_Number();
        int loc_id = employee.getSLN_Location();
        int readerId = employee.getSLN_Reader();
        System.out.println("Image Photo " + empPhoto);


        View contentView = View.inflate(getActivity(), R.layout.frag_bsd_emp_detail_main, null);
        dialog.setContentView(contentView);


        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        /*CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }*/

        View parent = (View) contentView.getParent();
        parent.setFitsSystemWindows(true);

        final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(parent);
        contentView.measure(0, 0);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenHeight = displaymetrics.heightPixels;
        bottomSheetBehavior.setPeekHeight(screenHeight);

        if (params.getBehavior() instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior)params.getBehavior()).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        //params.height = screenHeight;
        parent.setLayoutParams(params);

        FloatingActionButton fab = (FloatingActionButton) contentView.findViewById(R.id.fabDialog);
        fab.setVisibility(View.GONE);

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) contentView.findViewById(R.id.collapsing_toolbar_layout);
        collapsingToolbarLayout.setTitle(emp_id + " - " +emp_name);
        collapsingToolbarLayout.setExpandedTitleColor(getActivity().getResources().getColor(android.R.color.transparent));

        collapsingToolbarLayout.setContentScrimColor(getActivity().getResources().getColor(R.color.colorPrimary));
        collapsingToolbarLayout.setStatusBarScrimColor(getActivity().getResources().getColor(R.color.colorPrimary));

        Toolbar toolBar = (Toolbar) collapsingToolbarLayout.findViewById(R.id.tbEmpDetail);
        ImageView ivDetailClose = (ImageView) toolBar.findViewById(R.id.ivDetailClose);
        TextView tvDetailTitle = (TextView) toolBar.findViewById(R.id.tvDetailTitle);
        if (emp_id >= 0) {
            tvDetailTitle.setText(emp_id + " - " + emp_name);
        } else {
            tvDetailTitle.setText(""+emp_name);
        }
        ivDetailClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Emp Detail close click");
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });

        final CircleImageView image= (CircleImageView) contentView.findViewById(R.id.ivFullScreen);
        TextView tv_dialog_emp_id = (TextView) contentView.findViewById(R.id.tv_dialog_emp_id);
        TextView tv_dialog_emp_name = (TextView) contentView.findViewById(R.id.tv_dialog_emp_name);
        TextView tv_dialog_emp_card = (TextView) contentView.findViewById(R.id.tv_dialog_emp_card);
        TextView tv_dialog_emp_loc_id = (TextView) contentView.findViewById(R.id.tv_dialog_emp_loc_id);
        tv_dialog_emp_reader_id = (TextView) contentView.findViewById(R.id.tv_dialog_emp_reader_id);
        TextView tv_dialog_emp_datetime = (TextView) contentView.findViewById(R.id.tv_dialog_emp_datetime);

        lvReadersList = (ListView) contentView.findViewById(R.id.lvReadersList);

        ArrayList<String> list = new ArrayList<String>();
        list.add("Test");
        list.add("Test");
        list.add("Test");
        list.add("Test");
        list.add("Test");

        pbReaders = (ProgressBar) contentView.findViewById(R.id.pbReaders);

        tv_dialog_emp_name.setVisibility(View.GONE);
        tv_dialog_emp_loc_id.setVisibility(View.GONE);
        tv_dialog_emp_reader_id.setVisibility(View.GONE);
        pbReaders.setVisibility(View.GONE);
        if (forMemAccess) {
            //pbReaders.setVisibility(View.VISIBLE);
            new getReadersList().execute(""+emp_id, card_no);
        }
        tv_dialog_emp_datetime.setVisibility(View.GONE);

        if (emp_id >= 0 && emp_name != null && emp_name.length() > 0)
            tv_dialog_emp_id.setText(emp_id + " - " + emp_name);
        if (emp_name != null && emp_name.length() > 0)
            tv_dialog_emp_name.setText("" + emp_name);
        if (card_no != null && card_no.length() > 0) {
            tv_dialog_emp_card.setVisibility(View.VISIBLE);
            tv_dialog_emp_card.setText("Card Number : " + card_no);
        } else {
            tv_dialog_emp_card.setVisibility(View.GONE);
            tv_dialog_emp_card.setText("");
        }
        if (loc_id >= 0)
            tv_dialog_emp_loc_id.setText("" + loc_id);

                /*if (readerId >= 0) {
                    tv_dialog_emp_reader_id.setText(""+readerId);
                }*/
                /*if (punch_date != null && punch_date.length() > 0) {
                    tv_dialog_emp_loc_id.setText(""+punch_date);
                }*/

        /*dialog.setNegativeButton(R.string.ok_label, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
            }
        });*/

        Glide
                .with(getActivity())
                .asBitmap()
                .load(empPhoto)
                .centerCrop()
                .dontAnimate()
                .error(R.mipmap.ic_profile_pic)
                .into(new BitmapImageViewTarget(image) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getActivity().getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        image.setImageDrawable(circularBitmapDrawable);
                    }
                });

    }

    private class getReadersList extends AsyncTask<String , Void, ArrayList<String>> {
        DatabaseHandler db;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pbReaders.setVisibility(View.VISIBLE);
            db = new DatabaseHandler(getActivity());

        }

        @Override
        protected ArrayList<String> doInBackground(String... params) {
            ArrayList<String> list_readers = db.getAllReadersOfMemberAccess(Integer.parseInt(params[0]), params[1]);
            return list_readers;
        }

        @Override
        protected void onPostExecute(ArrayList<String> list) {
            super.onPostExecute(list);
            pbReaders.setVisibility(View.GONE);
            tv_dialog_emp_reader_id.setVisibility(View.GONE);
            if (list != null) {
                if (list.size() > 0) {
                    /*for (int i=0; i<list.size();i++) {
                        String reader_ids = tv_dialog_emp_reader_id.getText().toString();
                        if (i==0) {
                            tv_dialog_emp_reader_id.setText(list.get(i));
                        }else {
                            tv_dialog_emp_reader_id.setText(reader_ids+" , "+list.get(i));
                        }
                    }*/

                    ArrayAdapter<String> adapter =  new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, list);
                    lvReadersList.setAdapter(adapter);
                    Utility.setListViewHeightBasedOnChildren(lvReadersList);
                }
            }
            if (db != null)
                db.close();
        }
    }

}
