package com.smartportable.demo2.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Message
import com.google.android.material.snackbar.Snackbar
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fineapp.android.retrofitClient.RetrofitClientSingleton
import com.google.gson.JsonObject
import com.smartportable.demo2.HomeScreen
import com.smartportable.demo2.R
import com.smartportable.demo2.adapters.SessionListAdapter
import com.smartportable.demo2.api.URLCollections
import com.smartportable.demo2.db.DatabaseHandler
import com.smartportable.demo2.helper.Boast
import com.smartportable.demo2.models.SessionsInfoModel
import com.smartportable.demo2.prefs.MyPreferencesActivity
import com.smartportable.demo2.realm.ReadersModel
import com.smartportable.demo2.utils.*
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection
import retrofit2.Call
import retrofit2.Response





class SyncSessionsActivity: AppCompatActivity(), SwipyRefreshLayout.OnRefreshListener {
    private var hStopRequest: Handler? = null
    var db: DatabaseHandler ? = null
    var rlSession : RelativeLayout ?= null
    var sessionsList = ArrayList<SessionsInfoModel.DataBean>()
    var llSessionRecord : LinearLayout?= null
    var tvSyncSessionRecord : TextView ?= null
    var srlSessions : SwipyRefreshLayout?= null
    var rvSessionList : RecyclerView ?= null
    var sessionsAdapter : SessionListAdapter? = null
    var tvSessionWarningMsg: TextView?= null
    var currentPageIndex : Int = 0
    var totalPageCount : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sessions)



        init()

    }

    fun init() {
        llSessionRecord = findViewById(R.id.llSessionRecord) as LinearLayout
        tvSyncSessionRecord = findViewById(R.id.tvSyncSessionRecord) as TextView
        srlSessions = findViewById(R.id.srlSessions) as SwipyRefreshLayout
        rvSessionList = findViewById(R.id.rvSessionsList) as RecyclerView
        tvSessionWarningMsg = findViewById(R.id.tvSessionWarningMsg) as TextView
        rvSessionList?.setLayoutManager(LinearLayoutManager(this))
        sessionsAdapter = SessionListAdapter(this@SyncSessionsActivity, sessionsList)
        rvSessionList?.adapter = sessionsAdapter
        //readersAdapter.notifyDataSetChanged()

        // recyclerview scroll
        var check_ScrollingUp :Boolean = false

        rvSessionList!!.addOnScrollListener( object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    // Scrolling up
                    if(check_ScrollingUp)
                    {
                        llSessionRecord!!.startAnimation(AnimationUtils.loadAnimation(this@SyncSessionsActivity,R.anim.trans_downwards))
                        llSessionRecord!!.visibility = View.VISIBLE
                        check_ScrollingUp = false
                    }

                } else {
                    // User scrolls down
                    if(!check_ScrollingUp )
                    {
                        llSessionRecord!!
                                .startAnimation(AnimationUtils
                                        .loadAnimation(this@SyncSessionsActivity,R.anim.trans_upwards))
                        llSessionRecord!!.visibility = View.GONE
                        check_ScrollingUp = true

                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

        })

        db = DatabaseHandler(this@SyncSessionsActivity)
        if (db!!.checkSession()) {
            showSessionsFromDB(db!!)
        } else {
            if (isNetworkAvailable(this@SyncSessionsActivity)) {
                val url = SharedPref.getStringValue(this@SyncSessionsActivity, keyBaseUrl, "")
                if (url == null || url == "" || !URLCollections.isValidUrl(url)) run {

                    val snackbar = Snackbar
                            .make(rlSession!!, resources.getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                            .setAction(resources.getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener)
                    snackbar.setActionTextColor(Color.GREEN)
                    val snackbarView = snackbar.view
                    snackbarView.setBackgroundColor(Color.DKGRAY)
                    val textView = snackbarView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
                    textView.setTextColor(Color.WHITE)
                    snackbar.show()
                } else {
                    currentPageIndex = 1
                    SyncSessionsAPI(0)
                }
            } else {
                srlSessions!!.isRefreshing = false
                Boast.makeText(this@SyncSessionsActivity, R.string.no_conn).show()
            }
        }

        srlSessions?.setOnRefreshListener(this)

        customToastOnTop(this@SyncSessionsActivity, "Swipe down to refresh")

        hStopRequest = object: Handler()
        {
            override fun handleMessage(msg: Message)
            {
                super.handleMessage(msg)
                Log.e("stopRequest","stopRequest--incomingMsg--"+msg.data.get("isOkClicked")+"<<>>"+ srlSessions!!.isRefreshing )
                if (msg.data.get("isOkClicked") === true)
                {
                    if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlSessions!!.isRefreshing)
                    {
                        db = null
                        apiRetrofit!!.cancel()

                        onBackPressOperation()
                    }
                    else
                    {
                        onBackPressOperation()
                    }

                }
                else
                {
                    //onBackPressOperation()
                }

            }

        }

        //Realm.init(this)
    }

    var mOnSnackBarSetUrlClickListener: View.OnClickListener = View.OnClickListener {
        println("SnackBar Set URL Click")
        val int_set_url = Intent(this@SyncSessionsActivity, MyPreferencesActivity::class.java)
        int_set_url.putExtra(HomeScreen.ACTION_STR, HomeScreen.SET_URL)
        startActivity(int_set_url)
    }

    override fun onRefresh(direction: SwipyRefreshLayoutDirection) {
        if (isNetworkAvailable(this@SyncSessionsActivity)) {
            val url = SharedPref.getStringValue(this@SyncSessionsActivity, keyBaseUrl, "")
            if (url == null || url == "" || !URLCollections.isValidUrl(url)) run {

                val snackbar = Snackbar
                        .make(rlSession!!, resources.getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                        .setAction(resources.getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener)
                snackbar.setActionTextColor(Color.GREEN)
                val snackbarView = snackbar.view
                snackbarView.setBackgroundColor(Color.DKGRAY)
                val textView = snackbarView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
                textView.setTextColor(Color.WHITE)
                snackbar.show()
            } else {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    currentPageIndex = 1
                } else {
                    currentPageIndex++
                }
                SyncSessionsAPI(0)
            }
        } else {
            srlSessions!!.isRefreshing = false
            Boast.makeText(this@SyncSessionsActivity, getString(R.string.no_conn)).show()
        }
    }

    var apiRetrofit: Call<SessionsInfoModel>? = null

    fun SyncSessionsAPI(sessionId : Int) {
        //showProgressDialog(this@SyncReadersActivity)

        if (currentPageIndex !=1 && currentPageIndex >= totalPageCount) {
            srlSessions!!.isRefreshing = false
            warningMessage(this@SyncSessionsActivity, "All Sessions Records has been synced.")
            return
        }
        srlSessions!!.isRefreshing = true
        var jObj : JsonObject = JsonObject()

        jObj.addProperty("Token", SharedPref.getAuthToken(this))
        //jObj.addProperty("NextPageIndex", "1")

        var jData : JsonObject = JsonObject()
        jData.addProperty("SessionId", sessionId)
        jData.addProperty("CurrentPage", currentPageIndex)
        jObj.add("data", jData)
        System.out.println("JObj: "+jObj.toString())

        var baseUrl = SharedPref.getStringValue(this@SyncSessionsActivity, keyAPIBaseUrl, "")

      /*  RetrofitClientSingleton.getInstance(baseUrl).getSessions(jObj)
                .enqueue(object : retrofit2.Callback<SessionsInfoModel> {*/

        var abc = RetrofitClientSingleton.getInstance(baseUrl)
        apiRetrofit = abc.getSessions(jObj)

        apiRetrofit!!.enqueue(object : retrofit2.Callback<SessionsInfoModel>{

                    override fun onFailure(call: Call<SessionsInfoModel>?, t: Throwable?) {
                        //stopProgress()
                        srlSessions!!.isRefreshing = false
                    }

                    override fun onResponse(call: Call<SessionsInfoModel>?, response: Response<SessionsInfoModel>?) {
                        //stopProgress()
                        srlSessions!!.isRefreshing = false

                        if (response!!.isSuccessful) {
                            val sessionInfo: SessionsInfoModel? = response!!.body()

                            if (sessionInfo!!.status == 1) {
                                //System.out.println("Sync Reader : "+readerInfo.status.toString())
                                //System.out.println("Sync Reader : "+readerInfo!!.data?.get(0)?.readerName)
                                if (sessionInfo!!.pageCount!!.contains("/")) {
                                    var pageCountArr  = sessionInfo!!.pageCount!!.split("/")
                                    totalPageCount = Integer.parseInt(pageCountArr[1])
                                }
                                val data: ArrayList<SessionsInfoModel.DataBean>? = sessionInfo!!.data
                                if (data != null) {
                                    var data2 : ArrayList<ReadersModel> = ArrayList()

                                    /*for (reader in data) {
                                        var readerModel = ReadersModel()
                                        readerModel.readerId = reader.readerId
                                        readerModel.readerName = reader.readerName
                                        readerModel.readerType = reader.readerType
                                        System.out.println("ReaderName "+reader.readerName)

                                        data1.add(readerModel)
                                    }*/


                                    db = DatabaseHandler(this@SyncSessionsActivity)

                                    if (currentPageIndex == 1) {
                                        db!!.deleteAllSessions()
                                    }
                                    db!!.saveSessions(data)

                                    showSessionsFromDB(db!!)

                                    if (currentPageIndex < totalPageCount) {
                                        onRefresh(SwipyRefreshLayoutDirection.BOTTOM)
                                    }

                                } else {
                                    toast(this@SyncSessionsActivity, sessionInfo!!.message)
                                }
                            } else if (sessionInfo.status == 3) {
                                forceLogout(this@SyncSessionsActivity)

                            } else {
                                toast(this@SyncSessionsActivity, sessionInfo!!.message)
                            }
                        }
                    }


                })

    }

    fun showSessionsFromDB (db : DatabaseHandler) {


        var data1 : ArrayList<SessionsInfoModel.DataBean> = db.allSessions
        System.out.println("Reader List: "+data1.size)


        if (data1.size > 0) {
            sessionsAdapter!!.updateSessionsList(data1)
            sessionsAdapter!!.notifyDataSetChanged()
            tvSessionWarningMsg?.visibility = View.GONE
        } else {
            tvSessionWarningMsg?.visibility = View.VISIBLE
        }

        tvSyncSessionRecord!!.text = "Total Sync Session : "+data1.size
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        Log.e("stopRequest","stopRequest--incomingMsg-srlSessionsList-"+srlSessions!!.isRefreshing )
        if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlSessions!!.isRefreshing)
        {
            stopRequest(this@SyncSessionsActivity,hStopRequest!!)
        }
        else
        {
            onBackPressOperation()
        }
    }

    fun onBackPressOperation()
    {
        this@SyncSessionsActivity.finish()
        if (URLCollections.whichDirection(this@SyncSessionsActivity) == URLCollections.DEV_DIR_RTL) {
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left)
        } else {
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {

            if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlSessions!!.isRefreshing)
            {
                stopRequest(this@SyncSessionsActivity,hStopRequest!!)
            }
            else
            {
                onBackPressOperation()
            }

            /*this@SyncSessionsActivity.finish()
            if (URLCollections.whichDirection(this@SyncSessionsActivity) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left)
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right)
            }*/
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()

        hStopRequest!!.removeCallbacks(null)

    }
}