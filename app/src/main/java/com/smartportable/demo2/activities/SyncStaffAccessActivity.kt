package com.smartportable.demo2.activities

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Message
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fineapp.android.retrofitClient.RetrofitClientSingleton
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import com.smartportable.demo2.HomeScreen
import com.smartportable.demo2.R
import com.smartportable.demo2.adapters.StaffInfoAdapter
import com.smartportable.demo2.api.URLCollections
import com.smartportable.demo2.db.DatabaseHandler
import com.smartportable.demo2.helper.Boast
import com.smartportable.demo2.models.StaffAccessListModel
import com.smartportable.demo2.models.StaffInfoModel
import com.smartportable.demo2.prefs.MyPreferencesActivity
import com.smartportable.demo2.utils.*
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection
import retrofit2.Call
import retrofit2.Response

class SyncStaffAccessActivity : AppCompatActivity() , SwipyRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener {
    private var hStopRequest: Handler? = null
    var llReaderList : LinearLayout ?= null
    var staffList = ArrayList<StaffInfoModel.DataBean?>()
    var llStaffRecord : LinearLayout?= null
    var tvSyncStaffRecord : TextView ?= null
    var srlStaffList : SwipyRefreshLayout?= null
    var rvStaffList : RecyclerView?= null
    var staffInfoAdapter : StaffInfoAdapter? = null
    var fm : FragmentManager = supportFragmentManager
    var db: DatabaseHandler ? = null
    var searchView: SearchView? = null
    var currentPageIndex : Int = 0
    var totalPageCount : Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reader_list)

        init()

    }

    fun init() {
        llStaffRecord = findViewById(R.id.llStudentRecord) as LinearLayout
        tvSyncStaffRecord = findViewById(R.id.tvSyncStudentRecord) as TextView
        llReaderList = findViewById(R.id.llReaderList) as LinearLayout
        srlStaffList = findViewById(R.id.srlReaderList) as SwipyRefreshLayout
        rvStaffList = findViewById(R.id.rvReaderList) as RecyclerView
        //fm = supportFragmentManager
        //rvReaderList?.setHasFixedSize(true)
        rvStaffList?.setLayoutManager(LinearLayoutManager(this))
        staffInfoAdapter = StaffInfoAdapter(this@SyncStaffAccessActivity, staffList, fm)
        rvStaffList?.adapter = staffInfoAdapter
        //readersAdapter.notifyDataSetChanged()

        // recyclerview scroll
        var check_ScrollingUp :Boolean = false

        rvStaffList!!.addOnScrollListener( object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    // Scrolling up
                    if(check_ScrollingUp)
                    {
                        llStaffRecord!!.startAnimation(AnimationUtils.loadAnimation(this@SyncStaffAccessActivity,R.anim.trans_downwards))
                        llStaffRecord!!.visibility = View.VISIBLE
                        check_ScrollingUp = false
                    }

                } else {
                    // User scrolls down
                    if(!check_ScrollingUp )
                    {
                        llStaffRecord!!
                                .startAnimation(AnimationUtils
                                        .loadAnimation(this@SyncStaffAccessActivity,R.anim.trans_upwards))
                        llStaffRecord!!.visibility = View.GONE
                        check_ScrollingUp = true

                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

        })

        db = DatabaseHandler(this@SyncStaffAccessActivity)
        if (db!!.checkStaffAccessList()) {
            showStaffInfoFromDB(db!!)
        } else {
            if (isNetworkAvailable(this@SyncStaffAccessActivity)) {
                val url = SharedPref.getStringValue(this@SyncStaffAccessActivity, keyBaseUrl, "")


                if (url == null || url == "" || !URLCollections.isValidUrl(url)) run {

                    val snackbar = Snackbar
                            .make(llReaderList!!, resources.getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                            .setAction(resources.getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener)
                    snackbar.setActionTextColor(Color.GREEN)
                    val snackbarView = snackbar.view
                    snackbarView.setBackgroundColor(Color.DKGRAY)
                    val textView = snackbarView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
                    textView.setTextColor(Color.WHITE)
                    snackbar.show()
                } else {
                    currentPageIndex = 1
                    SyncStaffAccessListAPI(0)
                }
            } else {
                srlStaffList!!.isRefreshing = false
                Boast.makeText(this@SyncStaffAccessActivity, R.string.no_conn).show()
            }
        }

        srlStaffList?.setOnRefreshListener(this)

        customToastOnTop(this@SyncStaffAccessActivity, "Swipe down to refresh")

        hStopRequest = object: Handler()
        {
            override fun handleMessage(msg: Message)
            {
                super.handleMessage(msg)
                Log.e("stopRequest","stopRequest--incomingMsg--"+msg.data.get("isOkClicked")+"<<>>"+ srlStaffList!!.isRefreshing )
                if (msg.data.get("isOkClicked") === true)
                {
                    if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlStaffList!!.isRefreshing)
                    {
                        db = null
                        apiRetrofit!!.cancel()

                        onBackPressOperation()
                    }
                    else
                    {
                        onBackPressOperation()
                    }

                }
                else
                {
                    //onBackPressOperation()
                }

            }

        }

        //Realm.init(this)
    }

    override fun onCreateOptionsMenu(menu : Menu) : Boolean {

        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_sync_student, menu)

        val searchItem = menu!!.findItem(R.id.action_search)

        val searchManager = this@SyncStaffAccessActivity.getSystemService(Context.SEARCH_SERVICE) as SearchManager


        if (searchItem != null) {
            searchView = searchItem!!.getActionView() as SearchView
        }
        if (searchView != null) {
            searchView!!.setSearchableInfo(searchManager.getSearchableInfo(this@SyncStaffAccessActivity.getComponentName()))

            searchView!!.setOnQueryTextListener(this)
        }
        return super.onCreateOptionsMenu(menu)
    }

    fun onBackPressOperation()
    {
        if (!searchView!!.isIconified) {
            searchView!!.onActionViewCollapsed()
            if (db!!.checkStaffInfo()) {
                showStaffInfoFromDB(db!!)
            }
        } else {
            //super.onBackPressed()
            this@SyncStaffAccessActivity.finish()
            if (URLCollections.whichDirection(this@SyncStaffAccessActivity) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left)
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right)
            }
        }
    }

    override fun onBackPressed() {
        Log.e("stopRequest","stopRequest--incomingMsg-SyncStaffAccessList-"+srlStaffList!!.isRefreshing )
        if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlStaffList!!.isRefreshing)
        {
            stopRequest(this@SyncStaffAccessActivity,hStopRequest!!)
        }
        else
        {
            onBackPressOperation()
        }
    }

    var mOnSnackBarSetUrlClickListener: View.OnClickListener = View.OnClickListener {
        println("SnackBar Set URL Click")
        val int_set_url = Intent(this@SyncStaffAccessActivity, MyPreferencesActivity::class.java)
        int_set_url.putExtra(HomeScreen.ACTION_STR, HomeScreen.SET_URL)
        startActivity(int_set_url)
    }

    override fun onRefresh(direction: SwipyRefreshLayoutDirection) {
        if (isNetworkAvailable(this@SyncStaffAccessActivity)) {
            val url = SharedPref.getStringValue(this@SyncStaffAccessActivity, keyBaseUrl, "")


            if (url == null || url == "" || !URLCollections.isValidUrl(url)) run {

                val snackbar = Snackbar
                        .make(llReaderList!!, resources.getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                        .setAction(resources.getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener)
                snackbar.setActionTextColor(Color.GREEN)
                val snackbarView = snackbar.view
                snackbarView.setBackgroundColor(Color.DKGRAY)
                val textView = snackbarView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
                textView.setTextColor(Color.WHITE)
                snackbar.show()
            } else {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    currentPageIndex = 1
                } else {
                    currentPageIndex++
                }
                SyncStaffAccessListAPI(0)
            }
        } else {
            srlStaffList!!.isRefreshing = false
            Boast.makeText(this@SyncStaffAccessActivity, getString(R.string.no_conn)).show()
        }
    }

    var apiRetrofit: Call<StaffAccessListModel>? = null
    fun SyncStaffAccessListAPI(staffId : Int) {
        //showProgressDialog(this@SyncReadersActivity)
        if (currentPageIndex !=1 && currentPageIndex >= totalPageCount) {
            warningMessage(this@SyncStaffAccessActivity, "All Students Records has been synced.")
            srlStaffList!!.isRefreshing = false
            return
        }
        srlStaffList!!.isRefreshing = true
        val jObj : JsonObject = JsonObject()

        jObj.addProperty("Token", SharedPref.getAuthToken(this))
        //jObj.addProperty("NextPageIndex", "1")

        var jData : JsonObject = JsonObject()
        jData.addProperty("StaffId", staffId)
        jData.addProperty("CurrentPage", 1)
        jObj.add("data", jData)
        System.out.println("JObj: "+jObj.toString())

        var baseUrl = SharedPref.getStringValue(this@SyncStaffAccessActivity, keyAPIBaseUrl, "")

       /* RetrofitClientSingleton.getInstance(baseUrl).getStaffAccessInfo(jObj)
                .enqueue(object : retrofit2.Callback<StaffAccessListModel> {*/

        var abc = RetrofitClientSingleton.getInstance(baseUrl)
        apiRetrofit = abc.getStaffAccessInfo(jObj)

        apiRetrofit!!.enqueue(object : retrofit2.Callback<StaffAccessListModel>{

                    override fun onFailure(call: Call<StaffAccessListModel>?, t: Throwable?) {
                        //stopProgress()
                        srlStaffList!!.isRefreshing = false
                    }

                    override fun onResponse(call: Call<StaffAccessListModel>?, response: Response<StaffAccessListModel>?) {
                        //stopProgress()
                        srlStaffList!!.isRefreshing = false

                        if (response!!.isSuccessful) {
                            val accessList: StaffAccessListModel? = response!!.body()

                            if (accessList!!.status == 1) {
                                //System.out.println("Sync Reader : "+readerInfo.status.toString())
                                //System.out.println("Sync Reader : "+readerInfo!!.data?.get(0)?.readerName)

                                if (accessList!!.pageCount!!.contains("/")) {
                                    var pageCountArr  = accessList!!.pageCount!!.split("/")
                                    totalPageCount = Integer.parseInt(pageCountArr[1])
                                }

                                val data: ArrayList<StaffAccessListModel.DataBean>? = accessList!!.data
                                if (data != null) {


                                     db = DatabaseHandler(this@SyncStaffAccessActivity)

                                    if (currentPageIndex == 1) {
                                        db!!.deleteAllStaffAccessList()
                                    }

                                    db!!.saveStaffAccessList(data)

                                    showStaffInfoFromDB(db!!)

                                    if (currentPageIndex < totalPageCount) {
                                        onRefresh(SwipyRefreshLayoutDirection.BOTTOM)
                                    }

                                } else {
                                    toast(this@SyncStaffAccessActivity, accessList!!.message)

                                    if (currentPageIndex == 1) {
                                        db!!.deleteAllStaffAccessList()
                                    }

                                    showStaffInfoFromDB(db!!)
                                }
                            } else if (accessList.status == 3) {
                                forceLogout(this@SyncStaffAccessActivity)

                            } else {
                                toast(this@SyncStaffAccessActivity, accessList!!.message)
                            }
                        }
                    }


                })

    }

    fun showStaffInfoFromDB (db : DatabaseHandler) {


        var data1 : ArrayList<StaffInfoModel.DataBean?> = db.allStaffInfoInStaffAccess
        System.out.println("Staff List: "+data1.size)

        tvSyncStaffRecord!!.text = "Total Sync Staff Access : "+data1.size

        staffInfoAdapter!!.updateStaffInfo(data1, true) // seond boolean argument is for the member access alway true to display reader list in detail dialog
        staffInfoAdapter!!.notifyDataSetChanged()
    }

    override fun onQueryTextSubmit(searchKeyword: String?): Boolean {
        val db = DatabaseHandler(this@SyncStaffAccessActivity)
        var data1 = db.getStaffsBySearch(searchKeyword)
        staffInfoAdapter!!.updateStaffInfo(data1, true)
        staffInfoAdapter!!.notifyDataSetChanged()
        return true
    }

    override fun onQueryTextChange(p0: String?): Boolean {

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {


            Log.e("stopRequest","stopRequest--incomingMsg-SyncStaffAccessList-"+srlStaffList!!.isRefreshing )
            if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlStaffList!!.isRefreshing)
            {
                stopRequest(this@SyncStaffAccessActivity,hStopRequest!!)
            }
            else
            {
                onBackPressOperation()
            }


            /*if (!searchView!!.isIconified) {
                searchView!!.onActionViewCollapsed()
                if (db!!.checkStudentInfo()) {
                    showStaffInfoFromDB(db!!)
                }
                return true
            } else {
                this@SyncStaffAccessActivity.finish()
                if (URLCollections.whichDirection(this@SyncStaffAccessActivity) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left)
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right)
                }
            }*/

            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()

        hStopRequest!!.removeCallbacks(null)

    }

}
