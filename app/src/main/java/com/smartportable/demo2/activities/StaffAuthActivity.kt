package com.smartportable.demo2.activities

import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.TagLostException
import android.nfc.tech.IsoDep
import android.nfc.tech.MifareClassic
import android.os.Build
import android.os.Bundle
import android.os.Vibrator
import android.util.Base64
import android.util.Log
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.androidscade.nfckotlindemo.NFCUtil
import com.bumptech.glide.Glide
import com.fineapp.android.retrofitClient.RetrofitClientSingleton
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.smartportable.demo2.HomeScreen
import com.smartportable.demo2.R
import com.smartportable.demo2.api.URLCollections
import com.smartportable.demo2.db.DatabaseHandler
import com.smartportable.demo2.fragments.DoorAccessInfoFragment
import com.smartportable.demo2.fragments.StaffInfoFragment
import com.smartportable.demo2.helper.SaveImages
import com.smartportable.demo2.helper.SoundManage
import com.smartportable.demo2.interfaces.ChangeCardStatus
import com.smartportable.demo2.listeners.OnAuthDataReceived
import com.smartportable.demo2.listeners.OnStaffAuthDataReceived
import com.smartportable.demo2.models.MifareSettingModel
import com.smartportable.demo2.models.StaffInfoModel
import com.smartportable.demo2.prefs.MyPreferencesActivity
import com.smartportable.demo2.utils.*
import com.zebra.adc.decoder.Barcode2DWithSoft
import de.hdodenhof.circleimageview.CircleImageView
import fr.coppernic.sdk.utils.helpers.CpcOs
import kotlinx.android.synthetic.main.activity_auth.*
import kotlinx.android.synthetic.main.dialog_old_mem_acc_logs_exists.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONException
import retrofit2.Call
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.UnsupportedEncodingException
import java.util.jar.JarException
import kotlin.collections.ArrayList


class StaffAuthActivity : AppCompatActivity(), NfcAdapter.ReaderCallback, View.OnClickListener, ChangeCardStatus {

    val TAG : String = StaffAuthActivity:: class.java.simpleName
    lateinit var collapsingToolbarLayout: CollapsingToolbarLayout
    lateinit var toolbar: Toolbar
    lateinit var tabLayout: TabLayout
    lateinit var htab_maincontent: CoordinatorLayout
    lateinit var htab_appbar: AppBarLayout
    lateinit var htab_header: CircleImageView

    lateinit var rlLoadElements: RelativeLayout
    lateinit var rlAnim: RelativeLayout
    lateinit var ivDevice : ImageView
    lateinit var ivCard : ImageView
    lateinit var fabBarcodeAuth: FloatingActionButton
    lateinit var rlIDNumber: RelativeLayout
    lateinit var etIDNumber: EditText
    lateinit var tvIDNumberError: TextView
    lateinit var viewPager : ViewPager
    private lateinit var adapter: ViewPagerAdapter
    private var mNfcAdapter: NfcAdapter? = null
    lateinit var csnNumber : String

    lateinit var rgStaff : RadioGroup

    lateinit var rbAppNo : RadioButton
    lateinit var rbSlNo : RadioButton
    lateinit var rbUid : RadioButton
    lateinit var rbStaffId : RadioButton
    lateinit var rbCardNo : RadioButton


    internal var fieldName = ""
    internal var fieldValues = ""

    internal var list = ArrayList<MifareSettingModel>()

    lateinit var mifare_json_array : JSONArray

    lateinit var onAuthDataReceivedPI : OnAuthDataReceived
    lateinit var onAuthDataReceivedLI : OnAuthDataReceived
    lateinit var onAuthDataReceivedDA : OnAuthDataReceived

    lateinit var onStaffAuthDataReceivedPI : OnStaffAuthDataReceived
    lateinit var onStaffAuthDataReceivedLI : OnStaffAuthDataReceived
    lateinit var onStaffAuthDataReceivedDA : OnStaffAuthDataReceived

    lateinit var mRVLicenseCat : RecyclerView
    lateinit var mBSDLicenseCat: BottomSheetBehavior<NestedScrollView>

    var auth_type = -1
    var cardSt = -1

    var subCatData: StaffInfoModel = StaffInfoModel()

    // Barcode part
    private val INTENT_ACTION_SCAN = "fr.coppernic.intent.action.SCAN"
    val ACTION_SCAN_SUCCESS = "fr.coppernic.intent.scansuccess"
    val ACTION_SCAN_ERROR = "fr.coppernic.intent.scanfailed"
    val BARCODE_DATA = "BarcodeData"
    private val KEY_PACKAGE = "package"
    lateinit var db : DatabaseHandler

    internal var barcode2DWithSoft: Barcode2DWithSoft? = null
    lateinit var receiver: HomeKeyEventBroadCastReceiver

    var barCode = ""
    internal var seldata = "ASCII"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        // gotoMain(this);
        auth_type = intent.getIntExtra(AUTH_TYPE, -1)
        initView()
        mapMifareSetting()

        if (auth_type == READ_BARCODE) {
            // broasr cast receiver object for the C71 device
            receiver = HomeKeyEventBroadCastReceiver()

            barcode2DWithSoft = Barcode2DWithSoft.getInstance()
            //InitTask().execute()
            InitBarcodeReader()
        }

    }

    override fun onDestroy() {
        if (auth_type == READ_BARCODE) {
            if (barcode2DWithSoft != null) {
                barcode2DWithSoft!!.stopScan()
                barcode2DWithSoft!!.close()
            }
        }
        super.onDestroy()
    }

    var ScanBack: Barcode2DWithSoft.ScanCallback = Barcode2DWithSoft.ScanCallback { i, length, bytes ->
        if (length < 1) {
            if (length == -1) {
                showSnackBar("Scan cancel")
            } else if (length == 0) {
                showSnackBar("Scan TimeOut")
            } else {
                Log.i(TAG, "Scan fail")
            }
        } else {
            SoundManage.PlaySound(this@StaffAuthActivity, SoundManage.SoundType.SUCCESS)
            barCode = ""


            //  String res = new String(dd,"gb2312");
            try {
                Log.i("Ascii", seldata)
                barCode = String(bytes)//, 0, length, seldata)
                zt()
            } catch (ex: UnsupportedEncodingException) {
            }

            showSnackBar(barCode)
            getLicenseDataAPI(barCode)
        }
    }

    internal fun zt() {

        val vibrator = this.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        vibrator.vibrate(100)
    }

    private fun ScanBarcode() {
        if (barcode2DWithSoft != null) {
            Log.i(TAG, "ScanBarcode")

            barcode2DWithSoft!!.scan()
            barcode2DWithSoft!!.setScanCallback(ScanBack)
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == 139 || keyCode == 66) {
            if (event.repeatCount == 0) {
                ScanBarcode()
                return true
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == 139) {
            if (event.repeatCount == 0) {
                barcode2DWithSoft!!.stopScan()
                return true
            }
        }
        return super.onKeyUp(keyCode, event)
    }

    fun InitBarcodeReader() {
        var mypDialog = ProgressDialog(this@StaffAuthActivity)
        mypDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        mypDialog.setMessage("init...")
        mypDialog.setCanceledOnTouchOutside(false)
        mypDialog.show()

        doAsync {
            //Execute all the lon running tasks here
            var result  = false
            if (barcode2DWithSoft != null) {
                result = barcode2DWithSoft!!.open(this@StaffAuthActivity)
                Log.i(TAG, "open=$result")

            }
            uiThread {
                //Update the UI thread here
                if (result!!) {
                    //                barcode2DWithSoft.setParameter(324, 1);
                    //                barcode2DWithSoft.setParameter(300, 0); // Snapshot Aiming
                    //                barcode2DWithSoft.setParameter(361, 0); // Image Capture Illumination

                    // interleaved 2 of 5
                    barcode2DWithSoft!!.setParameter(6, 1)
                    barcode2DWithSoft!!.setParameter(22, 0)
                    barcode2DWithSoft!!.setParameter(23, 55)
                    barcode2DWithSoft!!.setParameter(402, 1)
                    //Toast.makeText(this@AuthActivity, "Success", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this@StaffAuthActivity, "fail to open barcode reader", Toast.LENGTH_SHORT).show()
                }
                mypDialog.cancel()
            }
        }
    }

    inner class HomeKeyEventBroadCastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (action == "com.rscja.android.KEY_DOWN") {
                val reason = intent.getIntExtra("Keycode", 0)
                //getStringExtra
                val long1 = intent.getBooleanExtra("Pressed", false)
                // home key处理点
                if (reason == 280 || reason == 66) {

                    ScanBarcode()


                }
                // Toast.makeText(getApplicationContext(), "home key="+reason+",long1="+long1, Toast.LENGTH_SHORT).show();
            }
        }

        /*companion object {

            val SYSTEM_REASON = "reason"
            val SYSTEM_HOME_KEY = "homekey"//home key
            val SYSTEM_RECENT_APPS = "recentapps"//long home key
        }*/
    }


    private fun initView() {

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this)

        db = DatabaseHandler(this@StaffAuthActivity)

        htab_maincontent = findViewById(R.id.htab_maincontent) as CoordinatorLayout
        htab_header = findViewById(R.id.htab_header) as CircleImageView
        rlLoadElements = findViewById(R.id.rlLoadElements) as RelativeLayout

        rlAnim = findViewById(R.id.rlAnim) as RelativeLayout
        ivDevice = findViewById(R.id.ivDevice) as ImageView
        ivCard = findViewById(R.id.ivCard) as ImageView

        fabBarcodeAuth = findViewById(R.id.fabBarcodeAuth) as FloatingActionButton
        rlIDNumber = findViewById(R.id.rlIDNumber) as RelativeLayout
        etIDNumber = findViewById(R.id.etIDNumber) as EditText
        tvIDNumberError = findViewById(R.id.tvIDNumberError) as TextView

        toolbar = findViewById(R.id.htab_toolbar) as Toolbar
        setSupportActionBar(toolbar)
        if (getSupportActionBar() != null) getSupportActionBar()!!.setTitle("")
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)

        htab_appbar = this.findViewById(R.id.htab_appbar) as AppBarLayout
        tabLayout = findViewById(R.id.htab_tabs) as TabLayout

        htab_appbar.addOnOffsetChangedListener(offsetChangeListener(toolbar))

        viewPager = findViewById(R.id.htab_viewpager) as ViewPager
        viewPager.id = View.generateViewId()
        setupViewPager(viewPager)
        tabLayout.setupWithViewPager(viewPager)

        setupCollaspView(viewPager)
        //  openBottomDialog(false)

        rgStaff = findViewById(R.id.rgStaff) as RadioGroup
        rbAppNo = findViewById(R.id.rbAppNo) as RadioButton
        rbSlNo = findViewById(R.id.rbSlNo) as RadioButton
        rbUid = findViewById(R.id.rbUid) as RadioButton
        rbStaffId = findViewById(R.id.rbStaffId) as RadioButton
        rbCardNo = findViewById(R.id.rbCardNo) as RadioButton


        rgStaff.visibility = View.VISIBLE

        if (auth_type == READ_CARD_OFFLINE || auth_type == READ_CARD_NUMBER_OFF || auth_type == READ_CARD_NUMBER) {
            showAnimation1()
        } else if (auth_type == READ_BARCODE) {
            showAnimation2()
        } else if (auth_type == READ_CARD_BY_SEARCH) {
            showSearchOption()
        }

        fabBarcodeAuth.setOnClickListener(this)

        etIDNumber.setOnEditorActionListener() {v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val idValue = etIDNumber.text.toString().trim()

                if (idValue.length == 0) {
                    etIDNumber.setError("Field cannot be blank!")
                } else if (idValue.length >= 1) {
                    etIDNumber.setError(null)
                    hideSoftKeypad()
                    if (!isNetworkAvailable(this@StaffAuthActivity)) {
                        //toast(this@AuthActivity, getString(R.string.network_error))
                        showSnackBar(getString(R.string.network_error))
                    } else {
                        val url = SharedPref.getStringValue(this@StaffAuthActivity, keyBaseUrl, "")
                        //permissionStatus = PreferenceManager.getDefaultSharedPreferences(this);


                        if (url == null || url == "" || !URLCollections.isValidUrl(url)) run {

                            val snackbar = Snackbar
                                    .make(htab_maincontent, resources.getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                                    .setAction(resources.getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener)
                            snackbar.setActionTextColor(Color.GREEN)
                            val snackbarView = snackbar.view
                            snackbarView.setBackgroundColor(Color.DKGRAY)
                            val textView = snackbarView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
                            textView.setTextColor(Color.WHITE)
                            snackbar.show()
                        } else {
                            getLicenseDataAPI(etIDNumber.text.toString().trim());
                        }
                    }
                    etIDNumber.text.clear()
                } else {
                    etIDNumber.setError("Please enter proper value")
                }

                true
            } else {
                false
            }

        }
    }

    var mOnSnackBarSetUrlClickListener: View.OnClickListener = View.OnClickListener {
        println("SnackBar Set URL Click")
        val int_set_url = Intent(this@StaffAuthActivity, MyPreferencesActivity::class.java)
        int_set_url.putExtra(HomeScreen.ACTION_STR, HomeScreen.SET_URL)
        startActivity(int_set_url)
    }

    private fun openBottomDialog(hide_or_show : Boolean) {
        /* val view = layoutInflater.inflate(R.layout.bottom_sheet_dialog, null)
         val dialog = BottomSheetDialog(this)

         rlAnim = view.findViewById(R.id.rlAnim) as RelativeLayout
         ivDevice = view.findViewById(R.id.ivDevice) as ImageView
         ivCard = view.findViewById(R.id.ivCard) as ImageView

         dialog.setContentView(view)
         dialog.show()
         dialog.setCanceledOnTouchOutside(hide_or_show)*/

    }

    /*private fun openLicenseCatBSD(hide_or_show : Boolean) {

        val subCatCode = subCatData.subCategory
        if (subCatCode != null) {
            if (subCatCode.isNotEmpty()) {
                val bottomSheetDialogFragment = BSDLicenseTypesFragment()
                bottomSheetDialogFragment.setLicenseData(subCatCode)
                bottomSheetDialogFragment.show(supportFragmentManager, bottomSheetDialogFragment.getTag())
            } else {
                showSnackBar(getString(R.string.no_license_data))
            }
        }else {
            showSnackBar(getString(R.string.no_license_data))
        }

    }*/

    private fun mapMifareSetting() {
        try {
            val ins = this@StaffAuthActivity.getResources().openRawResource(R.raw.staff_mifare_config)//getIdentifier("mifare_config", "raw", getPackageName());
            val data = readTextFile(ins)
            mifare_json_array = JSONArray(data)
            //if (mifare_json_array != null)
            Log.d(TAG, "array Length" + mifare_json_array.length())

        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    public fun setStaffAuthDataReceivedListenerPI (listenerPI: OnStaffAuthDataReceived) {
        onStaffAuthDataReceivedPI = listenerPI
    }

    public fun setStaffAuthDataReceivedListenerLI (listenerLI: OnStaffAuthDataReceived) {
        onStaffAuthDataReceivedLI = listenerLI
    }
    public fun setStaffAuthDataReceivedListenerDA (listenerDA: OnStaffAuthDataReceived) {
        onStaffAuthDataReceivedDA = listenerDA
    }

    fun readTextFile(inputStream: InputStream): String {
        val outputStream = ByteArrayOutputStream()

        val buf = ByteArray(1024)
        var len: Int
        try {
            var tmp = -1
            while (inputStream.read(buf).let { tmp = it; it != -1 }) {
                outputStream.write(buf, 0, tmp)
            }
            outputStream.close()
            inputStream.close()
        } catch (e: IOException) {

        }

        return outputStream.toString()
    }

    private fun setupCollaspView(viewPager : ViewPager) {




        collapsingToolbarLayout = findViewById(R.id.htab_collapse_toolbar) as CollapsingToolbarLayout

        try {
            val bitmap = BitmapFactory.decodeResource(resources, R.drawable.ic_person_black_256dp)
            Palette.from(bitmap).generate(object : Palette.PaletteAsyncListener {
                override fun onGenerated(@Nullable palette: Palette?) {

                    collapsingToolbarLayout.setContentScrimColor(
                            ContextCompat.getColor(this@StaffAuthActivity, R.color.colorPrimaryDark)
                    )
                    collapsingToolbarLayout.setStatusBarScrimColor(
                            ContextCompat.getColor(this@StaffAuthActivity, R.color.colorPrimaryDark)
                    )
                }
            })

        } catch (e: Exception) {
            // if Bitmap fetch fails, fallback to primary colors
            //Log.e(TAG, "onCreate: failed to create bitmap from background", e.fillInStackTrace())
            collapsingToolbarLayout.setContentScrimColor(
                    ContextCompat.getColor(this, R.color.colorPrimaryDark)
            )
            collapsingToolbarLayout.setStatusBarScrimColor(
                    ContextCompat.getColor(this, R.color.colorPrimaryDark)
            )
        }

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {

                viewPager.currentItem = tab.position

                when (tab.position) {
                    0 -> {
                    }
                }// TODO: 31/03/17
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })


    }

    fun offsetChangeListener(toolbar: Toolbar): AppBarLayout.OnOffsetChangedListener  = object : AppBarLayout.OnOffsetChangedListener {
        internal var scrollRange = -1

        override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
            //Initialize the size of the scroll
            if (scrollRange == -1) {
                scrollRange = appBarLayout.totalScrollRange
            }
            //Check if the view is collapsed
            if (scrollRange + verticalOffset == 0) {

                if (cardSt == CARD_ST_ACTIVE) {
                    toolbar.setBackgroundColor(ContextCompat.getColor(this@StaffAuthActivity, R.color.colorGreen))
                    tabLayout.setBackgroundColor(ContextCompat.getColor(this@StaffAuthActivity, R.color.colorGreen))
                } else if (cardSt == CARD_ST_REVOKED) {
                    toolbar.setBackgroundColor(ContextCompat.getColor(this@StaffAuthActivity, R.color.colorGray))
                    tabLayout.setBackgroundColor(ContextCompat.getColor(this@StaffAuthActivity, R.color.colorGray))
                } else if (cardSt == CARD_ST_LOST) {
                    toolbar.setBackgroundColor(ContextCompat.getColor(this@StaffAuthActivity, R.color.colorBlack))
                    tabLayout.setBackgroundColor(ContextCompat.getColor(this@StaffAuthActivity, R.color.colorBlack))
                } else if (cardSt == CARD_ST_SUSPENDED) {
                    toolbar.setBackgroundColor(ContextCompat.getColor(this@StaffAuthActivity, R.color.colorYellow))
                    tabLayout.setBackgroundColor(ContextCompat.getColor(this@StaffAuthActivity, R.color.colorYellow))
                } else if(cardSt == CARD_ST_EXPIRED) {
                    toolbar.setBackgroundColor(ContextCompat.getColor(this@StaffAuthActivity, R.color.colorRed))
                    tabLayout.setBackgroundColor(ContextCompat.getColor(this@StaffAuthActivity, R.color.colorRed))
                } else {
                    toolbar.setBackgroundColor(ContextCompat.getColor(this@StaffAuthActivity, R.color.colorPrimary))
                    tabLayout.setBackgroundColor(ContextCompat.getColor(this@StaffAuthActivity, R.color.colorPrimary))
                }
            } else {
                toolbar.setBackgroundColor(ContextCompat.getColor(this@StaffAuthActivity, android.R.color.transparent))
                tabLayout.setBackgroundColor(ContextCompat.getColor(this@StaffAuthActivity, android.R.color.transparent))
            }
        }
    }

    private fun setupViewPager(viewPager: ViewPager) {
        adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFrag(StaffInfoFragment(), "Staff information")
        adapter.addFrag(DoorAccessInfoFragment.newInstance("Staff"), "Door Access")


        viewPager.adapter = adapter

        viewPager.offscreenPageLimit = 2
    }


    private class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getItemPosition(`object`: Any): Int {
            return PagerAdapter.POSITION_NONE
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }

    override fun onResume() {
        super.onResume()
        if (auth_type == READ_CARD_OFFLINE || auth_type == READ_CARD_NUMBER_OFF || auth_type == READ_CARD_NUMBER) {
            mNfcAdapter?.let {
                mNfcAdapter?.enableReaderMode(this, this, NFCUtil.READER_FLAGS, null)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if (auth_type == READ_CARD_OFFLINE || auth_type == READ_CARD_NUMBER_OFF || auth_type == READ_CARD_NUMBER) {
            mNfcAdapter?.let {
                mNfcAdapter?.disableReaderMode(this)
            }
        }
    }

    public override fun onStart() {
        super.onStart()
        if (auth_type == READ_BARCODE) {
            //registerReceiver()
            registerReceiver(receiver, IntentFilter("com.rscja.android.KEY_DOWN"))
        }
    }

    public override fun onStop() {
        super.onStop()
        if (auth_type == READ_BARCODE) {
            //unregisterReceiver(scanResult)
            unregisterReceiver(receiver)
        }
    }

    /**
     * Triggers a barcode scan
     */
    private fun startScan() {
        val scanIntent = Intent()
        System.out.println("Package Name:" + CpcOs.getSystemServicePackage(this))
        scanIntent.`package` = CpcOs.getSystemServicePackage(this)
        scanIntent.action = INTENT_ACTION_SCAN
        scanIntent.putExtra(KEY_PACKAGE, this?.getPackageName())
        val info = this?.startService(scanIntent)
        if (info != null) {
            // OK
            //println("Scan Started Successfully")
            //tvBarcode.text = "Scan Started Successfully"
            showAVILoaderIndicator()
        } else {
            // Error
            //println("ERROR Starting Scan")
            //tvBarcode.text = "ERROR Starting Scan"
            showSnackBar("ERROR Starting Scan")
        }
    }

    private val scanResult = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            println("onreceive")
            hideAVILoaderIndicator()
            if (intent.action == ACTION_SCAN_SUCCESS) {
                val dataRead = intent.extras!!.getString(BARCODE_DATA)
                println("data" + dataRead!!)
                //val tvBarcode = findViewById(R.id.tvBarcode) as TextView
                //tvBarcode.text = dataRead
                if (!isNetworkAvailable(this@StaffAuthActivity)) {
                    //toast(this@AuthActivity, getString(R.string.network_error))
                    showSnackBar(getString(R.string.network_error))
                } else {
                    getLicenseDataAPI(dataRead)
                }
            } else if (intent.action == ACTION_SCAN_ERROR) {
                // Handle error
                println("Scan onreceive Error")
            }
        }
    }

    private fun registerReceiver() {
        val filter = IntentFilter()
        filter.addAction(ACTION_SCAN_SUCCESS)
        filter.addAction(ACTION_SCAN_ERROR)
        registerReceiver(scanResult, filter)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_auth, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        /*if (auth_type == READ_CARD_BY_SEARCH) {
            menu!!.findItem(R.id.showIDNumber).setVisible(true)
        } else {
            menu!!.findItem(R.id.showIDNumber).setVisible(false)
        }*/
        return super.onPrepareOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
            /*R.id.showIDNumber -> {
                showSearchOption()
            }
            R.id.licenseType -> {
                openLicenseCatBSD(true)
            }*/
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {

        when (v!!.id) {
            R.id.fabBarcodeAuth -> {
                /*val scanResult = "07-16487"
                if (!isNetworkAvailable(this@AuthActivity)) {
                    toast(this@AuthActivity, getString(R.string.network_error))
                } else {
                    getLicenseDataAPI(scanResult)
                }*/
                if (CpcOs.isC5()) {
                    startScan()
                }
                // scan barcode for the C71 device
                if (Build.MODEL.equals("C71"))
                    ScanBarcode()
            }
        }

    }

    override fun onTagDiscovered(tag: Tag?) {

        runOnUiThread(Runnable {
            kotlin.run {
                hideAnimation1()
                showAVILoaderIndicator()
            }
        })



        System.out.println("tag Discovered")
        Log.d(TAG, "New tag discovered$tag")
        val id = tag!!.getId()
        Log.d(TAG, "TAG ID:$id")
        val total_length = id.size - 1
        Log.d("TAG", "tag " + id + " tag length " + total_length)
        /*if (!tag.techList.contains("android.nfc.tech.MifareClassic")) {
            runOnUiThread(Runnable {
                kotlin.run {
                    hideAVILoaderIndicator()
                    showSnackBar("Card is not valid!!")
                }
            })

        }*/
        // Read card first with tag based on authentication key
        val mifareClassic = MifareClassic.get(tag)
        val isoDep = IsoDep.get(tag)

        //Log.d(TAG, "Mifare CLassic : " + mifareClassic!!)
        if (mifareClassic != null) {
            try {
                mifareClassic.connect()
                Log.d(TAG, "Transceive Length: " + mifareClassic.maxTransceiveLength)
                Log.d(TAG, "Selector count: " + mifareClassic.sectorCount)
                val blockCount = mifareClassic.blockCount
                Log.d(TAG, "SectorToBlock: " + mifareClassic.sectorToBlock(blockCount))

                var auth = false
                // 5.2) and get the number of sectors this card has..and loop thru these sectors
                val secCount = mifareClassic.sectorCount
                var bCount = 0
                var bIndex = 0
                for (j in 0 until NFCUtil.SECTOR_COUNT_FOR_MIFARE) {
                    // 6.1) authenticate the sector
                    //auth = mifareClassic.authenticateSectorWithKeyB(j, NFCUtil.MIFARE_AUTH_KEY)
                    auth = NFCUtil.mifareKeyAuthentication(mifareClassic, j)

                    if (auth) {
                        // 6.2) In each sector - get the block count
                        bCount = mifareClassic.getBlockCountInSector(j)
                        bIndex = 0
                        for (i in 0 until NFCUtil.BYTE_COUNT_FOR_MIFARE) {
                            bIndex = mifareClassic.sectorToBlock(j)
                            // 6.3) Read the block
                            val data = mifareClassic.readBlock(bIndex)
                            // 7) Convert the data into a string from Hex format.
                            csnNumber = NFCUtil.ByteArrayToHexString(data, NFCUtil.CARD_TYPE_MIFARE, this@StaffAuthActivity)
                            Log.d(TAG, csnNumber)//, data.length));
                            bIndex++
                        }
                    } else { // Authentication failed - Handle it using directly reading TAG
                        Log.d(TAG, "Authentication Failed!")
                        csnNumber = NFCUtil.ByteArrayToHexString(id, NFCUtil.CARD_TYPE_MIFARE, this@StaffAuthActivity)
                    }
                }

            } catch (ex: TagLostException) {
                ex.printStackTrace()
                runOnUiThread(Runnable {
                    kotlin.run {
                        hideAVILoaderIndicator()
                        viewPager.adapter!!.notifyDataSetChanged()
                        if (auth_type == READ_CARD_OFFLINE) {
                            showAnimation1()
                        } else if (auth_type == READ_BARCODE) {
                            showAnimation2()
                        }
                    }
                })
            } catch (ex: IOException) {
                Log.d(TAG, ""+ex.printStackTrace())
            }
        } else if (isoDep != null) {
            try {
                csnNumber = NFCUtil.ByteArrayToHexStringDesfire(tag.id, this@StaffAuthActivity)
            } catch (ex : TagLostException) {
                ex.printStackTrace()
                runOnUiThread(Runnable {
                    kotlin.run {
                        hideAVILoaderIndicator()
                        viewPager.adapter!!.notifyDataSetChanged()
                        if (auth_type == READ_CARD_OFFLINE) {
                            showAnimation1()
                        } else if (auth_type == READ_BARCODE) {
                            showAnimation2()
                        }
                    }
                })
            }
        }  else {
            runOnUiThread(Runnable {
                kotlin.run {
                    hideAVILoaderIndicator()
                    showSnackBar("Card is not valid!!")
                }
            })
        }

        if (csnNumber != null && csnNumber.isNotEmpty() && NFCUtil.checkAlphNumericString(csnNumber)) {
            Log.d(TAG, "Card Number: $csnNumber")

            runOnUiThread(Runnable { kotlin.run {
                if (auth_type == READ_CARD_OFFLINE) {
                    readCardData(mifareClassic)
                } else if (auth_type == READ_CARD_NUMBER_OFF) {
                    var cardData: StaffInfoModel.DataBean = db.getStaffByCardNumber(csnNumber);

                    if (cardData!! != null) {
                        if(cardData.cardNumber != null) {
                            cardData?.let {

                                hideAVILoaderIndicator()

                                /*if (onAuthDataReceivedPI != null) {
                                onAuthDataReceivedPI.onAuthApiDataReceived(cardData)
                            }*/

                                var baseUrl = SharedPref.getStringValue(this@StaffAuthActivity, keyBaseUrl, "")
                                if (onStaffAuthDataReceivedLI != null) {
                                    onStaffAuthDataReceivedLI.onStaffAuthApiDataReceived(cardData)
                                    //System.out.println("Student Photo ;  "+""+cardData.get(0).studentImages!!.replace("~", imageBaseUrl))
                                    setCardHolderImageIntoPelleteUrl("" + cardData.image64byte)
                                    if (cardData.cardstatus != null) {
                                        cardSt = Integer.parseInt(cardData.cardstatus)
                                        setCardStatus(this@StaffAuthActivity, cardSt)
                                    }
                                }

                                if (onStaffAuthDataReceivedDA != null) {
                                    onStaffAuthDataReceivedDA.onStaffAuthApiDataReceived(cardData)
                                    //System.out.println("Student Photo ;  "+""+cardData.get(0).studentImages!!.replace("~", imageBaseUrl))
                                    setCardHolderImageIntoPelleteUrl("" + cardData.image64byte)
                                    if (cardData.cardstatus != null) {
                                        cardSt = Integer.parseInt(cardData.cardstatus)
                                        setCardStatus(this@StaffAuthActivity, cardSt)
                                    }
                                }


                                /*if (onAuthDataReceivedAI != null) {
                                onAuthDataReceivedAI.onAuthApiDataReceived(cardData)
                            }*/
                                //subCatData = userDataResponse!!
                            }
                        } else {
                            hideAVILoaderIndicator()
                            viewPager.adapter!!.notifyDataSetChanged()
                            warningMessage(this@StaffAuthActivity, "Unknown card found, please contact to administrator!")
                        }
                    } else {
                        hideAVILoaderIndicator()
                        viewPager.adapter!!.notifyDataSetChanged()
                        //Boast.makeText(this@StaffAuthActivity, R.string.no_data, Toast.LENGTH_LONG).show()
                        warningMessage(this@StaffAuthActivity, "Unknown card found, please contact to administrator!")
                    }

                } else if (auth_type == READ_CARD_NUMBER) {
                    if (!isNetworkAvailable(this@StaffAuthActivity)) {
                        //toast(this@AuthActivity, getString(R.string.network_error))
                        runOnUiThread(Runnable {
                            kotlin.run {
                                hideAVILoaderIndicator()
                                viewPager.adapter!!.notifyDataSetChanged()
                            }
                        })
                        showSnackBar(getString(R.string.network_error))
                    } else {
                        getLicenseDataAPI(csnNumber)
                    }
                }
            }
            })
        } else {
            Log.d(TAG, "CardNumber null")
        }

    }

    fun showAVILoaderIndicator() {
        //htab_appbar.addOnOffsetChangedListener(null)
        // hide loader and show all views
        rlLoadElements.visibility = View.VISIBLE
        htab_appbar.visibility = View.GONE
        viewPager.visibility = View.GONE
    }

    fun hideAVILoaderIndicator () {
        //htab_appbar.addOnOffsetChangedListener(null)
        // hide loader and show all views
        rlLoadElements.visibility = View.GONE
        htab_appbar.visibility = View.VISIBLE
        viewPager.visibility = View.VISIBLE
    }

    fun showAnimation1() {
        //htab_appbar.addOnOffsetChangedListener(null)

        val animRightToCentre = AnimationUtils.loadAnimation(this, R.anim.anim_slide_in_middle_from_right)
        ivCard.startAnimation(animRightToCentre)

        val animLeftToCentre = AnimationUtils.loadAnimation(this, R.anim.anim_slide_in_middle_from_left)
        ivDevice.startAnimation(animLeftToCentre)//animation = animLeftToCentre

        rlAnim.visibility = View.VISIBLE
        rlIDNumber.visibility = View.GONE
        /*rlLoadElements.visibility = View.GONE
        htab_appbar.visibility = View.GONE
        viewPager.visibility = View.GONE*/
    }

    fun showAnimation2() {

        //htab_appbar.addOnOffsetChangedListener(null)
        // set margin top to position card for barcode
        val param = ivCard.layoutParams as RelativeLayout.LayoutParams
        param.setMargins(0,-50,0,0)
        ivCard.layoutParams = param

        val animRightToCentre = AnimationUtils.loadAnimation(this, R.anim.slide_top_centre)
        ivCard.startAnimation(animRightToCentre)

        val animLeftToCentre = AnimationUtils.loadAnimation(this, R.anim.slide_bottom_centre)
        ivDevice.startAnimation(animLeftToCentre)//animation = animLeftToCentre

        rlAnim.visibility = View.VISIBLE
        rlIDNumber.visibility = View.GONE

        ivDevice.animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {

            }

            override fun onAnimationEnd(animation: Animation) {
                /*if (auth_type == READ_CARD_OFFLINE) {
                    hideAnimation1()
                } else*/ if (auth_type == READ_BARCODE) {
                    hideAnimation2()  //@kns.p : onbackpress dialog is hide automatically
                }
            }

            override fun onAnimationRepeat(animation: Animation) {

            }
        })

        /*rlLoadElements.visibility = View.GONE
        htab_appbar.visibility = View.GONE
        htab_viewpager.visibility = View.GONE*/
    }

    fun hideAnimation1() {
        ivCard.clearAnimation()
        ivDevice.clearAnimation()

        rlAnim.visibility = View.GONE

        //htab_appbar.addOnOffsetChangedListener(offsetChangeListener(toolbar))
        /*rlLoadElements.visibility = View.VISIBLE
        htab_appbar.visibility = View.GONE
        htab_viewpager.visibility = View.GONE*/
    }

    fun hideAnimation2() {
        ivCard.clearAnimation()
        ivDevice.clearAnimation()

        rlAnim.visibility = View.GONE
        fabBarcodeAuth.visibility = View.VISIBLE

        //htab_appbar.addOnOffsetChangedListener(offsetChangeListener(toolbar))
    }

    fun showSearchOption() {
        rlIDNumber.visibility = View.VISIBLE
        rgStaff.visibility = View.VISIBLE
        /*htab_appbar.visibility = View.GONE
        htab_viewpager.visibility = View.GONE*/
        rlAnim.visibility = View.GONE
    }

    fun hideSearchOption() {
        rlIDNumber.visibility = View.GONE
        rgStaff.visibility = View.GONE
        /*htab_appbar.visibility = View.VISIBLE
        htab_viewpager.visibility = View.VISIBLE*/
        rlAnim.visibility = View.GONE
    }

    fun readCardData (mifareClassic : MifareClassic) {
        for (i in 0 until mifare_json_array.length()) {
            try {
                val obj = mifare_json_array.getJSONObject(i)
                fieldName = obj.getString("Fieldname")
                val fieldType = obj.getString("Fieldtype")

                Log.d(TAG, "Fieldname: $fieldName Fieldtype: $fieldType")
                val dataArray = obj.getJSONArray("data")
                fieldValues = ""
                val map = MifareSettingModel()

                val list_byte = ArrayList<ByteArray>()

                for (j in 0 until dataArray.length()) {
                    val dataObj = dataArray.getJSONObject(j)
                    val sectorStr = dataObj.getString("Sector")
                    if (dataArray.length() > 1) {
                        var auth = false
                        val sector = Integer.parseInt(sectorStr)
                        auth = NFCUtil.mifareKeyAuthentication(mifareClassic, sector)//mifareClassic.authenticateSectorWithKeyB(sector, MifareClassic.KEY_DEFAULT)
                        if (auth) {
                            val blocksStr = dataObj.getString("Blocks")
                            if (blocksStr.contains(",")) {
                                val blocksArr = blocksStr.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                                for (k in blocksArr.indices) {
                                    val block = Integer.parseInt(blocksArr[k])
                                    //Log.d(TAG, "Sector " + sector + " Block " + block);
                                    val blockByteData = mifareClassic.readBlock(block)
                                    var blockData = NFCUtil.ByteArrayToHexString(blockByteData)
                                    /*if (fieldName.equals("FullNeng")) {
                                        blockData = URLDecoder.decode(blockData, "UTF-8")
                                    }*/

                                    fieldValues = fieldValues + blockData
                                    // check wheter the card is student card or not
                                    if (i == 0 && fieldName.equals(keyStaffCode) && !fieldValues.contains("AAU")) {
                                        runOnUiThread(Runnable {
                                            kotlin.run {
                                                hideAVILoaderIndicator()
                                                viewPager.adapter!!.notifyDataSetChanged()
                                                warningMessage(this@StaffAuthActivity, "Unknown card found, please contact to administrator!")
                                            }
                                        })
                                        return
                                    }

                                    /*if(fieldName.equals(Helper.EMP_PHOTO)) {
                                    list_byte.add(blockByteData);
                                }*/
                                }

                            } else {
                                val block = Integer.parseInt(blocksStr)
                                //Log.d(TAG, "Sector " + sector + " Block " + block);
                                val blockByteData = mifareClassic.readBlock(block)
                                var blockData = NFCUtil.ByteArrayToHexString(blockByteData)
                                /*if (fieldName.equals("FullNeng")) {
                                    blockData = URLDecoder.decode(blockData, "UTF-8")
                                }*/
                                fieldValues = fieldValues + blockData

                                /*if(fieldName.equals(Helper.EMP_PHOTO)) {
                                list_byte.add(blockByteData);
                            }*/
                            }
                            //System.out.println("Field Values " + hexToString(fieldValues));
                        } else {
                            Log.d(TAG, "If Authetication fail")
                            runOnUiThread(Runnable {
                                kotlin.run {
                                    hideAVILoaderIndicator()
                                    viewPager.adapter!!.notifyDataSetChanged()
                                    warningMessage(this@StaffAuthActivity, "Unknown card found, please contact to administrator!")
                                }
                            })
                        }
                    } else {
                        var auth = false
                        val sector = Integer.parseInt(sectorStr)
                        auth = NFCUtil.mifareKeyAuthentication(mifareClassic, sector)//mifareClassic.authenticateSectorWithKeyB(sector, MifareClassic.KEY_DEFAULT)
                        if (auth) {
                            val blocksStr = dataObj.getString("Blocks")
                            if (blocksStr.contains(",")) {
                                val blocksArr = blocksStr.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                                for (k in blocksArr.indices) {
                                    val block = Integer.parseInt(blocksArr[k])
                                    //Log.d(TAG, "Sector " + sector + " Block " + block);
                                    val blockByteData = mifareClassic.readBlock(block)
                                    var blockData = NFCUtil.ByteArrayToHexString(blockByteData)
                                    /*if (fieldName.equals("FullNeng")) {
                                        Log.d(TAG, "Encoded Value: "+blockData)
                                        blockData = String(blockByteData, StandardCharsets.UTF_8)
                                        Log.d(TAG, "Decoded Value: "+blockData.trim())
                                    }*/
                                    fieldValues = fieldValues + blockData

                                    /*if(fieldName.equals(Helper.EMP_PHOTO)) {
                                    list_byte.add(blockByteData);
                                }*/
                                }

                            } else {
                                val block = Integer.parseInt(blocksStr)
                                //Log.d(TAG, "Sector " + sector + " Block " + block);
                                val blockByteData = mifareClassic.readBlock(block)
                                var blockData = NFCUtil.ByteArrayToHexString(blockByteData)
                                /*if (fieldName.equals("FullNameAhm")) {
                                    blockData = URLDecoder.decode(NFCUtil.hexToString(blockData), "UTF-8")
                                }*/
                                /*if(fieldName.equals(Helper.EMP_PHOTO)) {
                                list_byte.add(blockByteData);
                            }*/
                                fieldValues = fieldValues + blockData
                            }
                            //System.out.println("Field Values " + hexToString(fieldValues));
                        } else {
                            Log.d(TAG, "Else Authetication fail")
                            runOnUiThread(Runnable {
                                kotlin.run {
                                    hideAVILoaderIndicator()
                                    viewPager.adapter!!.notifyDataSetChanged()
                                    warningMessage(this@StaffAuthActivity, "Unknown card found, please contact to administrator!")
                                }
                            })
                        }
                    }
                }

                val finalValues = fieldValues
                map.setFieldName(fieldName)
                map.setFieldValue(finalValues)

                println("Final Field Values $finalValues")
                list.add(map)

            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (ex: JarException) {
                ex.printStackTrace()
            }

        }
        // CALL YOU API CODE OR FURTHER CODE TO USE THIS CARD SERIAL NUMBER
        runOnUiThread(Runnable {
            kotlin.run {
                hideAVILoaderIndicator()

                //tvCSN.text = csnNumber
                /*if (onAuthDataReceivedPI != null) {
                    onAuthDataReceivedPI.onAuthDataReceived(list, csnNumber)
                }*/

                if (onStaffAuthDataReceivedLI != null) {
                    val cardSt : Int = db.getCardStatusFromStaffCardNo(csnNumber)
                    setCardStatus(this@StaffAuthActivity, cardSt)
                    onStaffAuthDataReceivedLI.onStaffAuthDataReceived(list, csnNumber)
                }

                /*if (onAuthDataReceivedAI != null) {
                    onAuthDataReceivedAI.onAuthDataReceived(list, csnNumber)
                }*/

                for (cardData in list) {
                    if (cardData.fieldName.equals("Subcategory")) {
                        //subCatData.subCategory = cardData.fieldValue
                    }
                }
            }
        })
    }

    fun getLicenseDataAPI(value: String) {
        if (auth_type == READ_CARD_BY_SEARCH) {
            hideSearchOption()
        }
        showAVILoaderIndicator()

        var rbId : Int = rgStaff.checkedRadioButtonId

        var jObj : JsonObject = JsonObject()

        jObj.addProperty("Token", SharedPref.getAuthToken(this))
        //jObj.addProperty("NextPageIndex", "1")

        var jData : JsonObject = JsonObject()

        var staffType: String ?= null
        when(rbId) {
            R.id.rbAppNo -> {
                staffType = "Application_No"
            }
            R.id.rbSlNo -> {
                staffType = "SL_No"
            }
            R.id.rbUid -> {
                staffType = "UID"
            }
            R.id.rbStaffId -> {
                staffType = "Staff_Id"
            }
            R.id.rbCardNo -> {
                staffType = "cardnumber"
            }
        }

        // if auth type is read card number then pass only card number
        if (auth_type == READ_CARD_NUMBER) {
            staffType = "cardnumber"
        }

        if (auth_type == READ_BARCODE) {
            staffType = "Staff_Id"
        }

        jData.addProperty(staffType, value)
        jData.addProperty("CurrentPage", 1)
        jObj.add("data", jData)
        System.out.println("JObj: "+jObj.toString())

        var baseUrl = SharedPref.getStringValue(this@StaffAuthActivity, keyAPIBaseUrl, "")

        RetrofitClientSingleton.getInstance(baseUrl).getStaffInfo(jObj)
                .enqueue(object : retrofit2.Callback<StaffInfoModel> {

                    override fun onFailure(call: Call<StaffInfoModel>, t: Throwable) {
                        stopProgress()
                        toast(this@StaffAuthActivity, getString(R.string.something_went_wrong))
                    }

                    override fun onResponse(call: Call<StaffInfoModel>, response: Response<StaffInfoModel>) {
                        stopProgress()

                        hideAVILoaderIndicator()

                        if (response.isSuccessful) {
                            val userDataResponse =  response.body()

                            //SharedPref.saveLogin(this@AuthActivity, userDataResponse!!)

                            when (userDataResponse!!.status == 1) {
                                true -> {
                                    if (auth_type == READ_CARD_BY_SEARCH) {
                                        tvIDNumberError.text = ""
                                    }
                                    val cardData: ArrayList<StaffInfoModel.DataBean>? = userDataResponse!!.data;
                                    System.out.println("CardData: "+ cardData)
                                    if (cardData!!.size > 0) {

                                        // save staff data to db
                                        db.saveStaffInfo(cardData, this@StaffAuthActivity)

                                        cardData?.let {

                                            hideAVILoaderIndicator()

                                            /*if (onAuthDataReceivedPI != null) {
                                            onAuthDataReceivedPI.onAuthApiDataReceived(cardData)
                                        }*/

                                            var baseUrl = SharedPref.getStringValue(this@StaffAuthActivity, keyBaseUrl, "")
                                            if (onStaffAuthDataReceivedLI != null) {
                                                onStaffAuthDataReceivedLI.onStaffAuthApiDataReceived(cardData.get(0))
                                                //System.out.println("Student Photo ;  "+""+cardData.get(0).studentImages!!.replace("~", imageBaseUrl))
                                                setCardHolderImageIntoPellete(""+cardData.get(0).image64byte)
                                                if (cardData.get(0).cardstatus != null) {
                                                    cardSt = Integer.parseInt(cardData.get(0).cardstatus)
                                                    setCardStatus(this@StaffAuthActivity, cardSt)
                                                }
                                            }

                                            if (onStaffAuthDataReceivedDA != null) {
                                                onStaffAuthDataReceivedDA.onStaffAuthApiDataReceived(cardData.get(0))
                                                //System.out.println("Student Photo ;  "+""+cardData.get(0).studentImages!!.replace("~", imageBaseUrl))
                                                setCardHolderImageIntoPellete(""+cardData.get(0).image64byte)
                                                if (cardData.get(0).cardstatus != null) {
                                                    cardSt = Integer.parseInt(cardData.get(0).cardstatus)
                                                    setCardStatus(this@StaffAuthActivity, cardSt)
                                                }
                                            }


                                            /*if (onAuthDataReceivedAI != null) {
                                            onAuthDataReceivedAI.onAuthApiDataReceived(cardData)
                                        }*/
                                            subCatData = userDataResponse!!
                                        }
                                    } else {
                                        hideAVILoaderIndicator()
                                        showSearchOption()
                                        viewPager.adapter!!.notifyDataSetChanged()
                                        //Boast.makeText(this@StaffAuthActivity, R.string.no_data, Toast.LENGTH_LONG).show()
                                        warningMessage(this@StaffAuthActivity, "Unknown card found, please contact to administrator!")
                                    }
                                }

                                false -> {
                                    viewPager.adapter!!.notifyDataSetChanged()
                                    if (userDataResponse?.status ==2) {
                                        hideAVILoaderIndicator()

                                        if (auth_type == READ_CARD_NUMBER) {
                                            showSnackBar(userDataResponse!!.message)
                                        }else if (auth_type == READ_CARD_BY_SEARCH) {
                                            showSearchOption()
                                            tvIDNumberError.text = userDataResponse!!.message
                                        }
                                    } else {
                                        hideAVILoaderIndicator()
                                        showSnackBar(userDataResponse!!.message)
                                    }
                                }
                            }
                            when (userDataResponse!!.status == 2) {
                                true -> {
                                    viewPager.adapter!!.notifyDataSetChanged()
                                    showSearchOption()
                                    showSnackBar(userDataResponse!!.message)
                                }
                            }
                            when (userDataResponse!!.status == 3) {
                                true -> {
                                    forceLogout(this@StaffAuthActivity)
                                }
                            }
                        } else {
                            //toast(this@AuthActivity, R.string.something_went_wrong)

                            if (auth_type == READ_CARD_NUMBER || auth_type == READ_CARD_BY_SEARCH || auth_type == READ_BARCODE) {
                                showSearchOption()
                                viewPager.adapter!!.notifyDataSetChanged()
                                tvIDNumberError.text = response!!.message()
                            }
                        }

                    }
                })
    }

    fun hideSoftKeypad () {
        val inputManager:InputMethodManager =getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(currentFocus.windowToken, InputMethodManager.SHOW_FORCED)
    }

    fun showSnackBar (msg: String?) {
        val snack = Snackbar.make(htab_maincontent,msg!!,Snackbar.LENGTH_LONG)
        snack.show()
    }

    fun setCardHolderImageIntoPellete (url: String) {

        val byteImage = Base64.decode(url, Base64.DEFAULT)
        val bmp = BitmapFactory.decodeByteArray(byteImage, 0, byteImage.size)
        Glide.with(this@StaffAuthActivity)
                .asBitmap()
                .load(bmp)
                .into(htab_header)

    }

    fun setCardHolderImageIntoPelleteUrl (staffUrl: String) {

        if (!staffUrl.isNullOrEmpty()) {
            var staffBmp: Bitmap = SaveImages.getInstance().loadImageFromStorage(staffUrl)


            Glide.with(this@StaffAuthActivity)
                    .asBitmap()
                    .load(staffBmp)
                    .placeholder(android.R.drawable.ic_menu_report_image)
                    .error(android.R.drawable.ic_menu_report_image)
                    .into(htab_header)
        }

    }

    public fun setCardStatus (mContext: Context, cardStCode : Int) {
        var cardColor: Int = R.color.colorPrimary
        var cardStStr: String? = ""
        var cardStIcon : Int = R.mipmap.ic_action_done
        if (cardStCode == CARD_ST_ACTIVE) {
            cardColor = R.color.colorGreen
            cardStStr = "Active"
            cardStIcon = R.mipmap.ic_action_done
        } else if (cardStCode == CARD_ST_REVOKED) {
            cardColor = R.color.colorGray
            cardStStr = "Revoked"
            cardStIcon = R.mipmap.ic_close_app
        } else if (cardStCode == CARD_ST_LOST) {
            cardColor = R.color.colorBlack
            cardStStr = "Lost"
            cardStIcon = R.mipmap.ic_close_app
        } else if (cardStCode == CARD_ST_SUSPENDED) {
            cardColor = R.color.colorYellow
            cardStStr = "Suspennded"
            cardStIcon = R.mipmap.ic_close_app
        } else if (cardStCode == CARD_ST_EXPIRED) {
            cardColor = R.color.colorRed
            cardStStr = "Expired"
            cardStIcon = R.mipmap.ic_close_app
        }

        htab_appbar.setBackgroundColor(resources.getColor(cardColor))
        collapsingToolbarLayout.setStatusBarScrimColor(resources.getColor(cardColor))
        collapsingToolbarLayout.setBackgroundColor(resources.getColor(cardColor))
        collapsingToolbarLayout.setContentScrimColor(resources.getColor(cardColor))
        collapsingToolbarLayout.setStatusBarScrimColor(resources.getColor(cardColor))
        mTvStuStatusLabel.text = cardStStr
        ivStuStatus.setImageResource(cardStIcon)
        rlStuStatus.setBackgroundColor(resources.getColor(cardColor))
    }


    override fun onChangeCardStatus(mContext: Context, cardStCode : Int) {
        var cardColor: Int = R.color.colorPrimary
        var cardStStr: String? = ""
        var cardStIcon : Int = R.mipmap.ic_action_done
        if (cardStCode == CARD_ST_ACTIVE) {
            cardColor = R.color.colorGreen
            cardStStr = "Active"
            cardStIcon = R.mipmap.ic_action_done
        } else if (cardStCode == CARD_ST_REVOKED) {
            cardColor = R.color.colorGray
            cardStStr = "Revoked"
            cardStIcon = R.mipmap.ic_close_app
        } else if (cardStCode == CARD_ST_LOST) {
            cardColor = R.color.colorBlack
            cardStStr = "Lost"
            cardStIcon = R.mipmap.ic_close_app
        } else if (cardStCode == CARD_ST_SUSPENDED) {
            cardColor = R.color.colorYellow
            cardStStr = "Suspennded"
            cardStIcon = R.mipmap.ic_close_app
        } else if (cardStCode == CARD_ST_EXPIRED) {
            cardColor = R.color.colorRed
            cardStStr = "Expired"
            cardStIcon = R.mipmap.ic_close_app
        }

        htab_appbar.setBackgroundColor(resources.getColor(cardColor))
        collapsingToolbarLayout.setStatusBarScrimColor(resources.getColor(cardColor))
        collapsingToolbarLayout.setBackgroundColor(resources.getColor(cardColor))
        collapsingToolbarLayout.setContentScrimColor(resources.getColor(cardColor))
        collapsingToolbarLayout.setStatusBarScrimColor(resources.getColor(cardColor))
        mTvStuStatusLabel.text = cardStStr
        ivStuStatus.setImageResource(cardStIcon)
        rlStuStatus.setBackgroundColor(resources.getColor(cardColor))
    }

    override fun onCardHolderImageIntoPellete(url: String?) {
        Glide.with(this@StaffAuthActivity)
                .load(url)
                .into(htab_header)
    }

}

