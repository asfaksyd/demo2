package com.smartportable.demo2.activities

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Message
import com.google.android.material.snackbar.Snackbar

import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.fineapp.android.retrofitClient.RetrofitClientSingleton
import com.google.gson.JsonObject
import com.smartportable.demo2.HomeScreen
import com.smartportable.demo2.R
import com.smartportable.demo2.api.URLCollections
import com.smartportable.demo2.db.DatabaseHandler
import com.smartportable.demo2.helper.Boast
import com.smartportable.demo2.prefs.MyPreferencesActivity
import com.smartportable.demo2.utils.*
import kotlinx.android.synthetic.main.activity_reader_list.*
import retrofit2.Call
import retrofit2.Response
import android.view.Menu
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection
import com.smartportable.demo2.adapters.VisitorInfoAdapter
import com.smartportable.demo2.models.VisitorInfoModel

class SyncVisitorInfoActivity : AppCompatActivity() , SwipyRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener {
    private var hStopRequest: Handler? = null
    var db: DatabaseHandler? = null
    //var apiRetrofit: Call<StudentInfoModel>? = null
    var visitorList = ArrayList<VisitorInfoModel.DataBean?>()
    var llVisitorRecord : LinearLayout?= null
    var tvSyncVisitorRecord : TextView ?= null
    var srlVisitorList : SwipyRefreshLayout?= null
    var rvVisitorList : RecyclerView?= null
    var visitorInfoAdapter : VisitorInfoAdapter? = null
    var fm: FragmentManager = supportFragmentManager
    var searchView: SearchView? = null
    var currentPageIndex : Int = 0
    var totalPageCount : Int = 0
    var retrofitObj = null
    var tvNoVisData: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reader_list)

        init()

    }



    override fun onCreateOptionsMenu(menu : Menu) : Boolean {

        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_sync_student, menu)

        val searchItem = menu!!.findItem(R.id.action_search)

        val searchManager = this@SyncVisitorInfoActivity.getSystemService(Context.SEARCH_SERVICE) as SearchManager


        if (searchItem != null) {
            searchView = searchItem!!.getActionView() as SearchView
        }
        if (searchView != null) {
            searchView!!.setSearchableInfo(searchManager.getSearchableInfo(this@SyncVisitorInfoActivity.getComponentName()))

            searchView!!.setOnQueryTextListener(this)
        }
        return super.onCreateOptionsMenu(menu)
    }

    fun onBackPressOperation()
    {
        if (!searchView!!.isIconified) {
            searchView!!.onActionViewCollapsed()
            if (db!!.checkVisitorInfo()) {
                showVisitorInfoFromDB(db!!)
            }
        } else {
            //super.onBackPressed()
            this@SyncVisitorInfoActivity.finish()
            if (URLCollections.whichDirection(this@SyncVisitorInfoActivity) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left)
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right)
            }

        }
    }



    override fun onBackPressed() {

        Log.e("stopRequest","stopRequest--incomingMsg--"+srlVisitorList!!.isRefreshing )
        if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlVisitorList!!.isRefreshing)
        {
            stopRequest(this@SyncVisitorInfoActivity,hStopRequest!!)
        }
        else
        {
            onBackPressOperation()
        }



/*
        if (!searchView!!.isIconified) {
            searchView!!.onActionViewCollapsed()
            if (db!!.checkStudentInfo()) {
                showStudentInfoFromDB(db!!)
            }
        } else {
            //super.onBackPressed()
            this@SyncStudentInfoActivity.finish()
            if (URLCollections.whichDirection(this@SyncStudentInfoActivity) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left)
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right)
            }

        }*/
    }
    fun init() {
        llVisitorRecord = findViewById(R.id.llStudentRecord) as LinearLayout
        tvSyncVisitorRecord = findViewById(R.id.tvSyncStudentRecord) as TextView
        srlVisitorList = findViewById(R.id.srlReaderList) as SwipyRefreshLayout
        rvVisitorList = findViewById(R.id.rvReaderList) as RecyclerView
        //rvReaderList?.setHasFixedSize(true)
        rvVisitorList?.setLayoutManager(LinearLayoutManager(this))
        visitorInfoAdapter = VisitorInfoAdapter(this@SyncVisitorInfoActivity, visitorList!!, fm)
        rvVisitorList?.adapter = visitorInfoAdapter
        //readersAdapter.notifyDataSetChanged()
        tvNoVisData = findViewById(R.id.tvNoVisData) as TextView

        // recyclerview scroll
        var check_ScrollingUp :Boolean = false

        rvVisitorList!!.addOnScrollListener( object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    // Scrolling up
                    if(check_ScrollingUp)
                    {
                        llVisitorRecord!!.startAnimation(AnimationUtils.loadAnimation(this@SyncVisitorInfoActivity,R.anim.trans_downwards))
                        llVisitorRecord!!.visibility = View.VISIBLE
                        check_ScrollingUp = false
                    }

                } else {
                    // User scrolls down
                    if(!check_ScrollingUp )
                    {
                        llVisitorRecord!!
                                .startAnimation(AnimationUtils
                                        .loadAnimation(this@SyncVisitorInfoActivity,R.anim.trans_upwards))
                        llVisitorRecord!!.visibility = View.GONE
                        check_ScrollingUp = true

                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

        })

        db = DatabaseHandler(this@SyncVisitorInfoActivity)
        if (db!!.checkVisitorInfo()) {
            showVisitorInfoFromDB(db!!)
        } else {
            if (isNetworkAvailable(this@SyncVisitorInfoActivity)) {
                val url = SharedPref.getStringValue(this@SyncVisitorInfoActivity, keyBaseUrl, "")
                if (url == null || url == "" || !URLCollections.isValidUrl(url)) run {

                    val snackbar = Snackbar
                            .make(llReaderList!!, resources.getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                            .setAction(resources.getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener)
                    snackbar.setActionTextColor(Color.GREEN)
                    val snackbarView = snackbar.view
                    snackbarView.setBackgroundColor(Color.DKGRAY)
                    val textView = snackbarView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
                    textView.setTextColor(Color.WHITE)
                    snackbar.show()
                } else {
                    //srlAccessAreaList!!.post(Runnable { srlAccessAreaList!!.isRefreshing = true })
                    //showRefreshProgressBar(srlAccessAreaList!!)
                    currentPageIndex = 1
                    SyncVisitorInfoAPI("0", "0")
                }
            } else {
                //srlAccessAreaList!!.post(Runnable { srlAccessAreaList!!.isRefreshing = false })
                //hideRefreshProgressBar(srlAccessAreaList!!)
                Boast.makeText(this@SyncVisitorInfoActivity, R.string.no_conn).show()
            }
        }

        srlVisitorList?.setOnRefreshListener(this)

        customToastOnTop(this@SyncVisitorInfoActivity, "Swipe down to refresh")

        //Realm.init(this)

        hStopRequest = object:Handler()
        {
            override fun handleMessage(msg: Message)
            {
                super.handleMessage(msg)
                Log.e("stopRequest","stopRequest--incomingMsg--"+msg.data.get("isOkClicked")+"<<>>"+ srlVisitorList!!.isRefreshing )
                if (msg.data.get("isOkClicked") === true)
                {
                    if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlVisitorList!!.isRefreshing)
                    {
                        db = null
                        apiRetrofit!!.cancel()

                        onBackPressOperation()
                    }
                    else
                    {
                        onBackPressOperation()
                    }

                }
                else
                {
                    //onBackPressOperation()
                }

            }

        }

    }


    override fun onRefresh(direction: SwipyRefreshLayoutDirection?) {
        if (isNetworkAvailable(this@SyncVisitorInfoActivity)) {
            val url = SharedPref.getStringValue(this@SyncVisitorInfoActivity, keyBaseUrl, "")
            if (url == null || url == "" || !URLCollections.isValidUrl(url)) run {

                val snackbar = Snackbar
                        .make(llReaderList!!, resources.getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                        .setAction(resources.getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener)
                snackbar.setActionTextColor(Color.GREEN)
                val snackbarView = snackbar.view
                snackbarView.setBackgroundColor(Color.DKGRAY)
                val textView = snackbarView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
                textView.setTextColor(Color.WHITE)
                snackbar.show()
            } else {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    currentPageIndex = 1
                } else {
                    currentPageIndex++
                }
                SyncVisitorInfoAPI("0", "0")
            }

        } else {
            srlVisitorList!!.isRefreshing = false
            Boast.makeText(this@SyncVisitorInfoActivity, getString(R.string.no_conn)).show()
        }
    }

    var mOnSnackBarSetUrlClickListener: View.OnClickListener = View.OnClickListener {
        println("SnackBar Set URL Click")
        val int_set_url = Intent(this@SyncVisitorInfoActivity, MyPreferencesActivity::class.java)
        int_set_url.putExtra(HomeScreen.ACTION_STR, HomeScreen.SET_URL)
        startActivity(int_set_url)
    }

    var apiRetrofit: Call<VisitorInfoModel>? = null

    fun SyncVisitorInfoAPI(visCardNo : String, visRegNo : String) {
        //showProgressDialog(this@SyncReadersActivity)
        if (currentPageIndex !=1 && currentPageIndex >= totalPageCount) {
            warningMessage(this@SyncVisitorInfoActivity, "All Visitors Records has been synced.")
            srlVisitorList!!.isRefreshing = false
            return
        }
        srlVisitorList!!.isRefreshing = true
        val jObj : JsonObject = JsonObject()

        jObj.addProperty("Token", SharedPref.getAuthToken(this))

        var jData : JsonObject = JsonObject()
        jData.addProperty("VisitorCardNumber", visCardNo)
        jData.addProperty("VisitorRegNo", visRegNo)
        jData.addProperty("CurrentPage", currentPageIndex)
        jObj.add("data", jData)
        System.out.println("JObj: "+jObj.toString())

        var baseUrl = SharedPref.getStringValue(this@SyncVisitorInfoActivity, keyAPIBaseUrl, "")


        var abc = RetrofitClientSingleton.getInstance(baseUrl)
        apiRetrofit = abc.getVisitorInfo(jObj)


        apiRetrofit!!.enqueue(object : retrofit2.Callback<VisitorInfoModel>
        {

            override fun onFailure(call: Call<VisitorInfoModel>?, t: Throwable?)
            {
                //stopProgress()
                srlVisitorList!!.isRefreshing = false
            }

            override fun onResponse(call: Call<VisitorInfoModel>?, response: Response<VisitorInfoModel>?) {
                //stopProgress()
                srlVisitorList!!.isRefreshing = false
                Log.e("SyncStudentInfoActivity", "SyncStudentInfoActivity>>response>>>> "+response)
                if (response!!.isSuccessful) {
                    val visitorInfo: VisitorInfoModel? = response!!.body()

                    if (visitorInfo!!.status == 1) {
                        //System.out.println("Sync Reader : "+readerInfo.status.toString())
                        //System.out.println("Sync Reader : "+readerInfo!!.data?.get(0)?.readerName)
                        if (visitorInfo!!.pageCount!!.contains("/")) {
                            var pageCountArr  = visitorInfo!!.pageCount!!.split("/")
                            totalPageCount = Integer.parseInt(pageCountArr[1])
                        }
                        val data: ArrayList<VisitorInfoModel.DataBean>? = visitorInfo!!.data
                        if (data != null) {


                            db  = DatabaseHandler(this@SyncVisitorInfoActivity)
                            if (currentPageIndex == 1) {
                                db!!.deleteAllVisitors()
                            }

                            db!!.saveVisitorInfo(this@SyncVisitorInfoActivity, data)

                            showVisitorInfoFromDB(db!!)

                            if (currentPageIndex < totalPageCount) {
                                onRefresh(SwipyRefreshLayoutDirection.BOTTOM)
                            }

                        } else {
                            toast(this@SyncVisitorInfoActivity, visitorInfo!!.message)
                            if (currentPageIndex == 1) {
                                db!!.deleteAllVisitors()
                            }
                            showVisitorInfoFromDB(db!!)
                        }
                    } else if (visitorInfo.status == 2) {
                        if (currentPageIndex == 1) {
                            db!!.deleteAllVisitors()
                        }
                        showVisitorInfoFromDB(db!!)
                    } else if (visitorInfo.status == 3) {
                        forceLogout(this@SyncVisitorInfoActivity)

                    } else {
                        toast(this@SyncVisitorInfoActivity, visitorInfo!!.message)
                    }
                }
            }


        })

    }

    fun showVisitorInfoFromDB (db : DatabaseHandler) {


        var data1 : ArrayList<VisitorInfoModel.DataBean?> = db.allVisitorInfo
        System.out.println("Visitor List: "+data1.size)

        if (data1.size == 0) {
            tvNoVisData!!.visibility = View.VISIBLE
            rvVisitorList!!.visibility = View.GONE
        } else {
            tvNoVisData!!.visibility = View.GONE
            rvVisitorList!!.visibility = View.VISIBLE
        }

        tvSyncVisitorRecord!!.text = "Total Sync Visitors Records"+data1.size

        visitorInfoAdapter!!.updateVisitorInfo(data1, false)
        visitorInfoAdapter!!.notifyDataSetChanged()
    }

    override fun onQueryTextSubmit(searchKeyword: String?): Boolean {
        val db = DatabaseHandler(this@SyncVisitorInfoActivity)
        var data1 = db.getVisitorsBySearch(searchKeyword)
        visitorInfoAdapter!!.updateVisitorInfo(data1, false)
        visitorInfoAdapter!!.notifyDataSetChanged()
        return true
    }

    override fun onQueryTextChange(p0: String?): Boolean {

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {

            if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlVisitorList!!.isRefreshing)
            {
                stopRequest(this@SyncVisitorInfoActivity,hStopRequest!!)
            }
            else
            {
                onBackPressOperation()
            }

            /*if (!searchView!!.isIconified) {
                searchView!!.onActionViewCollapsed()
                if (db!!.checkStudentInfo()) {
                    showStudentInfoFromDB(db!!)
                }
                return true
            }  else {
                this@SyncStudentInfoActivity.finish()
                if (URLCollections.whichDirection(this@SyncStudentInfoActivity) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left)
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right)
                }
                return true
            }*/
        }
        return true
    }

    override fun onDestroy() {
        super.onDestroy()

        hStopRequest!!.removeCallbacks(null)

    }

}
