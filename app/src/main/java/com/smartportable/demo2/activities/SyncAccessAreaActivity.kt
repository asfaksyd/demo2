package com.smartportable.demo2.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fineapp.android.retrofitClient.RetrofitClientSingleton
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection
import com.smartportable.demo2.HomeScreen
import com.smartportable.demo2.R
import com.smartportable.demo2.SyncActivity
import com.smartportable.demo2.adapters.AccessAreaAdapter
import com.smartportable.demo2.api.URLCollections
import com.smartportable.demo2.db.DatabaseHandler
import com.smartportable.demo2.helper.Boast
import com.smartportable.demo2.models.AGInfoModel
import com.smartportable.demo2.prefs.MyPreferencesActivity
import com.smartportable.demo2.utils.*
import kotlinx.android.synthetic.main.activity_access_area.*
import kotlinx.android.synthetic.main.activity_reader_list.*
import retrofit2.Call
import retrofit2.Response
import java.util.ArrayList

/**
 * Created by sCampus on 1/3/2017.
 */

class SyncAccessAreasActivity : AppCompatActivity() , SwipyRefreshLayout.OnRefreshListener {
    private var hStopRequest: Handler? = null
    lateinit var db: DatabaseHandler
    var apiRetrofit: Call<AGInfoModel>? = null
    var accessAreaList = ArrayList<AGInfoModel.DataBean?>()
    var srlAccessAreaList : SwipyRefreshLayout?= null
    var rvAccessAreaList : RecyclerView?= null
    var accessAreaInfoAdapter : AccessAreaAdapter? = null
    var fm: FragmentManager = supportFragmentManager
    var searchView: SearchView? = null
    var currentPageIndex : Int = 0
    var totalPageCount : Int = 0
    var retrofitObj = null
    var tvNoVisData: TextView? = null

    var mOnSnackBarClickListener: View.OnClickListener = View.OnClickListener {
        println("SnackBar Refreshed Click")
        SyncAccessAreaInfoAPI("0")
    }

    var mOnSnackBarSetUrlClickListener: View.OnClickListener = View.OnClickListener {
        println("SnackBar Set URL Click")
        val int_set_url = Intent(this@SyncAccessAreasActivity, MyPreferencesActivity::class.java)
        int_set_url.putExtra(HomeScreen.ACTION_STR, HomeScreen.SET_URL)
        startActivity(int_set_url)
    }

    var mBtnOnClickListener: View.OnClickListener = View.OnClickListener {
        println("BtnOkCLick")
        setResult(SyncActivity.REQUEST_SYNC_AREAS)
        this@SyncAccessAreasActivity.finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reader_list)

        val ab = supportActionBar
        ab!!.setHomeButtonEnabled(true)

        db = DatabaseHandler(this)

        srlAccessAreaList = findViewById(com.smartportable.demo2.R.id.srlReaderList) as SwipyRefreshLayout
        rvAccessAreaList = findViewById<View>(R.id.rvReaderList) as RecyclerView
        rvAccessAreaList?.layoutManager = LinearLayoutManager(this)
        accessAreaInfoAdapter = AccessAreaAdapter(this@SyncAccessAreasActivity, accessAreaList)
        rvAccessAreaList?.adapter = accessAreaInfoAdapter

        srlAccessAreaList?.setOnRefreshListener(this)

        customToastOnTop(this@SyncAccessAreasActivity, "Swipe down to refresh")

        hStopRequest = object: Handler()
        {
            override fun handleMessage(msg: Message)
            {
                super.handleMessage(msg)
                Log.e("stopRequest","stopRequest--incomingMsg--"+msg.data.get("isOkClicked")+"<<>>"+ srlReaderList!!.isRefreshing )
                if (msg.data.get("isOkClicked") === true)
                {
                    if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlReaderList!!.isRefreshing)
                    {
                        //db = null
                        apiRetrofit!!.cancel()

                        onBackPressOperation()
                    }
                    else
                    {
                        onBackPressOperation()
                    }

                }
                else
                {
                    //onBackPressOperation()
                }

            }

        }

        if (!db.checkAGInfo()) {
            currentPageIndex = 1
            SyncAccessAreaInfoAPI("0")
        } else {
            showAccessAreasInfoFromDB(db)
        }

        if (db.checkAGInfo()) {
            showAccessAreasInfoFromDB(db)
        } else {
            if (isNetworkAvailable(this@SyncAccessAreasActivity)) {
                val url = SharedPref.getStringValue(this@SyncAccessAreasActivity, keyBaseUrl, "")
                if (url == null || url == "" || !URLCollections.isValidUrl(url)) run {

                    val snackbar = Snackbar
                            .make(rlAccessArea!!, resources.getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                            .setAction(resources.getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener)
                    snackbar.setActionTextColor(Color.GREEN)
                    val snackbarView = snackbar.view
                    snackbarView.setBackgroundColor(Color.DKGRAY)
                    val textView = snackbarView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
                    textView.setTextColor(Color.WHITE)
                    snackbar.show()
                } else {
                    currentPageIndex = 1
                    SyncAccessAreaInfoAPI("0")
                }
            } else {
                srlAccessAreaList!!.isRefreshing = false
                Boast.makeText(this@SyncAccessAreasActivity, R.string.no_conn).show()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        //menuInflater.inflate(R.menu.menu_cat_sync, menu)
        return super.onCreateOptionsMenu(menu)
    }

    fun SyncAccessAreaInfoAPI(agId : String) {
        //showProgressDialog(this@SyncReadersActivity)
        if (currentPageIndex !=1 && currentPageIndex >= totalPageCount) {
            warningMessage(this@SyncAccessAreasActivity, "All Access Areas Records has been synced.")
            srlAccessAreaList!!.isRefreshing = false
            return
        }
        srlAccessAreaList!!.isRefreshing = true
        val jObj : JsonObject = JsonObject()

        jObj.addProperty("Token", SharedPref.getAuthToken(this))

        var jData : JsonObject = JsonObject()
        jData.addProperty("AGId", agId)
        jData.addProperty("CurrentPage", currentPageIndex)
        jObj.add("data", jData)
        System.out.println("JObj: "+jObj.toString())

        var baseUrl = SharedPref.getStringValue(this@SyncAccessAreasActivity, keyAPIBaseUrl, "")


        var abc = RetrofitClientSingleton.getInstance(baseUrl)
        apiRetrofit = abc.getAGInfo(jObj)


        apiRetrofit!!.enqueue(object : retrofit2.Callback<AGInfoModel>
        {

            override fun onFailure(call: Call<AGInfoModel>?, t: Throwable?)
            {
                //stopProgress()
                srlAccessAreaList!!.isRefreshing = false
            }

            override fun onResponse(call: Call<AGInfoModel>?, response: Response<AGInfoModel>?) {
                //stopProgress()
                srlAccessAreaList!!.isRefreshing = false
                Log.e("SyncStudentInfoActivity", "SyncStudentInfoActivity>>response>>>> "+response)
                if (response!!.isSuccessful) {
                    val agInfo: AGInfoModel? = response.body()

                    if (agInfo!!.status == 1) {
                        //System.out.println("Sync Reader : "+readerInfo.status.toString())
                        //System.out.println("Sync Reader : "+readerInfo!!.data?.get(0)?.readerName)
                        if (agInfo!!.pageCount!!.contains("/")) {
                            var pageCountArr  = agInfo!!.pageCount!!.split("/")
                            totalPageCount = Integer.parseInt(pageCountArr[1])
                        }
                        val data: ArrayList<AGInfoModel.DataBean>? = agInfo!!.data
                        if (data != null) {


                            db  = DatabaseHandler(this@SyncAccessAreasActivity)
                            if (currentPageIndex == 1) {
                                db.deleteAllAGs()
                            }

                            db.saveAGInfo(this@SyncAccessAreasActivity, data)

                            showAccessAreasInfoFromDB(db)

                            if (currentPageIndex < totalPageCount) {
                                onRefresh(SwipyRefreshLayoutDirection.BOTTOM)
                            }

                        } else {
                            toast(this@SyncAccessAreasActivity, agInfo.message)
                            if (currentPageIndex == 1) {
                                db.deleteAllAGs()
                            }
                            showAccessAreasInfoFromDB(db)
                        }
                    } else if (agInfo.status == 2) {
                        if (currentPageIndex == 1) {
                            db.deleteAllAGs()
                        }
                        showAccessAreasInfoFromDB(db)
                    } else if (agInfo.status == 3) {
                        forceLogout(this@SyncAccessAreasActivity)

                    } else {
                        toast(this@SyncAccessAreasActivity, agInfo.message)
                    }
                }
            }


        })

    }

    override fun onRefresh(direction: SwipyRefreshLayoutDirection?) {
        if (isNetworkAvailable(this@SyncAccessAreasActivity)) {
            val url = SharedPref.getStringValue(this@SyncAccessAreasActivity, keyBaseUrl, "")
            if (url == null || url == "" || !URLCollections.isValidUrl(url)) run {

                val snackbar = Snackbar
                        .make(llReaderList!!, resources.getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                        .setAction(resources.getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener)
                snackbar.setActionTextColor(Color.GREEN)
                val snackbarView = snackbar.view
                snackbarView.setBackgroundColor(Color.DKGRAY)
                val textView = snackbarView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
                textView.setTextColor(Color.WHITE)
                snackbar.show()
            } else {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    currentPageIndex = 1
                } else {
                    currentPageIndex++
                }
                SyncAccessAreaInfoAPI("0")
            }

        } else {
            srlAccessAreaList!!.isRefreshing = false
            Boast.makeText(this@SyncAccessAreasActivity, getString(R.string.no_conn)).show()
        }
    }

    fun showAccessAreasInfoFromDB (db : DatabaseHandler) {


        var data1 : ArrayList<AGInfoModel.DataBean?> = db.allUniqueAGInfo
        System.out.println("AG List: "+data1.size)

        if (data1.size == 0) {
            rvAccessAreaList!!.visibility = View.GONE
        } else {
            rvAccessAreaList!!.visibility = View.VISIBLE
        }

        accessAreaInfoAdapter!!.setList(data1)
        accessAreaInfoAdapter!!.notifyDataSetChanged()
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        Log.e("stopRequest","stopRequest--incomingMsg-SyncReadersList-"+srlReaderList!!.isRefreshing )
        if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlReaderList!!.isRefreshing)
        {
            stopRequest(this@SyncAccessAreasActivity,hStopRequest!!)
        }
        else
        {
            onBackPressOperation()
        }
    }

    fun onBackPressOperation() {
        this@SyncAccessAreasActivity.finish()
        if (URLCollections.whichDirection(this@SyncAccessAreasActivity) == URLCollections.DEV_DIR_RTL) {
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left)
        } else {
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            if (item.itemId == android.R.id.home) {


                if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlReaderList!!.isRefreshing)
                {
                    stopRequest(this@SyncAccessAreasActivity,hStopRequest!!)
                }
                else
                {
                    onBackPressOperation()
                }

                return true
            }
        } else if (item.itemId == R.id.action_cat_refresh) {
            SyncAccessAreaInfoAPI("0")
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()

        hStopRequest!!.removeCallbacks(null)

    }

    companion object {
        private val TAG = "SyncAreas"
    }
}

