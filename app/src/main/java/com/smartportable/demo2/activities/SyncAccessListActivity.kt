package com.smartportable.demo2.activities

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Message
import com.google.android.material.snackbar.Snackbar
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fineapp.android.retrofitClient.RetrofitClientSingleton
import com.google.gson.JsonObject
import com.smartportable.demo2.HomeScreen
import com.smartportable.demo2.R
import com.smartportable.demo2.adapters.StudentInfoAdapter
import com.smartportable.demo2.api.URLCollections
import com.smartportable.demo2.db.DatabaseHandler
import com.smartportable.demo2.helper.Boast
import com.smartportable.demo2.models.AccessListModel
import com.smartportable.demo2.models.StudentInfoModel
import com.smartportable.demo2.prefs.MyPreferencesActivity
import com.smartportable.demo2.utils.*
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection
import retrofit2.Call
import retrofit2.Response

class SyncAccessListActivity : AppCompatActivity() , SwipyRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener {
    private var hStopRequest: Handler? = null
    var llReaderList : LinearLayout ?= null
    var studentList = ArrayList<StudentInfoModel.DataBean?>()
    var llStudentRecord : LinearLayout ?= null
    var tvSyncStudentRecord : TextView ?= null
    var srlStudentList : SwipyRefreshLayout?= null
    var rvStudentList : RecyclerView?= null
    var studentInfoAdapter : StudentInfoAdapter? = null
    var fm : FragmentManager = supportFragmentManager
    var db: DatabaseHandler ? = null
    var searchView: SearchView? = null
    var currentPageIndex : Int = 0
    var totalPageCount : Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reader_list)

        init()

    }

    fun init() {
        llStudentRecord = findViewById(R.id.llStudentRecord) as LinearLayout
        tvSyncStudentRecord = findViewById(R.id.tvSyncStudentRecord) as TextView
        llReaderList = findViewById(R.id.llReaderList) as LinearLayout
        srlStudentList = findViewById(R.id.srlReaderList) as SwipyRefreshLayout
        rvStudentList = findViewById(R.id.rvReaderList) as RecyclerView
        //fm = supportFragmentManager
        //rvReaderList?.setHasFixedSize(true)
        rvStudentList?.setLayoutManager(LinearLayoutManager(this))
        studentInfoAdapter = StudentInfoAdapter(this@SyncAccessListActivity, studentList, fm)
        rvStudentList?.adapter = studentInfoAdapter
        //readersAdapter.notifyDataSetChanged()


        // recyclerview scroll
        var check_ScrollingUp :Boolean = false

        rvStudentList!!.addOnScrollListener( object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView!!, dx, dy)
                if (dy > 0) {
                    // Scrolling up
                    if(check_ScrollingUp)
                    {
                        llStudentRecord!!.startAnimation(AnimationUtils.loadAnimation(this@SyncAccessListActivity,R.anim.trans_downwards))
                        llStudentRecord!!.visibility = View.VISIBLE
                        check_ScrollingUp = false
                    }

                } else {
                    // User scrolls down
                    if(!check_ScrollingUp )
                    {
                        llStudentRecord!!
                                .startAnimation(AnimationUtils
                                        .loadAnimation(this@SyncAccessListActivity,R.anim.trans_upwards))
                        llStudentRecord!!.visibility = View.GONE
                        check_ScrollingUp = true

                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView!!, newState)
            }

        })

        db = DatabaseHandler(this@SyncAccessListActivity)
        if (db!!.checkAccessList()) {
            showStudentInfoFromDB(db!!)
        } else {
            if (isNetworkAvailable(this@SyncAccessListActivity)) {
                val url = SharedPref.getStringValue(this@SyncAccessListActivity, keyBaseUrl, "")


                if (url == null || url == "" || !URLCollections.isValidUrl(url)) run {

                    val snackbar = Snackbar
                            .make(llReaderList!!, resources.getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                            .setAction(resources.getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener)
                    snackbar.setActionTextColor(Color.GREEN)
                    val snackbarView = snackbar.view
                    snackbarView.setBackgroundColor(Color.DKGRAY)
                    val textView = snackbarView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
                    textView.setTextColor(Color.WHITE)
                    snackbar.show()
                } else {
                    currentPageIndex = 1
                    SyncAccessListAPI(0)
                }
            } else {
                srlStudentList!!.isRefreshing = false
                Boast.makeText(this@SyncAccessListActivity, R.string.no_conn).show()
            }
        }

        srlStudentList?.setOnRefreshListener(this)

        customToastOnTop(this@SyncAccessListActivity, "Swipe down to refresh")


        hStopRequest = object: Handler()
        {
            override fun handleMessage(msg: Message)
            {
                super.handleMessage(msg)
                Log.e("stopRequest","stopRequest--incomingMsg--"+msg.data.get("isOkClicked")+"<<>>"+ srlStudentList!!.isRefreshing )
                if (msg.data.get("isOkClicked") === true)
                {
                    if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlStudentList!!.isRefreshing)
                    {
                        db = null
                        apiRetrofit!!.cancel()

                        onBackPressOperation()
                    }
                    else
                    {
                        onBackPressOperation()
                    }

                }
                else
                {
                    //onBackPressOperation()
                }

            }

        }

        //Realm.init(this)
    }

    override fun onCreateOptionsMenu(menu : Menu) : Boolean {

        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_sync_student, menu)

        val searchItem = menu!!.findItem(R.id.action_search)

        val searchManager = this@SyncAccessListActivity.getSystemService(Context.SEARCH_SERVICE) as SearchManager


        if (searchItem != null) {
            searchView = searchItem!!.getActionView() as SearchView
        }
        if (searchView != null) {
            searchView!!.setSearchableInfo(searchManager.getSearchableInfo(this@SyncAccessListActivity.getComponentName()))

            searchView!!.setOnQueryTextListener(this)
        }
        return super.onCreateOptionsMenu(menu)
    }

    fun onBackPressOperation()
    {
        if (!searchView!!.isIconified) {
            searchView!!.onActionViewCollapsed()
            if (db!!.checkStudentInfo()) {
                showStudentInfoFromDB(db!!)
            }
        } else {
            //super.onBackPressed()
            this@SyncAccessListActivity.finish()
            if (URLCollections.whichDirection(this@SyncAccessListActivity) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left)
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right)
            }
        }
    }

    override fun onBackPressed() {

        Log.e("stopRequest","stopRequest--incomingMsg-SyncAccessList-"+srlStudentList!!.isRefreshing )
        if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlStudentList!!.isRefreshing)
        {
            stopRequest(this@SyncAccessListActivity,hStopRequest!!)
        }
        else
        {
            onBackPressOperation()
        }

       /* if (!searchView!!.isIconified) {
            searchView!!.onActionViewCollapsed()
            if (db!!.checkStudentInfo()) {
                showStudentInfoFromDB(db!!)
            }
        } else {
            //super.onBackPressed()
            this@SyncAccessListActivity.finish()
            if (URLCollections.whichDirection(this@SyncAccessListActivity) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left)
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right)
            }
        }*/
    }

    var mOnSnackBarSetUrlClickListener: View.OnClickListener = View.OnClickListener {
        println("SnackBar Set URL Click")
        val int_set_url = Intent(this@SyncAccessListActivity, MyPreferencesActivity::class.java)
        int_set_url.putExtra(HomeScreen.ACTION_STR, HomeScreen.SET_URL)
        startActivity(int_set_url)
    }

    override fun onRefresh(direction: SwipyRefreshLayoutDirection) {
        if (isNetworkAvailable(this@SyncAccessListActivity)) {
            val url = SharedPref.getStringValue(this@SyncAccessListActivity, keyBaseUrl, "")


            if (url == null || url == "" || !URLCollections.isValidUrl(url)) run {

                val snackbar = Snackbar
                        .make(llReaderList!!, resources.getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                        .setAction(resources.getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener)
                snackbar.setActionTextColor(Color.GREEN)
                val snackbarView = snackbar.view
                snackbarView.setBackgroundColor(Color.DKGRAY)
                val textView = snackbarView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
                textView.setTextColor(Color.WHITE)
                snackbar.show()
            } else {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    currentPageIndex = 1
                } else {
                    currentPageIndex++
                }
                SyncAccessListAPI(0)
            }
        } else {
            srlStudentList!!.isRefreshing = false
            Boast.makeText(this@SyncAccessListActivity, getString(R.string.no_conn)).show()
        }
    }

    var apiRetrofit: Call<AccessListModel>? = null
    fun SyncAccessListAPI(studentId : Int) {
        //showProgressDialog(this@SyncReadersActivity)
        if (currentPageIndex !=1 && currentPageIndex >= totalPageCount) {
            warningMessage(this@SyncAccessListActivity, "All Students Records has been synced.")
            srlStudentList!!.isRefreshing = false
            return
        }
        srlStudentList!!.isRefreshing = true
        val jObj : JsonObject = JsonObject()

        jObj.addProperty("Token", SharedPref.getAuthToken(this))
        //jObj.addProperty("NextPageIndex", "1")

        var jData : JsonObject = JsonObject()
        jData.addProperty("StudentId", studentId)
        jData.addProperty("CurrentPage", 1)
        jObj.add("data", jData)
        System.out.println("JObj: "+jObj.toString())

        var baseUrl = SharedPref.getStringValue(this@SyncAccessListActivity, keyAPIBaseUrl, "")

        /* RetrofitClientSingleton.getInstance(baseUrl).getAccessList(jObj)
                .enqueue(object : retrofit2.Callback<AccessListModel> {*/

        var abc = RetrofitClientSingleton.getInstance(baseUrl)
        apiRetrofit = abc.getAccessList(jObj)


        apiRetrofit!!.enqueue(object : retrofit2.Callback<AccessListModel>
        {
                    override fun onFailure(call: Call<AccessListModel>?, t: Throwable?) {
                        //stopProgress()
                        srlStudentList!!.isRefreshing = false
                    }

                    override fun onResponse(call: Call<AccessListModel>?, response: Response<AccessListModel>?) {
                        //stopProgress()
                        srlStudentList!!.isRefreshing = false

                        if (response!!.isSuccessful) {
                            val accessList: AccessListModel? = response!!.body()

                            if (accessList!!.status == 1) {
                                //System.out.println("Sync Reader : "+readerInfo.status.toString())
                                //System.out.println("Sync Reader : "+readerInfo!!.data?.get(0)?.readerName)

                                if (accessList!!.pageCount!!.contains("/")) {
                                    var pageCountArr  = accessList!!.pageCount!!.split("/")
                                    totalPageCount = Integer.parseInt(pageCountArr[1])
                                }

                                val data: ArrayList<AccessListModel.DataBean>? = accessList!!.data
                                if (data != null) {


                                    db = DatabaseHandler(this@SyncAccessListActivity)

                                    if (currentPageIndex == 1) {
                                        db!!.deleteAllAccessList()
                                    }

                                    db!!.saveAccessList(data)

                                    showStudentInfoFromDB(db!!)

                                    if (currentPageIndex < totalPageCount) {
                                        onRefresh(SwipyRefreshLayoutDirection.BOTTOM)
                                    }

                                } else {
                                    toast(this@SyncAccessListActivity, accessList!!.message)

                                    if (currentPageIndex == 1) {
                                        db!!.deleteAllAccessList()
                                    }

                                    showStudentInfoFromDB(db!!)
                                }
                            } else if (accessList.status == 3) {
                                forceLogout(this@SyncAccessListActivity)

                            } else {
                                toast(this@SyncAccessListActivity, accessList!!.message)
                            }
                        }
                    }


                })

    }

    fun showStudentInfoFromDB (db : DatabaseHandler) {


        var data1 : ArrayList<StudentInfoModel.DataBean?> = db.allStudentInfoInStudentAccess
        System.out.println("Student List: "+data1.size)

        tvSyncStudentRecord!!.text = "Total Sync Student Access : "+data1.size

        studentInfoAdapter!!.updateStudentInfo(data1, true) // seond boolean argument is for the member access alway true to display reader list in detail dialog
        studentInfoAdapter!!.notifyDataSetChanged()
    }

    override fun onQueryTextSubmit(searchKeyword: String?): Boolean {
        val db = DatabaseHandler(this@SyncAccessListActivity)
        var data1 = db.getStudentsBySearch(searchKeyword)
        studentInfoAdapter!!.updateStudentInfo(data1, true)
        studentInfoAdapter!!.notifyDataSetChanged()
        return true
    }

    override fun onQueryTextChange(p0: String?): Boolean {

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {


            if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlStudentList!!.isRefreshing)
            {
                stopRequest(this@SyncAccessListActivity,hStopRequest!!)
            }
            else
            {
                onBackPressOperation()
            }


            /*if (!searchView!!.isIconified) {
                searchView!!.onActionViewCollapsed()
                if (db!!.checkStudentInfo()) {
                    showStudentInfoFromDB(db!!)
                }
                return true
            } else {
                this@SyncAccessListActivity.finish()
                if (URLCollections.whichDirection(this@SyncAccessListActivity) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left)
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right)
                }
            }*/

            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()

        hStopRequest!!.removeCallbacks(null)

    }

}
