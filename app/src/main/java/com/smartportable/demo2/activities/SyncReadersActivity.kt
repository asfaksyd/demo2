package com.smartportable.demo2.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Message
import com.google.android.material.snackbar.Snackbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fineapp.android.retrofitClient.RetrofitClientSingleton
import com.google.gson.JsonObject

import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout
import com.smartportable.demo2.HomeScreen
import com.smartportable.demo2.R
import com.smartportable.demo2.adapters.ReaderListAdapter
import com.smartportable.demo2.api.URLCollections
import com.smartportable.demo2.db.DatabaseHandler
import com.smartportable.demo2.helper.Boast
import com.smartportable.demo2.models.ReaderInfoModel
import com.smartportable.demo2.prefs.MyPreferencesActivity
import com.smartportable.demo2.realm.ReadersModel
import com.smartportable.demo2.utils.*
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection
import retrofit2.Call
import retrofit2.Response





class SyncReadersActivity: AppCompatActivity(), SwipyRefreshLayout.OnRefreshListener {
    private var hStopRequest: Handler? = null
    var db: DatabaseHandler ? = null
    var llReaderList : LinearLayout ?= null
    var readersList = ArrayList<ReaderInfoModel.DataBean>()
    var srlReaderList : SwipyRefreshLayout ?= null
    var rvReaderList : RecyclerView?= null
    var readersAdapter : ReaderListAdapter ? = null

    var currentPageIndex : Int = 0
    var totalPageCount : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reader_list)

        init()

    }

    fun init() {
        llReaderList = findViewById(R.id.llReaderList) as LinearLayout
        srlReaderList = findViewById(R.id.srlReaderList) as SwipyRefreshLayout
        rvReaderList = findViewById(R.id.rvReaderList) as RecyclerView
        //rvReaderList?.setHasFixedSize(true)
        rvReaderList?.setLayoutManager(LinearLayoutManager(this))
        readersAdapter = ReaderListAdapter(this@SyncReadersActivity, readersList)
        rvReaderList?.adapter = readersAdapter
        //readersAdapter.notifyDataSetChanged()


             db = DatabaseHandler(this@SyncReadersActivity)
            if (db!!.checkReaders()) {
                showReadersFromDB(db!!)
            } else {
                if (isNetworkAvailable(this@SyncReadersActivity)) {
                    val url = SharedPref.getStringValue(this@SyncReadersActivity, keyBaseUrl, "")
                    if (url == null || url == "" || !URLCollections.isValidUrl(url)) run {

                        val snackbar = Snackbar
                                .make(llReaderList!!, resources.getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                                .setAction(resources.getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener)
                        snackbar.setActionTextColor(Color.GREEN)
                        val snackbarView = snackbar.view
                        snackbarView.setBackgroundColor(Color.DKGRAY)
                        val textView = snackbarView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
                        textView.setTextColor(Color.WHITE)
                        snackbar.show()
                    } else {
                        currentPageIndex = 1
                        SyncReadersAPI(0)
                    }
                } else {
                    srlReaderList!!.isRefreshing = false
                    Boast.makeText(this@SyncReadersActivity, R.string.no_conn).show()
                }
            }

        srlReaderList?.setOnRefreshListener(this)

        customToastOnTop(this@SyncReadersActivity, "Swipe down to refresh")

        hStopRequest = object: Handler()
        {
            override fun handleMessage(msg: Message)
            {
                super.handleMessage(msg)
                Log.e("stopRequest","stopRequest--incomingMsg--"+msg.data.get("isOkClicked")+"<<>>"+ srlReaderList!!.isRefreshing )
                if (msg.data.get("isOkClicked") === true)
                {
                    if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlReaderList!!.isRefreshing)
                    {
                        db = null
                        apiRetrofit!!.cancel()

                        onBackPressOperation()
                    }
                    else
                    {
                        onBackPressOperation()
                    }

                }
                else
                {
                    //onBackPressOperation()
                }

            }

        }

        //Realm.init(this)
    }

    override fun onRefresh(direction: SwipyRefreshLayoutDirection) {
        if (isNetworkAvailable(this@SyncReadersActivity)) {
            val url = SharedPref.getStringValue(this@SyncReadersActivity, keyBaseUrl, "")
            //permissionStatus = PreferenceManager.getDefaultSharedPreferences(this);


            if (url == null || url == "" || !URLCollections.isValidUrl(url)) run {

                val snackbar = Snackbar
                        .make(llReaderList!!, resources.getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                        .setAction(resources.getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener)
                snackbar.setActionTextColor(Color.GREEN)
                val snackbarView = snackbar.view
                snackbarView.setBackgroundColor(Color.DKGRAY)
                val textView = snackbarView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
                textView.setTextColor(Color.WHITE)
                snackbar.show()
            } else {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    currentPageIndex = 1
                } else {
                    currentPageIndex++
                }
                SyncReadersAPI(0)
            }
        } else {
            srlReaderList!!.isRefreshing = false
            Boast.makeText(this@SyncReadersActivity, getString(R.string.no_conn)).show()
        }
    }

    var mOnSnackBarSetUrlClickListener: View.OnClickListener = View.OnClickListener {
        println("SnackBar Set URL Click")
        val int_set_url = Intent(this@SyncReadersActivity, MyPreferencesActivity::class.java)
        int_set_url.putExtra(HomeScreen.ACTION_STR, HomeScreen.SET_URL)
        startActivity(int_set_url)
    }

    var apiRetrofit: Call<ReaderInfoModel>? = null

    fun SyncReadersAPI(readerId : Int) {
        //showProgressDialog(this@SyncReadersActivity)
        if (currentPageIndex !=1 && currentPageIndex >= totalPageCount) {
            srlReaderList!!.isRefreshing = false
            warningMessage(this@SyncReadersActivity, "All Sessions Records has been synced.")
            return
        }
        srlReaderList!!.isRefreshing = true
        var jObj : JsonObject = JsonObject()

        jObj.addProperty("Token", SharedPref.getAuthToken(this))
        //jObj.addProperty("NextPageIndex", "1")

        var jData : JsonObject = JsonObject()
        jData.addProperty("ReaderId", readerId)
        jData.addProperty("CurrentPage", currentPageIndex)
        jObj.add("data", jData)
        System.out.println("JObj: "+jObj.toString())

        var baseUrl = SharedPref.getStringValue(this@SyncReadersActivity, keyAPIBaseUrl, "")

      /*  RetrofitClientSingleton.getInstance(baseUrl).getReaderInfo(jObj)
                .enqueue(object : retrofit2.Callback<ReaderInfoModel> {*/

        var abc = RetrofitClientSingleton.getInstance(baseUrl)
        apiRetrofit = abc.getReaderInfo(jObj)

        apiRetrofit!!.enqueue(object : retrofit2.Callback<ReaderInfoModel>{

                    override fun onFailure(call: Call<ReaderInfoModel>?, t: Throwable?) {
                        //stopProgress()
                        srlReaderList!!.isRefreshing = false
                    }

                    override fun onResponse(call: Call<ReaderInfoModel>?, response: Response<ReaderInfoModel>?) {
                        //stopProgress()
                        srlReaderList!!.isRefreshing = false

                        if (response!!.isSuccessful) {
                            val readerInfo: ReaderInfoModel? = response!!.body()

                            if (readerInfo!!.status == 1) {
                                //System.out.println("Sync Reader : "+readerInfo.status.toString())
                                //System.out.println("Sync Reader : "+readerInfo!!.data?.get(0)?.readerName)
                                if (readerInfo!!.pageCount!!.contains("/")) {
                                    var pageCountArr  = readerInfo!!.pageCount!!.split("/")
                                    totalPageCount = Integer.parseInt(pageCountArr[1])
                                }

                                val data: ArrayList<ReaderInfoModel.DataBean>? = readerInfo!!.data
                                if (data != null) {
                                    var data2 : ArrayList<ReadersModel> = ArrayList()

                                    /*for (reader in data) {
                                        var readerModel = ReadersModel()
                                        readerModel.readerId = reader.readerId
                                        readerModel.readerName = reader.readerName
                                        readerModel.readerType = reader.readerType
                                        System.out.println("ReaderName "+reader.readerName)

                                        data1.add(readerModel)
                                    }*/


                                     db = DatabaseHandler(this@SyncReadersActivity)

                                    if (currentPageIndex == 1) {
                                        db!!.deleteAllReaders()
                                    }

                                    db!!.saveReaders(data)

                                    showReadersFromDB(db!!)

                                    if (currentPageIndex < totalPageCount) {
                                        onRefresh(SwipyRefreshLayoutDirection.BOTTOM)
                                    }

                                } else {
                                    toast(this@SyncReadersActivity, readerInfo!!.message)
                                }
                            } else if (readerInfo.status == 3) {
                                forceLogout(this@SyncReadersActivity)

                            } else {
                                toast(this@SyncReadersActivity, readerInfo!!.message)
                            }
                        }
                    }


                })

    }

    override fun onBackPressed() {
        //super.onBackPressed()
        Log.e("stopRequest","stopRequest--incomingMsg-SyncReadersList-"+srlReaderList!!.isRefreshing )
        if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlReaderList!!.isRefreshing)
        {
            stopRequest(this@SyncReadersActivity,hStopRequest!!)
        }
        else
        {
            onBackPressOperation()
        }
    }

    fun showReadersFromDB (db : DatabaseHandler) {


        var data1 : ArrayList<ReaderInfoModel.DataBean> = db.allReaders
        System.out.println("Reader List: "+data1.size)


        readersAdapter!!.updateReadersList(data1)
        readersAdapter!!.notifyDataSetChanged()
    }

    fun onBackPressOperation()
    {
        this@SyncReadersActivity.finish()
        if (URLCollections.whichDirection(this@SyncReadersActivity) == URLCollections.DEV_DIR_RTL) {
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left)
        } else {
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {


            if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlReaderList!!.isRefreshing)
            {
                stopRequest(this@SyncReadersActivity,hStopRequest!!)
            }
            else
            {
                onBackPressOperation()
            }

          /*  this@SyncReadersActivity.finish()
            if (URLCollections.whichDirection(this@SyncReadersActivity) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left)
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right)
            }*/
            return true
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onDestroy() {
        super.onDestroy()

        hStopRequest!!.removeCallbacks(null)

    }

}