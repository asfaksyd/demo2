package com.smartportable.demo2.activities

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Message
import com.google.android.material.snackbar.Snackbar

import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.fineapp.android.retrofitClient.RetrofitClientSingleton
import com.google.gson.JsonObject
import com.smartportable.demo2.HomeScreen
import com.smartportable.demo2.R
import com.smartportable.demo2.adapters.StudentInfoAdapter
import com.smartportable.demo2.api.URLCollections
import com.smartportable.demo2.db.DatabaseHandler
import com.smartportable.demo2.helper.Boast
import com.smartportable.demo2.models.StudentInfoModel
import com.smartportable.demo2.prefs.MyPreferencesActivity
import com.smartportable.demo2.utils.*
import kotlinx.android.synthetic.main.activity_reader_list.*
import retrofit2.Call
import retrofit2.Response
import android.view.Menu
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection


class SyncStudentInfoActivity : AppCompatActivity() , SwipyRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener {
    private var hStopRequest: Handler? = null
    var db: DatabaseHandler? = null
    //var apiRetrofit: Call<StudentInfoModel>? = null
    var studentList = ArrayList<StudentInfoModel.DataBean?>()
    var llStudentRecord : LinearLayout?= null
    var tvSyncStudentRecord : TextView ?= null
    var srlStudentList : SwipyRefreshLayout?= null
    var rvStudentList : RecyclerView?= null
    var studentInfoAdapter : StudentInfoAdapter? = null
    var fm: FragmentManager = supportFragmentManager
    var searchView: SearchView? = null
    var currentPageIndex : Int = 0
    var totalPageCount : Int = 0
    var retrofitObj = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reader_list)

        init()

    }



    override fun onCreateOptionsMenu(menu : Menu) : Boolean {

        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_sync_student, menu)

        val searchItem = menu!!.findItem(R.id.action_search)

        val searchManager = this@SyncStudentInfoActivity.getSystemService(Context.SEARCH_SERVICE) as SearchManager


        if (searchItem != null) {
            searchView = searchItem!!.getActionView() as SearchView
        }
        if (searchView != null) {
            searchView!!.setSearchableInfo(searchManager.getSearchableInfo(this@SyncStudentInfoActivity.getComponentName()))

            searchView!!.setOnQueryTextListener(this)
        }
        return super.onCreateOptionsMenu(menu)
    }

    fun onBackPressOperation()
    {
        if (!searchView!!.isIconified) {
            searchView!!.onActionViewCollapsed()
            if (db!!.checkStudentInfo()) {
                showStudentInfoFromDB(db!!)
            }
        } else {
            //super.onBackPressed()
            this@SyncStudentInfoActivity.finish()
            if (URLCollections.whichDirection(this@SyncStudentInfoActivity) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left)
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right)
            }

        }
    }



    override fun onBackPressed() {

        Log.e("stopRequest","stopRequest--incomingMsg--"+srlStudentList!!.isRefreshing )
        if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlStudentList!!.isRefreshing)
        {
            stopRequest(this@SyncStudentInfoActivity,hStopRequest!!)
        }
        else
        {
            onBackPressOperation()
        }



/*
        if (!searchView!!.isIconified) {
            searchView!!.onActionViewCollapsed()
            if (db!!.checkStudentInfo()) {
                showStudentInfoFromDB(db!!)
            }
        } else {
            //super.onBackPressed()
            this@SyncStudentInfoActivity.finish()
            if (URLCollections.whichDirection(this@SyncStudentInfoActivity) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left)
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right)
            }

        }*/
    }
    fun init() {
        llStudentRecord = findViewById(R.id.llStudentRecord) as LinearLayout
        tvSyncStudentRecord = findViewById(R.id.tvSyncStudentRecord) as TextView
        srlStudentList = findViewById(R.id.srlReaderList) as SwipyRefreshLayout
        rvStudentList = findViewById(R.id.rvReaderList) as RecyclerView
        //rvReaderList?.setHasFixedSize(true)
        rvStudentList?.setLayoutManager(LinearLayoutManager(this))
        studentInfoAdapter = StudentInfoAdapter(this@SyncStudentInfoActivity, studentList!!, fm)
        rvStudentList?.adapter = studentInfoAdapter
        //readersAdapter.notifyDataSetChanged()


        // recyclerview scroll
        var check_ScrollingUp :Boolean = false

        rvStudentList!!.addOnScrollListener( object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    // Scrolling up
                    if(check_ScrollingUp)
                    {
                        llStudentRecord!!.startAnimation(AnimationUtils.loadAnimation(this@SyncStudentInfoActivity,R.anim.trans_downwards))
                        llStudentRecord!!.visibility = View.VISIBLE
                        check_ScrollingUp = false
                    }

                } else {
                    // User scrolls down
                    if(!check_ScrollingUp )
                    {
                        llStudentRecord!!
                                .startAnimation(AnimationUtils
                                        .loadAnimation(this@SyncStudentInfoActivity,R.anim.trans_upwards))
                        llStudentRecord!!.visibility = View.GONE
                        check_ScrollingUp = true

                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

        })

        db = DatabaseHandler(this@SyncStudentInfoActivity)
        if (db!!.checkStudentInfo()) {
            showStudentInfoFromDB(db!!)
        } else {
            if (isNetworkAvailable(this@SyncStudentInfoActivity)) {
                val url = SharedPref.getStringValue(this@SyncStudentInfoActivity, keyBaseUrl, "")
                if (url == null || url == "" || !URLCollections.isValidUrl(url)) run {

                    val snackbar = Snackbar
                            .make(llReaderList!!, resources.getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                            .setAction(resources.getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener)
                    snackbar.setActionTextColor(Color.GREEN)
                    val snackbarView = snackbar.view
                    snackbarView.setBackgroundColor(Color.DKGRAY)
                    val textView = snackbarView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
                    textView.setTextColor(Color.WHITE)
                    snackbar.show()
                } else {
                    currentPageIndex = 1
                    SyncStudentInfoAPI(0)
                }
            } else {
                srlStudentList!!.isRefreshing = false
                Boast.makeText(this@SyncStudentInfoActivity, R.string.no_conn).show()
            }
        }

        srlStudentList?.setOnRefreshListener(this)

        customToastOnTop(this@SyncStudentInfoActivity, "Swipe down to refresh")

        //Realm.init(this)

        hStopRequest = object:Handler()
        {
            override fun handleMessage(msg: Message)
            {
                super.handleMessage(msg)
                Log.e("stopRequest","stopRequest--incomingMsg--"+msg.data.get("isOkClicked")+"<<>>"+ srlStudentList!!.isRefreshing )
                if (msg.data.get("isOkClicked") === true)
                {
                    if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlStudentList!!.isRefreshing)
                    {
                        db = null
                        apiRetrofit!!.cancel()

                        onBackPressOperation()
                    }
                    else
                    {
                        onBackPressOperation()
                    }

                }
                else
                {
                    //onBackPressOperation()
                }

            }

        }

    }


    override fun onRefresh(direction: SwipyRefreshLayoutDirection?) {
        if (isNetworkAvailable(this@SyncStudentInfoActivity)) {
            val url = SharedPref.getStringValue(this@SyncStudentInfoActivity, keyBaseUrl, "")
            if (url == null || url == "" || !URLCollections.isValidUrl(url)) run {

                val snackbar = Snackbar
                        .make(llReaderList!!, resources.getString(R.string.conn_string_not_define), Snackbar.LENGTH_INDEFINITE)
                        .setAction(resources.getString(R.string.snack_action_url_conn), mOnSnackBarSetUrlClickListener)
                snackbar.setActionTextColor(Color.GREEN)
                val snackbarView = snackbar.view
                snackbarView.setBackgroundColor(Color.DKGRAY)
                val textView = snackbarView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
                textView.setTextColor(Color.WHITE)
                snackbar.show()
            } else {
                if (direction == SwipyRefreshLayoutDirection.TOP) {
                    currentPageIndex = 1
                } else {
                    currentPageIndex++
                }
                SyncStudentInfoAPI(0)
            }

        } else {
            srlStudentList!!.isRefreshing = false
            Boast.makeText(this@SyncStudentInfoActivity, getString(R.string.no_conn)).show()
        }
    }

    var mOnSnackBarSetUrlClickListener: View.OnClickListener = View.OnClickListener {
        println("SnackBar Set URL Click")
        val int_set_url = Intent(this@SyncStudentInfoActivity, MyPreferencesActivity::class.java)
        int_set_url.putExtra(HomeScreen.ACTION_STR, HomeScreen.SET_URL)
        startActivity(int_set_url)
    }

//    fun SyncStudentInfoAPI(studentId : Int) {
//        //showProgressDialog(this@SyncReadersActivity)
//        if (currentPageIndex !=1 && currentPageIndex >= totalPageCount) {
//            warningMessage(this@SyncStudentInfoActivity, "All Students Records has been synced.")
//            srlStudentList!!.isRefreshing = false
//            return
//        }
//        srlStudentList!!.isRefreshing = true
//        val jObj : JsonObject = JsonObject()
//
//        jObj.addProperty("Token", SharedPref.getAuthToken(this))
//        //jObj.addProperty("NextPageIndex", "1")
//
//        var jData : JsonObject = JsonObject()
//        jData.addProperty("Id_Number", studentId)
//        jData.addProperty("CurrentPage", currentPageIndex)
//        jObj.add("data", jData)
//        System.out.println("JObj: "+jObj.toString())
//
//        var baseUrl = SharedPref.getStringValue(this@SyncStudentInfoActivity, keyAPIBaseUrl, "")
//
//        apiRetrofit = RetrofitClientSingleton.getInstance(baseUrl).getStudentInfo(jObj)
//        apiRetrofit!!.enqueue(object : retrofit2.Callback<StudentInfoModel> {
//                    override fun onFailure(call: Call<StudentInfoModel>?, t: Throwable?) {
//                        //stopProgress()
//                        srlStudentList!!.isRefreshing = false
//                    }
//
//                    override fun onResponse(call: Call<StudentInfoModel>?, response: Response<StudentInfoModel>?) {
//                        //stopProgress()
//                        srlStudentList!!.isRefreshing = false
//
//                        if (response!!.isSuccessful) {
//                            val studentInfo: StudentInfoModel? = response!!.body()
//
//                            if (studentInfo!!.status == 1) {
//                                //System.out.println("Sync Reader : "+readerInfo.status.toString())
//                                //System.out.println("Sync Reader : "+readerInfo!!.data?.get(0)?.readerName)
//                                if (studentInfo!!.pageCount!!.contains("/")) {
//                                    var pageCountArr  = studentInfo!!.pageCount!!.split("/")
//                                    totalPageCount = Integer.parseInt(pageCountArr[1])
//                                }
//                                val data: ArrayList<StudentInfoModel.DataBean>? = studentInfo!!.data
//                                if (data != null) {
//
//
//                                   /* val db: DatabaseHandler = DatabaseHandler(this@SyncStudentInfoActivity)*/
//                                    if (currentPageIndex == 1) {
//                                        db!!.deleteAllStudents()
//                                    }
//
//                                    db!!.saveStudentInfo(applicationContext/*this@SyncStudentInfoActivity*/, data)
//
//                                    showStudentInfoFromDB(db!!)
//
//                                    if (currentPageIndex < totalPageCount) {
//                                        onRefresh(SwipyRefreshLayoutDirection.BOTTOM)
//                                    }
//
//                                } else {
//                                    toast(this@SyncStudentInfoActivity, studentInfo!!.message)
//                                }
//                            } else if (studentInfo.status == 3) {
//                                forceLogout(this@SyncStudentInfoActivity)
//
//                            } else {
//                                toast(this@SyncStudentInfoActivity, studentInfo!!.message)
//                            }
//                        }
//                    }
//
//
//                })
//
//    }


    var apiRetrofit: Call<StudentInfoModel>? = null

    fun SyncStudentInfoAPI(studentId : Int) {
        //showProgressDialog(this@SyncReadersActivity)
        if (currentPageIndex !=1 && currentPageIndex >= totalPageCount) {
            warningMessage(this@SyncStudentInfoActivity, "All Students Records has been synced.")
            srlStudentList!!.isRefreshing = false
            return
        }
        srlStudentList!!.isRefreshing = true
        val jObj : JsonObject = JsonObject()

        jObj.addProperty("Token", SharedPref.getAuthToken(this))

        var jData : JsonObject = JsonObject()
        jData.addProperty("Id_Number", studentId)
        jData.addProperty("CurrentPage", currentPageIndex)
        jObj.add("data", jData)
        System.out.println("JObj: "+jObj.toString())

        var baseUrl = SharedPref.getStringValue(this@SyncStudentInfoActivity, keyAPIBaseUrl, "")


        var abc = RetrofitClientSingleton.getInstance(baseUrl)
        apiRetrofit = abc.getStudentInfo(jObj)


        apiRetrofit!!.enqueue(object : retrofit2.Callback<StudentInfoModel>
        {

            override fun onFailure(call: Call<StudentInfoModel>?, t: Throwable?)
            {
                //stopProgress()
                srlStudentList!!.isRefreshing = false
            }

            override fun onResponse(call: Call<StudentInfoModel>?, response: Response<StudentInfoModel>?) {
                //stopProgress()
                srlStudentList!!.isRefreshing = false
                Log.e("SyncStudentInfoActivity", "SyncStudentInfoActivity>>response>>>> "+response)
                if (response!!.isSuccessful) {
                    val studentInfo: StudentInfoModel? = response!!.body()

                    if (studentInfo!!.status == 1) {
                        //System.out.println("Sync Reader : "+readerInfo.status.toString())
                        //System.out.println("Sync Reader : "+readerInfo!!.data?.get(0)?.readerName)
                        if (studentInfo!!.pageCount!!.contains("/")) {
                            var pageCountArr  = studentInfo!!.pageCount!!.split("/")
                            totalPageCount = Integer.parseInt(pageCountArr[1])
                        }
                        val data: ArrayList<StudentInfoModel.DataBean>? = studentInfo!!.data
                        if (data != null) {


                            db  = DatabaseHandler(this@SyncStudentInfoActivity)
                            if (currentPageIndex == 1) {
                                db!!.deleteAllStudents()
                            }

                            db!!.saveStudentInfo(this@SyncStudentInfoActivity, data)

                            showStudentInfoFromDB(db!!)

                            if (currentPageIndex < totalPageCount) {
                                onRefresh(SwipyRefreshLayoutDirection.BOTTOM)
                            }

                        } else {
                            toast(this@SyncStudentInfoActivity, studentInfo!!.message)

                            if (currentPageIndex == 1) {
                                db!!.deleteAllStudents()
                            }

                            showStudentInfoFromDB(db!!)
                        }
                    } else if (studentInfo.status == 3) {
                        forceLogout(this@SyncStudentInfoActivity)

                    } else {
                        toast(this@SyncStudentInfoActivity, studentInfo!!.message)
                    }
                }
            }


        })

    }

    fun showStudentInfoFromDB (db : DatabaseHandler) {


        var data1 : ArrayList<StudentInfoModel.DataBean?> = db.allStudentInfo
        System.out.println("Student List: "+data1.size)

        tvSyncStudentRecord!!.text = "Total sync students : "+data1.size

        studentInfoAdapter!!.updateStudentInfo(data1, false)
        studentInfoAdapter!!.notifyDataSetChanged()
    }

    override fun onQueryTextSubmit(searchKeyword: String?): Boolean {
        val db = DatabaseHandler(this@SyncStudentInfoActivity)
        var data1 = db.getStudentsBySearch(searchKeyword)
        studentInfoAdapter!!.updateStudentInfo(data1, false)
        studentInfoAdapter!!.notifyDataSetChanged()
        return true
    }

    override fun onQueryTextChange(p0: String?): Boolean {

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {

            if(apiRetrofit!=null && apiRetrofit!!.isExecuted && srlStudentList!!.isRefreshing)
            {
                stopRequest(this@SyncStudentInfoActivity,hStopRequest!!)
            }
            else
            {
                onBackPressOperation()
            }

            /*if (!searchView!!.isIconified) {
                searchView!!.onActionViewCollapsed()
                if (db!!.checkStudentInfo()) {
                    showStudentInfoFromDB(db!!)
                }
                return true
            }  else {
                this@SyncStudentInfoActivity.finish()
                if (URLCollections.whichDirection(this@SyncStudentInfoActivity) == URLCollections.DEV_DIR_RTL) {
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left)
                } else {
                    overridePendingTransition(R.anim.anim_slide_in_right,
                            R.anim.anim_slide_out_right)
                }
                return true
            }*/
        }
        return true
    }

    override fun onDestroy() {
        super.onDestroy()

        hStopRequest!!.removeCallbacks(null)

    }

}
