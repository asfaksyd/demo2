package com.smartportable.demo2.activities

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import com.smartportable.demo2.R
import com.smartportable.demo2.StaffAuth
import com.smartportable.demo2.StudentAuth
import com.smartportable.demo2.VisitorAuth
import com.smartportable.demo2.api.URLCollections

class CardAuthActivity : AppCompatActivity() , View.OnClickListener {

    var rlStudentAuth: RelativeLayout ?= null
    var rlStaffAuth: RelativeLayout ?= null
    var rlVisitorAuth: RelativeLayout ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_auth)

        initViews()

    }

    fun initViews() {
        rlStudentAuth = findViewById(R.id.rlStudentAuth) as RelativeLayout
        rlStaffAuth = findViewById(R.id.rlStaffAuth) as RelativeLayout
        rlVisitorAuth = findViewById(R.id.rlVisitorAuth) as RelativeLayout

        rlStudentAuth!!.setOnClickListener(this)
        rlStaffAuth!!.setOnClickListener(this)
        rlVisitorAuth!!.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.rlStudentAuth -> {
                startActivity(Intent(this@CardAuthActivity, StudentAuth::class.java))
            }

            R.id.rlStaffAuth -> {
                startActivity(Intent(this@CardAuthActivity, StaffAuth::class.java))
            }

            R.id.rlVisitorAuth -> {
                startActivity(Intent(this@CardAuthActivity, VisitorAuth::class.java))
                //Boast.makeText(this@CardAuthActivity, "Under Development", Toast.LENGTH_SHORT).show()
            }
        }
        if (URLCollections.whichDirection(this@CardAuthActivity) == URLCollections.DEV_DIR_RTL) {
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right)
        } else {
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            this@CardAuthActivity.finish()
            if (URLCollections.whichDirection(this@CardAuthActivity) == URLCollections.DEV_DIR_RTL) {
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left)
            } else {
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right)
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (URLCollections.whichDirection(this@CardAuthActivity) == URLCollections.DEV_DIR_RTL) {
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left)
        } else {
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right)
        }
    }

}