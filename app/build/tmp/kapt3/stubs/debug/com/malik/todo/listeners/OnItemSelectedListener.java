package com.malik.todo.listeners;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001\u0013B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J0\u0010\u0007\u001a\u00020\b2\f\u0010\t\u001a\b\u0012\u0002\b\u0003\u0018\u00010\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0016\u0010\u0011\u001a\u00020\b2\f\u0010\u0012\u001a\b\u0012\u0002\b\u0003\u0018\u00010\nH\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0014"}, d2 = {"Lcom/malik/todo/listeners/OnItemSelectedListener;", "Landroid/widget/AdapterView$OnItemSelectedListener;", "categoryName", "Lcom/malik/todo/listeners/OnItemSelectedListener$CategoryName;", "(Lcom/malik/todo/listeners/OnItemSelectedListener$CategoryName;)V", "getCategoryName", "()Lcom/malik/todo/listeners/OnItemSelectedListener$CategoryName;", "onItemSelected", "", "parent", "Landroid/widget/AdapterView;", "view", "Landroid/view/View;", "pos", "", "id", "", "onNothingSelected", "p0", "CategoryName", "app_debug"})
public final class OnItemSelectedListener implements android.widget.AdapterView.OnItemSelectedListener {
    @org.jetbrains.annotations.NotNull()
    private final com.malik.todo.listeners.OnItemSelectedListener.CategoryName categoryName = null;
    
    @java.lang.Override()
    public void onNothingSelected(@org.jetbrains.annotations.Nullable()
    android.widget.AdapterView<?> p0) {
    }
    
    @java.lang.Override()
    public void onItemSelected(@org.jetbrains.annotations.Nullable()
    android.widget.AdapterView<?> parent, @org.jetbrains.annotations.Nullable()
    android.view.View view, int pos, long id) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.malik.todo.listeners.OnItemSelectedListener.CategoryName getCategoryName() {
        return null;
    }
    
    public OnItemSelectedListener(@org.jetbrains.annotations.NotNull()
    com.malik.todo.listeners.OnItemSelectedListener.CategoryName categoryName) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/malik/todo/listeners/OnItemSelectedListener$CategoryName;", "", "spinnerCatName", "", "categoryName", "", "app_debug"})
    public static abstract interface CategoryName {
        
        public abstract void spinnerCatName(@org.jetbrains.annotations.NotNull()
        java.lang.String categoryName);
    }
}