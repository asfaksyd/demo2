package com.fineapp.android.retrofitClient;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/fineapp/android/retrofitClient/RetrofitClientSingleton;", "", "()V", "Companion", "app_debug"})
public final class RetrofitClientSingleton {
    private static final java.lang.String TAG = null;
    public static final com.fineapp.android.retrofitClient.RetrofitClientSingleton.Companion Companion = null;
    
    public RetrofitClientSingleton() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0004R\u0019\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u000b"}, d2 = {"Lcom/fineapp/android/retrofitClient/RetrofitClientSingleton$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "getTAG", "()Ljava/lang/String;", "getInstance", "Lcom/fineapp/android/retrofitClient/ServiceGenerator;", "baseurl", "app_debug"})
    public static final class Companion {
        
        public final java.lang.String getTAG() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.fineapp.android.retrofitClient.ServiceGenerator getInstance(@org.jetbrains.annotations.NotNull()
        java.lang.String baseurl) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}