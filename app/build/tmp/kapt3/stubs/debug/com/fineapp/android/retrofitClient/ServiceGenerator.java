package com.fineapp.android.retrofitClient;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00180\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001c0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'\u00a8\u0006\u001d"}, d2 = {"Lcom/fineapp/android/retrofitClient/ServiceGenerator;", "", "getAGInfo", "Lretrofit2/Call;", "Lcom/smartportable/demo2/models/AGInfoModel;", "data", "Lcom/google/gson/JsonObject;", "getAccessList", "Lcom/smartportable/demo2/models/AccessListModel;", "getReaderInfo", "Lcom/smartportable/demo2/models/ReaderInfoModel;", "getSessions", "Lcom/smartportable/demo2/models/SessionsInfoModel;", "getStaffAccessInfo", "Lcom/smartportable/demo2/models/StaffAccessListModel;", "getStaffInfo", "Lcom/smartportable/demo2/models/StaffInfoModel;", "getStudentInfo", "Lcom/smartportable/demo2/models/StudentInfoModel;", "getStudentList", "getUserLogin", "Lcom/smartportable/demo2/models/LoginModel;", "getUserLogout", "getVisitorInfo", "Lcom/smartportable/demo2/models/VisitorInfoModel;", "setMainGateAccLogs", "Lcom/smartportable/demo2/models/MainGateAccLogsModel;", "setMemAccLog", "Lcom/smartportable/demo2/models/MemAccLogsInfoModel;", "app_debug"})
public abstract interface ServiceGenerator {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "StudentApp/Login")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Call<com.smartportable.demo2.models.LoginModel> getUserLogin(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject data);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "StudentApp/Logout")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Call<com.smartportable.demo2.models.LoginModel> getUserLogout(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject data);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "StudentApp/GetStudentInformation")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Call<com.smartportable.demo2.models.StudentInfoModel> getStudentInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject data);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "StudentApp/GetReader")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Call<com.smartportable.demo2.models.ReaderInfoModel> getReaderInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject data);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "StudentApp/GetStudentInformation")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Call<com.smartportable.demo2.models.ReaderInfoModel> getStudentList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject data);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "StudentApp/GetAccessList")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Call<com.smartportable.demo2.models.AccessListModel> getAccessList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject data);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "StudentApp/GetSessionList")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Call<com.smartportable.demo2.models.SessionsInfoModel> getSessions(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject data);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "StudentApp/GetStaffInformation")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Call<com.smartportable.demo2.models.StaffInfoModel> getStaffInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject data);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "StudentApp/GetStaffAccessList")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Call<com.smartportable.demo2.models.StaffAccessListModel> getStaffAccessInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject data);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "StudentApp/SetMemAccLog")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Call<com.smartportable.demo2.models.MemAccLogsInfoModel> setMemAccLog(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject data);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "StudentApp/GetVisitorList")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Call<com.smartportable.demo2.models.VisitorInfoModel> getVisitorInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject data);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "StudentApp/GetAGInformation")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Call<com.smartportable.demo2.models.AGInfoModel> getAGInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject data);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "StudentApp/GetAccessLogs")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract retrofit2.Call<com.smartportable.demo2.models.MainGateAccLogsModel> setMainGateAccLogs(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject data);
}