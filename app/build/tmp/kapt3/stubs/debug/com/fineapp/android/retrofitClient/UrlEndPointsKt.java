package com.fineapp.android.retrofitClient;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u001e\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\f\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\r\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000e\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000f\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0010\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0011\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0012\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0013\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0014\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0015\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0016\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0017\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0018\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0019\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001a\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001b\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001c\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001d\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001e\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"}, d2 = {"baseUrl", "", "createNotification", "getAGInfo", "getAccessList", "getCity", "getDashbordDEtailsEndpoint", "getEduLevel", "getLicenseCategoryEndpoint", "getLicenseSubCategoryEndpoint", "getNationality", "getNotification", "getReaderInfo", "getRegion", "getSessions", "getStaffAccessInfo", "getStaffInfo", "getStudentInfo", "getStudentList", "getSubCity", "getTrainingCenter", "getTrainingLanguage", "getUserLogin", "getUserLogout", "getUserProfile", "getVisitorInfo", "getZone", "imageBaseUrl", "sendNotificationImages", "setMainGateAccLogs", "setMemAccLog", "app_debug"})
public final class UrlEndPointsKt {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String baseUrl = "http://smartcapus.wiseportable.com/API/api/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String imageBaseUrl = "http://213.55.95.36:8080";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getUserLogin = "StudentApp/Login";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getUserLogout = "StudentApp/Logout";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getStudentInfo = "StudentApp/GetStudentInformation";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getReaderInfo = "StudentApp/GetReader";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getStudentList = "StudentApp/GetStudentInformation";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getAccessList = "StudentApp/GetAccessList";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getSessions = "StudentApp/GetSessionList";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getStaffInfo = "StudentApp/GetStaffInformation";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getStaffAccessInfo = "StudentApp/GetStaffAccessList";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String setMemAccLog = "StudentApp/SetMemAccLog";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getVisitorInfo = "StudentApp/GetVisitorList";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getAGInfo = "StudentApp/GetAGInformation";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String setMainGateAccLogs = "StudentApp/GetAccessLogs";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getUserProfile = "fineapp/GetOfficer";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getLicenseCategoryEndpoint = "fineapp/GetLicenseCategory";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getDashbordDEtailsEndpoint = "fineapp/GetNotificationVariant";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String createNotification = "fineapp/CreateNotification";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getNotification = "fineapp/GetNotification";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String sendNotificationImages = "FineApp/NotificationImages";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getLicenseSubCategoryEndpoint = "fineapp/GetLicenseSubCategory";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getNationality = "fineapp/GetNationality";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getEduLevel = "fineapp/GetEducationLevel";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getTrainingLanguage = "fineapp/GetTrainingLanguage";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getTrainingCenter = "fineapp/GetTrainingCenter";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getRegion = "fineapp/GetRegionList";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getZone = "fineapp/GetZoneList";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getCity = "fineapp/GetCityList";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getSubCity = "fineapp/GetSubCity";
}