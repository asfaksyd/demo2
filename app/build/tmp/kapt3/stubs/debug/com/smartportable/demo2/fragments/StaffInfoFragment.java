package com.smartportable.demo2.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b8\n\u0002\u0018\u0002\n\u0002\b>\n\u0002\u0018\u0002\n\u0002\b8\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u00c6\u0001\u001a\u00030\u00c7\u00012\b\u0010\u00c8\u0001\u001a\u00030\u008e\u0001H\u0002J\u0014\u0010\u00c9\u0001\u001a\u00030\u00c7\u00012\b\u0010\u00ca\u0001\u001a\u00030\u00cb\u0001H\u0016J\u0016\u0010\u00cc\u0001\u001a\u00030\u00c7\u00012\n\u0010\u00cd\u0001\u001a\u0005\u0018\u00010\u008e\u0001H\u0016J.\u0010\u00ce\u0001\u001a\u0005\u0018\u00010\u008e\u00012\b\u0010\u00cf\u0001\u001a\u00030\u00d0\u00012\n\u0010\u00d1\u0001\u001a\u0005\u0018\u00010\u00d2\u00012\n\u0010\u00d3\u0001\u001a\u0005\u0018\u00010\u00d4\u0001H\u0016J\u0016\u0010\u00d5\u0001\u001a\u00030\u00c7\u00012\n\u0010\u00d6\u0001\u001a\u0005\u0018\u00010\u00d7\u0001H\u0016J0\u0010\u00d8\u0001\u001a\u00030\u00c7\u00012\u001b\u0010\u00d9\u0001\u001a\u0016\u0012\u0005\u0012\u00030\u00db\u00010\u00da\u0001j\n\u0012\u0005\u0012\u00030\u00db\u0001`\u00dc\u00012\u0007\u0010\u00dd\u0001\u001a\u00020\u0006H\u0016R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\u0010X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u001a\u0010\u001b\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0018\"\u0004\b\u001d\u0010\u001aR\u001a\u0010\u001e\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0018\"\u0004\b \u0010\u001aR\u001a\u0010!\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u0018\"\u0004\b#\u0010\u001aR\u001a\u0010$\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\u0018\"\u0004\b&\u0010\u001aR\u001a\u0010\'\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0018\"\u0004\b)\u0010\u001aR\u001a\u0010*\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\u0018\"\u0004\b,\u0010\u001aR\u001a\u0010-\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\u0018\"\u0004\b/\u0010\u001aR\u001a\u00100\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u0010\u0018\"\u0004\b2\u0010\u001aR\u001a\u00103\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\u0018\"\u0004\b5\u0010\u001aR\u001a\u00106\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u0010\u0018\"\u0004\b8\u0010\u001aR\u001a\u00109\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010\u0018\"\u0004\b;\u0010\u001aR\u001a\u0010<\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b=\u0010\u0018\"\u0004\b>\u0010\u001aR\u001a\u0010?\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b@\u0010\u0018\"\u0004\bA\u0010\u001aR\u001a\u0010B\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bC\u0010\u0018\"\u0004\bD\u0010\u001aR\u001a\u0010E\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bF\u0010\u0018\"\u0004\bG\u0010\u001aR\u001a\u0010H\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bI\u0010\u0018\"\u0004\bJ\u0010\u001aR\u001a\u0010K\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bL\u0010\u0018\"\u0004\bM\u0010\u001aR\u001a\u0010N\u001a\u00020OX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bP\u0010Q\"\u0004\bR\u0010SR\u001a\u0010T\u001a\u00020OX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bU\u0010Q\"\u0004\bV\u0010SR\u001a\u0010W\u001a\u00020OX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bX\u0010Q\"\u0004\bY\u0010SR\u001a\u0010Z\u001a\u00020OX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b[\u0010Q\"\u0004\b\\\u0010SR\u001a\u0010]\u001a\u00020OX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b^\u0010Q\"\u0004\b_\u0010SR\u001a\u0010`\u001a\u00020OX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\ba\u0010Q\"\u0004\bb\u0010SR\u001a\u0010c\u001a\u00020OX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bd\u0010Q\"\u0004\be\u0010SR\u001a\u0010f\u001a\u00020OX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bg\u0010Q\"\u0004\bh\u0010SR\u001a\u0010i\u001a\u00020OX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bj\u0010Q\"\u0004\bk\u0010SR\u001a\u0010l\u001a\u00020OX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bm\u0010Q\"\u0004\bn\u0010SR\u001a\u0010o\u001a\u00020OX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bp\u0010Q\"\u0004\bq\u0010SR\u001a\u0010r\u001a\u00020OX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bs\u0010Q\"\u0004\bt\u0010SR\u001a\u0010u\u001a\u00020OX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bv\u0010Q\"\u0004\bw\u0010SR\u001a\u0010x\u001a\u00020OX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\by\u0010Q\"\u0004\bz\u0010SR\u001a\u0010{\u001a\u00020OX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b|\u0010Q\"\u0004\b}\u0010SR\u001b\u0010~\u001a\u00020OX\u0086.\u00a2\u0006\u000f\n\u0000\u001a\u0004\b\u007f\u0010Q\"\u0005\b\u0080\u0001\u0010SR\u001d\u0010\u0081\u0001\u001a\u00020OX\u0086.\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0082\u0001\u0010Q\"\u0005\b\u0083\u0001\u0010SR\u001d\u0010\u0084\u0001\u001a\u00020OX\u0086.\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0085\u0001\u0010Q\"\u0005\b\u0086\u0001\u0010SR\u001d\u0010\u0087\u0001\u001a\u00020OX\u0086.\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0088\u0001\u0010Q\"\u0005\b\u0089\u0001\u0010SR\u001d\u0010\u008a\u0001\u001a\u00020OX\u0086.\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u008b\u0001\u0010Q\"\u0005\b\u008c\u0001\u0010SR \u0010\u008d\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u008f\u0001\u0010\u0090\u0001\"\u0006\b\u0091\u0001\u0010\u0092\u0001R \u0010\u0093\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u0094\u0001\u0010\u0090\u0001\"\u0006\b\u0095\u0001\u0010\u0092\u0001R \u0010\u0096\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u0097\u0001\u0010\u0090\u0001\"\u0006\b\u0098\u0001\u0010\u0092\u0001R \u0010\u0099\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u009a\u0001\u0010\u0090\u0001\"\u0006\b\u009b\u0001\u0010\u0092\u0001R \u0010\u009c\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u009d\u0001\u0010\u0090\u0001\"\u0006\b\u009e\u0001\u0010\u0092\u0001R \u0010\u009f\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00a0\u0001\u0010\u0090\u0001\"\u0006\b\u00a1\u0001\u0010\u0092\u0001R \u0010\u00a2\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00a3\u0001\u0010\u0090\u0001\"\u0006\b\u00a4\u0001\u0010\u0092\u0001R \u0010\u00a5\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00a6\u0001\u0010\u0090\u0001\"\u0006\b\u00a7\u0001\u0010\u0092\u0001R \u0010\u00a8\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00a9\u0001\u0010\u0090\u0001\"\u0006\b\u00aa\u0001\u0010\u0092\u0001R \u0010\u00ab\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00ac\u0001\u0010\u0090\u0001\"\u0006\b\u00ad\u0001\u0010\u0092\u0001R \u0010\u00ae\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00af\u0001\u0010\u0090\u0001\"\u0006\b\u00b0\u0001\u0010\u0092\u0001R \u0010\u00b1\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00b2\u0001\u0010\u0090\u0001\"\u0006\b\u00b3\u0001\u0010\u0092\u0001R \u0010\u00b4\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00b5\u0001\u0010\u0090\u0001\"\u0006\b\u00b6\u0001\u0010\u0092\u0001R \u0010\u00b7\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00b8\u0001\u0010\u0090\u0001\"\u0006\b\u00b9\u0001\u0010\u0092\u0001R \u0010\u00ba\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00bb\u0001\u0010\u0090\u0001\"\u0006\b\u00bc\u0001\u0010\u0092\u0001R \u0010\u00bd\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00be\u0001\u0010\u0090\u0001\"\u0006\b\u00bf\u0001\u0010\u0092\u0001R \u0010\u00c0\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00c1\u0001\u0010\u0090\u0001\"\u0006\b\u00c2\u0001\u0010\u0092\u0001R \u0010\u00c3\u0001\u001a\u00030\u008e\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00c4\u0001\u0010\u0090\u0001\"\u0006\b\u00c5\u0001\u0010\u0092\u0001\u00a8\u0006\u00de\u0001"}, d2 = {"Lcom/smartportable/demo2/fragments/StaffInfoFragment;", "Landroidx/fragment/app/Fragment;", "Landroid/view/View$OnClickListener;", "Lcom/smartportable/demo2/listeners/OnStaffAuthDataReceived;", "()V", "TAG", "", "getTAG", "()Ljava/lang/String;", "changeCardStListener", "Lcom/smartportable/demo2/interfaces/ChangeCardStatus;", "getChangeCardStListener", "()Lcom/smartportable/demo2/interfaces/ChangeCardStatus;", "setChangeCardStListener", "(Lcom/smartportable/demo2/interfaces/ChangeCardStatus;)V", "db", "Lcom/smartportable/demo2/db/DatabaseHandler;", "getDb", "()Lcom/smartportable/demo2/db/DatabaseHandler;", "setDb", "(Lcom/smartportable/demo2/db/DatabaseHandler;)V", "llAddress", "Landroid/widget/LinearLayout;", "getLlAddress", "()Landroid/widget/LinearLayout;", "setLlAddress", "(Landroid/widget/LinearLayout;)V", "llAppNo", "getLlAppNo", "setLlAppNo", "llCardNo", "getLlCardNo", "setLlCardNo", "llCardStatus", "getLlCardStatus", "setLlCardStatus", "llDOB", "getLlDOB", "setLlDOB", "llDepartment", "getLlDepartment", "setLlDepartment", "llEmailId", "getLlEmailId", "setLlEmailId", "llEmpPhoto", "getLlEmpPhoto", "setLlEmpPhoto", "llFullName", "getLlFullName", "setLlFullName", "llGender", "getLlGender", "setLlGender", "llIdNo", "getLlIdNo", "setLlIdNo", "llIsActive", "getLlIsActive", "setLlIsActive", "llIssueDate", "getLlIssueDate", "setLlIssueDate", "llJobTitle", "getLlJobTitle", "setLlJobTitle", "llSignature", "getLlSignature", "setLlSignature", "llSlNo", "getLlSlNo", "setLlSlNo", "llStaffId", "getLlStaffId", "setLlStaffId", "llUID", "getLlUID", "setLlUID", "mTvAddress", "Landroid/widget/TextView;", "getMTvAddress", "()Landroid/widget/TextView;", "setMTvAddress", "(Landroid/widget/TextView;)V", "mTvAppNo", "getMTvAppNo", "setMTvAppNo", "mTvCardNo", "getMTvCardNo", "setMTvCardNo", "mTvCardStatus", "getMTvCardStatus", "setMTvCardStatus", "mTvDOB", "getMTvDOB", "setMTvDOB", "mTvDepartment", "getMTvDepartment", "setMTvDepartment", "mTvEmailId", "getMTvEmailId", "setMTvEmailId", "mTvEmpPhoto", "getMTvEmpPhoto", "setMTvEmpPhoto", "mTvFullName", "getMTvFullName", "setMTvFullName", "mTvGender", "getMTvGender", "setMTvGender", "mTvIdNo", "getMTvIdNo", "setMTvIdNo", "mTvIsActive", "getMTvIsActive", "setMTvIsActive", "mTvIssueDate", "getMTvIssueDate", "setMTvIssueDate", "mTvJobTitle", "getMTvJobTitle", "setMTvJobTitle", "mTvPassword", "getMTvPassword", "setMTvPassword", "mTvSignature", "getMTvSignature", "setMTvSignature", "mTvSlNo", "getMTvSlNo", "setMTvSlNo", "mTvStaffId", "getMTvStaffId", "setMTvStaffId", "mTvUID", "getMTvUID", "setMTvUID", "mtvId_Number", "getMtvId_Number", "setMtvId_Number", "viewAddress", "Landroid/view/View;", "getViewAddress", "()Landroid/view/View;", "setViewAddress", "(Landroid/view/View;)V", "viewAppNo", "getViewAppNo", "setViewAppNo", "viewCardNo", "getViewCardNo", "setViewCardNo", "viewCardStatus", "getViewCardStatus", "setViewCardStatus", "viewDOB", "getViewDOB", "setViewDOB", "viewDepartment", "getViewDepartment", "setViewDepartment", "viewEmailId", "getViewEmailId", "setViewEmailId", "viewEmpPhoto", "getViewEmpPhoto", "setViewEmpPhoto", "viewFullName", "getViewFullName", "setViewFullName", "viewGender", "getViewGender", "setViewGender", "viewIdNo", "getViewIdNo", "setViewIdNo", "viewIsActive", "getViewIsActive", "setViewIsActive", "viewIssueDate", "getViewIssueDate", "setViewIssueDate", "viewJobTitle", "getViewJobTitle", "setViewJobTitle", "viewSignature", "getViewSignature", "setViewSignature", "viewSlNo", "getViewSlNo", "setViewSlNo", "viewStaffId", "getViewStaffId", "setViewStaffId", "viewUID", "getViewUID", "setViewUID", "initView", "", "view", "onAttach", "context", "Landroid/content/Context;", "onClick", "v", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onStaffAuthApiDataReceived", "cardData", "Lcom/smartportable/demo2/models/StaffInfoModel$DataBean;", "onStaffAuthDataReceived", "list", "Ljava/util/ArrayList;", "Lcom/smartportable/demo2/models/MifareSettingModel;", "Lkotlin/collections/ArrayList;", "uuidNumber", "app_debug"})
public final class StaffInfoFragment extends androidx.fragment.app.Fragment implements android.view.View.OnClickListener, com.smartportable.demo2.listeners.OnStaffAuthDataReceived {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String TAG = null;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvStaffId;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvAppNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvSlNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvUID;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvFullName;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvGender;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvDOB;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvDepartment;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvJobTitle;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvEmpPhoto;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvSignature;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvAddress;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvIdNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvIssueDate;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvCardStatus;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvCardNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvEmailId;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvIsActive;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvPassword;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mtvId_Number;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llStaffId;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llAppNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llSlNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llUID;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llFullName;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llGender;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llDOB;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llDepartment;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llJobTitle;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llEmpPhoto;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llSignature;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llAddress;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llIdNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llIssueDate;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llCardStatus;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llCardNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llEmailId;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llIsActive;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewStaffId;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewAppNo;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewSlNo;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewUID;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewFullName;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewGender;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewDOB;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewDepartment;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewJobTitle;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewEmpPhoto;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewSignature;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewAddress;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewIdNo;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewIssueDate;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewCardStatus;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewCardNo;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewEmailId;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewIsActive;
    @org.jetbrains.annotations.NotNull()
    public com.smartportable.demo2.interfaces.ChangeCardStatus changeCardStListener;
    @org.jetbrains.annotations.NotNull()
    public com.smartportable.demo2.db.DatabaseHandler db;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTAG() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvStaffId() {
        return null;
    }
    
    public final void setMTvStaffId(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvAppNo() {
        return null;
    }
    
    public final void setMTvAppNo(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvSlNo() {
        return null;
    }
    
    public final void setMTvSlNo(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvUID() {
        return null;
    }
    
    public final void setMTvUID(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvFullName() {
        return null;
    }
    
    public final void setMTvFullName(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvGender() {
        return null;
    }
    
    public final void setMTvGender(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvDOB() {
        return null;
    }
    
    public final void setMTvDOB(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvDepartment() {
        return null;
    }
    
    public final void setMTvDepartment(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvJobTitle() {
        return null;
    }
    
    public final void setMTvJobTitle(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvEmpPhoto() {
        return null;
    }
    
    public final void setMTvEmpPhoto(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvSignature() {
        return null;
    }
    
    public final void setMTvSignature(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvAddress() {
        return null;
    }
    
    public final void setMTvAddress(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvIdNo() {
        return null;
    }
    
    public final void setMTvIdNo(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvIssueDate() {
        return null;
    }
    
    public final void setMTvIssueDate(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvCardStatus() {
        return null;
    }
    
    public final void setMTvCardStatus(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvCardNo() {
        return null;
    }
    
    public final void setMTvCardNo(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvEmailId() {
        return null;
    }
    
    public final void setMTvEmailId(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvIsActive() {
        return null;
    }
    
    public final void setMTvIsActive(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvPassword() {
        return null;
    }
    
    public final void setMTvPassword(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMtvId_Number() {
        return null;
    }
    
    public final void setMtvId_Number(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlStaffId() {
        return null;
    }
    
    public final void setLlStaffId(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlAppNo() {
        return null;
    }
    
    public final void setLlAppNo(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlSlNo() {
        return null;
    }
    
    public final void setLlSlNo(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlUID() {
        return null;
    }
    
    public final void setLlUID(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlFullName() {
        return null;
    }
    
    public final void setLlFullName(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlGender() {
        return null;
    }
    
    public final void setLlGender(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlDOB() {
        return null;
    }
    
    public final void setLlDOB(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlDepartment() {
        return null;
    }
    
    public final void setLlDepartment(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlJobTitle() {
        return null;
    }
    
    public final void setLlJobTitle(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlEmpPhoto() {
        return null;
    }
    
    public final void setLlEmpPhoto(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlSignature() {
        return null;
    }
    
    public final void setLlSignature(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlAddress() {
        return null;
    }
    
    public final void setLlAddress(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlIdNo() {
        return null;
    }
    
    public final void setLlIdNo(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlIssueDate() {
        return null;
    }
    
    public final void setLlIssueDate(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlCardStatus() {
        return null;
    }
    
    public final void setLlCardStatus(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlCardNo() {
        return null;
    }
    
    public final void setLlCardNo(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlEmailId() {
        return null;
    }
    
    public final void setLlEmailId(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlIsActive() {
        return null;
    }
    
    public final void setLlIsActive(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewStaffId() {
        return null;
    }
    
    public final void setViewStaffId(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewAppNo() {
        return null;
    }
    
    public final void setViewAppNo(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewSlNo() {
        return null;
    }
    
    public final void setViewSlNo(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewUID() {
        return null;
    }
    
    public final void setViewUID(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewFullName() {
        return null;
    }
    
    public final void setViewFullName(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewGender() {
        return null;
    }
    
    public final void setViewGender(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewDOB() {
        return null;
    }
    
    public final void setViewDOB(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewDepartment() {
        return null;
    }
    
    public final void setViewDepartment(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewJobTitle() {
        return null;
    }
    
    public final void setViewJobTitle(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewEmpPhoto() {
        return null;
    }
    
    public final void setViewEmpPhoto(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewSignature() {
        return null;
    }
    
    public final void setViewSignature(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewAddress() {
        return null;
    }
    
    public final void setViewAddress(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewIdNo() {
        return null;
    }
    
    public final void setViewIdNo(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewIssueDate() {
        return null;
    }
    
    public final void setViewIssueDate(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewCardStatus() {
        return null;
    }
    
    public final void setViewCardStatus(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewCardNo() {
        return null;
    }
    
    public final void setViewCardNo(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewEmailId() {
        return null;
    }
    
    public final void setViewEmailId(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewIsActive() {
        return null;
    }
    
    public final void setViewIsActive(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.interfaces.ChangeCardStatus getChangeCardStListener() {
        return null;
    }
    
    public final void setChangeCardStListener(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.interfaces.ChangeCardStatus p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.db.DatabaseHandler getDb() {
        return null;
    }
    
    public final void setDb(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.db.DatabaseHandler p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    private final void initView(android.view.View view) {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    @java.lang.Override()
    public void onStaffAuthDataReceived(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.MifareSettingModel> list, @org.jetbrains.annotations.NotNull()
    java.lang.String uuidNumber) {
    }
    
    @java.lang.Override()
    public void onStaffAuthApiDataReceived(@org.jetbrains.annotations.Nullable()
    com.smartportable.demo2.models.StaffInfoModel.DataBean cardData) {
    }
    
    public StaffInfoFragment() {
        super();
    }
}