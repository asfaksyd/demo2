package com.smartportable.demo2.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b,\n\u0002\u0018\u0002\n\u0002\b/\n\u0002\u0018\u0002\n\u0002\b,\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0013\u0010\u009f\u0001\u001a\u00030\u00a0\u00012\u0007\u0010\u00a1\u0001\u001a\u00020sH\u0002J\u0014\u0010\u00a2\u0001\u001a\u00030\u00a0\u00012\b\u0010\u00a3\u0001\u001a\u00030\u00a4\u0001H\u0016J\u0015\u0010\u00a5\u0001\u001a\u00030\u00a0\u00012\t\u0010\u00a6\u0001\u001a\u0004\u0018\u00010sH\u0016J-\u0010\u00a7\u0001\u001a\u0004\u0018\u00010s2\b\u0010\u00a8\u0001\u001a\u00030\u00a9\u00012\n\u0010\u00aa\u0001\u001a\u0005\u0018\u00010\u00ab\u00012\n\u0010\u00ac\u0001\u001a\u0005\u0018\u00010\u00ad\u0001H\u0016J\u0016\u0010\u00ae\u0001\u001a\u00030\u00a0\u00012\n\u0010\u00af\u0001\u001a\u0005\u0018\u00010\u00b0\u0001H\u0016J0\u0010\u00b1\u0001\u001a\u00030\u00a0\u00012\u001b\u0010\u00b2\u0001\u001a\u0016\u0012\u0005\u0012\u00030\u00b4\u00010\u00b3\u0001j\n\u0012\u0005\u0012\u00030\u00b4\u0001`\u00b5\u00012\u0007\u0010\u00b6\u0001\u001a\u00020\u0006H\u0016R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\u0010X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u001a\u0010\u001b\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0018\"\u0004\b\u001d\u0010\u001aR\u001a\u0010\u001e\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0018\"\u0004\b \u0010\u001aR\u001a\u0010!\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u0018\"\u0004\b#\u0010\u001aR\u001a\u0010$\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\u0018\"\u0004\b&\u0010\u001aR\u001a\u0010\'\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0018\"\u0004\b)\u0010\u001aR\u001a\u0010*\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\u0018\"\u0004\b,\u0010\u001aR\u001a\u0010-\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\u0018\"\u0004\b/\u0010\u001aR\u001a\u00100\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u0010\u0018\"\u0004\b2\u0010\u001aR\u001a\u00103\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\u0018\"\u0004\b5\u0010\u001aR\u001a\u00106\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u0010\u0018\"\u0004\b8\u0010\u001aR\u001a\u00109\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010\u0018\"\u0004\b;\u0010\u001aR\u001a\u0010<\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b=\u0010\u0018\"\u0004\b>\u0010\u001aR\u001a\u0010?\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b@\u0010\u0018\"\u0004\bA\u0010\u001aR\u001a\u0010B\u001a\u00020CX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bD\u0010E\"\u0004\bF\u0010GR\u001a\u0010H\u001a\u00020CX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bI\u0010E\"\u0004\bJ\u0010GR\u001a\u0010K\u001a\u00020CX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bL\u0010E\"\u0004\bM\u0010GR\u001a\u0010N\u001a\u00020CX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bO\u0010E\"\u0004\bP\u0010GR\u001a\u0010Q\u001a\u00020CX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bR\u0010E\"\u0004\bS\u0010GR\u001a\u0010T\u001a\u00020CX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bU\u0010E\"\u0004\bV\u0010GR\u001a\u0010W\u001a\u00020CX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bX\u0010E\"\u0004\bY\u0010GR\u001a\u0010Z\u001a\u00020CX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b[\u0010E\"\u0004\b\\\u0010GR\u001a\u0010]\u001a\u00020CX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b^\u0010E\"\u0004\b_\u0010GR\u001a\u0010`\u001a\u00020CX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\ba\u0010E\"\u0004\bb\u0010GR\u001a\u0010c\u001a\u00020CX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bd\u0010E\"\u0004\be\u0010GR\u001a\u0010f\u001a\u00020CX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bg\u0010E\"\u0004\bh\u0010GR\u001a\u0010i\u001a\u00020CX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bj\u0010E\"\u0004\bk\u0010GR\u001a\u0010l\u001a\u00020CX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bm\u0010E\"\u0004\bn\u0010GR\u001a\u0010o\u001a\u00020CX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bp\u0010E\"\u0004\bq\u0010GR\u001a\u0010r\u001a\u00020sX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bt\u0010u\"\u0004\bv\u0010wR\u001a\u0010x\u001a\u00020sX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\by\u0010u\"\u0004\bz\u0010wR\u001a\u0010{\u001a\u00020sX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b|\u0010u\"\u0004\b}\u0010wR\u001b\u0010~\u001a\u00020sX\u0086.\u00a2\u0006\u000f\n\u0000\u001a\u0004\b\u007f\u0010u\"\u0005\b\u0080\u0001\u0010wR\u001d\u0010\u0081\u0001\u001a\u00020sX\u0086.\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0082\u0001\u0010u\"\u0005\b\u0083\u0001\u0010wR\u001d\u0010\u0084\u0001\u001a\u00020sX\u0086.\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0085\u0001\u0010u\"\u0005\b\u0086\u0001\u0010wR\u001d\u0010\u0087\u0001\u001a\u00020sX\u0086.\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0088\u0001\u0010u\"\u0005\b\u0089\u0001\u0010wR\u001d\u0010\u008a\u0001\u001a\u00020sX\u0086.\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u008b\u0001\u0010u\"\u0005\b\u008c\u0001\u0010wR\u001d\u0010\u008d\u0001\u001a\u00020sX\u0086.\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u008e\u0001\u0010u\"\u0005\b\u008f\u0001\u0010wR\u001d\u0010\u0090\u0001\u001a\u00020sX\u0086.\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0091\u0001\u0010u\"\u0005\b\u0092\u0001\u0010wR\u001d\u0010\u0093\u0001\u001a\u00020sX\u0086.\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0094\u0001\u0010u\"\u0005\b\u0095\u0001\u0010wR\u001d\u0010\u0096\u0001\u001a\u00020sX\u0086.\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0097\u0001\u0010u\"\u0005\b\u0098\u0001\u0010wR\u001d\u0010\u0099\u0001\u001a\u00020sX\u0086.\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u009a\u0001\u0010u\"\u0005\b\u009b\u0001\u0010wR\u001d\u0010\u009c\u0001\u001a\u00020sX\u0086.\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u009d\u0001\u0010u\"\u0005\b\u009e\u0001\u0010w\u00a8\u0006\u00b7\u0001"}, d2 = {"Lcom/smartportable/demo2/fragments/VisitorInfoFragment;", "Landroidx/fragment/app/Fragment;", "Landroid/view/View$OnClickListener;", "Lcom/smartportable/demo2/listeners/OnVisitorAuthDataReceived;", "()V", "TAG", "", "getTAG", "()Ljava/lang/String;", "changeCardStListener", "Lcom/smartportable/demo2/interfaces/ChangeCardStatus;", "getChangeCardStListener", "()Lcom/smartportable/demo2/interfaces/ChangeCardStatus;", "setChangeCardStListener", "(Lcom/smartportable/demo2/interfaces/ChangeCardStatus;)V", "db", "Lcom/smartportable/demo2/db/DatabaseHandler;", "getDb", "()Lcom/smartportable/demo2/db/DatabaseHandler;", "setDb", "(Lcom/smartportable/demo2/db/DatabaseHandler;)V", "llVisAccessLevel", "Landroid/widget/LinearLayout;", "getLlVisAccessLevel", "()Landroid/widget/LinearLayout;", "setLlVisAccessLevel", "(Landroid/widget/LinearLayout;)V", "llVisCardNumber", "getLlVisCardNumber", "setLlVisCardNumber", "llVisCardStatus", "getLlVisCardStatus", "setLlVisCardStatus", "llVisCheckInTime", "getLlVisCheckInTime", "setLlVisCheckInTime", "llVisCheckOutTime", "getLlVisCheckOutTime", "setLlVisCheckOutTime", "llVisComapnyName", "getLlVisComapnyName", "setLlVisComapnyName", "llVisEmailId", "getLlVisEmailId", "setLlVisEmailId", "llVisFirstName", "getLlVisFirstName", "setLlVisFirstName", "llVisLastName", "getLlVisLastName", "setLlVisLastName", "llVisPhoneNo", "getLlVisPhoneNo", "setLlVisPhoneNo", "llVisRegNo", "getLlVisRegNo", "setLlVisRegNo", "llVisStatus", "getLlVisStatus", "setLlVisStatus", "llVisitReason", "getLlVisitReason", "setLlVisitReason", "llVisitorType", "getLlVisitorType", "setLlVisitorType", "mTvCompanyName", "Landroid/widget/TextView;", "getMTvCompanyName", "()Landroid/widget/TextView;", "setMTvCompanyName", "(Landroid/widget/TextView;)V", "mTvJobTitle", "getMTvJobTitle", "setMTvJobTitle", "mTvVisAccessLevel", "getMTvVisAccessLevel", "setMTvVisAccessLevel", "mTvVisCardNumber", "getMTvVisCardNumber", "setMTvVisCardNumber", "mTvVisCardStatus", "getMTvVisCardStatus", "setMTvVisCardStatus", "mTvVisCheckInTime", "getMTvVisCheckInTime", "setMTvVisCheckInTime", "mTvVisCheckOutTime", "getMTvVisCheckOutTime", "setMTvVisCheckOutTime", "mTvVisEmailId", "getMTvVisEmailId", "setMTvVisEmailId", "mTvVisFirstName", "getMTvVisFirstName", "setMTvVisFirstName", "mTvVisLastName", "getMTvVisLastName", "setMTvVisLastName", "mTvVisPhoneNo", "getMTvVisPhoneNo", "setMTvVisPhoneNo", "mTvVisRegNo", "getMTvVisRegNo", "setMTvVisRegNo", "mTvVisStatus", "getMTvVisStatus", "setMTvVisStatus", "mTvVisitReason", "getMTvVisitReason", "setMTvVisitReason", "mTvVisitorType", "getMTvVisitorType", "setMTvVisitorType", "viewComapnyName", "Landroid/view/View;", "getViewComapnyName", "()Landroid/view/View;", "setViewComapnyName", "(Landroid/view/View;)V", "viewVisAccessLevel", "getViewVisAccessLevel", "setViewVisAccessLevel", "viewVisCardNumber", "getViewVisCardNumber", "setViewVisCardNumber", "viewVisCardStatus", "getViewVisCardStatus", "setViewVisCardStatus", "viewVisCheckInTime", "getViewVisCheckInTime", "setViewVisCheckInTime", "viewVisCheckOutTime", "getViewVisCheckOutTime", "setViewVisCheckOutTime", "viewVisEmailId", "getViewVisEmailId", "setViewVisEmailId", "viewVisFirstName", "getViewVisFirstName", "setViewVisFirstName", "viewVisLastName", "getViewVisLastName", "setViewVisLastName", "viewVisPhoneNo", "getViewVisPhoneNo", "setViewVisPhoneNo", "viewVisRegNo", "getViewVisRegNo", "setViewVisRegNo", "viewVisStatus", "getViewVisStatus", "setViewVisStatus", "viewVisiitReason", "getViewVisiitReason", "setViewVisiitReason", "viewVisitorType", "getViewVisitorType", "setViewVisitorType", "initView", "", "view", "onAttach", "context", "Landroid/content/Context;", "onClick", "v", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onVisitorAuthApiDataReceived", "cardData", "Lcom/smartportable/demo2/models/VisitorInfoModel$DataBean;", "onVisitorAuthDataReceived", "list", "Ljava/util/ArrayList;", "Lcom/smartportable/demo2/models/MifareSettingModel;", "Lkotlin/collections/ArrayList;", "uuidNumber", "app_debug"})
public final class VisitorInfoFragment extends androidx.fragment.app.Fragment implements android.view.View.OnClickListener, com.smartportable.demo2.listeners.OnVisitorAuthDataReceived {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String TAG = null;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvVisRegNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvVisFirstName;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvVisLastName;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvCompanyName;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvVisitorType;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvVisitReason;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvVisPhoneNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvVisEmailId;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvJobTitle;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvVisAccessLevel;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvVisCheckInTime;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvVisCheckOutTime;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvVisCardNumber;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvVisCardStatus;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvVisStatus;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llVisRegNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llVisFirstName;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llVisLastName;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llVisComapnyName;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llVisitorType;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llVisitReason;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llVisPhoneNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llVisEmailId;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llVisAccessLevel;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llVisCheckInTime;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llVisCheckOutTime;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llVisCardNumber;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llVisCardStatus;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llVisStatus;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewVisRegNo;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewVisFirstName;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewVisLastName;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewComapnyName;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewVisitorType;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewVisiitReason;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewVisPhoneNo;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewVisEmailId;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewVisAccessLevel;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewVisCheckInTime;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewVisCheckOutTime;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewVisCardNumber;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewVisCardStatus;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewVisStatus;
    @org.jetbrains.annotations.NotNull()
    public com.smartportable.demo2.interfaces.ChangeCardStatus changeCardStListener;
    @org.jetbrains.annotations.NotNull()
    public com.smartportable.demo2.db.DatabaseHandler db;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTAG() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvVisRegNo() {
        return null;
    }
    
    public final void setMTvVisRegNo(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvVisFirstName() {
        return null;
    }
    
    public final void setMTvVisFirstName(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvVisLastName() {
        return null;
    }
    
    public final void setMTvVisLastName(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvCompanyName() {
        return null;
    }
    
    public final void setMTvCompanyName(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvVisitorType() {
        return null;
    }
    
    public final void setMTvVisitorType(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvVisitReason() {
        return null;
    }
    
    public final void setMTvVisitReason(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvVisPhoneNo() {
        return null;
    }
    
    public final void setMTvVisPhoneNo(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvVisEmailId() {
        return null;
    }
    
    public final void setMTvVisEmailId(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvJobTitle() {
        return null;
    }
    
    public final void setMTvJobTitle(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvVisAccessLevel() {
        return null;
    }
    
    public final void setMTvVisAccessLevel(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvVisCheckInTime() {
        return null;
    }
    
    public final void setMTvVisCheckInTime(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvVisCheckOutTime() {
        return null;
    }
    
    public final void setMTvVisCheckOutTime(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvVisCardNumber() {
        return null;
    }
    
    public final void setMTvVisCardNumber(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvVisCardStatus() {
        return null;
    }
    
    public final void setMTvVisCardStatus(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvVisStatus() {
        return null;
    }
    
    public final void setMTvVisStatus(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlVisRegNo() {
        return null;
    }
    
    public final void setLlVisRegNo(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlVisFirstName() {
        return null;
    }
    
    public final void setLlVisFirstName(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlVisLastName() {
        return null;
    }
    
    public final void setLlVisLastName(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlVisComapnyName() {
        return null;
    }
    
    public final void setLlVisComapnyName(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlVisitorType() {
        return null;
    }
    
    public final void setLlVisitorType(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlVisitReason() {
        return null;
    }
    
    public final void setLlVisitReason(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlVisPhoneNo() {
        return null;
    }
    
    public final void setLlVisPhoneNo(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlVisEmailId() {
        return null;
    }
    
    public final void setLlVisEmailId(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlVisAccessLevel() {
        return null;
    }
    
    public final void setLlVisAccessLevel(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlVisCheckInTime() {
        return null;
    }
    
    public final void setLlVisCheckInTime(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlVisCheckOutTime() {
        return null;
    }
    
    public final void setLlVisCheckOutTime(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlVisCardNumber() {
        return null;
    }
    
    public final void setLlVisCardNumber(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlVisCardStatus() {
        return null;
    }
    
    public final void setLlVisCardStatus(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlVisStatus() {
        return null;
    }
    
    public final void setLlVisStatus(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewVisRegNo() {
        return null;
    }
    
    public final void setViewVisRegNo(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewVisFirstName() {
        return null;
    }
    
    public final void setViewVisFirstName(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewVisLastName() {
        return null;
    }
    
    public final void setViewVisLastName(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewComapnyName() {
        return null;
    }
    
    public final void setViewComapnyName(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewVisitorType() {
        return null;
    }
    
    public final void setViewVisitorType(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewVisiitReason() {
        return null;
    }
    
    public final void setViewVisiitReason(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewVisPhoneNo() {
        return null;
    }
    
    public final void setViewVisPhoneNo(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewVisEmailId() {
        return null;
    }
    
    public final void setViewVisEmailId(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewVisAccessLevel() {
        return null;
    }
    
    public final void setViewVisAccessLevel(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewVisCheckInTime() {
        return null;
    }
    
    public final void setViewVisCheckInTime(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewVisCheckOutTime() {
        return null;
    }
    
    public final void setViewVisCheckOutTime(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewVisCardNumber() {
        return null;
    }
    
    public final void setViewVisCardNumber(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewVisCardStatus() {
        return null;
    }
    
    public final void setViewVisCardStatus(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewVisStatus() {
        return null;
    }
    
    public final void setViewVisStatus(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.interfaces.ChangeCardStatus getChangeCardStListener() {
        return null;
    }
    
    public final void setChangeCardStListener(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.interfaces.ChangeCardStatus p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.db.DatabaseHandler getDb() {
        return null;
    }
    
    public final void setDb(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.db.DatabaseHandler p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    private final void initView(android.view.View view) {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    @java.lang.Override()
    public void onVisitorAuthDataReceived(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.MifareSettingModel> list, @org.jetbrains.annotations.NotNull()
    java.lang.String uuidNumber) {
    }
    
    @java.lang.Override()
    public void onVisitorAuthApiDataReceived(@org.jetbrains.annotations.Nullable()
    com.smartportable.demo2.models.VisitorInfoModel.DataBean cardData) {
    }
    
    public VisitorInfoFragment() {
        super();
    }
}