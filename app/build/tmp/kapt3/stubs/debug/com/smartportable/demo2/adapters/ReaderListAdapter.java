package com.smartportable.demo2.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001dB%\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\b\u00a2\u0006\u0002\u0010\tJ\b\u0010\u0012\u001a\u00020\u0013H\u0016J\u0018\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u0013H\u0016J\u0018\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0013H\u0016J\u001e\u0010\u001c\u001a\u00020\u00152\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bR\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR*\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011\u00a8\u0006\u001e"}, d2 = {"Lcom/smartportable/demo2/adapters/ReaderListAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/smartportable/demo2/adapters/ReaderListAdapter$ReaderViewHolder;", "mContext", "Landroid/content/Context;", "readersList", "Ljava/util/ArrayList;", "Lcom/smartportable/demo2/models/ReaderInfoModel$DataBean;", "Lkotlin/collections/ArrayList;", "(Landroid/content/Context;Ljava/util/ArrayList;)V", "getMContext", "()Landroid/content/Context;", "setMContext", "(Landroid/content/Context;)V", "getReadersList", "()Ljava/util/ArrayList;", "setReadersList", "(Ljava/util/ArrayList;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "updateReadersList", "ReaderViewHolder", "app_debug"})
public final class ReaderListAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.smartportable.demo2.adapters.ReaderListAdapter.ReaderViewHolder> {
    @org.jetbrains.annotations.Nullable()
    private android.content.Context mContext;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.smartportable.demo2.models.ReaderInfoModel.DataBean> readersList;
    
    @org.jetbrains.annotations.Nullable()
    public final android.content.Context getMContext() {
        return null;
    }
    
    public final void setMContext(@org.jetbrains.annotations.Nullable()
    android.content.Context p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.smartportable.demo2.models.ReaderInfoModel.DataBean> getReadersList() {
        return null;
    }
    
    public final void setReadersList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.ReaderInfoModel.DataBean> p0) {
    }
    
    public final void updateReadersList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.ReaderInfoModel.DataBean> readersList) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.smartportable.demo2.adapters.ReaderListAdapter.ReaderViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.adapters.ReaderListAdapter.ReaderViewHolder holder, int position) {
    }
    
    public ReaderListAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.ReaderInfoModel.DataBean> readersList) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0019\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0019\u0010\n\u001a\n \u0007*\u0004\u0018\u00010\u000b0\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0019\u0010\u000e\u001a\n \u0007*\u0004\u0018\u00010\u000b0\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\r\u00a8\u0006\u0010"}, d2 = {"Lcom/smartportable/demo2/adapters/ReaderListAdapter$ReaderViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "ivReaderType", "Landroid/widget/ImageView;", "kotlin.jvm.PlatformType", "getIvReaderType", "()Landroid/widget/ImageView;", "tvReaderId", "Landroid/widget/TextView;", "getTvReaderId", "()Landroid/widget/TextView;", "tvReaderName", "getTvReaderName", "app_debug"})
    public static final class ReaderViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final android.widget.TextView tvReaderId = null;
        private final android.widget.TextView tvReaderName = null;
        private final android.widget.ImageView ivReaderType = null;
        
        public final android.widget.TextView getTvReaderId() {
            return null;
        }
        
        public final android.widget.TextView getTvReaderName() {
            return null;
        }
        
        public final android.widget.ImageView getIvReaderType() {
            return null;
        }
        
        public ReaderViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
    }
}