package com.smartportable.demo2.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b5\n\u0002\u0018\u0002\n\u0002\b5\n\u0002\u0018\u0002\n\u0002\b5\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u00b7\u0001\u001a\u00030\u00b8\u00012\b\u0010\u00b9\u0001\u001a\u00030\u0082\u0001H\u0002J\u0016\u0010\u00ba\u0001\u001a\u00030\u00b8\u00012\n\u0010\u00bb\u0001\u001a\u0005\u0018\u00010\u00bc\u0001H\u0016J0\u0010\u00bd\u0001\u001a\u00030\u00b8\u00012\u001b\u0010\u00be\u0001\u001a\u0016\u0012\u0005\u0012\u00030\u00c0\u00010\u00bf\u0001j\n\u0012\u0005\u0012\u00030\u00c0\u0001`\u00c1\u00012\u0007\u0010\u00c2\u0001\u001a\u00020\u0006H\u0016J\u0016\u0010\u00c3\u0001\u001a\u00030\u00b8\u00012\n\u0010\u00c4\u0001\u001a\u0005\u0018\u00010\u0082\u0001H\u0016J.\u0010\u00c5\u0001\u001a\u0005\u0018\u00010\u0082\u00012\b\u0010\u00c6\u0001\u001a\u00030\u00c7\u00012\n\u0010\u00c8\u0001\u001a\u0005\u0018\u00010\u00c9\u00012\n\u0010\u00ca\u0001\u001a\u0005\u0018\u00010\u00cb\u0001H\u0016R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\u0010X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u001a\u0010\u001b\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0018\"\u0004\b\u001d\u0010\u001aR\u001a\u0010\u001e\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0018\"\u0004\b \u0010\u001aR\u001a\u0010!\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u0018\"\u0004\b#\u0010\u001aR\u001a\u0010$\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\u0018\"\u0004\b&\u0010\u001aR\u001a\u0010\'\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0018\"\u0004\b)\u0010\u001aR\u001a\u0010*\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\u0018\"\u0004\b,\u0010\u001aR\u001a\u0010-\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\u0018\"\u0004\b/\u0010\u001aR\u001a\u00100\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u0010\u0018\"\u0004\b2\u0010\u001aR\u001a\u00103\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\u0018\"\u0004\b5\u0010\u001aR\u001a\u00106\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u0010\u0018\"\u0004\b8\u0010\u001aR\u001a\u00109\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010\u0018\"\u0004\b;\u0010\u001aR\u001a\u0010<\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b=\u0010\u0018\"\u0004\b>\u0010\u001aR\u001a\u0010?\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b@\u0010\u0018\"\u0004\bA\u0010\u001aR\u001a\u0010B\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bC\u0010\u0018\"\u0004\bD\u0010\u001aR\u001a\u0010E\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bF\u0010\u0018\"\u0004\bG\u0010\u001aR\u001a\u0010H\u001a\u00020\u0016X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bI\u0010\u0018\"\u0004\bJ\u0010\u001aR\u001a\u0010K\u001a\u00020LX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bM\u0010N\"\u0004\bO\u0010PR\u001a\u0010Q\u001a\u00020LX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bR\u0010N\"\u0004\bS\u0010PR\u001a\u0010T\u001a\u00020LX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bU\u0010N\"\u0004\bV\u0010PR\u001a\u0010W\u001a\u00020LX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bX\u0010N\"\u0004\bY\u0010PR\u001a\u0010Z\u001a\u00020LX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b[\u0010N\"\u0004\b\\\u0010PR\u001a\u0010]\u001a\u00020LX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b^\u0010N\"\u0004\b_\u0010PR\u001a\u0010`\u001a\u00020LX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\ba\u0010N\"\u0004\bb\u0010PR\u001a\u0010c\u001a\u00020LX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bd\u0010N\"\u0004\be\u0010PR\u001a\u0010f\u001a\u00020LX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bg\u0010N\"\u0004\bh\u0010PR\u001a\u0010i\u001a\u00020LX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bj\u0010N\"\u0004\bk\u0010PR\u001a\u0010l\u001a\u00020LX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bm\u0010N\"\u0004\bn\u0010PR\u001a\u0010o\u001a\u00020LX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bp\u0010N\"\u0004\bq\u0010PR\u001a\u0010r\u001a\u00020LX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bs\u0010N\"\u0004\bt\u0010PR\u001a\u0010u\u001a\u00020LX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bv\u0010N\"\u0004\bw\u0010PR\u001a\u0010x\u001a\u00020LX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\by\u0010N\"\u0004\bz\u0010PR\u001a\u0010{\u001a\u00020LX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b|\u0010N\"\u0004\b}\u0010PR\u001b\u0010~\u001a\u00020LX\u0086.\u00a2\u0006\u000f\n\u0000\u001a\u0004\b\u007f\u0010N\"\u0005\b\u0080\u0001\u0010PR \u0010\u0081\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u0083\u0001\u0010\u0084\u0001\"\u0006\b\u0085\u0001\u0010\u0086\u0001R \u0010\u0087\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u0088\u0001\u0010\u0084\u0001\"\u0006\b\u0089\u0001\u0010\u0086\u0001R \u0010\u008a\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u008b\u0001\u0010\u0084\u0001\"\u0006\b\u008c\u0001\u0010\u0086\u0001R \u0010\u008d\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u008e\u0001\u0010\u0084\u0001\"\u0006\b\u008f\u0001\u0010\u0086\u0001R \u0010\u0090\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u0091\u0001\u0010\u0084\u0001\"\u0006\b\u0092\u0001\u0010\u0086\u0001R \u0010\u0093\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u0094\u0001\u0010\u0084\u0001\"\u0006\b\u0095\u0001\u0010\u0086\u0001R \u0010\u0096\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u0097\u0001\u0010\u0084\u0001\"\u0006\b\u0098\u0001\u0010\u0086\u0001R \u0010\u0099\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u009a\u0001\u0010\u0084\u0001\"\u0006\b\u009b\u0001\u0010\u0086\u0001R \u0010\u009c\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u009d\u0001\u0010\u0084\u0001\"\u0006\b\u009e\u0001\u0010\u0086\u0001R \u0010\u009f\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00a0\u0001\u0010\u0084\u0001\"\u0006\b\u00a1\u0001\u0010\u0086\u0001R \u0010\u00a2\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00a3\u0001\u0010\u0084\u0001\"\u0006\b\u00a4\u0001\u0010\u0086\u0001R \u0010\u00a5\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00a6\u0001\u0010\u0084\u0001\"\u0006\b\u00a7\u0001\u0010\u0086\u0001R \u0010\u00a8\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00a9\u0001\u0010\u0084\u0001\"\u0006\b\u00aa\u0001\u0010\u0086\u0001R \u0010\u00ab\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00ac\u0001\u0010\u0084\u0001\"\u0006\b\u00ad\u0001\u0010\u0086\u0001R \u0010\u00ae\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00af\u0001\u0010\u0084\u0001\"\u0006\b\u00b0\u0001\u0010\u0086\u0001R \u0010\u00b1\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00b2\u0001\u0010\u0084\u0001\"\u0006\b\u00b3\u0001\u0010\u0086\u0001R \u0010\u00b4\u0001\u001a\u00030\u0082\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00b5\u0001\u0010\u0084\u0001\"\u0006\b\u00b6\u0001\u0010\u0086\u0001\u00a8\u0006\u00cc\u0001"}, d2 = {"Lcom/smartportable/demo2/fragments/StudentInfoFragment;", "Landroidx/fragment/app/Fragment;", "Landroid/view/View$OnClickListener;", "Lcom/smartportable/demo2/listeners/OnAuthDataReceived;", "()V", "TAG", "", "getTAG", "()Ljava/lang/String;", "changeCardStListener", "Lcom/smartportable/demo2/interfaces/ChangeCardStatus;", "getChangeCardStListener", "()Lcom/smartportable/demo2/interfaces/ChangeCardStatus;", "setChangeCardStListener", "(Lcom/smartportable/demo2/interfaces/ChangeCardStatus;)V", "db", "Lcom/smartportable/demo2/db/DatabaseHandler;", "getDb", "()Lcom/smartportable/demo2/db/DatabaseHandler;", "setDb", "(Lcom/smartportable/demo2/db/DatabaseHandler;)V", "llAdmissionType", "Landroid/widget/LinearLayout;", "getLlAdmissionType", "()Landroid/widget/LinearLayout;", "setLlAdmissionType", "(Landroid/widget/LinearLayout;)V", "llAdmissionTypeShort", "getLlAdmissionTypeShort", "setLlAdmissionTypeShort", "llCampus", "getLlCampus", "setLlCampus", "llCollege", "getLlCollege", "setLlCollege", "llDateOfBirth", "getLlDateOfBirth", "setLlDateOfBirth", "llDegreeType", "getLlDegreeType", "setLlDegreeType", "llDepartment", "getLlDepartment", "setLlDepartment", "llFullName", "getLlFullName", "setLlFullName", "llGender", "getLlGender", "setLlGender", "llIssueDate", "getLlIssueDate", "setLlIssueDate", "llMealNumber", "getLlMealNumber", "setLlMealNumber", "llProgram", "getLlProgram", "setLlProgram", "llStatus", "getLlStatus", "setLlStatus", "llStudentId", "getLlStudentId", "setLlStudentId", "llUUIDNo", "getLlUUIDNo", "setLlUUIDNo", "llUniqueNo", "getLlUniqueNo", "setLlUniqueNo", "llValidUntil", "getLlValidUntil", "setLlValidUntil", "mTvAdmissionType", "Landroid/widget/TextView;", "getMTvAdmissionType", "()Landroid/widget/TextView;", "setMTvAdmissionType", "(Landroid/widget/TextView;)V", "mTvAdmissionTypeShort", "getMTvAdmissionTypeShort", "setMTvAdmissionTypeShort", "mTvCampus", "getMTvCampus", "setMTvCampus", "mTvCollege", "getMTvCollege", "setMTvCollege", "mTvDateOfBirth", "getMTvDateOfBirth", "setMTvDateOfBirth", "mTvDegreeType", "getMTvDegreeType", "setMTvDegreeType", "mTvDepartment", "getMTvDepartment", "setMTvDepartment", "mTvFullName", "getMTvFullName", "setMTvFullName", "mTvGender", "getMTvGender", "setMTvGender", "mTvIssueDate", "getMTvIssueDate", "setMTvIssueDate", "mTvMealNumber", "getMTvMealNumber", "setMTvMealNumber", "mTvProgram", "getMTvProgram", "setMTvProgram", "mTvStatus", "getMTvStatus", "setMTvStatus", "mTvStudentId", "getMTvStudentId", "setMTvStudentId", "mTvUUIDNo", "getMTvUUIDNo", "setMTvUUIDNo", "mTvUniqueNo", "getMTvUniqueNo", "setMTvUniqueNo", "mTvValidUntil", "getMTvValidUntil", "setMTvValidUntil", "viewAdmissionType", "Landroid/view/View;", "getViewAdmissionType", "()Landroid/view/View;", "setViewAdmissionType", "(Landroid/view/View;)V", "viewAdmissionTypeShort", "getViewAdmissionTypeShort", "setViewAdmissionTypeShort", "viewCampus", "getViewCampus", "setViewCampus", "viewCollege", "getViewCollege", "setViewCollege", "viewDateOfBirth", "getViewDateOfBirth", "setViewDateOfBirth", "viewDegreeType", "getViewDegreeType", "setViewDegreeType", "viewDepartment", "getViewDepartment", "setViewDepartment", "viewFullName", "getViewFullName", "setViewFullName", "viewGender", "getViewGender", "setViewGender", "viewIssueDate", "getViewIssueDate", "setViewIssueDate", "viewMealNumber", "getViewMealNumber", "setViewMealNumber", "viewProgram", "getViewProgram", "setViewProgram", "viewStatus", "getViewStatus", "setViewStatus", "viewStudentId", "getViewStudentId", "setViewStudentId", "viewUUIDNo", "getViewUUIDNo", "setViewUUIDNo", "viewUniqueNo", "getViewUniqueNo", "setViewUniqueNo", "viewValidUntil", "getViewValidUntil", "setViewValidUntil", "initView", "", "view", "onAuthApiDataReceived", "cardData", "Lcom/smartportable/demo2/models/StudentInfoModel$DataBean;", "onAuthDataReceived", "list", "Ljava/util/ArrayList;", "Lcom/smartportable/demo2/models/MifareSettingModel;", "Lkotlin/collections/ArrayList;", "uuidNumber", "onClick", "v", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "app_debug"})
public final class StudentInfoFragment extends androidx.fragment.app.Fragment implements android.view.View.OnClickListener, com.smartportable.demo2.listeners.OnAuthDataReceived {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String TAG = null;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llStudentId;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llIssueDate;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llFullName;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llGender;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llDateOfBirth;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llCollege;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llDepartment;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llCampus;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llProgram;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llDegreeType;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llAdmissionType;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llAdmissionTypeShort;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llValidUntil;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llMealNumber;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llUniqueNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llStatus;
    @org.jetbrains.annotations.NotNull()
    public android.widget.LinearLayout llUUIDNo;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewStudentId;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewIssueDate;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewFullName;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewGender;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewDateOfBirth;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewCollege;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewDepartment;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewCampus;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewProgram;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewDegreeType;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewAdmissionType;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewAdmissionTypeShort;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewValidUntil;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewMealNumber;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewUniqueNo;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewStatus;
    @org.jetbrains.annotations.NotNull()
    public android.view.View viewUUIDNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvStudentId;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvIssueDate;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvFullName;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvGender;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvDateOfBirth;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvCollege;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvDepartment;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvCampus;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvProgram;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvDegreeType;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvAdmissionType;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvAdmissionTypeShort;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvValidUntil;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvMealNumber;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvUniqueNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvStatus;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView mTvUUIDNo;
    @org.jetbrains.annotations.NotNull()
    public com.smartportable.demo2.db.DatabaseHandler db;
    @org.jetbrains.annotations.NotNull()
    public com.smartportable.demo2.interfaces.ChangeCardStatus changeCardStListener;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTAG() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlStudentId() {
        return null;
    }
    
    public final void setLlStudentId(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlIssueDate() {
        return null;
    }
    
    public final void setLlIssueDate(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlFullName() {
        return null;
    }
    
    public final void setLlFullName(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlGender() {
        return null;
    }
    
    public final void setLlGender(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlDateOfBirth() {
        return null;
    }
    
    public final void setLlDateOfBirth(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlCollege() {
        return null;
    }
    
    public final void setLlCollege(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlDepartment() {
        return null;
    }
    
    public final void setLlDepartment(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlCampus() {
        return null;
    }
    
    public final void setLlCampus(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlProgram() {
        return null;
    }
    
    public final void setLlProgram(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlDegreeType() {
        return null;
    }
    
    public final void setLlDegreeType(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlAdmissionType() {
        return null;
    }
    
    public final void setLlAdmissionType(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlAdmissionTypeShort() {
        return null;
    }
    
    public final void setLlAdmissionTypeShort(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlValidUntil() {
        return null;
    }
    
    public final void setLlValidUntil(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlMealNumber() {
        return null;
    }
    
    public final void setLlMealNumber(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlUniqueNo() {
        return null;
    }
    
    public final void setLlUniqueNo(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlStatus() {
        return null;
    }
    
    public final void setLlStatus(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.LinearLayout getLlUUIDNo() {
        return null;
    }
    
    public final void setLlUUIDNo(@org.jetbrains.annotations.NotNull()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewStudentId() {
        return null;
    }
    
    public final void setViewStudentId(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewIssueDate() {
        return null;
    }
    
    public final void setViewIssueDate(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewFullName() {
        return null;
    }
    
    public final void setViewFullName(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewGender() {
        return null;
    }
    
    public final void setViewGender(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewDateOfBirth() {
        return null;
    }
    
    public final void setViewDateOfBirth(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewCollege() {
        return null;
    }
    
    public final void setViewCollege(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewDepartment() {
        return null;
    }
    
    public final void setViewDepartment(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewCampus() {
        return null;
    }
    
    public final void setViewCampus(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewProgram() {
        return null;
    }
    
    public final void setViewProgram(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewDegreeType() {
        return null;
    }
    
    public final void setViewDegreeType(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewAdmissionType() {
        return null;
    }
    
    public final void setViewAdmissionType(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewAdmissionTypeShort() {
        return null;
    }
    
    public final void setViewAdmissionTypeShort(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewValidUntil() {
        return null;
    }
    
    public final void setViewValidUntil(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewMealNumber() {
        return null;
    }
    
    public final void setViewMealNumber(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewUniqueNo() {
        return null;
    }
    
    public final void setViewUniqueNo(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewStatus() {
        return null;
    }
    
    public final void setViewStatus(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getViewUUIDNo() {
        return null;
    }
    
    public final void setViewUUIDNo(@org.jetbrains.annotations.NotNull()
    android.view.View p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvStudentId() {
        return null;
    }
    
    public final void setMTvStudentId(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvIssueDate() {
        return null;
    }
    
    public final void setMTvIssueDate(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvFullName() {
        return null;
    }
    
    public final void setMTvFullName(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvGender() {
        return null;
    }
    
    public final void setMTvGender(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvDateOfBirth() {
        return null;
    }
    
    public final void setMTvDateOfBirth(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvCollege() {
        return null;
    }
    
    public final void setMTvCollege(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvDepartment() {
        return null;
    }
    
    public final void setMTvDepartment(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvCampus() {
        return null;
    }
    
    public final void setMTvCampus(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvProgram() {
        return null;
    }
    
    public final void setMTvProgram(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvDegreeType() {
        return null;
    }
    
    public final void setMTvDegreeType(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvAdmissionType() {
        return null;
    }
    
    public final void setMTvAdmissionType(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvAdmissionTypeShort() {
        return null;
    }
    
    public final void setMTvAdmissionTypeShort(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvValidUntil() {
        return null;
    }
    
    public final void setMTvValidUntil(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvMealNumber() {
        return null;
    }
    
    public final void setMTvMealNumber(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvUniqueNo() {
        return null;
    }
    
    public final void setMTvUniqueNo(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvStatus() {
        return null;
    }
    
    public final void setMTvStatus(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getMTvUUIDNo() {
        return null;
    }
    
    public final void setMTvUUIDNo(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.db.DatabaseHandler getDb() {
        return null;
    }
    
    public final void setDb(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.db.DatabaseHandler p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.interfaces.ChangeCardStatus getChangeCardStListener() {
        return null;
    }
    
    public final void setChangeCardStListener(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.interfaces.ChangeCardStatus p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void initView(android.view.View view) {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    @java.lang.Override()
    public void onAuthDataReceived(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.MifareSettingModel> list, @org.jetbrains.annotations.NotNull()
    java.lang.String uuidNumber) {
    }
    
    @java.lang.Override()
    public void onAuthApiDataReceived(@org.jetbrains.annotations.Nullable()
    com.smartportable.demo2.models.StudentInfoModel.DataBean cardData) {
    }
    
    public StudentInfoFragment() {
        super();
    }
}