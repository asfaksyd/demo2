package com.smartportable.demo2.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0012\u001a\u00020\u0013H\u0016J&\u0010\u0014\u001a\u00020\u00152\u001e\u0010\u0016\u001a\u001a\u0012\b\u0012\u00060\u0018R\u00020\u00190\u0017j\f\u0012\b\u0012\u00060\u0018R\u00020\u0019`\u001aR\u001a\u0010\u0007\u001a\u00020\bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\u00020\u0003X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011\u00a8\u0006\u001b"}, d2 = {"Lcom/smartportable/demo2/services/SendMainGateWorker;", "Landroidx/work/Worker;", "mContext", "Landroid/content/Context;", "params", "Landroidx/work/WorkerParameters;", "(Landroid/content/Context;Landroidx/work/WorkerParameters;)V", "db", "Lcom/smartportable/demo2/db/DatabaseHandler;", "getDb", "()Lcom/smartportable/demo2/db/DatabaseHandler;", "setDb", "(Lcom/smartportable/demo2/db/DatabaseHandler;)V", "mCtx", "getMCtx", "()Landroid/content/Context;", "setMCtx", "(Landroid/content/Context;)V", "doWork", "Landroidx/work/ListenableWorker$Result;", "sendPendingLogs", "", "list", "Ljava/util/ArrayList;", "Lcom/smartportable/demo2/models/MainGateAccLogs$Data;", "Lcom/smartportable/demo2/models/MainGateAccLogs;", "Lkotlin/collections/ArrayList;", "app_debug"})
public final class SendMainGateWorker extends androidx.work.Worker {
    @org.jetbrains.annotations.NotNull()
    public android.content.Context mCtx;
    @org.jetbrains.annotations.NotNull()
    public com.smartportable.demo2.db.DatabaseHandler db;
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getMCtx() {
        return null;
    }
    
    public final void setMCtx(@org.jetbrains.annotations.NotNull()
    android.content.Context p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.db.DatabaseHandler getDb() {
        return null;
    }
    
    public final void setDb(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.db.DatabaseHandler p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public androidx.work.ListenableWorker.Result doWork() {
        return null;
    }
    
    public final void sendPendingLogs(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.MainGateAccLogs.Data> list) {
    }
    
    public SendMainGateWorker(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    androidx.work.WorkerParameters params) {
        super(null, null);
    }
}