package com.smartportable.demo2.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0010\b\n\u0002\bD\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\f\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\r\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000e\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000f\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0010\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0011\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0012\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0013\u001a\u00020\u0014X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0015\u001a\u00020\u0014X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0016\u001a\u00020\u0014X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0017\u001a\u00020\u0014X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0018\u001a\u00020\u0014X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0019\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001a\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001b\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001c\u001a\u00020\u0014X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001d\u001a\u00020\u0014X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001e\u001a\u00020\u0014X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001f\u001a\u00020\u0014X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010 \u001a\u00020\u0014X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010!\u001a\u00020\u0014X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\"\u001a\u00020\u0014X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010#\u001a\u00020\u0014X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010$\u001a\u00020\u0014X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010%\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010&\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\'\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010(\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010)\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010*\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010+\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010,\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010-\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010.\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010/\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u00100\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u00101\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u00102\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u00103\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u00104\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u00105\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u00106\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u00107\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u00108\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u00109\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010:\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010;\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010<\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010=\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010>\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010?\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010@\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010A\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010B\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010C\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010D\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010E\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010F\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010G\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010H\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010I\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010J\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010K\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010L\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010M\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010N\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010O\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010P\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010Q\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010R\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010S\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010T\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010U\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010V\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010W\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006X"}, d2 = {"API_DATA", "", "API_LOGIN_CANTEEN_ID", "API_LOGIN_CANTEEN_NAME", "API_LOGIN_EMAIL", "API_LOGIN_FULLNAME", "API_LOGIN_ID", "API_LOGIN_PASSWORD", "API_LOGIN_PHOTO", "API_LOGIN_STAFFID", "API_LOGIN_USER_ID", "API_LOGIN_USER_TYPE_ID", "API_LOGIN_USER_TYPE_NAME", "API_MESSAGE", "API_PAGE_COUNT", "API_STATUS", "API_TOKEN", "API_TOKEN_EXPIRE", "AUTH_TYPE", "CARD_ST_ACTIVE", "", "CARD_ST_EXPIRED", "CARD_ST_LOST", "CARD_ST_REVOKED", "CARD_ST_SUSPENDED", "DEVICEID", "DEVICETYPE", "DEVICETYPE_VAL", "READ_BARCODE", "READ_CARD_BY_SEARCH", "READ_CARD_NUMBER", "READ_CARD_NUMBER_OFF", "READ_CARD_OFFLINE", "VIS_CARD_ST_ACTIVE", "VIS_CARD_ST_EXPIRED", "VIS_CARD_ST_IN_USE", "VIS_CARD_ST_LOST", "keyAPIBaseUrl", "keyActive", "keyAdmissionType", "keyAdmissionTypeShort", "keyAuthToken", "keyBaseUrl", "keyCampus", "keyCanteenId", "keyCanteenName", "keyCardNo", "keyCollege", "keyCurrentTime", "keyDateOfBirth", "keyDegreeType", "keyDepartment", "keyDeviceId", "keyDeviceType", "keyDoorId", "keyDoorName", "keyEmail", "keyExpirationTime", "keyFatherName", "keyFirstName", "keyFullName", "keyGender", "keyGrandFatherName", "keyId", "keyIsactive", "keyIssueDate", "keyMealNumber", "keyPassword", "keyPhoto", "keyProgram", "keyRowIndex", "keySignature", "keyStaffCode", "keyStaffCollege", "keyStaffDept", "keyStaffFullName", "keyStaffId", "keyStaffJobTitle", "keyStaffSex", "keyStatus", "keyStudentId", "keyTotalRecord", "keyUniqueNo", "keyUserId", "keyUserTypeId", "keyUserTypeName", "keyValidDateUntil", "phone", "app_debug"})
public final class KeysKt {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyId = "id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyUserId = "user_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyStaffId = "staffid";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String phone = "phone";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyEmail = "email";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyPassword = "password";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyAuthToken = "Authorization";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyExpirationTime = "token_expire";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyCurrentTime = "current_time";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyPhoto = "photo";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyCanteenId = "CanteenId";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyCanteenName = "CanteenName";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyUserTypeId = "UserTypeId";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyUserTypeName = "UserTypeName";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyDeviceId = "device_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyDeviceType = "device_type";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyTotalRecord = "total_record";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyRowIndex = "row_index";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyBaseUrl = "base_url";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyAPIBaseUrl = "base_url_api";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyStudentId = "StudentID";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyFullName = "FullName";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyFirstName = "FirstName";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyFatherName = "FatherName";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyGrandFatherName = "GrandFatherName";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyGender = "Gender";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyDateOfBirth = "DateOfBirth";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keySignature = "Signature";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyCollege = "College";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyDepartment = "Department";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyCampus = "Campus";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyProgram = "Program";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyDegreeType = "DegreeType";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyAdmissionType = "AdmissionType";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyAdmissionTypeShort = "AdmissionTypeShort";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyValidDateUntil = "ValidDateUntil";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyIssueDate = "IssueDate";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyMealNumber = "MealNumber";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyUniqueNo = "Uniqueno";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyStatus = "Status";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyIsactive = "Isactive";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyActive = "Active";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyCardNo = "CardNo";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyStaffCode = "stacode";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyStaffFullName = "FullNeng";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyStaffSex = "sex";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyStaffJobTitle = "jobtitle";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyStaffDept = "deptname";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyStaffCollege = "college";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyDoorId = "DoorId";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String keyDoorName = "DoorName";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String AUTH_TYPE = "auth_type";
    public static final int READ_CARD_OFFLINE = 1;
    public static final int READ_CARD_NUMBER = 2;
    public static final int READ_BARCODE = 3;
    public static final int READ_CARD_BY_SEARCH = 4;
    public static final int READ_CARD_NUMBER_OFF = 5;
    public static final int CARD_ST_ACTIVE = 1;
    public static final int CARD_ST_REVOKED = 2;
    public static final int CARD_ST_LOST = 3;
    public static final int CARD_ST_SUSPENDED = 4;
    public static final int CARD_ST_EXPIRED = 5;
    public static final int VIS_CARD_ST_ACTIVE = 1;
    public static final int VIS_CARD_ST_IN_USE = 2;
    public static final int VIS_CARD_ST_EXPIRED = 3;
    public static final int VIS_CARD_ST_LOST = 4;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_STATUS = "Status";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_MESSAGE = "Message";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_PAGE_COUNT = "PageCount";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_DATA = "Data";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_TOKEN = "Token";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_TOKEN_EXPIRE = "TokenExpiredTime";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DEVICEID = "DeviceId";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DEVICETYPE = "DeviceType";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DEVICETYPE_VAL = "Android";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_LOGIN_ID = "Id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_LOGIN_USER_ID = "UserId";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_LOGIN_EMAIL = "Email";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_LOGIN_PASSWORD = "Password";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_LOGIN_STAFFID = "StaffId";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_LOGIN_FULLNAME = "FullName";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_LOGIN_PHOTO = "StaffImage";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_LOGIN_CANTEEN_ID = "CanteenId";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_LOGIN_CANTEEN_NAME = "CanteenName";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_LOGIN_USER_TYPE_ID = "UserTypeId";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String API_LOGIN_USER_TYPE_NAME = "UserTypeName";
}