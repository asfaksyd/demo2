package com.smartportable.demo2.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 F2\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0001FB\u0005\u00a2\u0006\u0002\u0010\u0005J\u000e\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u00020\u0007J\u000e\u00100\u001a\u00020.2\u0006\u00101\u001a\u000202J\u0012\u00103\u001a\u00020.2\b\u00104\u001a\u0004\u0018\u000105H\u0016J\"\u00106\u001a\u00020.2\u000e\u00107\u001a\n\u0012\u0004\u0012\u000208\u0018\u00010\u000b2\b\u00109\u001a\u0004\u0018\u00010\u0007H\u0016J\u0012\u0010:\u001a\u00020.2\b\u0010;\u001a\u0004\u0018\u000102H\u0016J&\u0010<\u001a\u0004\u0018\u0001022\u0006\u0010=\u001a\u00020>2\b\u0010?\u001a\u0004\u0018\u00010@2\b\u0010A\u001a\u0004\u0018\u00010BH\u0016J\u0012\u0010C\u001a\u00020.2\b\u00104\u001a\u0004\u0018\u00010DH\u0016J\"\u0010E\u001a\u00020.2\u000e\u00107\u001a\n\u0012\u0004\u0012\u000208\u0018\u00010\u000b2\b\u00109\u001a\u0004\u0018\u00010\u0007H\u0016R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR \u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u00020\u0012X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001a\u0010\u0017\u001a\u00020\u0007X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\t\"\u0004\b\u0019\u0010\u001aR\u001a\u0010\u001b\u001a\u00020\u001cX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u001a\u0010!\u001a\u00020\u001cX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u001e\"\u0004\b#\u0010 R \u0010$\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\u000e\"\u0004\b&\u0010\u0010R\u001a\u0010\'\u001a\u00020(X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,\u00a8\u0006G"}, d2 = {"Lcom/smartportable/demo2/fragments/DoorAccessInfoFragment;", "Landroidx/fragment/app/Fragment;", "Landroid/view/View$OnClickListener;", "Lcom/smartportable/demo2/listeners/OnAuthDataReceived;", "Lcom/smartportable/demo2/listeners/OnStaffAuthDataReceived;", "()V", "TAG", "", "getTAG", "()Ljava/lang/String;", "canteenList", "Ljava/util/ArrayList;", "Lcom/smartportable/demo2/models/DoorAccessInfoModel;", "getCanteenList", "()Ljava/util/ArrayList;", "setCanteenList", "(Ljava/util/ArrayList;)V", "db", "Lcom/smartportable/demo2/db/DatabaseHandler;", "getDb", "()Lcom/smartportable/demo2/db/DatabaseHandler;", "setDb", "(Lcom/smartportable/demo2/db/DatabaseHandler;)V", "fromStr", "getFromStr", "setFromStr", "(Ljava/lang/String;)V", "mCanteenAdapter", "Lcom/smartportable/demo2/adapters/DoorAccessInfoAdapter;", "getMCanteenAdapter", "()Lcom/smartportable/demo2/adapters/DoorAccessInfoAdapter;", "setMCanteenAdapter", "(Lcom/smartportable/demo2/adapters/DoorAccessInfoAdapter;)V", "mMainGateAdapter", "getMMainGateAdapter", "setMMainGateAdapter", "mainGateList", "getMainGateList", "setMainGateList", "rvMainGateList", "Landroidx/recyclerview/widget/RecyclerView;", "getRvMainGateList", "()Landroidx/recyclerview/widget/RecyclerView;", "setRvMainGateList", "(Landroidx/recyclerview/widget/RecyclerView;)V", "fillData", "", "id", "initView", "view", "Landroid/view/View;", "onAuthApiDataReceived", "cardData", "Lcom/smartportable/demo2/models/StudentInfoModel$DataBean;", "onAuthDataReceived", "list", "Lcom/smartportable/demo2/models/MifareSettingModel;", "cardNo", "onClick", "v", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onStaffAuthApiDataReceived", "Lcom/smartportable/demo2/models/StaffInfoModel$DataBean;", "onStaffAuthDataReceived", "Companion", "app_debug"})
public final class DoorAccessInfoFragment extends androidx.fragment.app.Fragment implements android.view.View.OnClickListener, com.smartportable.demo2.listeners.OnAuthDataReceived, com.smartportable.demo2.listeners.OnStaffAuthDataReceived {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String TAG = null;
    @org.jetbrains.annotations.NotNull()
    public androidx.recyclerview.widget.RecyclerView rvMainGateList;
    @org.jetbrains.annotations.NotNull()
    public com.smartportable.demo2.adapters.DoorAccessInfoAdapter mMainGateAdapter;
    @org.jetbrains.annotations.NotNull()
    public com.smartportable.demo2.adapters.DoorAccessInfoAdapter mCanteenAdapter;
    @org.jetbrains.annotations.NotNull()
    public java.util.ArrayList<com.smartportable.demo2.models.DoorAccessInfoModel> mainGateList;
    @org.jetbrains.annotations.NotNull()
    public java.util.ArrayList<com.smartportable.demo2.models.DoorAccessInfoModel> canteenList;
    @org.jetbrains.annotations.NotNull()
    public com.smartportable.demo2.db.DatabaseHandler db;
    @org.jetbrains.annotations.NotNull()
    public java.lang.String fromStr;
    public static final com.smartportable.demo2.fragments.DoorAccessInfoFragment.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTAG() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.recyclerview.widget.RecyclerView getRvMainGateList() {
        return null;
    }
    
    public final void setRvMainGateList(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.adapters.DoorAccessInfoAdapter getMMainGateAdapter() {
        return null;
    }
    
    public final void setMMainGateAdapter(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.adapters.DoorAccessInfoAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.adapters.DoorAccessInfoAdapter getMCanteenAdapter() {
        return null;
    }
    
    public final void setMCanteenAdapter(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.adapters.DoorAccessInfoAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.smartportable.demo2.models.DoorAccessInfoModel> getMainGateList() {
        return null;
    }
    
    public final void setMainGateList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.DoorAccessInfoModel> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.smartportable.demo2.models.DoorAccessInfoModel> getCanteenList() {
        return null;
    }
    
    public final void setCanteenList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.DoorAccessInfoModel> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.db.DatabaseHandler getDb() {
        return null;
    }
    
    public final void setDb(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.db.DatabaseHandler p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFromStr() {
        return null;
    }
    
    public final void setFromStr(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    public final void initView(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    public final void fillData(@org.jetbrains.annotations.NotNull()
    java.lang.String id) {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    @java.lang.Override()
    public void onAuthApiDataReceived(@org.jetbrains.annotations.Nullable()
    com.smartportable.demo2.models.StudentInfoModel.DataBean cardData) {
    }
    
    @java.lang.Override()
    public void onAuthDataReceived(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<com.smartportable.demo2.models.MifareSettingModel> list, @org.jetbrains.annotations.Nullable()
    java.lang.String cardNo) {
    }
    
    @java.lang.Override()
    public void onStaffAuthApiDataReceived(@org.jetbrains.annotations.Nullable()
    com.smartportable.demo2.models.StaffInfoModel.DataBean cardData) {
    }
    
    @java.lang.Override()
    public void onStaffAuthDataReceived(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<com.smartportable.demo2.models.MifareSettingModel> list, @org.jetbrains.annotations.Nullable()
    java.lang.String cardNo) {
    }
    
    public DoorAccessInfoFragment() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/smartportable/demo2/fragments/DoorAccessInfoFragment$Companion;", "", "()V", "newInstance", "Lcom/smartportable/demo2/fragments/DoorAccessInfoFragment;", "msg", "", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.smartportable.demo2.fragments.DoorAccessInfoFragment newInstance(@org.jetbrains.annotations.NotNull()
        java.lang.String msg) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}