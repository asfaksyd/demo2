package com.smartportable.demo2.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00e2\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0017\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0004\u00b7\u0002\u00b8\u0002B\u0005\u00a2\u0006\u0002\u0010\u0005J\b\u0010\u00ec\u0001\u001a\u00030\u00ed\u0001J\n\u0010\u00ee\u0001\u001a\u00030\u00ed\u0001H\u0002J\u0011\u0010\u00ef\u0001\u001a\u00030\u00ed\u00012\u0007\u0010\u00f0\u0001\u001a\u00020\u0007J\b\u0010\u00f1\u0001\u001a\u00030\u00ed\u0001J\b\u0010\u00f2\u0001\u001a\u00030\u00ed\u0001J\b\u0010\u00f3\u0001\u001a\u00030\u00ed\u0001J\b\u0010\u00f4\u0001\u001a\u00030\u00ed\u0001J\b\u0010\u00f5\u0001\u001a\u00030\u00ed\u0001J\n\u0010\u00f6\u0001\u001a\u00030\u00ed\u0001H\u0002J\n\u0010\u00f7\u0001\u001a\u00030\u00ed\u0001H\u0002J\u0012\u0010\u00f8\u0001\u001a\u00030\u00f9\u00012\b\u0010\u00da\u0001\u001a\u00030\u00db\u0001J\u0015\u0010\u00fa\u0001\u001a\u00030\u00ed\u00012\t\u0010\u00fb\u0001\u001a\u0004\u0018\u00010\u0007H\u0016J\u001d\u0010\u00fc\u0001\u001a\u00030\u00ed\u00012\b\u0010\u00fd\u0001\u001a\u00030\u00fe\u00012\u0007\u0010\u00ff\u0001\u001a\u00020\u001bH\u0016J\u0016\u0010\u0080\u0002\u001a\u00030\u00ed\u00012\n\u0010\u0081\u0002\u001a\u0005\u0018\u00010\u0082\u0002H\u0016J\u0016\u0010\u0083\u0002\u001a\u00030\u00ed\u00012\n\u0010\u0084\u0002\u001a\u0005\u0018\u00010\u0085\u0002H\u0014J\u0016\u0010\u0086\u0002\u001a\u00030\u0087\u00022\n\u0010\u0088\u0002\u001a\u0005\u0018\u00010\u0089\u0002H\u0016J\n\u0010\u008a\u0002\u001a\u00030\u00ed\u0001H\u0014J\u001d\u0010\u008b\u0002\u001a\u00030\u0087\u00022\u0007\u0010\u008c\u0002\u001a\u00020\u001b2\b\u0010\u008d\u0002\u001a\u00030\u008e\u0002H\u0016J\u001d\u0010\u008f\u0002\u001a\u00030\u0087\u00022\u0007\u0010\u008c\u0002\u001a\u00020\u001b2\b\u0010\u008d\u0002\u001a\u00030\u008e\u0002H\u0016J\u0016\u0010\u0090\u0002\u001a\u00030\u0087\u00022\n\u0010\u0091\u0002\u001a\u0005\u0018\u00010\u0092\u0002H\u0016J\n\u0010\u0093\u0002\u001a\u00030\u00ed\u0001H\u0014J\u0016\u0010\u0094\u0002\u001a\u00030\u0087\u00022\n\u0010\u0088\u0002\u001a\u0005\u0018\u00010\u0089\u0002H\u0016J\n\u0010\u0095\u0002\u001a\u00030\u00ed\u0001H\u0014J\n\u0010\u0096\u0002\u001a\u00030\u00ed\u0001H\u0016J\n\u0010\u0097\u0002\u001a\u00030\u00ed\u0001H\u0016J\u0016\u0010\u0098\u0002\u001a\u00030\u00ed\u00012\n\u0010\u0099\u0002\u001a\u0005\u0018\u00010\u009a\u0002H\u0016J\u0014\u0010\u009b\u0002\u001a\u00030\u00ed\u00012\b\u0010\u009c\u0002\u001a\u00030\u0087\u0002H\u0002J\u0012\u0010\u009d\u0002\u001a\u00030\u00ed\u00012\b\u0010\u009e\u0002\u001a\u00030\u009f\u0002J\u0011\u0010\u00a0\u0002\u001a\u00020\u00072\b\u0010\u00a1\u0002\u001a\u00030\u00a2\u0002J\n\u0010\u00a3\u0002\u001a\u00030\u00ed\u0001H\u0002J\u0011\u0010\u00a4\u0002\u001a\u00030\u00ed\u00012\u0007\u0010\u00fb\u0001\u001a\u00020\u0007J\u001b\u0010\u00a5\u0002\u001a\u00030\u00ed\u00012\b\u0010\u00fd\u0001\u001a\u00030\u00fe\u00012\u0007\u0010\u00ff\u0001\u001a\u00020\u001bJ\u0012\u0010\u00a6\u0002\u001a\u00030\u00ed\u00012\b\u0010\u00a7\u0002\u001a\u00030\u008b\u0001J\u0012\u0010\u00a8\u0002\u001a\u00030\u00ed\u00012\b\u0010\u00a9\u0002\u001a\u00030\u008b\u0001J\u0012\u0010\u00aa\u0002\u001a\u00030\u00ed\u00012\b\u0010\u00ab\u0002\u001a\u00030\u008b\u0001J\u0014\u0010\u00ac\u0002\u001a\u00030\u00ed\u00012\b\u0010\u00e6\u0001\u001a\u00030\u00e7\u0001H\u0002J\u0014\u0010\u00ad\u0002\u001a\u00030\u00ed\u00012\b\u0010\u00e6\u0001\u001a\u00030\u00e7\u0001H\u0002J\b\u0010\u00ae\u0002\u001a\u00030\u00ed\u0001J\b\u0010\u00af\u0002\u001a\u00030\u00ed\u0001J\b\u0010\u00b0\u0002\u001a\u00030\u00ed\u0001J\b\u0010\u00b1\u0002\u001a\u00030\u00ed\u0001J\u0013\u0010\u00b2\u0002\u001a\u00030\u00ed\u00012\t\u0010\u00b3\u0002\u001a\u0004\u0018\u00010\u0007J\n\u0010\u00b4\u0002\u001a\u00030\u00ed\u0001H\u0002J\u0010\u0010\u00b5\u0002\u001a\u00030\u00ed\u0001H\u0000\u00a2\u0006\u0003\b\u00b6\u0002R\u0014\u0010\u0006\u001a\u00020\u0007X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\u00020\u0007X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\tR\u0014\u0010\f\u001a\u00020\u0007X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\tR\u000e\u0010\u000e\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0010\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0016\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\tR\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001a\u001a\u00020\u001bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u001a\u0010 \u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\t\"\u0004\b\"\u0010#R\u001c\u0010$\u001a\u0004\u0018\u00010%X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\'\"\u0004\b(\u0010)R\u001a\u0010*\u001a\u00020\u001bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\u001d\"\u0004\b,\u0010\u001fR\u001a\u0010-\u001a\u00020.X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u00100\"\u0004\b1\u00102R\u001a\u00103\u001a\u00020\u0007X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\t\"\u0004\b5\u0010#R\u001a\u00106\u001a\u000207X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b8\u00109\"\u0004\b:\u0010;R\u001a\u0010<\u001a\u00020=X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b>\u0010?\"\u0004\b@\u0010AR\u001a\u0010B\u001a\u00020CX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bD\u0010E\"\u0004\bF\u0010GR\u001a\u0010H\u001a\u00020\u0007X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bI\u0010\t\"\u0004\bJ\u0010#R\u001a\u0010K\u001a\u00020\u0007X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bL\u0010\t\"\u0004\bM\u0010#R\u001a\u0010N\u001a\u00020OX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bP\u0010Q\"\u0004\bR\u0010SR\u001a\u0010T\u001a\u00020UX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bV\u0010W\"\u0004\bX\u0010YR\u001a\u0010Z\u001a\u00020[X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\\\u0010]\"\u0004\b^\u0010_R\u001a\u0010`\u001a\u00020aX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bb\u0010c\"\u0004\bd\u0010eR\u001a\u0010f\u001a\u00020aX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bg\u0010c\"\u0004\bh\u0010eR \u0010i\u001a\b\u0012\u0004\u0012\u00020k0jX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bl\u0010m\"\u0004\bn\u0010oR \u0010p\u001a\b\u0012\u0004\u0012\u00020r0qX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\bs\u0010t\"\u0004\bu\u0010vR\u0010\u0010w\u001a\u0004\u0018\u00010xX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010y\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bz\u0010{\"\u0004\b|\u0010}R\u001e\u0010~\u001a\u00020\u007fX\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u0080\u0001\u0010\u0081\u0001\"\u0006\b\u0082\u0001\u0010\u0083\u0001R \u0010\u0084\u0001\u001a\u00030\u0085\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u0086\u0001\u0010\u0087\u0001\"\u0006\b\u0088\u0001\u0010\u0089\u0001R \u0010\u008a\u0001\u001a\u00030\u008b\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u008c\u0001\u0010\u008d\u0001\"\u0006\b\u008e\u0001\u0010\u008f\u0001R \u0010\u0090\u0001\u001a\u00030\u008b\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u0091\u0001\u0010\u008d\u0001\"\u0006\b\u0092\u0001\u0010\u008f\u0001R \u0010\u0093\u0001\u001a\u00030\u008b\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u0094\u0001\u0010\u008d\u0001\"\u0006\b\u0095\u0001\u0010\u008f\u0001R \u0010\u0096\u0001\u001a\u00030\u0097\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u0098\u0001\u0010\u0099\u0001\"\u0006\b\u009a\u0001\u0010\u009b\u0001R \u0010\u009c\u0001\u001a\u00030\u0097\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u009d\u0001\u0010\u0099\u0001\"\u0006\b\u009e\u0001\u0010\u009b\u0001R \u0010\u009f\u0001\u001a\u00030\u0097\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00a0\u0001\u0010\u0099\u0001\"\u0006\b\u00a1\u0001\u0010\u009b\u0001R \u0010\u00a2\u0001\u001a\u00030\u0097\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00a3\u0001\u0010\u0099\u0001\"\u0006\b\u00a4\u0001\u0010\u009b\u0001R \u0010\u00a5\u0001\u001a\u00030\u0097\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00a6\u0001\u0010\u0099\u0001\"\u0006\b\u00a7\u0001\u0010\u009b\u0001R \u0010\u00a8\u0001\u001a\u00030\u0097\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00a9\u0001\u0010\u0099\u0001\"\u0006\b\u00aa\u0001\u0010\u009b\u0001R \u0010\u00ab\u0001\u001a\u00030\u0097\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00ac\u0001\u0010\u0099\u0001\"\u0006\b\u00ad\u0001\u0010\u009b\u0001R$\u0010\u00ae\u0001\u001a\u00070\u00af\u0001R\u00020\u0000X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00b0\u0001\u0010\u00b1\u0001\"\u0006\b\u00b2\u0001\u0010\u00b3\u0001R \u0010\u00b4\u0001\u001a\u00030\u00b5\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00b6\u0001\u0010\u00b7\u0001\"\u0006\b\u00b8\u0001\u0010\u00b9\u0001R \u0010\u00ba\u0001\u001a\u00030\u00b5\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00bb\u0001\u0010\u00b7\u0001\"\u0006\b\u00bc\u0001\u0010\u00b9\u0001R \u0010\u00bd\u0001\u001a\u00030\u00be\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00bf\u0001\u0010\u00c0\u0001\"\u0006\b\u00c1\u0001\u0010\u00c2\u0001R \u0010\u00c3\u0001\u001a\u00030\u00be\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00c4\u0001\u0010\u00c0\u0001\"\u0006\b\u00c5\u0001\u0010\u00c2\u0001R \u0010\u00c6\u0001\u001a\u00030\u00be\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00c7\u0001\u0010\u00c0\u0001\"\u0006\b\u00c8\u0001\u0010\u00c2\u0001R\u0010\u0010\u00c9\u0001\u001a\u00030\u00ca\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u00cb\u0001\u001a\u00020\u0007X\u0080\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00cc\u0001\u0010\t\"\u0005\b\u00cd\u0001\u0010#R \u0010\u00ce\u0001\u001a\u00030\u00cf\u0001X\u0086\u000e\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00d0\u0001\u0010\u00d1\u0001\"\u0006\b\u00d2\u0001\u0010\u00d3\u0001R \u0010\u00d4\u0001\u001a\u00030\u00d5\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00d6\u0001\u0010\u00d7\u0001\"\u0006\b\u00d8\u0001\u0010\u00d9\u0001R \u0010\u00da\u0001\u001a\u00030\u00db\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00dc\u0001\u0010\u00dd\u0001\"\u0006\b\u00de\u0001\u0010\u00df\u0001R \u0010\u00e0\u0001\u001a\u00030\u00e1\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00e2\u0001\u0010\u00e3\u0001\"\u0006\b\u00e4\u0001\u0010\u00e5\u0001R \u0010\u00e6\u0001\u001a\u00030\u00e7\u0001X\u0086.\u00a2\u0006\u0012\n\u0000\u001a\u0006\b\u00e8\u0001\u0010\u00e9\u0001\"\u0006\b\u00ea\u0001\u0010\u00eb\u0001\u00a8\u0006\u00b9\u0002"}, d2 = {"Lcom/smartportable/demo2/activities/VisitorAuthActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Landroid/nfc/NfcAdapter$ReaderCallback;", "Landroid/view/View$OnClickListener;", "Lcom/smartportable/demo2/interfaces/ChangeCardStatus;", "()V", "ACTION_SCAN_ERROR", "", "getACTION_SCAN_ERROR", "()Ljava/lang/String;", "ACTION_SCAN_SUCCESS", "getACTION_SCAN_SUCCESS", "BARCODE_DATA", "getBARCODE_DATA", "INTENT_ACTION_SCAN", "KEY_PACKAGE", "ScanBack", "Lcom/zebra/adc/decoder/Barcode2DWithSoft$ScanCallback;", "getScanBack", "()Lcom/zebra/adc/decoder/Barcode2DWithSoft$ScanCallback;", "setScanBack", "(Lcom/zebra/adc/decoder/Barcode2DWithSoft$ScanCallback;)V", "TAG", "getTAG", "adapter", "Lcom/smartportable/demo2/activities/VisitorAuthActivity$ViewPagerAdapter;", "auth_type", "", "getAuth_type", "()I", "setAuth_type", "(I)V", "barCode", "getBarCode", "setBarCode", "(Ljava/lang/String;)V", "barcode2DWithSoft", "Lcom/zebra/adc/decoder/Barcode2DWithSoft;", "getBarcode2DWithSoft$app_debug", "()Lcom/zebra/adc/decoder/Barcode2DWithSoft;", "setBarcode2DWithSoft$app_debug", "(Lcom/zebra/adc/decoder/Barcode2DWithSoft;)V", "cardSt", "getCardSt", "setCardSt", "collapsingToolbarLayout", "Lcom/google/android/material/appbar/CollapsingToolbarLayout;", "getCollapsingToolbarLayout", "()Lcom/google/android/material/appbar/CollapsingToolbarLayout;", "setCollapsingToolbarLayout", "(Lcom/google/android/material/appbar/CollapsingToolbarLayout;)V", "csnNumber", "getCsnNumber", "setCsnNumber", "db", "Lcom/smartportable/demo2/db/DatabaseHandler;", "getDb", "()Lcom/smartportable/demo2/db/DatabaseHandler;", "setDb", "(Lcom/smartportable/demo2/db/DatabaseHandler;)V", "etIDNumber", "Landroid/widget/EditText;", "getEtIDNumber", "()Landroid/widget/EditText;", "setEtIDNumber", "(Landroid/widget/EditText;)V", "fabBarcodeAuth", "Lcom/google/android/material/floatingactionbutton/FloatingActionButton;", "getFabBarcodeAuth", "()Lcom/google/android/material/floatingactionbutton/FloatingActionButton;", "setFabBarcodeAuth", "(Lcom/google/android/material/floatingactionbutton/FloatingActionButton;)V", "fieldName", "getFieldName$app_debug", "setFieldName$app_debug", "fieldValues", "getFieldValues$app_debug", "setFieldValues$app_debug", "htab_appbar", "Lcom/google/android/material/appbar/AppBarLayout;", "getHtab_appbar", "()Lcom/google/android/material/appbar/AppBarLayout;", "setHtab_appbar", "(Lcom/google/android/material/appbar/AppBarLayout;)V", "htab_header", "Lde/hdodenhof/circleimageview/CircleImageView;", "getHtab_header", "()Lde/hdodenhof/circleimageview/CircleImageView;", "setHtab_header", "(Lde/hdodenhof/circleimageview/CircleImageView;)V", "htab_maincontent", "Landroidx/coordinatorlayout/widget/CoordinatorLayout;", "getHtab_maincontent", "()Landroidx/coordinatorlayout/widget/CoordinatorLayout;", "setHtab_maincontent", "(Landroidx/coordinatorlayout/widget/CoordinatorLayout;)V", "ivCard", "Landroid/widget/ImageView;", "getIvCard", "()Landroid/widget/ImageView;", "setIvCard", "(Landroid/widget/ImageView;)V", "ivDevice", "getIvDevice", "setIvDevice", "list", "Ljava/util/ArrayList;", "Lcom/smartportable/demo2/models/MifareSettingModel;", "getList$app_debug", "()Ljava/util/ArrayList;", "setList$app_debug", "(Ljava/util/ArrayList;)V", "mBSDLicenseCat", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "Landroidx/core/widget/NestedScrollView;", "getMBSDLicenseCat", "()Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "setMBSDLicenseCat", "(Lcom/google/android/material/bottomsheet/BottomSheetBehavior;)V", "mNfcAdapter", "Landroid/nfc/NfcAdapter;", "mOnSnackBarSetUrlClickListener", "getMOnSnackBarSetUrlClickListener", "()Landroid/view/View$OnClickListener;", "setMOnSnackBarSetUrlClickListener", "(Landroid/view/View$OnClickListener;)V", "mRVLicenseCat", "Landroidx/recyclerview/widget/RecyclerView;", "getMRVLicenseCat", "()Landroidx/recyclerview/widget/RecyclerView;", "setMRVLicenseCat", "(Landroidx/recyclerview/widget/RecyclerView;)V", "mifare_json_array", "Lorg/json/JSONArray;", "getMifare_json_array", "()Lorg/json/JSONArray;", "setMifare_json_array", "(Lorg/json/JSONArray;)V", "onVisitorAuthDataReceivedDA", "Lcom/smartportable/demo2/listeners/OnVisitorAuthDataReceived;", "getOnVisitorAuthDataReceivedDA", "()Lcom/smartportable/demo2/listeners/OnVisitorAuthDataReceived;", "setOnVisitorAuthDataReceivedDA", "(Lcom/smartportable/demo2/listeners/OnVisitorAuthDataReceived;)V", "onVisitorAuthDataReceivedLI", "getOnVisitorAuthDataReceivedLI", "setOnVisitorAuthDataReceivedLI", "onVisitorAuthDataReceivedPI", "getOnVisitorAuthDataReceivedPI", "setOnVisitorAuthDataReceivedPI", "rbAppNo", "Landroid/widget/RadioButton;", "getRbAppNo", "()Landroid/widget/RadioButton;", "setRbAppNo", "(Landroid/widget/RadioButton;)V", "rbCardNo", "getRbCardNo", "setRbCardNo", "rbSlNo", "getRbSlNo", "setRbSlNo", "rbStaffId", "getRbStaffId", "setRbStaffId", "rbUid", "getRbUid", "setRbUid", "rbVisCardno", "getRbVisCardno", "setRbVisCardno", "rbVisRegNo", "getRbVisRegNo", "setRbVisRegNo", "receiver", "Lcom/smartportable/demo2/activities/VisitorAuthActivity$HomeKeyEventBroadCastReceiver;", "getReceiver", "()Lcom/smartportable/demo2/activities/VisitorAuthActivity$HomeKeyEventBroadCastReceiver;", "setReceiver", "(Lcom/smartportable/demo2/activities/VisitorAuthActivity$HomeKeyEventBroadCastReceiver;)V", "rgStaff", "Landroid/widget/RadioGroup;", "getRgStaff", "()Landroid/widget/RadioGroup;", "setRgStaff", "(Landroid/widget/RadioGroup;)V", "rgVisitor", "getRgVisitor", "setRgVisitor", "rlAnim", "Landroid/widget/RelativeLayout;", "getRlAnim", "()Landroid/widget/RelativeLayout;", "setRlAnim", "(Landroid/widget/RelativeLayout;)V", "rlIDNumber", "getRlIDNumber", "setRlIDNumber", "rlLoadElements", "getRlLoadElements", "setRlLoadElements", "scanResult", "Landroid/content/BroadcastReceiver;", "seldata", "getSeldata$app_debug", "setSeldata$app_debug", "subCatData", "Lcom/smartportable/demo2/models/VisitorInfoModel;", "getSubCatData", "()Lcom/smartportable/demo2/models/VisitorInfoModel;", "setSubCatData", "(Lcom/smartportable/demo2/models/VisitorInfoModel;)V", "tabLayout", "Lcom/google/android/material/tabs/TabLayout;", "getTabLayout", "()Lcom/google/android/material/tabs/TabLayout;", "setTabLayout", "(Lcom/google/android/material/tabs/TabLayout;)V", "toolbar", "Landroidx/appcompat/widget/Toolbar;", "getToolbar", "()Landroidx/appcompat/widget/Toolbar;", "setToolbar", "(Landroidx/appcompat/widget/Toolbar;)V", "tvIDNumberError", "Landroid/widget/TextView;", "getTvIDNumberError", "()Landroid/widget/TextView;", "setTvIDNumberError", "(Landroid/widget/TextView;)V", "viewPager", "Landroidx/viewpager/widget/ViewPager;", "getViewPager", "()Landroidx/viewpager/widget/ViewPager;", "setViewPager", "(Landroidx/viewpager/widget/ViewPager;)V", "InitBarcodeReader", "", "ScanBarcode", "getLicenseDataAPI", "value", "hideAVILoaderIndicator", "hideAnimation1", "hideAnimation2", "hideSearchOption", "hideSoftKeypad", "initView", "mapMifareSetting", "offsetChangeListener", "Lcom/google/android/material/appbar/AppBarLayout$OnOffsetChangedListener;", "onCardHolderImageIntoPellete", "url", "onChangeCardStatus", "mContext", "Landroid/content/Context;", "cardStCode", "onClick", "v", "Landroid/view/View;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "", "menu", "Landroid/view/Menu;", "onDestroy", "onKeyDown", "keyCode", "event", "Landroid/view/KeyEvent;", "onKeyUp", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onPause", "onPrepareOptionsMenu", "onResume", "onStart", "onStop", "onTagDiscovered", "tag", "Landroid/nfc/Tag;", "openBottomDialog", "hide_or_show", "readCardData", "mifareClassic", "Landroid/nfc/tech/MifareClassic;", "readTextFile", "inputStream", "Ljava/io/InputStream;", "registerReceiver", "setCardHolderImageIntoPellete", "setCardStatus", "setVisitorAuthDataReceivedListenerDA", "listenerDA", "setVisitorAuthDataReceivedListenerLI", "listenerLI", "setVisitorAuthDataReceivedListenerPI", "listenerPI", "setupCollaspView", "setupViewPager", "showAVILoaderIndicator", "showAnimation1", "showAnimation2", "showSearchOption", "showSnackBar", "msg", "startScan", "zt", "zt$app_debug", "HomeKeyEventBroadCastReceiver", "ViewPagerAdapter", "app_debug"})
public final class VisitorAuthActivity extends androidx.appcompat.app.AppCompatActivity implements android.nfc.NfcAdapter.ReaderCallback, android.view.View.OnClickListener, com.smartportable.demo2.interfaces.ChangeCardStatus {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String TAG = null;
    @org.jetbrains.annotations.NotNull()
    public com.google.android.material.appbar.CollapsingToolbarLayout collapsingToolbarLayout;
    @org.jetbrains.annotations.NotNull()
    public androidx.appcompat.widget.Toolbar toolbar;
    @org.jetbrains.annotations.NotNull()
    public com.google.android.material.tabs.TabLayout tabLayout;
    @org.jetbrains.annotations.NotNull()
    public androidx.coordinatorlayout.widget.CoordinatorLayout htab_maincontent;
    @org.jetbrains.annotations.NotNull()
    public com.google.android.material.appbar.AppBarLayout htab_appbar;
    @org.jetbrains.annotations.NotNull()
    public de.hdodenhof.circleimageview.CircleImageView htab_header;
    @org.jetbrains.annotations.NotNull()
    public android.widget.RelativeLayout rlLoadElements;
    @org.jetbrains.annotations.NotNull()
    public android.widget.RelativeLayout rlAnim;
    @org.jetbrains.annotations.NotNull()
    public android.widget.ImageView ivDevice;
    @org.jetbrains.annotations.NotNull()
    public android.widget.ImageView ivCard;
    @org.jetbrains.annotations.NotNull()
    public com.google.android.material.floatingactionbutton.FloatingActionButton fabBarcodeAuth;
    @org.jetbrains.annotations.NotNull()
    public android.widget.RelativeLayout rlIDNumber;
    @org.jetbrains.annotations.NotNull()
    public android.widget.EditText etIDNumber;
    @org.jetbrains.annotations.NotNull()
    public android.widget.TextView tvIDNumberError;
    @org.jetbrains.annotations.NotNull()
    public androidx.viewpager.widget.ViewPager viewPager;
    private com.smartportable.demo2.activities.VisitorAuthActivity.ViewPagerAdapter adapter;
    private android.nfc.NfcAdapter mNfcAdapter;
    @org.jetbrains.annotations.NotNull()
    public java.lang.String csnNumber;
    @org.jetbrains.annotations.NotNull()
    public android.widget.RadioGroup rgStaff;
    @org.jetbrains.annotations.NotNull()
    public android.widget.RadioButton rbAppNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.RadioButton rbSlNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.RadioButton rbUid;
    @org.jetbrains.annotations.NotNull()
    public android.widget.RadioButton rbStaffId;
    @org.jetbrains.annotations.NotNull()
    public android.widget.RadioButton rbCardNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.RadioGroup rgVisitor;
    @org.jetbrains.annotations.NotNull()
    public android.widget.RadioButton rbVisRegNo;
    @org.jetbrains.annotations.NotNull()
    public android.widget.RadioButton rbVisCardno;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String fieldName;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String fieldValues;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.smartportable.demo2.models.MifareSettingModel> list;
    @org.jetbrains.annotations.NotNull()
    public org.json.JSONArray mifare_json_array;
    @org.jetbrains.annotations.NotNull()
    public com.smartportable.demo2.listeners.OnVisitorAuthDataReceived onVisitorAuthDataReceivedPI;
    @org.jetbrains.annotations.NotNull()
    public com.smartportable.demo2.listeners.OnVisitorAuthDataReceived onVisitorAuthDataReceivedLI;
    @org.jetbrains.annotations.NotNull()
    public com.smartportable.demo2.listeners.OnVisitorAuthDataReceived onVisitorAuthDataReceivedDA;
    @org.jetbrains.annotations.NotNull()
    public androidx.recyclerview.widget.RecyclerView mRVLicenseCat;
    @org.jetbrains.annotations.NotNull()
    public com.google.android.material.bottomsheet.BottomSheetBehavior<androidx.core.widget.NestedScrollView> mBSDLicenseCat;
    private int auth_type;
    private int cardSt;
    @org.jetbrains.annotations.NotNull()
    private com.smartportable.demo2.models.VisitorInfoModel subCatData;
    private final java.lang.String INTENT_ACTION_SCAN = "fr.coppernic.intent.action.SCAN";
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String ACTION_SCAN_SUCCESS = "fr.coppernic.intent.scansuccess";
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String ACTION_SCAN_ERROR = "fr.coppernic.intent.scanfailed";
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String BARCODE_DATA = "BarcodeData";
    private final java.lang.String KEY_PACKAGE = "package";
    @org.jetbrains.annotations.NotNull()
    public com.smartportable.demo2.db.DatabaseHandler db;
    @org.jetbrains.annotations.Nullable()
    private com.zebra.adc.decoder.Barcode2DWithSoft barcode2DWithSoft;
    @org.jetbrains.annotations.NotNull()
    public com.smartportable.demo2.activities.VisitorAuthActivity.HomeKeyEventBroadCastReceiver receiver;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String barCode;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String seldata;
    @org.jetbrains.annotations.NotNull()
    private com.zebra.adc.decoder.Barcode2DWithSoft.ScanCallback ScanBack;
    @org.jetbrains.annotations.NotNull()
    private android.view.View.OnClickListener mOnSnackBarSetUrlClickListener;
    private final android.content.BroadcastReceiver scanResult = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTAG() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.google.android.material.appbar.CollapsingToolbarLayout getCollapsingToolbarLayout() {
        return null;
    }
    
    public final void setCollapsingToolbarLayout(@org.jetbrains.annotations.NotNull()
    com.google.android.material.appbar.CollapsingToolbarLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.appcompat.widget.Toolbar getToolbar() {
        return null;
    }
    
    public final void setToolbar(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.widget.Toolbar p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.google.android.material.tabs.TabLayout getTabLayout() {
        return null;
    }
    
    public final void setTabLayout(@org.jetbrains.annotations.NotNull()
    com.google.android.material.tabs.TabLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.coordinatorlayout.widget.CoordinatorLayout getHtab_maincontent() {
        return null;
    }
    
    public final void setHtab_maincontent(@org.jetbrains.annotations.NotNull()
    androidx.coordinatorlayout.widget.CoordinatorLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.google.android.material.appbar.AppBarLayout getHtab_appbar() {
        return null;
    }
    
    public final void setHtab_appbar(@org.jetbrains.annotations.NotNull()
    com.google.android.material.appbar.AppBarLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final de.hdodenhof.circleimageview.CircleImageView getHtab_header() {
        return null;
    }
    
    public final void setHtab_header(@org.jetbrains.annotations.NotNull()
    de.hdodenhof.circleimageview.CircleImageView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.RelativeLayout getRlLoadElements() {
        return null;
    }
    
    public final void setRlLoadElements(@org.jetbrains.annotations.NotNull()
    android.widget.RelativeLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.RelativeLayout getRlAnim() {
        return null;
    }
    
    public final void setRlAnim(@org.jetbrains.annotations.NotNull()
    android.widget.RelativeLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.ImageView getIvDevice() {
        return null;
    }
    
    public final void setIvDevice(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.ImageView getIvCard() {
        return null;
    }
    
    public final void setIvCard(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.google.android.material.floatingactionbutton.FloatingActionButton getFabBarcodeAuth() {
        return null;
    }
    
    public final void setFabBarcodeAuth(@org.jetbrains.annotations.NotNull()
    com.google.android.material.floatingactionbutton.FloatingActionButton p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.RelativeLayout getRlIDNumber() {
        return null;
    }
    
    public final void setRlIDNumber(@org.jetbrains.annotations.NotNull()
    android.widget.RelativeLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.EditText getEtIDNumber() {
        return null;
    }
    
    public final void setEtIDNumber(@org.jetbrains.annotations.NotNull()
    android.widget.EditText p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.TextView getTvIDNumberError() {
        return null;
    }
    
    public final void setTvIDNumberError(@org.jetbrains.annotations.NotNull()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.viewpager.widget.ViewPager getViewPager() {
        return null;
    }
    
    public final void setViewPager(@org.jetbrains.annotations.NotNull()
    androidx.viewpager.widget.ViewPager p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCsnNumber() {
        return null;
    }
    
    public final void setCsnNumber(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.RadioGroup getRgStaff() {
        return null;
    }
    
    public final void setRgStaff(@org.jetbrains.annotations.NotNull()
    android.widget.RadioGroup p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.RadioButton getRbAppNo() {
        return null;
    }
    
    public final void setRbAppNo(@org.jetbrains.annotations.NotNull()
    android.widget.RadioButton p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.RadioButton getRbSlNo() {
        return null;
    }
    
    public final void setRbSlNo(@org.jetbrains.annotations.NotNull()
    android.widget.RadioButton p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.RadioButton getRbUid() {
        return null;
    }
    
    public final void setRbUid(@org.jetbrains.annotations.NotNull()
    android.widget.RadioButton p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.RadioButton getRbStaffId() {
        return null;
    }
    
    public final void setRbStaffId(@org.jetbrains.annotations.NotNull()
    android.widget.RadioButton p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.RadioButton getRbCardNo() {
        return null;
    }
    
    public final void setRbCardNo(@org.jetbrains.annotations.NotNull()
    android.widget.RadioButton p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.RadioGroup getRgVisitor() {
        return null;
    }
    
    public final void setRgVisitor(@org.jetbrains.annotations.NotNull()
    android.widget.RadioGroup p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.RadioButton getRbVisRegNo() {
        return null;
    }
    
    public final void setRbVisRegNo(@org.jetbrains.annotations.NotNull()
    android.widget.RadioButton p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.RadioButton getRbVisCardno() {
        return null;
    }
    
    public final void setRbVisCardno(@org.jetbrains.annotations.NotNull()
    android.widget.RadioButton p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFieldName$app_debug() {
        return null;
    }
    
    public final void setFieldName$app_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFieldValues$app_debug() {
        return null;
    }
    
    public final void setFieldValues$app_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.smartportable.demo2.models.MifareSettingModel> getList$app_debug() {
        return null;
    }
    
    public final void setList$app_debug(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.MifareSettingModel> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final org.json.JSONArray getMifare_json_array() {
        return null;
    }
    
    public final void setMifare_json_array(@org.jetbrains.annotations.NotNull()
    org.json.JSONArray p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.listeners.OnVisitorAuthDataReceived getOnVisitorAuthDataReceivedPI() {
        return null;
    }
    
    public final void setOnVisitorAuthDataReceivedPI(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.listeners.OnVisitorAuthDataReceived p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.listeners.OnVisitorAuthDataReceived getOnVisitorAuthDataReceivedLI() {
        return null;
    }
    
    public final void setOnVisitorAuthDataReceivedLI(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.listeners.OnVisitorAuthDataReceived p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.listeners.OnVisitorAuthDataReceived getOnVisitorAuthDataReceivedDA() {
        return null;
    }
    
    public final void setOnVisitorAuthDataReceivedDA(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.listeners.OnVisitorAuthDataReceived p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.recyclerview.widget.RecyclerView getMRVLicenseCat() {
        return null;
    }
    
    public final void setMRVLicenseCat(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.google.android.material.bottomsheet.BottomSheetBehavior<androidx.core.widget.NestedScrollView> getMBSDLicenseCat() {
        return null;
    }
    
    public final void setMBSDLicenseCat(@org.jetbrains.annotations.NotNull()
    com.google.android.material.bottomsheet.BottomSheetBehavior<androidx.core.widget.NestedScrollView> p0) {
    }
    
    public final int getAuth_type() {
        return 0;
    }
    
    public final void setAuth_type(int p0) {
    }
    
    public final int getCardSt() {
        return 0;
    }
    
    public final void setCardSt(int p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.models.VisitorInfoModel getSubCatData() {
        return null;
    }
    
    public final void setSubCatData(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.models.VisitorInfoModel p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getACTION_SCAN_SUCCESS() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getACTION_SCAN_ERROR() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getBARCODE_DATA() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.db.DatabaseHandler getDb() {
        return null;
    }
    
    public final void setDb(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.db.DatabaseHandler p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.zebra.adc.decoder.Barcode2DWithSoft getBarcode2DWithSoft$app_debug() {
        return null;
    }
    
    public final void setBarcode2DWithSoft$app_debug(@org.jetbrains.annotations.Nullable()
    com.zebra.adc.decoder.Barcode2DWithSoft p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.activities.VisitorAuthActivity.HomeKeyEventBroadCastReceiver getReceiver() {
        return null;
    }
    
    public final void setReceiver(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.activities.VisitorAuthActivity.HomeKeyEventBroadCastReceiver p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getBarCode() {
        return null;
    }
    
    public final void setBarCode(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSeldata$app_debug() {
        return null;
    }
    
    public final void setSeldata$app_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.zebra.adc.decoder.Barcode2DWithSoft.ScanCallback getScanBack() {
        return null;
    }
    
    public final void setScanBack(@org.jetbrains.annotations.NotNull()
    com.zebra.adc.decoder.Barcode2DWithSoft.ScanCallback p0) {
    }
    
    public final void zt$app_debug() {
    }
    
    private final void ScanBarcode() {
    }
    
    @java.lang.Override()
    public boolean onKeyDown(int keyCode, @org.jetbrains.annotations.NotNull()
    android.view.KeyEvent event) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onKeyUp(int keyCode, @org.jetbrains.annotations.NotNull()
    android.view.KeyEvent event) {
        return false;
    }
    
    public final void InitBarcodeReader() {
    }
    
    private final void initView() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View.OnClickListener getMOnSnackBarSetUrlClickListener() {
        return null;
    }
    
    public final void setMOnSnackBarSetUrlClickListener(@org.jetbrains.annotations.NotNull()
    android.view.View.OnClickListener p0) {
    }
    
    private final void openBottomDialog(boolean hide_or_show) {
    }
    
    private final void mapMifareSetting() {
    }
    
    public final void setVisitorAuthDataReceivedListenerPI(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.listeners.OnVisitorAuthDataReceived listenerPI) {
    }
    
    public final void setVisitorAuthDataReceivedListenerLI(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.listeners.OnVisitorAuthDataReceived listenerLI) {
    }
    
    public final void setVisitorAuthDataReceivedListenerDA(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.listeners.OnVisitorAuthDataReceived listenerDA) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String readTextFile(@org.jetbrains.annotations.NotNull()
    java.io.InputStream inputStream) {
        return null;
    }
    
    private final void setupCollaspView(androidx.viewpager.widget.ViewPager viewPager) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener offsetChangeListener(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.widget.Toolbar toolbar) {
        return null;
    }
    
    private final void setupViewPager(androidx.viewpager.widget.ViewPager viewPager) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    protected void onPause() {
    }
    
    @java.lang.Override()
    public void onStart() {
    }
    
    @java.lang.Override()
    public void onStop() {
    }
    
    /**
     * * Triggers a barcode scan
     */
    private final void startScan() {
    }
    
    private final void registerReceiver() {
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.Nullable()
    android.view.Menu menu) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onPrepareOptionsMenu(@org.jetbrains.annotations.Nullable()
    android.view.Menu menu) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.Nullable()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    @java.lang.Override()
    public void onTagDiscovered(@org.jetbrains.annotations.Nullable()
    android.nfc.Tag tag) {
    }
    
    public final void showAVILoaderIndicator() {
    }
    
    public final void hideAVILoaderIndicator() {
    }
    
    public final void showAnimation1() {
    }
    
    public final void showAnimation2() {
    }
    
    public final void hideAnimation1() {
    }
    
    public final void hideAnimation2() {
    }
    
    public final void showSearchOption() {
    }
    
    public final void hideSearchOption() {
    }
    
    public final void readCardData(@org.jetbrains.annotations.NotNull()
    android.nfc.tech.MifareClassic mifareClassic) {
    }
    
    public final void getLicenseDataAPI(@org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    public final void hideSoftKeypad() {
    }
    
    public final void showSnackBar(@org.jetbrains.annotations.Nullable()
    java.lang.String msg) {
    }
    
    public final void setCardHolderImageIntoPellete(@org.jetbrains.annotations.NotNull()
    java.lang.String url) {
    }
    
    public final void setCardStatus(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, int cardStCode) {
    }
    
    @java.lang.Override()
    public void onChangeCardStatus(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, int cardStCode) {
    }
    
    @java.lang.Override()
    public void onCardHolderImageIntoPellete(@org.jetbrains.annotations.Nullable()
    java.lang.String url) {
    }
    
    public VisitorAuthActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016\u00a8\u0006\t"}, d2 = {"Lcom/smartportable/demo2/activities/VisitorAuthActivity$HomeKeyEventBroadCastReceiver;", "Landroid/content/BroadcastReceiver;", "(Lcom/smartportable/demo2/activities/VisitorAuthActivity;)V", "onReceive", "", "context", "Landroid/content/Context;", "intent", "Landroid/content/Intent;", "app_debug"})
    public final class HomeKeyEventBroadCastReceiver extends android.content.BroadcastReceiver {
        
        @java.lang.Override()
        public void onReceive(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        android.content.Intent intent) {
        }
        
        public HomeKeyEventBroadCastReceiver() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\r\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\tJ\b\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\u0010\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u000fH\u0016J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0011\u001a\u00020\u000fH\u0016R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/smartportable/demo2/activities/VisitorAuthActivity$ViewPagerAdapter;", "Landroidx/fragment/app/FragmentPagerAdapter;", "manager", "Landroidx/fragment/app/FragmentManager;", "(Landroidx/fragment/app/FragmentManager;)V", "mFragmentList", "Ljava/util/ArrayList;", "Landroidx/fragment/app/Fragment;", "mFragmentTitleList", "", "addFrag", "", "fragment", "title", "getCount", "", "getItem", "position", "getPageTitle", "", "app_debug"})
    static final class ViewPagerAdapter extends androidx.fragment.app.FragmentPagerAdapter {
        private final java.util.ArrayList<androidx.fragment.app.Fragment> mFragmentList = null;
        private final java.util.ArrayList<java.lang.String> mFragmentTitleList = null;
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public androidx.fragment.app.Fragment getItem(int position) {
            return null;
        }
        
        @java.lang.Override()
        public int getCount() {
            return 0;
        }
        
        public final void addFrag(@org.jetbrains.annotations.NotNull()
        androidx.fragment.app.Fragment fragment, @org.jetbrains.annotations.NotNull()
        java.lang.String title) {
        }
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public java.lang.CharSequence getPageTitle(int position) {
            return null;
        }
        
        public ViewPagerAdapter(@org.jetbrains.annotations.NotNull()
        androidx.fragment.app.FragmentManager manager) {
            super(null);
        }
    }
}