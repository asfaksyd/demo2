package com.smartportable.demo2.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u009c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010P\u001a\u00020Q2\u0006\u0010R\u001a\u00020\fJ\u0006\u0010S\u001a\u00020QJ\u0006\u0010T\u001a\u00020QJ\b\u0010U\u001a\u00020QH\u0016J\u0012\u0010V\u001a\u00020Q2\b\u0010W\u001a\u0004\u0018\u00010XH\u0014J\b\u0010Y\u001a\u00020QH\u0014J\u0010\u0010Z\u001a\u00020[2\u0006\u0010\\\u001a\u00020]H\u0016J\u0010\u0010^\u001a\u00020Q2\u0006\u0010_\u001a\u00020`H\u0016J\u000e\u0010a\u001a\u00020Q2\u0006\u0010\u0011\u001a\u00020\u0012R\"\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0019\u001a\u0004\u0018\u00010\u001aX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\u00020 X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R\u001c\u0010%\u001a\u0004\u0018\u00010&X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\'\u0010(\"\u0004\b)\u0010*R\u001c\u0010+\u001a\u0004\u0018\u00010,X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b-\u0010.\"\u0004\b/\u00100R\u001c\u00101\u001a\u0004\u0018\u000102X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b3\u00104\"\u0004\b5\u00106R \u00107\u001a\b\u0012\u0004\u0012\u00020908X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u001c\u0010>\u001a\u0004\u0018\u00010?X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b@\u0010A\"\u0004\bB\u0010CR\u001a\u0010D\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bE\u0010\u000e\"\u0004\bF\u0010\u0010R\u001c\u0010G\u001a\u0004\u0018\u00010HX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bI\u0010J\"\u0004\bK\u0010LR\u001c\u0010M\u001a\u0004\u0018\u00010HX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bN\u0010J\"\u0004\bO\u0010L\u00a8\u0006b"}, d2 = {"Lcom/smartportable/demo2/activities/SyncSessionsActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout$OnRefreshListener;", "()V", "apiRetrofit", "Lretrofit2/Call;", "Lcom/smartportable/demo2/models/SessionsInfoModel;", "getApiRetrofit", "()Lretrofit2/Call;", "setApiRetrofit", "(Lretrofit2/Call;)V", "currentPageIndex", "", "getCurrentPageIndex", "()I", "setCurrentPageIndex", "(I)V", "db", "Lcom/smartportable/demo2/db/DatabaseHandler;", "getDb", "()Lcom/smartportable/demo2/db/DatabaseHandler;", "setDb", "(Lcom/smartportable/demo2/db/DatabaseHandler;)V", "hStopRequest", "Landroid/os/Handler;", "llSessionRecord", "Landroid/widget/LinearLayout;", "getLlSessionRecord", "()Landroid/widget/LinearLayout;", "setLlSessionRecord", "(Landroid/widget/LinearLayout;)V", "mOnSnackBarSetUrlClickListener", "Landroid/view/View$OnClickListener;", "getMOnSnackBarSetUrlClickListener", "()Landroid/view/View$OnClickListener;", "setMOnSnackBarSetUrlClickListener", "(Landroid/view/View$OnClickListener;)V", "rlSession", "Landroid/widget/RelativeLayout;", "getRlSession", "()Landroid/widget/RelativeLayout;", "setRlSession", "(Landroid/widget/RelativeLayout;)V", "rvSessionList", "Landroidx/recyclerview/widget/RecyclerView;", "getRvSessionList", "()Landroidx/recyclerview/widget/RecyclerView;", "setRvSessionList", "(Landroidx/recyclerview/widget/RecyclerView;)V", "sessionsAdapter", "Lcom/smartportable/demo2/adapters/SessionListAdapter;", "getSessionsAdapter", "()Lcom/smartportable/demo2/adapters/SessionListAdapter;", "setSessionsAdapter", "(Lcom/smartportable/demo2/adapters/SessionListAdapter;)V", "sessionsList", "Ljava/util/ArrayList;", "Lcom/smartportable/demo2/models/SessionsInfoModel$DataBean;", "getSessionsList", "()Ljava/util/ArrayList;", "setSessionsList", "(Ljava/util/ArrayList;)V", "srlSessions", "Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout;", "getSrlSessions", "()Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout;", "setSrlSessions", "(Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout;)V", "totalPageCount", "getTotalPageCount", "setTotalPageCount", "tvSessionWarningMsg", "Landroid/widget/TextView;", "getTvSessionWarningMsg", "()Landroid/widget/TextView;", "setTvSessionWarningMsg", "(Landroid/widget/TextView;)V", "tvSyncSessionRecord", "getTvSyncSessionRecord", "setTvSyncSessionRecord", "SyncSessionsAPI", "", "sessionId", "init", "onBackPressOperation", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "onRefresh", "direction", "Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayoutDirection;", "showSessionsFromDB", "app_debug"})
public final class SyncSessionsActivity extends androidx.appcompat.app.AppCompatActivity implements com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout.OnRefreshListener {
    private android.os.Handler hStopRequest;
    @org.jetbrains.annotations.Nullable()
    private com.smartportable.demo2.db.DatabaseHandler db;
    @org.jetbrains.annotations.Nullable()
    private android.widget.RelativeLayout rlSession;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.smartportable.demo2.models.SessionsInfoModel.DataBean> sessionsList;
    @org.jetbrains.annotations.Nullable()
    private android.widget.LinearLayout llSessionRecord;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tvSyncSessionRecord;
    @org.jetbrains.annotations.Nullable()
    private com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout srlSessions;
    @org.jetbrains.annotations.Nullable()
    private androidx.recyclerview.widget.RecyclerView rvSessionList;
    @org.jetbrains.annotations.Nullable()
    private com.smartportable.demo2.adapters.SessionListAdapter sessionsAdapter;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tvSessionWarningMsg;
    private int currentPageIndex;
    private int totalPageCount;
    @org.jetbrains.annotations.NotNull()
    private android.view.View.OnClickListener mOnSnackBarSetUrlClickListener;
    @org.jetbrains.annotations.Nullable()
    private retrofit2.Call<com.smartportable.demo2.models.SessionsInfoModel> apiRetrofit;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.smartportable.demo2.db.DatabaseHandler getDb() {
        return null;
    }
    
    public final void setDb(@org.jetbrains.annotations.Nullable()
    com.smartportable.demo2.db.DatabaseHandler p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.RelativeLayout getRlSession() {
        return null;
    }
    
    public final void setRlSession(@org.jetbrains.annotations.Nullable()
    android.widget.RelativeLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.smartportable.demo2.models.SessionsInfoModel.DataBean> getSessionsList() {
        return null;
    }
    
    public final void setSessionsList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.SessionsInfoModel.DataBean> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.LinearLayout getLlSessionRecord() {
        return null;
    }
    
    public final void setLlSessionRecord(@org.jetbrains.annotations.Nullable()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTvSyncSessionRecord() {
        return null;
    }
    
    public final void setTvSyncSessionRecord(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout getSrlSessions() {
        return null;
    }
    
    public final void setSrlSessions(@org.jetbrains.annotations.Nullable()
    com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.recyclerview.widget.RecyclerView getRvSessionList() {
        return null;
    }
    
    public final void setRvSessionList(@org.jetbrains.annotations.Nullable()
    androidx.recyclerview.widget.RecyclerView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.smartportable.demo2.adapters.SessionListAdapter getSessionsAdapter() {
        return null;
    }
    
    public final void setSessionsAdapter(@org.jetbrains.annotations.Nullable()
    com.smartportable.demo2.adapters.SessionListAdapter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTvSessionWarningMsg() {
        return null;
    }
    
    public final void setTvSessionWarningMsg(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    public final int getCurrentPageIndex() {
        return 0;
    }
    
    public final void setCurrentPageIndex(int p0) {
    }
    
    public final int getTotalPageCount() {
        return 0;
    }
    
    public final void setTotalPageCount(int p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void init() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View.OnClickListener getMOnSnackBarSetUrlClickListener() {
        return null;
    }
    
    public final void setMOnSnackBarSetUrlClickListener(@org.jetbrains.annotations.NotNull()
    android.view.View.OnClickListener p0) {
    }
    
    @java.lang.Override()
    public void onRefresh(@org.jetbrains.annotations.NotNull()
    com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection direction) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final retrofit2.Call<com.smartportable.demo2.models.SessionsInfoModel> getApiRetrofit() {
        return null;
    }
    
    public final void setApiRetrofit(@org.jetbrains.annotations.Nullable()
    retrofit2.Call<com.smartportable.demo2.models.SessionsInfoModel> p0) {
    }
    
    public final void SyncSessionsAPI(int sessionId) {
    }
    
    public final void showSessionsFromDB(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.db.DatabaseHandler db) {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public final void onBackPressOperation() {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    public SyncSessionsActivity() {
        super();
    }
}