package com.smartportable.demo2.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\u0018B\u0005\u00a2\u0006\u0002\u0010\u0002R:\u0010\u0003\u001a\u001e\u0012\b\u0012\u00060\u0005R\u00020\u0000\u0018\u00010\u0004j\u000e\u0012\b\u0012\u00060\u0005R\u00020\u0000\u0018\u0001`\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR \u0010\u000b\u001a\u0004\u0018\u00010\f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\"\u0010\u0011\u001a\u0004\u0018\u00010\u00128\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0017\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016\u00a8\u0006\u0019"}, d2 = {"Lcom/smartportable/demo2/models/MainGateAccLogsModel;", "", "()V", "data", "Ljava/util/ArrayList;", "Lcom/smartportable/demo2/models/MainGateAccLogsModel$Data;", "Lkotlin/collections/ArrayList;", "getData", "()Ljava/util/ArrayList;", "setData", "(Ljava/util/ArrayList;)V", "message", "", "getMessage", "()Ljava/lang/String;", "setMessage", "(Ljava/lang/String;)V", "status", "", "getStatus", "()Ljava/lang/Integer;", "setStatus", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "Data", "app_debug"})
public final class MainGateAccLogsModel {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "Status")
    private java.lang.Integer status;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "Message")
    private java.lang.String message;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "Data")
    private java.util.ArrayList<com.smartportable.demo2.models.MainGateAccLogsModel.Data> data;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getStatus() {
        return null;
    }
    
    public final void setStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.ArrayList<com.smartportable.demo2.models.MainGateAccLogsModel.Data> getData() {
        return null;
    }
    
    public final void setData(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<com.smartportable.demo2.models.MainGateAccLogsModel.Data> p0) {
    }
    
    public MainGateAccLogsModel() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\"\u0010\t\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000e\u001a\u0004\b\t\u0010\u000b\"\u0004\b\f\u0010\rR \u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\bR \u0010\u0012\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0006\"\u0004\b\u0014\u0010\b\u00a8\u0006\u0015"}, d2 = {"Lcom/smartportable/demo2/models/MainGateAccLogsModel$Data;", "", "(Lcom/smartportable/demo2/models/MainGateAccLogsModel;)V", "agId", "", "getAgId", "()Ljava/lang/String;", "setAgId", "(Ljava/lang/String;)V", "isSync", "", "()Ljava/lang/Integer;", "setSync", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "punchDatetime", "getPunchDatetime", "setPunchDatetime", "studentId", "getStudentId", "setStudentId", "app_debug"})
    public final class Data {
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Student_Id")
        private java.lang.String studentId;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Is_Sync")
        private java.lang.Integer isSync;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Punch_Datetime")
        private java.lang.String punchDatetime;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "AG_Id")
        private java.lang.String agId;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getStudentId() {
            return null;
        }
        
        public final void setStudentId(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer isSync() {
            return null;
        }
        
        public final void setSync(@org.jetbrains.annotations.Nullable()
        java.lang.Integer p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPunchDatetime() {
            return null;
        }
        
        public final void setPunchDatetime(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getAgId() {
            return null;
        }
        
        public final void setAgId(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        public Data() {
            super();
        }
    }
}