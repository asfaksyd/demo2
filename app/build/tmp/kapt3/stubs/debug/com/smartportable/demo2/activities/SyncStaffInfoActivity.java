package com.smartportable.demo2.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00b6\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010T\u001a\u00020U2\u0006\u0010V\u001a\u00020\rJ\u0006\u0010W\u001a\u00020UJ\u0006\u0010X\u001a\u00020UJ\b\u0010Y\u001a\u00020UH\u0016J\u0012\u0010Z\u001a\u00020U2\b\u0010[\u001a\u0004\u0018\u00010\\H\u0014J\u0010\u0010]\u001a\u00020^2\u0006\u0010_\u001a\u00020`H\u0016J\b\u0010a\u001a\u00020UH\u0014J\u0010\u0010b\u001a\u00020^2\u0006\u0010c\u001a\u00020dH\u0016J\u0012\u0010e\u001a\u00020^2\b\u0010f\u001a\u0004\u0018\u00010gH\u0016J\u0012\u0010h\u001a\u00020^2\b\u0010i\u001a\u0004\u0018\u00010gH\u0016J\u0012\u0010j\u001a\u00020U2\b\u0010k\u001a\u0004\u0018\u00010lH\u0016J\u000e\u0010m\u001a\u00020U2\u0006\u0010\u0012\u001a\u00020\u0013R\"\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001a\u0010\f\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001a\u0010\u0018\u001a\u00020\u0019X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010 \u001a\u0004\u0018\u00010!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R\u001a\u0010&\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u001c\u0010,\u001a\u0004\u0018\u00010-X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u001c\u00102\u001a\u0004\u0018\u000103X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u00105\"\u0004\b6\u00107R\u001c\u00108\u001a\u0004\u0018\u000109X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u001c\u0010>\u001a\u0004\u0018\u00010?X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b@\u0010A\"\u0004\bB\u0010CR\"\u0010D\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010F0EX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bG\u0010H\"\u0004\bI\u0010JR\u001a\u0010K\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bL\u0010\u000f\"\u0004\bM\u0010\u0011R\u001c\u0010N\u001a\u0004\u0018\u00010OX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bP\u0010Q\"\u0004\bR\u0010S\u00a8\u0006n"}, d2 = {"Lcom/smartportable/demo2/activities/SyncStaffInfoActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout$OnRefreshListener;", "Landroid/widget/SearchView$OnQueryTextListener;", "()V", "apiRetrofit", "Lretrofit2/Call;", "Lcom/smartportable/demo2/models/StaffInfoModel;", "getApiRetrofit", "()Lretrofit2/Call;", "setApiRetrofit", "(Lretrofit2/Call;)V", "currentPageIndex", "", "getCurrentPageIndex", "()I", "setCurrentPageIndex", "(I)V", "db", "Lcom/smartportable/demo2/db/DatabaseHandler;", "getDb", "()Lcom/smartportable/demo2/db/DatabaseHandler;", "setDb", "(Lcom/smartportable/demo2/db/DatabaseHandler;)V", "fm", "Landroidx/fragment/app/FragmentManager;", "getFm", "()Landroidx/fragment/app/FragmentManager;", "setFm", "(Landroidx/fragment/app/FragmentManager;)V", "hStopRequest", "Landroid/os/Handler;", "llStaffRecord", "Landroid/widget/LinearLayout;", "getLlStaffRecord", "()Landroid/widget/LinearLayout;", "setLlStaffRecord", "(Landroid/widget/LinearLayout;)V", "mOnSnackBarSetUrlClickListener", "Landroid/view/View$OnClickListener;", "getMOnSnackBarSetUrlClickListener", "()Landroid/view/View$OnClickListener;", "setMOnSnackBarSetUrlClickListener", "(Landroid/view/View$OnClickListener;)V", "rvStaffList", "Landroidx/recyclerview/widget/RecyclerView;", "getRvStaffList", "()Landroidx/recyclerview/widget/RecyclerView;", "setRvStaffList", "(Landroidx/recyclerview/widget/RecyclerView;)V", "searchView", "Landroid/widget/SearchView;", "getSearchView", "()Landroid/widget/SearchView;", "setSearchView", "(Landroid/widget/SearchView;)V", "srlStaffList", "Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout;", "getSrlStaffList", "()Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout;", "setSrlStaffList", "(Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout;)V", "staffInfoAdapter", "Lcom/smartportable/demo2/adapters/StaffInfoAdapter;", "getStaffInfoAdapter", "()Lcom/smartportable/demo2/adapters/StaffInfoAdapter;", "setStaffInfoAdapter", "(Lcom/smartportable/demo2/adapters/StaffInfoAdapter;)V", "staffList", "Ljava/util/ArrayList;", "Lcom/smartportable/demo2/models/StaffInfoModel$DataBean;", "getStaffList", "()Ljava/util/ArrayList;", "setStaffList", "(Ljava/util/ArrayList;)V", "totalPageCount", "getTotalPageCount", "setTotalPageCount", "tvSyncStaffRecord", "Landroid/widget/TextView;", "getTvSyncStaffRecord", "()Landroid/widget/TextView;", "setTvSyncStaffRecord", "(Landroid/widget/TextView;)V", "SyncStaffInfoAPI", "", "staffId", "init", "onBackPressOperation", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "", "menu", "Landroid/view/Menu;", "onDestroy", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onQueryTextChange", "p0", "", "onQueryTextSubmit", "searchKeyword", "onRefresh", "direction", "Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayoutDirection;", "showStaffInfoFromDB", "app_debug"})
public final class SyncStaffInfoActivity extends androidx.appcompat.app.AppCompatActivity implements com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout.OnRefreshListener, android.widget.SearchView.OnQueryTextListener {
    private android.os.Handler hStopRequest;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.smartportable.demo2.models.StaffInfoModel.DataBean> staffList;
    @org.jetbrains.annotations.Nullable()
    private android.widget.LinearLayout llStaffRecord;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tvSyncStaffRecord;
    @org.jetbrains.annotations.Nullable()
    private com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout srlStaffList;
    @org.jetbrains.annotations.Nullable()
    private androidx.recyclerview.widget.RecyclerView rvStaffList;
    @org.jetbrains.annotations.Nullable()
    private com.smartportable.demo2.adapters.StaffInfoAdapter staffInfoAdapter;
    @org.jetbrains.annotations.NotNull()
    private androidx.fragment.app.FragmentManager fm;
    @org.jetbrains.annotations.Nullable()
    private com.smartportable.demo2.db.DatabaseHandler db;
    @org.jetbrains.annotations.Nullable()
    private android.widget.SearchView searchView;
    private int currentPageIndex;
    private int totalPageCount;
    @org.jetbrains.annotations.NotNull()
    private android.view.View.OnClickListener mOnSnackBarSetUrlClickListener;
    @org.jetbrains.annotations.Nullable()
    private retrofit2.Call<com.smartportable.demo2.models.StaffInfoModel> apiRetrofit;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.smartportable.demo2.models.StaffInfoModel.DataBean> getStaffList() {
        return null;
    }
    
    public final void setStaffList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.StaffInfoModel.DataBean> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.LinearLayout getLlStaffRecord() {
        return null;
    }
    
    public final void setLlStaffRecord(@org.jetbrains.annotations.Nullable()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTvSyncStaffRecord() {
        return null;
    }
    
    public final void setTvSyncStaffRecord(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout getSrlStaffList() {
        return null;
    }
    
    public final void setSrlStaffList(@org.jetbrains.annotations.Nullable()
    com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.recyclerview.widget.RecyclerView getRvStaffList() {
        return null;
    }
    
    public final void setRvStaffList(@org.jetbrains.annotations.Nullable()
    androidx.recyclerview.widget.RecyclerView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.smartportable.demo2.adapters.StaffInfoAdapter getStaffInfoAdapter() {
        return null;
    }
    
    public final void setStaffInfoAdapter(@org.jetbrains.annotations.Nullable()
    com.smartportable.demo2.adapters.StaffInfoAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.fragment.app.FragmentManager getFm() {
        return null;
    }
    
    public final void setFm(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentManager p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.smartportable.demo2.db.DatabaseHandler getDb() {
        return null;
    }
    
    public final void setDb(@org.jetbrains.annotations.Nullable()
    com.smartportable.demo2.db.DatabaseHandler p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.SearchView getSearchView() {
        return null;
    }
    
    public final void setSearchView(@org.jetbrains.annotations.Nullable()
    android.widget.SearchView p0) {
    }
    
    public final int getCurrentPageIndex() {
        return 0;
    }
    
    public final void setCurrentPageIndex(int p0) {
    }
    
    public final int getTotalPageCount() {
        return 0;
    }
    
    public final void setTotalPageCount(int p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void init() {
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.NotNull()
    android.view.Menu menu) {
        return false;
    }
    
    public final void onBackPressOperation() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    @java.lang.Override()
    public void onRefresh(@org.jetbrains.annotations.Nullable()
    com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection direction) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View.OnClickListener getMOnSnackBarSetUrlClickListener() {
        return null;
    }
    
    public final void setMOnSnackBarSetUrlClickListener(@org.jetbrains.annotations.NotNull()
    android.view.View.OnClickListener p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final retrofit2.Call<com.smartportable.demo2.models.StaffInfoModel> getApiRetrofit() {
        return null;
    }
    
    public final void setApiRetrofit(@org.jetbrains.annotations.Nullable()
    retrofit2.Call<com.smartportable.demo2.models.StaffInfoModel> p0) {
    }
    
    public final void SyncStaffInfoAPI(int staffId) {
    }
    
    @java.lang.Override()
    public boolean onQueryTextSubmit(@org.jetbrains.annotations.Nullable()
    java.lang.String searchKeyword) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onQueryTextChange(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
        return false;
    }
    
    public final void showStaffInfoFromDB(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.db.DatabaseHandler db) {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    public SyncStaffInfoActivity() {
        super();
    }
}