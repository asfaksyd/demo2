package com.smartportable.demo2.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\u001bB\u0005\u00a2\u0006\u0002\u0010\u0002R2\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004j\n\u0012\u0004\u0012\u00020\u0005\u0018\u0001`\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR \u0010\u000b\u001a\u0004\u0018\u00010\f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R \u0010\u0011\u001a\u0004\u0018\u00010\f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u000e\"\u0004\b\u0013\u0010\u0010R\"\u0010\u0014\u001a\u0004\u0018\u00010\u00158\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u001a\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019\u00a8\u0006\u001c"}, d2 = {"Lcom/smartportable/demo2/models/StaffInfoModel;", "Ljava/io/Serializable;", "()V", "data", "Ljava/util/ArrayList;", "Lcom/smartportable/demo2/models/StaffInfoModel$DataBean;", "Lkotlin/collections/ArrayList;", "getData", "()Ljava/util/ArrayList;", "setData", "(Ljava/util/ArrayList;)V", "message", "", "getMessage", "()Ljava/lang/String;", "setMessage", "(Ljava/lang/String;)V", "pageCount", "getPageCount", "setPageCount", "status", "", "getStatus", "()Ljava/lang/Integer;", "setStatus", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "DataBean", "app_debug"})
public final class StaffInfoModel implements java.io.Serializable {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "Status")
    private java.lang.Integer status;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "Message")
    private java.lang.String message;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "PageCount")
    private java.lang.String pageCount;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "Data")
    private java.util.ArrayList<com.smartportable.demo2.models.StaffInfoModel.DataBean> data;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getStatus() {
        return null;
    }
    
    public final void setStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPageCount() {
        return null;
    }
    
    public final void setPageCount(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.ArrayList<com.smartportable.demo2.models.StaffInfoModel.DataBean> getData() {
        return null;
    }
    
    public final void setData(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<com.smartportable.demo2.models.StaffInfoModel.DataBean> p0) {
    }
    
    public StaffInfoModel() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\bA\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR \u0010\f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR \u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\bR \u0010\u0012\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0006\"\u0004\b\u0014\u0010\bR \u0010\u0015\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0006\"\u0004\b\u0017\u0010\bR \u0010\u0018\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0006\"\u0004\b\u001a\u0010\bR \u0010\u001b\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0006\"\u0004\b\u001d\u0010\bR \u0010\u001e\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0006\"\u0004\b \u0010\bR \u0010!\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u0006\"\u0004\b#\u0010\bR \u0010$\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\u0006\"\u0004\b&\u0010\bR \u0010\'\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0006\"\u0004\b)\u0010\bR \u0010*\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\u0006\"\u0004\b,\u0010\bR \u0010-\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\u0006\"\u0004\b/\u0010\bR \u00100\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u0010\u0006\"\u0004\b2\u0010\bR \u00103\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\u0006\"\u0004\b5\u0010\bR \u00106\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u0010\u0006\"\u0004\b8\u0010\bR \u00109\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010\u0006\"\u0004\b;\u0010\bR \u0010<\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b=\u0010\u0006\"\u0004\b>\u0010\bR \u0010?\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b@\u0010\u0006\"\u0004\bA\u0010\bR \u0010B\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bC\u0010\u0006\"\u0004\bD\u0010\b\u00a8\u0006E"}, d2 = {"Lcom/smartportable/demo2/models/StaffInfoModel$DataBean;", "", "()V", "address", "", "getAddress", "()Ljava/lang/String;", "setAddress", "(Ljava/lang/String;)V", "appNo", "getAppNo", "setAppNo", "cardNumber", "getCardNumber", "setCardNumber", "cardstatus", "getCardstatus", "setCardstatus", "department", "getDepartment", "setDepartment", "dob", "getDob", "setDob", "emailId", "getEmailId", "setEmailId", "empPhoto", "getEmpPhoto", "setEmpPhoto", "fullName", "getFullName", "setFullName", "gender", "getGender", "setGender", "idNo", "getIdNo", "setIdNo", "idNumber", "getIdNumber", "setIdNumber", "image64byte", "getImage64byte", "setImage64byte", "isactive", "getIsactive", "setIsactive", "issueDate", "getIssueDate", "setIssueDate", "jobTitle", "getJobTitle", "setJobTitle", "password", "getPassword", "setPassword", "signature", "getSignature", "setSignature", "slNo", "getSlNo", "setSlNo", "staffID", "getStaffID", "setStaffID", "uID", "getUID", "setUID", "app_debug"})
    public static final class DataBean {
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Staff_Id")
        private java.lang.String staffID;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Application_No")
        private java.lang.String appNo;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "SL_No")
        private java.lang.String slNo;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "UID")
        private java.lang.String uID;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Full_Name")
        private java.lang.String fullName;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Gender")
        private java.lang.String gender;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "DOB")
        private java.lang.String dob;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Department")
        private java.lang.String department;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Job_Title")
        private java.lang.String jobTitle;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Emp_Photo")
        private java.lang.String empPhoto;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Signature")
        private java.lang.String signature;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Address")
        private java.lang.String address;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "ID_no")
        private java.lang.String idNo;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Issue_Date")
        private java.lang.String issueDate;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "cardstatus")
        private java.lang.String cardstatus;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "cardnumber")
        private java.lang.String cardNumber;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Email_id")
        private java.lang.String emailId;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Isactive")
        private java.lang.String isactive;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Password")
        private java.lang.String password;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Id_Number")
        private java.lang.String idNumber;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Image64byte")
        private java.lang.String image64byte;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getStaffID() {
            return null;
        }
        
        public final void setStaffID(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getAppNo() {
            return null;
        }
        
        public final void setAppNo(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getSlNo() {
            return null;
        }
        
        public final void setSlNo(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getUID() {
            return null;
        }
        
        public final void setUID(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getFullName() {
            return null;
        }
        
        public final void setFullName(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getGender() {
            return null;
        }
        
        public final void setGender(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getDob() {
            return null;
        }
        
        public final void setDob(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getDepartment() {
            return null;
        }
        
        public final void setDepartment(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getJobTitle() {
            return null;
        }
        
        public final void setJobTitle(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getEmpPhoto() {
            return null;
        }
        
        public final void setEmpPhoto(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getSignature() {
            return null;
        }
        
        public final void setSignature(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getAddress() {
            return null;
        }
        
        public final void setAddress(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getIdNo() {
            return null;
        }
        
        public final void setIdNo(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getIssueDate() {
            return null;
        }
        
        public final void setIssueDate(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCardstatus() {
            return null;
        }
        
        public final void setCardstatus(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCardNumber() {
            return null;
        }
        
        public final void setCardNumber(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getEmailId() {
            return null;
        }
        
        public final void setEmailId(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getIsactive() {
            return null;
        }
        
        public final void setIsactive(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPassword() {
            return null;
        }
        
        public final void setPassword(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getIdNumber() {
            return null;
        }
        
        public final void setIdNumber(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getImage64byte() {
            return null;
        }
        
        public final void setImage64byte(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        public DataBean() {
            super();
        }
    }
}