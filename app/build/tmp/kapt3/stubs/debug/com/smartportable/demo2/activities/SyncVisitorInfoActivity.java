package com.smartportable.demo2.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00bc\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0001\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010]\u001a\u00020^2\u0006\u0010_\u001a\u00020`2\u0006\u0010a\u001a\u00020`J\u0006\u0010b\u001a\u00020^J\u0006\u0010c\u001a\u00020^J\b\u0010d\u001a\u00020^H\u0016J\u0012\u0010e\u001a\u00020^2\b\u0010f\u001a\u0004\u0018\u00010gH\u0014J\u0010\u0010h\u001a\u00020i2\u0006\u0010j\u001a\u00020kH\u0016J\b\u0010l\u001a\u00020^H\u0014J\u0010\u0010m\u001a\u00020i2\u0006\u0010n\u001a\u00020oH\u0016J\u0012\u0010p\u001a\u00020i2\b\u0010q\u001a\u0004\u0018\u00010`H\u0016J\u0012\u0010r\u001a\u00020i2\b\u0010s\u001a\u0004\u0018\u00010`H\u0016J\u0012\u0010t\u001a\u00020^2\b\u0010u\u001a\u0004\u0018\u00010vH\u0016J\u000e\u0010w\u001a\u00020^2\u0006\u0010\u0012\u001a\u00020\u0013R\"\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u001a\u0010\f\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001a\u0010\u0018\u001a\u00020\u0019X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010 \u001a\u0004\u0018\u00010!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R\u001a\u0010&\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u001c\u0010,\u001a\u0004\u0018\u00010-X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u001c\u00102\u001a\u0004\u0018\u000103X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u00105\"\u0004\b6\u00107R\u001c\u00108\u001a\u0004\u0018\u000109X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u001c\u0010>\u001a\u0004\u0018\u00010?X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b@\u0010A\"\u0004\bB\u0010CR\u001a\u0010D\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bE\u0010\u000f\"\u0004\bF\u0010\u0011R\u001c\u0010G\u001a\u0004\u0018\u00010HX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bI\u0010J\"\u0004\bK\u0010LR\u001c\u0010M\u001a\u0004\u0018\u00010HX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bN\u0010J\"\u0004\bO\u0010LR\u001c\u0010P\u001a\u0004\u0018\u00010QX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bR\u0010S\"\u0004\bT\u0010UR\"\u0010V\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010X0WX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bY\u0010Z\"\u0004\b[\u0010\\\u00a8\u0006x"}, d2 = {"Lcom/smartportable/demo2/activities/SyncVisitorInfoActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout$OnRefreshListener;", "Landroid/widget/SearchView$OnQueryTextListener;", "()V", "apiRetrofit", "Lretrofit2/Call;", "Lcom/smartportable/demo2/models/VisitorInfoModel;", "getApiRetrofit", "()Lretrofit2/Call;", "setApiRetrofit", "(Lretrofit2/Call;)V", "currentPageIndex", "", "getCurrentPageIndex", "()I", "setCurrentPageIndex", "(I)V", "db", "Lcom/smartportable/demo2/db/DatabaseHandler;", "getDb", "()Lcom/smartportable/demo2/db/DatabaseHandler;", "setDb", "(Lcom/smartportable/demo2/db/DatabaseHandler;)V", "fm", "Landroidx/fragment/app/FragmentManager;", "getFm", "()Landroidx/fragment/app/FragmentManager;", "setFm", "(Landroidx/fragment/app/FragmentManager;)V", "hStopRequest", "Landroid/os/Handler;", "llVisitorRecord", "Landroid/widget/LinearLayout;", "getLlVisitorRecord", "()Landroid/widget/LinearLayout;", "setLlVisitorRecord", "(Landroid/widget/LinearLayout;)V", "mOnSnackBarSetUrlClickListener", "Landroid/view/View$OnClickListener;", "getMOnSnackBarSetUrlClickListener", "()Landroid/view/View$OnClickListener;", "setMOnSnackBarSetUrlClickListener", "(Landroid/view/View$OnClickListener;)V", "retrofitObj", "", "getRetrofitObj", "()Ljava/lang/Void;", "setRetrofitObj", "(Ljava/lang/Void;)V", "rvVisitorList", "Landroidx/recyclerview/widget/RecyclerView;", "getRvVisitorList", "()Landroidx/recyclerview/widget/RecyclerView;", "setRvVisitorList", "(Landroidx/recyclerview/widget/RecyclerView;)V", "searchView", "Landroid/widget/SearchView;", "getSearchView", "()Landroid/widget/SearchView;", "setSearchView", "(Landroid/widget/SearchView;)V", "srlVisitorList", "Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout;", "getSrlVisitorList", "()Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout;", "setSrlVisitorList", "(Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout;)V", "totalPageCount", "getTotalPageCount", "setTotalPageCount", "tvNoVisData", "Landroid/widget/TextView;", "getTvNoVisData", "()Landroid/widget/TextView;", "setTvNoVisData", "(Landroid/widget/TextView;)V", "tvSyncVisitorRecord", "getTvSyncVisitorRecord", "setTvSyncVisitorRecord", "visitorInfoAdapter", "Lcom/smartportable/demo2/adapters/VisitorInfoAdapter;", "getVisitorInfoAdapter", "()Lcom/smartportable/demo2/adapters/VisitorInfoAdapter;", "setVisitorInfoAdapter", "(Lcom/smartportable/demo2/adapters/VisitorInfoAdapter;)V", "visitorList", "Ljava/util/ArrayList;", "Lcom/smartportable/demo2/models/VisitorInfoModel$DataBean;", "getVisitorList", "()Ljava/util/ArrayList;", "setVisitorList", "(Ljava/util/ArrayList;)V", "SyncVisitorInfoAPI", "", "visCardNo", "", "visRegNo", "init", "onBackPressOperation", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "", "menu", "Landroid/view/Menu;", "onDestroy", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onQueryTextChange", "p0", "onQueryTextSubmit", "searchKeyword", "onRefresh", "direction", "Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayoutDirection;", "showVisitorInfoFromDB", "app_debug"})
public final class SyncVisitorInfoActivity extends androidx.appcompat.app.AppCompatActivity implements com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout.OnRefreshListener, android.widget.SearchView.OnQueryTextListener {
    private android.os.Handler hStopRequest;
    @org.jetbrains.annotations.Nullable()
    private com.smartportable.demo2.db.DatabaseHandler db;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.smartportable.demo2.models.VisitorInfoModel.DataBean> visitorList;
    @org.jetbrains.annotations.Nullable()
    private android.widget.LinearLayout llVisitorRecord;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tvSyncVisitorRecord;
    @org.jetbrains.annotations.Nullable()
    private com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout srlVisitorList;
    @org.jetbrains.annotations.Nullable()
    private androidx.recyclerview.widget.RecyclerView rvVisitorList;
    @org.jetbrains.annotations.Nullable()
    private com.smartportable.demo2.adapters.VisitorInfoAdapter visitorInfoAdapter;
    @org.jetbrains.annotations.NotNull()
    private androidx.fragment.app.FragmentManager fm;
    @org.jetbrains.annotations.Nullable()
    private android.widget.SearchView searchView;
    private int currentPageIndex;
    private int totalPageCount;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Void retrofitObj;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tvNoVisData;
    @org.jetbrains.annotations.NotNull()
    private android.view.View.OnClickListener mOnSnackBarSetUrlClickListener;
    @org.jetbrains.annotations.Nullable()
    private retrofit2.Call<com.smartportable.demo2.models.VisitorInfoModel> apiRetrofit;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.smartportable.demo2.db.DatabaseHandler getDb() {
        return null;
    }
    
    public final void setDb(@org.jetbrains.annotations.Nullable()
    com.smartportable.demo2.db.DatabaseHandler p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.smartportable.demo2.models.VisitorInfoModel.DataBean> getVisitorList() {
        return null;
    }
    
    public final void setVisitorList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.VisitorInfoModel.DataBean> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.LinearLayout getLlVisitorRecord() {
        return null;
    }
    
    public final void setLlVisitorRecord(@org.jetbrains.annotations.Nullable()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTvSyncVisitorRecord() {
        return null;
    }
    
    public final void setTvSyncVisitorRecord(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout getSrlVisitorList() {
        return null;
    }
    
    public final void setSrlVisitorList(@org.jetbrains.annotations.Nullable()
    com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.recyclerview.widget.RecyclerView getRvVisitorList() {
        return null;
    }
    
    public final void setRvVisitorList(@org.jetbrains.annotations.Nullable()
    androidx.recyclerview.widget.RecyclerView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.smartportable.demo2.adapters.VisitorInfoAdapter getVisitorInfoAdapter() {
        return null;
    }
    
    public final void setVisitorInfoAdapter(@org.jetbrains.annotations.Nullable()
    com.smartportable.demo2.adapters.VisitorInfoAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.fragment.app.FragmentManager getFm() {
        return null;
    }
    
    public final void setFm(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentManager p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.SearchView getSearchView() {
        return null;
    }
    
    public final void setSearchView(@org.jetbrains.annotations.Nullable()
    android.widget.SearchView p0) {
    }
    
    public final int getCurrentPageIndex() {
        return 0;
    }
    
    public final void setCurrentPageIndex(int p0) {
    }
    
    public final int getTotalPageCount() {
        return 0;
    }
    
    public final void setTotalPageCount(int p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Void getRetrofitObj() {
        return null;
    }
    
    public final void setRetrofitObj(@org.jetbrains.annotations.Nullable()
    java.lang.Void p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTvNoVisData() {
        return null;
    }
    
    public final void setTvNoVisData(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.NotNull()
    android.view.Menu menu) {
        return false;
    }
    
    public final void onBackPressOperation() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public final void init() {
    }
    
    @java.lang.Override()
    public void onRefresh(@org.jetbrains.annotations.Nullable()
    com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection direction) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View.OnClickListener getMOnSnackBarSetUrlClickListener() {
        return null;
    }
    
    public final void setMOnSnackBarSetUrlClickListener(@org.jetbrains.annotations.NotNull()
    android.view.View.OnClickListener p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final retrofit2.Call<com.smartportable.demo2.models.VisitorInfoModel> getApiRetrofit() {
        return null;
    }
    
    public final void setApiRetrofit(@org.jetbrains.annotations.Nullable()
    retrofit2.Call<com.smartportable.demo2.models.VisitorInfoModel> p0) {
    }
    
    public final void SyncVisitorInfoAPI(@org.jetbrains.annotations.NotNull()
    java.lang.String visCardNo, @org.jetbrains.annotations.NotNull()
    java.lang.String visRegNo) {
    }
    
    public final void showVisitorInfoFromDB(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.db.DatabaseHandler db) {
    }
    
    @java.lang.Override()
    public boolean onQueryTextSubmit(@org.jetbrains.annotations.Nullable()
    java.lang.String searchKeyword) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onQueryTextChange(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    public SyncVisitorInfoActivity() {
        super();
    }
}