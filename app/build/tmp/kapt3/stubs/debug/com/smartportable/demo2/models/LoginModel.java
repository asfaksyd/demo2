package com.smartportable.demo2.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001\u0015B\u0005\u00a2\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001e\u0010\u000f\u001a\u00020\u00108\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014\u00a8\u0006\u0016"}, d2 = {"Lcom/smartportable/demo2/models/LoginModel;", "", "()V", "data", "Lcom/smartportable/demo2/models/LoginModel$DataBean;", "getData", "()Lcom/smartportable/demo2/models/LoginModel$DataBean;", "setData", "(Lcom/smartportable/demo2/models/LoginModel$DataBean;)V", "message", "", "getMessage", "()Ljava/lang/String;", "setMessage", "(Ljava/lang/String;)V", "status", "", "getStatus", "()I", "setStatus", "(I)V", "DataBean", "app_debug"})
public final class LoginModel {
    @com.google.gson.annotations.SerializedName(value = "Status")
    private int status;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "Message")
    private java.lang.String message;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "Data")
    private com.smartportable.demo2.models.LoginModel.DataBean data;
    
    public final int getStatus() {
        return 0;
    }
    
    public final void setStatus(int p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.smartportable.demo2.models.LoginModel.DataBean getData() {
        return null;
    }
    
    public final void setData(@org.jetbrains.annotations.Nullable()
    com.smartportable.demo2.models.LoginModel.DataBean p0) {
    }
    
    public LoginModel() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b \u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR \u0010\f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR \u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\bR\"\u0010\u0012\u001a\u0004\u0018\u00010\u00138\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0018\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R \u0010\u0019\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u0006\"\u0004\b\u001b\u0010\bR \u0010\u001c\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u0006\"\u0004\b\u001e\u0010\bR \u0010\u001f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010\u0006\"\u0004\b!\u0010\bR \u0010\"\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u0006\"\u0004\b$\u0010\bR\u001e\u0010%\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\u0006\"\u0004\b\'\u0010\bR\u001e\u0010(\u001a\u00020\u00138\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,R \u0010-\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\u0006\"\u0004\b/\u0010\bR \u00100\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u0010\u0006\"\u0004\b2\u0010\b\u00a8\u00063"}, d2 = {"Lcom/smartportable/demo2/models/LoginModel$DataBean;", "", "()V", "canteenId", "", "getCanteenId", "()Ljava/lang/String;", "setCanteenId", "(Ljava/lang/String;)V", "canteenName", "getCanteenName", "setCanteenName", "email", "getEmail", "setEmail", "fullName", "getFullName", "setFullName", "id", "", "getId", "()Ljava/lang/Integer;", "setId", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "password", "getPassword", "setPassword", "photo", "getPhoto", "setPhoto", "staffid", "getStaffid", "setStaffid", "token", "getToken", "setToken", "tokenExpireTime", "getTokenExpireTime", "setTokenExpireTime", "userId", "getUserId", "()I", "setUserId", "(I)V", "userTypeId", "getUserTypeId", "setUserTypeId", "userTypeName", "getUserTypeName", "setUserTypeName", "app_debug"})
    public static final class DataBean {
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Id")
        private java.lang.Integer id;
        @com.google.gson.annotations.SerializedName(value = "UserId")
        private int userId;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Token")
        private java.lang.String token;
        @org.jetbrains.annotations.NotNull()
        @com.google.gson.annotations.SerializedName(value = "TokenExpiredTime")
        private java.lang.String tokenExpireTime;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Email")
        private java.lang.String email;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "Password")
        private java.lang.String password;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "StaffId")
        private java.lang.String staffid;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "FullName")
        private java.lang.String fullName;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "StaffImage")
        private java.lang.String photo;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "CanteenId")
        private java.lang.String canteenId;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "CanteenName")
        private java.lang.String canteenName;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "UserTypeId")
        private java.lang.String userTypeId;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "UserTypeName")
        private java.lang.String userTypeName;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getId() {
            return null;
        }
        
        public final void setId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer p0) {
        }
        
        public final int getUserId() {
            return 0;
        }
        
        public final void setUserId(int p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getToken() {
            return null;
        }
        
        public final void setToken(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTokenExpireTime() {
            return null;
        }
        
        public final void setTokenExpireTime(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getEmail() {
            return null;
        }
        
        public final void setEmail(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPassword() {
            return null;
        }
        
        public final void setPassword(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getStaffid() {
            return null;
        }
        
        public final void setStaffid(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getFullName() {
            return null;
        }
        
        public final void setFullName(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPhoto() {
            return null;
        }
        
        public final void setPhoto(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCanteenId() {
            return null;
        }
        
        public final void setCanteenId(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCanteenName() {
            return null;
        }
        
        public final void setCanteenName(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getUserTypeId() {
            return null;
        }
        
        public final void setUserTypeId(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getUserTypeName() {
            return null;
        }
        
        public final void setUserTypeName(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        public DataBean() {
            super();
        }
    }
}