package com.smartportable.demo2.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010A\u001a\u00020B2\u0006\u0010C\u001a\u00020\fJ\u0006\u0010D\u001a\u00020BJ\u0006\u0010E\u001a\u00020BJ\b\u0010F\u001a\u00020BH\u0016J\u0012\u0010G\u001a\u00020B2\b\u0010H\u001a\u0004\u0018\u00010IH\u0014J\b\u0010J\u001a\u00020BH\u0014J\u0010\u0010K\u001a\u00020L2\u0006\u0010M\u001a\u00020NH\u0016J\u0010\u0010O\u001a\u00020B2\u0006\u0010P\u001a\u00020QH\u0016J\u000e\u0010R\u001a\u00020B2\u0006\u0010\u0011\u001a\u00020\u0012R\"\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0019\u001a\u0004\u0018\u00010\u001aX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\u00020 X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R\u001c\u0010%\u001a\u0004\u0018\u00010&X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\'\u0010(\"\u0004\b)\u0010*R \u0010+\u001a\b\u0012\u0004\u0012\u00020-0,X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u001c\u00102\u001a\u0004\u0018\u000103X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u00105\"\u0004\b6\u00107R\u001c\u00108\u001a\u0004\u0018\u000109X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u001a\u0010>\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b?\u0010\u000e\"\u0004\b@\u0010\u0010\u00a8\u0006S"}, d2 = {"Lcom/smartportable/demo2/activities/SyncReadersActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout$OnRefreshListener;", "()V", "apiRetrofit", "Lretrofit2/Call;", "Lcom/smartportable/demo2/models/ReaderInfoModel;", "getApiRetrofit", "()Lretrofit2/Call;", "setApiRetrofit", "(Lretrofit2/Call;)V", "currentPageIndex", "", "getCurrentPageIndex", "()I", "setCurrentPageIndex", "(I)V", "db", "Lcom/smartportable/demo2/db/DatabaseHandler;", "getDb", "()Lcom/smartportable/demo2/db/DatabaseHandler;", "setDb", "(Lcom/smartportable/demo2/db/DatabaseHandler;)V", "hStopRequest", "Landroid/os/Handler;", "llReaderList", "Landroid/widget/LinearLayout;", "getLlReaderList", "()Landroid/widget/LinearLayout;", "setLlReaderList", "(Landroid/widget/LinearLayout;)V", "mOnSnackBarSetUrlClickListener", "Landroid/view/View$OnClickListener;", "getMOnSnackBarSetUrlClickListener", "()Landroid/view/View$OnClickListener;", "setMOnSnackBarSetUrlClickListener", "(Landroid/view/View$OnClickListener;)V", "readersAdapter", "Lcom/smartportable/demo2/adapters/ReaderListAdapter;", "getReadersAdapter", "()Lcom/smartportable/demo2/adapters/ReaderListAdapter;", "setReadersAdapter", "(Lcom/smartportable/demo2/adapters/ReaderListAdapter;)V", "readersList", "Ljava/util/ArrayList;", "Lcom/smartportable/demo2/models/ReaderInfoModel$DataBean;", "getReadersList", "()Ljava/util/ArrayList;", "setReadersList", "(Ljava/util/ArrayList;)V", "rvReaderList", "Landroidx/recyclerview/widget/RecyclerView;", "getRvReaderList", "()Landroidx/recyclerview/widget/RecyclerView;", "setRvReaderList", "(Landroidx/recyclerview/widget/RecyclerView;)V", "srlReaderList", "Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout;", "getSrlReaderList", "()Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout;", "setSrlReaderList", "(Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout;)V", "totalPageCount", "getTotalPageCount", "setTotalPageCount", "SyncReadersAPI", "", "readerId", "init", "onBackPressOperation", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "onRefresh", "direction", "Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayoutDirection;", "showReadersFromDB", "app_debug"})
public final class SyncReadersActivity extends androidx.appcompat.app.AppCompatActivity implements com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout.OnRefreshListener {
    private android.os.Handler hStopRequest;
    @org.jetbrains.annotations.Nullable()
    private com.smartportable.demo2.db.DatabaseHandler db;
    @org.jetbrains.annotations.Nullable()
    private android.widget.LinearLayout llReaderList;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.smartportable.demo2.models.ReaderInfoModel.DataBean> readersList;
    @org.jetbrains.annotations.Nullable()
    private com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout srlReaderList;
    @org.jetbrains.annotations.Nullable()
    private androidx.recyclerview.widget.RecyclerView rvReaderList;
    @org.jetbrains.annotations.Nullable()
    private com.smartportable.demo2.adapters.ReaderListAdapter readersAdapter;
    private int currentPageIndex;
    private int totalPageCount;
    @org.jetbrains.annotations.NotNull()
    private android.view.View.OnClickListener mOnSnackBarSetUrlClickListener;
    @org.jetbrains.annotations.Nullable()
    private retrofit2.Call<com.smartportable.demo2.models.ReaderInfoModel> apiRetrofit;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final com.smartportable.demo2.db.DatabaseHandler getDb() {
        return null;
    }
    
    public final void setDb(@org.jetbrains.annotations.Nullable()
    com.smartportable.demo2.db.DatabaseHandler p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.LinearLayout getLlReaderList() {
        return null;
    }
    
    public final void setLlReaderList(@org.jetbrains.annotations.Nullable()
    android.widget.LinearLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.smartportable.demo2.models.ReaderInfoModel.DataBean> getReadersList() {
        return null;
    }
    
    public final void setReadersList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.ReaderInfoModel.DataBean> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout getSrlReaderList() {
        return null;
    }
    
    public final void setSrlReaderList(@org.jetbrains.annotations.Nullable()
    com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.recyclerview.widget.RecyclerView getRvReaderList() {
        return null;
    }
    
    public final void setRvReaderList(@org.jetbrains.annotations.Nullable()
    androidx.recyclerview.widget.RecyclerView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.smartportable.demo2.adapters.ReaderListAdapter getReadersAdapter() {
        return null;
    }
    
    public final void setReadersAdapter(@org.jetbrains.annotations.Nullable()
    com.smartportable.demo2.adapters.ReaderListAdapter p0) {
    }
    
    public final int getCurrentPageIndex() {
        return 0;
    }
    
    public final void setCurrentPageIndex(int p0) {
    }
    
    public final int getTotalPageCount() {
        return 0;
    }
    
    public final void setTotalPageCount(int p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void init() {
    }
    
    @java.lang.Override()
    public void onRefresh(@org.jetbrains.annotations.NotNull()
    com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection direction) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View.OnClickListener getMOnSnackBarSetUrlClickListener() {
        return null;
    }
    
    public final void setMOnSnackBarSetUrlClickListener(@org.jetbrains.annotations.NotNull()
    android.view.View.OnClickListener p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final retrofit2.Call<com.smartportable.demo2.models.ReaderInfoModel> getApiRetrofit() {
        return null;
    }
    
    public final void setApiRetrofit(@org.jetbrains.annotations.Nullable()
    retrofit2.Call<com.smartportable.demo2.models.ReaderInfoModel> p0) {
    }
    
    public final void SyncReadersAPI(int readerId) {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public final void showReadersFromDB(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.db.DatabaseHandler db) {
    }
    
    public final void onBackPressOperation() {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    public SyncReadersActivity() {
        super();
    }
}