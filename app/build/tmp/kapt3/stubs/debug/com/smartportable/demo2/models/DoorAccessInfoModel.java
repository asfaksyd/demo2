package com.smartportable.demo2.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\"\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\t\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000f\u00a8\u0006\u0010"}, d2 = {"Lcom/smartportable/demo2/models/DoorAccessInfoModel;", "Ljava/io/Serializable;", "()V", "doorId", "", "getDoorId", "()Ljava/lang/Integer;", "setDoorId", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "doorName", "", "getDoorName", "()Ljava/lang/String;", "setDoorName", "(Ljava/lang/String;)V", "app_debug"})
public final class DoorAccessInfoModel implements java.io.Serializable {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "DoorId")
    private java.lang.Integer doorId;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "DoorName")
    private java.lang.String doorName;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getDoorId() {
        return null;
    }
    
    public final void setDoorId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDoorName() {
        return null;
    }
    
    public final void setDoorName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public DoorAccessInfoModel() {
        super();
    }
}