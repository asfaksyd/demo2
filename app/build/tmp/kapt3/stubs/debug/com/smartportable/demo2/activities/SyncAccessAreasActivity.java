package com.smartportable.demo2.activities;

import java.lang.System;

/**
 * * Created by sCampus on 1/3/2017.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00b0\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0001\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 n2\u00020\u00012\u00020\u0002:\u0001nB\u0005\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010Y\u001a\u00020Z2\u0006\u0010[\u001a\u00020\\J\u0006\u0010]\u001a\u00020ZJ\b\u0010^\u001a\u00020ZH\u0016J\u0012\u0010_\u001a\u00020Z2\b\u0010`\u001a\u0004\u0018\u00010aH\u0014J\u0010\u0010b\u001a\u00020c2\u0006\u0010d\u001a\u00020eH\u0016J\b\u0010f\u001a\u00020ZH\u0014J\u0010\u0010g\u001a\u00020c2\u0006\u0010h\u001a\u00020iH\u0016J\u0012\u0010j\u001a\u00020Z2\b\u0010k\u001a\u0004\u0018\u00010lH\u0016J\u000e\u0010m\u001a\u00020Z2\u0006\u0010\u001e\u001a\u00020\u001fR\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\"\u0010\n\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\f0\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\"\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001a\u0010\u0018\u001a\u00020\u0019X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u001a\u0010\u001e\u001a\u00020\u001fX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R\u001a\u0010$\u001a\u00020%X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\'\"\u0004\b(\u0010)R\u0010\u0010*\u001a\u0004\u0018\u00010+X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010,\u001a\u00020-X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u001a\u00102\u001a\u00020-X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b3\u0010/\"\u0004\b4\u00101R\u001a\u00105\u001a\u00020-X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b6\u0010/\"\u0004\b7\u00101R\u001c\u00108\u001a\u0004\u0018\u000109X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u001c\u0010>\u001a\u0004\u0018\u00010?X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b@\u0010A\"\u0004\bB\u0010CR\u001c\u0010D\u001a\u0004\u0018\u00010EX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bF\u0010G\"\u0004\bH\u0010IR\u001c\u0010J\u001a\u0004\u0018\u00010KX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bL\u0010M\"\u0004\bN\u0010OR\u001a\u0010P\u001a\u00020\u0019X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bQ\u0010\u001b\"\u0004\bR\u0010\u001dR\u001c\u0010S\u001a\u0004\u0018\u00010TX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bU\u0010V\"\u0004\bW\u0010X\u00a8\u0006o"}, d2 = {"Lcom/smartportable/demo2/activities/SyncAccessAreasActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout$OnRefreshListener;", "()V", "accessAreaInfoAdapter", "Lcom/smartportable/demo2/adapters/AccessAreaAdapter;", "getAccessAreaInfoAdapter", "()Lcom/smartportable/demo2/adapters/AccessAreaAdapter;", "setAccessAreaInfoAdapter", "(Lcom/smartportable/demo2/adapters/AccessAreaAdapter;)V", "accessAreaList", "Ljava/util/ArrayList;", "Lcom/smartportable/demo2/models/AGInfoModel$DataBean;", "getAccessAreaList", "()Ljava/util/ArrayList;", "setAccessAreaList", "(Ljava/util/ArrayList;)V", "apiRetrofit", "Lretrofit2/Call;", "Lcom/smartportable/demo2/models/AGInfoModel;", "getApiRetrofit", "()Lretrofit2/Call;", "setApiRetrofit", "(Lretrofit2/Call;)V", "currentPageIndex", "", "getCurrentPageIndex", "()I", "setCurrentPageIndex", "(I)V", "db", "Lcom/smartportable/demo2/db/DatabaseHandler;", "getDb", "()Lcom/smartportable/demo2/db/DatabaseHandler;", "setDb", "(Lcom/smartportable/demo2/db/DatabaseHandler;)V", "fm", "Landroidx/fragment/app/FragmentManager;", "getFm", "()Landroidx/fragment/app/FragmentManager;", "setFm", "(Landroidx/fragment/app/FragmentManager;)V", "hStopRequest", "Landroid/os/Handler;", "mBtnOnClickListener", "Landroid/view/View$OnClickListener;", "getMBtnOnClickListener", "()Landroid/view/View$OnClickListener;", "setMBtnOnClickListener", "(Landroid/view/View$OnClickListener;)V", "mOnSnackBarClickListener", "getMOnSnackBarClickListener", "setMOnSnackBarClickListener", "mOnSnackBarSetUrlClickListener", "getMOnSnackBarSetUrlClickListener", "setMOnSnackBarSetUrlClickListener", "retrofitObj", "", "getRetrofitObj", "()Ljava/lang/Void;", "setRetrofitObj", "(Ljava/lang/Void;)V", "rvAccessAreaList", "Landroidx/recyclerview/widget/RecyclerView;", "getRvAccessAreaList", "()Landroidx/recyclerview/widget/RecyclerView;", "setRvAccessAreaList", "(Landroidx/recyclerview/widget/RecyclerView;)V", "searchView", "Landroid/widget/SearchView;", "getSearchView", "()Landroid/widget/SearchView;", "setSearchView", "(Landroid/widget/SearchView;)V", "srlAccessAreaList", "Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout;", "getSrlAccessAreaList", "()Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout;", "setSrlAccessAreaList", "(Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayout;)V", "totalPageCount", "getTotalPageCount", "setTotalPageCount", "tvNoVisData", "Landroid/widget/TextView;", "getTvNoVisData", "()Landroid/widget/TextView;", "setTvNoVisData", "(Landroid/widget/TextView;)V", "SyncAccessAreaInfoAPI", "", "agId", "", "onBackPressOperation", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "", "menu", "Landroid/view/Menu;", "onDestroy", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onRefresh", "direction", "Lcom/omadahealth/github/swipyrefreshlayout/library/SwipyRefreshLayoutDirection;", "showAccessAreasInfoFromDB", "Companion", "app_debug"})
public final class SyncAccessAreasActivity extends androidx.appcompat.app.AppCompatActivity implements com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout.OnRefreshListener {
    private android.os.Handler hStopRequest;
    @org.jetbrains.annotations.NotNull()
    public com.smartportable.demo2.db.DatabaseHandler db;
    @org.jetbrains.annotations.Nullable()
    private retrofit2.Call<com.smartportable.demo2.models.AGInfoModel> apiRetrofit;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.smartportable.demo2.models.AGInfoModel.DataBean> accessAreaList;
    @org.jetbrains.annotations.Nullable()
    private com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout srlAccessAreaList;
    @org.jetbrains.annotations.Nullable()
    private androidx.recyclerview.widget.RecyclerView rvAccessAreaList;
    @org.jetbrains.annotations.Nullable()
    private com.smartportable.demo2.adapters.AccessAreaAdapter accessAreaInfoAdapter;
    @org.jetbrains.annotations.NotNull()
    private androidx.fragment.app.FragmentManager fm;
    @org.jetbrains.annotations.Nullable()
    private android.widget.SearchView searchView;
    private int currentPageIndex;
    private int totalPageCount;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Void retrofitObj;
    @org.jetbrains.annotations.Nullable()
    private android.widget.TextView tvNoVisData;
    @org.jetbrains.annotations.NotNull()
    private android.view.View.OnClickListener mOnSnackBarClickListener;
    @org.jetbrains.annotations.NotNull()
    private android.view.View.OnClickListener mOnSnackBarSetUrlClickListener;
    @org.jetbrains.annotations.NotNull()
    private android.view.View.OnClickListener mBtnOnClickListener;
    private static final java.lang.String TAG = "SyncAreas";
    public static final com.smartportable.demo2.activities.SyncAccessAreasActivity.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.smartportable.demo2.db.DatabaseHandler getDb() {
        return null;
    }
    
    public final void setDb(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.db.DatabaseHandler p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final retrofit2.Call<com.smartportable.demo2.models.AGInfoModel> getApiRetrofit() {
        return null;
    }
    
    public final void setApiRetrofit(@org.jetbrains.annotations.Nullable()
    retrofit2.Call<com.smartportable.demo2.models.AGInfoModel> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.smartportable.demo2.models.AGInfoModel.DataBean> getAccessAreaList() {
        return null;
    }
    
    public final void setAccessAreaList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.AGInfoModel.DataBean> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout getSrlAccessAreaList() {
        return null;
    }
    
    public final void setSrlAccessAreaList(@org.jetbrains.annotations.Nullable()
    com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.recyclerview.widget.RecyclerView getRvAccessAreaList() {
        return null;
    }
    
    public final void setRvAccessAreaList(@org.jetbrains.annotations.Nullable()
    androidx.recyclerview.widget.RecyclerView p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.smartportable.demo2.adapters.AccessAreaAdapter getAccessAreaInfoAdapter() {
        return null;
    }
    
    public final void setAccessAreaInfoAdapter(@org.jetbrains.annotations.Nullable()
    com.smartportable.demo2.adapters.AccessAreaAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.fragment.app.FragmentManager getFm() {
        return null;
    }
    
    public final void setFm(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentManager p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.SearchView getSearchView() {
        return null;
    }
    
    public final void setSearchView(@org.jetbrains.annotations.Nullable()
    android.widget.SearchView p0) {
    }
    
    public final int getCurrentPageIndex() {
        return 0;
    }
    
    public final void setCurrentPageIndex(int p0) {
    }
    
    public final int getTotalPageCount() {
        return 0;
    }
    
    public final void setTotalPageCount(int p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Void getRetrofitObj() {
        return null;
    }
    
    public final void setRetrofitObj(@org.jetbrains.annotations.Nullable()
    java.lang.Void p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.widget.TextView getTvNoVisData() {
        return null;
    }
    
    public final void setTvNoVisData(@org.jetbrains.annotations.Nullable()
    android.widget.TextView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View.OnClickListener getMOnSnackBarClickListener() {
        return null;
    }
    
    public final void setMOnSnackBarClickListener(@org.jetbrains.annotations.NotNull()
    android.view.View.OnClickListener p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View.OnClickListener getMOnSnackBarSetUrlClickListener() {
        return null;
    }
    
    public final void setMOnSnackBarSetUrlClickListener(@org.jetbrains.annotations.NotNull()
    android.view.View.OnClickListener p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View.OnClickListener getMBtnOnClickListener() {
        return null;
    }
    
    public final void setMBtnOnClickListener(@org.jetbrains.annotations.NotNull()
    android.view.View.OnClickListener p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.NotNull()
    android.view.Menu menu) {
        return false;
    }
    
    public final void SyncAccessAreaInfoAPI(@org.jetbrains.annotations.NotNull()
    java.lang.String agId) {
    }
    
    @java.lang.Override()
    public void onRefresh(@org.jetbrains.annotations.Nullable()
    com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection direction) {
    }
    
    public final void showAccessAreasInfoFromDB(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.db.DatabaseHandler db) {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public final void onBackPressOperation() {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    public SyncAccessAreasActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/smartportable/demo2/activities/SyncAccessAreasActivity$Companion;", "", "()V", "TAG", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}