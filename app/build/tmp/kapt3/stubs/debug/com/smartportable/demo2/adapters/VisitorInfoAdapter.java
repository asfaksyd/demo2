package com.smartportable.demo2.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001+B1\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u001a\u0010\u0005\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u0006j\n\u0012\u0006\u0012\u0004\u0018\u00010\u0007`\b\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\b\u0010\u001e\u001a\u00020\u001fH\u0016J\u0018\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u00022\u0006\u0010#\u001a\u00020\u001fH\u0016J\u0018\u0010$\u001a\u00020\u00022\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020\u001fH\u0016J\u000e\u0010(\u001a\u00020!2\u0006\u0010#\u001a\u00020\u001fJ*\u0010)\u001a\u00020!2\u001a\u0010\u0005\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u0006j\n\u0012\u0006\u0012\u0004\u0018\u00010\u0007`\b2\u0006\u0010*\u001a\u00020\u0011R\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\u00020\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R.\u0010\u0005\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u0006j\n\u0012\u0006\u0012\u0004\u0018\u00010\u0007`\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001d\u00a8\u0006,"}, d2 = {"Lcom/smartportable/demo2/adapters/VisitorInfoAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/smartportable/demo2/adapters/VisitorInfoAdapter$VisitorInfoViewHolder;", "mContext", "Landroid/content/Context;", "visitorList", "Ljava/util/ArrayList;", "Lcom/smartportable/demo2/models/VisitorInfoModel$DataBean;", "Lkotlin/collections/ArrayList;", "fm", "Landroidx/fragment/app/FragmentManager;", "(Landroid/content/Context;Ljava/util/ArrayList;Landroidx/fragment/app/FragmentManager;)V", "getFm", "()Landroidx/fragment/app/FragmentManager;", "setFm", "(Landroidx/fragment/app/FragmentManager;)V", "forMemberAccess", "", "getForMemberAccess", "()Z", "setForMemberAccess", "(Z)V", "getMContext", "()Landroid/content/Context;", "setMContext", "(Landroid/content/Context;)V", "getVisitorList", "()Ljava/util/ArrayList;", "setVisitorList", "(Ljava/util/ArrayList;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "showVisitorInfoDialog", "updateVisitorInfo", "forMemAcc", "VisitorInfoViewHolder", "app_debug"})
public final class VisitorInfoAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.smartportable.demo2.adapters.VisitorInfoAdapter.VisitorInfoViewHolder> {
    @org.jetbrains.annotations.Nullable()
    private android.content.Context mContext;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.smartportable.demo2.models.VisitorInfoModel.DataBean> visitorList;
    private boolean forMemberAccess;
    @org.jetbrains.annotations.NotNull()
    private androidx.fragment.app.FragmentManager fm;
    
    @org.jetbrains.annotations.Nullable()
    public final android.content.Context getMContext() {
        return null;
    }
    
    public final void setMContext(@org.jetbrains.annotations.Nullable()
    android.content.Context p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.smartportable.demo2.models.VisitorInfoModel.DataBean> getVisitorList() {
        return null;
    }
    
    public final void setVisitorList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.VisitorInfoModel.DataBean> p0) {
    }
    
    public final boolean getForMemberAccess() {
        return false;
    }
    
    public final void setForMemberAccess(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.fragment.app.FragmentManager getFm() {
        return null;
    }
    
    public final void setFm(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentManager p0) {
    }
    
    public final void updateVisitorInfo(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.VisitorInfoModel.DataBean> visitorList, boolean forMemAcc) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.smartportable.demo2.adapters.VisitorInfoAdapter.VisitorInfoViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.smartportable.demo2.adapters.VisitorInfoAdapter.VisitorInfoViewHolder holder, int position) {
    }
    
    public final void showVisitorInfoDialog(int position) {
    }
    
    public VisitorInfoAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.smartportable.demo2.models.VisitorInfoModel.DataBean> visitorList, @org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentManager fm) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0019\u0010\t\u001a\n \u000b*\u0004\u0018\u00010\n0\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0019\u0010\u000e\u001a\n \u000b*\u0004\u0018\u00010\n0\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\rR\u0019\u0010\u0010\u001a\n \u000b*\u0004\u0018\u00010\n0\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\rR\u0019\u0010\u0012\u001a\n \u000b*\u0004\u0018\u00010\n0\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\r\u00a8\u0006\u0014"}, d2 = {"Lcom/smartportable/demo2/adapters/VisitorInfoAdapter$VisitorInfoViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "ivVisPhoto", "Lde/hdodenhof/circleimageview/CircleImageView;", "getIvVisPhoto", "()Lde/hdodenhof/circleimageview/CircleImageView;", "tvVisCardNo", "Landroid/widget/TextView;", "kotlin.jvm.PlatformType", "getTvVisCardNo", "()Landroid/widget/TextView;", "tvVisComp", "getTvVisComp", "tvVisName", "getTvVisName", "tvVisRegNo", "getTvVisRegNo", "app_debug"})
    public static final class VisitorInfoViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final de.hdodenhof.circleimageview.CircleImageView ivVisPhoto = null;
        private final android.widget.TextView tvVisRegNo = null;
        private final android.widget.TextView tvVisName = null;
        private final android.widget.TextView tvVisComp = null;
        private final android.widget.TextView tvVisCardNo = null;
        
        @org.jetbrains.annotations.NotNull()
        public final de.hdodenhof.circleimageview.CircleImageView getIvVisPhoto() {
            return null;
        }
        
        public final android.widget.TextView getTvVisRegNo() {
            return null;
        }
        
        public final android.widget.TextView getTvVisName() {
            return null;
        }
        
        public final android.widget.TextView getTvVisComp() {
            return null;
        }
        
        public final android.widget.TextView getTvVisCardNo() {
            return null;
        }
        
        public VisitorInfoViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
    }
}