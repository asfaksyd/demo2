package com.smartportable.demo2.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\u001bB\u0005\u00a2\u0006\u0002\u0010\u0002R2\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004j\n\u0012\u0004\u0012\u00020\u0005\u0018\u0001`\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR \u0010\u000b\u001a\u0004\u0018\u00010\f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R \u0010\u0011\u001a\u0004\u0018\u00010\f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u000e\"\u0004\b\u0013\u0010\u0010R\"\u0010\u0014\u001a\u0004\u0018\u00010\u00158\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u001a\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019\u00a8\u0006\u001c"}, d2 = {"Lcom/smartportable/demo2/models/VisitorInfoModel;", "Ljava/io/Serializable;", "()V", "data", "Ljava/util/ArrayList;", "Lcom/smartportable/demo2/models/VisitorInfoModel$DataBean;", "Lkotlin/collections/ArrayList;", "getData", "()Ljava/util/ArrayList;", "setData", "(Ljava/util/ArrayList;)V", "message", "", "getMessage", "()Ljava/lang/String;", "setMessage", "(Ljava/lang/String;)V", "pageCount", "getPageCount", "setPageCount", "status", "", "getStatus", "()Ljava/lang/Integer;", "setStatus", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "DataBean", "app_debug"})
public final class VisitorInfoModel implements java.io.Serializable {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "Status")
    private java.lang.Integer status;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "Message")
    private java.lang.String message;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "PageCount")
    private java.lang.String pageCount;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "Data")
    private java.util.ArrayList<com.smartportable.demo2.models.VisitorInfoModel.DataBean> data;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getStatus() {
        return null;
    }
    
    public final void setStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPageCount() {
        return null;
    }
    
    public final void setPageCount(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.ArrayList<com.smartportable.demo2.models.VisitorInfoModel.DataBean> getData() {
        return null;
    }
    
    public final void setData(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<com.smartportable.demo2.models.VisitorInfoModel.DataBean> p0) {
    }
    
    public VisitorInfoModel() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b/\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR \u0010\t\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR \u0010\f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR \u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\bR \u0010\u0012\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0006\"\u0004\b\u0014\u0010\bR \u0010\u0015\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0006\"\u0004\b\u0017\u0010\bR \u0010\u0018\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0006\"\u0004\b\u001a\u0010\bR \u0010\u001b\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0006\"\u0004\b\u001d\u0010\bR \u0010\u001e\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0006\"\u0004\b \u0010\bR \u0010!\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u0006\"\u0004\b#\u0010\bR \u0010$\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\u0006\"\u0004\b&\u0010\bR \u0010\'\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0006\"\u0004\b)\u0010\bR \u0010*\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\u0006\"\u0004\b,\u0010\bR \u0010-\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\u0006\"\u0004\b/\u0010\bR \u00100\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u0010\u0006\"\u0004\b2\u0010\b\u00a8\u00063"}, d2 = {"Lcom/smartportable/demo2/models/VisitorInfoModel$DataBean;", "", "()V", "companyName", "", "getCompanyName", "()Ljava/lang/String;", "setCompanyName", "(Ljava/lang/String;)V", "visitReason", "getVisitReason", "setVisitReason", "visitorAccessLevel", "getVisitorAccessLevel", "setVisitorAccessLevel", "visitorCardNumber", "getVisitorCardNumber", "setVisitorCardNumber", "visitorCardStatus", "getVisitorCardStatus", "setVisitorCardStatus", "visitorCheckInTime", "getVisitorCheckInTime", "setVisitorCheckInTime", "visitorCheckOutTime", "getVisitorCheckOutTime", "setVisitorCheckOutTime", "visitorEmailId", "getVisitorEmailId", "setVisitorEmailId", "visitorFirstName", "getVisitorFirstName", "setVisitorFirstName", "visitorLasttName", "getVisitorLasttName", "setVisitorLasttName", "visitorPhoneNo", "getVisitorPhoneNo", "setVisitorPhoneNo", "visitorPhoto", "getVisitorPhoto", "setVisitorPhoto", "visitorRegNo", "getVisitorRegNo", "setVisitorRegNo", "visitorStatus", "getVisitorStatus", "setVisitorStatus", "visitorType", "getVisitorType", "setVisitorType", "app_debug"})
    public static final class DataBean {
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "VisitorRegNo")
        private java.lang.String visitorRegNo;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "VisitorFirstName")
        private java.lang.String visitorFirstName;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "VisitorLastName")
        private java.lang.String visitorLasttName;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "CompanyName")
        private java.lang.String companyName;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "VisitorType")
        private java.lang.String visitorType;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "VisitReason")
        private java.lang.String visitReason;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "PhoneNo")
        private java.lang.String visitorPhoneNo;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "EmailId")
        private java.lang.String visitorEmailId;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "VisitorPhoto")
        private java.lang.String visitorPhoto;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "VisitorAccessLevel")
        private java.lang.String visitorAccessLevel;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "VisitorCheckInTime")
        private java.lang.String visitorCheckInTime;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "VisitorCheckOutTime")
        private java.lang.String visitorCheckOutTime;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "VisitorCardNumber")
        private java.lang.String visitorCardNumber;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "VisitorCardStatus")
        private java.lang.String visitorCardStatus;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "VisitorStatus")
        private java.lang.String visitorStatus;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getVisitorRegNo() {
            return null;
        }
        
        public final void setVisitorRegNo(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getVisitorFirstName() {
            return null;
        }
        
        public final void setVisitorFirstName(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getVisitorLasttName() {
            return null;
        }
        
        public final void setVisitorLasttName(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCompanyName() {
            return null;
        }
        
        public final void setCompanyName(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getVisitorType() {
            return null;
        }
        
        public final void setVisitorType(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getVisitReason() {
            return null;
        }
        
        public final void setVisitReason(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getVisitorPhoneNo() {
            return null;
        }
        
        public final void setVisitorPhoneNo(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getVisitorEmailId() {
            return null;
        }
        
        public final void setVisitorEmailId(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getVisitorPhoto() {
            return null;
        }
        
        public final void setVisitorPhoto(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getVisitorAccessLevel() {
            return null;
        }
        
        public final void setVisitorAccessLevel(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getVisitorCheckInTime() {
            return null;
        }
        
        public final void setVisitorCheckInTime(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getVisitorCheckOutTime() {
            return null;
        }
        
        public final void setVisitorCheckOutTime(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getVisitorCardNumber() {
            return null;
        }
        
        public final void setVisitorCardNumber(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getVisitorCardStatus() {
            return null;
        }
        
        public final void setVisitorCardStatus(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getVisitorStatus() {
            return null;
        }
        
        public final void setVisitorStatus(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        public DataBean() {
            super();
        }
    }
}