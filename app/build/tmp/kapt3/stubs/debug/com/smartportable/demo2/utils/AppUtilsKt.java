package com.smartportable.demo2.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000`\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0010\u0006\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\u001a,\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u00012\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001\u001a\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0011\u001a\u00020\u0001\u001a\u001e\u0010\u0012\u001a\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u00012\u0006\u0010\u0014\u001a\u00020\u00012\u0006\u0010\u0015\u001a\u00020\u0001\u001a\u0016\u0010\u0016\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0017\u001a\u00020\u0001\u001a\u000e\u0010\u0018\u001a\u00020\u00012\u0006\u0010\u0019\u001a\u00020\u0001\u001a\u000e\u0010\u001a\u001a\u00020\u00012\u0006\u0010\u001b\u001a\u00020\u0001\u001a\u000e\u0010\u001c\u001a\u00020\u00012\u0006\u0010\u001d\u001a\u00020\u0001\u001a\u000e\u0010\u001e\u001a\u00020\t2\u0006\u0010\u001f\u001a\u00020\u000b\u001a\u000e\u0010 \u001a\u00020\u00012\u0006\u0010!\u001a\u00020\"\u001a\u0010\u0010#\u001a\u00020\u00012\u0006\u0010\u001f\u001a\u00020\u000bH\u0007\u001a\u0006\u0010$\u001a\u00020\u0001\u001a\u0006\u0010%\u001a\u00020\u0001\u001a#\u0010&\u001a\u00020\'2\b\u0010\u001f\u001a\u0004\u0018\u00010\u000b2\f\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00010)\u00a2\u0006\u0002\u0010*\u001a\u000e\u0010+\u001a\u00020\'2\u0006\u0010,\u001a\u00020\u0001\u001a\u001a\u0010-\u001a\u00020\'2\u0006\u0010\u001f\u001a\u00020\u000b2\n\u0010.\u001a\u0006\u0012\u0002\b\u00030/\u001a\u000e\u00100\u001a\u00020\'2\u0006\u0010\u001f\u001a\u00020\u000b\u001a\u000e\u00101\u001a\u00020\'2\u0006\u00102\u001a\u00020\u0001\u001a\u000e\u00103\u001a\u00020\'2\u0006\u0010\u001d\u001a\u00020\u0001\u001a\u000e\u00104\u001a\u00020\'2\u0006\u0010\u001d\u001a\u00020\u0001\u001a\u0018\u00105\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u001f\u001a\u00020\u000b2\u0006\u00106\u001a\u00020\u0001\u001a\u0016\u00107\u001a\u00020\t2\u0006\u0010\u001f\u001a\u00020\u000b2\u0006\u00108\u001a\u00020\u0001\u001a\u000e\u00109\u001a\u00020\t2\u0006\u0010\u001f\u001a\u00020\u000b\u001a\u0006\u0010:\u001a\u00020\t\u001a\u0016\u0010;\u001a\u00020\t2\u0006\u0010<\u001a\u00020=2\u0006\u0010>\u001a\u00020?\u001a\u0016\u0010@\u001a\u00020\t2\u0006\u0010\u001f\u001a\u00020\u000b2\u0006\u0010A\u001a\u00020\u0001\u001a\u001f\u0010B\u001a\u00020\t2\b\u0010\u001f\u001a\u0004\u0018\u00010\u000b2\b\u0010C\u001a\u0004\u0018\u00010D\u00a2\u0006\u0002\u0010E\u001a\u001a\u0010B\u001a\u00020\t2\b\u0010\u001f\u001a\u0004\u0018\u00010\u000b2\b\u0010C\u001a\u0004\u0018\u00010\u0001\u001a\u000e\u0010F\u001a\u00020\'2\u0006\u0010G\u001a\u00020\u0001\u001a\u0016\u0010H\u001a\u00020\t2\u0006\u0010\u001f\u001a\u00020\u000b2\u0006\u0010\u0017\u001a\u00020\u0001\u001a\n\u0010I\u001a\u00020\t*\u00020J\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0004\u0010\u0005\"\u0004\b\u0006\u0010\u0007\u00a8\u0006K"}, d2 = {"TAG", "", "progressDialog", "Landroid/app/ProgressDialog;", "getProgressDialog", "()Landroid/app/ProgressDialog;", "setProgressDialog", "(Landroid/app/ProgressDialog;)V", "ImageToLocal", "", "mContext", "Landroid/content/Context;", "imageUrl", "imageView", "Landroid/widget/ImageView;", "imageFolder", "convert12To24FormatTime", "inputTime", "convertDateTime", "fromFormat", "toFormat", "dateOriginalGot", "customToastOnTop", "msg", "decodeBase64", "base64", "decodeUnicode", "theString", "encodeStringToBase64", "str", "forceLogout", "context", "formatString", "d", "", "getAndroidId", "getCurrentTimezoneOffset", "getLocalIpAddress", "hasPermissions", "", "permissions", "", "(Landroid/content/Context;[Ljava/lang/String;)Z", "isColorValid", "colorString", "isMyServiceRunning", "serviceClass", "Ljava/lang/Class;", "isNetworkAvailable", "isValidMail", "mailString", "isValidText", "isValidUserName", "loadJSONFromAsset", "fileName", "showDialogPermissionRational", "strMessage", "showProgressDialog", "stopProgress", "stopRequest", "activity", "Landroid/app/Activity;", "hStopRequest", "Landroid/os/Handler;", "submitSearchQuery", "query", "toast", "text", "", "(Landroid/content/Context;Ljava/lang/Integer;)V", "validatePassword", "password", "warningMessage", "hideKeyboard", "Landroid/view/View;", "app_debug"})
public final class AppUtilsKt {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TAG = "AppUtils";
    @org.jetbrains.annotations.Nullable()
    private static android.app.ProgressDialog progressDialog;
    
    @org.jetbrains.annotations.Nullable()
    public static final android.app.ProgressDialog getProgressDialog() {
        return null;
    }
    
    public static final void setProgressDialog(@org.jetbrains.annotations.Nullable()
    android.app.ProgressDialog p0) {
    }
    
    public static final void showProgressDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public static final void stopProgress() {
    }
    
    public static final void toast(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String text) {
    }
    
    public static final void toast(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.Integer text) {
    }
    
    public static final boolean isValidText(@org.jetbrains.annotations.NotNull()
    java.lang.String str) {
        return false;
    }
    
    public static final boolean isValidUserName(@org.jetbrains.annotations.NotNull()
    java.lang.String str) {
        return false;
    }
    
    public static final boolean isValidMail(@org.jetbrains.annotations.NotNull()
    java.lang.String mailString) {
        return false;
    }
    
    public static final boolean isNetworkAvailable(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.annotation.SuppressLint(value = {"MissingPermission", "HardwareIds"})
    public static final java.lang.String getAndroidId(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getLocalIpAddress() {
        return null;
    }
    
    public static final boolean validatePassword(@org.jetbrains.annotations.NotNull()
    java.lang.String password) {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String convertDateTime(@org.jetbrains.annotations.NotNull()
    java.lang.String fromFormat, @org.jetbrains.annotations.NotNull()
    java.lang.String toFormat, @org.jetbrains.annotations.NotNull()
    java.lang.String dateOriginalGot) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String formatString(double d) {
        return null;
    }
    
    public static final boolean hasPermissions(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions) {
        return false;
    }
    
    public static final void hideKeyboard(@org.jetbrains.annotations.NotNull()
    android.view.View $receiver) {
    }
    
    public static final void submitSearchQuery(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String query) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String encodeStringToBase64(@org.jetbrains.annotations.NotNull()
    java.lang.String str) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String decodeBase64(@org.jetbrains.annotations.NotNull()
    java.lang.String base64) {
        return null;
    }
    
    public static final boolean isColorValid(@org.jetbrains.annotations.NotNull()
    java.lang.String colorString) {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public static final java.lang.String loadJSONFromAsset(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String fileName) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String decodeUnicode(@org.jetbrains.annotations.NotNull()
    java.lang.String theString) {
        return null;
    }
    
    public static final void showDialogPermissionRational(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String strMessage) {
    }
    
    public static final boolean isMyServiceRunning(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.Class<?> serviceClass) {
        return false;
    }
    
    public static final void customToastOnTop(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    java.lang.String msg) {
    }
    
    public static final void forceLogout(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public static final void warningMessage(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String msg) {
    }
    
    public static final void stopRequest(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    android.os.Handler hStopRequest) {
    }
    
    public static final void ImageToLocal(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.Nullable()
    java.lang.String imageUrl, @org.jetbrains.annotations.Nullable()
    android.widget.ImageView imageView, @org.jetbrains.annotations.Nullable()
    java.lang.String imageFolder) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getCurrentTimezoneOffset() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public static final java.lang.String convert12To24FormatTime(@org.jetbrains.annotations.NotNull()
    java.lang.String inputTime) {
        return null;
    }
}