package com.androidscade.nfckotlindemo;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0010\u0012\n\u0002\b\u0018\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u00107\u001a\u00020\u00132\u0006\u00108\u001a\u00020\"J\u001e\u00107\u001a\u00020\u00132\u0006\u00108\u001a\u00020\"2\u0006\u00109\u001a\u00020\u00132\u0006\u0010:\u001a\u00020;J\u0016\u0010<\u001a\u00020\u00132\u0006\u00108\u001a\u00020\"2\u0006\u0010:\u001a\u00020;J\u000e\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020\u0013J\u000e\u0010@\u001a\u00020\u00042\u0006\u0010A\u001a\u00020\u0013J\u000e\u0010B\u001a\u00020\u00132\u0006\u0010A\u001a\u00020\u0013J\u000e\u0010C\u001a\u00020\u00042\u0006\u0010:\u001a\u00020;J\u0016\u0010D\u001a\u00020>2\u0006\u0010E\u001a\u00020F2\u0006\u0010G\u001a\u00020\u0004J\u000e\u0010H\u001a\u00020\"2\u0006\u00108\u001a\u00020\"R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR\u001a\u0010\f\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR\u001a\u0010\u000f\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\bR\u001a\u0010\u0012\u001a\u00020\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001a\u0010\u0018\u001a\u00020\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0015\"\u0004\b\u001a\u0010\u0017R\u001a\u0010\u001b\u001a\u00020\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0015\"\u0004\b\u001d\u0010\u0017R\u001a\u0010\u001e\u001a\u00020\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0015\"\u0004\b \u0010\u0017R\"\u0010!\u001a\n #*\u0004\u0018\u00010\"0\"X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010%\"\u0004\b&\u0010\'R\"\u0010(\u001a\n #*\u0004\u0018\u00010\"0\"X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010%\"\u0004\b*\u0010\'R\"\u0010+\u001a\n #*\u0004\u0018\u00010\"0\"X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010%\"\u0004\b-\u0010\'R\u001a\u0010.\u001a\u00020\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u0010\u0015\"\u0004\b0\u0010\u0017R\u001a\u00101\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b2\u0010\u0006\"\u0004\b3\u0010\bR\u001a\u00104\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b5\u0010\u0006\"\u0004\b6\u0010\b\u00a8\u0006I"}, d2 = {"Lcom/androidscade/nfckotlindemo/NFCUtil;", "", "()V", "BYTE_COUNT_FOR_MIFARE", "", "getBYTE_COUNT_FOR_MIFARE", "()I", "setBYTE_COUNT_FOR_MIFARE", "(I)V", "BYTE_LENGTH_FOR_DESFIRE_TO_REVRSE", "getBYTE_LENGTH_FOR_DESFIRE_TO_REVRSE", "setBYTE_LENGTH_FOR_DESFIRE_TO_REVRSE", "BYTE_LENGTH_FOR_MIFARE", "getBYTE_LENGTH_FOR_MIFARE", "setBYTE_LENGTH_FOR_MIFARE", "BYTE_LENGTH_FOR_MIFARE_TO_REVERSE", "getBYTE_LENGTH_FOR_MIFARE_TO_REVERSE", "setBYTE_LENGTH_FOR_MIFARE_TO_REVERSE", "C5_MODEL_NAME", "", "getC5_MODEL_NAME", "()Ljava/lang/String;", "setC5_MODEL_NAME", "(Ljava/lang/String;)V", "CARD_TYPE_DESFIRE", "getCARD_TYPE_DESFIRE", "setCARD_TYPE_DESFIRE", "CARD_TYPE_MIFARE", "getCARD_TYPE_MIFARE", "setCARD_TYPE_MIFARE", "LSB", "getLSB", "setLSB", "MIFARE_AUTH_KEY", "", "kotlin.jvm.PlatformType", "getMIFARE_AUTH_KEY", "()[B", "setMIFARE_AUTH_KEY", "([B)V", "MIFARE_MAD_KEY", "getMIFARE_MAD_KEY", "setMIFARE_MAD_KEY", "MIFARE_NDEF_KEY", "getMIFARE_NDEF_KEY", "setMIFARE_NDEF_KEY", "MSB", "getMSB", "setMSB", "READER_FLAGS", "getREADER_FLAGS", "setREADER_FLAGS", "SECTOR_COUNT_FOR_MIFARE", "getSECTOR_COUNT_FOR_MIFARE", "setSECTOR_COUNT_FOR_MIFARE", "ByteArrayToHexString", "bytes", "typeStr", "context", "Landroid/content/Context;", "ByteArrayToHexStringDesfire", "checkAlphNumericString", "", "str", "hexToDec", "hex", "hexToString", "isDeviceHaveNFC", "mifareKeyAuthentication", "mifareClassic", "Landroid/nfc/tech/MifareClassic;", "sectorIndex", "trim", "app_debug"})
public final class NFCUtil {
    private static int READER_FLAGS;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String C5_MODEL_NAME;
    private static byte[] MIFARE_AUTH_KEY;
    private static byte[] MIFARE_NDEF_KEY;
    private static byte[] MIFARE_MAD_KEY;
    private static int SECTOR_COUNT_FOR_MIFARE;
    private static int BYTE_COUNT_FOR_MIFARE;
    private static int BYTE_LENGTH_FOR_MIFARE;
    private static int BYTE_LENGTH_FOR_MIFARE_TO_REVERSE;
    private static int BYTE_LENGTH_FOR_DESFIRE_TO_REVRSE;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String CARD_TYPE_MIFARE;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String CARD_TYPE_DESFIRE;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String MSB;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String LSB;
    public static final com.androidscade.nfckotlindemo.NFCUtil INSTANCE = null;
    
    public final int getREADER_FLAGS() {
        return 0;
    }
    
    public final void setREADER_FLAGS(int p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getC5_MODEL_NAME() {
        return null;
    }
    
    public final void setC5_MODEL_NAME(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public final byte[] getMIFARE_AUTH_KEY() {
        return null;
    }
    
    public final void setMIFARE_AUTH_KEY(byte[] p0) {
    }
    
    public final byte[] getMIFARE_NDEF_KEY() {
        return null;
    }
    
    public final void setMIFARE_NDEF_KEY(byte[] p0) {
    }
    
    public final byte[] getMIFARE_MAD_KEY() {
        return null;
    }
    
    public final void setMIFARE_MAD_KEY(byte[] p0) {
    }
    
    public final int getSECTOR_COUNT_FOR_MIFARE() {
        return 0;
    }
    
    public final void setSECTOR_COUNT_FOR_MIFARE(int p0) {
    }
    
    public final int getBYTE_COUNT_FOR_MIFARE() {
        return 0;
    }
    
    public final void setBYTE_COUNT_FOR_MIFARE(int p0) {
    }
    
    public final int getBYTE_LENGTH_FOR_MIFARE() {
        return 0;
    }
    
    public final void setBYTE_LENGTH_FOR_MIFARE(int p0) {
    }
    
    public final int getBYTE_LENGTH_FOR_MIFARE_TO_REVERSE() {
        return 0;
    }
    
    public final void setBYTE_LENGTH_FOR_MIFARE_TO_REVERSE(int p0) {
    }
    
    public final int getBYTE_LENGTH_FOR_DESFIRE_TO_REVRSE() {
        return 0;
    }
    
    public final void setBYTE_LENGTH_FOR_DESFIRE_TO_REVRSE(int p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCARD_TYPE_MIFARE() {
        return null;
    }
    
    public final void setCARD_TYPE_MIFARE(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCARD_TYPE_DESFIRE() {
        return null;
    }
    
    public final void setCARD_TYPE_DESFIRE(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMSB() {
        return null;
    }
    
    public final void setMSB(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLSB() {
        return null;
    }
    
    public final void setLSB(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public final int isDeviceHaveNFC(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return 0;
    }
    
    public final boolean checkAlphNumericString(@org.jetbrains.annotations.NotNull()
    java.lang.String str) {
        return false;
    }
    
    /**
     * * Utility class to convert a byte array to a hexadecimal string.
     *     *
     *     * @param bytes Bytes to convert
     *     * @return String, containing hexadecimal representation.
     */
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String ByteArrayToHexString(@org.jetbrains.annotations.NotNull()
    byte[] bytes, @org.jetbrains.annotations.NotNull()
    java.lang.String typeStr, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    /**
     * * Utility class to convert a byte array to a hexadecimal string.
     *     *
     *     * @param bytes Bytes to convert
     *     * @return String, containing hexadecimal representation.
     */
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String ByteArrayToHexString(@org.jetbrains.annotations.NotNull()
    byte[] bytes) {
        return null;
    }
    
    /**
     * * Utility class to convert a byte array to a hexadecimal string.
     *     *
     *     * @param bytes Bytes to convert
     *     * @return String, containing hexadecimal representation.
     */
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String ByteArrayToHexStringDesfire(@org.jetbrains.annotations.NotNull()
    byte[] bytes, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String hexToString(@org.jetbrains.annotations.NotNull()
    java.lang.String hex) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final byte[] trim(@org.jetbrains.annotations.NotNull()
    byte[] bytes) {
        return null;
    }
    
    public final boolean mifareKeyAuthentication(@org.jetbrains.annotations.NotNull()
    android.nfc.tech.MifareClassic mifareClassic, int sectorIndex) {
        return false;
    }
    
    public final int hexToDec(@org.jetbrains.annotations.NotNull()
    java.lang.String hex) {
        return 0;
    }
    
    private NFCUtil() {
        super();
    }
}