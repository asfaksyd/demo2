package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ImportFlag;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.internal.objectstore.OsObjectBuilder;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class com_smartportable_demo2_realm_ReadersModelRealmProxy extends com.smartportable.demo2.realm.ReadersModel
    implements RealmObjectProxy, com_smartportable_demo2_realm_ReadersModelRealmProxyInterface {

    static final class ReadersModelColumnInfo extends ColumnInfo {
        long maxColumnIndexValue;
        long idIndex;
        long readerIdIndex;
        long readerNameIndex;
        long readerTypeIndex;

        ReadersModelColumnInfo(OsSchemaInfo schemaInfo) {
            super(4);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("ReadersModel");
            this.idIndex = addColumnDetails("id", "id", objectSchemaInfo);
            this.readerIdIndex = addColumnDetails("readerId", "readerId", objectSchemaInfo);
            this.readerNameIndex = addColumnDetails("readerName", "readerName", objectSchemaInfo);
            this.readerTypeIndex = addColumnDetails("readerType", "readerType", objectSchemaInfo);
            this.maxColumnIndexValue = objectSchemaInfo.getMaxColumnIndex();
        }

        ReadersModelColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new ReadersModelColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final ReadersModelColumnInfo src = (ReadersModelColumnInfo) rawSrc;
            final ReadersModelColumnInfo dst = (ReadersModelColumnInfo) rawDst;
            dst.idIndex = src.idIndex;
            dst.readerIdIndex = src.readerIdIndex;
            dst.readerNameIndex = src.readerNameIndex;
            dst.readerTypeIndex = src.readerTypeIndex;
            dst.maxColumnIndexValue = src.maxColumnIndexValue;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private ReadersModelColumnInfo columnInfo;
    private ProxyState<com.smartportable.demo2.realm.ReadersModel> proxyState;

    com_smartportable_demo2_realm_ReadersModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (ReadersModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.smartportable.demo2.realm.ReadersModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public Long realmGet$id() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.idIndex)) {
            return null;
        }
        return (long) proxyState.getRow$realm().getLong(columnInfo.idIndex);
    }

    @Override
    public void realmSet$id(Long value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'id' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public Integer realmGet$readerId() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.readerIdIndex)) {
            return null;
        }
        return (int) proxyState.getRow$realm().getLong(columnInfo.readerIdIndex);
    }

    @Override
    public void realmSet$readerId(Integer value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.readerIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setLong(columnInfo.readerIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.readerIdIndex);
            return;
        }
        proxyState.getRow$realm().setLong(columnInfo.readerIdIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$readerName() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.readerNameIndex);
    }

    @Override
    public void realmSet$readerName(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.readerNameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.readerNameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.readerNameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.readerNameIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$readerType() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.readerTypeIndex);
    }

    @Override
    public void realmSet$readerType(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.readerTypeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.readerTypeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.readerTypeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.readerTypeIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("ReadersModel", 4, 0);
        builder.addPersistedProperty("id", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("readerId", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("readerName", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("readerType", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static ReadersModelColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new ReadersModelColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "ReadersModel";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "ReadersModel";
    }

    @SuppressWarnings("cast")
    public static com.smartportable.demo2.realm.ReadersModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.smartportable.demo2.realm.ReadersModel obj = null;
        if (update) {
            Table table = realm.getTable(com.smartportable.demo2.realm.ReadersModel.class);
            ReadersModelColumnInfo columnInfo = (ReadersModelColumnInfo) realm.getSchema().getColumnInfo(com.smartportable.demo2.realm.ReadersModel.class);
            long pkColumnIndex = columnInfo.idIndex;
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("id")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("id"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.smartportable.demo2.realm.ReadersModel.class), false, Collections.<String> emptyList());
                    obj = new io.realm.com_smartportable_demo2_realm_ReadersModelRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("id")) {
                if (json.isNull("id")) {
                    obj = (io.realm.com_smartportable_demo2_realm_ReadersModelRealmProxy) realm.createObjectInternal(com.smartportable.demo2.realm.ReadersModel.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.com_smartportable_demo2_realm_ReadersModelRealmProxy) realm.createObjectInternal(com.smartportable.demo2.realm.ReadersModel.class, json.getLong("id"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
            }
        }

        final com_smartportable_demo2_realm_ReadersModelRealmProxyInterface objProxy = (com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) obj;
        if (json.has("readerId")) {
            if (json.isNull("readerId")) {
                objProxy.realmSet$readerId(null);
            } else {
                objProxy.realmSet$readerId((int) json.getInt("readerId"));
            }
        }
        if (json.has("readerName")) {
            if (json.isNull("readerName")) {
                objProxy.realmSet$readerName(null);
            } else {
                objProxy.realmSet$readerName((String) json.getString("readerName"));
            }
        }
        if (json.has("readerType")) {
            if (json.isNull("readerType")) {
                objProxy.realmSet$readerType(null);
            } else {
                objProxy.realmSet$readerType((String) json.getString("readerType"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.smartportable.demo2.realm.ReadersModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final com.smartportable.demo2.realm.ReadersModel obj = new com.smartportable.demo2.realm.ReadersModel();
        final com_smartportable_demo2_realm_ReadersModelRealmProxyInterface objProxy = (com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("id")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$id((long) reader.nextLong());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$id(null);
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("readerId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$readerId((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$readerId(null);
                }
            } else if (name.equals("readerName")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$readerName((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$readerName(null);
                }
            } else if (name.equals("readerType")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$readerType((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$readerType(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
        }
        return realm.copyToRealm(obj);
    }

    private static com_smartportable_demo2_realm_ReadersModelRealmProxy newProxyInstance(BaseRealm realm, Row row) {
        // Ignore default values to avoid creating uexpected objects from RealmModel/RealmList fields
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        objectContext.set(realm, row, realm.getSchema().getColumnInfo(com.smartportable.demo2.realm.ReadersModel.class), false, Collections.<String>emptyList());
        io.realm.com_smartportable_demo2_realm_ReadersModelRealmProxy obj = new io.realm.com_smartportable_demo2_realm_ReadersModelRealmProxy();
        objectContext.clear();
        return obj;
    }

    public static com.smartportable.demo2.realm.ReadersModel copyOrUpdate(Realm realm, ReadersModelColumnInfo columnInfo, com.smartportable.demo2.realm.ReadersModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.smartportable.demo2.realm.ReadersModel) cachedRealmObject;
        }

        com.smartportable.demo2.realm.ReadersModel realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(com.smartportable.demo2.realm.ReadersModel.class);
            long pkColumnIndex = columnInfo.idIndex;
            Number value = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$id();
            long rowIndex = Table.NO_MATCH;
            if (value == null) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstLong(pkColumnIndex, value.longValue());
            }
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), columnInfo, false, Collections.<String> emptyList());
                    realmObject = new io.realm.com_smartportable_demo2_realm_ReadersModelRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, columnInfo, realmObject, object, cache, flags) : copy(realm, columnInfo, object, update, cache, flags);
    }

    public static com.smartportable.demo2.realm.ReadersModel copy(Realm realm, ReadersModelColumnInfo columnInfo, com.smartportable.demo2.realm.ReadersModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.smartportable.demo2.realm.ReadersModel) cachedRealmObject;
        }

        com_smartportable_demo2_realm_ReadersModelRealmProxyInterface realmObjectSource = (com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) newObject;

        Table table = realm.getTable(com.smartportable.demo2.realm.ReadersModel.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, columnInfo.maxColumnIndexValue, flags);

        // Add all non-"object reference" fields
        builder.addInteger(columnInfo.idIndex, realmObjectSource.realmGet$id());
        builder.addInteger(columnInfo.readerIdIndex, realmObjectSource.realmGet$readerId());
        builder.addString(columnInfo.readerNameIndex, realmObjectSource.realmGet$readerName());
        builder.addString(columnInfo.readerTypeIndex, realmObjectSource.realmGet$readerType());

        // Create the underlying object and cache it before setting any object/objectlist references
        // This will allow us to break any circular dependencies by using the object cache.
        Row row = builder.createNewObject();
        io.realm.com_smartportable_demo2_realm_ReadersModelRealmProxy realmObjectCopy = newProxyInstance(realm, row);
        cache.put(newObject, realmObjectCopy);

        return realmObjectCopy;
    }

    public static long insert(Realm realm, com.smartportable.demo2.realm.ReadersModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.smartportable.demo2.realm.ReadersModel.class);
        long tableNativePtr = table.getNativePtr();
        ReadersModelColumnInfo columnInfo = (ReadersModelColumnInfo) realm.getSchema().getColumnInfo(com.smartportable.demo2.realm.ReadersModel.class);
        long pkColumnIndex = columnInfo.idIndex;
        Object primaryKeyValue = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$id();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$id());
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        Number realmGet$readerId = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$readerId();
        if (realmGet$readerId != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.readerIdIndex, rowIndex, realmGet$readerId.longValue(), false);
        }
        String realmGet$readerName = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$readerName();
        if (realmGet$readerName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.readerNameIndex, rowIndex, realmGet$readerName, false);
        }
        String realmGet$readerType = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$readerType();
        if (realmGet$readerType != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.readerTypeIndex, rowIndex, realmGet$readerType, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.smartportable.demo2.realm.ReadersModel.class);
        long tableNativePtr = table.getNativePtr();
        ReadersModelColumnInfo columnInfo = (ReadersModelColumnInfo) realm.getSchema().getColumnInfo(com.smartportable.demo2.realm.ReadersModel.class);
        long pkColumnIndex = columnInfo.idIndex;
        com.smartportable.demo2.realm.ReadersModel object = null;
        while (objects.hasNext()) {
            object = (com.smartportable.demo2.realm.ReadersModel) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            Object primaryKeyValue = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$id();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$id());
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            Number realmGet$readerId = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$readerId();
            if (realmGet$readerId != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.readerIdIndex, rowIndex, realmGet$readerId.longValue(), false);
            }
            String realmGet$readerName = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$readerName();
            if (realmGet$readerName != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.readerNameIndex, rowIndex, realmGet$readerName, false);
            }
            String realmGet$readerType = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$readerType();
            if (realmGet$readerType != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.readerTypeIndex, rowIndex, realmGet$readerType, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.smartportable.demo2.realm.ReadersModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.smartportable.demo2.realm.ReadersModel.class);
        long tableNativePtr = table.getNativePtr();
        ReadersModelColumnInfo columnInfo = (ReadersModelColumnInfo) realm.getSchema().getColumnInfo(com.smartportable.demo2.realm.ReadersModel.class);
        long pkColumnIndex = columnInfo.idIndex;
        Object primaryKeyValue = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$id();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$id());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$id());
        }
        cache.put(object, rowIndex);
        Number realmGet$readerId = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$readerId();
        if (realmGet$readerId != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.readerIdIndex, rowIndex, realmGet$readerId.longValue(), false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.readerIdIndex, rowIndex, false);
        }
        String realmGet$readerName = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$readerName();
        if (realmGet$readerName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.readerNameIndex, rowIndex, realmGet$readerName, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.readerNameIndex, rowIndex, false);
        }
        String realmGet$readerType = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$readerType();
        if (realmGet$readerType != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.readerTypeIndex, rowIndex, realmGet$readerType, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.readerTypeIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.smartportable.demo2.realm.ReadersModel.class);
        long tableNativePtr = table.getNativePtr();
        ReadersModelColumnInfo columnInfo = (ReadersModelColumnInfo) realm.getSchema().getColumnInfo(com.smartportable.demo2.realm.ReadersModel.class);
        long pkColumnIndex = columnInfo.idIndex;
        com.smartportable.demo2.realm.ReadersModel object = null;
        while (objects.hasNext()) {
            object = (com.smartportable.demo2.realm.ReadersModel) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            Object primaryKeyValue = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$id();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$id());
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$id());
            }
            cache.put(object, rowIndex);
            Number realmGet$readerId = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$readerId();
            if (realmGet$readerId != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.readerIdIndex, rowIndex, realmGet$readerId.longValue(), false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.readerIdIndex, rowIndex, false);
            }
            String realmGet$readerName = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$readerName();
            if (realmGet$readerName != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.readerNameIndex, rowIndex, realmGet$readerName, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.readerNameIndex, rowIndex, false);
            }
            String realmGet$readerType = ((com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) object).realmGet$readerType();
            if (realmGet$readerType != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.readerTypeIndex, rowIndex, realmGet$readerType, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.readerTypeIndex, rowIndex, false);
            }
        }
    }

    public static com.smartportable.demo2.realm.ReadersModel createDetachedCopy(com.smartportable.demo2.realm.ReadersModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.smartportable.demo2.realm.ReadersModel unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new com.smartportable.demo2.realm.ReadersModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.smartportable.demo2.realm.ReadersModel) cachedObject.object;
            }
            unmanagedObject = (com.smartportable.demo2.realm.ReadersModel) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        com_smartportable_demo2_realm_ReadersModelRealmProxyInterface unmanagedCopy = (com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) unmanagedObject;
        com_smartportable_demo2_realm_ReadersModelRealmProxyInterface realmSource = (com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$id(realmSource.realmGet$id());
        unmanagedCopy.realmSet$readerId(realmSource.realmGet$readerId());
        unmanagedCopy.realmSet$readerName(realmSource.realmGet$readerName());
        unmanagedCopy.realmSet$readerType(realmSource.realmGet$readerType());

        return unmanagedObject;
    }

    static com.smartportable.demo2.realm.ReadersModel update(Realm realm, ReadersModelColumnInfo columnInfo, com.smartportable.demo2.realm.ReadersModel realmObject, com.smartportable.demo2.realm.ReadersModel newObject, Map<RealmModel, RealmObjectProxy> cache, Set<ImportFlag> flags) {
        com_smartportable_demo2_realm_ReadersModelRealmProxyInterface realmObjectTarget = (com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) realmObject;
        com_smartportable_demo2_realm_ReadersModelRealmProxyInterface realmObjectSource = (com_smartportable_demo2_realm_ReadersModelRealmProxyInterface) newObject;
        Table table = realm.getTable(com.smartportable.demo2.realm.ReadersModel.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, columnInfo.maxColumnIndexValue, flags);
        builder.addInteger(columnInfo.idIndex, realmObjectSource.realmGet$id());
        builder.addInteger(columnInfo.readerIdIndex, realmObjectSource.realmGet$readerId());
        builder.addString(columnInfo.readerNameIndex, realmObjectSource.realmGet$readerName());
        builder.addString(columnInfo.readerTypeIndex, realmObjectSource.realmGet$readerType());

        builder.updateExistingObject();
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("ReadersModel = proxy[");
        stringBuilder.append("{id:");
        stringBuilder.append(realmGet$id() != null ? realmGet$id() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{readerId:");
        stringBuilder.append(realmGet$readerId() != null ? realmGet$readerId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{readerName:");
        stringBuilder.append(realmGet$readerName() != null ? realmGet$readerName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{readerType:");
        stringBuilder.append(realmGet$readerType() != null ? realmGet$readerType() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        com_smartportable_demo2_realm_ReadersModelRealmProxy aReadersModel = (com_smartportable_demo2_realm_ReadersModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aReadersModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aReadersModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aReadersModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
