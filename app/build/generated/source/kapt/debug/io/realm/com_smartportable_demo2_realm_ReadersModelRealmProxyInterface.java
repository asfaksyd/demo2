package io.realm;


public interface com_smartportable_demo2_realm_ReadersModelRealmProxyInterface {
    public Long realmGet$id();
    public void realmSet$id(Long value);
    public Integer realmGet$readerId();
    public void realmSet$readerId(Integer value);
    public String realmGet$readerName();
    public void realmSet$readerName(String value);
    public String realmGet$readerType();
    public void realmSet$readerType(String value);
}
